package com.karza.kyc.modeltest;

import com.karza.kyc.model.TAN;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 3/16/2016.
 */
public class TANTest {

    @Test
    public void assert_that_certain_fields_cant_be_null_or_blank() {
        TAN tan = new TAN("ABCD56789A");
        Map<String, ConstraintViolation<TAN>> violationsMap = validate(tan);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("NotNull"));
    }

    @Test
    public void assert_that_document_has_to_be_there() {
        TAN tan = new TAN("ABCD56789A");
        Map<String, ConstraintViolation<TAN>> violationsMap = validate(tan);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("document"));
    }

    /*TODO don't know how it works*/
    private <T> Map<String, ConstraintViolation<T>> validate(T tan) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Map<String, ConstraintViolation<T>> violations = new HashMap<String, ConstraintViolation<T>>();
        for (ConstraintViolation<T> violation : validator.validate(tan)) {
            violations.put(violation.getPropertyPath().toString(), violation);
        }
        return violations;
    }

}
