package com.karza.kyc.modeltest;

import com.karza.kyc.model.CIN;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 3/16/2016.
 */
public class CINTest {
    @Test
    public void assert_that_certain_fields_cant_be_null_or_blank() {
        CIN cin = new CIN("L12312AP1234FTC123123");
        Map<String, ConstraintViolation<CIN>> violationsMap = validate(cin);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("NotNull"));
    }

    @Test
    public void assert_that_document_has_to_be_there() {
        CIN cin = new CIN("L12312AP1234FTC123123");
        Map<String, ConstraintViolation<CIN>> violationsMap = validate(cin);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("document"));
    }

    /*TODO don't know how it works*/
    private <T> Map<String, ConstraintViolation<T>> validate(T cin) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Map<String, ConstraintViolation<T>> violations = new HashMap<String, ConstraintViolation<T>>();
        for (ConstraintViolation<T> violation : validator.validate(cin)) {
            violations.put(violation.getPropertyPath().toString(), violation);
        }
        return violations;
    }


}
