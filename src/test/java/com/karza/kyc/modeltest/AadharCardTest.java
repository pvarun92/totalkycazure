package com.karza.kyc.modeltest;

import com.karza.kyc.model.AadharCard;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by Amit on 15-Mar-16.
 */
public class AadharCardTest {

    @Test
    public void assert_that_certain_fields_cant_be_null_or_blank() {
        AadharCard aadharCard = new AadharCard("123123123123");
        Map<String, ConstraintViolation<AadharCard>> violationsMap = validate(aadharCard);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("NotNull"));
    }

    @Test
    public void assert_that_document_has_to_be_there() {
        AadharCard aadharCard = new AadharCard("123123123123");
        Map<String, ConstraintViolation<AadharCard>> violationsMap = validate(aadharCard);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("document"));
    }

    /*TODO don't know how it works*/
    private <T> Map<String, ConstraintViolation<T>> validate(T aadharCard) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Map<String, ConstraintViolation<T>> violations = new HashMap<String, ConstraintViolation<T>>();
        for (ConstraintViolation<T> violation : validator.validate(aadharCard)) {
            violations.put(violation.getPropertyPath().toString(), violation);
        }
        return violations;
    }
}
