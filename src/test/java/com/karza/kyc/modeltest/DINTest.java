package com.karza.kyc.modeltest;

import com.karza.kyc.model.DIN;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by Administrator on 3/16/2016.
 */
public class DINTest {

    @Test
    public void assert_that_certain_fields_cant_be_null_or_blank() {
        DIN din = new DIN("12345678");
        Map<String, ConstraintViolation<DIN>> violationsMap = validate(din);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("NotNull"));
    }

    @Test
    public void assert_that_document_has_to_be_there() {
        DIN din = new DIN("12345678");
        Map<String, ConstraintViolation<DIN>> violationsMap = validate(din);
        assertTrue(violationsMap.get("document").getMessageTemplate().contains("document"));
    }

    /*TODO don't know how it works*/
    private <T> Map<String, ConstraintViolation<T>> validate(T din) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Map<String, ConstraintViolation<T>> violations = new HashMap<String, ConstraintViolation<T>>();
        for (ConstraintViolation<T> violation : validator.validate(din)) {
            violations.put(violation.getPropertyPath().toString(), violation);
        }
        return violations;
    }

}
