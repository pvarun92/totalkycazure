package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.ServiceTaxDaoImpl;
import com.karza.kyc.model.ServiceTax;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class ServiceTaxDaoImplTest extends DaoTest {

    @Autowired
    ServiceTaxDaoImpl serviceTaxDao;

    @Test
    public void all_fields_are_persisted() {
        ServiceTax serviceTax = new ServiceTax("AAAAA1234AST123");
        serviceTaxDao.save(serviceTax);
        List<ServiceTax> serviceTaxs = serviceTaxDao.findAll();
        Assert.assertEquals("AAAAA1234AST123", serviceTaxs.get(0).getDocument());
        serviceTaxDao.delete(serviceTax);
    }
}
