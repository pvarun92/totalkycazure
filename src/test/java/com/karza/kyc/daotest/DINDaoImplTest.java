package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.DINDaoImpl;
import com.karza.kyc.model.DIN;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class DINDaoImplTest extends DaoTest {

    @Autowired
    DINDaoImpl dinDao;

    @Test
    public void all_fields_are_persisted() {
        DIN din = new DIN("12345678");
        dinDao.save(din);
        List<DIN> dins = dinDao.findAll();
        Assert.assertEquals("12345678", dins.get(0).getDocument());
        dinDao.delete(din);
    }


}
