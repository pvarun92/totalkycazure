package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.PassportDaoImpl;
import com.karza.kyc.model.Passport;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class PassportDaoImplTest extends DaoTest {

    @Autowired
    PassportDaoImpl passPortDao;

    @Test
    public void all_fields_are_persisted() {
        Passport passport = new Passport("A1234567");
        passPortDao.save(passport);
        List<Passport> passPorts = passPortDao.findAll();
        Assert.assertEquals("A1234567", passPorts.get(0).getDocument());
        passPortDao.delete(passport);
    }
}
