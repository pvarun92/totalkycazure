package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.ExciseDaoImpl;
import com.karza.kyc.model.Excise;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class ExciseDaoImplTest extends DaoTest {

    @Autowired
    ExciseDaoImpl exciseDao;

    @Test
    public void all_fields_are_persisted() {
        Excise excise = new Excise("ABCDE1234AEM123");
        exciseDao.save(excise);
        List<Excise> excises = exciseDao.findAll();
        Assert.assertEquals("ABCDE1234AEM123", excises.get(0).getDocument());
        exciseDao.delete(excise);
    }
}
