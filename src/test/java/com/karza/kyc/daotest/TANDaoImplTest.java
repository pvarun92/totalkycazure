package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.TANDaoImpl;
import com.karza.kyc.model.TAN;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class TANDaoImplTest extends DaoTest {


    @Autowired
    TANDaoImpl tanDao;

    @Test
    public void all_fields_are_persisted() {
        TAN tan = new TAN("ABCD56789A");
        tanDao.save(tan);
        List<TAN> tans = tanDao.findAll();
        Assert.assertEquals("ABCD56789A", tans.get(0).getDocument());
        tanDao.delete(tan);
    }
}
