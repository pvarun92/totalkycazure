package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.TINDaoImpl;
import com.karza.kyc.model.TIN;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public class TINDaoImplTest extends DaoTest {
    @Autowired
    TINDaoImpl tinDao;

    @Test
    public void all_fields_are_persisted() {
        TIN tin = new TIN("123123123123");
        tinDao.save(tin);
        List<TIN> tins = tinDao.findAll();
        Assert.assertEquals("123123123123", tins.get(0).getDocument());
        tinDao.delete(tin);
    }
}
