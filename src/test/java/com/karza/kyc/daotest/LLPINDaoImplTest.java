package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.LLPINDaoImpl;
import com.karza.kyc.model.LLPIN;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public class LLPINDaoImplTest extends DaoTest {
    @Autowired
    LLPINDaoImpl llpinDao;

    @Test
    public void all_fields_are_persisted() {
        LLPIN llpin = new LLPIN("123123123123");
        llpinDao.save(llpin);
        List<LLPIN> llpins = llpinDao.findAll();
        Assert.assertEquals("123123123123", llpins.get(0).getDocument());
        llpinDao.delete(llpin);
    }
}
