package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.PanCardDaoImpl;
import com.karza.kyc.model.PanCard;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */

public class PanCardDaoImplTest extends DaoTest {
    @Autowired
    PanCardDaoImpl panCardDao;

    @Test
    public void all_fields_are_persisted() {
        PanCard panCard = new PanCard("123123123123");
        panCardDao.save(panCard);
        List<PanCard> panCards = panCardDao.findAll();
        Assert.assertEquals("123123123123", panCards.get(0).getDocument());
        panCardDao.delete(panCard);
    }

}
