package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.AadharCardDaoImpl;
import com.karza.kyc.model.AadharCard;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public class AadharDaoImplTest extends DaoTest {

    @Autowired
    AadharCardDaoImpl aadharCardDao;

    @Test
    public void all_fields_are_persisted() {
        AadharCard aadharCard = new AadharCard("123123123123");
        aadharCardDao.save(aadharCard);
        List<AadharCard> aadharCards = aadharCardDao.findAll();
        Assert.assertEquals("123123123123", aadharCards.get(0).getDocument());
        aadharCardDao.delete(aadharCard);
    }
}
