package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.IECDaoImpl;
import com.karza.kyc.model.IEC;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public class IECDaoImplTest extends DaoTest {
    @Autowired
    IECDaoImpl iecDao;

    @Test
    public void all_fields_are_persisted() {
        IEC iec = new IEC("123123123123");
        iecDao.save(iec);
        List<IEC> iecs = iecDao.findAll();
        Assert.assertEquals("123123123123", iecs.get(0).getDocument());
        iecDao.delete(iec);
    }
}
