package com.karza.kyc.daotest;

import com.karza.kyc.dao.impl.CINDaoImpl;
import com.karza.kyc.model.CIN;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class CINDaoImplTest extends DaoTest {


    @Autowired
    CINDaoImpl cinDao;

    @Test
    public void all_fields_are_persisted() {
        CIN cin = new CIN("L12312AP1234FTC123123");
        cinDao.save(cin);
        List<CIN> cins = cinDao.findAll();
        Assert.assertEquals("L12312AP1234FTC123123", cins.get(0).getDocument());
        cinDao.delete(cin);
    }


}
