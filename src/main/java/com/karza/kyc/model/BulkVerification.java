package com.karza.kyc.model;

public class BulkVerification {
	
	private long id;
	private String firstName;
	private String lastName;
	private String document;
	private String gender;
	private int age;
	
	
	public BulkVerification(long id, String firstName, String lastName, String document, String gender, int age) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.document = document;
		this.gender = gender;
		this.age = age;
	}
	
	public BulkVerification() {
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString(){
		return "Customer [id=" + id + ", firstName=" + firstName
                + ", lastName=" + lastName + ", document=" + document + ", gender=" + gender + ", age="
                + age + "]";

	}

}
