package com.karza.kyc.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "electricity_board")
public class ElectricityBoard{

	@Id
	@GeneratedValue
	@NotNull
	private Long id;
	private String electricityBoardName;
	private String electBoardId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getElectricityBoardName() {
		return electricityBoardName;
	}
	public void setElectricityBoardName(String electricityBoardName) {
		this.electricityBoardName = electricityBoardName;
	}
	public String getElectBoardId() {
		return electBoardId;
	}
	public void setElectBoardId(String electBoardId) {
		this.electBoardId = electBoardId;
	}
	public ElectricityBoard(Long id, String electricityBoardName, String electBoardId) {
		super();
		this.id = id;
		this.electricityBoardName = electricityBoardName;
		this.electBoardId = electBoardId;
	}
	public ElectricityBoard() {
		
		// TODO Auto-generated constructor stub
	}
	
}
