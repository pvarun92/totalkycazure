package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Fallon on 4/19/2016.
 */
@Entity
@Table(name = "fllpin")
public class FLLPIN extends BaseDocument {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue
    private Long id;
    @NotNull
    private Long customerEntityId;

    private String apiValidation;
    private Date createdAt;
    private Date updatedAt;
    private String fllpin;
    private String foreignLlpName;
    private String countryOfIncorporation;
    private String noOfAuthorisedRepresentatives;
    private String dateOfIncorporation;
    private String registeredAddress;
    private String emailID;
    private String typeOfOffice;
    private String details;
    private String mainDivisionOfBusiness;
    private String descriptionOfMainDivision;
    private String dateOfLastStatementOfAccountsAndSolvency;
    private String fllpStatus;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
   	private Integer APICount;
    private String statusAsPerSource;

    public FLLPIN() {
    }

    public FLLPIN(String document, Long customerEntityId, String apiValidation, JSONObject fllpinData) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
        this.fllpin = document;
        if (fllpinData.has("apiCount")) {
            this.APICount = (Integer)fllpinData.get("apiCount");
        }
        if(fllpinData.has("llpin")){
        fllpinData = (JSONObject)KYCUtil.checkKeyAndGetValueFromJSON(fllpinData, "llpin");
        if (fllpinData.has("Foreign LLP Name")) {
            this.foreignLlpName = fllpinData.get("Foreign LLP Name").toString();
        }
        if (fllpinData.has("Country of Incorporation")) {
            this.countryOfIncorporation = fllpinData.get("Country of Incorporation").toString();
        }
        if (fllpinData.has("Number of Authorised Representatives")) {
            this.noOfAuthorisedRepresentatives = fllpinData.get("Number of Authorised Representatives").toString();
        }
        if (fllpinData.has("Date of Incorporation")) { 
            this.dateOfIncorporation = fllpinData.get("Date of Incorporation").toString();
        }
        if (fllpinData.has("Registered Address")) {
            this.registeredAddress = fllpinData.get("Registered Address").toString();
        }
        if (fllpinData.has("Email Id")) {
            this.emailID = fllpinData.get("Email Id").toString();
        }
        if (fllpinData.has("Type of Office")) {
            this.typeOfOffice = fllpinData.get("Type of Office").toString();
        }
        if (fllpinData.has("Details")) {
            this.details = fllpinData.get("Details").toString();
        }
        if (fllpinData.has("Main division of business activity")) {
            this.mainDivisionOfBusiness = fllpinData.get("Main division of business activity").toString();
        }
        if (fllpinData.has("Description of main division")) {
            this.descriptionOfMainDivision = fllpinData.get("Description of main division").toString();
        }
        if (fllpinData.has("Date of last financial year")) {
            this.dateOfLastStatementOfAccountsAndSolvency = fllpinData.get("Date of last financial year").toString();
        }
        if (fllpinData.has("FLLP Status")) {
            this.fllpStatus = fllpinData.get("FLLP Status").toString();
        }
        if (fllpinData.has("statusAsPerSource")) {
            this.statusAsPerSource = fllpinData.get("statusAsPerSource").toString();
        }
        if (fllpinData.has("apiCount")) {
            this.APICount = (Integer)fllpinData.get("apiCount");
        }
        }
    }

    public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public FLLPIN(String document) {
        super(document);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFllpin() {
        return fllpin;
    }

    public void setFllpin(String fllpin) {
        this.fllpin = fllpin;
    }

    public String getForeignLlpName() {
        return foreignLlpName;
    }

    public void setForeignLlpName(String foreignLlpName) {
        this.foreignLlpName = foreignLlpName;
    }

    public String getCountryOfIncorporation() {
        return countryOfIncorporation;
    }

    public void setCountryOfIncorporation(String countryOfIncorporation) {
        this.countryOfIncorporation = countryOfIncorporation;
    }

    public String getNoOfAuthorisedRepresentatives() {
        return noOfAuthorisedRepresentatives;
    }

    public void setNoOfAuthorisedRepresentatives(String noOfAuthorisedRepresentatives) {
        this.noOfAuthorisedRepresentatives = noOfAuthorisedRepresentatives;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getTypeOfOffice() {
        return typeOfOffice;
    }

    public void setTypeOfOffice(String typeOfOffice) {
        this.typeOfOffice = typeOfOffice;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMainDivisionOfBusiness() {
        return mainDivisionOfBusiness;
    }

    public void setMainDivisionOfBusiness(String mainDivisionOfBusiness) {
        this.mainDivisionOfBusiness = mainDivisionOfBusiness;
    }

    public String getDescriptionOfMainDivision() {
        return descriptionOfMainDivision;
    }

    public void setDescriptionOfMainDivision(String descriptionOfMainDivision) {
        this.descriptionOfMainDivision = descriptionOfMainDivision;
    }

    public String getFllpStatus() {
        return fllpStatus;
    }

    public void setFllpStatus(String fllpStatus) {
        this.fllpStatus = fllpStatus;
    }

    public String getDateOfIncorporation() {
        return dateOfIncorporation;
    }

    public void setDateOfIncorporation(String dateOfIncorporation) {
        this.dateOfIncorporation = dateOfIncorporation;
    }

    public String getDateOfLastStatementOfAccountsAndSolvency() {
        return dateOfLastStatementOfAccountsAndSolvency;
    }

    public void setDateOfLastStatementOfAccountsAndSolvency(String dateOfLastStatementOfAccountsAndSolvency) {
        this.dateOfLastStatementOfAccountsAndSolvency = dateOfLastStatementOfAccountsAndSolvency;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
