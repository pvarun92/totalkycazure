package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:30 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "service_tax")
public class ServiceTax extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private Long id;

    private String serviceTaxNumber;
    private String assesseeBelongsTo;
    private String nameOfAssessee;
    private String addressOfAssessee;
    private String locationCode;
    private String commissionerateCode;
    private String divisionCode;
    private String rangeCode;
    private String status;
    private String statusAsOn;
    private String statusAsPerSource;
    private Date createdAt;
    private Date updatedAt;
    @NotNull
    private Long customerEntityId;
    private String apiValidation;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
    public ServiceTax(String document, Long customerEntityId, String apiValidation, JSONObject stData) throws JSONException {
        super(document);
        this.apiValidation = apiValidation;
        this.customerEntityId = customerEntityId;
        this.serviceTaxNumber = document;
        if(stData.has("apiCount")){
        	this.APICount = (Integer) stData.get("apiCount");
        }
        if(stData.has("service_tax")){
        stData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(stData, "service_tax");
        if (stData.has("Name of Assessee")) {
            this.nameOfAssessee = stData.get("Name of Assessee").toString();
        }
        if (stData.has("Assessee Belongs To")) {
            this.assesseeBelongsTo = stData.get("Assessee Belongs To").toString();
        }
        if (stData.has("Address of Assessee")) {
            this.addressOfAssessee = stData.get("Address of Assessee").toString();
        }
        if (stData.has("Location Code")) {
            this.locationCode = stData.get("Location Code").toString();
        }
        if (stData.has("Commissionerate Code")) {
            this.commissionerateCode = stData.get("Commissionerate Code").toString();
        }
        if (stData.has("Division Code")) {
            this.divisionCode = stData.get("Division Code").toString();
        }
        if (stData.has("Range Code")) {
            this.rangeCode = stData.get("Range Code").toString();
        }
        if (stData.has("Status")) {
            this.status = stData.get("Status").toString();
        }
        if (stData.has("status_as_on")) {
            this.statusAsOn = stData.get("status_as_on").toString();
        }
        if (stData.has("Status")) {
            this.statusAsPerSource = stData.get("Status").toString();
        }
        this.updatedAt = new Date();
        }
    }

    public ServiceTax(String document, Long customerEntityId, String apiValidation, String statusAsPerSource,Integer apiCount) throws JSONException {
       this(document,customerEntityId,apiValidation,apiCount);
        this.status = apiValidation;
        this.serviceTaxNumber = document;
        this.statusAsPerSource = statusAsPerSource;
        this.updatedAt = new Date();
    }

    public ServiceTax(String document) {
        super(document);
    }

    public ServiceTax() {

    }

    public ServiceTax(String document, Long customerEntityId, String apiValidation,Integer apiCount) {
       this(customerEntityId,document,apiCount);
        this.serviceTaxNumber = document;
        this.apiValidation = apiValidation;
    }

    public ServiceTax(Long customerEntityId, String document,Integer apiCount) {
        super(document);
        this.serviceTaxNumber = document;
        this.customerEntityId = customerEntityId;
        this.APICount = apiCount;
    }

    public ServiceTax(String apiValidation, Long customerEntityId) {
        this.apiValidation = apiValidation;
        this.customerEntityId = customerEntityId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getServiceTaxNumber() {
        return serviceTaxNumber;
    }

    public void setServiceTaxNumber(String serviceTaxNumber) {
        this.serviceTaxNumber = serviceTaxNumber;
    }

    public String getAssesseeBelongsTo() {
        return assesseeBelongsTo;
    }

    public void setAssesseeBelongsTo(String assesseeBelongsTo) {
        this.assesseeBelongsTo = assesseeBelongsTo;
    }

    public String getNameOfAssessee() {
        return nameOfAssessee;
    }

    public void setNameOfAssessee(String nameOfAssessee) {
        this.nameOfAssessee = nameOfAssessee;
    }

    public String getAddressOfAssessee() {
        return addressOfAssessee;
    }

    public void setAddressOfAssessee(String addressOfAssessee) {
        this.addressOfAssessee = addressOfAssessee;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCommissionerateCode() {
        return commissionerateCode;
    }

    public void setCommissionerateCode(String commissionerateCode) {
        this.commissionerateCode = commissionerateCode;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getRangeCode() {
        return rangeCode;
    }

    public void setRangeCode(String rangeCode) {
        this.rangeCode = rangeCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }

    public String getStatusAsOn() {
        return statusAsOn;
    }

    public void setStatusAsOn(String statusAsOn) {
        this.statusAsOn = statusAsOn;
    }

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}
    
}
