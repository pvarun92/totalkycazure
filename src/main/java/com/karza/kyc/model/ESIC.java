package com.karza.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.json.JSONException;
import org.json.JSONObject;

@Entity
@Table(name="esic")
public class ESIC extends BaseDocument {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long customerEntityId;
	@Column(name = "api_count", nullable = false, columnDefinition = "int default 0")
	private Integer APICount;
	private String statusAsPerSource;
	private String apiValidation;
	private String status;
    private String uhid;
    private String dob;
    private String aadharStatus;
    private String relationWithIP;
    private String currentDateOfAppointment;
    private String nomineeAddress;
    private String presentAddress;
    private String permanentAddress;
    private String disabilityType;
    private String aadharNumber;
    private String phoneNumber;
    private String maritalStatus;
    private String name;
    private String gender;
    private String nomineeAadharNo;
    private String fatherOrHusband;
    private String esicNumber;
    private String firstDateOfAppointment;
    private String regDate;
    private String nomineeName;
    private String insuranceNumber;
    private String disPensaryName;
	
    public ESIC() {
	}
    public ESIC(String esicID) {
    	super(esicID);
	}
    public ESIC(String esicNumber,Long customerEntityID){
    	super(esicNumber);
    	this.esicNumber = esicNumber;
    	this.customerEntityId =customerEntityID;
    	
    }
    public ESIC(String document, Long customerEntityID, String apiValidation, Integer apiCount){
    	super(document);
    	this.esicNumber = document;
    	this.customerEntityId = customerEntityID;
    	this.apiValidation = apiValidation;
    	this.APICount = apiCount;
    }
    	
	public String getFirstDateOfAppointment() {
		return firstDateOfAppointment;
	}
	public void setFirstDateOfAppointment(String firstDateOfAppointment) {
		this.firstDateOfAppointment = firstDateOfAppointment;
	}
	public String getEsicNumber() {
		return esicNumber;
	}
	public void setEsicNumber(String esicNumber) {
		this.esicNumber = esicNumber;
	}
	

	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	
	public String getDisPensaryName() {
		return disPensaryName;
	}
	public void setDisPensaryName(String disPensaryName) {
		this.disPensaryName = disPensaryName;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	
	public String getInsuranceNumber() {
		return insuranceNumber;
	}
	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}
	public ESIC(Long id, Long customerEntityId, Integer aPICount, String statusAsPerSource, String apiValidation,
			String status, String uhid, String dob, String aadharStatus, String relationWithIP,
			String currentDateOfAppointment, String nomineeAddress, String presentAddress, String disabilityType,
			String aadharNumber, String phoneNumber, String maritalStatus, String name, String gender,
			String nomineeAadharNo, String fatherOrHusband,String esicNumber,String firstDateOfApp,String regNumber,
			String perManentAddress,String nomineeName,String insuranceNumber,String disPensary) {
		super();
		this.id = id;
		this.customerEntityId = customerEntityId;
		APICount = aPICount;
		this.statusAsPerSource = statusAsPerSource;
		this.apiValidation = apiValidation;
		this.status = status;
		this.uhid = uhid;
		this.dob = dob;
		this.aadharStatus = aadharStatus;
		this.relationWithIP = relationWithIP;
		this.currentDateOfAppointment = currentDateOfAppointment;
		this.nomineeAddress = nomineeAddress;
		this.presentAddress = presentAddress;
		this.disabilityType = disabilityType;
		this.aadharNumber = aadharNumber;
		this.phoneNumber = phoneNumber;
		this.maritalStatus = maritalStatus;
		this.name = name;
		this.gender = gender;
		this.nomineeAadharNo = nomineeAadharNo;
		this.fatherOrHusband = fatherOrHusband;
		this.esicNumber = esicNumber;
		this.firstDateOfAppointment = firstDateOfApp;
		this.regDate = regNumber;
		this.permanentAddress = perManentAddress;
		this.nomineeName = nomineeName;
		this.insuranceNumber = insuranceNumber;
		this.disPensaryName = disPensary;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerEntityId() {
		return customerEntityId;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}

	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}

	public String getApiValidation() {
		return apiValidation;
	}

	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUhid() {
		return uhid;
	}

	public void setUhid(String uhid) {
		this.uhid = uhid;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAadharStatus() {
		return aadharStatus;
	}

	public void setAadharStatus(String aadharStatus) {
		this.aadharStatus = aadharStatus;
	}

	public String getRelationWithIP() {
		return relationWithIP;
	}

	public void setRelationWithIP(String relationWithIP) {
		this.relationWithIP = relationWithIP;
	}

	public String getCurrentDateOfAppointment() {
		return currentDateOfAppointment;
	}

	public void setCurrentDateOfAppointment(String currentDateOfAppointment) {
		this.currentDateOfAppointment = currentDateOfAppointment;
	}

	public String getNomineeAddress() {
		return nomineeAddress;
	}

	public void setNomineeAddress(String nomineeAddress) {
		this.nomineeAddress = nomineeAddress;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getDisabilityType() {
		return disabilityType;
	}

	public void setDisabilityType(String disabilityType) {
		this.disabilityType = disabilityType;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNomineeAadharNo() {
		return nomineeAadharNo;
	}

	public void setNomineeAadharNo(String nomineeAadharNo) {
		this.nomineeAadharNo = nomineeAadharNo;
	}

	public String getFatherOrHusband() {
		return fatherOrHusband;
	}

	public void setFatherOrHusband(String fatherOrHusband) {
		this.fatherOrHusband = fatherOrHusband;
	}
   
    public ESIC(String document, Long customerId,JSONObject esicResponse) throws JSONException{
    	
    	super(document);
    	this.customerEntityId = customerId;
    	
    	if(esicResponse.has("apiCount")){
    		this.APICount = (Integer) esicResponse.get("apiCount");
    	}
    	if(esicResponse.has("statusAsPerSource")){
    		this.statusAsPerSource = esicResponse.get("statusAsPerSource").toString();
    	}
    	if(esicResponse.has("status")){
    		this.status = esicResponse.get("status").toString();
    	}
    	if(esicResponse.has("AdhaarStatus")){
    		this.aadharStatus = esicResponse.get("AdhaarStatus").toString();
    	}
    	if(esicResponse.has("FirstDateOfAppointment")){
    		this.firstDateOfAppointment = esicResponse.get("FirstDateOfAppointment").toString();
    	}
    	if(esicResponse.has("RegistrationDate")){
    		this.regDate = esicResponse.get("RegistrationDate").toString();
    	}
    	if(esicResponse.has("PermanentAddress")){
    		this.permanentAddress = esicResponse.get("PermanentAddress").toString();
    	}
    	if(esicResponse.has("UHID")){
    		this.uhid = esicResponse.get("UHID").toString();
    	}
    	if(esicResponse.has("Nominee_Name")){
    		this.nomineeName = esicResponse.get("Nominee_Name").toString();
    	}
    	if(esicResponse.has("Gender")){
    		this.gender = esicResponse.get("Gender").toString();
    	}
    	if(esicResponse.has("InsuranceNO")){
    		this.insuranceNumber = esicResponse.get("InsuranceNO").toString();
    	}
    	if(esicResponse.has("Name")){
    		this.name = esicResponse.get("Name").toString();
    	}
    	if(esicResponse.has("CurrentDateOfAppointment")){
    		this.currentDateOfAppointment = esicResponse.get("CurrentDateOfAppointment").toString();
    	}
    	if(esicResponse.has("Nominee_Address")){
    		this.nomineeAddress= esicResponse.get("Nominee_Address").toString();
    	}
    	if(esicResponse.has("DOB")){
    		this.dob = esicResponse.get("DOB").toString();
    	}
    	if(esicResponse.has("Father_or_Husband")){
    		this.fatherOrHusband = esicResponse.get("Father_or_Husband").toString();
    	}
    	if(esicResponse.has("apiValidation")){
    		this.apiValidation = esicResponse.get("apiValidation").toString();
    	}
    	if(esicResponse.has("MaritialStatus")){
    		this.maritalStatus = esicResponse.get("MaritialStatus").toString();
    	}
    	if(esicResponse.has("PresentAddress")){
    		this.presentAddress = esicResponse.get("PresentAddress").toString();
    	}
    	if(esicResponse.has("DisabilityType")){
    		this.disabilityType = esicResponse.get("DisabilityType").toString();
    	}
    	if(esicResponse.has("PhoneNO")){
    		this.phoneNumber = esicResponse.get("PhoneNO").toString();
    	}
    	if(esicResponse.has("DispensaryName")){
    		this.disPensaryName = esicResponse.get("DispensaryName").toString();
    	}
    	if(esicResponse.has("AdhaarNO")){
    		this.aadharNumber = esicResponse.get("AdhaarNO").toString();
    	}
    	if(esicResponse.has("Nominee_AdhaarNO")){
    		this.nomineeAadharNo = esicResponse.get("Nominee_AdhaarNO").toString();
    	}
    	if(esicResponse.has("Relation_with_IP")){
    		this.relationWithIP = esicResponse.get("Relation_with_IP").toString();
    	}
    }

}
