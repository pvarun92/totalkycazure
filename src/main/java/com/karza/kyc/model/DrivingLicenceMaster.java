package com.karza.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA. User: Vinay Date: 4/6/16 Time: 10:49 AM To change
 * this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "driving_licence_master")
public class DrivingLicenceMaster extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1519225410679192166L;

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private Long customerEntityId;

	private String apiValidation;

	private String dateTimeRequested;
	private String queryStatus;
	private String lastPinged;
	private String dateTimeReceived;
	private String drivingLicenseNumber;
	private String statusAsPerSource;
	private String name;
	private String dateOfIssue;
	private String rto;
	private String hazardousValidTill;
	private String hillValidTill;
	private String transportValidTo;
	private String transportValidFrom;
	private String nonTransportValidTo;
	private String nonTransportValidFrom;
	private String fatherName;
	private Date createdAt;
	private Date updatedAt;
	@Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
	private String address;
	private String dob;
	private String cov;
	private String covabbrv;
	private String vehCatg;
	private String dcIssueDt;

	public DrivingLicenceMaster(String document, Long customerEntityId, String apiValidation, JSONObject dlData)
			throws JSONException {
		super(document);
		this.customerEntityId = customerEntityId;
		this.apiValidation = apiValidation;
		this.drivingLicenseNumber = document;
		
		JSONObject dlObj = new JSONObject();
		if (dlData.has("dlobj")) {
			dlObj = (JSONObject) dlData.get("dlobj");
		}
		if(dlData.has("apiCount")){
			this.APICount = (Integer) dlData.get("apiCount");
		}
		if (dlObj.has("dlStatus")) {
			if (!StringUtils.containsIgnoreCase(dlObj.get("dlStatus").toString(), "No Details are available.")) {
				this.statusAsPerSource = "valid";
			} else {
				this.statusAsPerSource = "No Record found with the given DL number";
			}
		}
		if (this.statusAsPerSource == null) {
			if (dlData.has("errormsg")) {
				this.statusAsPerSource = dlData.get("errormsg").toString();
			}
		}
		if (dlObj.has("dlName")) {
			this.name = dlObj.get("dlName").toString();
		}
		if (dlObj.has("dob")) {
			this.dob = dlObj.get("dob").toString();
		}
		if (dlObj.has("dlIssueauth")) {
			this.rto = dlObj.get("dlIssueauth").toString();
		}

		if (dlObj.has("dlIssuedt")) {
			this.dateOfIssue = dlObj.get("dlIssuedt").toString();
		}
		if (dlObj.has("dlNtValdfrDt")) {
			this.nonTransportValidFrom = dlObj.get("dlNtValdfrDt").toString();
		}
		if (dlObj.has("dlNtValdtoDt")) {
			this.nonTransportValidTo = dlObj.get("dlNtValdtoDt").toString();
		}
		if (dlObj.has("dlValidFrTr")) {
			this.transportValidFrom = dlObj.get("dlValidFrTr").toString();
		}
		if (dlObj.has("dlValidToTr")) {
			this.transportValidTo = dlObj.get("dlValidToTr").toString();
		}
		if (dlObj.has("dlHazvalidtill")) {
			this.hazardousValidTill = dlObj.get("dlHazvalidtill").toString();
		}
		if (dlObj.has("dlHillvalidtill")) {
			this.hillValidTill = dlObj.get("dlHillvalidtill").toString();
		}
		if (dlObj.has("father")) {
			this.fatherName = dlObj.get("father").toString();
		}
		if (dlObj.has("address")) {
			this.address = dlObj.get("address").toString();
		}
		if(dlObj.has("apiCount")){
			this.APICount = (Integer) dlObj.get("apiCount");
		}

		this.updatedAt = new Date();
	}

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public DrivingLicenceMaster(Long customerEntityId, String apiValidation) {
		this.customerEntityId = customerEntityId;
		this.apiValidation = apiValidation;
	}

	public DrivingLicenceMaster(String document, Long customerEntityId, String apiValidation,Integer apiCount) {
		super(document);
		this.drivingLicenseNumber = document;
		this.customerEntityId = customerEntityId;
		this.apiValidation = apiValidation;
		this.APICount = apiCount;
	}

	public DrivingLicenceMaster() {
	}

	public DrivingLicenceMaster(String document) {
		super(document);
	}

	public DrivingLicenceMaster(String document, Long customerEntityId) {
		super(document);
		this.drivingLicenseNumber = document;
		this.customerEntityId = customerEntityId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerEntityId() {
		return customerEntityId;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public String getApiValidation() {
		return apiValidation;
	}

	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}

	public String getDateTimeRequested() {
		return dateTimeRequested;
	}

	public void setDateTimeRequested(String dateTimeRequested) {
		this.dateTimeRequested = dateTimeRequested;
	}

	public String getQueryStatus() {
		return queryStatus;
	}

	public void setQueryStatus(String queryStatus) {
		this.queryStatus = queryStatus;
	}

	public String getLastPinged() {
		return lastPinged;
	}

	public void setLastPinged(String lastPinged) {
		this.lastPinged = lastPinged;
	}

	public String getDateTimeReceived() {
		return dateTimeReceived;
	}

	public void setDateTimeReceived(String dateTimeReceived) {
		this.dateTimeReceived = dateTimeReceived;
	}

	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}

	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}

	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}

	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getRto() {
		return rto;
	}

	public void setRto(String rto) {
		this.rto = rto;
	}

	public String getHazardousValidTill() {
		return hazardousValidTill;
	}

	public void setHazardousValidTill(String hazardousValidTill) {
		this.hazardousValidTill = hazardousValidTill;
	}

	public String getHillValidTill() {
		return hillValidTill;
	}

	public void setHillValidTill(String hillValidTill) {
		this.hillValidTill = hillValidTill;
	}

	public String getTransportValidTo() {
		return transportValidTo;
	}

	public void setTransportValidTo(String transportValidTo) {
		this.transportValidTo = transportValidTo;
	}

	public String getTransportValidFrom() {
		return transportValidFrom;
	}

	public void setTransportValidFrom(String transportValidFrom) {
		this.transportValidFrom = transportValidFrom;
	}

	public String getNonTransportValidTo() {
		return nonTransportValidTo;
	}

	public void setNonTransportValidTo(String nonTransportValidTo) {
		this.nonTransportValidTo = nonTransportValidTo;
	}

	public String getNonTransportValidFrom() {
		return nonTransportValidFrom;
	}

	public void setNonTransportValidFrom(String nonTransportValidFrom) {
		this.nonTransportValidFrom = nonTransportValidFrom;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getCov() {
		return cov;
	}

	public void setCov(String cov) {
		this.cov = cov;
	}

	public String getCovabbrv() {
		return covabbrv;
	}

	public void setCovabbrv(String covabbrv) {
		this.covabbrv = covabbrv;
	}

	public String getVehCatg() {
		return vehCatg;
	}

	public void setVehCatg(String vehCatg) {
		this.vehCatg = vehCatg;
	}

	public String getDcIssueDt() {
		return dcIssueDt;
	}

	public void setDcIssueDt(String dcIssueDt) {
		this.dcIssueDt = dcIssueDt;
	}
}
