package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.engine.transaction.jta.platform.internal.JtaSynchronizationStrategy;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

/*THis class corresponds to the ESIC class, it holds the data related to one person,
 * that how much and how many times the person has contributed, this class holds the 
 * esicReqId field as the reference key from the ESIC class primary key,so to verify
 * for which ESIC ID fields how many data are their.
 * */

@Entity
@Table(name="esic_contri_detail")
public class ESICContriDetails {
	
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long esicReqId;
	private String emp_contribution;
	private String wage_period;
	private String total_monthly_wages;
	private String no_of_days_wage_paid;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEsicReqId() {
		return esicReqId;
	}

	public void setEsicReqId(Long esicReqId) {
		this.esicReqId = esicReqId;
	}

	public String getEmp_contribution() {
		return emp_contribution;
	}

	public void setEmp_contribution(String emp_contribution) {
		this.emp_contribution = emp_contribution;
	}

	public String getWage_period() {
		return wage_period;
	}

	public void setWage_period(String wage_period) {
		this.wage_period = wage_period;
	}

	public String getTotal_monthly_wages() {
		return total_monthly_wages;
	}

	public void setTotal_monthly_wages(String total_monthly_wages) {
		this.total_monthly_wages = total_monthly_wages;
	}

	public String getNo_of_days_wage_paid() {
		return no_of_days_wage_paid;
	}

	public void setNo_of_days_wage_paid(String no_of_days_wage_paid) {
		this.no_of_days_wage_paid = no_of_days_wage_paid;
	}

	public ESICContriDetails(Long id, Long esicReqId, String emp_contribution, String wage_period,
			String total_monthly_wages, String no_of_days_wage_paid) {
		super();
		this.id = id;
		this.esicReqId = esicReqId;
		this.emp_contribution = emp_contribution;
		this.wage_period = wage_period;
		this.total_monthly_wages = total_monthly_wages;
		this.no_of_days_wage_paid = no_of_days_wage_paid;
	}

	public ESICContriDetails() {
		super();
	}
	public ESICContriDetails(Long esicReqId,JSONObject jsonObject) throws JSONException{
		jsonObject=(JSONObject)KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "contribution");
		this.esicReqId = esicReqId;
		if(jsonObject.has("Employee_Conrtibution")){
			this.emp_contribution = jsonObject.get("Employee_Conrtibution").toString();
		}
		if(jsonObject.has("Wage_Period")){
			this.wage_period = jsonObject.get("Wage_Period").toString();
		}
		if(jsonObject.has("Number_of_Days_wages_paid")){
			this.no_of_days_wage_paid = jsonObject.get("Number_of_Days_wages_paid").toString();
		}
		if(jsonObject.has("total_monthly_wages")){
			this.total_monthly_wages = jsonObject.get("total_monthly_wages").toString();
		}
	}
}
