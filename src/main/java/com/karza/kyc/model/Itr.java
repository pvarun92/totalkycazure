package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Admin on 7/16/2016.
 */
@Entity
@Table(name = "itr")
public class Itr extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private Long id;

    private String status;

    private String acknowledgmentNumber;

    private String assessmentYear;

    private Long panId;

    private Date dateOfFilling;

    private String statusAsPerSourcePan;

    private String statusAsPerSourceAck;

    @NotNull
    private Long customerEntityId;

    private String apiValidation;

    private Date createdAt;

    private Date updatedAt;

    public Itr() {

    }

    public Itr(String acknowledgmentNumber, String assessmentYear, Long panId, Date dateOfFilling, Long customerEntityId, String apiValidation) {
    	this(acknowledgmentNumber,assessmentYear,panId,dateOfFilling,customerEntityId);
        this.apiValidation = apiValidation;
    }

    public Itr(String acknowledgmentNumber, String assessmentYear, Long panId, Date dateOfFilling, Long customerEntityId) {
    	this(acknowledgmentNumber,assessmentYear,panId,dateOfFilling);
        this.customerEntityId = customerEntityId;
    }

    public Itr(String acknowledgmentNumber, String assessmentYear, Long panId, Date dateOfFilling) {
        super(acknowledgmentNumber);
        this.acknowledgmentNumber = acknowledgmentNumber;
        this.assessmentYear = assessmentYear;
        this.panId = panId;
        this.dateOfFilling = dateOfFilling;
    }

    public Itr(String document, Long panId, String assessmentYear, Date dateOfFilling, Long customerEntityId, JSONObject itrData) throws JSONException {
        super(document);
        this.acknowledgmentNumber = document;
        this.assessmentYear = assessmentYear;
        this.panId = panId;
        this.dateOfFilling = dateOfFilling;
        this.customerEntityId = customerEntityId;

        if (itrData.has("status")) {
            this.status = itrData.get("status").toString();
        }
        if (itrData.has("status")) {
            this.apiValidation = itrData.get("status").toString();
        }
        if (itrData.has("statusAsPerSourcePan")) {
            this.statusAsPerSourcePan = itrData.get("statusAsPerSourcePan").toString();
        }
        if (itrData.has("statusAsPerSourceAcknowledment")) {
            this.statusAsPerSourceAck = itrData.get("statusAsPerSourceAcknowledment").toString();
        }
        this.updatedAt = new Date();
    }

    public Itr(String acknowledgmentNumber, String assessmentYear, Date dateOfFilling) {
        super(acknowledgmentNumber);
        this.acknowledgmentNumber = acknowledgmentNumber;
        this.assessmentYear = assessmentYear;
        this.dateOfFilling = dateOfFilling;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcknowledgmentNumber() {
        return acknowledgmentNumber;
    }

    public void setAcknowledgmentNumber(String acknowledgmentNumber) {
        this.acknowledgmentNumber = acknowledgmentNumber;
    }

    public String getAssessmentYear() {
        return assessmentYear;
    }

    public void setAssessmentYear(String assessmentYear) {
        this.assessmentYear = assessmentYear;
    }

    public Date getDateOfFilling() {
        return dateOfFilling;
    }

    public void setDateOfFilling(Date dateOfFilling) {
        this.dateOfFilling = dateOfFilling;
    }

    public String getStatusAsPerSourcePan() {
        return statusAsPerSourcePan;
    }

    public void setStatusAsPerSourcePan(String statusAsPerSourcePan) {
        this.statusAsPerSourcePan = statusAsPerSourcePan;
    }

    public String getStatusAsPerSourceAck() {
        return statusAsPerSourceAck;
    }

    public void setStatusAsPerSourceAck(String statusAsPerSourceAck) {
        this.statusAsPerSourceAck = statusAsPerSourceAck;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getPanId() {
        return panId;
    }

    public void setPanId(Long panId) {
        this.panId = panId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
