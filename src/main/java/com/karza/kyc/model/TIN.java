package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/14/16
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tin")
public class TIN extends BaseDocument {

    private static final long serialVersionUID = -4060739788760795254L;

    @Id
    @GeneratedValue
    private Long id;


    public TIN(String document) {
        super(document);
    }

    public TIN() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
