package com.karza.kyc.model;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class SuperAdmin {

	/*This class is written in order to cast to the specific object for the Karza Admin MIS Report
	 * the initial string name indicates from which table this attribute is taken from,
	 * here 4 tables are being merged,request,customer_master,user_master,customer_entity
	 * */
	Integer id;
	Integer request_id;
	String customer_entity_first_name;
	String customer_entity_middle_name;
	String customer_entity_last_name;
	String request_parent_request_id;
	String request_customer_master_id;
	String request_status;
	String request_user_master_id;
	String customer_master_name;
	String user_master_id;
	Long   customer_entity_branch_id;
	Date   customer_entity_created_date;
	String customer_entity_name;
	String customer_entity_status;
	String customer_entity_type;
	String customer_entity_itr_validity;
	String customer_entity_no_of_documents;
	String customer_entity_no_of_parties;
	String customer_entity_request_id;
	String customer_entity_validity;
	
	public SuperAdmin(Integer request_id, String customer_entity_first_name,
			String customer_entity_middle_name, String customer_entity_last_name, String request_parent_request_id,
			String request_customer_master_id, String request_status, String request_user_master_id,
			String customer_master_name, String user_master_id, Long customer_entity_branch_id,
			Date customer_entity_created_date, String customer_entity_name, String customer_entity_status,
			String customer_entity_type, String customer_entity_itr_validity, String customer_entity_no_of_documents,
			String customer_entity_no_of_parties, String customer_entity_request_id, String customer_entity_validity) {
		super();
		
		this.request_id = request_id;
		this.customer_entity_first_name = customer_entity_first_name;
		this.customer_entity_middle_name = customer_entity_middle_name;
		this.customer_entity_last_name = customer_entity_last_name;
		this.request_parent_request_id = request_parent_request_id;
		this.request_customer_master_id = request_customer_master_id;
		this.request_status = request_status;
		this.request_user_master_id = request_user_master_id;
		this.customer_master_name = customer_master_name;
		this.user_master_id = user_master_id;
		this.customer_entity_branch_id = customer_entity_branch_id;
		this.customer_entity_created_date = customer_entity_created_date;
		this.customer_entity_name = customer_entity_name;
		this.customer_entity_status = customer_entity_status;
		this.customer_entity_type = customer_entity_type;
		this.customer_entity_itr_validity = customer_entity_itr_validity;
		this.customer_entity_no_of_documents = customer_entity_no_of_documents;
		this.customer_entity_no_of_parties = customer_entity_no_of_parties;
		this.customer_entity_request_id = customer_entity_request_id;
		this.customer_entity_validity = customer_entity_validity;
	}
	public String getCustomer_entity_first_name() {
		return customer_entity_first_name;
	}
	public void setCustomer_entity_first_name(String customer_entity_first_name) {
		this.customer_entity_first_name = customer_entity_first_name;
	}
	public String getCustomer_entity_middle_name() {
		return customer_entity_middle_name;
	}
	public void setCustomer_entity_middle_name(String customer_entity_middle_name) {
		this.customer_entity_middle_name = customer_entity_middle_name;
	}
	public String getCustomer_entity_last_name() {
		return customer_entity_last_name;
	}
	public void setCustomer_entity_last_name(String customer_entity_last_name) {
		this.customer_entity_last_name = customer_entity_last_name;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRequest_id() {
		return request_id;
	}
	public void setRequest_id(Integer request_id) {
		this.request_id = request_id;
	}
	public String getRequest_parent_request_id() {
		return request_parent_request_id;
	}
	public void setRequest_parent_request_id(String request_parent_request_id) {
		this.request_parent_request_id = request_parent_request_id;
	}
	public String getRequest_customer_master_id() {
		return request_customer_master_id;
	}
	public void setRequest_customer_master_id(String request_customer_master_id) {
		this.request_customer_master_id = request_customer_master_id;
	}
	public String getRequest_status() {
		return request_status;
	}
	public void setRequest_status(String request_status) {
		this.request_status = request_status;
	}
	public String getRequest_user_master_id() {
		return request_user_master_id;
	}
	public void setRequest_user_master_id(String request_user_master_id) {
		this.request_user_master_id = request_user_master_id;
	}
	public String getCustomer_master_name() {
		return customer_master_name;
	}
	public void setCustomer_master_name(String customer_master_name) {
		this.customer_master_name = customer_master_name;
	}
	public String getUser_master_id() {
		return user_master_id;
	}
	public void setUser_master_id(String user_master_id) {
		this.user_master_id = user_master_id;
	}
	public Long getCustomer_entity_branch_id() {
		return customer_entity_branch_id;
	}
	public void setCustomer_entity_branch_id(Long customer_entity_branch_id) {
		this.customer_entity_branch_id = customer_entity_branch_id;
	}
	public Date getCustomer_entity_created_date() {
		return customer_entity_created_date;
	}
	public void setCustomer_entity_created_date(Date customer_entity_created_date) {
		this.customer_entity_created_date = customer_entity_created_date;
	}
	public String getCustomer_entity_name() {
		return customer_entity_name;
	}
	public void setCustomer_entity_name(String customer_entity_name) {
		this.customer_entity_name = customer_entity_name;
	}
	public String getCustomer_entity_status() {
		return customer_entity_status;
	}
	public void setCustomer_entity_status(String customer_entity_status) {
		this.customer_entity_status = customer_entity_status;
	}
	public String getCustomer_entity_type() {
		return customer_entity_type;
	}
	public void setCustomer_entity_type(String customer_entity_type) {
		this.customer_entity_type = customer_entity_type;
	}
	public String getCustomer_entity_itr_validity() {
		return customer_entity_itr_validity;
	}
	public void setCustomer_entity_itr_validity(String customer_entity_itr_validity) {
		this.customer_entity_itr_validity = customer_entity_itr_validity;
	}
	public String getCustomer_entity_no_of_documents() {
		return customer_entity_no_of_documents;
	}
	public void setCustomer_entity_no_of_documents(String customer_entity_no_of_documents) {
		this.customer_entity_no_of_documents = customer_entity_no_of_documents;
	}
	public String getCustomer_entity_no_of_parties() {
		return customer_entity_no_of_parties;
	}
	public void setCustomer_entity_no_of_parties(String customer_entity_no_of_parties) {
		this.customer_entity_no_of_parties = customer_entity_no_of_parties;
	}
	public String getCustomer_entity_request_id() {
		return customer_entity_request_id;
	}
	public void setCustomer_entity_request_id(String customer_entity_request_id) {
		this.customer_entity_request_id = customer_entity_request_id;
	}
	public String getCustomer_entity_validity() {
		return customer_entity_validity;
	}
	public void setCustomer_entity_validity(String customer_entity_validity) {
		this.customer_entity_validity = customer_entity_validity;
	}
	public SuperAdmin() {
	}
	public SuperAdmin(Integer request_id, String request_parent_request_id, String request_customer_master_id,
			String request_status, String request_user_master_id, String customer_master_name, String user_master_id,
			Long customer_entity_branch_id, Date customer_entity_created_date, String customer_entity_name,
			String customer_entity_status, String customer_entity_type, String customer_entity_itr_validity,
			String customer_entity_no_of_documents, String customer_entity_no_of_parties,
			String customer_entity_request_id, String customer_entity_validity) {
		super();
		this.request_id = request_id;
		this.request_parent_request_id = request_parent_request_id;
		this.request_customer_master_id = request_customer_master_id;
		this.request_status = request_status;
		this.request_user_master_id = request_user_master_id;
		this.customer_master_name = customer_master_name;
		this.user_master_id = user_master_id;
		this.customer_entity_branch_id = customer_entity_branch_id;
		this.customer_entity_created_date = customer_entity_created_date;
		this.customer_entity_name = customer_entity_name;
		this.customer_entity_status = customer_entity_status;
		this.customer_entity_type = customer_entity_type;
		this.customer_entity_itr_validity = customer_entity_itr_validity;
		this.customer_entity_no_of_documents = customer_entity_no_of_documents;
		this.customer_entity_no_of_parties = customer_entity_no_of_parties;
		this.customer_entity_request_id = customer_entity_request_id;
		this.customer_entity_validity = customer_entity_validity;
	}
	
}
