package com.karza.kyc.model;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Admin on 07-03-2016.
 */
@Entity
@Table(name = "pancard")
public class PanCard extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private Long id;

    private String panNumber;

    private String status;

    private String statusAsPerSource;

    private String title;

    private String firstName;

    private String middleName;

    private String lastName;

    private Date requestedTimestamp;

    private Date recievedTimestamp;

    @NotNull
    private Long customerEntityId;

    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
    
    private String apiValidation;

    private String entityType;

    private Date createdAt;

    private Date updatedAt;

    public PanCard() {

    }

    public PanCard(String document, String entityType, Long customerEntityId, String apiValidation,Integer apiCount) {
        super(document);
        this.entityType = entityType;
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.APICount = apiCount;
    }

    public PanCard(String document, String entityType, Long customerEntityId, String apiValidation, String statusAsPerSource,Integer apiCount) {
        super(document);
        this.entityType = entityType;
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.status = apiValidation;
        this.statusAsPerSource = statusAsPerSource;
        this.panNumber = document;
        this.APICount = apiCount;
    }

    public PanCard(String document, String entityType, Long customerEntityId) {
        super(document);
        this.entityType = entityType;
        this.customerEntityId = customerEntityId;
    }

    public PanCard(String document, String entityType) {
        super(document);
        this.entityType = entityType;
    }

    public PanCard(String document) {
        super(document);
    }

    public PanCard(String document, String entityType, Long customerEntityId, JSONObject panData) throws JSONException {
        super(document);
        this.entityType = entityType;
        this.customerEntityId = customerEntityId;
        this.panNumber = document;
       
        String Title = "";
        if (!panData.get("gender").toString().isEmpty()) {
            String gender = panData.get("gender").toString();
           /* if (gender == "female") {
                Title = "Ms.";
            } else {
                Title = "Mr.";
            }*/
            if(gender.equals("female")){
            	Title = "Ms.";
            }
            else{
            	Title = "Mr.";
            }
        }
        if(panData.has("apiCount")){
        	this.APICount = (Integer) panData.get("apiCount");
        }
        if(panData.has("pan")){
        panData = (JSONObject)KYCUtil.checkKeyAndGetValueFromJSON(panData,"pan");
		/*if(panData.has("status")){
			if(!StringUtils.containsIgnoreCase(panData.getString("status").toString(),"invalid")){
				this.statusAsPerSource = "valid";
			}
			else{
				this.statusAsPerSource = "invalid";
			}
		}*/
		
        if (panData.has("firstName")) {
            this.firstName = panData.get("firstName").toString();
        }
        if (panData.has("lastName")) {
            this.lastName = panData.get("lastName").toString();
        }
        if (panData.has("middleName")) {
            this.middleName = panData.get("middleName").toString();
        }
        if (panData.has("status")) {
            this.status = panData.get("status").toString();
        }
        if (panData.has("apiResponse")) {
            this.apiValidation = panData.get("apiResponse").toString();
        }
        if (panData.has("statusAsPerSource")) {
            this.statusAsPerSource = panData.get("statusAsPerSource").toString();
        }
        if(panData.has("apiCount")){
        	this.APICount = (Integer) panData.get("apiCount");
        }
        this.title = Title;
        this.updatedAt = new Date();
    }

   }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getRequestedTimestamp() {
        return requestedTimestamp;
    }

    public void setRequestedTimestamp(Date requestedTimestamp) {
        this.requestedTimestamp = requestedTimestamp;
    }

    public Date getRecievedTimestamp() {
        return recievedTimestamp;
    }

    public void setRecievedTimestamp(Date recievedTimestamp) {
        this.recievedTimestamp = recievedTimestamp;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}
    
}
