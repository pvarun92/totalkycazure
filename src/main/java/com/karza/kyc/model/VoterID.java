package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:53 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "voter_id")
public class VoterID extends BaseDocument {

    private static final long serialVersionUID = -4060739788760795254L;
    @Id
    @GeneratedValue
    private Long id;
    private Date dateTimeRequested;
    @NotNull
    private Long customerEntityId;
    private String apiValidation;
    private Date dateTimeReceived;
    private String voterIDNumber;
    private String State;
    private String District;
    private String assemblyConstituency;
    private Date createdAt;
    private Date updatedAt;
    private String parliamentryConstituency;
    private String assemblyConstituencyNo;
    private String partNo;
    private String partName;
    private String sectionNo;
    private String statusAsPerSource;
    private String firstName;
    private String middleName;
    private String lastName;
    private String voterName;
    private String age;
    private String gender;
    private String fatherOrHusbandOrMotherOrName;
    private String relation;
    private String pollingStationAddress;
    private Date dateOfBirth;
    private String houseNumber;
    private String Address;
    private String lastUpdatedAt;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
    public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public VoterID(String document, Long customerEntityId, JSONObject viData) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.voterIDNumber = document;
        if (viData.has("status")) {
            this.apiValidation = viData.get("status").toString();
        }
        if (viData.has("statusAsPerSource")) {
            this.statusAsPerSource = viData.get("statusAsPerSource").toString();
        }
        if (viData.has("dateTimeRequested")) {
            this.dateTimeRequested = (Date) viData.get("dateTimeRequested");
        }
        if (viData.has("dateTimeReceived")) {
            this.dateTimeReceived = (Date) viData.get("dateTimeReceived");
        }
        if(viData.has("apiCount")){
        	this.APICount = (Integer) viData.get("apiCount");
        }
		if (viData.has("voter")) {
			viData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(viData, "voter");
			if (viData.has("Age")) {
				this.age = viData.get("Age").toString();
			} else {
				if (viData.has("AGE")) {
					this.age = viData.get("AGE").toString();
				}
			}
			if (viData.has("Assembly Constituency")) {
				this.assemblyConstituency = viData.get("Assembly Constituency").toString();
			}
			if (viData.has("District")) {
				this.District = viData.get("District").toString();
			}
			if (viData.has("EPIC No")) {
				this.voterIDNumber = viData.get("EPIC No").toString();
			}
			if (viData.has("Father's/Husband's Name")) {
				this.fatherOrHusbandOrMotherOrName = viData.get("Father's/Husband's Name").toString();
			}
			if (viData.has("gender")) {
				this.gender = viData.get("gender").toString();
			} else {
				if (viData.has("GENDER")) {
					this.gender = viData.get("GENDER").toString();
				}
			}
			if (viData.has("Name")) {
				this.voterName = viData.get("Name").toString();
			} else {
				if (viData.has("Voter Name")) {
					this.voterName = viData.get("Voter Name").toString();
				}
			}

			if (viData.has("Polling Station")) {
				this.pollingStationAddress = viData.get("Polling Station").toString();
			}
			if (viData.has("State")) {
				this.State = viData.get("State").toString();
			}
			if (viData.has("address")) {
				this.Address = viData.get("address").toString();
			}
			if (viData.has("AC NO")) {
				this.assemblyConstituencyNo = viData.get("AC NO").toString();
			}
			if (viData.has("PART NO")) {
				this.partNo = viData.getString("PART NO").toString();
			}
			if (viData.has("SR NO")) {
				this.sectionNo = viData.get("SR NO").toString();
			}
			if (viData.has("createdAt")) {
				this.createdAt = (Date) viData.get("createdAt");
			}
			if (viData.has("updatedAt")) {
				this.updatedAt = (Date) viData.get("updatedAt");
			}
			if (viData.has("lastUpdatedAt")) {
				this.lastUpdatedAt = viData.get("lastUpdatedAt").toString();
			}
			if (viData.has("parliamentryConstituency")) {
				this.parliamentryConstituency = viData.get("parliamentryConstituency").toString();
			}
			if (viData.has("dob")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss a", Locale.ENGLISH);
				try {
					Date date = dateFormat.parse(viData.get("dob").toString());
					this.dateOfBirth = date;
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}
			if (viData.has("partName")) {
				this.partName = viData.get("partName").toString();
			}
			if (viData.has("house_no")) {
				this.houseNumber = viData.get("house_no").toString();
			}
			if (viData.has("apiCount")) {
				this.APICount = (Integer) viData.get("apiCount");
			}
		}
        	
    }

    public String getParliamentryConstituency() {
		return parliamentryConstituency;
	}

	public void setParliamentryConstituency(String parliamentryConstituency) {
		this.parliamentryConstituency = parliamentryConstituency;
	}

	public VoterID(String document, Long customerEntityId, String District, String apiValidation,Integer apiCount) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.District = District;
        this.APICount = apiCount;
    }

    public VoterID(String document, Long customerEntityId, String apiValidation,Integer apiCount) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.APICount = apiCount;
    }

    public VoterID(String document, Long customerEntityId, String District, String state, String assembly_consituency, String apiValidation) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.District = District;
        this.State = state;
        this.assemblyConstituency = assembly_consituency;
    }

    public VoterID(String document, Long customerEntityId, String District, String state, String assembly_consituency, String apiValidation, String statusAsPerSource) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.District = District;
        this.State = state;
        this.assemblyConstituency = assembly_consituency;
        this.statusAsPerSource = statusAsPerSource;
        this.updatedAt = new Date();
    }

    public VoterID(String document, Long customerEntityId, String District, String state, String assembly_consituency) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.District = District;
        this.State = state;
        this.assemblyConstituency = assembly_consituency;
    }

    public VoterID(String document,Long customerEntityId){
    	super(document);
    	this.customerEntityId = customerEntityId;
    }
    
    public VoterID(Date dateTimeRequested) {
        this.dateTimeRequested = dateTimeRequested;
    }

    public VoterID(String document) {
        super(document);
    }

    public VoterID() {
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Date getDateTimeRequested() {
        return dateTimeRequested;
    }

    public void setDateTimeRequested(Date dateTimeRequested) {
        this.dateTimeRequested = dateTimeRequested;
    }

    public Date getDateTimeReceived() {
        return dateTimeReceived;
    }

    public void setDateTimeReceived(Date dateTimeReceived) {
        this.dateTimeReceived = dateTimeReceived;
    }

    public String getVoterIDNumber() {
        return voterIDNumber;
    }

    public void setVoterIDNumber(String voterIDNumber) {
        this.voterIDNumber = voterIDNumber;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getAssemblyConstituency() {
        return assemblyConstituency;
    }

    public void setAssemblyConstituency(String assemblyConstituency) {
        this.assemblyConstituency = assemblyConstituency;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAssemblyConstituencyNo() {
        return assemblyConstituencyNo;
    }

    public void setAssemblyConstituencyNo(String assemblyConstituencyNo) {
        this.assemblyConstituencyNo = assemblyConstituencyNo;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getSectionNo() {
        return sectionNo;
    }

    public void setSectionNo(String sectionNo) {
        this.sectionNo = sectionNo;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVoterName() {
        return voterName;
    }

    public void setVoterName(String voterName) {
        this.voterName = voterName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFatherOrHusbandOrMotherOrName() {
        return fatherOrHusbandOrMotherOrName;
    }

    public void setFatherOrHusbandOrMotherOrName(String fatherOrHusbandOrMotherOrName) {
        this.fatherOrHusbandOrMotherOrName = fatherOrHusbandOrMotherOrName;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getPollingStationAddress() {
        return pollingStationAddress;
    }

    public void setPollingStationAddress(String pollingStationAddress) {
        this.pollingStationAddress = pollingStationAddress;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(String lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

}
