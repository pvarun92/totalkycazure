package com.karza.kyc.model;

/**
 * Created by Fallon on 4/18/2016.
 */

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "vat_frequency")
public class VATFrequency {
    @Id
    @GeneratedValue
    private Long id;

    private String financialYear;
    private String frequencyName;

    @NotNull
    private Long vatId;
    private Date createdAt;
    private Date updatedAt;

    public VATFrequency(Long vatId, JSONObject frequencyData) throws JSONException {
        this.vatId = vatId;
        this.updatedAt = new Date();
        if (frequencyData.has("financialYear")) {
            this.financialYear = frequencyData.getString("financialYear");
        }

        if (frequencyData.has("frequencyName")) {
            this.frequencyName = frequencyData.getString("frequencyName");
        }
    }

    public VATFrequency() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(String financialYear) {
        this.financialYear = financialYear;
    }

    public String getFrequencyName() {
        return frequencyName;
    }

    public void setFrequencyName(String frequencyName) {
        this.frequencyName = frequencyName;
    }

    public Long getVatId() {
        return vatId;
    }

    public void setVatId(Long vatId) {
        this.vatId = vatId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
