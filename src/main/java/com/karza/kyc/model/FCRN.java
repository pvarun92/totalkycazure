package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "fcrn")
public class FCRN extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
    private Long customerEntityId;
    private String apiValidation;
    private String fcrn;
    private String companyName;
    private String dateOfIncorporation;
    private String countryOfIncorporation;
    private String registeredAddress;
    private String emailID;
    private String foreignCompanyWithShareCapital;
    private String companyStatus;
    private String typeOfOffice;
    private String details;
    private String mainDivisionOfBusiness;
    private String descriptionOfMainDivision;
    private String statusAsPerSource;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
   	private Integer APICount;
    private Date createdAt;
    private Date updatedAt;

    @Id
    @GeneratedValue
    private Long id;

    public FCRN(String document, Long customerEntityId, String apiValidation, JSONObject fcrnData) throws JSONException {
        super(document);
        this.fcrn = document;
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
        if(fcrnData.has("apiCount")){
        	this.APICount = (Integer)fcrnData.get("apiCount");
        }
        if(fcrnData.has("llpin")){
        fcrnData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(fcrnData, "llpin");
        if (fcrnData.has("Company Name")) {
            this.companyName = fcrnData.get("Company Name").toString();
        }
        if (fcrnData.has("Date of Incorporation")) {
            this.dateOfIncorporation = fcrnData.get("Date of Incorporation").toString();
        }

        if (fcrnData.has("Country of Incorporation")) {
            this.countryOfIncorporation = fcrnData.get("Country of Incorporation").toString();
        }
        if (fcrnData.has("Registered Address")) {
            this.registeredAddress = fcrnData.get("Registered Address").toString();
        }
        if (fcrnData.has("Email Id")) {
            this.emailID = fcrnData.get("Email Id").toString();
        }
        if (fcrnData.has("Foreign Company with Share Capital")) {
            this.foreignCompanyWithShareCapital = fcrnData.get("Foreign Company with Share Capital").toString();
        }
        if (fcrnData.has("Company Status")) {
            this.companyStatus = fcrnData.get("Company Status").toString();
        }
        if (fcrnData.has("Type of Office")) {
            this.typeOfOffice = fcrnData.get("Type of Office").toString();
        }
        if (fcrnData.has("Details")) {
            this.details = fcrnData.get("Details").toString();
        }
        if (fcrnData.has("Main division of business activity to be carried out in India")) {
            this.mainDivisionOfBusiness = fcrnData.get("Main division of business activity to be carried out in India").toString();
        }
        if (fcrnData.has("Description of main division")) {
            this.descriptionOfMainDivision = fcrnData.get("Description of main division").toString();
        }
        if (fcrnData.has("statusAsPerSource")) {
            this.statusAsPerSource = fcrnData.get("Company Status").toString();
        }
        if(fcrnData.has("apiCount")){
        	this.APICount = (Integer)fcrnData.get("apiCount");
        }
        }
    }

    public FCRN(String document) {
        super(document);
    }

    public FCRN() {
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public String getFcrn() {
        return fcrn;
    }

    public void setFcrn(String fcrn) {
        this.fcrn = fcrn;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDateOfIncorporation() {
        return dateOfIncorporation;
    }

    public void setDateOfIncorporation(String dateOfIncorporation) {
        this.dateOfIncorporation = dateOfIncorporation;
    }

    public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public String getCountryOfIncorporation() {
        return countryOfIncorporation;
    }

    public void setCountryOfIncorporation(String countryOfIncorporation) {
        this.countryOfIncorporation = countryOfIncorporation;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getForeignCompanyWithShareCapital() {
        return foreignCompanyWithShareCapital;
    }

    public void setForeignCompanyWithShareCapital(String foreignCompanyWithShareCapital) {
        this.foreignCompanyWithShareCapital = foreignCompanyWithShareCapital;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getTypeOfOffice() {
        return typeOfOffice;
    }

    public void setTypeOfOffice(String typeOfOffice) {
        this.typeOfOffice = typeOfOffice;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMainDivisionOfBusiness() {
        return mainDivisionOfBusiness;
    }

    public void setMainDivisionOfBusiness(String mainDivisionOfBusiness) {
        this.mainDivisionOfBusiness = mainDivisionOfBusiness;
    }

    public String getDescriptionOfMainDivision() {
        return descriptionOfMainDivision;
    }

    public void setDescriptionOfMainDivision(String descriptionOfMainDivision) {
        this.descriptionOfMainDivision = descriptionOfMainDivision;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}