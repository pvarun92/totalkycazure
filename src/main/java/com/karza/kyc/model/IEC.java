package com.karza.kyc.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "iec")
public class IEC extends BaseDocument {

    private static final long serialVersionUID = -4060739788760795254L;

    private String iecCode;
    private String name;
    private String address;
    private String iecStatusCode;
    private String iecStatusDetails;
    private String totalNoOfBranches;
    private String statusAsPerSource;
    private Date createdAt;
    private Date updatedAt;
    private String panNumber;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
    @Id
    @GeneratedValue
    private Long id;
    private String apiValidation;
    @NotNull
    private Long customerEntityId;

    public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public IEC(String document, Long customerEntityId, String apiValidation, JSONObject iecData) throws JSONException {
        super(document);
        this.iecCode = document;
        this.apiValidation = apiValidation;
        this.customerEntityId = customerEntityId;
        if(iecData.has("apiCount")){
        	this.APICount = (Integer) iecData.get("apiCount");
        }
        if (iecData.has("name")) {
            this.name = iecData.get("name").toString();
        }
        if (iecData.has("address")) {
            this.address = iecData.get("address").toString();
        }
        if (iecData.has("iecStatusCode")) {
            this.iecStatusCode = iecData.get("iecStatusCode").toString();
        }
        if (iecData.has("iecStatusDetails")) {
            this.iecStatusDetails = iecData.get("iecStatusDetails").toString();
        }
        if (iecData.has("totalNoOfBranches")) {
            this.totalNoOfBranches = iecData.get("totalNoOfBranches").toString();
        }
        if (iecData.has("statusAsPerSource")) {
            this.statusAsPerSource = iecData.get("statusAsPerSource").toString();
        }
        this.updatedAt = new Date();
    }

    public IEC(String document, Long customerEntityId, String apiValidation, String statusAsPerSource,Integer apiCount) throws JSONException {
        super(document);
        this.iecCode = document;
        this.apiValidation = apiValidation;
        this.customerEntityId = customerEntityId;
        this.statusAsPerSource = statusAsPerSource;
        this.updatedAt = new Date();
        this.APICount = apiCount;
    }

    public IEC(Long customerEntityId, String document) {
        super(document);
        this.iecCode = document;
        this.customerEntityId = customerEntityId;
    }

    public IEC(String document) {
        super(document);
    }

    public IEC(String document, String apiValidation) {
        super(document);
        this.apiValidation = apiValidation;
    }

    public IEC(String document, Long customerEntityId, String apiValidation,Integer apiCount) {
        super(document);
        this.iecCode = document;
        this.apiValidation = apiValidation;
        this.customerEntityId = customerEntityId;
        this.APICount = apiCount;
    }

    public IEC(String document, Long customerEntityId, JSONObject iecData) throws JSONException {
        super(document);
        this.iecCode = document;
        this.customerEntityId = customerEntityId;
        if(iecData.has("apiCount")){
        	this.APICount = (Integer) iecData.get("apiCount");
        }
        if (iecData.has("status")) {
            this.apiValidation = iecData.get("status").toString();
        }
        if (iecData.has("statusAsPerSource")) {
            this.statusAsPerSource = iecData.get("statusAsPerSource").toString();
        }
		if (iecData.has("iec")) {
			iecData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(iecData, "iec");
			if (iecData.has("Name")) {
				this.name = iecData.get("Name").toString();
			}
			if (iecData.has("Address")) {
				this.address = iecData.get("Address").toString();
			}
			if (iecData.has("status")) {
				this.apiValidation = iecData.get("status").toString();
			}
			if (iecData.has("PAN")) {
				this.panNumber = iecData.get("PAN").toString();
			}
			if (iecData.has("statusAsPerSource")) {
				this.statusAsPerSource = iecData.get("statusAsPerSource").toString();
			}
			if (iecData.has("branches")) {
				// Integer noOfBranch = ((JSONArray)
				// (iecData.get("branches"))).length();
				// this.totalNoOfBranches = noOfBranch.toString();
				this.totalNoOfBranches = iecData.get("branches").toString();
			}
		}
    }


    public IEC(String apiValidation, Long customerEntityId) {
        this.apiValidation = apiValidation;
        this.customerEntityId = customerEntityId;
    }

    public IEC() {
    }

    public String getIecCode() {
        return iecCode;
    }

    public void setIecCode(String iecCode) {
        this.iecCode = iecCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIecStatusCode() {
        return iecStatusCode;
    }

    public void setIecStatusCode(String iecStatusCode) {
        this.iecStatusCode = iecStatusCode;
    }

    public String getIecStatusDetails() {
        return iecStatusDetails;
    }

    public void setIecStatusDetails(String iecStatusDetails) {
        this.iecStatusDetails = iecStatusDetails;
    }

    public String getTotalNoOfBranches() {
        return totalNoOfBranches;
    }

    public void setTotalNoOfBranches(String totalNoOfBranches) {
        this.totalNoOfBranches = totalNoOfBranches;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
