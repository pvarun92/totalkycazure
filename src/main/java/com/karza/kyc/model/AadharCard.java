package com.karza.kyc.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 2:03 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "aadhar_card")
public class AadharCard extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Long customerEntityId;

    private String apiValidation;

    private String aadharNumber;

    private String statusAsPerSource;

    private String Year;

    private Date createdAt;

    private Date updatedAt;
    
    private String referenceCode;

	public AadharCard(String document) {
        super(document);
    }

    public AadharCard() {
    }

    public AadharCard(String document, Long customerEntityId, String apiValidation, String statusAsPerSource,String referenceCode) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.aadharNumber = document;
        this.statusAsPerSource = statusAsPerSource;
        this.referenceCode=referenceCode;
        this.updatedAt = new Date();
    }
    
   
    public AadharCard(String document, Long customerEntityId) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.aadharNumber = document;
        this.updatedAt = new Date();
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

 
	public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

}
