package com.karza.kyc.model;

/**
 * Created by Fallon on 4/18/2016.
 */

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "vat")
public class VAT extends BaseDocument {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private Long id;

    private String dealerName;
    private String tinNo;
    private String effectiveCancelledDate;
    private String address1;
    private String streetName;
    private String address2;
    private String address3;
    private String talukaName;
    private String districtName;
    private String cityName;
    private String stateName;
    private String pinCode;
    private String oldRCNo;
    private String locationName;
    private String actName;
    private String statusAsPerSource;

    private Date createdAt;
    private Date updatedAt;
    @NotNull
    private Long customerEntityId;

    private String apiValidation;

    public VAT(String document, Long customerEntityId, String apiValidation, JSONObject vatData) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();

        if (vatData.has("statusAsPerSource")) {
            this.statusAsPerSource = vatData.get("statusAsPerSource").toString();
        }

        if (vatData.has("Dealer Name")) {
            this.dealerName = vatData.getString("Dealer Name");
        }

        if (vatData.has("Tin Number")) {
            this.tinNo = vatData.getString("Tin Number");
        }

        if (vatData.has("Effective/Canceled Date")) {
            this.effectiveCancelledDate = vatData.getString("Effective/Canceled Date");
        }

        if (vatData.has("Address1")) {
            this.address1 = vatData.getString("Address1");
        }

        if (vatData.has("Street Name")) {
            this.streetName = vatData.getString("Street Name");
        }

        if (vatData.has("Address2")) {
            this.address2 = vatData.getString("Address2");
        }

        if (vatData.has("Address3")) {
            this.address3 = vatData.getString("Address3");
        }

        if (vatData.has("Taluka Name")) {
            this.talukaName = vatData.getString("Taluka Name");
        }

        if (vatData.has("District Name")) {
            this.districtName = vatData.getString("District Name");
        }

        if (vatData.has("City Name")) {
            this.cityName = vatData.getString("City Name");
        }

        if (vatData.has("State Name")) {
            this.stateName = vatData.getString("State Name");
        }

        if (vatData.has("Pin Code")) {
            this.pinCode = vatData.getString("Pin Code");
        }

        if (vatData.has("Old RC No")) {
            this.oldRCNo = vatData.getString("Old RC No");
        }

        if (vatData.has("Location Name")) {
            this.locationName = vatData.getString("Location Name");
        }

        if (vatData.has("Act Name")) {
            this.actName = vatData.getString("Act Name");
        }
    }

    public VAT(String document, Long customerEntityId, String apiValidation) throws JSONException {
        super(document);
        this.tinNo = document;
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
    }

    public VAT(String document, Long customerEntityId, String apiValidation, String statusAsPerSource) throws JSONException {
    	this(document,customerEntityId,apiValidation);
        this.tinNo = document;
        this.statusAsPerSource = statusAsPerSource;
        this.updatedAt = new Date();
    }

    public VAT() {
    }

    public VAT(String document) {
        super(document);
    }

    public VAT(String document, Long customerEntityId) {
        super(document);
        this.tinNo = document;
        this.customerEntityId = customerEntityId;
        this.updatedAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getEffectiveCancelledDate() {
        return effectiveCancelledDate;
    }

    public void setEffectiveCancelledDate(String effectiveCancelledDate) {
        this.effectiveCancelledDate = effectiveCancelledDate;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getTalukaName() {
        return talukaName;
    }

    public void setTalukaName(String talukaName) {
        this.talukaName = talukaName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getOldRCNo() {
        return oldRCNo;
    }

    public void setOldRCNo(String oldRCNo) {
        this.oldRCNo = oldRCNo;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
