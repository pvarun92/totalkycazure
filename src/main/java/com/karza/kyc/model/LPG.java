package com.karza.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

@Entity
@Table(name ="lpg")
public class LPG extends BaseDocument {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long customerEntityId;
	@Column(name = "api_count", nullable = false, columnDefinition = "int default 0")
	private Integer APICount;
	private String statusAsPerSource;
	private String apiValidation;
	private String status;
	private String statusOfLpg;
	private String approximateSubsAvailed;
	private String subsRefilledConsumed;
	private String pin;
	private String consumerEmail;
	private String distributorCode;
	private String bankName;
	private String ifscCode;
	private String city;
	private String aadhaarNo;
	private String consumerContact;
	private String distributorAdd;
	private String consumerName;
	private String consumerNumber;
	private String distributorName;
	private String bankAccountNumber;
	private String givenUpSubsidy;
	private String consumerAddress;
	private String lastBookingDate;
	private String totalRefillConsumed;
	private String lpgId;

	public LPG() {

	}
	public LPG(String lpgNumber){
		super(lpgNumber);
	}
	
	public LPG(String lpgNumber, Long CustomerEntityID){
		super(lpgNumber);
		this.lpgId = lpgNumber;
		this.customerEntityId = CustomerEntityID;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLpgId() {
		return lpgId;
	}
	public void setLpgId(String lpgId) {
		this.lpgId = lpgId;
	}
	public Long getCustomerEntityId() {
		return customerEntityId;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}

	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}

	public String getApiValidation() {
		return apiValidation;
	}

	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusOfLpg() {
		return statusOfLpg;
	}

	public void setStatusOfLpg(String statusOfLpg) {
		this.statusOfLpg = statusOfLpg;
	}

	public String getApproximateSubsAvailed() {
		return approximateSubsAvailed;
	}

	public void setApproximateSubsAvailed(String approximateSubsAvailed) {
		this.approximateSubsAvailed = approximateSubsAvailed;
	}

	public String getSubsRefilledConsumed() {
		return subsRefilledConsumed;
	}

	public void setSubsRefilledConsumed(String subsRefilledConsumed) {
		this.subsRefilledConsumed = subsRefilledConsumed;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getConsumerEmail() {
		return consumerEmail;
	}

	public void setConsumerEmail(String consumerEmail) {
		this.consumerEmail = consumerEmail;
	}

	public String getDistributorCode() {
		return distributorCode;
	}
	public void setDistributorCode(String distributorCode) {
		this.distributorCode = distributorCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAadhaarNo() {
		return aadhaarNo;
	}

	public void setAadhaarNo(String aadhaarNo) {
		this.aadhaarNo = aadhaarNo;
	}

	public String getConsumerContact() {
		return consumerContact;
	}

	public void setConsumerContact(String consumerContact) {
		this.consumerContact = consumerContact;
	}

	public String getDistributorAdd() {
		return distributorAdd;
	}

	public void setDistributorAdd(String distributorAdd) {
		this.distributorAdd = distributorAdd;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getConsumerNumber() {
		return consumerNumber;
	}

	public void setConsumerNumber(String consumerNumber) {
		this.consumerNumber = consumerNumber;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getGivenUpSubsidy() {
		return givenUpSubsidy;
	}

	public void setGivenUpSubsidy(String givenUpSubsidy) {
		this.givenUpSubsidy = givenUpSubsidy;
	}



	public String getConsumerAddress() {
		return consumerAddress;
	}

	public void setConsumerAddress(String consumerAddress) {
		this.consumerAddress = consumerAddress;
	}

	public String getLastBookingDate() {
		return lastBookingDate;
	}

	public void setLastBookingDate(String lastBookingDate) {
		this.lastBookingDate = lastBookingDate;
	}

	public String getTotalRefillConsumed() {
		return totalRefillConsumed;
	}

	public void setTotalRefillConsumed(String totalRefillConsumed) {
		this.totalRefillConsumed = totalRefillConsumed;
	}

	public LPG(Long id, Long customerEntityId, Integer aPICount, String statusAsPerSource, String apiValidation,
			String status, String statusOfLpg, String approximateSubsAvailed, String subsRefilledConsumed, String pin,
			String consumerEmail, String distributorCode, String bankName, String ifscCode, String city,
			String aadhaarNo, String consumerContact, String distributorAdd, String consumerName, String consumerNumber,
			String distributorName, String bankAccountNumber, String givenUpSubsidy, String consumerAddress,
			String lastBookingDate, String totalRefillConsumed,String lpgNumber) {
		super();
		this.id = id;
		this.customerEntityId = customerEntityId;
		this.APICount = aPICount;
		this.statusAsPerSource = statusAsPerSource;
		this.apiValidation = apiValidation;
		this.status = status;
		this.statusOfLpg = statusOfLpg;
		this.approximateSubsAvailed = approximateSubsAvailed;
		this.subsRefilledConsumed = subsRefilledConsumed;
		this.pin = pin;
		this.consumerEmail = consumerEmail;
		this.distributorCode = distributorCode;
		this.bankName = bankName;
		this.ifscCode = ifscCode;
		this.city = city;
		this.aadhaarNo = aadhaarNo;
		this.consumerContact = consumerContact;
		this.distributorAdd = distributorAdd;
		this.consumerName = consumerName;
		this.consumerNumber = consumerNumber;
		this.distributorName = distributorName;
		this.bankAccountNumber = bankAccountNumber;
		this.givenUpSubsidy = givenUpSubsidy;
		this.consumerAddress = consumerAddress;
		this.lastBookingDate = lastBookingDate;
		this.totalRefillConsumed = totalRefillConsumed;
		this.lpgId = lpgNumber;
	}

	public LPG(String document, Long customerEntityId, String apiValidation, Integer apiCount){
		super(document);
		this.lpgId = document;
		this.customerEntityId = customerEntityId;
		this.apiValidation = apiValidation;
		this.APICount = apiCount;
		
	}
	public LPG(String document,Long customerEntityId,JSONObject lpgResponse) throws JSONException{
		super(document);
		this.customerEntityId = customerEntityId;
		this.lpgId = document;
		if(lpgResponse.has("apiCount")){
			this.APICount = (Integer) lpgResponse.get("apiCount");
		}
		if(lpgResponse.has("lpg")){
			lpgResponse = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(lpgResponse, "lpg");
			if(lpgResponse.has("statusAsPerSource")){
				this.statusAsPerSource = lpgResponse.get("statusAsPerSource").toString();
				this.status = lpgResponse.get("statusAsPerSource").toString();
				this.apiValidation =  lpgResponse.get("statusAsPerSource").toString();
			}
			if(lpgResponse.has("ApproximateSubsidyAvailed")){
				this.approximateSubsAvailed = lpgResponse.get("ApproximateSubsidyAvailed").toString();
			}
			if(lpgResponse.has("SubsidizedRefillConsumed")){
				this.subsRefilledConsumed = lpgResponse.get("SubsidizedRefillConsumed").toString();
			}
			if(lpgResponse.has("pin")){
				this.pin = lpgResponse.get("pin").toString();
			}
			
			if(lpgResponse.has("ConsumerEmail")){
				this.consumerEmail = lpgResponse.get("ConsumerEmail").toString();
			}
			
			if(lpgResponse.has("DistributorCode")){
				this.distributorCode = lpgResponse.get("DistributorCode").toString();
			}
			
			if(lpgResponse.has("BankName")){
				this.bankName = lpgResponse.get("BankName").toString();
			}
			
			if(lpgResponse.has("IFSCCode")){
				this.ifscCode = lpgResponse.get("IFSCCode").toString();
			}
			
			if(lpgResponse.has("city/town")){
				this.city = lpgResponse.get("city/town").toString();
			}
			
			if(lpgResponse.has("AadhaarNo")){
				this.aadhaarNo = lpgResponse.get("AadhaarNo").toString();
			}
			
			if(lpgResponse.has("ConsumerContact")){
				this.consumerContact = lpgResponse.get("ConsumerContact").toString();
			}
			
			if(lpgResponse.has("DistributorAddress")){
				this.distributorAdd = lpgResponse.get("DistributorAddress").toString();
			}
			
			if(lpgResponse.has("ConsumerName")){
				this.consumerName = lpgResponse.get("ConsumerName").toString();
			}
			
			if(lpgResponse.has("ConsumerNo")){
				this.consumerNumber = lpgResponse.get("ConsumerNo").toString();
			}
			
			if(lpgResponse.has("DistributorName")){
				this.distributorName = lpgResponse.get("DistributorName").toString();
			}
			
			if(lpgResponse.has("BankAccountNo")){
				this.bankAccountNumber = lpgResponse.get("BankAccountNo").toString();
			}
			
			if(lpgResponse.has("GivenUpSubsidy")){
				this.givenUpSubsidy = lpgResponse.get("GivenUpSubsidy").toString();
			}
			
			if(lpgResponse.has("ConsumerAddress")){
				this.consumerAddress = lpgResponse.get("ConsumerAddress").toString();
			}
			
			if(lpgResponse.has("LastBookingDate")){
				this.lastBookingDate = lpgResponse.get("LastBookingDate").toString();
			}
			
			if(lpgResponse.has("TotalRefillConsumed")){
				this.totalRefillConsumed = lpgResponse.get("TotalRefillConsumed").toString();
			}
			
		}
	}

}
