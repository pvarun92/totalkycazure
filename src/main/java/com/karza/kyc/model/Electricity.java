package com.karza.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

/**
 * Created by Varun_Prakash on 09-01-2017.
 */
@Entity
@Table(name = "electricity")
public class Electricity extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 161834114307462477L;
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	private Long customerEntityId;
	
	private String apiValidation;
	private String statusAsPerSource;
	private String bill_number;
	private String bill_due_date;
	private String consumer_number;
	private String bill_amount;
	private String bill_issue_date;
	private String mobile_no;
	private String amount_payable;
	private String total_amount;
	private String address;
	@Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
	private String consumer_name;
	private String email;
	private String bill_date;
	private String connection_type;
	private String amount;
	private String source;
	private String serviceProvider;
	private String electricityBoardId;
	private String status;


	public Electricity() {
	}

	public Electricity(String consumerNumber, String elecServiceProvider, Long customerEntityId) {
		super(consumerNumber);
		this.consumer_number = consumerNumber;
		this.serviceProvider = elecServiceProvider;
		this.customerEntityId = customerEntityId;
	}

	public Electricity(String consumerNumber, String elecServiceProvider, Long customerEntityId, String apiValidation,Integer apiCount) {
		this(consumerNumber, elecServiceProvider, customerEntityId);
		this.apiValidation = apiValidation;
		this.APICount = apiCount;

	}

	public Electricity(String consumerNumber, String elecServiceProvider, Long customerEntityId, JSONObject eData)
			throws JSONException {
		this(consumerNumber, elecServiceProvider, customerEntityId);
	//	eData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(eData,"electricity");
		if(eData==null){
			this.statusAsPerSource = "invalid";
		}
		if(eData.has("status")){
			if(!StringUtils.containsIgnoreCase(eData.getString("status").toString(),"invalid")){
				this.status = eData.getString("status").toString();
			}
			else{
				this.status = eData.getString("status").toString();
			}
		}
	
		if(eData.has("apiResponse")){
			this.apiValidation =  eData.getString("apiResponse").toString();
		}
		
		if(eData.has("statusAsPerSource")){
			this.statusAsPerSource =  eData.get("statusAsPerSource").toString();
		}
		
		if(eData.has("apiCount")){
			this.APICount = (Integer) eData.get("apiCount");
		}
		if(eData.has("electricity")){
		eData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(eData,"electricity");
		if(eData.has("apiReponse")){
			this.apiValidation=eData.getString("apiReponse").toString();
		}
		if (eData.has("bill_no")) {
			this.bill_number = eData.getString("bill_no").toString();
		}
		if (eData.has("bill_due_date")) {
			this.bill_due_date = eData.getString("bill_due_date").toString();
		}
		if (eData.has("bill_amount")) {
			this.bill_amount = eData.getString("bill_amount").toString();
		}
		if (eData.has("source")) {
			this.source = eData.getString("source").toString();
		}
		if (eData.has("amount_payable")) {
			this.amount_payable = eData.getString("amount_payable").toString();
		}
		if (eData.has("mobile_number")) {
			this.mobile_no = eData.getString("mobile_number").toString();
		}
		if (eData.has(bill_issue_date)) {
			this.bill_issue_date = eData.getString("bill_issue_date").toString();
		}
		if (eData.has("total_amount")) {
			this.total_amount = eData.getString("total_amount").toString();
		}
		if (eData.has("amount")) {
			this.amount = eData.getString("amount").toString();
		}
		if (eData.has("address")) {
			this.address = eData.getString("address").toString();
		}
		if (eData.has("consumer_name")) {
			this.consumer_name = eData.getString("consumer_name").toString();
		}
		if (eData.has("email_address")) {
			this.email = eData.getString("email_address").toString();
		}
		if (eData.has("bill_date")) {
			this.bill_date = eData.getString("bill_date").toString();

		}
		if (eData.has("connection_type")) {
			this.connection_type = eData.getString("connection_type").toString();
		}
		if(eData.has("apiCount")){
			this.APICount = (Integer) eData.get("apiCount");
		
		}
		if(eData.has("statusAsPerSource")){
			this.statusAsPerSource =  eData.get("statusAsPerSource").toString();
		}
	}

	}

	public String getElectricityBoardId() {
		return electricityBoardId;
	}

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}

	public void setElectricityBoardId(String electricityBoardId) {
		this.electricityBoardId = electricityBoardId;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}

/*	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}*/

	public String getApiValidation() {
		return apiValidation;
	}

	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}

	public String getBill_number() {
		return bill_number;
	}

	public void setBill_number(String bill_number) {
		this.bill_number = bill_number;
	}

	public String getBill_due_date() {
		return bill_due_date;
	}

	public void setBill_due_date(String bill_due_date) {
		this.bill_due_date = bill_due_date;
	}

	public String getConsumer_number() {
		return consumer_number;
	}

	public void setConsumer_number(String consumer_number) {
		this.consumer_number = consumer_number;
	}

	public String getBill_amount() {
		return bill_amount;
	}

	public void setBill_amount(String bill_amount) {
		this.bill_amount = bill_amount;
	}

	public String getBill_issue_date() {
		return bill_issue_date;
	}

	public void setBill_issue_date(String bill_issue_date) {
		this.bill_issue_date = bill_issue_date;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getAmount_payable() {
		return amount_payable;
	}

	public void setAmount_payable(String amount_payable) {
		this.amount_payable = amount_payable;
	}

	public String getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getConsumer_name() {
		return consumer_name;
	}

	public void setConsumer_name(String consumer_name) {
		this.consumer_name = consumer_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBill_date() {
		return bill_date;
	}

	public void setBill_date(String bill_date) {
		this.bill_date = bill_date;
	}

	public String getConnection_type() {
		return connection_type;
	}

	public void setConnection_type(String connection_type) {
		this.connection_type = connection_type;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Long getCustomerEntityId() {
		return customerEntityId;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
