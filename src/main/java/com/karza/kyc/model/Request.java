package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 4/1/16
 * Time: 4:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "request")
public class Request implements Serializable {
    private static final long serialVersionUID = -4060739788760795254L;

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String request_status;

    private Boolean isRead;

    private Boolean isEdited;

    private Long userMasterId;

    private Long customerMasterId;


    private Long parentRequestId;

    public Request(String request_status) {
        this.request_status = request_status;
    }

    public Request() {
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public Long getUserMasterId() {
        return userMasterId;
    }

    public void setUserMasterId(Long userMasterId) {
        this.userMasterId = userMasterId;
    }

    public Long getCustomerMasterId() {
        return customerMasterId;
    }

    public void setCustomerMasterId(Long customerMasterId) {
        this.customerMasterId = customerMasterId;
    }

    public Long getParentRequestId() {
        return parentRequestId;
    }

    public void setParentRequestId(Long parentRequestId) {
        this.parentRequestId = parentRequestId;
    }

}
