package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA. User: Vinay Date: 3/8/16 Time: 2:52 PM To change
 * this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "cin")
public class CIN extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	private Long customerEntityId;
	private String apiValidation;
	private String cin;
	private String companyName;
	private String rocCode;
	private String registrationNo;
	private String companyCategory;
	private String companySubcategory;
	private String classOfCompany;
	private String authorizedCapital;
	private String paidUpCapital;
	private String noOfMembers;
	private String dateOfIncorporation;
	private String registeredAddress;
	private String emailID;
	private String listedOrNot;
	private String dateOfLastAGM;
	private String dateOfBalanceSheet;
	private Date createdAt;
	private Date updatedAt;
	private String companyStatus;
	private String statusAsPerSource;
	@Column(name = "api_count", nullable = false, columnDefinition = "int default 0")
	private Integer APICount;

	@Id
	@GeneratedValue
	private Long id;

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public CIN(String document, Long customerEntityId, String apiValidation, JSONObject cinData) throws JSONException {
		super(document);
		this.customerEntityId = customerEntityId;
		this.apiValidation = apiValidation;
		this.updatedAt = new Date();
		this.cin = document;
		if (cinData.has("apiCount")) {
			this.APICount = (Integer) cinData.get("apiCount");
		}
		String[] fieldsToPopulate = { "llpin", "cin", "Company Name", "ROC Code", "Registration Number",
				"Company Category" };// @TOTALKYC
		String currentFieldName = fieldsToPopulate[0];
		if (cinData.has(currentFieldName)) {
			cinData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(cinData, currentFieldName);
			currentFieldName = fieldsToPopulate[1];
			if (cinData.has(currentFieldName)) {
				this.cin = cinData.get(currentFieldName).toString();
			}
			currentFieldName = fieldsToPopulate[2];
			if (cinData.has(currentFieldName)) {
				this.companyName = cinData.get(currentFieldName).toString();
			}
			currentFieldName = fieldsToPopulate[3];
			if (cinData.has(currentFieldName)) {
				this.rocCode = cinData.get(currentFieldName).toString();
			}
			currentFieldName = fieldsToPopulate[4];
			if (cinData.has(currentFieldName)) {
				this.registrationNo = cinData.get(currentFieldName).toString();
			}
			currentFieldName = fieldsToPopulate[5];
			if (cinData.has(currentFieldName)) {
				this.companyCategory = cinData.get(currentFieldName).toString();
			}
			if (cinData.has("Company SubCategory")) {
				this.companySubcategory = cinData.get("Company SubCategory").toString();
			}
			if (cinData.has("Class of Company")) {
				this.classOfCompany = cinData.get("Class of Company").toString();
			}
			if (cinData.has("Authorised Capital(Rs)")) {
				this.authorizedCapital = cinData.get("Authorised Capital(Rs)").toString();
			}
			if (cinData.has("Paid up Capital(Rs)")) {
				this.paidUpCapital = cinData.get("Paid up Capital(Rs)").toString();
			}
			if (cinData.has("Number of Members")) {
				this.noOfMembers = cinData.get("Number of Members").toString();
			}
			if (cinData.has("Date of Incorporation")) {
				this.dateOfIncorporation = cinData.get("Date of Incorporation").toString();
			}
			if (cinData.has("Registered Address")) {
				this.registeredAddress = cinData.get("Registered Address").toString();
			}
			if (cinData.has("Email Id")) {
				this.emailID = cinData.get("Email Id").toString();
			}
			if (cinData.has("Whether Listed or not")) {
				this.listedOrNot = cinData.get("Whether Listed or not").toString();
			}
			if (cinData.has("Date of last AGM")) {
				this.dateOfLastAGM = cinData.get("Date of last AGM").toString();
			}
			if (cinData.has("Date of Balance Sheet")) {
				this.dateOfBalanceSheet = cinData.get("Date of Balance Sheet").toString();
			}
			if (cinData.has("Company Status")) {
				this.companyStatus = cinData.get("Company Status").toString();
			}
			if (cinData.has("statusAsPerSource")) {
				this.statusAsPerSource = cinData.get("statusAsPerSource").toString();
			}
			if (cinData.has("apiCount")) {
				this.APICount = (Integer) cinData.get("apiCount");
			}
		}
	}

	public CIN(String document, Long customerEntityId, String apiValidation, String statusAsPerSource, Integer apiCount)
			throws JSONException {
		super(document);
		this.customerEntityId = customerEntityId;
		this.apiValidation = apiValidation;
		this.updatedAt = new Date();
		this.cin = document;
		this.statusAsPerSource = statusAsPerSource;
		this.APICount = apiCount;
	}

	public CIN(String document) {
		super(document);
	}

	public CIN() {
	}

	public Long getCustomerEntityId() {
		return customerEntityId;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public String getApiValidation() {
		return apiValidation;
	}

	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getRocCode() {
		return rocCode;
	}

	public void setRocCode(String rocCode) {
		this.rocCode = rocCode;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getCompanyCategory() {
		return companyCategory;
	}

	public void setCompanyCategory(String companyCategory) {
		this.companyCategory = companyCategory;
	}

	public String getCompanySubcategory() {
		return companySubcategory;
	}

	public void setCompanySubcategory(String companySubcategory) {
		this.companySubcategory = companySubcategory;
	}

	public String getClassOfCompany() {
		return classOfCompany;
	}

	public void setClassOfCompany(String classOfCompany) {
		this.classOfCompany = classOfCompany;
	}

	public String getAuthorizedCapital() {
		return authorizedCapital;
	}

	public void setAuthorizedCapital(String authorizedCapital) {
		this.authorizedCapital = authorizedCapital;
	}

	public String getPaidUpCapital() {
		return paidUpCapital;
	}

	public void setPaidUpCapital(String paidUpCapital) {
		this.paidUpCapital = paidUpCapital;
	}

	public String getRegisteredAddress() {
		return registeredAddress;
	}

	public void setRegisteredAddress(String registeredAddress) {
		this.registeredAddress = registeredAddress;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getListedOrNot() {
		return listedOrNot;
	}

	public void setListedOrNot(String listedOrNot) {
		this.listedOrNot = listedOrNot;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCompanyStatus() {
		return companyStatus;
	}

	public void setCompanyStatus(String companyStatus) {
		this.companyStatus = companyStatus;
	}

	public String getDateOfBalanceSheet() {
		return dateOfBalanceSheet;
	}

	public void setDateOfBalanceSheet(String dateOfBalanceSheet) {
		this.dateOfBalanceSheet = dateOfBalanceSheet;
	}

	public String getNoOfMembers() {
		return noOfMembers;
	}

	public void setNoOfMembers(String noOfMembers) {
		this.noOfMembers = noOfMembers;
	}

	public String getDateOfIncorporation() {
		return dateOfIncorporation;
	}

	public void setDateOfIncorporation(String dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}

	public String getDateOfLastAGM() {
		return dateOfLastAGM;
	}

	public void setDateOfLastAGM(String dateOfLastAGM) {
		this.dateOfLastAGM = dateOfLastAGM;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}

	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}
}