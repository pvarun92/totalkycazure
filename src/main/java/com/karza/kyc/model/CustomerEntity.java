package com.karza.kyc.model;

import org.hibernate.validator.constraints.Email;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 5:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "customer_entity")
public class CustomerEntity implements Serializable {

    private static final long serialVersionUID = -4060739788760795254L;

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String entity_type;

    private String entity_status;

    @NotNull
    private String relation;

    private String branch;

    private Date created_date;

    private String first_name;

    private String middle_name;

    private String last_name;

    private String entity_name;

    private String address;

    private String city;

    private String pinCode;

    private String state;

    private String country;

    private Date dob;

    @Email
    private String email;

    private String contactNumber;

    private String father_husband_first_name;

    private String father_husband_middle_name;

    private String father_husband_last_name;

    private String gender;

    private String validity;

    private String itrValidity;

    private int no_of_parties;

    private int no_of_documents;


    @NotNull
	public Long request_id;

    private Long branchId;

    private Long userMaster_id;

    public String getEntity_name() {
        return entity_name;
    }

    public void setEntity_name(String entity_name) {
        this.entity_name = entity_name;
    }

    public Long getRequest_id() {
        return request_id;
    }

    public void setRequest_id(Long request_id) {
        this.request_id = request_id;
    }

    public String getFather_husband_first_name() {
        return father_husband_first_name;
    }

    public void setFather_husband_first_name(String father_husband_first_name) {
        this.father_husband_first_name = father_husband_first_name;
    }

    public String getFather_husband_middle_name() {
        return father_husband_middle_name;
    }

    public void setFather_husband_middle_name(String father_husband_middle_name) {
        this.father_husband_middle_name = father_husband_middle_name;
    }

    public String getFather_husband_last_name() {
        return father_husband_last_name;
    }

    public void setFather_husband_last_name(String father_husband_last_name) {
        this.father_husband_last_name = father_husband_last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getEntity_status() {
        return entity_status;
    }

    public void setEntity_status(String entity_status) {
        this.entity_status = entity_status;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Long getUserMaster_id() {
        return userMaster_id;
    }

    public void setUserMaster_id(Long userMaster_id) {
        this.userMaster_id = userMaster_id;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getItrValidity() {
        return itrValidity;
    }

    public void setItrValidity(String itrValidity) {
        this.itrValidity = itrValidity;
    }

    public int getNo_of_parties() {
        return no_of_parties;
    }

    public void setNo_of_parties(int no_of_parties) {
        this.no_of_parties = no_of_parties;
    }

    public int getNo_of_documents() {
        return no_of_documents;
    }

    public void setNo_of_documents(int no_of_documents) {
        this.no_of_documents = no_of_documents;
    }
}
