package com.karza.kyc.model;

import java.util.Date;

public class RolePermission {


    private int id;


    private Role role;


    private Permission permission;


    private Date creationDate;


    private Date updationDate;


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public Role getRole() {
        return role;
    }


    public void setRole(Role role) {
        this.role = role;
    }


    public Permission getPermission() {
        return permission;
    }


    public void setPermission(Permission permission) {
        this.permission = permission;
    }


    public Date getCreationDate() {
        return creationDate;
    }


    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    public Date getUpdationDate() {
        return updationDate;
    }


    public void setUpdationDate(Date updationDate) {
        this.updationDate = updationDate;
    }


}
