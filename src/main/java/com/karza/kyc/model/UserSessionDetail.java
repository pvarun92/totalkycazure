package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Admin on 7/12/2016.
 */
@Entity
@Table(name = "user_session_detail")
public class UserSessionDetail {

    @Id
    @GeneratedValue
    private Long id;
    private Date updatedAt;
    private Date createdAt;
    private Long userId;
    private String sessionId;
    private String ipAddress;
    private String userStatus;

    public UserSessionDetail() {
    }

    public UserSessionDetail(Date updatedAt, Date createdAt, Long userId, String sessionId, String ipAddress, String userStatus) {
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.userId = userId;
        this.sessionId = sessionId;
        this.ipAddress = ipAddress;
        this.userStatus = userStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
}
