package com.karza.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

@Entity
@Table (name = "telephone")
public class Telephone extends BaseDocument {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long customerEntityId;
	private String apiValidation;
	private String statusAsPerSource;
	private String category;
	private String telephoneNumber;
	private String name;
	private String installationType;
	private String address;
	@Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
	public Telephone(){
		
	}
	
	public Telephone(String document){
		super(document);
	}
	
	public Telephone(String telNumber,Long customerMasterId){
		super(telNumber);
		this.telephoneNumber = telNumber;
		this.customerEntityId = customerMasterId;
	}
	
	public Telephone(String telePhoneNumber, Long customerMasterId,JSONObject telJSONObj) throws JSONException{
		
		this(telePhoneNumber,customerMasterId);
		if(telJSONObj.has("apiCount")){
			this.APICount = (Integer) telJSONObj.get("apiCount");
		}
		if(telJSONObj.has("status")){
			if(!StringUtils.containsIgnoreCase(telJSONObj.getString("status").toString(),"invalid")){
				this.statusAsPerSource = "valid";
			}
			else{
				this.statusAsPerSource = "invalid";
			}
		}
		if(telJSONObj.has("apiResponse")){
			this.apiValidation = telJSONObj.getString("apiResponse").toString();
		}
		if(telJSONObj.has("name")){
			this.name = telJSONObj.getString("name").toString();
		}
		if(telJSONObj.has("category")){
				this.category = telJSONObj.getString("category").toString();
			}
		if(telJSONObj.has("telephone")){
			this.telephoneNumber = telJSONObj.getString("telephone").toString();
		}
		if(telJSONObj.has("installation")){
			this.installationType = telJSONObj.getString("installation").toString();
		}
		if(telJSONObj.has("address")){
			this.address = telJSONObj.getString("address").toString();
		}
			
	}
	public Telephone(String document, Long customerEntityId,String APIValidation,Integer apiCount){
		super(document);
		this.telephoneNumber = document;
		this.customerEntityId = customerEntityId;
		this.apiValidation = APIValidation;
		this.APICount = apiCount;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerEntityId() {
		return customerEntityId;
	}
	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}
	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public String getApiValidation() {
		return apiValidation;
	}
	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}
	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}
	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInstallationType() {
		return installationType;
	}
	public void setInstallationType(String installationType) {
		this.installationType = installationType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}
