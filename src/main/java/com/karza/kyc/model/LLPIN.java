package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "llpin")
public class LLPIN extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue
    private Long id;
    @NotNull
    private Long customerEntityId;

    private String apiValidation;

    private String llpin;
    private String llpName;
    private String noOfPartners;
    private String noOfDesignatedPartners;
    private String rocCode;
    private String DateOfIncorporation;
    private String registeredAddress;
    private String emailID;
    private String previousFirmOrCompany;
    private String totalObligationOfContribution;
    private String mainDivisionOfBusiness;
    private String descriptionOfMainDivision;
    private String dateOfLastStatementOfAccountsAndSolvency;
    private Date createdAt;
    private Date updatedAt;
    private String dateOfLastAnnualReturn;
    private String llpStatus;
    private String statusAsPerSource;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
   	private Integer APICount;
       

    public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public LLPIN(String document, Long customerEntityId, String apiValidation, JSONObject llpinData) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
        this.llpin = document;
        if (llpinData.has("apiCount")) {
            this.APICount = (Integer)llpinData.get("apiCount");
        }
        if(llpinData.has("llpin")){
        llpinData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(llpinData, "llpin");
        if (llpinData.has("LLP Name")) {
            this.llpName = llpinData.get("LLP Name").toString();
        }
        if (llpinData.has("Number of Partners")) {
            this.noOfPartners = llpinData.get("Number of Partners").toString();
        }
        if (llpinData.has("Number of Designated Partners")) {
            this.noOfDesignatedPartners = llpinData.get("Number of Designated Partners").toString();
        }
        if (llpinData.has("ROC Code")) {
            this.rocCode = llpinData.get("ROC Code").toString();
        }
        if (llpinData.has("Date of Incorporation")) {
            this.DateOfIncorporation = llpinData.get("Date of Incorporation").toString();
        }
        if (llpinData.has("Registered Address")) {
            this.registeredAddress = llpinData.get("Registered Address").toString();
        }
        if (llpinData.has("Email Id")) {
            this.emailID = llpinData.get("Email Id").toString();
        }
        if (llpinData.has("Previous firm/ company details, if applicable")) {
            this.previousFirmOrCompany = llpinData.get("Previous firm/ company details, if applicable").toString();
        }
        if (llpinData.has("Total Obligation of Contribution")) {
            this.totalObligationOfContribution = llpinData.get("Total Obligation of Contribution").toString();
        }
        if (llpinData.has("Main division of business activity to be carried out in India")) {
            this.mainDivisionOfBusiness = llpinData.get("Main division of business activity to be carried out in India").toString();
        }
        if (llpinData.has("Description of main division")) {
            this.descriptionOfMainDivision = llpinData.get("Description of main division").toString();
        }
        if (llpinData.has("Date of last financial year end date for which Statement of Accounts and Solvency filed")) {
            this.dateOfLastStatementOfAccountsAndSolvency = llpinData.get("Date of last financial year end date for which Statement of Accounts and Solvency filed").toString();
        }
        if (llpinData.has("Date of last financial year end date for which Statement of Accounts")) {
            this.dateOfLastAnnualReturn = llpinData.get("Date of last financial year end date for which Statement of Accounts").toString();
        }
        if (llpinData.has("LLP Status")) {
            this.llpStatus = llpinData.get("LLP Status").toString();
        }
        if (llpinData.has("statusAsPerSource")) {
            this.statusAsPerSource = llpinData.get("statusAsPerSource").toString();
        }
        if (llpinData.has("apiCount")) {
            this.APICount = (Integer)llpinData.get("apiCount");
        }

        }
    }

    public LLPIN(String document, Long customerEntityId, String apiValidation,Integer apiCount) throws JSONException {
        this(document,customerEntityId,apiCount);
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
        this.llpin = document;
    }

    public LLPIN(String document, Long customerEntityId, String apiValidation, String statusAsPerSource,Integer apiCount) throws JSONException {
    	this(document,customerEntityId,apiValidation,apiCount);
        this.updatedAt = new Date();
        this.llpin = document;
        this.llpStatus = apiValidation;
        this.statusAsPerSource = statusAsPerSource;
    }

    public LLPIN(String document, Long customerEntityId,Integer apiCount) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.updatedAt = new Date();
        this.llpin = document;
        this.APICount = apiCount;
    }

    public LLPIN(String document) {
        super(document);
    }

    public LLPIN() {
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getLlpin() {
        return llpin;
    }

    public void setLlpin(String llpin) {
        this.llpin = llpin;
    }

    public String getLlpName() {
        return llpName;
    }

    public void setLlpName(String llpName) {
        this.llpName = llpName;
    }

    public String getNoOfPartners() {
        return noOfPartners;
    }

    public void setNoOfPartners(String noOfPartners) {
        this.noOfPartners = noOfPartners;
    }

    public String getNoOfDesignatedPartners() {
        return noOfDesignatedPartners;
    }

    public void setNoOfDesignatedPartners(String noOfDesignatedPartners) {
        this.noOfDesignatedPartners = noOfDesignatedPartners;
    }

    public String getRocCode() {
        return rocCode;
    }

    public void setRocCode(String rocCode) {
        this.rocCode = rocCode;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getPreviousFirmOrCompany() {
        return previousFirmOrCompany;
    }

    public void setPreviousFirmOrCompany(String previousFirmOrCompany) {
        this.previousFirmOrCompany = previousFirmOrCompany;
    }

    public String getTotalObligationOfContribution() {
        return totalObligationOfContribution;
    }

    public void setTotalObligationOfContribution(String totalObligationOfContribution) {
        this.totalObligationOfContribution = totalObligationOfContribution;
    }

    public String getMainDivisionOfBusiness() {
        return mainDivisionOfBusiness;
    }

    public void setMainDivisionOfBusiness(String mainDivisionOfBusiness) {
        this.mainDivisionOfBusiness = mainDivisionOfBusiness;
    }

    public String getDescriptionOfMainDivision() {
        return descriptionOfMainDivision;
    }

    public void setDescriptionOfMainDivision(String descriptionOfMainDivision) {
        this.descriptionOfMainDivision = descriptionOfMainDivision;
    }

    public String getDateOfLastStatementOfAccountsAndSolvency() {
        return dateOfLastStatementOfAccountsAndSolvency;
    }

    public void setDateOfLastStatementOfAccountsAndSolvency(String dateOfLastStatementOfAccountsAndSolvency) {
        this.dateOfLastStatementOfAccountsAndSolvency = dateOfLastStatementOfAccountsAndSolvency;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDateOfIncorporation() {
        return DateOfIncorporation;
    }

    public void setDateOfIncorporation(String dateOfIncorporation) {
        DateOfIncorporation = dateOfIncorporation;
    }

    public String getDateOfLastAnnualReturn() {
        return dateOfLastAnnualReturn;
    }

    public void setDateOfLastAnnualReturn(String dateOfLastAnnualReturn) {
        this.dateOfLastAnnualReturn = dateOfLastAnnualReturn;
    }

    public String getLlpStatus() {
        return llpStatus;
    }

    public void setLlpStatus(String llpStatus) {
        this.llpStatus = llpStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
