package com.karza.kyc.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA. User: Vinay Date: 4/6/16 Time: 10:49 AM To change
 * this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "driving_licence_COV")
public class DrivingLicenceCOV_Bak {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private Long DrivingLicenceMasterId;

	private String COVCategory;
	private String classOfVehicle;
	private String issueDate;

	private Date createdAt;
	private Date updatedAt;

	public DrivingLicenceCOV_Bak(Long DrivingLicenceMasterId, JSONObject dl_covData) throws JSONException {
		this.DrivingLicenceMasterId = DrivingLicenceMasterId;
		JSONObject dlCovs = new JSONObject();

		if (dl_covData.has("dlCovs")) {
			dlCovs = (JSONObject) dl_covData.get("dlCovs");
			// dlCovs = (JSONArray) dl_covData.get("dlCovs");
			// System.out.println("---> "+(JSONArray)dl_covData.get("dlCovs"));
			if (dlCovs.has("vecatg")) {
				JSONArray vecat = dl_covData.getJSONObject("dlCovs").getJSONArray("vecatg");
				for(int i=0;i<vecat.length();i++){
					this.COVCategory = vecat.getString(i);
				}
			//	this.COVCategory = dl_covData.getJSONObject("dlCovs").getJSONArray("vecatg").toString();
				// this.COVCategory = dl_covData.get("vecatg").toString();
			}
			if (dlCovs.has("covabbrv")) {
				// this.classOfVehicle = dl_covData.get("covabbrv").toString();
				JSONArray covabbr = dl_covData.getJSONObject("dlCovs").getJSONArray("covabbrv");
				//this.classOfVehicle = dl_covData.getJSONObject("dlCovs").getJSONArray("covabbrv").toString();
				for(int i=0;i<covabbr.length();i++){
					this.classOfVehicle = covabbr.getString(i);
					
				}
			}
			if (dlCovs.has("dcIssuedt")) {
				// this.issueDate = dl_covData.get("dcIssuedt").toString();
				//this.issueDate = dl_covData.getJSONObject("dlCovs").getJSONArray("dcIssuedt").toString();
				JSONArray dcIssued = dl_covData.getJSONObject("dlCovs").getJSONArray("dcIssuedt");
				for(int i=0;i<dcIssued.length();i++){
					this.issueDate = dcIssued.getString(i);
				}
			}
			this.updatedAt = new Date();
		}
	}

	public DrivingLicenceCOV_Bak() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDrivingLicenceMasterId() {
		return DrivingLicenceMasterId;
	}

	public void setDrivingLicenceMasterId(Long drivingLicenceMasterId) {
		DrivingLicenceMasterId = drivingLicenceMasterId;
	}

	public String getCOVCategory() {
		return COVCategory;
	}

	public void setCOVCategory(String COVCategory) {
		this.COVCategory = COVCategory;
	}

	public String getClassOfVehicle() {
		return classOfVehicle;
	}

	public void setClassOfVehicle(String classOfVehicle) {
		this.classOfVehicle = classOfVehicle;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
