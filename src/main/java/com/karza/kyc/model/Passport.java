package com.karza.kyc.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/9/16
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "passport")
public class Passport extends BaseDocument {

    private static final long serialVersionUID = -4060739788760795254L;

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String country;

    @NotEmpty
    private String address;

    @NotNull
    private Long customerEntityId;

    private Date expiryDate;

    private Date dob;

    private String statusAsPerSource;

    public Passport(String document) {
        super(document);
    }

    public Passport(String document, String country) {
        super(document);
        this.country = country;
    }

    public Passport(String document, String country, String address) {
        super(document);
        this.country = country;
        this.address = address;
    }

    public Passport(String document, Long customerEntityId, String country, String address, Date expiryDate, Date dob) {
    	this(document,country,address);
        this.customerEntityId = customerEntityId;
        this.expiryDate = expiryDate;
        this.dob = dob;
    }

    public Passport() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
