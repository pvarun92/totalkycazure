package com.karza.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;


/*THis is the Model class for the Employement Provident Fund*/
@Entity
@Table(name = "epfo_request")
public class EPFORequest extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EPFORequest() {
	}

	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long customerEntityId;
	@Column(name = "api_count", nullable = false, columnDefinition = "int default 0")
	private Integer APICount;
	private String statusAsPerSource;
	private String apiValidation;
	private String status;
	private String uan_number;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerEntityId() {
		return customerEntityId;
	}

	public void setCustomerEntityId(Long customerEntityId) {
		this.customerEntityId = customerEntityId;
	}

	public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}


	public String getStatusAsPerSource() {
		return statusAsPerSource;
	}

	public void setStatusAsPerSource(String statusAsPerSource) {
		this.statusAsPerSource = statusAsPerSource;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApiValidation() {
		return apiValidation;
	}

	public void setApiValidation(String apiValidation) {
		this.apiValidation = apiValidation;
	}

	public String getUan_number() {
		return uan_number;
	}

	public void setUan_number(String uan_number) {
		this.uan_number = uan_number;
	}

	public EPFORequest(String document, Long customeEntityId, String apiValidation, Integer apiCount) {
		super(document);
		this.uan_number = document;
		this.customerEntityId = customeEntityId;
		this.apiValidation = apiValidation;
		this.APICount = apiCount;
	}

	public EPFORequest(String uanNumber, Long customerEntityId) {
		super(uanNumber);
		this.uan_number = uanNumber;
		this.customerEntityId = customerEntityId;
	}

	@Autowired
	public EPFORequest(String uanNumber, Long customerEntityId, JSONObject epfoJSONObject) throws JSONException {
		super(uanNumber);
		this.customerEntityId = customerEntityId;
		
		if (epfoJSONObject.has("apiResponse")) {
			this.apiValidation = epfoJSONObject.getString("apiResponse").toString();
		}
		if (epfoJSONObject.has("status")) {
			this.status = epfoJSONObject.getString("status").toString();
		}

	}
}
