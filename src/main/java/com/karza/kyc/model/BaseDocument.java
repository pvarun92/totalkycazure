package com.karza.kyc.model;

/**
 * Created by Admin on 07-03-2016.
 */

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@MappedSuperclass
public class BaseDocument implements Serializable {

    
	private static final long serialVersionUID = 296669255302896324L;
	@NotNull
    @Column(name = "document")
    private String document;

    public BaseDocument() {
    }

    public BaseDocument(String document) {
        this.document = document;
    }

    public String getDocument() {
        return document;
    }
}
