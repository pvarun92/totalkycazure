package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 7/25/16
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "iec_branch")
public class IEC_Branch {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Long iecId;
    private String Address;
    private String branchSerialNumber;
    private Date createdAt;
    private Date updatedAt;

    public IEC_Branch(Long iecId, String address, String branchSerialNumber) {
        this.iecId = iecId;
        Address = address;
        this.branchSerialNumber = branchSerialNumber;
    }

    public IEC_Branch() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIecId() {
        return iecId;
    }

    public void setIecId(Long iecId) {
        this.iecId = iecId;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBranchSerialNumber() {
        return branchSerialNumber;
    }

    public void setBranchSerialNumber(String branchSerialNumber) {
        this.branchSerialNumber = branchSerialNumber;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
