package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Mahananda on 28-April-16.
 */
@Entity
@Table(name = "subscription_master")
public class SubscriptionMaster {
    @Id
    @GeneratedValue
    private Long id;

    private Date createdAt;

    private Date updatedAt;

    private String application;

    private String clientId;

    private String plan;

    private String oneTimeFees;

    private String perUserIdFees;

    private String perEntityRate;

    private Date contractDate;

    private String billingPeriodicity;

    private Date billingStartDate;

    private Date billingEndDate;
    private Date validFromDate;
    private Date validTillDate;
    private Long customerMasterId;

    public SubscriptionMaster() {
    }

    @SuppressWarnings("deprecation")
	public SubscriptionMaster(JSONObject requestData) throws JSONException {
        if (requestData.has("contractDate")) {
            this.contractDate = new Date(requestData.get("contractDate").toString());
        }
        if (requestData.has("validTill")) {
            this.validTillDate = new Date(requestData.get("validTill").toString());
        }
        if (requestData.has("perUserIdFees")) {
            this.perUserIdFees = requestData.get("perUserIdFees").toString();
        }
        if (requestData.has("perEntityRate")) {
            this.perEntityRate = requestData.get("perEntityRate").toString();
        }
        if (requestData.has("billingEndDate")) {
            this.billingEndDate = new Date(requestData.get("billingEndDate").toString());
        }
        if (requestData.has("oneTimeFees")) {
            this.oneTimeFees = requestData.get("oneTimeFees").toString();
        }
        if (requestData.has("billingPeriodicity")) {
            this.billingPeriodicity = requestData.get("billingPeriodicity").toString();
        }
        if (requestData.has("validFromDate")) {
            this.validFromDate = new Date(requestData.get("validFromDate").toString());
        }
        if (requestData.has("billingStartDate")) {
            this.billingStartDate = new Date(requestData.get("billingStartDate").toString());
        }

    }

    public Date getValidTillDate() {
        return validTillDate;
    }

    public void setValidTillDate(Date validTillDate) {
        this.validTillDate = validTillDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getOneTimeFees() {
        return oneTimeFees;
    }

    public void setOneTimeFees(String oneTimeFees) {
        this.oneTimeFees = oneTimeFees;
    }

    public String getPerUserIdFees() {
        return perUserIdFees;
    }

    public void setPerUserIdFees(String perUserIdFees) {
        this.perUserIdFees = perUserIdFees;
    }

    public String getPerEntityRate() {
        return perEntityRate;
    }

    public void setPerEntityRate(String perEntityRate) {
        this.perEntityRate = perEntityRate;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getBillingPeriodicity() {
        return billingPeriodicity;
    }

    public void setBillingPeriodicity(String billingPeriodicity) {
        this.billingPeriodicity = billingPeriodicity;
    }

    public Date getBillingStartDate() {
        return billingStartDate;
    }

    public void setBillingStartDate(Date billingStartDate) {
        this.billingStartDate = billingStartDate;
    }

    public Date getBillingEndDate() {
        return billingEndDate;
    }

    public void setBillingEndDate(Date billingEndDate) {
        this.billingEndDate = billingEndDate;
    }

    public Date getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(Date validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Long getCustomerMasterId() {
        return customerMasterId;
    }

    public void setCustomerMasterId(Long customerMasterId) {
        this.customerMasterId = customerMasterId;
    }

}