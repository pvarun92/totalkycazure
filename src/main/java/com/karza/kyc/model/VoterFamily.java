package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 7/27/16
 * Time: 4:03 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "voter_family")
public class VoterFamily {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String voterId;
    private String voterName;
    private String srNO;
    private String partNO;
    private String gender;
    private String acNO;
    private String voterIdCardNo;
    private String age;
    private Date createdAt;
    private Date updatedAt;

    public VoterFamily() {
    }

    public VoterFamily(String voterId, JSONObject familyDetail) throws JSONException {
        if (familyDetail.has("SR NO")) {
            this.srNO = familyDetail.get("SR NO").toString();
        }
        if (familyDetail.has("Voter Name")) {
            this.voterName = familyDetail.get("Voter Name").toString();
        }
        if (familyDetail.has("PART NO")) {
            this.partNO = familyDetail.get("PART NO").toString();
        }
        if (familyDetail.has("GENDER")) {
            this.gender = familyDetail.get("GENDER").toString();
        }
        if (familyDetail.has("AC NO")) {
            this.acNO = familyDetail.get("AC NO").toString();
        }
        if (familyDetail.has("ID CARD NO")) {
            this.acNO = familyDetail.get("ID CARD NO").toString();
        }
        if (familyDetail.has("AGE")) {
            this.age = familyDetail.get("AGE").toString();
        }
        if (familyDetail.has("ID CARD NO")) {
            this.voterIdCardNo = familyDetail.get("ID CARD NO").toString();
        }
        this.voterId = voterId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getVoterName() {
        return voterName;
    }

    public void setVoterName(String voterName) {
        this.voterName = voterName;
    }

    public String getSrNO() {
        return srNO;
    }

    public void setSrNO(String srNO) {
        this.srNO = srNO;
    }

    public String getPartNO() {
        return partNO;
    }

    public void setPartNO(String partNO) {
        this.partNO = partNO;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAcNO() {
        return acNO;
    }

    public void setAcNO(String acNO) {
        this.acNO = acNO;
    }

    public String getVoterIdCardNo() {
        return voterIdCardNo;
    }

    public void setVoterIdCardNo(String voterIdCardNo) {
        this.voterIdCardNo = voterIdCardNo;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
