package com.karza.kyc.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.util.KYCUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "excise")
public class Excise extends BaseDocument {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue
    private Long id;
    private String exciseNumber;
    @NotNull
    private Long customerEntityId;
    private String apiValidation;
    private String assesseeBelongsTo;
    private String nameOfAssessee;
    private String addressOfAssessee;
    private String locationCode;
    private String commissionerateCode;
    private String divisionCode;
    private String rangeCode;
    private String status;
    private String statusAsOn;
    private String statusAsPerSource;
    private Date createdAt;
    private Date updatedAt;
    @Column(name="api_count",nullable = false,columnDefinition = "int default 0")
	private Integer APICount;
    public Integer getAPICount() {
		return APICount;
	}

	public void setAPICount(Integer aPICount) {
		APICount = aPICount;
	}

	public Excise(String document, Long customerEntityId, String apiValidation, JSONObject exceiseData) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
        this.exciseNumber = document;
        if (exceiseData.has("status")) {
            this.status = exceiseData.get("status").toString();
        }
        if (exceiseData.has("status_as_on")) {
            this.statusAsOn = exceiseData.get("status_as_on").toString();
        }
        if (exceiseData.has("status")) {
            this.statusAsPerSource = exceiseData.get("status").toString();
        }
        if(exceiseData.has("apiCount")){
        	this.APICount = (Integer) exceiseData.get("apiCount");
        } 
        if(exceiseData.has("excise")){
        exceiseData =(JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(exceiseData, "excise");
        if (exceiseData.has("Name of Assessee")) {
            this.nameOfAssessee = exceiseData.get("Name of Assessee").toString();
        }
        if (exceiseData.has("Assessee Belongs To")) {
            this.assesseeBelongsTo = exceiseData.get("Assessee Belongs To").toString();
        }
        if (exceiseData.has("Address of Assessee")) {
            this.addressOfAssessee = exceiseData.get("Address of Assessee").toString();
        }
        if (exceiseData.has("Location Code")) {
            this.locationCode = exceiseData.get("Location Code").toString();
        }
        if (exceiseData.has("Commissionerate Code")) {
            this.commissionerateCode = exceiseData.get("Commissionerate Code").toString();
        }
        if (exceiseData.has("Division Code")) {
            this.divisionCode = exceiseData.get("Division Code").toString();
        }
        if (exceiseData.has("Range Code")) {
            this.rangeCode = exceiseData.get("Range Code").toString();
        }
        if (exceiseData.has("Status")) {
            this.status = exceiseData.get("Status").toString();
        }
        if (exceiseData.has("status_as_on")) {
            this.statusAsOn = exceiseData.get("status_as_on").toString();
        }
        if (exceiseData.has("Status")) {
            this.statusAsPerSource = exceiseData.get("Status").toString();
        }
        if(exceiseData.has("apiCount")){
        	this.APICount = (Integer) exceiseData.get("apiCount");
        }
      }

    }

    public Excise(String document, Long customerEntityId, String apiValidation,Integer apiCount) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
        this.exciseNumber = document;
        this.APICount = apiCount;
    }

    public Excise(String document, Long customerEntityId, String apiValidation, String statusAsPerSource, String status,Integer apiCount) throws JSONException {
    	this(document,customerEntityId,apiValidation,apiCount);
        this.updatedAt = new Date();
        this.exciseNumber = document;
        this.statusAsPerSource = statusAsPerSource;
        this.status = status;
    }


    public Excise(String document, Long customerEntityId,Integer apiCount) {
        super(document);
        this.customerEntityId = customerEntityId;
        this.updatedAt = new Date();
        this.exciseNumber = document;
        this.APICount = apiCount;
    }

    public Excise(String document) {
        super(document);
    }

    public Excise() {
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getExciseNumber() {
        return exciseNumber;
    }

    public void setExciseNumber(String exciseNumber) {
        this.exciseNumber = exciseNumber;
    }

    public String getAssesseeBelongsTo() {
        return assesseeBelongsTo;
    }

    public void setAssesseeBelongsTo(String assesseeBelongsTo) {
        this.assesseeBelongsTo = assesseeBelongsTo;
    }

    public String getNameOfAssessee() {
        return nameOfAssessee;
    }

    public void setNameOfAssessee(String nameOfAssessee) {
        this.nameOfAssessee = nameOfAssessee;
    }

    public String getAddressOfAssessee() {
        return addressOfAssessee;
    }

    public void setAddressOfAssessee(String addressOfAssessee) {
        this.addressOfAssessee = addressOfAssessee;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCommissionerateCode() {
        return commissionerateCode;
    }

    public void setCommissionerateCode(String commissionerateCode) {
        this.commissionerateCode = commissionerateCode;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getRangeCode() {
        return rangeCode;
    }

    public void setRangeCode(String rangeCode) {
        this.rangeCode = rangeCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusAsOn() {
        return statusAsOn;
    }

    public void setStatusAsOn(String statusAsOn) {
        this.statusAsOn = statusAsOn;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
