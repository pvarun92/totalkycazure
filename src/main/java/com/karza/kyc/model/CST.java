package com.karza.kyc.model;

/**
 * Created by Fallon on 4/18/2016.
 */

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "cst")
public class CST extends BaseDocument {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    private Long id;

    private String dealerName;
    private String tinNo;
    private String effectiveCancelledDate;
    private String address1;
    private String streetName;
    private String address2;
    private String address3;
    private String talukaName;
    private String districtName;
    private String cityName;
    private String stateName;
    private String pinCode;
    private String oldRCNo;
    private String locationName;
    private String actName;
    private String statusAsPerSource;

    private Date createdAt;
    private Date updatedAt;
    @NotNull
    private Long customerEntityId;

    private String apiValidation;

    public CST(String document, Long customerEntityId, String apiValidation, JSONObject cstData) throws JSONException {
        super(document);
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();

        if (cstData.has("statusAsPerSource")) {
            this.statusAsPerSource = cstData.get("statusAsPerSource").toString();
        }
        if (cstData.has("Dealer Name")) {
            this.dealerName = cstData.getString("Dealer Name");
        }

        if (cstData.has("Tin Number")) {
            this.tinNo = cstData.getString("Tin Number");
        }

        if (cstData.has("Effective/Canceled Date")) {
            this.effectiveCancelledDate = cstData.getString("Effective/Canceled Date");
        }

        if (cstData.has("Address1")) {
            this.address1 = cstData.getString("Address1");
        }

        if (cstData.has("Street Name")) {
            this.streetName = cstData.getString("Street Name");
        }

        if (cstData.has("Address2")) {
            this.address2 = cstData.getString("Address2");
        }

        if (cstData.has("Address3")) {
            this.address3 = cstData.getString("Address3");
        }

        if (cstData.has("Taluka Name")) {
            this.talukaName = cstData.getString("Taluka Name");
        }

        if (cstData.has("District Name")) {
            this.districtName = cstData.getString("District Name");
        }

        if (cstData.has("City Name")) {
            this.cityName = cstData.getString("City Name");
        }

        if (cstData.has("State Name")) {
            this.stateName = cstData.getString("State Name");
        }

        if (cstData.has("Pin Code")) {
            this.pinCode = cstData.getString("Pin Code");
        }

        if (cstData.has("Old RC No")) {
            this.oldRCNo = cstData.getString("Old RC No");
        }

        if (cstData.has("Location Name")) {
            this.locationName = cstData.getString("Location Name");
        }

        if (cstData.has("Act Name")) {
            this.actName = cstData.getString("Act Name");
        }
    }

    public CST(String document, Long customerEntityId, String apiValidation) throws JSONException {
        super(document);
        this.tinNo = document;
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.updatedAt = new Date();
    }

    public CST(String document, Long customerEntityId, String apiValidation, String statusAsPerSource) throws JSONException {
        super(document);
        this.tinNo = document;
        this.customerEntityId = customerEntityId;
        this.apiValidation = apiValidation;
        this.statusAsPerSource = statusAsPerSource;
        this.updatedAt = new Date();
    }

    public CST() {
    }

    public CST(String document) {
        super(document);
    }

    public CST(String document, Long customerEntityId) {
        super(document);
        this.tinNo = document;
        this.customerEntityId = customerEntityId;
        this.updatedAt = new Date();
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getEffectiveCancelledDate() {
        return effectiveCancelledDate;
    }

    public void setEffectiveCancelledDate(String effectiveCancelledDate) {
        this.effectiveCancelledDate = effectiveCancelledDate;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getTalukaName() {
        return talukaName;
    }

    public void setTalukaName(String talukaName) {
        this.talukaName = talukaName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getOldRCNo() {
        return oldRCNo;
    }

    public void setOldRCNo(String oldRCNo) {
        this.oldRCNo = oldRCNo;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getCustomerEntityId() {
        return customerEntityId;
    }

    public void setCustomerEntityId(Long customerEntityId) {
        this.customerEntityId = customerEntityId;
    }

    public String getApiValidation() {
        return apiValidation;
    }

    public void setApiValidation(String apiValidation) {
        this.apiValidation = apiValidation;
    }

    public String getStatusAsPerSource() {
        return statusAsPerSource;
    }

    public void setStatusAsPerSource(String statusAsPerSource) {
        this.statusAsPerSource = statusAsPerSource;
    }
}
