package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Mahananda on 28-April-16.
 */
@Entity
@Table(name = "location_master")
public class LocationMaster {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Long customerMasterId;

    private Date createdAt;

    private Date updatedAt;

    private String locationOrZone;

    private String linkedBranches;

    private Long parentLocation;

    public LocationMaster() {
    }

    public LocationMaster(String locationOrZone, Long customerMasterId, Long parentLocation) {
        this.locationOrZone = locationOrZone;
        this.customerMasterId = customerMasterId;
        this.parentLocation = parentLocation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLocationOrZone() {
        return locationOrZone;
    }

    public void setLocationOrZone(String locationOrZone) {
        this.locationOrZone = locationOrZone;
    }

    public String getLinkedBranches() {
        return linkedBranches;
    }

    public void setLinkedBranches(String linkedBranches) {
        this.linkedBranches = linkedBranches;
    }

    public Long getCustomerMasterId() {
        return customerMasterId;
    }

    public void setCustomerMasterId(Long customerMasterId) {
        this.customerMasterId = customerMasterId;
    }

    public Long getParentLocation() {
        return parentLocation;
    }

    public void setParentLocation(Long parentLocation) {
        this.parentLocation = parentLocation;
    }
}
