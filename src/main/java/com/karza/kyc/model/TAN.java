package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/11/16
 * Time: 11:51 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tan")
public class TAN extends BaseDocument {

    private static final long serialVersionUID = -4060739788760795254L;

    @Id
    @GeneratedValue
    private Long id;

    public TAN(String document) {
        super(document);
    }

    public TAN() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
