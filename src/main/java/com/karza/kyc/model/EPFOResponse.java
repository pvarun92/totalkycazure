package com.karza.kyc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;


/*THis is the Model class for the Employement Provident Fund*/
@Entity
@Table(name = "epfo_response")
public class EPFOResponse extends BaseDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EPFOResponse() {
	}

	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long epfoRequestID;
	private String statusOfSettlement;
	private String name;
	private String employer_share;
	private String mid;
	private String employee_share;
	private String dob;
	private String last_tran_date;
	private String last_contri_amt;
	private String uan_number;
	private String balance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmployer_share() {
		return employer_share;
	}

	public void setEmployer_share(String employer_share) {
		this.employer_share = employer_share;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getEmployee_share() {
		return employee_share;
	}

	public void setEmployee_share(String employee_share) {
		this.employee_share = employee_share;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getLast_tran_date() {
		return last_tran_date;
	}

	public void setLast_tran_date(String last_tran_date) {
		this.last_tran_date = last_tran_date;
	}

	public String getLast_contri_amt() {
		return last_contri_amt;
	}

	public void setLast_contri_amt(String last_contri_amt) {
		this.last_contri_amt = last_contri_amt;
	}

	public String getUan_number() {
		return uan_number;
	}

	public void setUan_number(String uan_number) {
		this.uan_number = uan_number;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}



	public Long getEpfoRequestID() {
		return epfoRequestID;
	}

	public void setEpfoRequestID(Long epfoRequestID) {
		this.epfoRequestID = epfoRequestID;
	}

	public String getStatusOfSettlement() {
		return statusOfSettlement;
	}

	public void setStatusOfSettlement(String statusOfSettlement) {
		this.statusOfSettlement = statusOfSettlement;
	}

	public EPFOResponse(String document, Long customeEntityId, String apiValidation, Integer apiCount) {
		super(document);
		this.uan_number = document;
	}

	public EPFOResponse(String uanNumber, Long customerEntityId) {
		super(uanNumber);
		this.uan_number = uanNumber;
	}

//	@Autowired
	public EPFOResponse(String uanNumber, JSONObject epfoJSONObject) throws JSONException {
		super(uanNumber);
		this.uan_number = uanNumber;
		if (epfoJSONObject.has("name")) {
			this.name = epfoJSONObject.getString("name").toString();
		}
		if (epfoJSONObject.has("statusOfSettlement")) {
			this.statusOfSettlement = epfoJSONObject.getString("statusOfSettlement").toString();
		}
		
		if (epfoJSONObject.has("mid")) {
			this.mid = epfoJSONObject.getString("mid").toString();
		}
		
		if (epfoJSONObject.has("last_transaction_date")) {
			this.last_tran_date = epfoJSONObject.getString("last_transaction_date").toString();
		}
		if (epfoJSONObject.has("last_contributed_amount")) {
			this.last_contri_amt = epfoJSONObject.getString("last_contributed_amount").toString();
		}
		if (epfoJSONObject.has("employee_share")) {
			this.employee_share = epfoJSONObject.getString("employee_share").toString();
		}
		if (epfoJSONObject.has("balance")) {
			this.balance = epfoJSONObject.getString("balance").toString();
		}
		if (epfoJSONObject.has("dob")) {
			this.dob = epfoJSONObject.getString("dob");
		}
		if (epfoJSONObject.has("employer_share")) {
			this.employer_share = epfoJSONObject.getString("employer_share").toString();
		}
	}
}
