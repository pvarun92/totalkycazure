package com.karza.kyc.dao;

import java.util.List;

import com.karza.kyc.model.ElectricityBoard;

public interface ElectricityBoardDao {
	// Electricity board details
	ElectricityBoard get(Long id);
	// find all the list of electricity boards
	List<ElectricityBoard> findAll();
	// save the details of the electricity bill
	void save(ElectricityBoard board);
	//Electricity Board FullName from Electricity Board Code
	String getBoardName(String electricityBoardCode);
	
}
