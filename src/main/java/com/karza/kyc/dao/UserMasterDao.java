package com.karza.kyc.dao;

import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.UserSessionDetail;

import java.util.List;

/**
 * Created by Mahananda on 28-April-16.
 */

public interface UserMasterDao {
    UserMaster get(Long id);

    UserMaster getByCustomerId(Long CustomerId);

    UserMaster save(UserMaster userMaster);

    void update(UserMaster userMaster);

    void delete(UserMaster userMaster);

    List<UserMaster> findAll();

    List<UserMaster> findByCustomerId(Long customerMasterId);

    List<UserMaster> findByCustomerIdAndRole(Long customerMasterId, String role);

    List<UserMaster> findByRole(String role);

    UserMaster getUserByUsernameAndPassword(String username, String password);

    UserMaster getUserByUsernameAndPasswordForChangePassword(String username, String password);

    String IsExistUser(String email, String contactNo);

    String IsEmailExist(String email);

    String IsContactExist(String contactNo);

    Integer getMaxUserAccountNumber(Long customerMasterId);

    UserMaster getUserByUsername(String username);

    List<UserSessionDetail> getInactiveUserList();

    List<UserMaster> getUserIdAndEmailByPendingEntities();

    Integer replaceLocation(Long newLocation, Long oldLocation);

    Integer getUserCountByCustomerId(Long customerMasterId);

    List<UserMaster> getAll();
    
    List<UserMaster> getUsersOfCustomer(String customerName);
    
    List<BranchMaster> getBranchOfCustomer(String customerName);

}
