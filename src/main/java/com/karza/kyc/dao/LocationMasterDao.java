package com.karza.kyc.dao;

import com.karza.kyc.model.LocationMaster;

import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
public interface LocationMasterDao {
    LocationMaster get(Long id);

    void save(LocationMaster locationMaster);

    LocationMaster saveAndGet(LocationMaster locationMaster);

    LocationMaster getParentLocation(Long customerMasterId);

    void update(LocationMaster locationMaster);

    void delete(LocationMaster locationMaster);

    List<LocationMaster> findAll();

    List<LocationMaster> findByCustomerId(Long customerMasterId);

    List<LocationMaster> getChildLocations(Long locationId);

    Integer getUserCountByCustomerId(Long customerMasterId);
}
