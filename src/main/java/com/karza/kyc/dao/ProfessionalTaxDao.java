package com.karza.kyc.dao;

import com.karza.kyc.model.ProfessionalTax;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
public interface ProfessionalTaxDao {
    ProfessionalTax get(Long id);

    ProfessionalTax getByCustomerId(Long CustomerId);

    ProfessionalTax save(ProfessionalTax professionalTax);

    void update(ProfessionalTax professionalTax);

    void delete(ProfessionalTax professionalTax);

    List<ProfessionalTax> findAll();
}
