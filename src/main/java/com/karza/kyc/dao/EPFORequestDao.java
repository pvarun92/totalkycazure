package com.karza.kyc.dao;

import java.util.List;
import com.karza.kyc.model.EPFORequest;

public interface EPFORequestDao {
	
	// To get the Detail of any Particular data related to the EPFO
	EPFORequest get(Long id);
	
	// To save the EPFO details
	EPFORequest Save(EPFORequest epfo);
	
	//TO update the Details of the EPFO
	void update(EPFORequest epfo);
	
	// To delete the details of the EPFO
	void delete(EPFORequest epfo);
	
	//To list down all the EPFO numbers existing in DB
	List<EPFORequest> findAll();
	
	// Fetch the EPFO detail related to any specific Customer
	EPFORequest getByCustomerID(Long customerID);

}
