package com.karza.kyc.dao;

import java.util.List;
import com.karza.kyc.model.Telephone;

public interface TelephoneDao {

	Telephone get(Long id); // Get the Telephone Data

	Telephone save(Telephone telephone); // Save the Telphone Related Data to the DB

	void update(Telephone telephone); // Update the telephone related to Telephone Number

	void Delete(Telephone telephone); // Delete the telephone related to  telephone Number

	List<Telephone> findAll(); // List all the Telphone related data

	Telephone getByCustomerId(Long customerId); // fetch the data related to any particular customer

}
