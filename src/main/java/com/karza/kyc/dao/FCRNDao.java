package com.karza.kyc.dao;


import com.karza.kyc.model.FCRN;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface FCRNDao {
    FCRN get(Long id);

    void save(FCRN fcrn);

    void delete(FCRN fcrn);

    List<FCRN> findAll();

    void update(FCRN fcrn);

    FCRN getByCustomerId(Long CustomerId);
}
