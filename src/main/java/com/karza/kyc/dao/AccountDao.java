package com.karza.kyc.dao;

import com.karza.kyc.model.Account;

import java.util.List;

/**
 * Created by Fallon Software on 4/14/2016.
 */
public interface AccountDao {
    Account get(Long id);

    void save(Account accountUser);

    void update(Account accountUser);

    void delete(Account accountUser);

    List<Account> findAll();

    Account findByAccountUserName(String accountUserName);
}
