package com.karza.kyc.dao;

import com.karza.kyc.model.CSTFrequency;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
public interface CSTFrequencyDao {
    CSTFrequency get(Long id);

    List<CSTFrequency> getByCstId(Long cstId);

    void save(CSTFrequency cstFrequency);

    void update(CSTFrequency cstFrequency);

    void delete(CSTFrequency cstFrequency);

    List<CSTFrequency> findAll();
}
