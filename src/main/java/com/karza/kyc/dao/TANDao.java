package com.karza.kyc.dao;

import com.karza.kyc.model.TAN;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface TANDao {
    TAN get(Long id);

    void save(TAN tan);

    void delete(TAN tan);

    List<TAN> findAll();

}
