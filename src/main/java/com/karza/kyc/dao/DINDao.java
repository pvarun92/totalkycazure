package com.karza.kyc.dao;

import com.karza.kyc.model.DIN;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface DINDao {

    DIN get(Long id);

    void save(DIN din);

    void delete(DIN din);

    List<DIN> findAll();
}
