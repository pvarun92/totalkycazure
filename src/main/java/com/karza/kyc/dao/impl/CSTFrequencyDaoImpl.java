package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.CSTFrequencyDao;
import com.karza.kyc.model.CSTFrequency;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
@Repository
public class CSTFrequencyDaoImpl implements CSTFrequencyDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CSTFrequency get(Long id) {
        return (CSTFrequency) sessionFactory.getCurrentSession().get(CSTFrequency.class, id);
    }

    @Override
    public List<CSTFrequency> getByCstId(Long cstId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CSTFrequency where cstId =" + cstId);
        List<CSTFrequency> cstFrequencyList = query.list();
        if (cstFrequencyList.size() > 0)
            return cstFrequencyList;
        else
            return null;
    }

    @Override
    public void save(CSTFrequency cstFrequency) {
        sessionFactory.getCurrentSession().merge(cstFrequency);
    }

    @Override
    public void update(CSTFrequency cstFrequency) {
        sessionFactory.getCurrentSession().update(cstFrequency);
    }

    @Override
    public void delete(CSTFrequency cstFrequency) {
        sessionFactory.getCurrentSession().delete(cstFrequency);
    }

    @Override
    public List<CSTFrequency> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CSTFrequency ");
        List<CSTFrequency> cstFrequencies = query.list();
        return cstFrequencies;
    }
}
