package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.VATDao;
import com.karza.kyc.model.VAT;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
@Repository
public class VATDaoImpl implements VATDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public VAT get(Long id) {
        return (VAT) sessionFactory.getCurrentSession().get(VAT.class, id);
    }

    @Override
    public VAT getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VAT where customerEntityId =" + CustomerId);
        List<VAT> VATList = query.list();
        if (VATList.size() > 0)
            return VATList.get(0);
        else
            return null;
    }

    @Override
    public VAT save(VAT vat) {
        return (VAT) sessionFactory.getCurrentSession().merge(vat);
    }

    @Override
    public void update(VAT vat) {
        sessionFactory.getCurrentSession().update(vat);
    }

    @Override
    public void delete(VAT vat) {
        sessionFactory.getCurrentSession().delete(vat);
    }

    @Override
    public List<VAT> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from VAT ");
        List<VAT> vats = query.list();
        return vats;
    }
}
