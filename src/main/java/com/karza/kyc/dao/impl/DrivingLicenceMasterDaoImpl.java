package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.DrivingLicenceMasterDao;
import com.karza.kyc.model.DrivingLicenceMaster;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Repository
public class DrivingLicenceMasterDaoImpl implements DrivingLicenceMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public DrivingLicenceMaster get(Long id) {
        return (DrivingLicenceMaster) sessionFactory.getCurrentSession().get(DrivingLicenceMaster.class, id);
    }

    @Override
    public DrivingLicenceMaster getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from DrivingLicenceMaster where customerEntityId =" + CustomerId);
        List<DrivingLicenceMaster> drivingLicenceMasters = query.list();
        if (drivingLicenceMasters.size() > 0)
            return drivingLicenceMasters.get(0);
        else
            return null;
    }

    @Override
    public DrivingLicenceMaster save(DrivingLicenceMaster drivingLicenceMaster) {
        sessionFactory.getCurrentSession().save(drivingLicenceMaster);
        return drivingLicenceMaster;
    }

    @Override
    public void update(DrivingLicenceMaster drivingLicenceMaster) {
        sessionFactory.getCurrentSession().update(drivingLicenceMaster);
    }

    @Override
    public void delete(DrivingLicenceMaster drivingLicenceMaster) {
        sessionFactory.getCurrentSession().delete(drivingLicenceMaster);
    }

    @Override
    public List<DrivingLicenceMaster> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from DrivingLicenceMaster");
        List<DrivingLicenceMaster> drivingLicenceMasters = query.list();
        return drivingLicenceMasters;
    }
}
