package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.karza.kyc.dao.ElectricityBoardDao;
import com.karza.kyc.model.ElectricityBoard;

@Repository
public class ElectricityBoardDaoImpl implements ElectricityBoardDao {
	@Autowired
	private SessionFactory sessionFactory;


	@Override
	public ElectricityBoard get(Long id) {
		return (ElectricityBoard) sessionFactory.getCurrentSession().get(ElectricityBoard.class,id);
	}

	@Override
	public List<ElectricityBoard> findAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("from ElectricityBoard");
		List<ElectricityBoard> boards = query.list();
		return boards;
	}

	@Override
	public void save(ElectricityBoard board) {
		sessionFactory.getCurrentSession().save(board);
		
	}

	@Override
	public String getBoardName(String electricityBoardCode) {
		String hql = "select electricityBoardName  from ElectricityBoard where electBoardId ="+"'"+electricityBoardCode+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<ElectricityBoard> boardName = query.list();
		return boardName.toString();
			
	}
	
	
	

}
