package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.AadharCardDao;
import com.karza.kyc.model.AadharCard;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
@Repository
public class AadharCardDaoImpl implements AadharCardDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public AadharCard get(Long id) {
        return (AadharCard) sessionFactory.getCurrentSession().get(AadharCard.class, id);

    }

    @Override
    public AadharCard getByCustomerId(Long CustomerId) {

        Query query = sessionFactory.getCurrentSession().createQuery("from AadharCard where customerEntityId =" + CustomerId);
        List<AadharCard> aadharCardList = query.list();
        if (aadharCardList.size() > 0)
            return aadharCardList.get(0);
        else
            return null;
    }

    @Override
    public void save(AadharCard aadharCard) {
        sessionFactory.getCurrentSession().merge(aadharCard);
    }

    @Override
    public void update(AadharCard aadharCard) {
        sessionFactory.getCurrentSession().update(aadharCard);
    }

    @Override
    public void delete(AadharCard aadharCard) {
        sessionFactory.getCurrentSession().delete(aadharCard);
    }

    @Override
    public List<AadharCard> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from AadharCard");
        List<AadharCard> aadharCards = query.list();
        return aadharCards;
    }
}
