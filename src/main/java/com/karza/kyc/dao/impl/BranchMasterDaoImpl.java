package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.BranchMasterDao;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.LocationMaster;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
@Repository
public class BranchMasterDaoImpl implements BranchMasterDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public BranchMaster get(Long id) {
        return (BranchMaster) sessionFactory.getCurrentSession().get(BranchMaster.class, id);
    }

    @Override
    public BranchMaster getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from BranchMaster where customerEntityId =" + CustomerId);
        List<BranchMaster> branchMasterList = query.list();
        if (branchMasterList.size() > 0)
            return branchMasterList.get(0);
        else
            return null;
    }

    @Override
    public void save(BranchMaster branchMaster) {
        sessionFactory.getCurrentSession().merge(branchMaster);
    }

    @Override
    public void update(BranchMaster branchMaster) {
        sessionFactory.getCurrentSession().update(branchMaster);
    }

    @Override
    public void delete(BranchMaster branchMaster) {
        sessionFactory.getCurrentSession().delete(branchMaster);
    }

    @Override
    public List<BranchMaster> findAll() {
        String hql = "select * from BranchMaster";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List<BranchMaster> branchMasters = query.list();
        return branchMasters;

    }

    @Transactional
    @Override
    public List<BranchMaster> findByUserLocation(List<LocationMaster> locationMasters) {
        List<BranchMaster> branchMasters = new ArrayList<>();
        for (int i = 0; i < locationMasters.size(); i++) {
            Query query = sessionFactory.getCurrentSession().createQuery("from BranchMaster where location=" + locationMasters.get(i).getId());
            List<BranchMaster> branchMasterList = query.list();
            if (branchMasterList != null) {
                branchMasters.addAll(branchMasterList);
            }
        }

        return branchMasters;
    }

    @Transactional
    @Override
    public List<BranchMaster> findByCustomerId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from BranchMaster where customerMasterId=" + customerMasterId);
        List<BranchMaster> branchMasters = query.list();
        return branchMasters;
    }

    @Transactional
    @Override
    public BranchMaster createBranch(BranchMaster newBranchMaster) {
        return (BranchMaster) sessionFactory.getCurrentSession().merge(newBranchMaster);
    }

    @Transactional
    @Override
    public Integer getUserCountByCustomerId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(*) from BranchMaster where customerMasterId=" + customerMasterId);
        Integer user_count = Integer.parseInt(query.uniqueResult().toString());
        return user_count;
    }

    @Transactional
    @Override
    public List<BranchMaster> getAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from BranchMaster");
        List<BranchMaster> branchMasters = query.list();
        return branchMasters;
    }
}
