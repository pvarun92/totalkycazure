package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.LocationMasterDao;
import com.karza.kyc.model.LocationMaster;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
@Repository
public class LocationMasterDaoImpl implements LocationMasterDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public LocationMaster get(Long id) {
        return (LocationMaster) sessionFactory.getCurrentSession().get(LocationMaster.class, id);
    }

    @Override
    public void save(LocationMaster locationMaster) {
        sessionFactory.getCurrentSession().merge(locationMaster);
    }

    @Override
    public LocationMaster saveAndGet(LocationMaster locationMaster) {
        return (LocationMaster) sessionFactory.getCurrentSession().merge(locationMaster);
    }

    @Transactional
    @Override
    public LocationMaster getParentLocation(Long customerMasterId) {
        Long l = Long.parseLong("0");
        Query query = sessionFactory.getCurrentSession().createQuery("from LocationMaster where customerMasterId=" + customerMasterId + " and parentLocation=" + l);
        LocationMaster locationMaster = (LocationMaster) query.uniqueResult();
        return locationMaster;
    }

    @Override
    public void update(LocationMaster locationMaster) {
        sessionFactory.getCurrentSession().update(locationMaster);
    }

    @Override
    public void delete(LocationMaster locationMaster) {
        sessionFactory.getCurrentSession().delete(locationMaster);
    }

    @Override
    public List<LocationMaster> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from LocationMaster");
        List<LocationMaster> locationMasters = query.list();
        return locationMasters;
    }

    @Override
    public List<LocationMaster> findByCustomerId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from LocationMaster where customerMasterId=" + customerMasterId);
        List<LocationMaster> userMasters = query.list();
        return userMasters;
    }

    @Transactional
    @Override
    public List<LocationMaster> getChildLocations(Long locationId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from LocationMaster where parentLocation=" + locationId + "or id=" + locationId);
        List<LocationMaster> locationMasters = query.list();
        for (int i = 0; i < locationMasters.size(); i++) {
            List<LocationMaster> locationMasterList = getChildLocationsRec(locationMasters.get(i).getId());
            if (locationMasterList != null) {
                for (int j = 0; j < locationMasterList.size(); j++) {
                    LocationMaster locationMaster = locationMasterList.get(j);
                    if (!locationMasters.contains(locationMaster)) {
                        locationMasters.add(locationMaster);
                    }
                }
            }
        }
        return locationMasters;
    }

    public List<LocationMaster> getChildLocationsRec(Long locationId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from LocationMaster where parentLocation=" + locationId);
        List<LocationMaster> locationMasters = query.list();
        return locationMasters;
    }

    @Transactional
    @Override
    public Integer getUserCountByCustomerId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(*) from LocationMaster where customerMasterId=" + customerMasterId);
        Integer user_count = Integer.parseInt(query.uniqueResult().toString());
        return user_count;
    }
}
