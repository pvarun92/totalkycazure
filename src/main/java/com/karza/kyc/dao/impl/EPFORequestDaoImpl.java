package com.karza.kyc.dao.impl;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.karza.kyc.dao.EPFORequestDao;
import com.karza.kyc.model.EPFORequest;


@Repository
public class EPFORequestDaoImpl implements EPFORequestDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public EPFORequest get(Long id) {
		return (EPFORequest) sessionFactory.getCurrentSession().get(EPFORequest.class, id);
	}
	
	@Override
	public EPFORequest Save(EPFORequest epfo) {
		sessionFactory.getCurrentSession().save(epfo);
		return epfo;
	}

	@Override
	public void update(EPFORequest epfo) {
		sessionFactory.getCurrentSession().update(epfo);
	}

	@Override
	public void delete(EPFORequest epfo) {
		sessionFactory.getCurrentSession().delete(epfo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EPFORequest> findAll() {
		Query query = sessionFactory.getCurrentSession().createQuery(" from EPFORequest");
	
		List<EPFORequest> epfos = query.list();
		return epfos;
	}
	@SuppressWarnings("unchecked")
	@Override
	public EPFORequest getByCustomerID(Long customerID) {
		Query query = sessionFactory.getCurrentSession().createQuery(" from EPFORequest where customerEntityId = "+customerID);
		List<EPFORequest> epfos = query.list();
		if(epfos.size() > 0)
			return epfos.get(0);
		else
		return null;
	}

}
