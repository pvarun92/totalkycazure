package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.karza.kyc.dao.ESICDao;
import com.karza.kyc.model.ESIC;

@Repository
public class ESICDaoImpl implements ESICDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public ESIC get(Long id) {
		return (ESIC) sessionFactory.getCurrentSession().get(ESIC.class, id);
	}

	@Override
	public ESIC save(ESIC esic) {
		sessionFactory.getCurrentSession().save(esic);
		return esic;
	}

	@Override
	public void update(ESIC esic) {
		sessionFactory.getCurrentSession().update(esic);
	}

	@Override
	public void delete(ESIC esic) {
		sessionFactory.getCurrentSession().delete(esic);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ESIC> findAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("from ESIC");
		List<ESIC> esics = query.list();
		return esics;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ESIC getByCustomerId(Long customerId) {
		Query query = sessionFactory.getCurrentSession().createQuery("from ESIC where customerEntityId = "+customerId);
		List<ESIC> esicNumber = query.list();
		if(esicNumber.size() > 0)
			return esicNumber.get(0);
		else
			return null;
	}

}
