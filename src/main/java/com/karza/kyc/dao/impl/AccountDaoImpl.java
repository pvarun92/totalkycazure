package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.AccountDao;
import com.karza.kyc.model.Account;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon Software on 4/14/2016.
 */
@Repository
public class AccountDaoImpl implements AccountDao {
    @Autowired
    private SessionFactory sessionFactory;

    public Account get(Long id) {
        return (Account) sessionFactory.getCurrentSession().get(Account.class, id);
    }

    public void delete(Account accountUser) {
        sessionFactory.getCurrentSession().delete(accountUser);
    }

    @SuppressWarnings("unchecked")
    public List<Account> findAll() {
        return sessionFactory.getCurrentSession().createQuery(
                "FROM Account ORDER BY id")
                .list();
    }

    public void save(Account accountUser) {
        sessionFactory.getCurrentSession().merge(accountUser);
    }

    public void update(Account accountUser) {
        sessionFactory.getCurrentSession().update(accountUser);
    }

    @Override
    public Account findByAccountUserName(String accountUserName) {
        return (Account) sessionFactory.getCurrentSession().createQuery(
                "FROM Account u WHERE u.name = :username ")
                .setString("username", accountUserName).uniqueResult();
    }
}
