package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.DrivingLicenceCOVDao;
import com.karza.kyc.model.DrivingLicenceCOV;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Repository
public class DrivingLicenceCOVDaoImpl implements DrivingLicenceCOVDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public DrivingLicenceCOV get(Long id) {
        return (DrivingLicenceCOV) sessionFactory.getCurrentSession().get(DrivingLicenceCOV.class, id);
    }

    @Override
    public List<DrivingLicenceCOV> getByLicence(Long dlId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from DrivingLicenceCOV where DrivingLicenceMasterId =" + dlId);
        List<DrivingLicenceCOV> drivingLicenceCOVs = query.list();
        if (drivingLicenceCOVs.size() > 0)
            return drivingLicenceCOVs;
        else
            return null;
    }

    @Override
    public void save(DrivingLicenceCOV drivingLicenceCOV) {
        sessionFactory.getCurrentSession().save(drivingLicenceCOV);
    }

    @Override
    public void update(DrivingLicenceCOV drivingLicenceCOV) {
        sessionFactory.getCurrentSession().update(drivingLicenceCOV);
    }

    @Override
    public void delete(DrivingLicenceCOV drivingLicenceCOV) {
        sessionFactory.getCurrentSession().delete(drivingLicenceCOV);
    }

    @Override
    public List<DrivingLicenceCOV> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from DrivingLicenceCOV");
        List<DrivingLicenceCOV> drivingLicenceCOVs = query.list();
        return drivingLicenceCOVs;
    }
}
