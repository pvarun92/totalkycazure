package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.PanCardDao;
import com.karza.kyc.model.PanCard;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Repository
public class PanCardDaoImpl implements PanCardDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public PanCard get(Long id) {
        return (PanCard) sessionFactory.getCurrentSession().get(PanCard.class, id);
    }

    @Override
    public PanCard getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PanCard where customerEntityId =" + CustomerId);
        List<PanCard> panCardList = query.list();
        if (panCardList.size() > 0)
            return panCardList.get(0);
        else
            return null;
    }

    @Override
    public void save(PanCard panCard) {	
        sessionFactory.getCurrentSession().save(panCard);
    }

    @Override
    public void update(PanCard panCard) {
        sessionFactory.getCurrentSession().update(panCard);
    }

    @Override
    public void delete(PanCard panCard) {
        sessionFactory.getCurrentSession().delete(panCard);
    }

    @Override
    public List<PanCard> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from PanCard");
        List<PanCard> panCards = query.list();
        return panCards;
    }
}
