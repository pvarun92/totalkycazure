package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.UserSessionDetailDao;
import com.karza.kyc.model.UserSessionDetail;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class UserSessionDetailDaoImpl implements UserSessionDetailDao {
    @Autowired
    private SessionFactory sessionFactory;
	static final Logger LOG = LoggerFactory.getLogger(UserSessionDetailDaoImpl.class);

    @Override
    public Long save(UserSessionDetail userSessionDetail) {
        try {
        	LOG.warn("Saving the UserSession Detail Data.");
            Long userId = (Long) sessionFactory.getCurrentSession().save(userSessionDetail);
            return userId;
        } catch (Exception e) {
            System.out.println("error" + e);
            return null;
        }
    }

    @Transactional
    @Override
    public void update(UserSessionDetail userSessionDetail) {
        sessionFactory.getCurrentSession().update(userSessionDetail);
    }

    @Override
    public void delete(UserSessionDetail userSessionDetail) {
        sessionFactory.getCurrentSession().delete(userSessionDetail);
    }

    @Transactional
    @Override
    public Integer getLoggedInUserCount(Long userId) {
        try {
            Date newDateObj = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(newDateObj);
            c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
            c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) - 15);
            c.set(Calendar.SECOND, c.get(Calendar.SECOND));
            newDateObj = c.getTime();
            Query query = sessionFactory.getCurrentSession().createQuery("from UserSessionDetail where userId='" + userId + "'and userStatus='Active' and updatedAt > '" + newDateObj + "'");
            List<UserSessionDetail> userSessionDetailList = query.list();
            Integer noOfActiveSession = query.list().size();
            return noOfActiveSession;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    @Override
    public UserSessionDetail getBySessionId(String sessionId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserSessionDetail where sessionId='" + sessionId + "'");
        UserSessionDetail userSessionDetail = (UserSessionDetail) query.uniqueResult();
        return userSessionDetail;
    }

    @Override
    public UserSessionDetail getByUserId(Long userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserSessionDetail where userId='" + userId + "'");
        UserSessionDetail userSessionDetail = (UserSessionDetail) query.uniqueResult();
        return userSessionDetail;
    }

    @Override
    public UserSessionDetail getLastLoggedInUserById(Long userId) {
        String hql = "from UserSessionDetail where userId='" + userId + "'and updatedAt = (select max(updatedAt) from UserSessionDetail where userId = '" + userId + "')";
        String ss = "from UserSessionDetail where userId='" + userId + "'and updatedAt = (select max(updatedAt) from UserSessionDetail where userId = '" + userId + "')";
        try {
            Query query = sessionFactory.getCurrentSession().createQuery(hql);
            UserSessionDetail userSessionDetail = (UserSessionDetail) query.uniqueResult();
            return userSessionDetail;
        } catch (Exception e) {
            return null;
        }
    }
}
