package com.karza.kyc.dao.impl;

/**
 * Created by Admin on 7/16/2016.
 */

import com.karza.kyc.dao.ItrDao;
import com.karza.kyc.model.Itr;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItrDaoImpl implements ItrDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Itr get(Long id) {
        return (Itr) sessionFactory.getCurrentSession().get(Itr.class, id);
    }

    @Override
    public List<Itr> getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Itr where customerEntityId =" + CustomerId);
        List<Itr> itrList = query.list();
        if (itrList.size() > 0)
            return itrList;
        else
            return null;
    }

    @Override
    public void save(Itr itr) {
        try {
            sessionFactory.getCurrentSession().merge(itr);
        } catch (Exception e) {
            System.out.println("error" + e);
        }
    }

    @Override
    public void update(Itr itr) {
        sessionFactory.getCurrentSession().update(itr);
    }

    @Override
    public void delete(Itr itr) {
        sessionFactory.getCurrentSession().delete(itr);
    }

    @Override
    public List<Itr> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Itr");
        List<Itr> itrList = query.list();
        return itrList;
    }

    @Override
    public List<Itr> getByPanId(Long panId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Itr where panId =" + panId);
        List<Itr> itrList = query.list();
        if (itrList.size() > 0)
            return itrList;
        else
            return null;
    }
}
