package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.DINDao;
import com.karza.kyc.model.DIN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Repository
public class DINDaoImpl implements DINDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public DIN get(Long id) {
        return (DIN) sessionFactory.getCurrentSession().get(DIN.class, id);
    }

    @Override
    public void save(DIN din) {
        sessionFactory.getCurrentSession().merge(din);
    }

    @Override
    public void delete(DIN din) {
        sessionFactory.getCurrentSession().delete(din);
    }

    @Override
    public List<DIN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from DIN");
        List<DIN> din = query.list();
        return din;

    }
}
