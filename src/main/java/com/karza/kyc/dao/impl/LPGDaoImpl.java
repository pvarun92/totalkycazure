package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.karza.kyc.dao.LPGDao;
import com.karza.kyc.model.LPG;

@Repository
public class LPGDaoImpl implements LPGDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public LPG get(Long id) {
		return (LPG) sessionFactory.getCurrentSession().get(LPG.class, id);
	}

	@Override
	public LPG save(LPG lpg) {
		sessionFactory.getCurrentSession().save(lpg);
		return lpg;
	}

	@Override
	public void update(LPG lpg) {

		sessionFactory.getCurrentSession().update(lpg);
	}

	@Override
	public void delete(LPG lpg) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(lpg);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<LPG> findAll() {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from LPG");
		List<LPG> lpgs = query.list();
		return lpgs;
	}

	@Override
	@SuppressWarnings("unchecked")
	public LPG getByCustomerId(Long CustomerId) {
		Query query = sessionFactory.getCurrentSession().createQuery("from LPG where customerEntityId = "+CustomerId);
		List<LPG> lpgNumber = query.list();
		if(lpgNumber.size() > 0)
			return lpgNumber.get(0);
		else
		return null;
	}

}
