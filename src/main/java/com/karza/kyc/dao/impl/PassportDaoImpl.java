package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.PassportDao;
import com.karza.kyc.model.Passport;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Repository
public class PassportDaoImpl implements PassportDao {


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Passport get(Long id) {
        return (Passport) sessionFactory.getCurrentSession().get(Passport.class, id);
    }

    @Override
    public void save(Passport passport) {
        sessionFactory.getCurrentSession().merge(passport);
    }

    @Override
    public void delete(Passport passport) {
        sessionFactory.getCurrentSession().delete(passport);
    }

    @Override
    public List<Passport> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Passport");
        List<Passport> passPorts = query.list();
        return passPorts;
    }

    @Override
    public void update(Passport passport) {
        sessionFactory.getCurrentSession().update(passport);
    }

    @Override
    public Passport getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Passport where customerEntityId =" + CustomerId);
        List<Passport> passportList = query.list();
        if (passportList.size() > 0)
            return passportList.get(0);
        else
            return null;
    }
}
