package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.FCRNDao;
import com.karza.kyc.model.FCRN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Repository
public class FCRNDaoImpl implements FCRNDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public FCRN get(Long id) {
        return (FCRN) sessionFactory.getCurrentSession().get(FCRN.class, id);
    }

    @Override
    public void save(FCRN FCRN) {
        sessionFactory.getCurrentSession().merge(FCRN);
    }

    @Override
    public void update(FCRN FCRN) {
        sessionFactory.getCurrentSession().update(FCRN);
    }

    @Override
    public void delete(FCRN FCRN) {
        sessionFactory.getCurrentSession().delete(FCRN);
    }

    @Override
    public List<FCRN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from FCRN");
        List<FCRN> FCRNs = query.list();
        return FCRNs;
    }

    @Override
    public FCRN getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from FCRN where customerEntityId =" + CustomerId);
        List<FCRN> FCRNList = query.list();
        if (FCRNList.size() > 0)
            return FCRNList.get(0);
        else
            return null;
    }


}
