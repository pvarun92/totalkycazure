package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.LLPINDao;
import com.karza.kyc.model.LLPIN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Repository
public class LLPINDaoImpl implements LLPINDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public LLPIN get(Long id) {
        return (LLPIN) sessionFactory.getCurrentSession().get(LLPIN.class, id);
    }

    @Override
    public void save(LLPIN llpin) {
        sessionFactory.getCurrentSession().save(llpin);

    }

    @Override
    public void update(LLPIN llpin) {
        sessionFactory.getCurrentSession().update(llpin);

    }

    @Override
    public void delete(LLPIN llpin) {
        sessionFactory.getCurrentSession().delete(llpin);
    }

    @Override
    public List<LLPIN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from LLPIN");
        List<LLPIN> llpins = query.list();
        return llpins;
    }

    @Override
    public LLPIN getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from LLPIN where customerEntityId =" + CustomerId);
        List<LLPIN> llpinList = query.list();
        if (llpinList.size() > 0)
            return llpinList.get(0);
        else
            return null;
    }
}
