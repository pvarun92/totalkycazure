package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.FLLPINDao;
import com.karza.kyc.model.FLLPIN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Repository
public class FLLPINDaoImpl implements FLLPINDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public FLLPIN get(Long id) {
        return (FLLPIN) sessionFactory.getCurrentSession().get(FLLPIN.class, id);
    }

    @Override
    public FLLPIN getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from FLLPIN where customerEntityId =" + CustomerId);
        List<FLLPIN> fllpinList = query.list();
        if (fllpinList.size() > 0)
            return fllpinList.get(0);
        else
            return null;
    }

    @Override
    public void save(FLLPIN fllpin) {
        sessionFactory.getCurrentSession().merge(fllpin);
    }

    @Override
    public void update(FLLPIN fllpin) {
        sessionFactory.getCurrentSession().update(fllpin);
    }

    @Override
    public void delete(FLLPIN fllpin) {
        sessionFactory.getCurrentSession().delete(fllpin);
    }

    @Override
    public List<FLLPIN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from FLLPIN");
        List<FLLPIN> fllpins = query.list();
        return fllpins;
    }
}
