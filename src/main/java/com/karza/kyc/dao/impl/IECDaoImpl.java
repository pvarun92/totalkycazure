package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.IECDao;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.IEC_Branch;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Repository
public class IECDaoImpl implements IECDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override

    public IEC get(Long id) {
        return (IEC) sessionFactory.getCurrentSession().get(IEC.class, id);
    }

    @Override
    public void save(IEC iec) {
        sessionFactory.getCurrentSession().save(iec);
    }

    @Override
    public void update(IEC iec) {
        sessionFactory.getCurrentSession().update(iec);
    }

    @Override
    public void delete(IEC iec) {
        sessionFactory.getCurrentSession().delete(iec);
    }

    @Override
    public List<IEC> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from IEC");
        List<IEC> iecs = query.list();
        return iecs;
    }

    @Override
    public IEC getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from IEC where customerEntityId =" + CustomerId);
        List<IEC> iecList = query.list();
        if (iecList.size() > 0)
            return iecList.get(0);
        else
            return null;
    }

    @Override
    public void saveIECBranch(IEC_Branch iec_branch) {
        sessionFactory.getCurrentSession().merge(iec_branch);
    }

    @Transactional
    @Override
    public List<IEC_Branch> getBranchByIECID(Long iecID) {
        Query query = sessionFactory.getCurrentSession().createQuery("from IEC_Branch where iecId =" + iecID);
        List<IEC_Branch> iecBranchList = query.list();
        return iecBranchList;
    }

    @Override
    public void deleteBranch(IEC_Branch iec_branch) {
        sessionFactory.getCurrentSession().delete(iec_branch);
    }
}
