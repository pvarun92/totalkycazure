package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.VoterIDDao;
import com.karza.kyc.model.VoterFamily;
import com.karza.kyc.model.VoterID;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Repository
public class VoterIDDaoImpl implements VoterIDDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public VoterID get(Long id) {
        return (VoterID) sessionFactory.getCurrentSession().get(VoterID.class, id);
    }

    @Override
    public VoterID getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VoterID where customerEntityId =" + CustomerId);
        List<VoterID> voterIDList = query.list();
        if (voterIDList.size() > 0)
            return voterIDList.get(0);
        else
            return null;
    }

    @Override
    public void save(VoterID voterID) {
        sessionFactory.getCurrentSession().save(voterID);

    }

    @Override
    public void update(VoterID voterID) {
        sessionFactory.getCurrentSession().update(voterID);
    }

    @Override
    public void delete(VoterID voterID) {
        sessionFactory.getCurrentSession().delete(voterID);
    }

    @Override
    public List<VoterID> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from VoterID");
        List<VoterID> voterIDs = query.list();
        return voterIDs;
    }

    @Override
    public List<VoterFamily> getFamilyByVoterID(String voterID) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VoterFamily where voterId =" + voterID);
        List<VoterFamily> voterFamilyList = query.list();
        return voterFamilyList;
    }

    @Override
    public void deleteFamily(VoterFamily voterFamily) {
        sessionFactory.getCurrentSession().delete(voterFamily);
    }

    @Override
    public void saveVoterFamily(VoterFamily voterFamily) {
        sessionFactory.getCurrentSession().merge(voterFamily);
    }
}
