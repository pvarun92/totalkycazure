package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.ServiceTaxDao;
import com.karza.kyc.model.ServiceTax;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Repository
public class ServiceTaxDaoImpl implements ServiceTaxDao {


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ServiceTax get(Long id) {
        return (ServiceTax) sessionFactory.getCurrentSession().get(ServiceTax.class, id);
    }

    @Override
    public void save(ServiceTax serviceTax) {
        sessionFactory.getCurrentSession().save(serviceTax);
    }

    @Override
    public void update(ServiceTax serviceTax) {
        sessionFactory.getCurrentSession().update(serviceTax);
    }

    @Override
    public void delete(ServiceTax serviceTax) {
        sessionFactory.getCurrentSession().delete(serviceTax);
    }

    @Override
    public List<ServiceTax> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ServiceTax");
        List<ServiceTax> serviceTaxes = query.list();
        return serviceTaxes;
    }

    @Override
    public ServiceTax getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ServiceTax where customerEntityId =" + CustomerId);
        List<ServiceTax> serviceTaxListList = query.list();
        if (serviceTaxListList.size() > 0)
            return serviceTaxListList.get(0);
        else
            return null;
    }
}
