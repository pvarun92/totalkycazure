package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.TINDao;
import com.karza.kyc.model.TIN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */

@Repository
public class TINDaoImpl implements TINDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override

    public TIN get(Long id) {
        return (TIN) sessionFactory.getCurrentSession().get(TIN.class, id);
    }

    @Override
    public void save(TIN tin) {
        sessionFactory.getCurrentSession().merge(tin);
    }

    @Override
    public void delete(TIN tin) {
        sessionFactory.getCurrentSession().delete(tin);

    }

    @Override
    public List<TIN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from TIN");
        List<TIN> tins = query.list();
        return tins;
    }
}
