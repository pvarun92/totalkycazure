package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.ProfessionalTaxDao;
import com.karza.kyc.model.ProfessionalTax;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
@Repository
public class ProfessionalTaxDaoImpl implements ProfessionalTaxDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ProfessionalTax get(Long id) {
        return (ProfessionalTax) sessionFactory.getCurrentSession().get(ProfessionalTax.class, id);
    }

    @Override
    public ProfessionalTax getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProfessionalTax where customerEntityId =" + CustomerId);
        List<ProfessionalTax> professionalTaxList = query.list();
        if (professionalTaxList.size() > 0)
            return professionalTaxList.get(0);
        else
            return null;
    }

    @Override
    public ProfessionalTax save(ProfessionalTax professionalTax) {
        return (ProfessionalTax) sessionFactory.getCurrentSession().merge(professionalTax);
    }

    @Override
    public void update(ProfessionalTax professionalTax) {
        sessionFactory.getCurrentSession().update(professionalTax);
    }

    @Override
    public void delete(ProfessionalTax professionalTax) {
        sessionFactory.getCurrentSession().delete(professionalTax);
    }

    @Override
    public List<ProfessionalTax> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProfessionalTax ");
        List<ProfessionalTax> professionalTaxes = query.list();
        return professionalTaxes;
    }
}
