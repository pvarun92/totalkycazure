package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.karza.kyc.dao.ElectricityDao;
import com.karza.kyc.model.Electricity;

/**
 * Created by Varun_Prakash on 09-01-2017.
 */
@Repository
public class ElectricityDaoImpl implements ElectricityDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Electricity get(Long id) {
		// TODO Auto-generated method stub
		return (Electricity) sessionFactory.getCurrentSession().get(Electricity.class, id);
	}

	@Override
	public Electricity save(Electricity electricity) {
		sessionFactory.getCurrentSession().save(electricity);
		return electricity; 

	}

	@Override
	public void update(Electricity electricity) {
		/*Session currentSession = sessionFactory.getCurrentSession();
		currentSession.update(electricity);*/
	 sessionFactory.getCurrentSession().update(electricity);
	}

	@Override
	public void delete(Electricity electricity) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(electricity);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Electricity> findAll() {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().createQuery("from Electricity");
		
		List<Electricity> electricities = query.list();
		return electricities;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Electricity getByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession()
				.createQuery("from Electricity where customerEntityId = " + customerId);
	
		List<Electricity> electricityList = query.list();
		if (electricityList.size() > 0)
			return electricityList.get(0);
		else
			return null;
	}

}
