package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.AccountUserDao;
import com.karza.kyc.model.AccountUser;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/25/2016.
 */
@Repository
public class AccountUserDaoImpl implements AccountUserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public AccountUser get(Long id) {
        return (AccountUser) sessionFactory.getCurrentSession().get(AccountUser.class, id);
    }

    @Override
    public void save(AccountUser accountUser) {
        sessionFactory.getCurrentSession().merge(accountUser);
    }

    @Override
    public void update(AccountUser accountUser) {
        sessionFactory.getCurrentSession().update(accountUser);
    }

    @Override
    public void delete(AccountUser accountUser) {
        sessionFactory.getCurrentSession().delete(accountUser);
    }

    @Override
    public List<AccountUser> findAll() {
        return sessionFactory.getCurrentSession().createQuery(
                "FROM AccountUser ORDER BY id")
                .list();
    }

    @Override
    public List<AccountUser> findByAccount(Long accountID) {
        return sessionFactory.getCurrentSession().createQuery(
                "FROM AccountUser u WHERE u.accountId = :accountId ")
                .setLong("accountId", accountID)
                .list();
    }

    @Override
    public AccountUser findByAccountUserName(String accountUserName) {
        return (AccountUser) sessionFactory.getCurrentSession().createQuery(
                "FROM AccountUser u WHERE u.name = :username ")
                .setString("username", accountUserName).uniqueResult();
    }
}
