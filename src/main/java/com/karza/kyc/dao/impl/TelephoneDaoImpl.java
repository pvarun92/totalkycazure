package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.karza.kyc.dao.TelephoneDao;
import com.karza.kyc.model.Telephone;

@Repository
public class TelephoneDaoImpl implements TelephoneDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Telephone get(Long id) {
		return (Telephone) sessionFactory.getCurrentSession().get(Telephone.class, id);
	}

	@Override
	public Telephone save(Telephone telephone) {
		sessionFactory.getCurrentSession().save(telephone);
		return telephone;
	}

	@Override
	public void update(Telephone telephone) {
		sessionFactory.getCurrentSession().update(telephone);
	}

	@Override
	public void Delete(Telephone telephone) {
		sessionFactory.getCurrentSession().delete(telephone);
	}

	@Override
	public List<Telephone> findAll() {
	Query query = sessionFactory.getCurrentSession().createQuery("from Telephone");
	@SuppressWarnings("unchecked")
	List<Telephone> telephones = query.list();
	return telephones;
	}

	@Override
	public Telephone getByCustomerId(Long customerId) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Telephone where customerEntityId = "+customerId);
		@SuppressWarnings("unchecked")
		List<Telephone> telephonesNo = query.list();
		if(telephonesNo.size() > 0)
			return telephonesNo.get(0);
		else
			return null;
		
	}

}
