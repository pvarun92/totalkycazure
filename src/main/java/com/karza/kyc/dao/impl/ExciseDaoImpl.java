package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.ExciseDao;
import com.karza.kyc.model.Excise;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */


@Repository
public class ExciseDaoImpl implements ExciseDao {


    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public Excise get(Long id) {
        return (Excise) sessionFactory.getCurrentSession().get(Excise.class, id);
    }

    @Override
    public void save(Excise excise) {
        sessionFactory.getCurrentSession().save(excise);
    }

    @Override
    public void delete(Excise excise) {
        sessionFactory.getCurrentSession().delete(excise);
    }

    @Override
    public List<Excise> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Excise");
        List<Excise> excises = query.list();
        return excises;

    }

    @Override
    public void update(Excise excise) {
        sessionFactory.getCurrentSession().update(excise);
    }

    @Override
    public Excise getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Excise where customerEntityId =" + CustomerId);
        List<Excise> exciseList = query.list();
        if (exciseList.size() > 0)
            return exciseList.get(0);
        else
            return null;
    }
}
