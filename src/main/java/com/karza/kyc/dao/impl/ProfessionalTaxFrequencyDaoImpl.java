package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.ProfessionalTaxFrequencyDao;
import com.karza.kyc.model.ProfessionalTaxFrequency;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
@Repository
public class ProfessionalTaxFrequencyDaoImpl implements ProfessionalTaxFrequencyDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ProfessionalTaxFrequency get(Long id) {
        return (ProfessionalTaxFrequency) sessionFactory.getCurrentSession().get(ProfessionalTaxFrequency.class, id);
    }

    @Override
    public List<ProfessionalTaxFrequency> getByProfessionalTaxId(Long professionalTaxId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProfessionalTaxFrequency where professionalTaxId =" + professionalTaxId);
        List<ProfessionalTaxFrequency> professionalTaxFrequencyList = query.list();
        if (professionalTaxFrequencyList.size() > 0)
            return professionalTaxFrequencyList;
        else
            return null;
    }

    @Override
    public void save(ProfessionalTaxFrequency professionalTaxFrequency) {
        sessionFactory.getCurrentSession().merge(professionalTaxFrequency);
    }

    @Override
    public void update(ProfessionalTaxFrequency professionalTaxFrequency) {
        sessionFactory.getCurrentSession().update(professionalTaxFrequency);
    }

    @Override
    public void delete(ProfessionalTaxFrequency professionalTaxFrequency) {
        sessionFactory.getCurrentSession().delete(professionalTaxFrequency);
    }

    @Override
    public List<ProfessionalTaxFrequency> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProfessionalTaxFrequency ");
        List<ProfessionalTaxFrequency> professionalTaxFrequencies = query.list();
        return professionalTaxFrequencies;
    }
}
