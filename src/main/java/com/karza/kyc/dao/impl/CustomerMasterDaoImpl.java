package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.CustomerMasterDao;
import com.karza.kyc.model.CustomerMaster;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
@Repository
public class CustomerMasterDaoImpl implements CustomerMasterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CustomerMaster get(Long id) {
        return (CustomerMaster) sessionFactory.getCurrentSession().get(CustomerMaster.class, id);
    }

    @Override
    public CustomerMaster getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerMaster where customerEntityId =" + CustomerId);
        List<CustomerMaster> customerMasterList = query.list();
        if (customerMasterList.size() > 0)
            return customerMasterList.get(0);
        else
            return null;
    }

    @Override
    public CustomerMaster save(CustomerMaster customerMaster) {
        return (CustomerMaster) sessionFactory.getCurrentSession().merge(customerMaster);

    }

    @Override
    public void update(CustomerMaster customerMaster) {
        sessionFactory.getCurrentSession().update(customerMaster);
    }

    @Override
    public void delete(CustomerMaster customerMaster) {
        sessionFactory.getCurrentSession().delete(customerMaster);
    }

    @Override
    public List<CustomerMaster> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery(" from CustomerMaster where id !=" + 1000);
        List<CustomerMaster> customerCards = query.list();
        return customerCards;
    }

   
    @Override
    public List<CustomerMaster> getAll() {
        Query query = sessionFactory.getCurrentSession().createQuery(" from CustomerMaster where id !=" + 1000);
        List<CustomerMaster> customerDetails = query.list();
        return customerDetails;
    }

    @Override
    public List<CustomerMaster> findAllNew() {
        String hql = "SELECT COUNT(um.customer_master_id), cm.id,cm.customer_name,cm.customer_type FROM user_master AS um RIGHT JOIN customer_master AS cm ON um.customer_master_id = cm.id where cm.id !=+1000 GROUP BY cm.id";
        Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
        List<CustomerMaster> customerCards = query.list();
        return customerCards;
    }

    @Override
    public Integer getMaxAccountNumber() {

        String hql = "select max(accountNumber) from CustomerMaster";
        Query query1 = sessionFactory.getCurrentSession().createQuery(hql);
        Integer maxAccountNumber = (Integer) query1.uniqueResult();
        return maxAccountNumber;
    }

    @Override
    public String changeEmailDomain(Long customer_id, String old_domain, String new_domain) {
        String message = "";
        try {
            Query query1 = sessionFactory.getCurrentSession().createSQLQuery("UPDATE customer_master SET email = REPLACE(email, '" + old_domain + "' , '" + new_domain + "') WHERE id = " + customer_id);
            int a = query1.executeUpdate();
            Query query2 = sessionFactory.getCurrentSession().createSQLQuery("UPDATE user_master SET email = REPLACE(email, '" + old_domain + "' , '" + new_domain + "') WHERE unique_id LIKE '" + customer_id + "%' ");
            a = a + query2.executeUpdate();
            message = "" + a + " records are updated successful";
        } catch (Exception e) {
            message = "error in record update";
        }
        return message;
    }
}
