package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.CSTDao;
import com.karza.kyc.model.CST;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
@Repository
public class CSTDaoImpl implements CSTDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CST get(Long id) {
        return (CST) sessionFactory.getCurrentSession().get(CST.class, id);
    }

    @Override
    public CST getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CST where customerEntityId =" + CustomerId);
        List<CST> CSTList = query.list();
        if (CSTList.size() > 0)
            return CSTList.get(0);
        else
            return null;
    }

    @Override
    public CST save(CST cst) {
        return (CST) sessionFactory.getCurrentSession().merge(cst);
    }

    @Override
    public void update(CST cst) {
        sessionFactory.getCurrentSession().update(cst);
    }

    @Override
    public void delete(CST cst) {
        sessionFactory.getCurrentSession().delete(cst);
    }

    @Override
    public List<CST> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CST ");
        List<CST> csts = query.list();
        return csts;
    }
}
