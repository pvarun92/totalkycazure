package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.SubscriptionMasterDao;
import com.karza.kyc.model.SubscriptionMaster;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
@Repository
public class SubscriptionMasterDaoImpl implements SubscriptionMasterDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SubscriptionMaster get(Long id) {
        return (SubscriptionMaster) sessionFactory.getCurrentSession().get(SubscriptionMaster.class, id);
    }

    @Override
    public SubscriptionMaster getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SubscriptionMaster where customerEntityId =" + CustomerId);
        List<SubscriptionMaster> subscriptionMasterList = query.list();
        if (subscriptionMasterList.size() > 0)
            return subscriptionMasterList.get(0);
        else
            return null;
    }

    @Override
    public void save(SubscriptionMaster subscriptionMaster) {
        sessionFactory.getCurrentSession().merge(subscriptionMaster);
    }

    @Override
    public void update(SubscriptionMaster subscriptionMaster) {
        sessionFactory.getCurrentSession().update(subscriptionMaster);
    }

    @Override
    public void delete(SubscriptionMaster subscriptionMaster) {
        sessionFactory.getCurrentSession().delete(subscriptionMaster);
    }

    @Override
    public List<SubscriptionMaster> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from SubscriptionMaster");
        List<SubscriptionMaster> subscriptionMasters = query.list();
        return subscriptionMasters;
    }

    @Override
    public List<SubscriptionMaster> findByCustomerId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SubscriptionMaster where customerMasterId=" + customerMasterId);
        List<SubscriptionMaster> subscriptionMasterList = query.list();
        return subscriptionMasterList;
    }

    @Transactional
    @Override
    public List<SubscriptionMaster> getSubscriptionsByCustomerMasterId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from SubscriptionMaster where  customerMasterId =" + customerMasterId);
        List<SubscriptionMaster> subscriptionMasters = query.list();
        return subscriptionMasters;
    }
}