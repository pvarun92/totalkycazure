package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.VATFrequencyDao;
import com.karza.kyc.model.VATFrequency;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
@Repository
public class VATFrequencyDaoImpl implements VATFrequencyDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public VATFrequency get(Long id) {
        return (VATFrequency) sessionFactory.getCurrentSession().get(VATFrequency.class, id);
    }

    @Override
    public List<VATFrequency> getByVatId(Long vatId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VATFrequency where vatId =" + vatId);
        List<VATFrequency> vatFrequencyList = query.list();
        if (vatFrequencyList.size() > 0)
            return vatFrequencyList;
        else
            return null;
    }

    @Override
    public void save(VATFrequency vatFrequency) {
        sessionFactory.getCurrentSession().merge(vatFrequency);
    }

    @Override
    public void update(VATFrequency vatFrequency) {
        sessionFactory.getCurrentSession().update(vatFrequency);
    }

    @Override
    public void delete(VATFrequency vatFrequency) {
        sessionFactory.getCurrentSession().delete(vatFrequency);
    }

    @Override
    public List<VATFrequency> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from VATFrequency ");
        List<VATFrequency> vatFrequencies = query.list();
        return vatFrequencies;
    }
}
