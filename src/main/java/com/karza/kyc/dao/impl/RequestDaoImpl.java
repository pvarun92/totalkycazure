package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.RequestDao;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.UserMaster;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 5:59 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class RequestDaoImpl implements RequestDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Request get(Long id) {
        return (Request) sessionFactory.getCurrentSession().get(Request.class, id);
    }

    @Override
    public Request save(Request request) {
        return (Request) sessionFactory.getCurrentSession().merge(request);
    }

    @Override
    public void update(Request request) {
        sessionFactory.getCurrentSession().update(request);
    }

    @Override
    public void delete(Request request) {
        sessionFactory.getCurrentSession().delete(request);
    }

    @Override
    public List<Request> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Request");
        List<Request> requests = query.list();
        return requests;
    }

    @Transactional
    @Override
    public List<Request> getRequestsByRequestStatus(String request_status) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Request where request_status = '" + request_status + "'");
        List<Request> requests = query.list();
        return requests;
    }

    @Override
    public List<CustomerEntity> getEntitiesByRequestStatus(String request_status) {
        Query query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.request_status = '" + request_status + "' and r.id=c.request_id and c.entity_status='parent'");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Transactional
    @Override
    public List<CustomerEntity> getEntitiesByBranchAndStatus(Long branchId, String request_status, HttpSession httpSession) {
        Query query;
        String worklog = httpSession.getAttribute("workLog").toString();

        UserMaster userMaster = (UserMaster) httpSession.getAttribute("currentUser");
        Long current_user_id = userMaster.getId();
        if (request_status.equals("all")) {
            if (StringUtils.equalsIgnoreCase(worklog, "MY")) {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "and c.userMaster_id=" + current_user_id + "ORDER BY created_date DESC");
            } else {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "ORDER BY created_date DESC");
            }
        } else if (request_status.equals("landingData")) {
            if (StringUtils.equalsIgnoreCase(worklog, "MY")) {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where (r.request_status='in_queue' or r.request_status='in_draft' or r.request_status='submitted') and r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "and c.userMaster_id=" + current_user_id + "ORDER BY created_date DESC");
            } else {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where (r.request_status='in_queue' or r.request_status='in_draft' or r.request_status='submitted') and r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "ORDER BY created_date DESC");
            }
        } else if (request_status.equals("history_count")) {
            request_status = "submitted";
            if (StringUtils.equalsIgnoreCase(worklog, "MY")) {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.request_status = '" + request_status + "' and r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "and c.userMaster_id=" + current_user_id + " and r.isRead=0 ORDER BY created_date DESC");
            } else {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.request_status = '" + request_status + "' and r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + " and r.isRead=0 ORDER BY created_date DESC");
            }
        } else {
            if (StringUtils.equalsIgnoreCase(worklog, "MY")) {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.request_status = '" + request_status + "' and r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "and c.userMaster_id=" + current_user_id + "ORDER BY created_date DESC");
            } else {
                query = sessionFactory.getCurrentSession().createQuery("select c from Request r, CustomerEntity c where r.request_status = '" + request_status + "' and r.id=c.request_id and c.entity_status='parent' and c.branchId=" + branchId + "ORDER BY created_date DESC");
            }
        }
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Transactional
    @Override
    public Integer getRequsetCountByCustomerMasterId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(id) FROM Request WHERE customerMasterId = " + customerMasterId);
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getSubmittedEntitesByCustomerMasterId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(id) FROM Request WHERE customerMasterId = '" + customerMasterId + "' and request_status = 'submitted' ");
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getPendingEntitesByCustomerMasterId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(id) FROM Request WHERE customerMasterId = '" + customerMasterId + "' and request_status = 'in_queue' ");
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getTotalEntitesByCustomerMasterId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT  COUNT(c.id) FROM CustomerEntity c, Request r WHERE r.customerMasterId = '" + customerMasterId + "' AND c.request_id=r.id  ");
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getRequsetCountByUserMasterId(Long userMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(id) FROM Request WHERE userMasterId = " + userMasterId);
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getSubmittedEntitesByUserMasterId(Long userMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(id) FROM Request WHERE userMasterId = '" + userMasterId + "' and request_status = 'submitted' ");
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getPendingEntitesByUserMasterId(Long userMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(id) FROM Request WHERE userMasterId = '" + userMasterId + "' and request_status = 'in_queue' ");
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    @Transactional
    @Override
    public Integer getTotalEntitesByUserMasterId(Long userMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT  COUNT(c.id) FROM CustomerEntity c, Request r WHERE r.userMasterId = '" + userMasterId + "' AND c.request_id=r.id  ");
        Integer count = Integer.parseInt(query.uniqueResult().toString());
        return count;
    }

    /*Function written to get all the requests that has been submitted by all the users*/
    @Transactional
    @Override
    public Integer getRequestCountOfAllUser() {
    	String hql = "select count(*) from Request";
    	  Query query = sessionFactory.getCurrentSession().createQuery(hql);
    	  Long temp = (Long) query.uniqueResult();
    	  Integer count = temp.intValue();
        return count;
    }
    /*Method written to get all the Pending entities of all Users*/

    @Transactional
    @Override
    public Integer getPendingCountofAllUser() {
    	String hql = "select count(*) from Request as r where request_status='in_queue' ";
    	Query query = sessionFactory.getCurrentSession().createQuery(hql);
    	Long temp = (Long) query.uniqueResult();
    	Integer count = temp.intValue();
        return count;
    }
/*Method written to get all the Submitted entities of all the users*/

    @Transactional
    @Override
    public Integer getSubmittedCountofAllUser() {
    	String hql = "select count(*) from Request as r where r.request_status='submitted'";
    	Query query = sessionFactory.getCurrentSession().createQuery(hql);
    	Long temp = (Long) query.uniqueResult();
    	Integer count = temp.intValue();
        return count;

    }
    
   @Transactional
   @Override
   public Integer getDraftCountofAllUser(){
	   String hql = "select count(*) from Request as r where r.request_status='in_draft'";
   	Query query = sessionFactory.getCurrentSession().createQuery(hql);
   	Long temp = (Long) query.uniqueResult();
   	Integer count = temp.intValue();
       return count;
   }

   @Transactional
   @Override
   public Integer getTotalValidatedDocument(){
	   String hql = "select count(*) from Request  r join CustomerEntity  c on c.request_id=r.id where c.validity='valid'";
	   Query query =sessionFactory.getCurrentSession().createQuery(hql);
	   Long temp = (Long) query.uniqueResult();
	   Integer count = temp.intValue();
	   return count;
   }

}
