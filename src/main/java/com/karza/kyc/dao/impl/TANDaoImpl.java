package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.TANDao;
import com.karza.kyc.model.TAN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Repository
public class TANDaoImpl implements TANDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public TAN get(Long id) {
        return (TAN) sessionFactory.getCurrentSession().get(TAN.class, id);
    }

    @Override
    public void save(TAN tan) {
        sessionFactory.getCurrentSession().merge(tan);
    }

    @Override
    public void delete(TAN tan) {
        sessionFactory.getCurrentSession().delete(tan);
    }

    @Override
    public List<TAN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from TAN");
        List<TAN> tan = query.list();
        return tan;

    }
}
