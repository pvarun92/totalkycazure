package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.CINDao;
import com.karza.kyc.model.CIN;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Repository
public class CINDaoImpl implements CINDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CIN get(Long id) {
        return (CIN) sessionFactory.getCurrentSession().get(CIN.class, id);
    }

    @Override
    public void save(CIN cin) {
        sessionFactory.getCurrentSession().save(cin);
    }

    @Override
    public void update(CIN cin) {
        sessionFactory.getCurrentSession().update(cin);
    }

    @Override
    public void delete(CIN cin) {
        sessionFactory.getCurrentSession().delete(cin);
    }

    @Override
    public List<CIN> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CIN");
        List<CIN> cins = query.list();
        return cins;
    }

    @Override
    public CIN getByCustomerId(Long CustomerId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CIN where customerEntityId =" + CustomerId);
        List<CIN> cinList = query.list();
        if (cinList.size() > 0)
            return cinList.get(0);
        else
            return null;
    }


}
