package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;
import com.karza.kyc.dao.EPFOResponseDao;
import com.karza.kyc.model.EPFOResponse;

@Repository
public class EPFOResponseDaoImpl implements EPFOResponseDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public EPFOResponse get(Long id) {
		return (EPFOResponse) sessionFactory.getCurrentSession().get(EPFOResponse.class, id);
	}

	@Override
	public EPFOResponse save(EPFOResponse epfoResponse) {
		sessionFactory.getCurrentSession().save(epfoResponse);
		return epfoResponse;
	}

	@Override
	public void update(EPFOResponse epfoResponse) {
		sessionFactory.getCurrentSession().update(epfoResponse);
	}

	@Override
	public void delete(EPFOResponse epfoResponse) {
		sessionFactory.getCurrentSession().delete(epfoResponse);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EPFOResponse> findAll() {
		Query query = sessionFactory.getCurrentSession().createQuery(" From EPFOResponse ");
		List<EPFOResponse> epfoResponses = query.list();
		return epfoResponses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EPFOResponse getByCustomerId(Long customerId) {
		Query query = sessionFactory.getCurrentSession().createQuery(" from EPFOResponse where customerMasterId = "+customerId);
		List<EPFOResponse> epfoResponses = query.list();
		 if(epfoResponses.size() > 0)
			 return epfoResponses.get(0);
		 else
			 return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EPFOResponse> getByEPFOReqId(Long id) {
		Query query = sessionFactory.getCurrentSession().createQuery(" from EPFOResponse where epfoRequestID ="+id);
		List<EPFOResponse> epfoResponseList = query.list();
		if(epfoResponseList.size() > 0)
			return epfoResponseList;
		else
		return null;
	}

	

}
