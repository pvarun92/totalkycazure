package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.OtpGenerateDao;
import com.karza.kyc.model.OtpGenerate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Fallon-Software on 7/19/2016.
 */
@Repository
public class OtpGenerateDaoImpl implements OtpGenerateDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public OtpGenerate get(Long id) {
        return (OtpGenerate) sessionFactory.getCurrentSession().get(OtpGenerate.class, id);
    }

    @Override
    @Transactional
    public OtpGenerate findByOtp(Long otp) {
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("from OtpGenerate where otp='" + otp + "'");
            OtpGenerate otpGenerate = (OtpGenerate) query.uniqueResult();
            return otpGenerate;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public OtpGenerate createOtpGenerate(OtpGenerate otpGenerate) {
        return (OtpGenerate) sessionFactory.getCurrentSession().merge(otpGenerate);
    }
}
