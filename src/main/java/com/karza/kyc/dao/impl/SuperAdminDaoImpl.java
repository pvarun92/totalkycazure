package com.karza.kyc.dao.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.karza.kyc.dao.SuperAdminDao;
import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.model.SuperAdmin;
import com.karza.kyc.web.util.StoreProcedureTest;


@Repository
public class SuperAdminDaoImpl implements SuperAdminDao {
	@Autowired
    private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<SuperAdmin> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO, HttpSession httpSession) {
		
			return StoreProcedureTest.getMisDetails(misReportDTO);
	}

}
