package com.karza.kyc.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;
import com.karza.kyc.dao.ESICContriDao;
import com.karza.kyc.model.ESICContriDetails;


@Repository
public class ESICContriDaoImpl implements ESICContriDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public ESICContriDetails get(Long id) {
		return (ESICContriDetails) sessionFactory.getCurrentSession().get(ESICContriDetails.class, id);
	}

	@Override
	public ESICContriDetails save(ESICContriDetails contriDetails) {
		sessionFactory.getCurrentSession().save(contriDetails);
		return contriDetails;
	}

	@Override
	public void update(ESICContriDetails contriDetails) {
		sessionFactory.getCurrentSession().update(contriDetails);
	}

	@Override
	public void delete(ESICContriDetails contriDetails) {
		sessionFactory.getCurrentSession().delete(contriDetails);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ESICContriDetails> findAll() {
		Query query = sessionFactory.getCurrentSession().createQuery(" From ESICContriDetails ");
		List<ESICContriDetails> esicContriDetail = query.list();
		return esicContriDetail;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ESICContriDetails> getByESICReqId(Long id) {
		Query query = sessionFactory.getCurrentSession().createQuery("from ESICContriDetails where esicReqId = "+id);
		List<ESICContriDetails> esicContriDetailList = query.list();
		if(esicContriDetailList.size() > 0)
			return esicContriDetailList;
		else
			return null;
	
	}

}
