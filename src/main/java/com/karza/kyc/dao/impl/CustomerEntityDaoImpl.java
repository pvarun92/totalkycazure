package com.karza.kyc.dao.impl;

import com.karza.kyc.dao.CustomerEntityDao;
import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.SuperAdmin;

import flexjson.transformer.Transformer;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 5:59 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class CustomerEntityDaoImpl implements CustomerEntityDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CustomerEntity get(Long id) {
        return (CustomerEntity) sessionFactory.getCurrentSession().get(CustomerEntity.class, id);
    }

    @Override
    public List<CustomerEntity> getByRequestId(Long request_id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerEntity where request_id =" + request_id);
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Override
    public List<CustomerEntity> getByBranchId(Long branchId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerEntity where branch_id = '" + branchId + "'");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Override
    public List<CustomerEntity> getByEntityStatus(String entity_status) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerEntity where entity_status = '" + entity_status + "'");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Transactional
    @Override
    public List<CustomerEntity> getEntitiesByBranchAndFilter(MisReportDTO misReportDTO, HttpSession httpSession) {//ggfjghj
        String sql = misReportDTO.getSQL(httpSession);
        System.out.println("SQL Query "+sql);
        Query query = sessionFactory.getCurrentSession().createQuery(sql);
        System.out.println("Query "+query);
        try {
            misReportDTO.setValues(query, httpSession);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        List<CustomerEntity> filteredEntities = query.list();
        return filteredEntities;
    }

    /*Method to get all the entities filtered by branch*/
    @Transactional
    @Override
    public List<CustomerEntity> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO, HttpSession httpSession) {
        String sql="select r.id,r.parent_request_id,r.customer_master_id,r.request_status,"
        		+ " r.user_master_id,cm.customer_name,u.unique_id,c.branch_id,c.created_date,c.entity_name,"
        		+ " c.entity_status,c.entity_type,c.itr_validity,c.no_of_documents,c.no_of_parties,"
        		+ " c.request_id,c.validity"
        		+ " from Request as r "
        		+ " left join User_Master as u on r.user_master_id = u.id "
        		+ " left join Customer_Master as cm on u.customer_master_id = cm.id "
        		+ " left join .Customer_Entity as c on r.id = c.request_id"
        		+" AND c.branch_id =:branchId AND c.entity_status='parent'";
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        try {
            misReportDTO.setValues1(query, httpSession);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(" Hi i'm exception from Branch filter " + e.getMessage());
        }
        List<CustomerEntity> filteredEntities = query.list();
        return filteredEntities;
    }

    @Transactional
    @Override
    public List<CustomerEntity> getEntitiesByUserMasterId(Long userMaster_id) {
//        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerEntity where user_master_id = '"+userMaster_id + "'");
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerEntity where userMaster_id = '" + userMaster_id + "' ");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Transactional
    @Override
    public List<CustomerEntity> getIdAndUserMasterIdByRequestStatus(Long userMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery("select c from CustomerEntity c, Request as r where  request_id = r.id and request_status= 'in_queue' AND c.userMaster_id ='" + userMasterId + "' ");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Transactional
    @Override
    public Integer mergeBranchId(Long newBranchId, Long oldBrnachId1, Long oldBrnchId2) {
        Query query = sessionFactory.getCurrentSession().createQuery("update CustomerEntity set branchId = '" + newBranchId + "' where branchId = '" + oldBrnachId1 + "' or branchId = '" + oldBrnchId2 + "' ");
        Integer UpdatedEntities_count = query.executeUpdate();
        return UpdatedEntities_count;
    }

    @Transactional
    @Override
    public Integer replaceBranchId(Long oldBranchId, Long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("update CustomerEntity set branchId = '" + id + "' where branchId = '" + oldBranchId + "'  ");
        Integer UpdatedEntities_count = query.executeUpdate();
        return UpdatedEntities_count;
    }

    @Transactional
    @Override
    public List<CustomerEntity> getByCustomerMasterId(Long customerMasterId) {
        Query query = sessionFactory.getCurrentSession().createQuery(" select  c from CustomerEntity c, Request as r where r.customerMasterId = '" + customerMasterId + "' and r.id = c.request_id  ");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }

    @Override
    public CustomerEntity save(CustomerEntity customerEntity) {
        return (CustomerEntity) sessionFactory.getCurrentSession().merge(customerEntity);
    }

    @Override
    public void update(CustomerEntity customerEntity) {
        sessionFactory.getCurrentSession().update(customerEntity);
    }

    @Override
    public void delete(CustomerEntity customerEntity) {
        sessionFactory.getCurrentSession().delete(customerEntity);
    }

    @Override
    public List<CustomerEntity> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CustomerEntity");
        List<CustomerEntity> customerEntities = query.list();
        return customerEntities;
    }
}
