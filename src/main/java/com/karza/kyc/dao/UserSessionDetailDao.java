package com.karza.kyc.dao;

import com.karza.kyc.model.UserSessionDetail;

/**
 * Created by Admin on 7/12/2016.
 */
public interface UserSessionDetailDao {
    Long save(UserSessionDetail userSessionDetail);

    void update(UserSessionDetail userSessionDetail);

    void delete(UserSessionDetail userSessionDetail);

    Integer getLoggedInUserCount(Long userId);

    UserSessionDetail getBySessionId(String sessionId);

    UserSessionDetail getByUserId(Long userId);

    UserSessionDetail getLastLoggedInUserById(Long userId);
}
