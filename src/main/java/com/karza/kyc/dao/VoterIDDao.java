package com.karza.kyc.dao;

import com.karza.kyc.model.VoterFamily;
import com.karza.kyc.model.VoterID;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
public interface VoterIDDao {
    VoterID get(Long id);

    VoterID getByCustomerId(Long CustomerId);

    void save(VoterID voterID);

    void update(VoterID voterID);

    void delete(VoterID voterID);

    List<VoterID> findAll();

    List<VoterFamily> getFamilyByVoterID(String voterID);

    void deleteFamily(VoterFamily voterFamily);

    void saveVoterFamily(VoterFamily voterFamily);
}
