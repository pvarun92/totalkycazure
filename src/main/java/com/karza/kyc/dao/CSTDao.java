package com.karza.kyc.dao;

import com.karza.kyc.model.CST;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
public interface CSTDao {
    CST get(Long id);

    CST getByCustomerId(Long CustomerId);

    CST save(CST cst);

    void update(CST cst);

    void delete(CST cst);

    List<CST> findAll();
}
