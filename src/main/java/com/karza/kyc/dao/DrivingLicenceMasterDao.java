package com.karza.kyc.dao;

import com.karza.kyc.model.DrivingLicenceMaster;

import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public interface DrivingLicenceMasterDao {
    DrivingLicenceMaster get(Long id);

    DrivingLicenceMaster getByCustomerId(Long CustomerId);

    DrivingLicenceMaster save(DrivingLicenceMaster drivingLicenceMaster);

    void update(DrivingLicenceMaster drivingLicenceMaster);

    void delete(DrivingLicenceMaster drivingLicenceMaster);

    List<DrivingLicenceMaster> findAll();
}
