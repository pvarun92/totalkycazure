package com.karza.kyc.dao;

import com.karza.kyc.model.AccountUser;

import java.util.List;

/**
 * Created by Fallon on 4/25/2016.
 */
public interface AccountUserDao {
    AccountUser get(Long id);

    void save(AccountUser accountUser);

    void update(AccountUser accountUser);

    void delete(AccountUser accountUser);

    List<AccountUser> findAll();

    List<AccountUser> findByAccount(Long accontID);

    AccountUser findByAccountUserName(String accountUserName);
}
