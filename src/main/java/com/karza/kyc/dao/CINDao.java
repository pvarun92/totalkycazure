package com.karza.kyc.dao;


import com.karza.kyc.model.CIN;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface CINDao {
    CIN get(Long id);

    void save(CIN cin);

    void delete(CIN cin);

    List<CIN> findAll();

    void update(CIN panCard);

    CIN getByCustomerId(Long CustomerId);
}
