package com.karza.kyc.dao;

import com.karza.kyc.model.OtpGenerate;

/**
 * Created by Fallon-Software on 7/19/2016.
 */
public interface OtpGenerateDao {

    OtpGenerate createOtpGenerate(OtpGenerate otpGenerate);

    OtpGenerate get(Long id);

    OtpGenerate findByOtp(Long otp);
}
