package com.karza.kyc.dao;

import com.karza.kyc.model.Excise;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface ExciseDao {
    Excise get(Long id);

    void save(Excise excise);

    void update(Excise excise);

    Excise getByCustomerId(Long CustomerId);

    void delete(Excise excise);

    List<Excise> findAll();
}
