package com.karza.kyc.dao;


import com.karza.kyc.model.ServiceTax;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface ServiceTaxDao {

    ServiceTax get(Long id);

    ServiceTax getByCustomerId(Long CustomerId);

    void save(ServiceTax serviceTax);

    void update(ServiceTax serviceTax);

    void delete(ServiceTax serviceTax);

    List<ServiceTax> findAll();
}
