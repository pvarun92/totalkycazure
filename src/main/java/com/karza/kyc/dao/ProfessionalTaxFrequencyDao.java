package com.karza.kyc.dao;

import com.karza.kyc.model.ProfessionalTaxFrequency;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
public interface ProfessionalTaxFrequencyDao {
    ProfessionalTaxFrequency get(Long id);

    List<ProfessionalTaxFrequency> getByProfessionalTaxId(Long professionalTaxId);

    void save(ProfessionalTaxFrequency professionalTaxFrequency);

    void update(ProfessionalTaxFrequency professionalTaxFrequency);

    void delete(ProfessionalTaxFrequency professionalTaxFrequency);

    List<ProfessionalTaxFrequency> findAll();
}
