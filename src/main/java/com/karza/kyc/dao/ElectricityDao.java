package com.karza.kyc.dao;

import com.karza.kyc.model.Electricity;

import java.util.List;

/**
 * Created by Varun_Prakash on 09-01-2017.
 */
// Interface to define the electricity bill implementation as well as the functionality

public interface ElectricityDao {
    // Get the Electricity data
    Electricity get(Long id);
    // Save the Electricity data
    Electricity save(Electricity electricity);
    //Update the electricity data
    void update(Electricity electricity);
    //Delete the electricity data
    void delete(Electricity electricity);
    //List all the Electricity bills along with details
    List<Electricity> findAll();
    //Get the Electricity by Customer ID
    Electricity getByCustomerId(Long customerId);

}
