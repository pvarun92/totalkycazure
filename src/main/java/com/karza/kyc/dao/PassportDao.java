package com.karza.kyc.dao;

import com.karza.kyc.model.Passport;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface PassportDao {

    Passport get(Long id);

    void save(Passport passport);

    void update(Passport passport);

    void delete(Passport passport);

    List<Passport> findAll();

    Passport getByCustomerId(Long CustomerId);
}
