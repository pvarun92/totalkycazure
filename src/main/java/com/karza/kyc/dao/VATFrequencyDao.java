package com.karza.kyc.dao;

import com.karza.kyc.model.VATFrequency;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
public interface VATFrequencyDao {
    VATFrequency get(Long id);

    List<VATFrequency> getByVatId(Long vatId);

    void save(VATFrequency vatFrequency);

    void update(VATFrequency vatFrequency);

    void delete(VATFrequency vatFrequency);

    List<VATFrequency> findAll();
}
