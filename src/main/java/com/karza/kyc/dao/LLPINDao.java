package com.karza.kyc.dao;

import com.karza.kyc.model.LLPIN;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface LLPINDao {
    LLPIN get(Long id);

    void save(LLPIN llpin);

    void update(LLPIN llpin);

    void delete(LLPIN llpin);

    List<LLPIN> findAll();

    LLPIN getByCustomerId(Long CustomerId);
}
