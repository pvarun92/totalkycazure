package com.karza.kyc.dao;

import com.karza.kyc.model.PanCard;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface PanCardDao {
    PanCard get(Long id);

    PanCard getByCustomerId(Long CustomerId);

    void save(PanCard panCard);

    void update(PanCard panCard);

    void delete(PanCard panCard);

    List<PanCard> findAll();
}
