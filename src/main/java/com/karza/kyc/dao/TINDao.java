package com.karza.kyc.dao;

import com.karza.kyc.model.TIN;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface TINDao {
    TIN get(Long id);

    void save(TIN tin);

    void delete(TIN tin);

    List<TIN> findAll();
}
