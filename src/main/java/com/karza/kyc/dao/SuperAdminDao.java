package com.karza.kyc.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.model.SuperAdmin;

public interface SuperAdminDao {
	
	List<SuperAdmin> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO,HttpSession httpSession);


}
