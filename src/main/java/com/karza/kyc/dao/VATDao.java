package com.karza.kyc.dao;

import com.karza.kyc.model.VAT;

import java.util.List;

/**
 * Created by Fallon on 4/18/2016.
 */
public interface VATDao {
    VAT get(Long id);

    VAT getByCustomerId(Long CustomerId);

    VAT save(VAT vat);

    void update(VAT vat);

    void delete(VAT vat);

    List<VAT> findAll();
}
