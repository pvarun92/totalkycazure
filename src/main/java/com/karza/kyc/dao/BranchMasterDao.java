package com.karza.kyc.dao;

import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.LocationMaster;

import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
public interface BranchMasterDao {
    BranchMaster get(Long id);

    BranchMaster getByCustomerId(Long CustomerId);

    void save(BranchMaster branchMaster);

    void update(BranchMaster branchMaster);

    void delete(BranchMaster branchMaster);

    List<BranchMaster> findAll();

    List<BranchMaster> findByUserLocation(List<LocationMaster> locationMasters);

    List<BranchMaster> findByCustomerId(Long customerMasterId);

    BranchMaster createBranch(BranchMaster newBranchMaster);

    Integer getUserCountByCustomerId(Long customerMasterId);

    List<BranchMaster> getAll();
}
