package com.karza.kyc.dao;

import com.karza.kyc.model.DrivingLicenceCOV;

import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public interface DrivingLicenceCOVDao {
    DrivingLicenceCOV get(Long id);

    List<DrivingLicenceCOV> getByLicence(Long dlId);

    void save(DrivingLicenceCOV drivingLicenceCOV);

    void update(DrivingLicenceCOV drivingLicenceCOV);

    void delete(DrivingLicenceCOV drivingLicenceCOV);

    List<DrivingLicenceCOV> findAll();
}
