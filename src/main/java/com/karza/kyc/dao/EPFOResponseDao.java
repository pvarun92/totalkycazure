package com.karza.kyc.dao;

import java.util.List;

import com.karza.kyc.model.EPFOResponse;

public interface EPFOResponseDao {

	// To get the Detail of any Particular data related to the EPFOResponse
	EPFOResponse get(Long id);

	// To save the EPFOResponse details
	EPFOResponse save(EPFOResponse epfoResponse);

	//TO update the Details of the EPFOResponse
	void update(EPFOResponse epfoResponse);

	//TO delete the Details of the EPFOResponse
	void delete(EPFOResponse epfoResponse);

	//To list down all the EPFOResponse numbers existing in DB
	List<EPFOResponse> findAll();

	// Fetch the EPFO detail related to any specific Customer
	EPFOResponse getByCustomerId(Long customerId);
	
	//Fetch the list clubbed together for a particular EPFORequest Id
	List<EPFOResponse> getByEPFOReqId(Long id);
}
