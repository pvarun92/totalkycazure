package com.karza.kyc.dao;

import com.karza.kyc.model.User;

import java.util.List;

public interface UserDao {

    User get(Long id);

    void save(User user);

    void delete(User user);

    List<User> findAll();

    User findByUserName(String username);

}
