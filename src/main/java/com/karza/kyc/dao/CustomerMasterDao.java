package com.karza.kyc.dao;

import com.karza.kyc.model.CustomerMaster;

import java.util.List;

/**
 * Created by Mahananda on 28-April-16.
 */
public interface CustomerMasterDao {
    CustomerMaster get(Long id);

    CustomerMaster getByCustomerId(Long CustomerId);

    CustomerMaster save(CustomerMaster customerMaster);

    void update(CustomerMaster customerMaster);

    void delete(CustomerMaster customerMaster);

    List<CustomerMaster> findAll();

    List<CustomerMaster> findAllNew();

    Integer getMaxAccountNumber();

    String changeEmailDomain(Long customer_id, String old_domain, String new_domain);

    List<CustomerMaster> getAll();

}
