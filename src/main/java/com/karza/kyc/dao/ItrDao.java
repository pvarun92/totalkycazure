package com.karza.kyc.dao;

import com.karza.kyc.model.Itr;

import java.util.List;

/**
 * Created by Admin on 7/16/2016.
 */
public interface ItrDao {
    Itr get(Long id);

    List<Itr> getByCustomerId(Long CustomerId);

    void save(Itr itr);

    void update(Itr itr);

    void delete(Itr itr);

    List<Itr> findAll();

    List<Itr> getByPanId(Long panId);
}
