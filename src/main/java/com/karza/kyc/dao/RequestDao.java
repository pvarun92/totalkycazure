package com.karza.kyc.dao;

import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.Request;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface RequestDao {

    Request get(Long id);

    Request save(Request request);

    void delete(Request request);

    void update(Request request);

    List<Request> findAll();

    List<Request> getRequestsByRequestStatus(String request_status);

    List<CustomerEntity> getEntitiesByRequestStatus(String request_status);

    List<CustomerEntity> getEntitiesByBranchAndStatus(Long branchId, String request_status, HttpSession httpSession);

    Integer getRequsetCountByCustomerMasterId(Long customerMasterId);

    Integer getSubmittedEntitesByCustomerMasterId(Long customerMasterId);

    Integer getPendingEntitesByCustomerMasterId(Long customerMasterId);

    Integer getTotalEntitesByCustomerMasterId(Long customerMasterId);

    Integer getRequsetCountByUserMasterId(Long userMasterId);

    Integer getSubmittedEntitesByUserMasterId(Long userMasterId);

    Integer getPendingEntitesByUserMasterId(Long userMasterId);

    Integer getTotalEntitesByUserMasterId(Long userMasterId);

    Integer getRequestCountOfAllUser();

    Integer getPendingCountofAllUser();

    Integer getSubmittedCountofAllUser();
    
    Integer getDraftCountofAllUser();
    
    Integer getTotalValidatedDocument();
}
