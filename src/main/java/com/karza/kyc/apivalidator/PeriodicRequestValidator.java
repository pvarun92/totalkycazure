package com.karza.kyc.apivalidator;

import com.karza.kyc.service.CustomerEntityService;
import com.karza.kyc.util.ApiCallHelper;

import java.io.IOException;

import org.slf4j.Logger;
//import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Fallon Software on 5/18/2016.
 */
public class PeriodicRequestValidator implements Runnable {
    private static final PeriodicRequestValidator periodicRequestValidator = new PeriodicRequestValidator();
  //  private static final Logger Log = Logger.getLogger(ApiValidator.class);
	static final Logger LOG = LoggerFactory.getLogger(PeriodicRequestValidator.class);

    private static boolean runnerStarted = false;
    @Autowired
    CustomerEntityService customerEntityService;
    private ApiValidator apiValidator;

    public static PeriodicRequestValidator getPeriodicRequestValidator() {
        return periodicRequestValidator;
    }

    @Override
    public void run() {
    	System.out.println("Within the Crawling Thread !!");
        while (true) {
    		LOG.info("JSON String for Aadhar Verification has been Created");
        	System.out.println("woke up now about to do validation");
            apiValidator.validateDocuments();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                System.out.println(e.toString());
            }
        }
    }

    public void startPeriodicReader(ApiValidator apiValidator) {
        if (runnerStarted)
            return;
        this.apiValidator = apiValidator;
        System.out.println("Going to Start the Crawling Thread !!");
       
        Thread t = new Thread(this);
        t.start();
        System.out.println("Currently in Main Thread :~> Started the Crawling Thread !!");
        runnerStarted = true;
    }
}
