package com.karza.kyc.apivalidator;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.slf4j.Logger;
//import org.apache.log4j.Logger;
import org.h2.util.StringUtils;
import org.hibernate.engine.transaction.jta.platform.internal.JtaSynchronizationStrategy;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.ESICContriDetails;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTFrequencyService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.CustomerEntityService;
import com.karza.kyc.service.DrivingLicenceCOVService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.EPFOResponseService;
import com.karza.kyc.service.ESICContriService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityBoardService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxFrequencyService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.RequestService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.VATFrequencyService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;
import com.karza.kyc.util.ApiCallHelper;
import com.karza.kyc.util.DLHelper;
import com.karza.kyc.util.DocumentHelper;
import com.karza.kyc.util.EPFOResponseHelper;
import com.karza.kyc.util.ESICHelper;
import com.karza.kyc.util.ElectricityHelper;
import com.karza.kyc.util.IECHelper;
import com.karza.kyc.util.LPGHelper;
import com.karza.kyc.util.PANHelper;
import com.karza.kyc.util.TelephoneHelper;
import com.karza.kyc.util.VatCstPtHelper;
import com.karza.kyc.util.VoterHelper;

/**
 * Created by Fallon Software on 5/18/2016.
 */
@Repository
public class ApiValidator {
   // private static final Logger logger = Logger.getLogger(ApiValidator.class);
	static final Logger LOG = LoggerFactory.getLogger(ApiValidator.class);

    @Autowired
    PanCardService panCardService;
    @Autowired
    ItrService itrService;
    @Autowired
    ServiceTaxService serviceTaxService;
    @Autowired
    ExciseService exciseService;
    @Autowired
    VATService vatService;
    @Autowired
    VATFrequencyService vatFrequencyService;
    @Autowired
    CSTService cstService;
    @Autowired
    CSTFrequencyService cstFrequencyService;
    @Autowired
    ProfessionalTaxService professionalTaxService;
    @Autowired
    ProfessionalTaxFrequencyService professionalTaxFrequencyService;
    @Autowired
    LLPINService llpinService;
    @Autowired
    AadharCardService aadharCardService;
    @Autowired
    RequestService requestService;
    @Autowired
    IECService iecService;
    @Autowired
    DrivingLicenceMasterService dlService;
    @Autowired
    DrivingLicenceCOVService drivingLicenceCOVService;
    @Autowired
    CINService cinService;
    @Autowired
    FCRNService fcrnService;
    @Autowired
    FLLPINService fllpinService;
    @Autowired
    PassportService passportService;
    @Autowired
    CustomerEntityService customerEntityService;
    @Autowired
    VoterIDService voterIDService;
    @Autowired
    ElectricityService electricityService;
    @Autowired
    ElectricityBoardService boardService;
    @Autowired
    TelephoneService telephoneService;
    @Autowired
    EPFORequestService epfoService;
    @Autowired
    EPFOResponseService epfoResponseService;
    @Autowired
    LPGService lpgService;
    @Autowired
    ESICService esicService;
    @Autowired
    ESICContriService contriDetails;
    
    public ApiValidator() {
    }

    public void validateDocuments() {
        List<Request> pendingRequests = requestService.getRequestsByRequestStatus("in_queue");
        for (int i = 0; i < pendingRequests.size(); i++) {
            Request request = pendingRequests.get(i);
    		LOG.info("Going to process the request ~~> ",request.getId());
            System.out.println("Going to process the request ~~> " +request.getId());
            validateRequest(request);
    		LOG.info("Processed the request ~~> ",request.getId());
            System.out.println("Processed the request ~~> " +request.getId());
        }
    }

    public void validateRequest(Request request) {
        if (request != null) {
            List<CustomerEntity> pendingEntities = customerEntityService.getByRequestId(request.getId());
            int entityValidated = 0;
            boolean isPending = false;
            for (int i = 0; i < pendingEntities.size(); i++) {
                try {
                    CustomerEntity pendingEntity = pendingEntities.get(i);
                    if (pendingEntity != null) {
                        PanCard panCard = panCardService.getByCustomerId(pendingEntity.getId());
                        if (panCard != null && (StringUtils.equals(panCard.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(panCard.getStatusAsPerSource()))) {
                            PanCard panCard1 = validatePan(panCard, pendingEntity);
                            if (panCard1 == null || (StringUtils.equals(panCard1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(panCard1.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }

                        List<Itr> itrs = itrService.getByCustomerId(pendingEntity.getId());
                        if (itrs != null) {
                            if (itrs.size() > 0) {
                                for (int k = 0; k < itrs.size(); k++) {
                                    Itr itr = itrs.get(k);
                                    if (itr != null && (StringUtils.equals(itr.getApiValidation(), "pending") || (StringUtils.isNullOrEmpty(itr.getStatusAsPerSourceAck()) && StringUtils.isNullOrEmpty(itr.getStatusAsPerSourcePan())))) {
                                        Itr itr1 = validateItr(itr, pendingEntity);
                                        if (itr1 == null || (StringUtils.equals(itr.getApiValidation(), "pending") || (StringUtils.isNullOrEmpty(itr1.getStatusAsPerSourceAck()) && StringUtils.isNullOrEmpty(itr1.getStatusAsPerSourcePan())))) {
                                            isPending = true;
                                        }
                                    }
                                }
                            }
                        }

                        AadharCard aadharCard = aadharCardService.getByCustomerId(pendingEntity.getId());
                        if (aadharCard != null && (StringUtils.equals(aadharCard.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(aadharCard.getStatusAsPerSource()))) {
                            AadharCard aadharCard1 = validateAadhar(aadharCard, pendingEntity);
                            if (aadharCard1 == null || (StringUtils.equals(aadharCard.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(aadharCard1.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }

                        IEC iec = iecService.getByCustomerId(pendingEntity.getId());
                        if (iec != null && (StringUtils.equals(iec.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(iec.getStatusAsPerSource()))) {
                            IEC iec1 = validateIEC(iec, pendingEntity);
                            if (iec1 == null || StringUtils.equals(iec1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(iec1.getStatusAsPerSource())) {
                                isPending = true;
                            }
                        }
                        
                        DrivingLicenceMaster dl = dlService.getByCustomerId(pendingEntity.getId());
                        if (dl != null && (StringUtils.equals(dl.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(dl.getStatusAsPerSource()))) {
                            DrivingLicenceMaster drivingLicenceMaster = validateDL(dl, pendingEntity);
                            if (drivingLicenceMaster == null || StringUtils.equals(drivingLicenceMaster.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(drivingLicenceMaster.getStatusAsPerSource())) {
                                isPending = true;
                            }
                        }

                        ServiceTax serviceTax = serviceTaxService.getByCustomerId(pendingEntity.getId());
                        if (serviceTax != null && (StringUtils.equals(serviceTax.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(serviceTax.getStatusAsPerSource()))) {
                            ServiceTax serviceTax1 = validateServiceTax(serviceTax, pendingEntity);
                            if (serviceTax1 == null || (StringUtils.equals(serviceTax1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(serviceTax1.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }

                        Excise excise = exciseService.getByCustomerId(pendingEntity.getId());
                        if (excise != null && (StringUtils.equals(excise.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(excise.getStatusAsPerSource()))) {
                            Excise excise1 = validateExcise(excise, pendingEntity);
                            if (excise1 == null || (StringUtils.equals(excise1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(excise1.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }

                        LLPIN llpin = llpinService.getByCustomerId(pendingEntity.getId());
                        if (llpin != null && (StringUtils.equals(llpin.getApiValidation(), "pending"))) {
                            JSONObject jsonObject = validateLLPIN(llpin, pendingEntity);
                            if (jsonObject == null || (StringUtils.equals(jsonObject.get("apiValidation").toString(), "pending") || StringUtils.isNullOrEmpty(jsonObject.get("apiValidation").toString()))) {
                                isPending = true;
                            }
                        }
                       
                        VAT vat = vatService.getByCustomerId(pendingEntity.getId());
                        if (vat != null && (StringUtils.equals(vat.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(vat.getStatusAsPerSource()))) {
                            VAT vat1 = validateVat(vat, pendingEntity);
                            if (vat1 == null || (StringUtils.equals(vat1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(vat1.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }

                        CST cst = cstService.getByCustomerId(pendingEntity.getId());
                        if (cst != null && (StringUtils.equals(cst.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(cst.getStatusAsPerSource()))) {
                            CST cst1 = validateCst(cst, pendingEntity);
                            if (cst1 == null || StringUtils.equals(cst1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(cst1.getStatusAsPerSource())) {
                                isPending = true;
                            }
                        }

                        ProfessionalTax pt = professionalTaxService.getByCustomerId(pendingEntity.getId());
                        if (pt != null && (StringUtils.equals(pt.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(pt.getStatusAsPerSource()))) {
                            ProfessionalTax professionalTax = validatePT(pt, pendingEntity);
                            if (professionalTax == null || (StringUtils.equals(professionalTax.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(professionalTax.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }

                        Passport passport = passportService.getByCustomerId(pendingEntity.getId());
                        if (passport != null) {
                            validatePassport(passport, pendingEntity);
                        }

                        VoterID voterID = voterIDService.getByCustomerId(pendingEntity.getId());
                        if (voterID != null && (StringUtils.equals(voterID.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(voterID.getStatusAsPerSource()))) {
                            VoterID voterID1 = validateVoterID(voterID, pendingEntity);
                            if (voterID1 == null || (StringUtils.equals(voterID1.getApiValidation(), "pending") || StringUtils.isNullOrEmpty(voterID1.getStatusAsPerSource()))) {
                                isPending = true;
                            }
                        }
                        // this is to verify the electricity bill
                        Electricity electricity = electricityService.getByCustomerId(pendingEntity.getId());
                        //ElectricityBoard electrictyBoard=boardService.get(electricity.getElectricityBoardId());
                        if (electricity != null && (StringUtils.equals(electricity.getApiValidation(),"pending"))){
                      	Electricity electricity1 = validateElectricity(electricity,pendingEntity);
                        	//Check if we need to test "statusAsPerSource" field as well
                        	if(electricity1 == null || (StringUtils.equals(electricity1.getApiValidation(), "pending"))){
                        		isPending = true;
                        	}
                        }
                        
                        Telephone telephone = telephoneService.getByCustomerId(pendingEntity.getId());
                        if(telephone != null && (StringUtils.equals(telephone.getApiValidation(), "pending")||
                        	StringUtils.isNullOrEmpty(telephone.getStatusAsPerSource()))){
                        	Telephone telephone1 = validateTelephone(telephone,pendingEntity);
                        	if(telephone1 == null ||(StringUtils.equals(telephone1.getApiValidation(), "pending")||
                        			StringUtils.isNullOrEmpty(telephone1.getStatusAsPerSource()))){
                        		isPending = true;
                        	}
                        	
                        }
                        EPFORequest epfoRequest = epfoService.getByCustomerID(pendingEntity.getId());
                        if(epfoRequest != null && (StringUtils.equals(epfoRequest.getApiValidation(), "pending")||
                        		StringUtils.isNullOrEmpty(epfoRequest.getStatusAsPerSource()))){
                        		EPFORequest epfo1 = validateEPFO(epfoRequest,pendingEntity);
                        		if(epfo1 == null || (StringUtils.equals(epfo1.getApiValidation(), "pending")||
                        		StringUtils.isNullOrEmpty(epfo1.getStatusAsPerSource()))){
                        			isPending = true;
                        		}
                        }
                        LPG lpg = lpgService.getByCustomerId(pendingEntity.getId());
                        if(lpg != null && (StringUtils.equals(lpg.getApiValidation(), "pending") ||
                        			StringUtils.isNullOrEmpty(lpg.getApiValidation()))){
                        	LPG lpg1 = validateLPG(lpg,pendingEntity);
                        	if(lpg1 == null || (StringUtils.equals(lpg1.getApiValidation(), "pending")||
                        			StringUtils.isNullOrEmpty(lpg1.getStatusAsPerSource()))){
                        		isPending = true;
                        	}
                        }
                        
                        
                        ESIC esic = esicService.getByCustomerId(pendingEntity.getId());
                        if(esic != null && (StringUtils.equals(esic.getApiValidation(), "pending") ||
                    			StringUtils.isNullOrEmpty(esic.getApiValidation()))){
                    	ESIC esic1 = validateESIC(esic,pendingEntity);
                    	if(esic1 == null || (StringUtils.equals(esic1.getApiValidation(), "pending")||
                    			StringUtils.isNullOrEmpty(esic1.getStatusAsPerSource()))){
                    			isPending = true;
                    	}
                    }

                        if (!isPending) {
                            DocumentHelper d = new DocumentHelper();
                            JSONObject validityAndCount = d.getvalidityAndCount(pendingEntity, fcrnService, fllpinService, 
                            		iecService, dlService, cinService, panCardService, serviceTaxService, 
                            		exciseService, llpinService, aadharCardService, passportService, vatService,
                            		cstService, professionalTaxService, itrService, voterIDService,electricityService,
                            		telephoneService,epfoService,lpgService,esicService);
                            String entityValidity = validityAndCount.get("validity").toString();
                            String itrValidity = validityAndCount.get("itrValidity").toString();
                            pendingEntity.setValidity(entityValidity);
                            pendingEntity.setItrValidity(itrValidity);
                            customerEntityService.update(pendingEntity);
                            entityValidated++;
                        }
                    }
                } catch (JSONException ex) {
                    System.out.println(ex.toString());
                    isPending = true;
                }
            }
            if (entityValidated == pendingEntities.size()) {
                request.setRequest_status("submitted");
                requestService.update(request);
            }
        }
    }

    public PanCard validatePan(PanCard panCard, CustomerEntity customerEntity) {
        String entity_type = customerEntity.getEntity_type();
        try {
        	LOG.info("Came to Verify the PAN Document -> ", panCard);
            JSONObject panApiResponse = ApiCallHelper.getPanStatus(panCard.getDocument(),panCard.getAPICount());
            panApiResponse.put("gender", customerEntity.getGender());
            PanCard panCard1 = PANHelper.savePanDetails(panCard, panApiResponse, entity_type, panCardService, customerEntity);
            return panCard1;
        } catch (JSONException e) {
        	e.printStackTrace();
         	System.out.print("Exception from Validate PAN From APIValidator Class"+e.getMessage());
         }
        catch (IOException e) {
        	e.printStackTrace();
         	System.out.print("Exception from Validate PAN From APIValidator Class"+e.getMessage());
         }
        catch (DataAccessException e) {
        	e.printStackTrace();
         	System.out.print("Exception from Validate PAN From APIValidator Class"+e.getMessage());
         }
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }

    public Itr validateItr(Itr itr, CustomerEntity customerEntity) {
        try {
            PanCard panCard = panCardService.get(itr.getPanId());
            JSONObject itrApiResponse = ApiCallHelper.getItrStatus(itr.getDocument(), itr.getAssessmentYear(), panCard.getPanNumber().toString(), itr.getStatusAsPerSourcePan(), itr.getStatusAsPerSourceAck());
            Itr itr1 = new Itr(itr.getDocument(), itr.getPanId(), itr.getAssessmentYear(), itr.getDateOfFilling(), customerEntity.getId(), itrApiResponse);
            itr1.setId(itr.getId());
            itrService.update(itr1);
            return itr1;
        } catch (Exception e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate ITR From APIValidator Class"+e.getMessage());
        }
        return null;
    }

    public AadharCard validateAadhar(AadharCard aadharCard, CustomerEntity customerEntity)  {
        AadharCard aadharCardResponse=null;
        String state = customerEntity.getState();
    	if(customerEntity.getFirst_name()==null || customerEntity.getDob()==null || customerEntity.getPinCode() ==null
    			|| customerEntity.getState()== null){
    		aadharCardResponse= new AadharCard(aadharCard.getDocument(), customerEntity.getId(), "invalid",null,null);
    		aadharCardResponse.setId(aadharCard.getId());
            aadharCardService.update(aadharCardResponse);
            return aadharCardResponse;
    	}
        String name_of_customer = customerEntity.getFirst_name().trim() + " " + customerEntity.getMiddle_name().trim() + " " + customerEntity.getLast_name().trim();
        name_of_customer = name_of_customer.replace("\\s+", " ");
        String dobStr = (new SimpleDateFormat("yyyyMMdd")).format(customerEntity.getDob());
       // String gender = customerEntity.getGender();//AT the moment Ignoring
        String pincode = customerEntity.getPinCode();
        /*
        if (StringUtils.equals("male", gender.toString())) {
            gender = "M";
        } else if (StringUtils.equals("female", gender.toString())) {
            gender = "F";
        } else {
            gender = "T";
        }
        */
        try {
        	List<String> aadhaarErrorResponse=new ArrayList<String>();
        	aadhaarErrorResponse.add("");
        	List<String> khoslaRef=new ArrayList<String>();
        	khoslaRef.add(null);
            Boolean aadharApiResponseStatus = ApiCallHelper.getAadharStatus(aadharCard.getDocument(), name_of_customer, dobStr,state,pincode,aadhaarErrorResponse,khoslaRef);
            System.out.println("Aadhaar Api Response is " + aadharApiResponseStatus);
            if (aadharApiResponseStatus) {
                aadharCardResponse = new AadharCard(aadharCard.getDocument(), customerEntity.getId(),"valid","valid",khoslaRef.get(0));
            } else {
                aadharCardResponse = new AadharCard(aadharCard.getDocument(), customerEntity.getId(),"invalid", aadhaarErrorResponse.get(0),khoslaRef.get(0));
            }
            aadharCardResponse.setId(aadharCard.getId());
            aadharCardService.update(aadharCardResponse);
            return aadharCardResponse;

        } catch (JSONException e) {
        	System.out.print("Exception from Validate Aadhar From APIValidator Class"+e.getMessage());
        }
        catch(NullPointerException e){
        	e.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		}
        return aadharCardResponse;
    }

    public IEC validateIEC(IEC iec, CustomerEntity customerEntity)   {
        try {
            JSONObject iecApiResponse = ApiCallHelper.getIecStatus(iec.getDocument(),iec.getAPICount());
            IEC iec1 = IECHelper.saveIEC(iec, iec.getDocument(), iec.getCustomerEntityId(), iecApiResponse, iecService);
            return iec1;
        } catch (JSONException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate IEC From APIValidator Class"+e.getMessage());
        }
        catch (IOException e) {
         	e.printStackTrace();
         	System.out.print("Exception from Validate IEC From APIValidator Class"+e.getMessage());
         }
        catch (DataAccessException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate IEC From APIValidator Class"+e.getMessage());
		}
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }

    public DrivingLicenceMaster validateDL(DrivingLicenceMaster dl, CustomerEntity customerEntity) {
        try {
            String dob = (new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(customerEntity.getDob()));
            JSONObject dlApiResponse = ApiCallHelper.getDLStatus(dl.getDocument(), dob,dl.getAPICount());
            DrivingLicenceMaster drivingLicenceMaster = DLHelper.saveDL(dlApiResponse, customerEntity, dl.getDocument(), dlService, drivingLicenceCOVService);
            return drivingLicenceMaster;
            //This return value is not being used; as this method is called via a Background Thread [Desired SIde effect is achieved by inserting/updating records in the DL Master Table]
        } catch (JSONException e) {
            //logger.info(e.getMessage());
        	System.out.print("Exception from Validate Driving Licence From APIValidator Class"+e.getMessage());
        }
        catch (IOException e) {
        	System.out.print("Exception from Validate Driving Licence From APIValidator Class"+e.getMessage());
        }
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }

    public ServiceTax validateServiceTax(ServiceTax serviceTax, CustomerEntity customerEntity) {
        try {
            JSONObject serviceTaxApiResponse = ApiCallHelper.getServiceTaxStatus(serviceTax.getDocument(),serviceTax.getAPICount());
            ServiceTax serviceTax1 = new ServiceTax(serviceTax.getDocument(), customerEntity.getId(), serviceTaxApiResponse.get("status").toString(), serviceTaxApiResponse);
            serviceTax1.setId(serviceTax.getId());
            serviceTaxService.update(serviceTax1);
            return serviceTax1;
        } catch (JSONException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate Service Tax From APIValidator Class"+e.getMessage());
        }
        catch (IOException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate Service Tax From APIValidator Class"+e.getMessage());
        }
        catch (DataAccessException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate Service Tax From APIValidator Class"+e.getMessage());
        }
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }

    public Excise validateExcise(Excise excise, CustomerEntity customerEntity) {
        try {
            JSONObject exciseApiResponse = ApiCallHelper.getExcisetatus(excise.getDocument(),excise.getAPICount());
            Excise excise1 = new Excise(excise.getDocument(), customerEntity.getId(), exciseApiResponse.get("status").toString(), exciseApiResponse);
            excise1.setId(excise.getId());
            exciseService.update(excise1);
            return excise1;
        } catch (JSONException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate Excise From APIValidator Class"+e.getMessage());
        }
        catch (IOException e) {
         	e.printStackTrace();
         	System.out.print("Exception from Validate Excise From APIValidator Class"+e.getMessage());
         }
        catch (DataAccessException e) {
         	e.printStackTrace();
         	System.out.print("Exception from Validate Excise From APIValidator Class"+e.getMessage());
         }
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }

    public JSONObject validateLLPIN(LLPIN llpin, CustomerEntity customerEntity)  {
        try {
            JSONObject llpinApiResponse = ApiCallHelper.getLlPinStatus(llpin.getDocument(),customerEntity.getEntity_type(),llpin.getAPICount());
            if (llpinApiResponse.get("documentType").equals("llpin")) {
                LLPIN llpin1 = new LLPIN(llpin.getDocument(), customerEntity.getId(), llpinApiResponse.get("api_status").toString(), llpinApiResponse);
                llpin1.setId(llpin.getId());
                llpinService.update(llpin1);
                JSONObject jsonObject = new JSONObject(llpin1);
                return jsonObject;
            } else if (llpinApiResponse.get("documentType").equals("fllpin")) {
                FLLPIN fllpin = new FLLPIN(llpin.getDocument(), customerEntity.getId(), llpinApiResponse.get("api_status").toString(), llpinApiResponse);
                fllpinService.save(fllpin);
                llpinService.delete(llpin);
                JSONObject jsonObject = new JSONObject(fllpin);
                return jsonObject;
            } else if (llpinApiResponse.get("documentType").equals("cin")) {
                CIN cin = new CIN(llpin.getDocument(), customerEntity.getId(), llpinApiResponse.get("api_status").toString(), llpinApiResponse);
                cinService.save(cin);
                llpinService.delete(llpin);
                JSONObject jsonObject = new JSONObject(cin);
                return jsonObject;
            } else if (llpinApiResponse.get("documentType").equals("fcrn")) {
                FCRN fcrn = new FCRN(llpin.getDocument(), customerEntity.getId(), llpinApiResponse.get("api_status").toString(), llpinApiResponse);
                fcrnService.save(fcrn);
                llpinService.delete(llpin);
                JSONObject jsonObject = new JSONObject(fcrn);
                return jsonObject;
            } else if (llpinApiResponse.get("documentType").equals("")) {
                if (customerEntity.getEntity_type().equals("Company")) {
                    CIN cin = new CIN(llpin.getDocument(), customerEntity.getId(), llpinApiResponse.get("api_status").toString(), llpinApiResponse);
                    cinService.save(cin);
                    llpinService.delete(llpin);
                    JSONObject jsonObject = new JSONObject(cin);
                    return jsonObject;
                } else if (customerEntity.getEntity_type().contains("Limited Liability Partnership")) {
                    LLPIN llpin1 = new LLPIN(llpin.getDocument(), customerEntity.getId(), llpinApiResponse.get("api_status").toString(), llpinApiResponse);
                    llpin1.setId(llpin.getId());
                    llpinService.update(llpin1);
                    JSONObject jsonObject = new JSONObject(llpin1);
                    return jsonObject;
                }
            }
        } catch (JSONException e) {
        	e.printStackTrace();
        	System.out.print("Exception from Validate LLPIN From APIValidator Class"+e.getMessage());
        }
        
        catch(DataAccessException e){
        	e.printStackTrace();
        }
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }

    public VAT validateVat(VAT vat, CustomerEntity customerEntity) {
        try {
            JSONObject vatApiResponse = ApiCallHelper.getVatStatus(vat.getDocument());
            VAT vat1 =  VatCstPtHelper.saveVat(vatApiResponse, customerEntity, vat.getDocument(), vatService, vatFrequencyService);
            return vat1;
        } catch (Exception e) {
          //  logger.info(e.getMessage());
        	e.printStackTrace();
        	System.out.print("Exception from Validate VAT From APIValidator Class"+e.getMessage());
        }
        return null;
    }

    public CST validateCst(CST cst, CustomerEntity customerEntity)  {
        try {
            JSONObject cstApiResponse = ApiCallHelper.getVatStatus(cst.getDocument());
            CST cst1 = VatCstPtHelper.saveCst(cstApiResponse, customerEntity, cst.getDocument(), cstService, cstFrequencyService);
            return cst1;
        }catch (Exception e) {
			e.printStackTrace();
			System.out.print("Exception from Validate CST From APIValidator Class"+e.getMessage());
		}
        return null;
    }

    public ProfessionalTax validatePT(ProfessionalTax pt, CustomerEntity customerEntity) {
        try {
            JSONObject ptApiResponse = ApiCallHelper.getVatStatus(pt.getDocument());
            ProfessionalTax professionalTax = VatCstPtHelper.savePt(ptApiResponse, customerEntity, pt.getDocument(), professionalTaxService, professionalTaxFrequencyService);
            return professionalTax;
        } 
        catch (Exception e) {
			e.printStackTrace();
			System.out.print("Exception from Validate Professional Tax From APIValidator Class"+e.getMessage());
		}
        return null;
    }

    public void validatePassport(Passport passport, CustomerEntity customerEntity) {
        try {
            Passport passport1 = new Passport(passport.getDocument(), customerEntity.getId(), customerEntity.getCountry(), customerEntity.getAddress(), passport.getExpiryDate(), customerEntity.getDob());
            passport1.setId(passport.getId());
            passportService.update(passport1);
        }
        catch (DataAccessException e) {
			e.printStackTrace();
			System.out.print("Exception from Validate Passport From APIValidator Class"+e.getMessage());
		}
        catch(NullPointerException e){
        	e.printStackTrace();
        }    	
    }

    public VoterID validateVoterID(VoterID voterID, CustomerEntity customerEntity) {
        try {
            JSONObject voterApiResponse = ApiCallHelper.getVoterIdStatus(voterID.getDocument(),voterID.getAPICount());
            VoterID voterID1 = VoterHelper.saveVoter(voterID, customerEntity.getId(), voterApiResponse, voterIDService);
            return voterID1;
        } catch (DataAccessException e) {
			e.printStackTrace();
			System.out.print("Exception from Validate voter From APIValidator Class"+e.getMessage());
		}
    	catch(JSONException e){
    		e.printStackTrace();
    		System.out.print("Exception from Validate voter From APIValidator Class"+e.getMessage());
    	}
        catch(NullPointerException e){
        	e.printStackTrace();
        }
        return null;
    }
    //Validate the Telephone Number
    public Telephone validateTelephone(Telephone telephone,CustomerEntity customerEntity){
    	try {
			JSONObject telApiResponse = ApiCallHelper.getTelephoneStatus(telephone.getTelephoneNumber(),telephone.getAPICount());
			Telephone savedTelephoneObj = TelephoneHelper.saveTelephoneDetails(telephone, telApiResponse,
					customerEntity.getId(), telephoneService);
			return savedTelephoneObj;
		} catch (DataAccessException e) {
			e.printStackTrace();
			//logger.info(e.getMessage());
			System.out.print("Exception from Validate Telephone From APIValidator Class"+e.getMessage());
		}
    	catch(JSONException e){
    		e.printStackTrace();
    	}
    	catch(IOException e){
    		e.printStackTrace();
    	}
    	catch(NullPointerException e){
        	e.printStackTrace();
        }
    	return null;
    	
    }
   // validate the EPFO UAN Number
    public EPFORequest validateEPFO(EPFORequest epfo,CustomerEntity customerEntity){
    	try{
    		List<JSONObject> epfoApiResponse = ApiCallHelper.getEPFODetails(epfo.getUan_number(), 
    				customerEntity.getContactNumber(), epfo.getAPICount());
    		EPFORequest savedEPFOObject = EPFOResponseHelper.saveEPFODetails(epfo, epfoApiResponse, customerEntity.getId(), epfoService,epfoResponseService);
    		return savedEPFOObject;
    	}
    	catch (DataAccessException e) {
    		e.printStackTrace();
    	}
    	catch (JSONException e1) {
    		e1.printStackTrace();
		}
    	catch (IOException e) {
    		e.printStackTrace();
		}
    	catch (NullPointerException e) {
    		e.printStackTrace();
    	}
    	return null;
    }
   // validate the LPG ID
    public LPG validateLPG(LPG lpg, CustomerEntity customerEntity){
    	try {
			JSONObject lpgResponse = ApiCallHelper.getLPGStatus(lpg.getLpgId(), lpg.getAPICount());
			LPG saveLPGObject = LPGHelper.saveLPGDetails(lpg, lpgResponse, customerEntity, lpgService);
			return saveLPGObject;
		} catch (DataAccessException e) {
    		e.printStackTrace();
    	}
    	
    	catch(JSONException e){
    		e.printStackTrace();
    	}
    	catch (NullPointerException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
    	
    }
   //Validate the ESIC Number 
    public ESIC validateESIC(ESIC esic,CustomerEntity customerEntity){
    	try{
    		JSONObject esicResponse = ApiCallHelper.getESICStatus(esic.getEsicNumber(), esic.getAPICount());
    		ESIC saveESICObject = ESICHelper.saveESICDetails(esic, esicResponse, customerEntity.getId(), esicService,contriDetails);
    	return saveESICObject;
    	}
    	catch (DataAccessException e) {
    		e.printStackTrace();
    	}
    	catch (JSONException e) {
			e.printStackTrace();
		}
    	catch (NullPointerException e) {
    		e.printStackTrace();
    	}  catch (IOException e) {
			e.printStackTrace();
		}
    	return null;
    }
  // validate the Electricity Bill
    public Electricity validateElectricity(Electricity electricityReqObj,CustomerEntity customerEntity){
    	/*
    	 * This method is only being Called for a Non-NUll electricity Object
    	 * Background Thread will Pick this electricty Request Object from the Queue and will then try to process it
    	 */
    	/*assert(electricityReqObj!=null);*///Remove this asert statement after proper testing and validation
    	try {
			JSONObject electricityApiResponse = ApiCallHelper.getElectricityStatus(electricityReqObj.getConsumer_number(),electricityReqObj.getServiceProvider(),electricityReqObj.getAPICount());
			Electricity savedElectricityObj = ElectricityHelper.saveElectricity(electricityReqObj, electricityApiResponse, customerEntity.getId(),electricityService);
			return savedElectricityObj;
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.print("Exception from Validate Electricity From APIValidator Class"+e.getMessage());
		}
    	catch (DataAccessException e) {
			e.printStackTrace();
			System.out.print("Exception from Validate Electricity From APIValidator Class"+e.getMessage());
		}
    	catch (IOException e) {
			e.printStackTrace();
			System.out.print("Exception from Validate Electricity From APIValidator Class"+e.getMessage());
		}
    	catch(NullPointerException e){
        	e.printStackTrace();
        }
    	return null;
    }
    
}
