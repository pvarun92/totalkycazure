package com.karza.kyc.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.h2.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.karza.kyc.dto.BranchMasterDTO;
import com.karza.kyc.dto.CustomerMasterDTO;
import com.karza.kyc.dto.LocationMasterDTO;
import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.dto.SubscriptionMasterDTO;
import com.karza.kyc.dto.UserMasterDTO;
import com.karza.kyc.mapper.BranchMasterMapper;
import com.karza.kyc.mapper.CustomerMasterMapper;
import com.karza.kyc.mapper.LocationMasterMapper;
import com.karza.kyc.mapper.SubscriptionMasterMapper;
import com.karza.kyc.mapper.UserMasterMapper;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.CustomerMaster;
import com.karza.kyc.model.LocationMaster;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.SubscriptionMaster;
import com.karza.kyc.model.SuperAdmin;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.UserSessionDetail;
import com.karza.kyc.service.BranchMasterService;
import com.karza.kyc.service.CustomerEntityService;
import com.karza.kyc.service.CustomerMasterService;
import com.karza.kyc.service.LocationMasterService;
import com.karza.kyc.service.RequestService;
import com.karza.kyc.service.SubscriptionMasterService;
import com.karza.kyc.service.SuperAdminService;
import com.karza.kyc.service.UserMasterService;
import com.karza.kyc.service.UserService;
import com.karza.kyc.service.UserSessionDetailService;
import com.karza.kyc.util.EmailHelper;
import com.karza.kyc.util.PasswordEncryptDecrypt;
import com.karza.kyc.util.RandomPasswordGenerator;

/**
 * Created by Fallon Software on 4/13/2016.
 */
@Controller
@RequestMapping(SysAdminController.URL)
public class SysAdminController {
	static final String URL = "sysadmin";
	private static final Logger logger = Logger.getLogger(UserController.class);
	@Autowired
	CustomerMasterService customerMasterService;
	@Autowired
	LocationMasterService locationMasterService;
	@Autowired
	UserMasterService userMasterService;
	@Autowired
	BranchMasterService branchMasterService;
	@Autowired
	SubscriptionMasterService subscriptionMasterService;
	@Autowired
	UserService userService;
	@Autowired
	RequestService requestService;
	@Autowired
	CustomerEntityService customerEntityService;
	@Autowired
	UserSessionDetailService userSessionDetailService;
	@Autowired
	SuperAdminService superAdminService;

	/* To redirect the user to the specific page for the admin */
	@RequestMapping(value = "sysAdminMis", method = RequestMethod.GET)
	public ModelAndView getSysMisPage() {
		ModelAndView modelAndView = new ModelAndView("sysAdminMis");
		return modelAndView;
	}



	@RequestMapping(value = "createCustomer", method = RequestMethod.POST)
	public @ResponseBody String createCustomer(@RequestBody CustomerMasterDTO customerMasterDTO,
			HttpServletRequest request) {
		String data = "Customer Saved";
		try {
			JSONObject responseJson = new JSONObject();
			CustomerMasterMapper customerMasterMapper = new CustomerMasterMapper();
			CustomerMaster customerMaster = customerMasterMapper.createCustomerMasterForDTO(customerMasterDTO);
			CustomerMaster customerMaster1 = customerMasterService.save(customerMaster);

			LocationMaster locationMaster1 = locationMasterService
					.getParentLocation(Long.valueOf(customerMaster1.getId()).longValue());
			if (locationMaster1 == null) {
				LocationMaster locationMaster = new LocationMaster("India", customerMaster1.getId(),
						Long.parseLong("0"));
				locationMaster = locationMasterService.saveAndGet(locationMaster);
			}

			SubscriptionMasterMapper subscriptionMasterMapper = new SubscriptionMasterMapper();
			List<SubscriptionMasterDTO> subscriptionMasterDTOList = customerMasterDTO.getSubscriptionMasters();

			for (int i = 0; i < subscriptionMasterDTOList.size(); i++) {
				SubscriptionMaster subscriptionMaster = subscriptionMasterMapper
						.createSubscriptionMasterForDTO(subscriptionMasterDTOList.get(i));
				subscriptionMaster.setCustomerMasterId(customerMaster1.getId());
				subscriptionMasterService.save(subscriptionMaster);
			}

			responseJson.put("result", data);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "updateCustomer", method = RequestMethod.POST)
	public @ResponseBody String updateCustomer(@RequestBody CustomerMasterDTO customerMasterDTO,
			HttpServletRequest request) {
		try{
		JSONObject responseJson = new JSONObject();
		CustomerMasterMapper customerMasterMapper = new CustomerMasterMapper();
		CustomerMaster customerMaster = customerMasterMapper.createCustomerMasterForDTO(customerMasterDTO);
		customerMasterService.update(customerMaster);
		responseJson.put("result", "Customer Updated");
		return responseJson.toString();
	}
		catch (JSONException e) {
    	e.printStackTrace();
    }
    return null;
    }

	@RequestMapping(value = "deleteCustomer", method = RequestMethod.POST)
	public @ResponseBody String deleteCustomer(@RequestBody CustomerMasterDTO customerMasterDTO,
			HttpServletRequest request) {
		try {
			CustomerMasterMapper customerMasterMapper = new CustomerMasterMapper();
			CustomerMaster customerMaster = customerMasterMapper.createCustomerMasterForDTO(customerMasterDTO);
			customerMasterService.delete(customerMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "Customer Deleted");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "showCustomer", method = RequestMethod.GET)
	public @ResponseBody List<CustomerMaster> showCustomer() throws JSONException {
		List<CustomerMaster> allCustomerMasters = customerMasterService.findAll();
		return allCustomerMasters;
	}
	/*Method to fetch the Users specific to the Customers*/
	@RequestMapping(value="getUsersOfCustomer",method = RequestMethod.POST)
	public @ResponseBody List<UserMaster> getUsersOfCustomer(@RequestBody CustomerMasterDTO customerMasterDTO,
			HttpServletRequest request) throws JSONException{
		List<UserMaster> getUsersofCustomer = userMasterService.getUsersOfCustomer(customerMasterDTO.getCustomerName());
		System.out.println("Users Being Returned after Customer Selection"+getUsersofCustomer.iterator());
		return getUsersofCustomer;
	}
	
	/*Method to fetch the Branches Specific to the Customers  getBranchOfCustomer*/
	@RequestMapping(value="getBranchOfCustomer",method = RequestMethod.POST)
	public @ResponseBody List<BranchMaster> getBranchOfCustomer(@RequestBody CustomerMasterDTO customerMasterDTO,
			HttpServletRequest request) throws JSONException
	{
		List<BranchMaster> getBranchOfCustomer = userMasterService.getBranchOfCustomer(customerMasterDTO.getCustomerName());
		System.out.println("Branches Being Returned After Customer Selection  ->> "+getBranchOfCustomer.toString());
		return getBranchOfCustomer;
	}
	
	
	/* Method to fetch all the User details called for sysMisController.js */
	@RequestMapping(value = "showUser", method = RequestMethod.GET)
	public @ResponseBody List<UserMaster> showUser() throws JSONException {
		List<UserMaster> allUserMasters = userMasterService.findAll();
		return allUserMasters;
	}

	@RequestMapping(value = "showCustomerNew", method = RequestMethod.GET)
	public @ResponseBody List<CustomerMaster> showCustomerNew() throws JSONException {
		List<CustomerMaster> allCustomerMasters = customerMasterService.findAllNew();
		return allCustomerMasters;
	}

	@RequestMapping(value = "createCustomerUser", method = RequestMethod.POST)
	public @ResponseBody String createCustomerUser(@RequestBody UserMasterDTO userMasterDTO,
			HttpServletRequest request) {
		try {

			JSONObject responseJson = new JSONObject();
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			CustomerMaster customerMaster = customerMasterService.get(userMaster.getCustomerMasterId());
			userMaster.setCreatedBy(customerMaster.getCustomerName());
			LocationMaster locationMaster1 = locationMasterService
					.getParentLocation(Long.valueOf(userMaster.getCustomerMasterId()).longValue());
			if (locationMaster1 == null) {
				LocationMaster locationMaster = new LocationMaster("India", userMaster.getCustomerMasterId(),
						Long.parseLong("0"));
				locationMaster = locationMasterService.saveAndGet(locationMaster);
				userMaster.setLocation(locationMaster.getId());
			} else {
				userMaster.setLocation(locationMaster1.getId());
			}
			userMaster.setUserRole("Admin");
			int noOfCAPSAlpha = 1;
			int noOfDigits = 1;
			int noOfSplChars = 1;
			int minLen = 8;
			int maxLen = 12;
			char[] pswd = RandomPasswordGenerator.generatePswd(minLen, maxLen, noOfCAPSAlpha, noOfDigits, noOfSplChars);
			/*Line being added in order to encrypt the password which has been generated and the
			 * same value will be stored in Database, not in plain text, we will forward the above generated value of
			 * password only to the customer
			 * */
			String password = PasswordEncryptDecrypt.md5Hash(String.valueOf(pswd));
			
			/*Original Implementation which was saving the password in plain text*/
			//userMaster.setPassword(String.valueOf(pswd));
			/*Below line will ensure that the password will be encoded and saved in the database*/
			userMaster.setPassword(password);
			userMaster.setAutoGeneratedPasswordExpired(Boolean.FALSE);
			userMaster.setAutoGeneratedPasswordFlag(Boolean.FALSE);
			EmailHelper emailHelper = new EmailHelper();
			String isExistUser = userMasterService.IsExistUser(userMaster.getEmail(), userMaster.getContactNumber());
			if (isExistUser != null && isExistUser.isEmpty()) {
				Calendar cal = Calendar.getInstance();
				String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
				userMaster.setCurrentMonth(month);
				Integer totalUserMaster = userMasterService.findByCustomerId(customerMaster.getId()).size();
				UserMaster userMaster1 = null;
				userMaster.setActive(Boolean.TRUE);
				if (totalUserMaster > 0) {
					Integer uniqueId = userMasterService.getMaxUserAccountNumber(customerMaster.getId());
					userMaster.setUniqueId(uniqueId + 1);
					userMaster1 = userMasterService.save(userMaster);
				} else {
					userMaster.setUniqueId(Integer.parseInt(customerMaster.getId().toString() + "000001"));
					userMaster1 = userMasterService.save(userMaster);
				}
				String data = "Customer User Not Saved";
				if (userMaster1 != null) {
					data = "Customer User Saved";
					/*Below was the original Implemntation where the plain text value was fetched from DB*/
					/*String emailStatus = emailHelper.sendEmailToUser(userMaster.getEmail(), userMaster.getPassword(),
							userMaster, "createUser");*/
					/*Below line code will directly push the generated value of the password which is generated
					 * by out RandomPasswordGenerator Class, so that the user get the password in plain text not in 
					 * encoded manner.
					 * */
					String plainPassword = String.valueOf(pswd);
					String emailStatus = emailHelper.sendEmailToUser(userMaster.getEmail(), plainPassword,
							userMaster, "createUser");
				}
				responseJson.put("result", data);
			} else if (isExistUser != null && !isExistUser.isEmpty()) {
				responseJson.put("result", isExistUser);
			}
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "updateCustomerUser", method = RequestMethod.POST)
	public @ResponseBody String updateCustomerUser(@RequestBody UserMasterDTO userMasterDTO,
			HttpServletRequest request) {
		try {
			JSONObject responseJson = new JSONObject();
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			userMasterDTO.setUserName(userMasterService.get(userMasterDTO.getId()).getUserName());
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			userMaster.setUserRole("Admin");
			UserMaster userMaster1 = userMasterService.get(userMaster.getId());
			Boolean emailAlreadyExist = false;
			Boolean phoneAlreadyExist = false;
			String data = "";
			if (!userMaster.getEmail().equals(userMaster1.getEmail())) {
				String isExistUser = userMasterService.IsEmailExist(userMaster.getEmail());
				if (isExistUser != null && !isExistUser.isEmpty()) {
					emailAlreadyExist = true;
				}
			}
			if (!userMaster.getContactNumber().equals(userMaster1.getContactNumber())) {
				String isExistUser = userMasterService.IsContactExist(userMaster.getContactNumber());
				if (isExistUser != null && !isExistUser.isEmpty()) {
					phoneAlreadyExist = true;
				}
			}
			if (phoneAlreadyExist && emailAlreadyExist) {
				data = "Email and phone are already exist";
			}
			if (phoneAlreadyExist) {
				data = "User Contact Number already exist";
			}
			if (emailAlreadyExist) {
				data = "User email already exist";
			}
			if (!phoneAlreadyExist && !emailAlreadyExist) {
				Calendar cal = Calendar.getInstance();
				String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
				userMaster.setCurrentMonth(month);
				userMasterService.update(userMaster);
				responseJson.put("result", "Customer User Updated");
			} else {
				responseJson.put("result", data);
			}
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "deleteCustomerUser", method = RequestMethod.POST)
	public @ResponseBody String deleteCustomerUser(@RequestBody UserMasterDTO userMasterDTO,
			HttpServletRequest request) {
		try {
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			userMasterService.delete(userMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "User Deleted");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "getCustomerUsers", method = RequestMethod.POST)
	public @ResponseBody List<UserMaster> getCustomerUsers(@RequestBody UserMasterDTO userMasterDTO,
			HttpServletRequest request) {
		try{
		UserMasterMapper userMasterMapper = new UserMasterMapper();
		UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
		List<UserMaster> userMasterList = userMasterService.findByCustomerIdAndRole(userMaster.getCustomerMasterId(),
				"Admin");
		return userMasterList;
	}
	catch (NumberFormatException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }

	@RequestMapping(value = "getCustomerProfileData", method = RequestMethod.POST)
	public @ResponseBody String getCustomerProfileData(@RequestBody String customer_id, HttpServletRequest request) {
		try {
			JSONObject id = new JSONObject(customer_id);
			Long customerId = id.getLong("id");
			CustomerMaster customerMaster = customerMasterService.get(customerId);

			Integer totalNoOfUsers = userMasterService.getUserCountByCustomerId(customerId);
			Integer totalNoOfBranch = branchMasterService.getUserCountByCustomerId(customerId);
			Integer totalNoOfLocation = locationMasterService.getUserCountByCustomerId(customerId);
			Integer totalNoOfRequest = requestService.getRequsetCountByCustomerMasterId(customerId);
			Integer toNoPendingRequest = requestService.getPendingEntitesByCustomerMasterId(customerId);
			Integer totalNoOFCompletedRequest = requestService.getSubmittedEntitesByCustomerMasterId(customerId);
			Integer totalNoOfEntities = requestService.getTotalEntitesByCustomerMasterId(customerId);
			JSONObject customer_activity = new JSONObject();
			customer_activity.put("total_request", totalNoOfRequest);
			customer_activity.put("total_generated_report", totalNoOFCompletedRequest);
			customer_activity.put("corresponding_entities", totalNoOfEntities);
			customer_activity.put("total_pending", toNoPendingRequest);

			List<SubscriptionMaster> subscriptionMasterList = subscriptionMasterService.findByCustomerId(customerId);

			JSONArray subscriptionMasterJsonArray = new JSONArray();
			for (SubscriptionMaster subscriptionMaster : subscriptionMasterList) {
				JSONObject subscriptionMasterJSON = new JSONObject(subscriptionMaster);
				subscriptionMasterJsonArray.put(subscriptionMasterJSON);
			}
			JSONObject responseJson = new JSONObject();
			responseJson.put("customerName", customerMaster.getCustomerName());
			responseJson.put("isActive", true);
			responseJson.put("customerType", customerMaster.getCustomerType());
			responseJson.put("contactPerson", customerMaster.getContactPerson());
			responseJson.put("contactNumber", customerMaster.getContactNumber());
			responseJson.put("email", customerMaster.getEmail());
			responseJson.put("userCount", totalNoOfUsers);
			responseJson.put("locationCount", totalNoOfLocation);
			responseJson.put("branchCount", totalNoOfBranch);
			responseJson.put("chkInstakyc", "");
			responseJson.put("chkKscan", "");
			responseJson.put("chkWatch", "");
			responseJson.put("subscription_master", subscriptionMasterJsonArray);
			responseJson.put("customer_activity", customer_activity);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "getUserProfileData", method = RequestMethod.POST)
	public @ResponseBody String getUserProfileData(@RequestBody String user_id, HttpServletRequest request) {
		try {
			Long userId = Long.parseLong(user_id);
			UserMaster userMaster = userMasterService.get(userId);
			JSONObject responseJson = new JSONObject();
			Integer totalNoOfRequest = requestService.getRequsetCountByUserMasterId(userId);
			Integer totalPendingRequest = requestService.getPendingEntitesByUserMasterId(userId);
			Integer totalNoOFCompletedRequest = requestService.getSubmittedEntitesByUserMasterId(userId);
			Integer totalNoOfEntities = requestService.getTotalEntitesByUserMasterId(userId);
			JSONObject customer_activity = new JSONObject();
			UserSessionDetail lastLogin = userSessionDetailService.getLastLoggedInUserById(userId);
			// List<SubscriptionMaster> subscriptionMasterList =
			// subscriptionMasterService.findByCustomerId(userMaster.getCustomerMasterId());
			// for(int i=0;i<subscriptionMasterList.size();i++){
			// if(subscriptionMasterList.get(i).getApplication().equalsIgnoreCase("instakyc")){
			// responseJson.put("chkInstakyc",true);
			// }
			// else
			// if(subscriptionMasterList.get(i).getApplication().equalsIgnoreCase("kwatch")){
			// responseJson.put("chkWatch",true);
			// }
			// else
			// if(subscriptionMasterList.get(i).getApplication().equalsIgnoreCase("kscan")){
			// responseJson.put("chkKscan",true);
			// }
			// }
			customer_activity.put("total_request", totalNoOfRequest);
			customer_activity.put("total_generated_report", totalNoOFCompletedRequest);
			customer_activity.put("corresponding_entities", totalNoOfEntities);
			customer_activity.put("total_pending", totalPendingRequest);
			responseJson.put("userName", userMaster.getUserName());
			responseJson.put("active", userMaster.getActive());
			responseJson.put("id", userMaster.getId());
			responseJson.put("userId", userMaster.getUniqueId());
			responseJson.put("isActive", userMaster.getActive());
			responseJson.put("userType", userMaster.getUserRole());
			responseJson.put("contactNumber", userMaster.getContactNumber());
			responseJson.put("email", userMaster.getEmail());
			responseJson.put("location", (userMaster.getLocation()));
			responseJson.put("locationName", (locationMasterService.get(userMaster.getLocation())).getLocationOrZone());
			if (lastLogin != null) {
				responseJson.put("lastLogin", lastLogin.getUpdatedAt());
				responseJson.put("lastLoginCreatedAt", lastLogin.getCreatedAt());
			}

			responseJson.put("profile", new JSONObject(userMaster));
			responseJson.put("customer_activity", customer_activity);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "getAllLocationsByCustomerMasterId", method = RequestMethod.POST)
	public @ResponseBody List<LocationMaster> getAllLocationsByCustomerMasterId(@RequestBody String customerMasterId) {
		try {
			JSONObject id = new JSONObject(customerMasterId);
			Long customerId = id.getLong("id");
			List<LocationMaster> locationMasterList = locationMasterService.findByCustomerId(customerId);
			return locationMasterList;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "getAllBranchesByCustomerMasterId", method = RequestMethod.POST)
	public @ResponseBody List<BranchMaster> getAllBranchesByCustomerMasterId(@RequestBody String customerMasterId) {
		try {
			JSONObject id = new JSONObject(customerMasterId);
			Long customerId = id.getLong("id");
			List<BranchMaster> branchMasterList = branchMasterService.findByCustomerId(customerId);
			return branchMasterList;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "sysAdminCreateLocation", method = RequestMethod.POST)
	public @ResponseBody String sysAdminCreateLocation(@RequestBody LocationMasterDTO locationMasterDTO,
			HttpServletRequest request) {
		try
		{
		LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
		LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
		UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
		locationMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
		if (locationMaster.getParentLocation() == null) {
			LocationMaster locationMaster1 = locationMasterService.getParentLocation(currentUser.getCustomerMasterId());
			locationMaster.setParentLocation(locationMaster1.getId());
		}
		locationMasterService.save(locationMaster);
		String data = "Location Saved";
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", data);
		return responseJson.toString();
		}
		catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }

	@RequestMapping(value = "sysAdminCreateLocationNew", method = RequestMethod.POST)
	public @ResponseBody String sysAdminCreateLocationNew(@RequestBody LocationMasterDTO locationMasterDTO,
			HttpServletRequest request){
		try
		{
		LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
		LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
		UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
		if (locationMaster.getParentLocation() == null) {
			LocationMaster locationMaster1 = locationMasterService
					.getParentLocation(locationMaster.getCustomerMasterId());
			locationMaster.setParentLocation(locationMaster1.getId());
		}
		locationMasterService.save(locationMaster);
		String data = "Location Saved";
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", data);
		return responseJson.toString();
	}
		catch (JSONException e) {
    	e.printStackTrace();
    }
    return null;
    }

	@RequestMapping(value = "sysAdminUpdateLocation", method = RequestMethod.POST)
	public @ResponseBody String sysAdminUpdateLocation(@RequestBody LocationMasterDTO locationMasterDTO,
			HttpServletRequest request) {
		try
		{
		UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
		LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
		LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
		if (locationMaster.getParentLocation() == null) {
			LocationMaster locationMaster1 = locationMasterService.getParentLocation(currentUser.getCustomerMasterId());
			locationMaster.setParentLocation(locationMaster1.getId());
		}
		locationMasterService.update(locationMaster);
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", "Location Updated");
		return responseJson.toString();
	}
	catch (JSONException e) {
    	e.printStackTrace();
    }
    return null;
    }

	@RequestMapping(value = "sysAdminCreateBranch", method = RequestMethod.POST)
	public @ResponseBody String sysAdminCreateBranch(@RequestBody BranchMasterDTO branchMasterDTO,
			HttpServletRequest request) {
		try{
		BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
		BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
		UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
		LocationMaster locationMaster;
		if (branchMaster.getLocation() == null) {
			locationMaster = locationMasterService.getParentLocation(currentUser.getCustomerMasterId());
		} else {
			locationMaster = locationMasterService.get(branchMaster.getLocation());
		}
		LocationMaster branchLocation = new LocationMaster(branchMaster.getBranchName(),
				currentUser.getCustomerMasterId(), locationMaster.getId());
		branchLocation = locationMasterService.saveAndGet(branchLocation);
		branchMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
		branchMaster.setLocation(branchLocation.getId());
		branchMasterService.save(branchMaster);
		// Long new_branchID= branchMasterService.createBranch(branchMaster);
		// customerEntityService.mergeBranchId(new_branchID, Long.valueOf(8),
		// Long.valueOf(9));
		String data = "Branch Saved";
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", data);
		return responseJson.toString();
	}
		catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }

	@RequestMapping(value = "sysAdminCreateBranchNew", method = RequestMethod.POST)
	public @ResponseBody String sysAdminCreateBranchNew(@RequestBody BranchMasterDTO branchMasterDTO,
			HttpServletRequest request){
		try{
		BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
		BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
		LocationMaster locationMaster;
		if (branchMaster.getLocation() == null) {
			locationMaster = locationMasterService.getParentLocation(branchMaster.getCustomerMasterId());
		} else {
			locationMaster = locationMasterService.get(branchMaster.getLocation());
		}
		LocationMaster branchLocation = new LocationMaster(branchMaster.getBranchName(),
				branchMaster.getCustomerMasterId(), locationMaster.getId());
		branchLocation = locationMasterService.saveAndGet(branchLocation);
		branchMaster.setLocation(branchLocation.getId());
		branchMasterService.save(branchMaster);
		String data = "Branch Saved";
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", data);
		return responseJson.toString();
	}
	 catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }
	@RequestMapping(value = "sysUpdateBranch", method = RequestMethod.POST)
	public @ResponseBody String sysUpdateBranch(@RequestBody BranchMasterDTO branchMasterDTO,
			HttpServletRequest request) {
		try
			{
		BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
		BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
		UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
		if (branchMaster.getLocation() == null) {
			LocationMaster locationMaster = locationMasterService.getParentLocation(currentUser.getCustomerMasterId());
			branchMaster.setLocation(locationMaster.getId());
		}
		branchMasterService.update(branchMaster);
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", "Branch Updated");
		return responseJson.toString();
	}
		catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }

	@RequestMapping(value = "sysMergeBranch", method = RequestMethod.POST)
	public @ResponseBody String sysMergeBranch(@RequestBody String request_data, HttpServletRequest request)
			 {
		try
		{
		JSONObject jObject = new JSONObject(request_data);
		JSONArray jsonArray = jObject.getJSONArray("branchName");
		String newBranchId = jObject.getString("branch");
		JSONObject responseJson = new JSONObject();
		// UserMaster currentUser = (UserMaster)
		// request.getSession().getAttribute("currentUser");
		BranchMaster newBanchMaster = branchMasterService.get(Long.valueOf(newBranchId));
		Long newLocation = newBanchMaster.getLocation();

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			String oldBranchId = jsonObject.getString("id");
			Integer updatedEntitiesCount = customerEntityService.replaceBranchId(Long.valueOf(oldBranchId),
					Long.valueOf(newBranchId)); // replace old branchId to New
												// BranchId in Entities table
			Integer updatedUsers = userMasterService.replaceLocation(newLocation,
					Long.valueOf(jsonObject.getString("location")));
		}
		String data = "Branch Saved";
		responseJson.put("result", data);
		return responseJson.toString();
	}
		catch (JSONException e) {
    	e.printStackTrace();
    }
    return null;
    }

	@RequestMapping(value = "locationsData", method = RequestMethod.GET)
	public @ResponseBody List<LocationMaster> locationsData(HttpSession httpSession){
		try{
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		List<LocationMaster> locationMasterList = locationMasterService
				.findByCustomerId(currentUser.getCustomerMasterId());
		return locationMasterList;
	}
	catch (NumberFormatException e) {
	    	e.printStackTrace();
	   }
	    return null;
	    }

	@RequestMapping(value = "getCustomerData", method = RequestMethod.POST)
	public @ResponseBody String getCustomerData(@RequestBody String customer_Id) {
		try
		{
		JSONObject responseJson = new JSONObject();
		CustomerMaster customerMaster = customerMasterService.get(Long.parseLong(customer_Id.toString()));
		List<SubscriptionMaster> subscriptionMasterList = subscriptionMasterService
				.findByCustomerId(Long.parseLong(customer_Id.toString()));
		JSONArray subscriptionMasterJsonArray = new JSONArray();
		for (SubscriptionMaster subscriptionMaster : subscriptionMasterList) {
			JSONObject subscriptionMasterJSON = new JSONObject(subscriptionMaster);
			subscriptionMasterJsonArray.put(subscriptionMasterJSON);
		}

		responseJson.put("subscriptionMaster", subscriptionMasterJsonArray);
		responseJson.put("customerMaster", new JSONObject(customerMaster));
		return responseJson.toString();
	}
	catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	}
	/* Function written to get all the Branches */
	@RequestMapping(value = "allBranches", method = RequestMethod.GET)
	public @ResponseBody List<BranchMaster> allBranches() throws JSONException {
		List<BranchMaster> branchMasters = branchMasterService.getAll();
		return branchMasters;
	}

	/* function written to get all the customers data */
	@RequestMapping(value = "sysAdmingetAllCustomers", method = RequestMethod.GET)
	public @ResponseBody List<CustomerMaster> sysAdmingetAllCustomers() throws JSONException {
		List<CustomerMaster> customerData = customerMasterService.getAll();
		return customerData;
	}

	/* Function written to get the all the details of the user */
	@RequestMapping(value = "sysAdmingetAllUsers", method = RequestMethod.GET)
	public @ResponseBody List<UserMaster> sysAdmingetAllUsers() throws JSONException {
		List<UserMaster> userData = userMasterService.getAll();
		return userData;
	}

	/* function to get all the data related to system user */
	/*This method is to get the value for the Karza admin, in order to bill to it's customers*/
	@RequestMapping(value = "getFilteredDataAdmin", method = RequestMethod.POST)
	public @ResponseBody String getFilteredDataAdmin(@RequestBody MisReportDTO misReportDTO, HttpServletRequest request,
			HttpSession httpSession) {
		try
		{
		/* List of all the branches in order to make a filter upon */
		List<BranchMaster> allBranches = getAllBranches();
		/* List of all the customers in order to make a filter upon */
		List<CustomerMaster> allCustomers = getAllCustomers();
		/* List of all the Usere in order to make filter upon */
		List<UserMaster> allUsers = getAllUsers();
		// List<UserMaster> allUsers = sysAdmingetAllUsers();
		JSONArray filteredEntitiesArray = new JSONArray();
		int generated = 0;
		int pending = 0;
		int in_draft = 0;
		int valid = 0;
		int invalid = 0;
		int partial_valid = 0;
		int itr_valid = 0;
		int itr_invalid = 0;
		misReportDTO.formateData();
		if (misReportDTO.getReport_status() != null && misReportDTO.getReport_status().equals("in_draft")) {
			misReportDTO.setReport_status("in_queue");
		}
		List<SuperAdmin> filteredEntites = new ArrayList<>();
		if (misReportDTO.getBranchId() == null) {
			for (int i = 0; i < allBranches.size(); i++) {
				misReportDTO.setBranchId(allBranches.get(i).getId());
				List<SuperAdmin> filteredEntitesByBranch = superAdminService
						.getEntitiesByBranchAndFilterAdmin(misReportDTO, httpSession);
				filteredEntites.addAll(filteredEntitesByBranch);
			}
		} else {
			List<SuperAdmin> filteredEntitesByBranch = superAdminService
					.getEntitiesByBranchAndFilterAdmin(misReportDTO, httpSession);
			filteredEntites.addAll(filteredEntitesByBranch);
		}


		for (int i =0;i<filteredEntites.size();i++) {

			JSONObject entity = new JSONObject();
			System.out.println(filteredEntites.get(i));
			
			SuperAdmin superAdmin = filteredEntites.get(i);
		
			Request request1 = requestService.get(filteredEntites.get(i).getRequest_id().longValue());
			String request_status = request1.getRequest_status();

			if (request_status.equals("in_queue")) {
				request_status = "Pending";
				pending++;
			} else if (request_status.equals("in_draft")) {
				request_status = "In draft";
				in_draft++;
			} else {
				request_status = "Generated";
				generated++;
			}
			if(superAdmin.getCustomer_entity_type().equals("Individual")){
				entity.put("entity_name", superAdmin.getCustomer_entity_first_name() + " " + superAdmin.getCustomer_entity_middle_name() + " "
						+ superAdmin.getCustomer_entity_last_name());
			} 
		else {
				entity.put("entity_name", superAdmin.getCustomer_entity_name());
			}
			entity.put("report_id", superAdmin.getRequest_id());
			entity.put("report_date", superAdmin.getCustomer_entity_created_date());
			entity.put("entity_type", superAdmin.getCustomer_entity_type());
			entity.put("branch", branchMasterService.get(superAdmin.getCustomer_entity_branch_id()).getBranchName());
			entity.put("no_of_parties", superAdmin.getCustomer_entity_no_of_parties());
			entity.put("no_of_documents", superAdmin.getCustomer_entity_no_of_documents());
			entity.put("report_status", request_status);
			entity.put("validity", superAdmin.getCustomer_entity_validity());
			entity.put("Itrvalidity", superAdmin.getCustomer_entity_itr_validity());

			boolean validityValid = true;
			if (misReportDTO.getValidity() != null) {
				if (!misReportDTO.getValidity().equals(superAdmin.getCustomer_entity_validity())) {
					validityValid = false;
				}
			}

			if (validityValid) {
				if (superAdmin.getCustomer_entity_validity() != null) {
					if (superAdmin.getCustomer_entity_validity().equals("valid")) {
						valid++;
					} else if (superAdmin.getCustomer_entity_validity().equals("partial_valid")) {
						partial_valid++;
					} else {
						invalid++;
					}
				}
				if (!StringUtils.equals(superAdmin.getCustomer_entity_itr_validity(), "-")
						&& superAdmin.getCustomer_entity_itr_validity() != null) {
					if (StringUtils.equals(superAdmin.getCustomer_entity_itr_validity(), "valid")) {
						itr_valid++;
					} else {
						itr_invalid++;
					}
				}
				if (!request_status.equals("In draft")) {
					filteredEntitiesArray.put(entity);
				}
			}
		}
		Integer total_request = filteredEntites.size();
		Integer total_generated_request = valid+invalid+partial_valid;
		JSONObject summary_count_data = new JSONObject();
		summary_count_data.put("total_request", total_request);
		summary_count_data.put("generated", total_generated_request);
		summary_count_data.put("pending", pending);
		summary_count_data.put("inDraft", in_draft);
		summary_count_data.put("valid", valid);
		summary_count_data.put("invalid", invalid);
		summary_count_data.put("partial_valid", partial_valid);
		summary_count_data.put("itrValid", itr_valid);
		summary_count_data.put("itrInvalid", itr_invalid);

		JSONObject filteredData = new JSONObject();
		filteredData.put("data", filteredEntitiesArray);
		filteredData.put("summary_count_data", summary_count_data);
		return filteredData.toString();
	}
		catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }

	@RequestMapping(value = "commonFilter", method = RequestMethod.POST)
	public @ResponseBody String commonFilter(@RequestBody MisReportDTO misReportDTO, HttpServletRequest request,
			HttpSession httpSession) {
		try
		{
		List<BranchMaster> userBranches = getAllBranchesOfUser(request.getSession());

		misReportDTO.formateData();
		List<CustomerEntity> filteredEntites = new ArrayList<>();
		if (misReportDTO.getBranchId() == null) {
			for (int i = 0; i < userBranches.size(); i++) {
				misReportDTO.setBranchId(userBranches.get(i).getId());
				List<CustomerEntity> filteredEntitesByBranch = customerEntityService
						.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
				filteredEntites.addAll(filteredEntitesByBranch);
			}
		} else {
			List<CustomerEntity> filteredEntitesByBranch = customerEntityService
					.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
			filteredEntites.addAll(filteredEntitesByBranch);
		}

		JSONArray filteredEntitiesArray = new JSONArray();
		int generated = 0;
		int pending = 0;
		int valid = 0;
		int invalid = 0;
		for (int i = 0; i < filteredEntites.size(); i++) {
			JSONObject entity = new JSONObject();
			CustomerEntity customerEntity = filteredEntites.get(i);
			Request request1 = requestService.get(customerEntity.getRequest_id());
			String request_status = request1.getRequest_status();
			if (misReportDTO.getViewType() != null && !misReportDTO.getViewType().equalsIgnoreCase("all")) {
				if (misReportDTO.getViewType().equalsIgnoreCase("unread")) {
					if (!request1.getIsRead()) {
						if (request_status.equals("in_draft")) {
							request_status = "pending";
							pending++;
						} else {
							request_status = "Generated";
							generated++;
						}
						if (customerEntity.getEntity_type().equals("Individual")) {
							entity.put("entity_name", customerEntity.getFirst_name() + " "
									+ customerEntity.getMiddle_name() + " " + customerEntity.getLast_name());
						} else {
							entity.put("entity_name", customerEntity.getEntity_name());
						}
						entity.put("id", request1.getId());
						entity.put("request_id", request1.getId());
						entity.put("first_name", customerEntity.getFirst_name());
						entity.put("middle_name", customerEntity.getMiddle_name());
						entity.put("last_name", customerEntity.getLast_name());
						entity.put("entity_name", customerEntity.getEntity_name());
						entity.put("report_id", customerEntity.getRequest_id());
						entity.put("report_date", customerEntity.getCreated_date());
						entity.put("entity_type", customerEntity.getEntity_type());
						entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
						entity.put("no_of_parties", customerEntity.getNo_of_parties());
						entity.put("no_of_documents", customerEntity.getNo_of_documents());
						entity.put("report_status", request_status);
						entity.put("validity", customerEntity.getValidity());
						entity.put("created_date", customerEntity.getCreated_date());
						entity.put("isRead", request1.getIsRead());
						entity.put("isEdited", request1.getIsEdited());
						boolean validityValid = true;
						if (misReportDTO.getValidity() != null) {
							if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
								validityValid = false;
							}
						}
						if (validityValid) {
							if (customerEntity.getValidity() != null) {
								if (customerEntity.getValidity().equals("valid")) {
									valid++;
								} else {
									invalid++;
								}

							} else {
								invalid++;
							}
							filteredEntitiesArray.put(entity);
						}
					}
				} else if (misReportDTO.getViewType().equalsIgnoreCase("read")) {
					if (request1.getIsRead()) {
						if (request_status.equals("in_draft")) {
							request_status = "pending";
							pending++;
						} else {
							request_status = "Generated";
							generated++;
						}
						if (customerEntity.getEntity_type().equals("Individual")) {
							entity.put("entity_name", customerEntity.getFirst_name() + " "
									+ customerEntity.getMiddle_name() + " " + customerEntity.getLast_name());
						} else {
							entity.put("entity_name", customerEntity.getEntity_name());
						}
						entity.put("id", request1.getId());
						entity.put("request_id", request1.getId());
						entity.put("first_name", customerEntity.getFirst_name());
						entity.put("middle_name", customerEntity.getMiddle_name());
						entity.put("last_name", customerEntity.getLast_name());
						entity.put("entity_name", customerEntity.getEntity_name());
						entity.put("report_id", customerEntity.getRequest_id());
						entity.put("report_date", customerEntity.getCreated_date());
						entity.put("entity_type", customerEntity.getEntity_type());
						entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
						entity.put("no_of_parties", customerEntity.getNo_of_parties());
						entity.put("no_of_documents", customerEntity.getNo_of_documents());
						entity.put("report_status", request_status);
						entity.put("validity", customerEntity.getValidity());
						entity.put("created_date", customerEntity.getCreated_date());
						entity.put("isRead", request1.getIsRead());
						entity.put("isEdited", request1.getIsEdited());
						boolean validityValid = true;
						if (misReportDTO.getValidity() != null) {
							if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
								validityValid = false;
							}
						}
						if (validityValid) {
							if (customerEntity.getValidity() != null) {
								if (customerEntity.getValidity().equals("valid")) {
									valid++;
								} else {
									invalid++;
								}

							} else {
								invalid++;
							}
							filteredEntitiesArray.put(entity);
						}
					}
				}
			} else {
				if (request_status.equals("in_draft")) {
					request_status = "pending";
					pending++;
				} else {
					request_status = "Generated";
					generated++;
				}
				if (customerEntity.getEntity_type().equals("Individual")) {
					entity.put("entity_name", customerEntity.getFirst_name() + " " + customerEntity.getMiddle_name()
							+ " " + customerEntity.getLast_name());
				} else {
					entity.put("entity_name", customerEntity.getEntity_name());
				}
				entity.put("id", request1.getId());
				entity.put("request_id", request1.getId());
				entity.put("first_name", customerEntity.getFirst_name());
				entity.put("middle_name", customerEntity.getMiddle_name());
				entity.put("last_name", customerEntity.getLast_name());
				entity.put("entity_name", customerEntity.getEntity_name());
				entity.put("report_id", customerEntity.getRequest_id());
				entity.put("report_date", customerEntity.getCreated_date());
				entity.put("entity_type", customerEntity.getEntity_type());
				entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
				entity.put("no_of_parties", customerEntity.getNo_of_parties());
				entity.put("no_of_documents", customerEntity.getNo_of_documents());
				entity.put("report_status", request_status);
				entity.put("validity", customerEntity.getValidity());
				entity.put("created_date", customerEntity.getCreated_date());
				entity.put("isRead", request1.getIsRead());
				entity.put("isEdited", request1.getIsEdited());

				boolean validityValid = true;
				if (misReportDTO.getValidity() != null) {
					if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
						validityValid = false;
					}
				}
				if (validityValid) {
					if (customerEntity.getValidity() != null) {
						if (customerEntity.getValidity().equals("valid")) {
							valid++;
						} else {
							invalid++;
						}

					} else {
						invalid++;
					}
					filteredEntitiesArray.put(entity);
				}
			}
		}

		JSONObject summary_count_data = new JSONObject();
		summary_count_data.put("total_request", filteredEntitiesArray.length());
		summary_count_data.put("generated", generated);
		summary_count_data.put("pending", pending);
		summary_count_data.put("valid", valid);
		summary_count_data.put("invalid", invalid);

		JSONObject filteredData = new JSONObject();
		filteredData.put("data", filteredEntitiesArray);
		filteredData.put("summary_count_data", summary_count_data);
		return filteredData.toString();
	}
		catch (JSONException e) {
	    	e.printStackTrace();
	    }
	    return null;
	    }

	public List<BranchMaster> getAllBranchesOfUser(HttpSession httpSession) {
		List<LocationMaster> locationMasters = getAllChildLocations(httpSession);
		List<BranchMaster> branchMasterList = new ArrayList<>();
		if (locationMasters != null) {
			branchMasterList = branchMasterService.findByUserLocation(locationMasters);
		}
		return branchMasterList;
	}

	public List<LocationMaster> getAllChildLocations(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		if (currentUser.getLocation() != null) {
			Long locationId = currentUser.getLocation();
			List<LocationMaster> locationMasters = locationMasterService.getChildLocations(locationId);
			return locationMasters;
		} else {

		}
		return new ArrayList<>();
	}

	/* function written to get all the customers data */
	@RequestMapping(value = "showCustomers", method = RequestMethod.GET)
	public @ResponseBody List<CustomerMaster> showCustomers() {
		List<CustomerMaster> customerData = customerMasterService.getAll();
		return customerData;
	}

	/* Function written to get the all the details of the user */
	@RequestMapping(value = "showUsers", method = RequestMethod.GET)
	public @ResponseBody List<UserMaster> showUsers(){
		List<UserMaster> userData = userMasterService.getAll();
		return userData;
	}
	
@RequestMapping(value="getRequestCount",method=RequestMethod.GET)
public @ResponseBody String getRequestCount(){
	return requestService.getRequestCountOfAllUser().toString();
}


	/* Method to get all the Branches that are registered */
	public List<BranchMaster> getAllBranches() {
		List<BranchMaster> branchMasterList_ = new ArrayList<>();
		branchMasterList_ = branchMasterService.getAll();
		return branchMasterList_;
	}

	/* Method to get all the Customers */
	public List<CustomerMaster> getAllCustomers() {
		List<CustomerMaster> customerMasterList_ = new ArrayList<>();
		customerMasterList_ = customerMasterService.getAll();
		return customerMasterList_;
	}

	/* Mehtod to get all the Users */
	public List<UserMaster> getAllUsers() {
		List<UserMaster> userMasterList_ = new ArrayList<>();
		userMasterList_ = userMasterService.getAll();
		return userMasterList_;
	}
}