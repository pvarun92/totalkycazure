package com.karza.kyc.web;

import com.karza.kyc.dto.BranchMasterDTO;
import com.karza.kyc.dto.LocationMasterDTO;
import com.karza.kyc.dto.UserMasterDTO;
import com.karza.kyc.mapper.BranchMasterMapper;
import com.karza.kyc.mapper.LocationMasterMapper;
import com.karza.kyc.mapper.UserMasterMapper;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.LocationMaster;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.service.*;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Fallon Software on 4/13/2016.
 */
@Controller
@RequestMapping(AdminController.URL)
public class AdminController {
    static final String URL = "admin";
    private static final Logger logger = Logger.getLogger(UserController.class);
    @Autowired
    CustomerMasterService customerMasterService;
    @Autowired
    UserMasterService userMasterService;
    @Autowired
    UserService userService;
    @Autowired
    BranchMasterService branchMasterService;
    @Autowired
    LocationMasterService locationMasterService;

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public ModelAndView getUsersPage() {
        ModelAndView modelAndView = new ModelAndView("users");
        return modelAndView;
    }

    @RequestMapping(value = "locations", method = RequestMethod.GET)
    public ModelAndView getLocationsPage() {
        ModelAndView modelAndView = new ModelAndView("locations");
        return modelAndView;
    }

    @RequestMapping(value = "branches", method = RequestMethod.GET)
    public ModelAndView getBranchesPage() {
        ModelAndView modelAndView = new ModelAndView("branches");
        return modelAndView;
    }

    @RequestMapping(value = "branchesData", method = RequestMethod.GET)
    public
    @ResponseBody
    List<BranchMaster> branchesData() throws JSONException {
        List<BranchMaster> branchMasterList = branchMasterService.findAll();
        return branchMasterList;
    }

    @RequestMapping(value = "createBranch", method = RequestMethod.POST)
    public
    @ResponseBody
    String createBranch(@RequestBody BranchMasterDTO branchMasterDTO, HttpServletRequest request) {
    	try{
        BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
        BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
        branchMasterService.save(branchMaster);
        String data = "Branch Saved";
        JSONObject responseJson = new JSONObject();
        responseJson.put("result", data);
        return responseJson.toString();
    	}
    	catch (JSONException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

    @RequestMapping(value = "updateBranch", method = RequestMethod.POST)
    public
    @ResponseBody
    String updateBranch(@RequestBody BranchMasterDTO branchMasterDTO, HttpServletRequest request){
    	try
    	{
        BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
        BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
        branchMasterService.update(branchMaster);
        JSONObject responseJson = new JSONObject();
        responseJson.put("result", "Branch Updated");
        return responseJson.toString();
    	}
    	catch (JSONException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

    @RequestMapping(value = "locationsData", method = RequestMethod.GET)
    public
    @ResponseBody
    List<LocationMaster> locationsData() {
//        todo:- find current user and call findByCustomerId method and get all locations by Current logged in Customer
        List<LocationMaster> locationMasterList = locationMasterService.findAll();
        return locationMasterList;
    }

    @RequestMapping(value = "createLocation", method = RequestMethod.POST)
    public
    @ResponseBody
    String createLocation(@RequestBody LocationMasterDTO locationMasterDTO, HttpServletRequest request){
    	try{
        LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
        LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
        //code for get currentCustomer
        //Todo:-add currentCustomerId in customerMasterId
        //now it is default 1
        locationMaster.setCustomerMasterId(Long.parseLong("1"));
        locationMasterService.save(locationMaster);
        String data = "Location Saved";
        JSONObject responseJson = new JSONObject();
        responseJson.put("result", data);
        return responseJson.toString();
    	}
    	catch (JSONException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

    @RequestMapping(value = "updateLocation", method = RequestMethod.POST)
    public
    @ResponseBody
    String updateLocation(@RequestBody LocationMasterDTO locationMasterDTO, HttpServletRequest request){
    	try
    	{
        LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
        LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
        locationMasterService.update(locationMaster);
        JSONObject responseJson = new JSONObject();
        responseJson.put("result", "Location Updated");
        return responseJson.toString();
    	}
    	catch (JSONException e) {
    		e.printStackTrace();
    	}
    	return null;
    }

    @RequestMapping(value = "usersData", method = RequestMethod.GET)
    public
    @ResponseBody
    List<UserMaster> usersData() throws JSONException {
        //call findByCustomerIdAndRole after getting current loggedIn customer
        List<UserMaster> userMasterList = userMasterService.findByRole("User");
        return userMasterList;
    }

    @RequestMapping(value = "createUser", method = RequestMethod.POST)
    public
    @ResponseBody
    String createUser(@RequestBody UserMasterDTO userMasterDTO, HttpServletRequest request){
    	try
    	{
        UserMasterMapper userMasterMapper = new UserMasterMapper();
        UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
        userMaster.setUserRole("User");
        //code for get currentCustomer
        //Todo:-add currentCustomerId in customerMasterId
        //now it is default 1
        userMaster.setCustomerMasterId(Long.parseLong("1"));

        userMasterService.save(userMaster);
        String data = "User Saved";
        JSONObject responseJson = new JSONObject();
        responseJson.put("result", data);
        return responseJson.toString();
    }
	catch (JSONException e) {
		e.printStackTrace();
	}
	return null;
    }

    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    public
    @ResponseBody
    String updateUser(@RequestBody UserMasterDTO userMasterDTO, HttpServletRequest request) {
    	try{
        UserMasterMapper userMasterMapper = new UserMasterMapper();
        UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
        userMaster.setUserRole("User");
        userMasterService.update(userMaster);
        JSONObject responseJson = new JSONObject();
        responseJson.put("result", "User Updated");
        return responseJson.toString();
    
    }
	catch (JSONException e) {
		e.printStackTrace();
	}
	return null;
    }

}