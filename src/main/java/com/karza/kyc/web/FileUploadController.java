package com.karza.kyc.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadController {
	
	private static final Logger logger = LoggerFactory
			.getLogger(FileUploadController.class);

	/*private static final String upload_folder = "F:\\Upload File";

	public FileUploadController() {
		
	}
	
	@Context
	private UriInfo context;
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@FormDataParam("file") InputStream uploadedInputStream,
							  @FormDataParam("file") FormDataContentDisposition fileDetail){
			if(uploadedInputStream == null || fileDetail == null)
				return Response.status(400).entity("Invalid Form Data").build();
			
			try {
					createFolderIfNotExists(upload_folder);
				
			} catch (SecurityException e) {
				return Response.status(500).entity("Can not create destination folder on server").build();
			}
		String uploadFileLocation = upload_folder+fileDetail.getFileName();
		try {
			saveToFile(uploadedInputStream,uploadFileLocation);
		} catch (IOException e1) {
			return Response.status(500).entity("Can not save file").build();
		}
			return Response.status(200).entity("File Saved to "+uploadFileLocation).build();
	}
	
	private void saveToFile(InputStream inputStream, String target) throws IOException {
		OutputStream out = null;
		int read = 0;
		byte[] bytes = new byte[1024];
		out = new FileOutputStream(new File(target));
		while((read = inputStream.read(bytes))!= -1){
			out.write(bytes,0,read);
		}
		out.flush();
		out.close();
	}
	
	private void createFolderIfNotExists(String dirName) throws SecurityException{
		File theDir = new File(dirName);
		if(!theDir.exists()){
			theDir.mkdir();
		}
	}
	*/
	private static final Logger logger1 = LoggerFactory
			.getLogger(FileUploadController.class);

	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file) {

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
		//		String rootPath1 = System.getProperty("user.home");
				String rootPath = "F:\\Upload File";
				File dir = new File(rootPath + File.separator);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

				return "You successfully uploaded file=" + name;
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}
	}

	/**
	 * Upload multiple file using Spring Controller
	 */
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadMultipleFileHandler(@RequestParam("name") String[] names,
			@RequestParam("file") MultipartFile[] files) {

		if (files.length != names.length)
			return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("user.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

				message = message + "You successfully uploaded file=" + name
						+ "";
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		return message;
	}
	
}
