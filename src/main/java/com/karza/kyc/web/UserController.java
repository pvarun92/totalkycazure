package com.karza.kyc.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.h2.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import com.karza.kyc.dto.BranchMasterDTO;
import com.karza.kyc.dto.LocationMasterDTO;
import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.dto.UserMasterDTO;
import com.karza.kyc.mapper.BranchMasterMapper;
import com.karza.kyc.mapper.LocationMasterMapper;
import com.karza.kyc.mapper.UserMasterMapper;
import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.CustomerMaster;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.ElectricityBoard;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.LocationMaster;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.SubscriptionMaster;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.UserSessionDetail;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.BranchMasterService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTFrequencyService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.CustomerEntityService;
import com.karza.kyc.service.CustomerMasterService;
import com.karza.kyc.service.DrivingLicenceCOVService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.EPFOResponseService;
import com.karza.kyc.service.ESICContriService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityBoardService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.LocationMasterService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxFrequencyService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.RequestService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.SubscriptionMasterService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.UserMasterService;
import com.karza.kyc.service.UserService;
import com.karza.kyc.service.UserSessionDetailService;
import com.karza.kyc.service.VATFrequencyService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;
import com.karza.kyc.util.ApiCallHelper;
import com.karza.kyc.util.ApiCallHelperUtil;
import com.karza.kyc.util.CustomerEntityHelper;
import com.karza.kyc.util.DocumentHelper;
import com.karza.kyc.util.EmailHelper;
import com.karza.kyc.util.KYCUtil;
import com.karza.kyc.util.PasswordEncryptDecrypt;
import com.karza.kyc.util.RandomPasswordGenerator;
import com.karza.kyc.util.Test;
import com.karza.kyc.validators.DocumentValidatorException;
import com.karza.kyc.validators.ItrValidator;
import com.karza.kyc.validators.ValidatorHelper;
import com.karza.kyc.web.util.UserControllerEditRequestUtil;
import com.karza.kyc.web.util.UserControllerKYCReportUtil;
import com.karza.kyc.web.util.UserControllerUtil;
import com.karza.kyc.web.util.UserControlllerEntityUtil;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.blob.*;


@Controller
@RequestMapping(UserController.URL)
public class UserController {
	static Cipher cipher;
	static final String URL = "";
	private static final Logger logger = Logger.getLogger(UserController.class);
	// private static final Logger logger =Logger.getLogger("Hibernate Logging
	// Disabled");
	private static HashMap<String, Connection> connections = new HashMap<String, Connection>();

	@Autowired
	UserService userService;

	@Autowired
	CustomerEntityService customerEntityService;

	@Autowired
	PanCardService panCardService;

	@Autowired
	ServiceTaxService serviceTaxService;

	@Autowired
	ExciseService exciseService;

	@Autowired
	VATService vatService;

	@Autowired
	VATFrequencyService vatFrequencyService;

	@Autowired
	CSTService cstService;

	@Autowired
	CSTFrequencyService cstFrequencyService;

	@Autowired
	ProfessionalTaxService professionalTaxService;

	@Autowired
	ProfessionalTaxFrequencyService professionalTaxFrequencyService;

	@Autowired
	LLPINService llpinService;

	@Autowired
	AadharCardService aadharCardService;

	@Autowired
	RequestService requestService;

	@Autowired
	IECService iecService;

	@Autowired
	DrivingLicenceMasterService dlService;

	@Autowired
	DrivingLicenceCOVService drivingLicenceCOVService;

	@Autowired
	CINService cinService;

	@Autowired
	FCRNService fcrnService;

	@Autowired
	FLLPINService fllpinService;

	@Autowired
	PassportService passportService;

	@Autowired
	CustomerMasterService customerMasterService;

	@Autowired
	UserMasterService userMasterService;

	@Autowired
	UserSessionDetailService userSessionDetailService;

	@Autowired
	BranchMasterService branchMasterService;

	@Autowired
	LocationMasterService locationMasterService;

	@Autowired
	ItrService itrService;

	@Autowired
	VoterIDService voterIDService;

	@Autowired
	ElectricityService electricityService;

	@Autowired
	ElectricityBoardService elecBoardService;

	@Autowired
	SubscriptionMasterService subscriptionMasterService;

	@Autowired
	TelephoneService telephoneService;
	
	@Autowired
	EPFORequestService epfoReqService;
	
	@Autowired
	EPFOResponseService epfoResService;
	
	@Autowired
	LPGService lpgService;
	
	@Autowired
	ESICService esicService;
	@Autowired
	ESICContriService esicContriService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get(Model model, @ModelAttribute UserCommand userCommand, HttpSession httpSession) {
		UserMaster currentUser = null;
		ModelAndView modelAndView;
		currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		if (currentUser.getUserRole().equals("Sysadmin")) {
			modelAndView = new ModelAndView("redirect:sysadmin");
		} else {
			modelAndView = new ModelAndView("redirect:simple_sidebar_menu");
		}
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String post(Model model, @Valid UserCommand userCommand, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("userGrid", userService.findAll());
			return URL;
		}
		PasswordEncoder passwordEncoder = new StandardPasswordEncoder();
		userCommand.setPassword(passwordEncoder.encode(userCommand.getPassword()));
		userService.save(userCommand);

		return "redirect:" + URL;
	}

	@RequestMapping(method = RequestMethod.POST, params = "_method=put")
	public String put(Model model, @Valid UserGrid userGrid, BindingResult result) {
		if (result.hasErrors()) {
			userService.updateWithAll(userGrid);
			return URL;
		}
		userService.saveAll(userGrid);
		return "redirect:" + URL;
	}

	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public RedirectView checkLogin(HttpServletRequest request,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "customer_id", required = false) String customer_id, HttpSession httpSession) {
		RedirectView redirectView;
	//	password = PasswordEncryptDecrypt.md5Hash(password);
		UserMaster userMaster = userMasterService.getUserByUsernameAndPassword(email, password);
		try {
			if (userMaster != null) {
				UserMaster userMaster1 = userMaster;
				userMaster1.setAutoGeneratedPasswordExpired(Boolean.TRUE);
				userMaster1.setLoginCount(0);//Check It Out;
				userMasterService.update(userMaster1);
				Integer noOfActiveSession = userSessionDetailService.getLoggedInUserCount(userMaster.getId());
				if (noOfActiveSession < userMaster.getParallelSessionLimit()) {
					httpSession.setAttribute("currentUser", userMaster);
					httpSession.setAttribute("workLog", "MY");
					CustomerMaster customerMaster = customerMasterService.get(userMaster.getCustomerMasterId());
					httpSession.setAttribute("account", customerMaster);
					InetAddress IP;

					IP = InetAddress.getLocalHost();

					UserSessionDetail userSessionDetail1 = userSessionDetailService
							.getBySessionId(request.getRequestedSessionId());
					if (userSessionDetail1 != null) {
						userSessionDetail1.setUserStatus("Active");
						userSessionDetail1.setUpdatedAt(new Date());
						userSessionDetailService.update(userSessionDetail1);
					} else {
						UserSessionDetail userSessionDetail = new UserSessionDetail(
								new Date(request.getSession().getLastAccessedTime()),
								new Date(request.getSession().getCreationTime()), userMaster.getId(),
								request.getSession().getId(), IP.getHostAddress(), "Active");
						Long currentLoginUserSessionId = userSessionDetailService.save(userSessionDetail);
					}
					if (userMaster.getUserRole().equals("Sysadmin")) {
						redirectView = new RedirectView("sysadmin");
					} else {
						redirectView = new RedirectView("simple_sidebar_menu");
					}
				} else {
					redirectView = new RedirectView("login?maxLoggedinSession=true");
				}
			} else {
				String isExist = userMasterService.IsEmailExist(email);
				if (isExist != null) {
					UserMaster userMaster1 = userMasterService.getUserByUsername(email);
					if (userMaster1 != null) {
						if (StringUtils.equals(userMaster1.getUserRole().toString(), "Edit")
								|| StringUtils.equals(userMaster1.getUserRole().toString(), "View")) {
							int count = userMaster1.getLoginCount();
							if (count == 5) {
								userMaster1.setActive(Boolean.FALSE);
								userMasterService.update(userMaster1);
								redirectView = new RedirectView("login?suspended=true");
							} else {
								if (count < 5) {
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
									String todayDateWithoutTime = (sdf.format(new Date()));
									String lastLoginDate = (sdf.format(userMaster1.getUpdatedAt()));

									if (sdf.format(new Date()).compareTo(sdf.format(userMaster1.getUpdatedAt())) == 0) {
										count = count + 1;
										userMaster1.setUpdatedAt(new Date());
										userMaster1.setLoginCount(count);
										userMasterService.update(userMaster1);
									} else {
										count = 1;
										userMaster1.setUpdatedAt(new Date());
										userMaster1.setLoginCount(count);
										userMasterService.update(userMaster1);
									}
									redirectView = new RedirectView("login?error=true");
								} else {
									redirectView = new RedirectView("login?error=true");
								}
							}
						} else {
							redirectView = new RedirectView("login?error=true");
						}
					} else {
						redirectView = new RedirectView("login?error=true");
					}
				} else {
					redirectView = new RedirectView("login?error=true");
				}
			}
			return redirectView;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "changePassword", method = RequestMethod.POST)
	public @ResponseBody String changePassword(@RequestBody String request_data, HttpServletRequest request,
			HttpSession httpSession) {

		String data = "Request Saved";
		try {
			JSONObject requestData = new JSONObject(request_data);
			JSONObject responseJson = new JSONObject();

			if (requestData.get("new_password").toString().equals(requestData.get("confirm_password").toString())) {
				String tempCurrPassword = requestData.get("current_password").toString();
				tempCurrPassword = PasswordEncryptDecrypt.md5Hash(tempCurrPassword);
				UserMaster userMaster = userMasterService.getUserByUsernameAndPasswordForChangePassword(
						requestData.get("user_name").toString(),tempCurrPassword);

				if (userMaster != null) {
					String tempPassword = requestData.get("new_password").toString();
					tempPassword = PasswordEncryptDecrypt.md5Hash(tempPassword);
					userMaster.setPassword(tempPassword);
					userMaster.setAutoGeneratedPasswordFlag(Boolean.TRUE);
					userMasterService.update(userMaster);
					String emailStatus = new EmailHelper().sendEmailToUser(userMaster.getEmail(),
							tempPassword, userMaster, "changePassword");
					httpSession.setAttribute("currentUser", userMaster);
					responseJson.put("result", "password changed");
					return responseJson.toString();

				} else {
					data = "User name and password does not match";
					responseJson.put("result", data);
					return responseJson.toString();
				}

			} else {

				data = "New Password and confirm password is not matched ";
				responseJson.put("result", data);
				return responseJson.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "simple_sidebar_menu", method = RequestMethod.GET)
	public ModelAndView getSiteBarMenuPage() {
		ModelAndView modelAndView = new ModelAndView("simple_sidebar_menu");
		return modelAndView;
	}

	@RequestMapping(value = "forgotPassword", method = RequestMethod.GET)
	public ModelAndView getForgotPasswordPage() {
		ModelAndView modelAndView = new ModelAndView("forgotPassword");
		return modelAndView;
	}

	@RequestMapping(value = "terms", method = RequestMethod.GET)
	public ModelAndView getTermsPage() {
		ModelAndView modelAndView = new ModelAndView("terms");
		return modelAndView;
	}

	@RequestMapping(value = "disclaimer", method = RequestMethod.GET)
	public ModelAndView getDisclaimerPage() {
		ModelAndView modelAndView = new ModelAndView("disclaimer");
		return modelAndView;
	}

	@RequestMapping(value = "landing", method = RequestMethod.GET)
	public ModelAndView getDashboardPage() {
		ModelAndView modelAndView = new ModelAndView("landing");
		return modelAndView;
	}

	@RequestMapping(value = "new_request", method = RequestMethod.GET)
	public ModelAndView getNewRequestPage() {
		ModelAndView modelAndView = new ModelAndView("new_request");
		return modelAndView;
	}

	@RequestMapping(value = "drafts", method = RequestMethod.GET)
	public ModelAndView getDraftsPage() {
		ModelAndView modelAndView = new ModelAndView("drafts");
		return modelAndView;
	}
	/*For Bulk Verification*/
	@RequestMapping(value = "bulk", method = RequestMethod.GET)
	public ModelAndView getBulkPage() {
		ModelAndView modelAndView = new ModelAndView("bulk");
		return modelAndView;
	}

	@RequestMapping(value = "pending_queue", method = RequestMethod.GET)
	public ModelAndView getPendingQueuePage() {
		ModelAndView modelAndView = new ModelAndView("pending_queue");
		return modelAndView;
	}

	@RequestMapping(value = "history", method = RequestMethod.GET)
	public ModelAndView getHistoryPage() {
		ModelAndView modelAndView = new ModelAndView("history");
		return modelAndView;
	}

	@RequestMapping(value = "mis", method = RequestMethod.GET)
	public ModelAndView getMisPage() {
		ModelAndView modelAndView = new ModelAndView("mis");
		return modelAndView;
	}

	/* To redirect the user to the specific page for the admin */
	@RequestMapping(value = "sysAdminMis", method = RequestMethod.GET)
	public ModelAndView getSysMisPage() {
		ModelAndView modelAndView = new ModelAndView("sysAdminMis");
		return modelAndView;
	}
	
	/*To Redirect the Admin to go to specific page in order to see the API Count*/
	@RequestMapping(value="sysAdminAPIMis",method = RequestMethod.GET)
	public ModelAndView getSysAdminAPIMisPage(){
		ModelAndView modelAndView = new ModelAndView("sysAdminAPIMis");
		return modelAndView;
	}

	@RequestMapping(value = "kyc_validation_report", method = RequestMethod.GET)
	public ModelAndView getKycValidationReportPage() {
		ModelAndView modelAndView = new ModelAndView("kyc_validation_report");
		return modelAndView;
	}

	@RequestMapping(value = "kyc_validation_report_print", method = RequestMethod.GET)
	public ModelAndView getKycValidationPrintReportPage() {
		ModelAndView modelAndView = new ModelAndView("kyc_validation_report_print");
		return modelAndView;
	}

	@RequestMapping(value = "users", method = RequestMethod.GET)
	public ModelAndView getUsersPage(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		ModelAndView modelAndView;
		if (StringUtils.equals(currentUser.getUserRole(), "Admin")) {
			modelAndView = new ModelAndView("users");
		} else {
			modelAndView = new ModelAndView("landing");
		}
		return modelAndView;
	}

	@RequestMapping(value = "locations", method = RequestMethod.GET)
	public ModelAndView getLocationsPage(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		ModelAndView modelAndView;
		if (StringUtils.equals(currentUser.getUserRole(), "Admin")) {
			modelAndView = new ModelAndView("locations");
		} else {
			modelAndView = new ModelAndView("landing");
		}
		return modelAndView;
	}

	@RequestMapping(value = "branches", method = RequestMethod.GET)
	public ModelAndView getBranchesPage(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		ModelAndView modelAndView;
		if (StringUtils.equals(currentUser.getUserRole(), "Admin")) {
			modelAndView = new ModelAndView("branches");
		} else {
			modelAndView = new ModelAndView("landing");
		}
		return modelAndView;
	}

	@RequestMapping(value = "adminPanelNew", method = RequestMethod.GET)
	public ModelAndView getadminPanelNewPage() {
		ModelAndView modelAndView = new ModelAndView("adminPanelNew");
		return modelAndView;
	}

	@RequestMapping(value = "newCustomerNew", method = RequestMethod.GET)
	public ModelAndView getSysAdminDashboardPage() {
		ModelAndView modelAndView = new ModelAndView("newCustomerNew");
		return modelAndView;
	}

	@RequestMapping(value = "userManagementNew", method = RequestMethod.GET)
	public ModelAndView getuserManagementNewPage() {
		ModelAndView modelAndView = new ModelAndView("userManagementNew");
		return modelAndView;
	}

	@RequestMapping(value = "userProfileNew", method = RequestMethod.GET)
	public ModelAndView getUserProfileNewPage() {
		ModelAndView modelAndView = new ModelAndView("userProfileNew");
		return modelAndView;
	}

	@RequestMapping(value = "newCreateUserNew", method = RequestMethod.GET)
	public ModelAndView getNewCreateUserNewPage() {
		ModelAndView modelAndView = new ModelAndView("newCreateUserNew");
		return modelAndView;
	}

	@RequestMapping(value = "newSubscriptionMasterNew", method = RequestMethod.GET)
	public ModelAndView getNewSubscriptionMasterNewPage() {
		ModelAndView modelAndView = new ModelAndView("newSubscriptionMasterNew");
		return modelAndView;
	}

	@RequestMapping(value = "masterManagementNew", method = RequestMethod.GET)
	public ModelAndView getMasterManagementNewPage() {
		ModelAndView modelAndView = new ModelAndView("masterManagementNew");
		return modelAndView;
	}

	@RequestMapping(value = "newBranchNew", method = RequestMethod.GET)
	public ModelAndView getNewBranchNewPage() {
		ModelAndView modelAndView = new ModelAndView("newBranchNew");
		return modelAndView;
	}

	@RequestMapping(value = "newLocationNew", method = RequestMethod.GET)
	public ModelAndView getNewLocationNewPage() {
		ModelAndView modelAndView = new ModelAndView("newLocationNew");
		return modelAndView;
	}

	@RequestMapping(value = "sysadmin", method = RequestMethod.GET)
	public ModelAndView getSysadminPage() {
		ModelAndView modelAndView = new ModelAndView("sysadmin");
		return modelAndView;
	}

	@RequestMapping(value = "sysAdminDashboardNew", method = RequestMethod.GET)
	public ModelAndView getsysAdminDashboardNewPage() {
		ModelAndView modelAndView = new ModelAndView("sysAdminDashboardNew");
		return modelAndView;
	}

	@RequestMapping(value = "sysAdminCustomerManagement", method = RequestMethod.GET)
	public ModelAndView getSysAdminDashboardCustomerManagementPage() {
		ModelAndView modelAndView = new ModelAndView("sysAdminCustomerManagement");
		return modelAndView;
	}

	@RequestMapping(value = "sysAdminUserManagementNew", method = RequestMethod.GET)
	public ModelAndView getSysAdminUserManagementPage() {
		ModelAndView modelAndView = new ModelAndView("sysAdminUserManagementNew");
		return modelAndView;
	}

	@RequestMapping(value = "sysAdminMasterManagementNew", method = RequestMethod.GET)
	public ModelAndView getSysAdminMasterManagementPage() {
		ModelAndView modelAndView = new ModelAndView("sysAdminMasterManagementNew");
		return modelAndView;
	}

	@RequestMapping(value = "customerProfileNew", method = RequestMethod.GET)
	public ModelAndView getCustomerProfileNewPage() {
		ModelAndView modelAndView = new ModelAndView("customerProfileNew");
		return modelAndView;
	}

	@RequestMapping(value = "getCurrentUser", method = RequestMethod.GET)
	public @ResponseBody UserMaster getCurrentUser(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		// json.put("result",data);
		return currentUser;
	}

	@RequestMapping(value = "getCurrentWorkLog", method = RequestMethod.GET)
	public @ResponseBody String getCurrentWorkLog(HttpSession httpSession) throws Exception {
		String currentWorkLog = (String) httpSession.getAttribute("workLog");
		JSONObject json = new JSONObject();
		json.put("workLog", currentWorkLog);
		return json.toString();
	}

	@RequestMapping(value = "setCurrentWorkLog", method = RequestMethod.POST)
	public @ResponseBody String setCurrentWorkLog(@RequestBody String currentWorkLog, HttpSession httpSession)
			throws Exception {
		httpSession.setAttribute("workLog", currentWorkLog);
		String newWorkLog = (String) httpSession.getAttribute("workLog");
		JSONObject json = new JSONObject();
		json.put("workLog", newWorkLog);
		return json.toString();
	}

	@RequestMapping(value = "landingData", method = RequestMethod.GET)
	public @ResponseBody String getLandingData(HttpSession httpSession) {
		List<CustomerEntity> historyData = getUserEntitiesByStatus(httpSession, "submitted");
		List<CustomerEntity> pendingData = getUserEntitiesByStatus(httpSession, "in_queue");
		List<CustomerEntity> landingData = getUserEntitiesByStatus(httpSession, "landingData");
		try {
			int totalCount = 0;
			if (historyData != null && pendingData != null) {
				totalCount = historyData.size() + pendingData.size();
			}
			JSONArray landingDataArray = new JSONArray();
			if (landingData != null) {
				for (int i = 0; i < 5; i++) {
					if (landingData.size() > i) {
						CustomerEntity customerEntity = landingData.get(i);
						Request request = requestService.get(customerEntity.getRequest_id());
						JSONObject jsonObject = new JSONObject();
						if (customerEntity.getEntity_type().equals("Individual")) {
							jsonObject.put("name", customerEntity.getFirst_name() + " "
									+ customerEntity.getMiddle_name() + " " + customerEntity.getLast_name());
						} else {
							jsonObject.put("name", customerEntity.getEntity_name());
						}
						jsonObject.put("request_id", request.getId());
						jsonObject.put("action", request.getRequest_status());
						jsonObject.put("date", customerEntity.getCreated_date());
						landingDataArray.put(jsonObject);
					}
				}
			}
			JSONObject json = new JSONObject();
			json.put("recentActivity", landingDataArray);
			json.put("totalCount", totalCount);
			json.put("completedCount", historyData.size());
			json.put("pendingCount", pendingData.size());
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "draftsData", method = RequestMethod.GET)
	public @ResponseBody String getDraftsData(HttpSession httpSession) {
		String data = "Drafts Data Requested";
		List<CustomerEntity> drafts_data = getUserEntitiesByStatus(httpSession, "in_draft");
		try {
			JSONArray draftsData = new JSONArray();
			for (int i = 0; i < drafts_data.size(); i++) {
				BranchMaster branchMaster = branchMasterService.get(drafts_data.get(i).getBranchId());
				drafts_data.get(i).setBranch(branchMaster.getBranchName());
				JSONObject json = new JSONObject(drafts_data.get(i));
				draftsData.put(json);
			}
			JSONObject response = new JSONObject();
			response.put("data", draftsData);
			return response.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "discardDrafts", method = RequestMethod.POST)
	public @ResponseBody String discardDrafts(@RequestBody String selectedDrafts, HttpServletRequest request) {
		try {
			JSONObject request_data = new JSONObject(selectedDrafts);
			JSONArray selectedDraftList = (JSONArray) request_data.get("selected_drafts");
			for (int i = 0; i < selectedDraftList.length(); i++) {
				JSONObject obj = selectedDraftList.getJSONObject(i);
				if (obj != null) {
					Long customerEntityId = Long.parseLong(obj.get("id").toString());
					CustomerEntity customerEntity = customerEntityService.get(customerEntityId);
					Request request1 = requestService.get(customerEntity.getRequest_id());
					if (!request1.getRequest_status().equals("submitted")) {
						requestService.delete(request1);
					}
				}
			}
			JSONObject json = new JSONObject();
			json.put("data", "records deleted successfully");
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from DiscardDrafts " + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "historyData", method = RequestMethod.GET)
	public @ResponseBody String getHistoryData(HttpSession httpSession, HttpServletRequest request) {
		String data = "History Data Requested";
		List<CustomerEntity> submittedDdata = getUserEntitiesByStatus(httpSession, "submitted");
		JSONArray historyData = new JSONArray();
		try {
			for (int i = 0; i < submittedDdata.size(); i++) {
				CustomerEntity entity = submittedDdata.get(i);
				BranchMaster branchMaster = branchMasterService.get(entity.getBranchId());
				entity.setBranch(branchMaster.getBranchName());
				JSONObject json = new JSONObject(entity);
				Request request1 = requestService.get(entity.getRequest_id());
				json.put("isEdited", request1.getIsEdited());
				json.put("isRead", request1.getIsRead());
				historyData.put(json);
			}
			JSONObject response = new JSONObject();
			response.put("data", historyData);
			return response.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from get History Data " + e.getMessage().toString());
		}
		return null;
	}

	public String getKycDocumentStatus(Long customerEntityID) {
		String pendingDocumentList = "";
		PanCard panCard = panCardService.getByCustomerId(customerEntityID);
		if (panCard != null && StringUtils.equals(panCard.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "PAN Validation, ";
		}
		List<Itr> itrs = itrService.getByCustomerId(customerEntityID);
		if (itrs != null) {
			if (itrs.size() > 0) {
				for (int k = 0; k < itrs.size(); k++) {
					Itr itr = itrs.get(k);
					if (itr != null && StringUtils.equals(itr.getApiValidation(), "pending")) {
						pendingDocumentList = pendingDocumentList + "ITR Validation, ";
					}
				}
			}
		}
		AadharCard aadharCard = aadharCardService.getByCustomerId(customerEntityID);
		if (aadharCard != null && StringUtils.equals(aadharCard.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "AadharCard Validation, ";
		}

		IEC iec = iecService.getByCustomerId(customerEntityID);
		if (iec != null && StringUtils.equals(iec.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "IEC Validation, ";
		}

		DrivingLicenceMaster dl = dlService.getByCustomerId(customerEntityID);
		if (dl != null && StringUtils.equals(dl.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "Driving License Validation, ";
		}
		// to fetch the status of Electricity
		Electricity electricity = electricityService.getByCustomerId(customerEntityID);
		if (electricity != null && StringUtils.equals(electricity.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "Electricity Validation, ";
		}

		ServiceTax serviceTax = serviceTaxService.getByCustomerId(customerEntityID);
		if (serviceTax != null && StringUtils.equals(serviceTax.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "Service Tax Validation, ";
		}

		Excise excise = exciseService.getByCustomerId(customerEntityID);
		if (excise != null && StringUtils.equals(excise.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "Excise Validation, ";
		}

		LLPIN llpin = llpinService.getByCustomerId(customerEntityID);
		if (llpin != null && StringUtils.equals(llpin.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + ", LLPIN Validation";
		}

		FLLPIN fllpin = fllpinService.getByCustomerId(customerEntityID);
		if (fllpin != null && StringUtils.equals(fllpin.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "FLLPIN Validation, ";
		}

		CIN cin = cinService.getByCustomerId(customerEntityID);
		if (cin != null && StringUtils.equals(cin.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "CIN Validation, ";
		}

		FCRN fcrn = fcrnService.getByCustomerId(customerEntityID);
		if (fcrn != null && StringUtils.equals(fcrn.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "FCRN Validation, ";
		}

		VAT vat = vatService.getByCustomerId(customerEntityID);
		if (vat != null && StringUtils.equals(vat.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "VAT Validation, ";
		}

		CST cst = cstService.getByCustomerId(customerEntityID);
		if (cst != null && StringUtils.equals(cst.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "CST Validation, ";
		}

		ProfessionalTax pt = professionalTaxService.getByCustomerId(customerEntityID);
		if (pt != null && StringUtils.equals(pt.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "Profession Tax Validation, ";
		}

		Passport passport = passportService.getByCustomerId(customerEntityID);
		if (passport != null) {
			pendingDocumentList = pendingDocumentList + "Passport Validation, ";
		}
		
		Telephone telephone = telephoneService.getByCustomerId(customerEntityID);
		if(telephone != null && StringUtils.equals(telephone.getApiValidation(), "pending")){
			pendingDocumentList = pendingDocumentList + "Telephone Validation, ";
		}
		
		EPFORequest epfo = epfoReqService.getByCustomerID(customerEntityID);
		if(epfo != null && StringUtils.equals(epfo.getApiValidation(), "pending")){
			pendingDocumentList = pendingDocumentList + "EPFO Validation, ";
		}
		
		LPG lpg = lpgService.getByCustomerId(customerEntityID);
		if(lpg != null && StringUtils.equals(lpg.getApiValidation(), "pending")){
			pendingDocumentList = pendingDocumentList + "LPG Validation";
		}
		
		ESIC esic = esicService.getByCustomerId(customerEntityID);
		if(esic != null && StringUtils.equals(esic.getApiValidation(),"pending")){
			pendingDocumentList = pendingDocumentList + "ESIC Validation";
		}
		
		VoterID voterID = voterIDService.getByCustomerId(customerEntityID);
		if (voterID != null && StringUtils.equals(voterID.getApiValidation(), "pending")) {
			pendingDocumentList = pendingDocumentList + "Voter Id Validation, ";
		}
		if (!pendingDocumentList.equals("")) {
			StringBuilder b = new StringBuilder(pendingDocumentList);
			pendingDocumentList = b
					.replace(pendingDocumentList.lastIndexOf(","), pendingDocumentList.lastIndexOf(",") + 1, "")
					.toString();
		}
		return pendingDocumentList;
	}

	@RequestMapping(value = "autoMailer", method = RequestMethod.GET)
	public @ResponseBody String autoMailer() {
		JSONObject responseJson = new JSONObject();
		List<UserMaster> userMasters = userMasterService.getUserIdAndEmailByPendingEntities();
		JSONArray jsonArray = new JSONArray();
		try {
			if (!userMasters.isEmpty()) {
				for (int i = 0; i < userMasters.size(); i++) {
					Long userMasterId = userMasters.get(i).getId();
					List<CustomerEntity> customerEntities = customerEntityService
							.getIdAndUserMasterIdByRequestStatus(userMasterId);
					if (!customerEntities.isEmpty()) {
						jsonArray = new JSONArray();
						for (int j = 0; j < customerEntities.size(); j++) {
							responseJson = new JSONObject();
							String pendingDocumentList = "";
							pendingDocumentList = getKycDocumentStatus(customerEntities.get(j).getId());
							responseJson.put("requestId", customerEntities.get(j).getRequest_id().toString());
							responseJson.put("entityName", customerEntities.get(j).getEntity_name());
							responseJson.put("entityType", customerEntities.get(j).getEntity_type());
							responseJson.put("PendingDocumentList", pendingDocumentList);
							jsonArray.put(responseJson);
						}
						String emailStatus = new EmailHelper().sendPendingEntityDetailToUser(
								userMasters.get(i).getEmail(), userMasters.get(i).getUserName(), jsonArray);
					} else {
						System.out.println("Entities not found");
					}
				}
			} else {
				System.out.println("userMasters not found");
			}
			return jsonArray.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from the AutoMailer" + e.getMessage().toString());
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Exception from the AutoMailer" + e.getMessage().toString());

		}
		return null;
	}

	public List<CustomerEntity> getUserEntitiesByStatus(HttpSession httpSession, String request_status) {
		List<CustomerEntity> customerEntities = new ArrayList<>();
		List<BranchMaster> branchMasterList = getAllBranchesOfUser(httpSession);
		if (branchMasterList != null) {
			customerEntities = requestService.getEntityByBranchesAndStatus(branchMasterList, request_status,
					httpSession);
		}
		return customerEntities;
	}

	@RequestMapping(value = "misData", method = RequestMethod.GET)
	public @ResponseBody String getMisData() {
		String data = "MIS Data Requested";
		JSONObject json = new JSONObject();
		try {
			json.put("result", data);

			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*Method to implement Forgot password*/
	@RequestMapping(value="forgotPassword",method = RequestMethod.POST)
	public @ResponseBody String forgotPassword(@RequestBody String request_data,HttpSession session, HttpServletRequest servletRequest){
		
		String data ="Forgot Password Request";
		try {
			JSONObject requestData = new JSONObject(request_data);
			JSONObject responseData = new JSONObject();
			if(requestData.get("forgotPassword").toString().equals(requestData.get("confirmPassword").toString())){
				//UserMaster usrMaster = userMasterService.getUserByUsername(request_data)
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	@RequestMapping(value = "queueData", method = RequestMethod.GET)
	public @ResponseBody String getQueueData(HttpServletRequest request) {
		List<CustomerEntity> submittedDdata = getUserEntitiesByStatus(request.getSession(), "in_queue");
		JSONArray historyData = new JSONArray();
		try {
			for (int i = 0; i < submittedDdata.size(); i++) {
				CustomerEntity entity = submittedDdata.get(i);
				BranchMaster branchMaster = branchMasterService.get(entity.getBranchId());
				entity.setBranch(branchMaster.getBranchName());
				JSONObject json = new JSONObject(entity);
				historyData.put(json);
			}
			JSONObject response = new JSONObject();
			response.put("data", historyData);
			return response.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Queue Data" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "dataCount", method = RequestMethod.GET)
	public @ResponseBody String getDataCount(HttpSession httpSession) {
		JSONObject json = new JSONObject();
		try {
			List<CustomerEntity> drafts = getUserEntitiesByStatus(httpSession, "in_draft");
			List<CustomerEntity> history = getUserEntitiesByStatus(httpSession, "history_count");
			List<CustomerEntity> pendingInQueue = getUserEntitiesByStatus(httpSession, "in_queue");
			UserMaster userMaster = (UserMaster) httpSession.getAttribute("currentUser");
			List<UserMaster> userMasters = userMasterService.findByCustomerIdAndRole(userMaster.getCustomerMasterId(),
					"User");
			List<BranchMaster> branchMasters = branchMasterService.findByCustomerId(userMaster.getCustomerMasterId());
			List<LocationMaster> locationMasters = locationMasterService
					.findByCustomerId(userMaster.getCustomerMasterId());
			json.put("draft_count", drafts.size());
			json.put("pending_in_queue_count", pendingInQueue.size());
			json.put("history_count", history.size());
			json.put("user_count", userMasters.size());
			json.put("branch_count", branchMasters.size());
			json.put("location_count", locationMasters.size());
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from the Data Count " + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "deleteEntity", method = RequestMethod.POST)
	public @ResponseBody String deleteEntity(@RequestBody String selectedRequestID, HttpServletRequest request) {
		try {
			JSONObject request_data = new JSONObject(selectedRequestID);
			if (request_data != null) {
				String selectedRequestId = request_data.get("selectedRequestId").toString();
				if (!selectedRequestId.isEmpty()) {
					Long id = Long.parseLong(selectedRequestId);
					CustomerEntity customerEntity = customerEntityService.get(id);
					Request request1;
					if (customerEntity != null) {
						request1 = requestService.get(customerEntity.getRequest_id());
						if (!request1.getRequest_status().equalsIgnoreCase("submitted")) {
							customerEntityService.delete(customerEntity);
							if (request1 != null) {
								int entityCount = customerEntityService.getByRequestId(request1.getId()).size();
								if (entityCount == 0) {
									requestService.delete(request1);
								}
							}
						}
					}
				}
			}
			JSONObject json = new JSONObject();
			json.put("data", "entity deleted successfully");
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from DeleteEntity" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "deleteRequest", method = RequestMethod.POST)
	public @ResponseBody String deleteRequest(@RequestBody String request_id, HttpServletRequest request)
			throws JSONException {
		JSONObject request_data = new JSONObject(request_id);
		if (request_data != null) {
			String selected_request_id = request_data.get("requestId").toString();
			if (!selected_request_id.isEmpty()) {
				Long id = Long.parseLong(selected_request_id);
				Request request1 = requestService.get(id);
				if (request1 != null) {
					List<CustomerEntity> customerEntityList = customerEntityService.getByRequestId(request1.getId());
					for (int i = 0; i < customerEntityList.size(); i++) {
						CustomerEntity customerEntity = customerEntityList.get(i);
						if (customerEntity != null) {
							customerEntityService.delete(customerEntity);
						}
					}
					if (!request1.getRequest_status().equals("submitted")) {
						requestService.delete(request1);
					}
				}
			}
		}
		JSONObject json = new JSONObject();
		json.put("data", "Request deleted successfully");
		return json.toString();
	}

	private static Request handleRequestData(String request_data, HttpServletRequest request,
			JSONArray requestJSONArray, RequestService requestService) throws JSONException {
		Request newRequest = new Request();
		Request existingRequest = null;
		UserMaster current_user = (UserMaster) request.getSession().getAttribute("currentUser");
		if (requestJSONArray.length() > 0) {
			String reqIDStr = KYCUtil.checkKeyAndGetValueFromJSON(requestJSONArray.getJSONObject(0), "request_id")
					.toString();
			// If reqIDStr is not empty : Means that this is a new Save Request
			Long long_id;
			if (!reqIDStr.isEmpty()) {
				long_id = Long.parseLong(reqIDStr);
				existingRequest = requestService.get(long_id);
			}
		}
		if (existingRequest == null) {
			// Means It's a new request
			newRequest.setRequest_status("in_draft");
			newRequest.setIsEdited(Boolean.FALSE);
			newRequest.setIsRead(Boolean.FALSE);
			newRequest.setCustomerMasterId(current_user.getCustomerMasterId());
			newRequest.setUserMasterId(current_user.getId());
			newRequest = requestService.save(newRequest);
		} else {
			// This is an existing request : Which has been saved previously but not yet submitted
			/* newRequest=existingRequest; */ 
			// Here Lets Check if we need topadte any field in the requests Table
			// If yes: Need to Update and Save in this part of the Code
			if (StringUtils.equals(existingRequest.getRequest_status().toString(), "submitted")) {
				newRequest.setIsEdited(Boolean.FALSE);
				newRequest.setIsRead(Boolean.FALSE);
				newRequest.setCustomerMasterId(current_user.getCustomerMasterId());
				newRequest.setUserMasterId(current_user.getId());
				newRequest.setRequest_status("in_draft");
				newRequest.setParentRequestId(existingRequest.getId());
				newRequest = requestService.save(newRequest);
			} else {

				if (existingRequest.getParentRequestId() == null) {
					newRequest.setIsEdited(Boolean.FALSE);
					newRequest.setIsRead(Boolean.FALSE);
					newRequest.setCustomerMasterId(current_user.getCustomerMasterId());
					newRequest.setUserMasterId(current_user.getId());
					newRequest.setId(existingRequest.getId());
					newRequest.setRequest_status(existingRequest.getRequest_status());
					requestService.update(newRequest);
				} else {
					Long parentRequestId = existingRequest.getParentRequestId();
					newRequest.setIsRead(Boolean.FALSE);
					newRequest.setIsEdited(Boolean.FALSE);
					newRequest.setCustomerMasterId(current_user.getCustomerMasterId());
					newRequest.setUserMasterId(current_user.getId());
					newRequest.setId(existingRequest.getId());
					newRequest.setParentRequestId(parentRequestId);
					newRequest.setRequest_status(existingRequest.getRequest_status());
					requestService.update(newRequest);
				}
			}
		}
		return newRequest;
	}

	private static CustomerEntity saveIthCustomerEntityData(CustomerEntity custEntityFrmReqJSON,
			String entityIDFrmReqJSON, JSONObject requestJSONData, Request newRequest,
			CustomerEntityService customerEntityService, UserMaster current_user, JSONObject customerEntityids) {
		try {
			if (custEntityFrmReqJSON != null) {
				Long customer_entity_id;
				CustomerEntity existingEntity = null;
				if (!entityIDFrmReqJSON.isEmpty()) {
					// Means This is a Fresh Entity Save Request from the UI
					
					if (!StringUtils.equals(newRequest.getRequest_status().toString(), "submitted")) {
						customer_entity_id = Long.parseLong(entityIDFrmReqJSON);
						existingEntity = customerEntityService.get(customer_entity_id);
						
					}

				}
				if (existingEntity == null) {
					custEntityFrmReqJSON.setUserMaster_id(current_user.getId());
					custEntityFrmReqJSON = customerEntityService.save(custEntityFrmReqJSON);
					
				} else {
					// Means It is an existing request: Because on the UI, I
					// already have an "id" ~> PK of customer_entity table
					
					custEntityFrmReqJSON.setId(existingEntity.getId());
					custEntityFrmReqJSON.setUserMaster_id(current_user.getId());
					customerEntityService.update(custEntityFrmReqJSON);
				
				}
				if (requestJSONData.has("form_id")) {
					String form_id = requestJSONData.getString("form_id");
					// CHeck why we need this field form_id i.e. tab_id on the UI
					customerEntityids.put(form_id, custEntityFrmReqJSON.getId());
				}
			}
			return custEntityFrmReqJSON;
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from SaveIthEntityID" + e.getMessage().toString());
		}
		return null;
	}

	private void removeUncheckedKYCDocsFromDraftedEntityDB(JSONObject requestJSONData,
			CustomerEntity custEntityFrmReqJSON) {
		DocumentHelper documentHelper = new DocumentHelper();
		try {
			documentHelper.removeUncheckedKYCDocsFromDraft(requestJSONData, custEntityFrmReqJSON, fcrnService,
					fllpinService, iecService, dlService, cinService, panCardService, serviceTaxService, exciseService,
					llpinService, aadharCardService, passportService, vatService, cstService, professionalTaxService,
					itrService, voterIDService, electricityService, telephoneService,epfoReqService,lpgService,esicService);
		} catch (DataAccessException e) {
			/* logger.info(e.getMessage()); */
			e.printStackTrace();
			System.out
					.println("Exception from RemoveUncheckedKYCDocsFromDraft of userController Class" + e.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
			System.out
					.println("Exception from RemoveUncheckedKYCDocsFromDraft of userController Class" + e.getMessage());

		}
	}

	private int saveIthEntityData(JSONArray requestJSONArray, int i, Request newRequest,
			CustomerEntityService customerEntityService, UserMaster current_user, JSONObject customerEntityids,
			List<JSONObject> invalidArray, List<JSONObject> invalidCheckSumArray,List<JSONObject> invalidCheckSumArrayUAN) {
		try {
			JSONObject requestJSONData = requestJSONArray.getJSONObject(i);
			JSONObject selfValidatorMap = new ValidatorHelper().doSelfValidation(requestJSONData);
			// The below line is related to Aadhar Validation
			JSONObject selfCheckSumValidatorMap = new ValidatorHelper().doCheckSumValidationForAadhaar(requestJSONData);
			// The below line is related to UAN Validation which is similar to Aadhar
			JSONObject selfCheckSumValidatorMapUAN = new ValidatorHelper().doCheckSumValidationForUAN(requestJSONData);
			String entityIDFrmReqJSON = requestJSONData.get("id").toString(); 
			// Fetches the CustomerMaster  Primary Key
			CustomerEntity custEntityFrmReqJSON;
			custEntityFrmReqJSON = new CustomerEntityHelper().getCustomerEntity(requestJSONData, newRequest.getId(),
					requestJSONArray.length());
			// Created CustomerEntity Object based on Current Entity Data on the UI
			custEntityFrmReqJSON = saveIthCustomerEntityData(custEntityFrmReqJSON, entityIDFrmReqJSON, requestJSONData,
					newRequest, customerEntityService, current_user, customerEntityids);
			removeUncheckedKYCDocsFromDraftedEntityDB(requestJSONData, custEntityFrmReqJSON);
			int no_of_documents = 0;
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForAadhar(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, aadharCardService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForCST(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, cstService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForDL(requestJSONData, no_of_documents,
					custEntityFrmReqJSON, dlService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForElectricity(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, electricityService, logger);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForExcise(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, exciseService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForIEC(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, iecService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForLLPIN(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, llpinService, fllpinService, cinService, fcrnService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForPAN(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, panCardService, itrService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForPT(requestJSONData, no_of_documents,
					custEntityFrmReqJSON, professionalTaxService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForVAT(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, vatService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForServiceTax(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, serviceTaxService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForVoter(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, voterIDService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInforForPassport(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, passportService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForTelephone(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, telephoneService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForEPFO(requestJSONData, 
					no_of_documents, custEntityFrmReqJSON, epfoReqService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForLPG(requestJSONData,
					no_of_documents, custEntityFrmReqJSON, lpgService);
			no_of_documents = UserControllerEditRequestUtil.checkAndSaveDraftInfoForESIC(requestJSONData, 
					no_of_documents, custEntityFrmReqJSON, esicService);
			custEntityFrmReqJSON.setNo_of_documents(no_of_documents);
			customerEntityService.update(custEntityFrmReqJSON);
			JSONObject invalidCheckSum = new JSONObject();
			JSONObject invalid = new JSONObject();
			JSONObject invalidCheckSumUAN = new JSONObject();
			invalid.put("invalid", selfValidatorMap);// selfValidatorMap selfCheckSumValidatorMapUAN
			invalidCheckSum.put("invalidCheckSum", selfCheckSumValidatorMap);
			invalidCheckSumUAN.put("invalidCheckSumUAN", selfCheckSumValidatorMapUAN);
			invalidArray.add(invalid);
			invalidCheckSumArray.add(invalidCheckSum);
			invalidCheckSumArrayUAN.add(invalidCheckSumUAN);
			return custEntityFrmReqJSON.getId().intValue();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from the SaveIthEntity Data" + e.getMessage().toString());
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("Exception from the SaveIthEntity Data" + e.getMessage().toString());
		}
		return 0;
	}

	@RequestMapping(value = "/saveRequest", method = RequestMethod.POST)
	public @ResponseBody String saveRequestData(@RequestBody String request_data, HttpServletRequest request) {
		String responseStr = "Request Saved";
		try {
			JSONObject responseJson = new JSONObject();
			JSONArray requestJSONArray = new JSONArray(request_data);
			List<JSONObject> invalidArray = new ArrayList<JSONObject>();
			List<JSONObject> invalidCheckSumArray = new ArrayList<JSONObject>();
			List<JSONObject> invalidCheckSumArrayUAN = new ArrayList<JSONObject>();
			UserMaster current_user = (UserMaster) request.getSession().getAttribute("currentUser");
			Request newRequest = UserController.handleRequestData(request_data, request, requestJSONArray,
					requestService);
			// Now Handle All Entities(i.e Tabs Corresponding to this RequestID)
			JSONObject customerEntityids = new JSONObject();
			customerEntityids.put("request_id", newRequest.getId());
			for (int i = 0; i < requestJSONArray.length(); i++) {
				saveIthEntityData(requestJSONArray, i, newRequest, customerEntityService, current_user,
						customerEntityids, invalidArray, invalidCheckSumArray,invalidCheckSumArrayUAN);
			}
			responseJson.put("result", responseStr);
			responseJson.put("invalid", invalidArray);
			responseJson.put("invalidCheckSum", invalidCheckSumArray);
			responseJson.put("invalidCheckSumUAN", invalidCheckSumArrayUAN);
			responseJson.put("customerEntityids", customerEntityids);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Save Request" + e.getMessage().toString());
		}

		return null;
	}

	@RequestMapping(value = "submitRequest", method = RequestMethod.POST)
	public @ResponseBody String submitRequestData(@RequestBody String kycRequestStr,
			HttpServletRequest httpServletReq) {
		String data = "Request Saved";
		try {
			JSONArray kycRequestJSON = new JSONArray(kycRequestStr);
			List<JSONObject> invalidArray = new ArrayList<JSONObject>();
			List<JSONObject> invalidCheckSumArray = new ArrayList<JSONObject>();
			List<JSONObject> invalidCheckSumArrayUAN = new ArrayList<JSONObject>();
			JSONObject responseJson = new JSONObject();
			Request kycReq = new Request("in_draft");
			Request existingRequest = null;
			UserMaster current_user = (UserMaster) httpServletReq.getSession().getAttribute("currentUser");
			if (kycRequestJSON.length() > 0) {
				String req_id = kycRequestJSON.getJSONObject(0).get("request_id").toString();
				Long long_id;
				if (!req_id.isEmpty()) {
					long_id = Long.parseLong(req_id);
					existingRequest = requestService.get(long_id);
					// Finds whether this request id exists in the request table
				}
			}
			if (existingRequest == null) {
				// New KYC-Request
				kycReq = UserControllerUtil.newKYCRequest(kycReq, current_user, requestService, userMasterService);
			} else {
				// Existing KYC-Request
				// kycReq=UserControllerUtil.existingKYCRequest(kycReq,
				// existingRequest, current_user, requestService,
				// userMasterService);
				kycReq = UserControllerUtil.existingKYCRequestVer1(kycReq, existingRequest, current_user,
						requestService, userMasterService);
			}
			for (int i = 0; i < kycRequestJSON.length(); i++) {
				JSONObject kycReqFormJSON = kycRequestJSON.getJSONObject(i);
				JSONObject selfValidatorMap = new ValidatorHelper().doSelfValidation(kycReqFormJSON);
				JSONObject selfCheckSumValidatorMap = new ValidatorHelper()
						.doCheckSumValidationForAadhaar(kycReqFormJSON);
				JSONObject selfCheckSumValidatorMapUAN = new ValidatorHelper().doCheckSumValidationForUAN(kycReqFormJSON);
				String entity_id = kycReqFormJSON.get("id").toString();
				// Id here is customer_entity_id (PK of customer_entity Table)
				System.out.println("EntityID " + entity_id);
				// CustomerEntityHelper class uses the data filled in the KYC
				// Request Form to
				// populate CustomerEntity Class Object; Holds client to by
				// KYC'ed details
				CustomerEntity customerEntity = new CustomerEntityHelper().getCustomerEntity(kycReqFormJSON,
						kycReq.getId(), kycRequestJSON.length());
				if (customerEntity != null) {
					Long long_entity_id;
					CustomerEntity existingEntity = null;
					if (!entity_id.isEmpty()) {
						
						if (!StringUtils.equals(existingRequest.getRequest_status().toString(), "submitted")) {
							long_entity_id = Long.parseLong(entity_id);
							existingEntity = customerEntityService.get(long_entity_id);
						}
					}
					if (existingEntity == null) {
						// Means There was no entry in the Customer ENtity Table
						// Corresponding to this Submitted Request
						customerEntity.setUserMaster_id(current_user.getId());
						customerEntity = customerEntityService.save(customerEntity);
						
					} else {
						// Means There was an entry in the Customer Entity Table
						// Corresponding to this Submitted Request
						customerEntity.setId(existingEntity.getId());
						customerEntity.setUserMaster_id(current_user.getId());
						customerEntityService.update(customerEntity);
						
					}
					DocumentHelper documentHelper = new DocumentHelper();
					try {
						documentHelper.removeUncheckedKYCDocsFromDraft(kycReqFormJSON, customerEntity, fcrnService,
								fllpinService, iecService, dlService, cinService, panCardService, serviceTaxService,
								exciseService, llpinService, aadharCardService, passportService, vatService, cstService,
								professionalTaxService, itrService, voterIDService, electricityService,
								telephoneService,epfoReqService,lpgService,esicService);
					} catch (Exception e) {
						// logger.info(e.getMessage());
						e.printStackTrace();
						System.out.println(
								"Exeption from Document Helper ,UserController class Line 1262" + e.getMessage());
					}
					int no_of_documents = 0;
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForPAN(kycReqFormJSON, no_of_documents,
							panCardService, customerEntity, selfValidatorMap, logger, itrService);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForVoterID(kycReqFormJSON,
							no_of_documents, voterIDService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForAadhaar(kycReqFormJSON,
							no_of_documents, aadharCardService, customerEntity, selfCheckSumValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForIEC(kycReqFormJSON, no_of_documents,
							iecService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForDL(kycReqFormJSON, no_of_documents,
							dlService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForElectricty(kycReqFormJSON,
							no_of_documents, electricityService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForServiceTax(kycReqFormJSON,
							no_of_documents, serviceTaxService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForExcise(kycReqFormJSON,
							no_of_documents, exciseService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForLLP(kycReqFormJSON, no_of_documents,
							llpinService, fllpinService, cinService, fcrnService, customerEntity, selfValidatorMap,
							logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForVAT(kycReqFormJSON, no_of_documents,
							vatService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForCST(kycReqFormJSON, no_of_documents,
							cstService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForProfTax(kycReqFormJSON,
							no_of_documents, professionalTaxService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForPassport(kycReqFormJSON,
							no_of_documents, passportService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForTelephone(kycReqFormJSON,
							no_of_documents, telephoneService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForEPFO(kycReqFormJSON, 
							no_of_documents, epfoReqService, customerEntity, selfCheckSumValidatorMapUAN, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForLPG(kycReqFormJSON, no_of_documents, 
							lpgService, customerEntity, selfValidatorMap, logger);
					no_of_documents = UserControllerUtil.checkAndInsertKYCRequestForESIC(kycReqFormJSON, no_of_documents,
							esicService, customerEntity, selfValidatorMap, logger);
					
					customerEntity.setNo_of_documents(no_of_documents);
					customerEntityService.update(customerEntity);
				}
				JSONObject invalidCheckSum = new JSONObject();
				JSONObject invalid = new JSONObject();
				JSONObject invalidCheckSumUAN = new JSONObject();
				invalid.put("invalid", selfValidatorMap);
				invalidCheckSum.put("invalidCheckSum", selfCheckSumValidatorMap);
				invalidCheckSumUAN.put("invalidCheckSumUAN",selfCheckSumValidatorMapUAN);
				invalidArray.add(invalid);
				invalidCheckSumArray.add(invalidCheckSum);
				invalidCheckSumArrayUAN.add(invalidCheckSumUAN);
			}
			kycReq.setRequest_status("in_queue");
			requestService.update(kycReq);
			responseJson.put("id", kycReq.getId());
			responseJson.put("result", data);
			responseJson.put("invalid", invalidArray);
			responseJson.put("invalidCheckSum", invalidCheckSumArray);
			responseJson.put("invalidCheckSumUAN", invalidCheckSumArrayUAN);
			connections.clear();
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "entityData", method = RequestMethod.POST)
	public @ResponseBody String getEntityDataById(@RequestBody String json_data, HttpServletRequest request) {
		try {
			JSONObject id = new JSONObject(json_data);
			String request_id = id.get("id").toString();
			UserMaster current_user = (UserMaster) request.getSession().getAttribute("currentUser");
			if (hasAccessToRequest(request.getSession(), Long.parseLong(request_id))) {
				List<CustomerEntity> entities = customerEntityService.getByRequestId(Long.parseLong(request_id));
				Request request1 = requestService.get(Long.parseLong(request_id));
				JSONArray entities_array = new JSONArray();
				entities_array = UserControlllerEntityUtil.saveEntityData(entities, request1,branchMasterService,
						userMasterService,voterIDService,panCardService,aadharCardService,itrService,serviceTaxService,
						esicService,esicContriService,userService,iecService,telephoneService,epfoReqService,lpgService,
						dlService,electricityService,llpinService,cinService,fcrnService,fllpinService,vatService,
						cstService,professionalTaxService,passportService,exciseService,epfoResService,drivingLicenceCOVService,
						vatFrequencyService,cstFrequencyService,professionalTaxFrequencyService,customerEntityService);
				return entities_array.toString();
			} else {
				return "no access";
			}
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from the Entity Data" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "validateDocuments", method = RequestMethod.POST)
	public @ResponseBody String validateDocuments(@RequestBody String request_data, HttpServletRequest request) {
		try {
			JSONObject request_form_data = new JSONObject(request_data);
			// add validation on each document by following way with name of
			// document and validation call and return final validation
			// ErrorStatusJson
			JSONObject selfValidatorMap = new ValidatorHelper().doSelfValidation(request_form_data);
			JSONObject selfCheckSumValidatorMap = new ValidatorHelper()
					.doCheckSumValidationForAadhaar(request_form_data);
			JSONObject selfCheckValidatorMapUAN = new ValidatorHelper().doCheckSumValidationForUAN(request_form_data);
			JSONObject json = new JSONObject();
			json.put("invalid", selfValidatorMap);// selfCheckSumValidatorMap
			json.put("invalidCheckSum", selfCheckSumValidatorMap);
			json.put("invalidCheckSumUAN", selfCheckValidatorMapUAN);
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from validateDocuments Data" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "validateItrDocuments", method = RequestMethod.POST)
	public @ResponseBody String validateItrDocuments(@RequestBody String request_data, HttpServletRequest request) {
		JSONObject request_form_data;
		JSONObject json = new JSONObject();
		try {
			json.put("invalid", true);
			request_form_data = new JSONObject(request_data);
			String dateObj = (request_form_data.get("date_of_filling").toString());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date dateOfFilling = null;
			if (!dateObj.isEmpty()) {
				String day = dateObj.split("/")[0];
				String month = dateObj.split("/")[1];
				String year = dateObj.split("/")[2];
				dateOfFilling = formatter.parse(year + "-" + month + "-" + day);
			} else {
				dateOfFilling = null;
			}
			Itr itrObj = new Itr(request_form_data.get("acknowledgement_number").toString(),
					request_form_data.get("assesment_year").toString(), dateOfFilling);
			// String data = null;
			new ItrValidator().isValid(itrObj);
			json.put("invalid", false);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (DocumentValidatorException e) {
			System.out.println("ITR ACk Document is INValid");
			e.printStackTrace();
		}
		return json.toString();
	}

	@RequestMapping(value = "getFilteredData", method = RequestMethod.POST)
	public @ResponseBody String getFilteredData(@RequestBody MisReportDTO misReportDTO, HttpServletRequest request,
			HttpSession httpSession) {
		Integer total_request, total_generated_request, total_pending_request;
		try {
			List<BranchMaster> userBranches = getAllBranchesOfUser(request.getSession());

			misReportDTO.formateData();
			if (misReportDTO.getReport_status() != null && misReportDTO.getReport_status().equals("in_draft")) {
				misReportDTO.setReport_status("in_queue");
			}
			List<CustomerEntity> filteredEntites = new ArrayList<>();
			if (misReportDTO.getBranchId() == null) {
				for (int i = 0; i < userBranches.size(); i++) {
					misReportDTO.setBranchId(userBranches.get(i).getId());
					List<CustomerEntity> filteredEntitesByBranch = customerEntityService
							.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
					filteredEntites.addAll(filteredEntitesByBranch);
				}
			} else {
				List<CustomerEntity> filteredEntitesByBranch = customerEntityService
						.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
				filteredEntites.addAll(filteredEntitesByBranch);
			}
			System.out.println("Filtered Entity Size -- > " + filteredEntites.size());
			JSONArray filteredEntitiesArray = new JSONArray();
			int generated = 0;
			int pending = 0;
			int valid = 0;
			int invalid = 0;
			int partial_valid = 0;
			int itr_valid = 0;
			int itr_invalid = 0;
			int in_draft = 0;
			for (int i = 0; i < filteredEntites.size(); i++) {
				JSONObject entity = new JSONObject();
				System.out.println("Customer Filtered Entity " + filteredEntites.get(1).toString());
				CustomerEntity customerEntity = filteredEntites.get(i);
				Request request1 = requestService.get(customerEntity.getRequest_id());
				String request_status = request1.getRequest_status();

				if (request_status.equals("in_queue")) {
					request_status = "Pending";
					pending++;
				} else if (request_status.equals("in_draft")) {
					request_status = "In draft";
					in_draft++;
				} else {
					request_status = "Generated";
					generated++;
				}
				if (customerEntity.getEntity_type().equals("Individual")) {
					entity.put("entity_name", customerEntity.getFirst_name() + " " + customerEntity.getMiddle_name()
							+ " " + customerEntity.getLast_name());
				} else {
					entity.put("entity_name", customerEntity.getEntity_name());
				}
				entity.put("report_id", customerEntity.getRequest_id());
				entity.put("report_date", customerEntity.getCreated_date());
				entity.put("entity_type", customerEntity.getEntity_type());
				entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
				entity.put("no_of_parties", customerEntity.getNo_of_parties());
				entity.put("no_of_documents", customerEntity.getNo_of_documents());
				entity.put("report_status", request_status);
				entity.put("validity", customerEntity.getValidity());
				entity.put("Itrvalidity", customerEntity.getItrValidity());
				boolean validityValid = true;
				if (misReportDTO.getValidity() != null) {
					if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
						validityValid = false;
					}
				}
				if (validityValid) {
					if (customerEntity.getValidity() != null) {
						if (customerEntity.getValidity().equals("valid")) {
							valid++;
						} else if (customerEntity.getValidity().equals("partial_valid")) {
							partial_valid++;
						} else {
							invalid++;
						}
					}
					if (!StringUtils.equals(customerEntity.getItrValidity(), "-")
							&& customerEntity.getItrValidity() != null) {
						if (StringUtils.equals(customerEntity.getItrValidity(), "valid")) {
							itr_valid++;
						} else {
							itr_invalid++;
						}
					}
					if (!request_status.equals("In draft")) {
						filteredEntitiesArray.put(entity);
					}
				}
			}
			total_request = filteredEntites.size();
			total_generated_request = valid + invalid + partial_valid;
			total_pending_request = pending;
			JSONObject summary_count_data = new JSONObject();
			summary_count_data.put("total_request", total_request);
			summary_count_data.put("generated", total_generated_request);
			summary_count_data.put("pending", total_pending_request);
			summary_count_data.put("draft", in_draft);
			summary_count_data.put("valid", valid);
			summary_count_data.put("invalid", invalid);
			summary_count_data.put("partial_valid", partial_valid);
			summary_count_data.put("itrValid", itr_valid);
			summary_count_data.put("itrInvalid", itr_invalid);
			JSONObject filteredData = new JSONObject();
			filteredData.put("data", filteredEntitiesArray);
			filteredData.put("summary_count_data", summary_count_data);
			return filteredData.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from getFilteredData " + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "commonFilter", method = RequestMethod.POST)
	public @ResponseBody String commonFilter(@RequestBody MisReportDTO misReportDTO, HttpServletRequest request,
			HttpSession httpSession) {
		try {
			List<BranchMaster> userBranches = getAllBranchesOfUser(request.getSession());

			misReportDTO.formateData();
			List<CustomerEntity> filteredEntites = new ArrayList<>();
			if (misReportDTO.getBranchId() == null) {
				for (int i = 0; i < userBranches.size(); i++) {
					misReportDTO.setBranchId(userBranches.get(i).getId());
					List<CustomerEntity> filteredEntitesByBranch = customerEntityService
							.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
					filteredEntites.addAll(filteredEntitesByBranch);
				}
			} else {
				List<CustomerEntity> filteredEntitesByBranch = customerEntityService
						.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
				filteredEntites.addAll(filteredEntitesByBranch);
			}

			JSONArray filteredEntitiesArray = new JSONArray();
			int generated = 0;
			int pending = 0;
			int valid = 0;
			int invalid = 0;
			for (int i = 0; i < filteredEntites.size(); i++) {
				JSONObject entity = new JSONObject();
				CustomerEntity customerEntity = filteredEntites.get(i);
				Request request1 = requestService.get(customerEntity.getRequest_id());
				String request_status = request1.getRequest_status();
				if (misReportDTO.getViewType() != null && !misReportDTO.getViewType().equalsIgnoreCase("all")) {
					if (misReportDTO.getViewType().equalsIgnoreCase("unread")) {
						if (!request1.getIsRead()) {
							if (request_status.equals("in_draft")) {
								request_status = "pending";
								pending++;
							} else {
								request_status = "Generated";
								generated++;
							}
							if (customerEntity.getEntity_type().equals("Individual")) {
								entity.put("entity_name", customerEntity.getFirst_name() + " "
										+ customerEntity.getMiddle_name() + " " + customerEntity.getLast_name());
							} else {
								entity.put("entity_name", customerEntity.getEntity_name());
							}
							entity.put("id", request1.getId());
							entity.put("request_id", request1.getId());
							entity.put("first_name", customerEntity.getFirst_name());
							entity.put("middle_name", customerEntity.getMiddle_name());
							entity.put("last_name", customerEntity.getLast_name());
							entity.put("entity_name", customerEntity.getEntity_name());
							entity.put("report_id", customerEntity.getRequest_id());
							entity.put("report_date", customerEntity.getCreated_date());
							entity.put("entity_type", customerEntity.getEntity_type());
							entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
							entity.put("no_of_parties", customerEntity.getNo_of_parties());
							entity.put("no_of_documents", customerEntity.getNo_of_documents());
							entity.put("report_status", request_status);
							entity.put("validity", customerEntity.getValidity());
							entity.put("created_date", customerEntity.getCreated_date());
							entity.put("isRead", request1.getIsRead());
							entity.put("isEdited", request1.getIsEdited());
							boolean validityValid = true;
							if (misReportDTO.getValidity() != null) {
								if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
									validityValid = false;
								}
							}
							if (validityValid) {
								if (customerEntity.getValidity() != null) {
									if (customerEntity.getValidity().equals("valid")) {
										valid++;
									} else {
										invalid++;
									}

								} else {
									invalid++;
								}
								filteredEntitiesArray.put(entity);
							}
						}
					} else if (misReportDTO.getViewType().equalsIgnoreCase("read")) {
						if (request1.getIsRead()) {
							if (request_status.equals("in_draft")) {
								request_status = "pending";
								pending++;
							} else {
								request_status = "Generated";
								generated++;
							}
							if (customerEntity.getEntity_type().equals("Individual")) {
								entity.put("entity_name", customerEntity.getFirst_name() + " "
										+ customerEntity.getMiddle_name() + " " + customerEntity.getLast_name());
							} else {
								entity.put("entity_name", customerEntity.getEntity_name());
							}
							entity.put("id", request1.getId());
							entity.put("request_id", request1.getId());
							entity.put("first_name", customerEntity.getFirst_name());
							entity.put("middle_name", customerEntity.getMiddle_name());
							entity.put("last_name", customerEntity.getLast_name());
							entity.put("entity_name", customerEntity.getEntity_name());
							entity.put("report_id", customerEntity.getRequest_id());
							entity.put("report_date", customerEntity.getCreated_date());
							entity.put("entity_type", customerEntity.getEntity_type());
							entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
							entity.put("no_of_parties", customerEntity.getNo_of_parties());
							entity.put("no_of_documents", customerEntity.getNo_of_documents());
							entity.put("report_status", request_status);
							entity.put("validity", customerEntity.getValidity());
							entity.put("created_date", customerEntity.getCreated_date());
							entity.put("isRead", request1.getIsRead());
							entity.put("isEdited", request1.getIsEdited());
							boolean validityValid = true;
							if (misReportDTO.getValidity() != null) {
								if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
									validityValid = false;
								}
							}
							if (validityValid) {
								if (customerEntity.getValidity() != null) {
									if (customerEntity.getValidity().equals("valid")) {
										valid++;
									} else {
										invalid++;
									}

								} else {
									invalid++;
								}
								filteredEntitiesArray.put(entity);
							}
						}
					}
				} else {
					if (request_status.equals("in_draft")) {
						request_status = "pending";
						pending++;
					} else {
						request_status = "Generated";
						generated++;
					}
					if (customerEntity.getEntity_type().equals("Individual")) {
						entity.put("entity_name", customerEntity.getFirst_name() + " " + customerEntity.getMiddle_name()
								+ " " + customerEntity.getLast_name());
					} else {
						entity.put("entity_name", customerEntity.getEntity_name());
					}
					entity.put("id", request1.getId());
					entity.put("request_id", request1.getId());
					entity.put("first_name", customerEntity.getFirst_name());
					entity.put("middle_name", customerEntity.getMiddle_name());
					entity.put("last_name", customerEntity.getLast_name());
					entity.put("entity_name", customerEntity.getEntity_name());
					entity.put("report_id", customerEntity.getRequest_id());
					entity.put("report_date", customerEntity.getCreated_date());
					entity.put("entity_type", customerEntity.getEntity_type());
					entity.put("branch", branchMasterService.get(customerEntity.getBranchId()).getBranchName());
					entity.put("no_of_parties", customerEntity.getNo_of_parties());
					entity.put("no_of_documents", customerEntity.getNo_of_documents());
					entity.put("report_status", request_status);
					entity.put("validity", customerEntity.getValidity());
					entity.put("created_date", customerEntity.getCreated_date());
					entity.put("isRead", request1.getIsRead());
					entity.put("isEdited", request1.getIsEdited());

					boolean validityValid = true;
					if (misReportDTO.getValidity() != null) {
						if (!misReportDTO.getValidity().equals(customerEntity.getValidity())) {
							validityValid = false;
						}
					}
					if (validityValid) {
						if (customerEntity.getValidity() != null) {
							if (customerEntity.getValidity().equals("valid")) {
								valid++;
							} else {
								invalid++;
							}

						} else {
							invalid++;
						}
						filteredEntitiesArray.put(entity);
					}
				}
			}

			JSONObject summary_count_data = new JSONObject();
			summary_count_data.put("total_request", filteredEntitiesArray.length());
			summary_count_data.put("generated", generated);
			summary_count_data.put("pending", pending);
			summary_count_data.put("valid", valid);
			summary_count_data.put("invalid", invalid);

			JSONObject filteredData = new JSONObject();
			filteredData.put("data", filteredEntitiesArray);
			filteredData.put("summary_count_data", summary_count_data);
			return filteredData.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from commonFilter" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "getKycReport", method = RequestMethod.POST)
	public @ResponseBody String getKycReport(@RequestBody String json_data, HttpServletRequest request) {
		try {
			JSONObject id = new JSONObject(json_data);
			String request_id = id.get("id").toString();
			JSONArray response = new JSONArray();
			Request request1 = null;
			request1 = requestService.get(Long.parseLong(request_id));
			request1.setIsRead(Boolean.TRUE);
			requestService.update(request1);
			if (hasAccessToRequest(request.getSession(), Long.parseLong(request_id))) {
				List<CustomerEntity> entities = customerEntityService.getByRequestId(Long.parseLong(request_id));
				UserControllerKYCReportUtil controllerKYCReportUtil = new UserControllerKYCReportUtil();
				response = controllerKYCReportUtil.displayKYCData(entities,request1,request,branchMasterService,
						userMasterService,voterIDService,panCardService,aadharCardService,itrService,serviceTaxService,
						esicService,esicContriService,userService,iecService,telephoneService,epfoReqService,lpgService,
						dlService,electricityService,llpinService,cinService,fcrnService,fllpinService,vatService,
						cstService,professionalTaxService,passportService,exciseService,epfoResService,drivingLicenceCOVService,
						vatFrequencyService,cstFrequencyService,professionalTaxFrequencyService);
				JSONObject data = new JSONObject();
				data.put("data", response);
				return data.toString();
			} else {
				return "no access";
			}
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from getKYCReport" + e.getMessage().toString());
		}
		return null;
	}

	/* Method written to get all the Branches details */
	@RequestMapping(value = "allBranches", method = RequestMethod.GET)
	public @ResponseBody List<BranchMaster> allBranches() throws JSONException {

		List<BranchMaster> branchMasters = branchMasterService.getAll();
		return branchMasters;

	}

	/* Method Written to fetch all the Electricity Board Names */
	@RequestMapping(value = "allElectricityBoard", method = RequestMethod.GET)
	public @ResponseBody List<ElectricityBoard> allElectricityBoard(HttpSession httpSession) throws JSONException {

		List<ElectricityBoard> electricityBoards = elecBoardService.findAll();
		return electricityBoards;
	}

	@RequestMapping(value = "branchesData", method = RequestMethod.GET)
	public @ResponseBody List<BranchMaster> branchesData(HttpSession httpSession) throws JSONException {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		List<BranchMaster> branchMasterList = branchMasterService.findByCustomerId(currentUser.getCustomerMasterId());
		return branchMasterList;
	}

	@RequestMapping(value = "branchesbyUser", method = RequestMethod.GET)
	public @ResponseBody List<BranchMaster> branchesbyUser(HttpSession httpSession) throws JSONException {
		List<BranchMaster> branchMasterList = getAllBranchesOfUser(httpSession);
		return branchMasterList;
	}

	public List<BranchMaster> getAllBranchesOfUser(HttpSession httpSession) {
		List<LocationMaster> locationMasters = getAllChildLocations(httpSession);
		List<BranchMaster> branchMasterList = new ArrayList<>();
		if (locationMasters != null) {
			branchMasterList = branchMasterService.findByUserLocation(locationMasters);
		}
		return branchMasterList;
	}

	@RequestMapping(value = "createBranch", method = RequestMethod.POST)
	public @ResponseBody String createBranch(@RequestBody BranchMasterDTO branchMasterDTO, HttpServletRequest request) {
		try {
			BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
			BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			LocationMaster locationMaster;
			if (branchMaster.getLocation() == null) {
				locationMaster = locationMasterService.getParentLocation(currentUser.getCustomerMasterId());
			} else {
				locationMaster = locationMasterService.get(branchMaster.getLocation());
			}
			LocationMaster branchLocation = new LocationMaster(branchMaster.getBranchName(),
					currentUser.getCustomerMasterId(), locationMaster.getId());
			branchLocation = locationMasterService.saveAndGet(branchLocation);
			branchMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
			branchMaster.setLocation(branchLocation.getId());
			branchMasterService.save(branchMaster);
			String data = "Branch Saved";
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", data);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "mergeBranch", method = RequestMethod.POST)
	public @ResponseBody String mergeBranch(@RequestBody String request_data, HttpServletRequest request) {
		try {
			JSONObject jObject = new JSONObject(request_data);
			JSONArray jsonArray = jObject.getJSONArray("branchName");
			String newBranchId = jObject.getString("branch");
			JSONObject responseJson = new JSONObject();
			// UserMaster currentUser = (UserMaster)
			// request.getSession().getAttribute("currentUser");
			BranchMaster newBanchMaster = branchMasterService.get(Long.valueOf(newBranchId));
			Long newLocation = newBanchMaster.getLocation();

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String oldBranchId = jsonObject.getString("id");
				BranchMaster oldBranch = branchMasterService.get(Long.valueOf(oldBranchId));
				Integer updatedEntitiesCount = customerEntityService.replaceBranchId(Long.valueOf(oldBranchId),
						Long.valueOf(newBranchId)); 
				// replace old branchId to New BranchId in Entities table
				Integer updatedUsers = userMasterService.replaceLocation(newLocation, oldBranch.getLocation());
			}
			String data = "Branch Saved";
			responseJson.put("result", data);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "updateBranch", method = RequestMethod.POST)
	public @ResponseBody String updateBranch(@RequestBody BranchMasterDTO branchMasterDTO, HttpServletRequest request) {
		try {
			BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
			BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			if (branchMaster.getLocation() == null) {
				LocationMaster locationMaster = locationMasterService
						.getParentLocation(currentUser.getCustomerMasterId());
				branchMaster.setLocation(locationMaster.getId());
			}
			branchMasterService.update(branchMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "Branch Updated");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Update Branch" + e.getMessage().toString());
		}
		return null;
	}

	public List<LocationMaster> getAllChildLocations(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		if (currentUser.getLocation() != null) {
			Long locationId = currentUser.getLocation();
			List<LocationMaster> locationMasters = locationMasterService.getChildLocations(locationId);
			return locationMasters;
		} else {

		}
		return new ArrayList<>();
	}

	@RequestMapping(value = "locationsData", method = RequestMethod.GET)
	public @ResponseBody List<LocationMaster> locationsData(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		List<LocationMaster> locationMasterList = locationMasterService
				.findByCustomerId(currentUser.getCustomerMasterId());
		return locationMasterList;
	}

	@RequestMapping(value = "createLocation", method = RequestMethod.POST)
	public @ResponseBody String createLocation(@RequestBody LocationMasterDTO locationMasterDTO,
			HttpServletRequest request) {
		try {
			LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
			LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			locationMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
			if (locationMaster.getParentLocation() == null) {
				LocationMaster locationMaster1 = locationMasterService
						.getParentLocation(currentUser.getCustomerMasterId());
				locationMaster.setParentLocation(locationMaster1.getId());
			}
			locationMasterService.save(locationMaster);
			String data = "Location Saved";
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", data);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Create Location" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "updateLocation", method = RequestMethod.POST)
	public @ResponseBody String updateLocation(@RequestBody LocationMasterDTO locationMasterDTO,
			HttpServletRequest request) {
		try {
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
			LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
			if (locationMaster.getParentLocation() == null) {
				LocationMaster locationMaster1 = locationMasterService
						.getParentLocation(currentUser.getCustomerMasterId());
				locationMaster.setParentLocation(locationMaster1.getId());
			}
			locationMasterService.update(locationMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "Location Updated");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from UpdateLocation" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "usersData", method = RequestMethod.GET)
	public @ResponseBody List<UserMaster> usersData(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		List<UserMaster> userMasterList = userMasterService.findByCustomerIdAndRole(currentUser.getCustomerMasterId(),
				"User");
		return userMasterList;
	}

	@RequestMapping(value = "usersDataWithAdmin", method = RequestMethod.GET)
	public @ResponseBody List<UserMaster> usersDataWithAdmin(HttpSession httpSession) {
		UserMaster currentUser = (UserMaster) httpSession.getAttribute("currentUser");
		List<UserMaster> userMasterList = userMasterService.findByCustomerId(currentUser.getCustomerMasterId());
		return userMasterList;
	}

	@RequestMapping(value = "createUser", method = RequestMethod.POST)
	public @ResponseBody String createUser(@RequestBody UserMasterDTO userMasterDTO, HttpServletRequest request) {
		try {
			JSONObject responseJson = new JSONObject();
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			userMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
			int noOfCAPSAlpha = 1;
			int noOfDigits = 1;
			int noOfSplChars = 1;
			int minLen = 8;
			int maxLen = 12;
			char[] pswd = RandomPasswordGenerator.generatePswd(minLen, maxLen, noOfCAPSAlpha, noOfDigits, noOfSplChars);
			String password = String.valueOf(pswd);
			String tempPassword = String.valueOf(pswd);
			password = PasswordEncryptDecrypt.md5Hash(password);
		//	userMaster.setPassword(String.valueOf(pswd));
			userMaster.setPassword(password);
			Calendar cal = Calendar.getInstance();
			String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
			userMaster.setCurrentMonth(month);
			EmailHelper emailHelper = new EmailHelper();
			String isExistUser = userMasterService.IsExistUser(userMaster.getEmail(), userMaster.getContactNumber());
			if (isExistUser != null && isExistUser.isEmpty()) {
				Integer uniqueId = userMasterService.getMaxUserAccountNumber(currentUser.getCustomerMasterId());
				userMaster.setUniqueId(uniqueId + 1);
				userMaster.setActive(Boolean.TRUE);
				userMaster.setAutoGeneratedPasswordExpired(Boolean.FALSE);
				userMaster.setAutoGeneratedPasswordFlag(Boolean.FALSE);
				UserMaster userMaster1 = userMasterService.save(userMaster);
				String data = "User Not Saved";
				if (userMaster1 != null) {
					data = "User Saved";
					String emailStatus = emailHelper.sendEmailToUser(userMaster.getEmail(), tempPassword,
							userMaster, "createUser");
				}
				responseJson.put("result", data);
			} else if (isExistUser != null && !isExistUser.isEmpty()) {
				responseJson.put("result", isExistUser);
			}

			return responseJson.toString();
		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("Exception from create User" + e.getMessage().toString());
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Exception from create User" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "updateUser", method = RequestMethod.POST)
	public @ResponseBody String updateUser(@RequestBody UserMasterDTO userMasterDTO, HttpServletRequest request) {
		try {
			JSONObject responseJson = new JSONObject();
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			userMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
			UserMaster userMaster1 = userMasterService.get(userMaster.getId());
			Boolean emailAlreadyExist = false;
			Boolean phoneAlreadyExist = false;
			String data = "";
			if (!userMaster.getEmail().equals(userMaster1.getEmail())) {
				String isExistUser = userMasterService.IsEmailExist(userMaster.getEmail());
				if (isExistUser != null && !isExistUser.isEmpty()) {
					emailAlreadyExist = true;
				}
			}
			if (!userMaster.getContactNumber().equals(userMaster1.getContactNumber())) {
				String isExistUser = userMasterService.IsContactExist(userMaster.getContactNumber());
				if (isExistUser != null && !isExistUser.isEmpty()) {
					phoneAlreadyExist = true;
				}
			}
			if (phoneAlreadyExist && emailAlreadyExist) {
				data = "Email and phone are already exist";
			}
			if (phoneAlreadyExist) {
				data = "User Contact Number already exist";
			}
			if (emailAlreadyExist) {
				data = "User email already exist";
			}
			if (!phoneAlreadyExist && !emailAlreadyExist) {
				Calendar cal = Calendar.getInstance();
				String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
				userMaster.setCurrentMonth(month);
				userMasterService.update(userMaster);
				responseJson.put("result", "User Updated");
				responseJson.put("activeUser", userMaster.getActive());
			} else {
				responseJson.put("result", data);
			}
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from update User" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "toggleUser", method = RequestMethod.POST)
	public @ResponseBody String toggleUser(@RequestBody UserMasterDTO userMasterDTO, HttpServletRequest request) {
		try {
			JSONObject responseJson = new JSONObject();
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			Calendar cal = Calendar.getInstance();
			String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
			userMaster.setCurrentMonth(month);
			if (userMaster.getActive()) {
				userMaster.setAutoGeneratedPasswordFlag(Boolean.FALSE);
				userMaster.setAutoGeneratedPasswordExpired(Boolean.FALSE);
				int noOfCAPSAlpha = 1;
				int noOfDigits = 1;
				int noOfSplChars = 1;
				int minLen = 8;
				int maxLen = 12;
				char[] pswd = RandomPasswordGenerator.generatePswd(minLen, maxLen, noOfCAPSAlpha, noOfDigits,
						noOfSplChars);
				/*Setting the Passoword in Encoded Manner to the DB*/
				String password = String.valueOf(pswd);
				password = PasswordEncryptDecrypt.md5Hash(password);
				//userMaster.setPassword(String.valueOf(pswd));
				userMaster.setPassword(password);
				userMasterService.update(userMaster);
				/*Commenting the Below code line as of now, Forwarding the value */
				/*String emailStatus = new EmailHelper().sendEmailToUser(userMaster.getEmail(), userMaster.getPassword(),
						userMaster, "activateUser");*/
				String tempPassword = String.valueOf(pswd);
				String emailStatus = new EmailHelper().sendEmailToUser(userMaster.getEmail(), tempPassword,
						userMaster, "activateUser");
			} else {
				userMasterService.update(userMaster);
				String emailStatus = new EmailHelper().sendEmailToUser(userMaster.getEmail(), userMaster.getPassword(),
						userMaster, "deactivateUser");
			}
			responseJson.put("result", "User Updated");
			responseJson.put("activeUser", userMaster.getActive());
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Toggle User" + e.getMessage().toString());
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Exception from Toggle User" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("login");
		HttpSession httpSession = request.getSession();
		UserSessionDetail userSessionDetail = userSessionDetailService.getBySessionId(httpSession.getId());
		userSessionDetail.setUpdatedAt(new Date());
		userSessionDetail.setUserStatus("LogOut");
		userSessionDetailService.update(userSessionDetail);
		if (httpSession != null) {
			httpSession.invalidate();
		}
		return modelAndView;
	}

	public boolean hasAccessToRequest(HttpSession httpSession, Long requestId) {
		List<BranchMaster> branchMasterList = getAllBranchesOfUser(httpSession);
		List<CustomerEntity> entities = customerEntityService.getByRequestId(requestId);
		CustomerEntity firstEntity;
		if (entities != null) {
			firstEntity = entities.get(0);
			BranchMaster entityBranch = branchMasterService.get(firstEntity.getBranchId());
			for (int i = 0; i < branchMasterList.size(); i++) {
				if (entityBranch.getId() == branchMasterList.get(i).getId()
						&& entityBranch.getBranchName().equals(branchMasterList.get(i).getBranchName())) {
					return true;
				}
			}
		}
		return false;
	}

	@RequestMapping(value = "getAssessmentYears", method = RequestMethod.GET)
	public @ResponseBody String getAssessmentYears() {
		JSONArray assessmentYears = new JSONArray();
		assessmentYears = new ApiCallHelper().getAssessmentYears();
		return assessmentYears.toString();
	}

	@RequestMapping(value = "validateEmail", method = RequestMethod.POST)
	public @ResponseBody String validateEmail(@RequestBody String email_data, HttpServletRequest request) {
		try {
			String emailVerification = null;
			JSONObject emailData = new JSONObject(email_data);
			JSONObject response = new JSONObject();
			try {
				emailVerification = ApiCallHelper.getEmail(emailData.get("email").toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.put("result", emailVerification);
			return response.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("" + e.getMessage().toString());
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception from Validate Email" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "changeEmailDomain", method = RequestMethod.POST)
	public @ResponseBody String changeEmailDomain(@RequestBody String email_data, HttpServletRequest request) {
		try {
			JSONObject email_json = new JSONObject(email_data);
			String currentDomain = email_json.getString("currentDomain");
			String newDomain = email_json.getString("newDomain");
			Long id = email_json.getLong("id");
			String status = customerMasterService.changeEmailDomain(id, currentDomain, newDomain);
			JSONObject response = new JSONObject();
			response.put("data", status);
			return response.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Change Email Domain" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "createSubscriptionPlan", method = RequestMethod.POST)
	public @ResponseBody String createSubscriptionPlan(@RequestBody String request_data, HttpServletRequest request) {
		try {
			JSONObject jsonObject = new JSONObject(request_data);
			SubscriptionMaster subscriptionMaster = new SubscriptionMaster(jsonObject);
			JSONArray jsonArray = jsonObject.getJSONArray("application");
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			subscriptionMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
			String application;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = (JSONObject) jsonArray.get(i);
				application = object.getString("id");
				subscriptionMaster.setApplication(application);
				subscriptionMasterService.save(subscriptionMaster);
			}
			String data = "subscription plan Saved";
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", data);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Create Subscription Plan" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "showSubscriptionPlan", method = RequestMethod.POST)
	public @ResponseBody String showSubscriptionPlan(@RequestBody HttpServletRequest request) throws Exception {

		UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
		List<SubscriptionMaster> subscriptionMasterList = subscriptionMasterService
				.getSubscriptionsByCustomerMasterId(currentUser.getCustomerMasterId());
		String data = "subscription list";
		JSONObject responseJson = new JSONObject();
		responseJson.put("result", data);
		responseJson.put("list", subscriptionMasterList);
		return responseJson.toString();
	}

	@RequestMapping(value = "updateSubscriptionPlan", method = RequestMethod.POST)
	public @ResponseBody String updateSubscriptionPlan(@RequestBody String request_data, HttpServletRequest request) {
		try {
			JSONObject jsonObject = new JSONObject(request_data);
			SubscriptionMaster subscriptionMaster = new SubscriptionMaster(jsonObject);
			JSONArray jsonArray = jsonObject.getJSONArray("application");
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			subscriptionMaster.setCustomerMasterId(currentUser.getCustomerMasterId());
			String application;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = (JSONObject) jsonArray.get(i);
				application = object.getString("id");
				subscriptionMaster.setApplication(application);
				subscriptionMasterService.update(subscriptionMaster);
			}
			String data = "subscription plan updated";
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", data);
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception From updateSubscription Plan" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "deleteUser", method = RequestMethod.POST)
	public @ResponseBody String deleteUser(@RequestBody UserMasterDTO userMasterDTO, HttpServletRequest request) {
		try {
			UserMasterMapper userMasterMapper = new UserMasterMapper();
			UserMaster userMaster = userMasterMapper.createUserMasterForDTO(userMasterDTO);
			userMasterService.delete(userMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "User Deleted");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception From Delete User" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "deleteBranch", method = RequestMethod.POST)
	public @ResponseBody String deleteBranch(@RequestBody BranchMasterDTO branchMasterDTO, HttpServletRequest request) {
		try {
			BranchMasterMapper branchMasterMapper = new BranchMasterMapper();
			BranchMaster branchMaster = branchMasterMapper.createBranchMasterForDTO(branchMasterDTO);
			branchMasterService.delete(branchMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "branch Deleted");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception From Delete Branch" + e.getMessage().toString());
		}
		return null;
	}

	@RequestMapping(value = "deleteLocation", method = RequestMethod.POST)
	public @ResponseBody String deleteLocation(@RequestBody LocationMasterDTO locationMasterDTO,
			HttpServletRequest request) {
		try {
			LocationMasterMapper locationMasterMapper = new LocationMasterMapper();
			LocationMaster locationMaster = locationMasterMapper.createLocationMasterForDTO(locationMasterDTO);
			locationMasterService.delete(locationMaster);
			JSONObject responseJson = new JSONObject();
			responseJson.put("result", "location Deleted");
			return responseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception From Delete Location" + e.getMessage().toString());
		}
		return null;
	}

	/* function written to get all the customers data */
	@RequestMapping(value = "showCustomers", method = RequestMethod.GET)
	public @ResponseBody List<CustomerMaster> showCustomers() {
		List<CustomerMaster> customerData = customerMasterService.getAll();
		return customerData;
	}

	/* Function written to get the all the details of the user */
	@RequestMapping(value = "showUsers", method = RequestMethod.GET)
	public @ResponseBody List<UserMaster> showUsers() {
		List<UserMaster> userData = userMasterService.getAll();
		return userData;
	}

	/* Method to get all the Branches that are registered */
	public List<BranchMaster> getAllBranches() {
		List<BranchMaster> branchMasterList_ = new ArrayList<>();
		branchMasterList_ = branchMasterService.getAll();
		return branchMasterList_;
	}

	/* Method to get all the Customers */
	public List<CustomerMaster> getAllCustomers() {
		List<CustomerMaster> customerMasterList_ = new ArrayList<>();
		customerMasterList_ = customerMasterService.getAll();
		return customerMasterList_;
	}

	/* Mehtod to get all the Users */
	public List<UserMaster> getAllUsers() {
		List<UserMaster> userMasterList_ = new ArrayList<>();
		userMasterList_ = userMasterService.getAll();
		return userMasterList_;
	}
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file) {
		

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				//String rootPath = System.getProperty("user.home");
				String rootPath = "F:\\Upload File";
				File dir = new File(rootPath + File.separator);
				if (!dir.exists())
					dir.mkdirs();
				System.out.println("Directory Name  "+dir);
				// Create the file on server
				String filePath=dir.getAbsolutePath()
						+ File.separator + name;
				File serverFile = new File(filePath);
				System.out.println("Name -<<<<< "+name);
				System.out.println("File Uploaded at Server --> "+serverFile);
				String filena = serverFile.toString();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				Test.readCSVFile(filena);
				//System.out.print(Test.readCSVFile(filena));
				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());
			
				String storageConnectionString = ApiCallHelperUtil.getAzureCloudKey();
			    // Retrieve storage account from connection-string.
			    CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

			    // Create the blob client.
			    CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

			    // Retrieve reference to a previously created container.
			    CloudBlobContainer container = blobClient.getContainerReference("totalkycwebappstoragecontainer");

			    // Define the path to a local file.

			    // Create or overwrite the "myimage.jpg" blob with contents from a local file.
			    CloudBlockBlob blob = container.getBlockBlobReference(filePath);
			    StorageUri storageURI= blob.getQualifiedStorageUri();
			    System.out.println(storageURI.getPrimaryUri().toString());
			    //Storage blob.getQualifiedStorageUri()
			    File source = new File(filePath);
			    blob.upload(new FileInputStream(source), source.length());
			}
			catch (Exception e)
			{
			    // Output the stack trace.
			    e.printStackTrace();
			}
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}
		return "You successfully uploaded file=" + name ;
	}

	/**
	 * Upload multiple file using Spring Controller
	 */
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadMultipleFileHandler(@RequestParam("name") String[] names,
			@RequestParam("file") MultipartFile[] files) {

		if (files.length != names.length)
			return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("user.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

				message = message + "You successfully uploaded file=" + name
						+ "";
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		return message;
	}
	

}
