package com.karza.kyc.web.util;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.service.ElectricityService;

public class ElectricityWebUtil {
	public static boolean saveElectrictyData(JSONObject request_form_data,
			ElectricityService electricityService, CustomerEntity customerEntity, Logger logger) {
		try {
			JSONObject electricityJSONObj=((JSONObject) request_form_data.get("electricity"));
			String electricity_document = electricityJSONObj.get("unique_no").toString();
			String elecServiceProvider= electricityJSONObj.getString("board").toString();
			Electricity clientID = electricityService.getByCustomerId(customerEntity.getId());
			Electricity electricityObj = new Electricity(electricity_document,elecServiceProvider, customerEntity.getId());
			electricityObj.setAPICount(0);
			if (clientID == null) {
				electricityService.save(electricityObj);
			} else {
				//electricityObj.setId(electricityObj.getId());
				electricityObj.setId(clientID.getId());
				electricityService.update(electricityObj);
			}
		} catch (JSONException e) {
			//logger.debug(e.getMessage());// Put proper Level of Logging
			System.out.print("Exception from Electricity  From ElectricityWebUtil Class"+e.getMessage());
			return false;
		}
		return true;
	}

}
