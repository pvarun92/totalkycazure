package com.karza.kyc.web.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CSTFrequency;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.DrivingLicenceCOV;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.EPFOResponse;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.ESICContriDetails;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.ProfessionalTaxFrequency;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VATFrequency;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.BranchMasterService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTFrequencyService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.CustomerEntityService;
import com.karza.kyc.service.CustomerMasterService;
import com.karza.kyc.service.DrivingLicenceCOVService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.EPFOResponseService;
import com.karza.kyc.service.ESICContriService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityBoardService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.LocationMasterService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxFrequencyService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.RequestService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.SubscriptionMasterService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.UserMasterService;
import com.karza.kyc.service.UserService;
import com.karza.kyc.service.UserSessionDetailService;
import com.karza.kyc.service.VATFrequencyService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;
import com.karza.kyc.util.ApiCallHelper;
import com.karza.kyc.util.CustomerEntityHelper;

public class UserControllerKYCReportUtil {
		public JSONArray displayKYCData(List<CustomerEntity> entities,Request request1,HttpServletRequest request,
				BranchMasterService branchMasterService,UserMasterService userMasterService,VoterIDService voterIDService,
				PanCardService panCardService,AadharCardService aadharCardService,ItrService itrService,ServiceTaxService serviceTaxService,
				ESICService esicService,ESICContriService esicContriService,UserService userService,IECService iecService,TelephoneService
				telephoneService,EPFORequestService epfoReqService,LPGService lpgService,
				DrivingLicenceMasterService dlService,ElectricityService electricityService,LLPINService llpinService,
				CINService cinService,FCRNService fcrnService,FLLPINService fllpinService,VATService vatService,CSTService cstService,
				ProfessionalTaxService professionalTaxService,PassportService passportService,ExciseService exciseService,
				EPFOResponseService epfoResService,DrivingLicenceCOVService drivingLicenceCOVService,
				VATFrequencyService vatFrequencyService,CSTFrequencyService cstFrequencyService,
				ProfessionalTaxFrequencyService professionalTaxFrequencyService
				) throws JSONException{
		
		JSONArray response = new JSONArray();
		for (int i = 0; i < entities.size(); i++) {
			CustomerEntity entity = entities.get(i);
			long id = entity.getBranchId();
			BranchMaster entityBranch = branchMasterService.get(id);
			entity.setBranch(entityBranch.getBranchName());
			JSONObject entity_data = new JSONObject(entity);
			ArrayList detailed_document_report_data = new ArrayList();
			JSONObject customer_document_information = new JSONObject();
			UserMaster currentUser = (UserMaster) request.getSession().getAttribute("currentUser");
			Long userMasterId = request1.getUserMasterId();
			UserMaster userMaster = userMasterService.get(userMasterId);
			entity_data.put("user_id", currentUser.getId());
			entity_data.put("unique_id", userMaster.getUniqueId());
			PanCard panCard = panCardService.getByCustomerId(entity.getId());
			if (panCard != null) {
				JSONObject document_pan_data = new JSONObject();
				document_pan_data.put("document", "PAN");
				document_pan_data.put("number", (panCard).getDocument());
				document_pan_data.put("status", (panCard).getApiValidation());
				document_pan_data.put("status_as_per_source", (panCard).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((panCard).getStatusAsPerSource())) {
					if (panCard.getFirstName() == null && panCard.getMiddleName() == null) {
						document_information.put("Full Name", panCard.getLastName());
					} else if (panCard.getFirstName() == null) {
						document_information.put("Full Name",
								panCard.getMiddleName() + " " + panCard.getLastName());
					} else if (panCard.getMiddleName() == null) {
						document_information.put("Full Name",
								panCard.getFirstName() + " " + panCard.getLastName());
					} else
						document_information.put("Full Name", panCard.getFirstName() + " "
								+ panCard.getMiddleName() + " " + panCard.getLastName());
					document_pan_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_pan_data);
				customer_document_information.put("pan", document_pan_data);
			}

			VoterID voterID = voterIDService.getByCustomerId(entity.getId());
			if (voterID != null) {
				JSONObject document_voter_data = new JSONObject();
				document_voter_data.put("document", "Voter ID");
				document_voter_data.put("number", (voterID).getDocument());
				document_voter_data.put("status", (voterID).getApiValidation());
				document_voter_data.put("status_as_per_source", (voterID).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((voterID).getStatusAsPerSource())) {
					document_information.put("Name", voterID.getVoterName());
					document_information.put("Father’s / Husband’s / Mother’s Name",
							voterID.getFatherOrHusbandOrMotherOrName());
					document_information.put("Age", voterID.getAge());
					document_information.put("Gender", voterID.getGender());
					document_information.put("Assembly Constitution", voterID.getAssemblyConstituency());
					document_information.put("District ", voterID.getDistrict());
					document_information.put("State ", voterID.getState());
					document_voter_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_voter_data);
				customer_document_information.put("voter", document_voter_data);
			}

			List<Itr> itrs = itrService.getByCustomerId(entity.getId());
			if (itrs != null) {
				if (itrs.size() > 0) {
					JSONArray itrDataList = new JSONArray();
					for (int j = 0; j < itrs.size(); j++) {
						Itr itr = itrs.get(j);
						if (itr != null) {
							JSONObject document_itr_data = new JSONObject();
							document_itr_data.put("document", "ITR");
							document_itr_data.put("number", (itrs.get(j)).getDocument());
							document_itr_data.put("status", (itrs.get(j)).getApiValidation());
							document_itr_data.put("status_as_per_source_ack",
									(itrs.get(j)).getStatusAsPerSourceAck());
							document_itr_data.put("status_as_per_source_pan",
									(itrs.get(j)).getStatusAsPerSourcePan());
							document_itr_data.put("assessment_year", (itrs.get(j)).getAssessmentYear());
							itrDataList.put(document_itr_data);
						}
					}
					customer_document_information.put("itr", itrDataList);
				}
			}

			AadharCard aadharCard = aadharCardService.getByCustomerId(entity.getId());
			if (aadharCard != null) {
				JSONObject document_aadhar_data = new JSONObject();
				document_aadhar_data.put("document", "AADHAR");
				document_aadhar_data.put("number", (aadharCard).getDocument());
				if((aadharCard).getStatusAsPerSource()=="valid"){
					document_aadhar_data.put("status","valid");
				}
				else{
					document_aadhar_data.put("status","invalid");
				}
				document_aadhar_data.put("status_as_per_source", (aadharCard).getStatusAsPerSource());
				detailed_document_report_data.add(document_aadhar_data);
				customer_document_information.put("aadhar", document_aadhar_data);
			}

			ServiceTax serviceTax = serviceTaxService.getByCustomerId(entity.getId());
			if (serviceTax != null) {
				JSONObject document_serviceTax_data = new JSONObject();
				document_serviceTax_data.put("document", "Service Tax");
				document_serviceTax_data.put("number", (serviceTax).getDocument());
				document_serviceTax_data.put("status", (serviceTax).getApiValidation());
				document_serviceTax_data.put("status_as_per_source", (serviceTax).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((serviceTax).getStatusAsPerSource())) {
					document_information.put("Assessee Belongs To", serviceTax.getAssesseeBelongsTo());
					document_information.put("Name of Assessee", serviceTax.getNameOfAssessee());
					document_information.put("Address of Assessee", serviceTax.getAddressOfAssessee());
					document_information.put("Location Code", serviceTax.getLocationCode());
					document_information.put("Commissionerate Code", serviceTax.getCommissionerateCode());
					document_information.put("Division Code", serviceTax.getDivisionCode());
					document_information.put("Range Code", serviceTax.getRangeCode());
					document_information.put("Status", serviceTax.getStatus());
					document_information.put("Status As On", serviceTax.getStatusAsOn());

					document_serviceTax_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_serviceTax_data);
				customer_document_information.put("service_tax", document_serviceTax_data);
			}

			Excise excise = exciseService.getByCustomerId(entity.getId());
			if (excise != null) {
				JSONObject document_excise_data = new JSONObject();
				document_excise_data.put("document", "Excise");
				document_excise_data.put("number", (excise).getDocument());
				document_excise_data.put("status", (excise).getApiValidation());
				document_excise_data.put("status_as_per_source", (excise).getStatus());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((excise).getStatusAsPerSource())) {
					document_information.put("Assessee Belongs To", excise.getAssesseeBelongsTo());
					document_information.put("Name of Assessee", excise.getNameOfAssessee());
					document_information.put("Address of Assessee", excise.getAddressOfAssessee());
					document_information.put("Location Code", excise.getLocationCode());
					document_information.put("Commissionerate Code", excise.getCommissionerateCode());
					document_information.put("Division Code", excise.getDivisionCode());
					document_information.put("Range Code", excise.getRangeCode());
					document_information.put("Status", excise.getStatus());
					document_information.put("Status As On", excise.getStatusAsOn());

					document_excise_data.put("document_information", document_information);
				} else {
					if (excise.getStatusAsOn() != null && (!excise.getStatusAsOn().isEmpty()
							|| !excise.getStatusAsOn().trim().isEmpty())) {
						document_information.put("Status As On", excise.getStatusAsOn());
						document_excise_data.put("document_information", document_information);
					}
				}
				detailed_document_report_data.add(document_excise_data);
				customer_document_information.put("excise", document_excise_data);
			}

			IEC iec = iecService.getByCustomerId(entity.getId());
			if (iec != null) {
				JSONObject document_iec_data = new JSONObject();
				document_iec_data.put("document", "IEC");
				document_iec_data.put("number", (iec).getDocument());
				document_iec_data.put("status", (iec).getApiValidation());
				document_iec_data.put("status_as_per_source", (iec).getStatusAsPerSource());

				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((iec).getStatusAsPerSource())) {
					document_information.put("IEC Code", iec.getIecCode());
					document_information.put("Name", iec.getName());
					document_information.put("Address", iec.getAddress());
					document_information.put("PAN", iec.getPanNumber());
					document_information.put("Total number of branches", iec.getTotalNoOfBranches());
					document_iec_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_iec_data);
				customer_document_information.put("iec", document_iec_data);
			}

			Telephone telephone = telephoneService.getByCustomerId(entity.getId());
			if (telephone != null) {
				JSONObject document_telephone_data = new JSONObject();
				document_telephone_data.put("document", "Telephone Bill Details");
				document_telephone_data.put("number", (telephone).getDocument());
				document_telephone_data.put("status", (telephone).getApiValidation());
				document_telephone_data.put("status_as_per_source", (telephone).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				// cross check with the Response received from the API
				// against the Code and set it's status
				if (CustomerEntityHelper.showDetailedDocumentData((telephone).getStatusAsPerSource())) {
					document_information.put("Name", telephone.getName());
					document_information.put("Telephone Number", telephone.getTelephoneNumber());
					document_information.put("Installation Type", telephone.getInstallationType());
					document_information.put("Category", telephone.getCategory());
					document_information.put("Address", telephone.getAddress());
					document_telephone_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_telephone_data);
				customer_document_information.put("telephone", document_telephone_data);
			}
			
			EPFORequest epfoReq = epfoReqService.getByCustomerID(entity.getId());
				if(epfoReq != null){
					JSONObject document_uan_data = new JSONObject();
					 document_uan_data.put("document", "EPFO UAN Details");
					 document_uan_data.put("number", (epfoReq).getDocument());
					 document_uan_data.put("status", (epfoReq).getApiValidation());
					 document_uan_data.put("status_as_per_source", (epfoReq).getStatusAsPerSource());
					 Map document_information = new LinkedHashMap<>();
					 if(CustomerEntityHelper.showDetailedDocumentData((epfoReq).getStatusAsPerSource())){
						 document_uan_data.put("document_information", document_information);
						 List<EPFOResponse> epfoResFrequency = epfoResService.getByEPFOReqId(epfoReq.getId());
						 JSONArray epfo_details = new JSONArray();
						 if(epfoResFrequency != null){
							 for(int iEpfoResponse=0;iEpfoResponse <epfoResFrequency.size();iEpfoResponse++){
								 JSONObject frequency = new JSONObject(epfoResFrequency.get(iEpfoResponse));
									 frequency.put("name", epfoResFrequency.get(iEpfoResponse).getName());
									 frequency.put("employer_id",epfoResFrequency.get(iEpfoResponse).getMid());
									 frequency.put("balance", epfoResFrequency.get(iEpfoResponse).getBalance());
									 frequency.put("date_of_birth", epfoResFrequency.get(iEpfoResponse).getDob());
									 frequency.put("employee_share", epfoResFrequency.get(iEpfoResponse).getEmployee_share());
									 frequency.put("employer_share", epfoResFrequency.get(iEpfoResponse).getEmployer_share());
									 frequency.put("last_contributed_amount", epfoResFrequency.get(iEpfoResponse).getLast_contri_amt());
									 frequency.put("last_transaction_date", epfoResFrequency.get(iEpfoResponse).getLast_tran_date());
									 frequency.put("settlement_status", epfoResFrequency.get(iEpfoResponse).getStatusOfSettlement());
									 epfo_details.put(frequency);
							 }
						 }
						 document_information.put("epfo_details", epfo_details);
						 document_uan_data.put("document_information", document_information);
					 }
					 else{
							document_information.put("Status as per Source", epfoReq.getStatusAsPerSource());
							document_uan_data.put("document_information", document_information);
					 }
					 detailed_document_report_data.add(document_uan_data);
					 customer_document_information.put("uan", document_uan_data);
			}
				LPG lpg = lpgService.getByCustomerId(entity.getId());
				if(lpg != null){
					JSONObject document_lpg_data = new JSONObject();
					document_lpg_data.put("document","LPG Detail");
					document_lpg_data.put("number", (lpg).getDocument());
					document_lpg_data.put("status",(lpg).getApiValidation());
					document_lpg_data.put("status_as_per_source", (lpg).getStatusAsPerSource());
					Map document_information = new LinkedHashMap();
					if(CustomerEntityHelper.showDetailedDocumentData((lpg).getStatusAsPerSource())){
						document_information.put("Consumer Name ", lpg.getConsumerName());
						document_information.put("Consumer Number ", lpg.getConsumerNumber());
						document_information.put("Consumer Address ", lpg.getConsumerAddress());
						document_information.put("Consumer Email ", lpg.getConsumerEmail());
						document_information.put("Consumer Contact Number ", lpg.getConsumerContact());
						document_information.put("Consumer LPG Status ", lpg.getStatusOfLpg());
						document_information.put("Consumer Bank Account No. ", lpg.getBankAccountNumber());
						document_information.put("Subsidized Refill Consumed ", lpg.getSubsRefilledConsumed());
						document_information.put("Total Refill Consumed ", lpg.getTotalRefillConsumed());
						document_information.put("PIN  ", lpg.getPin());
						document_information.put("Bank Name ", lpg.getBankName());
						document_information.put("IFSC Code ", lpg.getIfscCode());
						document_information.put("City/Town ", lpg.getCity());
						document_information.put("Aadhaar Number ", lpg.getAadhaarNo());
						document_information.put("Approximate Subsidy Availed ", lpg.getApproximateSubsAvailed());
						document_information.put("Given Up Subsidy ", lpg.getGivenUpSubsidy());
						document_information.put("Last Booking Date ", lpg.getLastBookingDate());
						document_information.put("Distributor Name ", lpg.getDistributorName());
						document_information.put("Distributor Address ", lpg.getDistributorAdd());
						document_information.put("Distributor Code ", lpg.getDistributorCode());
						document_lpg_data.put("document_information", document_information);
					}
					detailed_document_report_data.add(document_lpg_data);
					customer_document_information.put("lpg", document_lpg_data);
				}
				
				ESIC esic = esicService.getByCustomerId(entity.getId());
				if(esic != null){
					JSONObject document_esic_data = new JSONObject();
					document_esic_data.put("document", "ESIC Details");
					document_esic_data.put("number", (esic).getDocument());
					document_esic_data.put("status",(esic).getApiValidation());
					document_esic_data.put("status_as_per_source", (esic).getStatusAsPerSource());
					Map document_information = new LinkedHashMap<>();
					if(CustomerEntityHelper.showDetailedDocumentData((esic).getStatusAsPerSource())){
						document_esic_data.put("document_information",document_information);
						document_information.put("Name ", esic.getName());
						document_information.put("Aadhaar Number ", esic.getAadharNumber());
						document_information.put("Nominee Name ", esic.getNomineeName());
						document_information.put("Date Of Birth ", esic.getDob());
						document_information.put("Aadhaar Status ", esic.getAadharStatus());
						document_information.put("Current Date Of Appointment ", esic.getCurrentDateOfAppointment());
						document_information.put("Disability Type ", esic.getDisabilityType());
						document_information.put("Dispensary Name ", esic.getDisPensaryName());
						document_information.put("ESIC Number ", esic.getEsicNumber());
						document_information.put("Father/Husband Name ", esic.getFatherOrHusband());
						document_information.put("Insurance Number ", esic.getInsuranceNumber());
						document_information.put("First Date of Appointment ", esic.getFirstDateOfAppointment());
						document_information.put("Gender ", esic.getGender());
						document_information.put("Marital Status ", esic.getMaritalStatus());
						document_information.put("Nominee Name ", esic.getNomineeName());
						document_information.put("Nominee Aadhaar Number ", esic.getNomineeAadharNo());
						document_information.put("Nominee Address ", esic.getNomineeAddress());
						document_information.put("Present Address ", esic.getPresentAddress());
						document_information.put("Phone Number ", esic.getPhoneNumber());
						document_information.put("Permanent Address ", esic.getPermanentAddress());
						document_information.put("UHID ", esic.getUhid());
						document_information.put("Registration Date ", esic.getRegDate());
						List<ESICContriDetails> esicResFrequency = esicContriService.getByESICReqId(esic.getId());
						JSONArray contribution_details = new JSONArray();
						if(esicResFrequency !=null){
							for(int loop1=0;loop1 <esicResFrequency.size();loop1++){
								JSONObject contribution  = new JSONObject(esicResFrequency.get(loop1));
								contribution.put("employee_contribution", esicResFrequency.get(loop1).getEmp_contribution());
								contribution.put("no_of_days_wages_paid", esicResFrequency.get(loop1).getNo_of_days_wage_paid());
								contribution.put("wages_period", esicResFrequency.get(loop1).getWage_period());
								contribution.put("total_wages", esicResFrequency.get(loop1).getTotal_monthly_wages());	
								contribution_details.put(contribution);
							}
							
						}
						document_information.put("contribution_details", contribution_details);
						
						document_esic_data.put("document_information", document_information);
					}
					else{
						document_information.put("Status as per Source", esic.getStatusAsPerSource());
						document_esic_data.put("document_information", document_information);
					}
					
					detailed_document_report_data.add(document_esic_data);
					customer_document_information.put("esic", document_esic_data);
				}
				
			
			DrivingLicenceMaster dl = dlService.getByCustomerId(entity.getId());
			if (dl != null) {
				JSONObject document_dl_data = new JSONObject();
				document_dl_data.put("document", "Driving Licence");
				document_dl_data.put("number", (dl).getDocument());
				document_dl_data.put("status", (dl).getApiValidation());
				document_dl_data.put("status_as_per_source", (dl).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((dl).getStatusAsPerSource())) {
					document_information.put("Status as per Source", dl.getStatusAsPerSource());
					document_information.put("Name", dl.getName());
					document_information.put("Father/Husband :", dl.getFatherName());
					document_information.put("DOB ", dl.getDob());
					document_information.put("Address ", dl.getAddress());
					document_information.put("RTO", dl.getRto());
					document_information.put("Date of Issue", dl.getDateOfIssue());
					document_information.put("Non Transport Valid From", dl.getNonTransportValidFrom());
					document_information.put("Non Transport Valid To", dl.getNonTransportValidTo());
					document_information.put("Transport Valid From ", dl.getTransportValidFrom());
					document_information.put("Transport Valid Till ", dl.getTransportValidTo());
					List<DrivingLicenceCOV> drivingLicenceCOV = drivingLicenceCOVService
							.getByLicence(dl.getId());
					JSONArray validity_details = new JSONArray();

					if (drivingLicenceCOV != null) {
						for (int idl = 0; idl < drivingLicenceCOV.size(); idl++) {
							JSONObject validity = new JSONObject(drivingLicenceCOV.get(idl));
							validity.put("cov_category", drivingLicenceCOV.get(idl).getCOVCategory());
							validity.put("class_of_vehicle", drivingLicenceCOV.get(idl).getClassOfVehicle());
							validity.put("issue_date", drivingLicenceCOV.get(idl).getIssueDate());
							validity_details.put(validity);
						}
					}
					document_information.put("validity_details", validity_details);

					document_dl_data.put("document_information", document_information);
				} else {
					document_information.put("Status as per Source", dl.getStatusAsPerSource());
					document_dl_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_dl_data);
				customer_document_information.put("dl", document_dl_data);
			}
			// Electricity bill Populated on KYC information page with
			// the data received
			Electricity electricityBill = electricityService.getByCustomerId(entity.getId());
			if (electricityBill != null) {
				JSONObject document_electricity_data = new JSONObject();
				document_electricity_data.put("document", "Electricity Bill");
				document_electricity_data.put("number", (electricityBill).getDocument());
				document_electricity_data.put("status_as_per_source", (electricityBill).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((electricityBill).getStatusAsPerSource())) {
					document_information.put("Source", electricityBill.getServiceProvider());
					// document_information.put("Source",
					// elecBoardService.getBoardName(electricityBill.getServiceProvider()));
					document_information.put("Electricity Bill Number", electricityBill.getBill_number());
					document_information.put("Consumer Name", electricityBill.getConsumer_name());
					document_information.put("Consumer Number", electricityBill.getConsumer_number());
					document_information.put("Address", electricityBill.getAddress());
					document_information.put("Bill Date", electricityBill.getBill_date());
					document_information.put("Bill Due Date", electricityBill.getBill_due_date());
					document_information.put("Total Amount", electricityBill.getTotal_amount());
					document_information.put("Amount ", electricityBill.getBill_amount());
					document_information.put("Amount Payable", electricityBill.getAmount_payable());
					document_information.put("Bill Date Issued", electricityBill.getBill_issue_date());
					document_electricity_data.put("document_information", document_information);
				} else {
					document_information.put("Status As Per Source", electricityBill.getStatusAsPerSource());
					document_information.put("Source", electricityBill.getServiceProvider());
					document_electricity_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_electricity_data);
				customer_document_information.put("electricity", document_electricity_data);
			}

			LLPIN llpin = llpinService.getByCustomerId(entity.getId());
			if (llpin != null) {
				JSONObject document_llpin_data = new JSONObject();
				document_llpin_data.put("document", "LLPIN");
				document_llpin_data.put("number", (llpin).getDocument());
				document_llpin_data.put("status", (llpin).getApiValidation());
				if(llpin.getLlpStatus() == null || llpin.getLlpStatus().length()<1){
					document_llpin_data.put("status_as_per_source", (llpin).getStatusAsPerSource());
				}
				else{
				document_llpin_data.put("status_as_per_source", (llpin).getLlpStatus());
				}
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((llpin).getStatusAsPerSource())) {
					document_information.put("LLPIN", llpin.getLlpin());
					document_information.put("LLP Name", llpin.getLlpName());
					document_information.put("Number of Partners", llpin.getNoOfPartners());
					document_information.put("Number of Designated Partners",
							llpin.getNoOfDesignatedPartners());
					document_information.put("ROC Code", llpin.getRocCode());
					document_information.put("Date of Incorporation", llpin.getDateOfIncorporation());
					document_information.put("Registered Address", llpin.getRegisteredAddress());
					document_information.put("Email Id", llpin.getEmailID());
					document_information.put("Previous firm/ company details, if applicable",
							llpin.getPreviousFirmOrCompany());
					document_information.put("Total Obligation of Contribution",
							llpin.getTotalObligationOfContribution());
					document_information.put("Main division of business activity",
							llpin.getMainDivisionOfBusiness());
					document_information.put("Date of last Statement of Accounts and Solvency",
							llpin.getDateOfLastStatementOfAccountsAndSolvency());
					document_information.put("Date of last Annual Return", llpin.getDateOfLastAnnualReturn());
					document_information.put("LLP Status", llpin.getLlpStatus());

					document_llpin_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_llpin_data);
				customer_document_information.put("llpin", document_llpin_data);
			}

			CIN cin = cinService.getByCustomerId(entity.getId());
			if (cin != null) {
				JSONObject document_cin_data = new JSONObject();
				document_cin_data.put("document", "CIN");
				document_cin_data.put("number", (cin).getDocument());
				document_cin_data.put("status", (cin).getApiValidation());
				if (cin.getCompanyStatus()==null || cin.getCompanyStatus().length()<1){
					document_cin_data.put("status_as_per_source", cin.getStatusAsPerSource());														
				}
				else{
					document_cin_data.put("status_as_per_source", (cin).getCompanyStatus());							
				}
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((cin).getStatusAsPerSource())) {
					document_information.put("CIN", cin.getCin());
					document_information.put("Company Name", cin.getCompanyName());
					document_information.put("ROC Code", cin.getRocCode());
					document_information.put("Registration Number", cin.getRegistrationNo());
					document_information.put("Company Category", cin.getCompanyCategory());
					document_information.put("Company SubCategory", cin.getCompanySubcategory());
					document_information.put("Class of Company", cin.getClassOfCompany());
					document_information.put("Authorised Capital(Rs)", cin.getAuthorizedCapital());
					document_information.put("Paid up Capital(Rs)", cin.getPaidUpCapital());
					document_information.put("Number of Members", cin.getNoOfMembers());
					document_information.put("Date of Incorporation", cin.getDateOfIncorporation());
					document_information.put("Registered Address", cin.getRegisteredAddress());
					document_information.put("Email Id", cin.getEmailID());
					document_information.put("Whether Listed or not", cin.getListedOrNot());
					document_information.put("Date of last AGM", cin.getDateOfLastAGM());
					document_information.put("Date of Balance Sheet", cin.getDateOfBalanceSheet());
					document_information.put("Company Status(for efiling)", cin.getCompanyStatus());

					document_cin_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_cin_data);
				customer_document_information.put("cin", document_cin_data);
			}

			FCRN fcrn = fcrnService.getByCustomerId(entity.getId());
			if (fcrn != null) {
				JSONObject document_fcrn_data = new JSONObject();
				document_fcrn_data.put("document", "FCRN");
				document_fcrn_data.put("number", (fcrn).getDocument());
				document_fcrn_data.put("status", (fcrn).getApiValidation());
				if(fcrn.getCompanyStatus() == null || fcrn.getCompanyStatus().length()<1){
					document_fcrn_data.put("status_as_per_source", (fcrn).getStatusAsPerSource());
				}
				else{
					document_fcrn_data.put("status_as_per_source", (fcrn).getCompanyStatus());
				}
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((fcrn).getStatusAsPerSource())) {
					document_information.put("FCRN", fcrn.getFcrn());
					document_information.put("Company Name", fcrn.getCompanyName());
					document_information.put("Date of Incorporation", fcrn.getDateOfIncorporation());
					document_information.put("Country of Incorporation", fcrn.getCountryOfIncorporation());
					document_information.put("Registered Address", fcrn.getRegisteredAddress());
					document_information.put("Email Id", fcrn.getEmailID());
					document_information.put("Foreign Company with Share Capital",
							fcrn.getForeignCompanyWithShareCapital());
					document_information.put("Company Status", fcrn.getCompanyStatus());
					document_information.put("Type of Office", fcrn.getTypeOfOffice());
					document_information.put("Details", fcrn.getDetails());
					document_information.put("Main division of business activity",
							fcrn.getMainDivisionOfBusiness());
					document_information.put("Description of main division",
							fcrn.getDescriptionOfMainDivision());

					document_fcrn_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_fcrn_data);
				customer_document_information.put("fcrn", document_fcrn_data);
			}

			FLLPIN fllpin = fllpinService.getByCustomerId(entity.getId());
			if (fllpin != null) {
				JSONObject document_fllpin_data = new JSONObject();
				document_fllpin_data.put("document", "FLLPIN");
				document_fllpin_data.put("number", (fllpin).getDocument());
				document_fllpin_data.put("status", (fllpin).getApiValidation());
				if(fllpin.getFllpStatus() == null || fllpin.getFllpStatus().length()<1){
					document_fllpin_data.put("status_as_per_source", (fllpin).getStatusAsPerSource());
				}
				else{
				document_fllpin_data.put("status_as_per_source", (fllpin).getFllpStatus());
				}
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((fllpin).getStatusAsPerSource())) {
					document_information.put("FLLPIN", fllpin.getFllpin());
					document_information.put("Foreign LLP Name", fllpin.getForeignLlpName());
					document_information.put("Country of Incorporation", fllpin.getCountryOfIncorporation());
					document_information.put("Number of Authorised Representatives",
							fllpin.getNoOfAuthorisedRepresentatives());
					document_information.put("Date of Incorporation", fllpin.getDateOfIncorporation());
					document_information.put("Registered Address", fllpin.getRegisteredAddress());
					document_information.put("Email Id", fllpin.getEmailID());
					document_information.put("Type of Office", fllpin.getTypeOfOffice());
					document_information.put("Details", fllpin.getDetails());
					document_information.put("Main division of business activity",
							fllpin.getMainDivisionOfBusiness());
					document_information.put("Description of main division",
							fllpin.getDescriptionOfMainDivision());
					document_information.put("Date of last Statement of Accounts and Solvency",
							fllpin.getDateOfLastStatementOfAccountsAndSolvency());
					document_information.put("FLLP Status", fllpin.getFllpStatus());

					document_fllpin_data.put("document_information", document_information);
				}
				detailed_document_report_data.add(document_fllpin_data);
				customer_document_information.put("fllpin", document_fllpin_data);
			}

			VAT vat = vatService.getByCustomerId(entity.getId());
			if (vat != null) {
				JSONObject document_vat_data = new JSONObject();
				document_vat_data.put("document", "VAT");
				document_vat_data.put("number", (vat).getDocument());
				document_vat_data.put("status", (vat).getApiValidation());
				document_vat_data.put("status_as_per_source", (vat).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((vat).getStatusAsPerSource())) {
					document_information.put("Dealer Name", vat.getDealerName());
					document_information.put("Tin Number", vat.getTinNo());
					document_information.put("Effective/Canceled Date", vat.getEffectiveCancelledDate());
					document_information.put("Address1", vat.getAddress1());
					document_information.put("Street Name", vat.getStreetName());
					document_information.put("Address2", vat.getAddress2());
					document_information.put("Address3", vat.getAddress3());
					document_information.put("Taluka Name", vat.getTalukaName());
					document_information.put("District Name", vat.getDistrictName());
					document_information.put("City Name", vat.getCityName());
					document_information.put("State Name", vat.getStateName());
					document_information.put("Pin Code", vat.getPinCode());
					document_information.put("Old RC No", vat.getOldRCNo());
					document_information.put("Location Name", vat.getLocationName());
					document_information.put("Act Name", vat.getActName());

					document_vat_data.put("document_information", document_information);

					JSONArray frequencies = new JSONArray();
					List<VATFrequency> vatFrequencies = vatFrequencyService.getByVatId(vat.getId());
					if (vatFrequencies != null) {
						for (int ivat = 0; ivat < vatFrequencies.size(); ivat++) {
							JSONObject frequency;
							VATFrequency vatFrequency = vatFrequencies.get(ivat);
							if (vatFrequency != null) {
								frequency = new JSONObject(vatFrequency);
								frequencies.put(frequency);
							}
						}
					}
					if (frequencies.length() > 0) {
						document_vat_data.put("frequencies", frequencies);
					}
				}
				detailed_document_report_data.add(document_vat_data);
				customer_document_information.put("vat", document_vat_data);
			}

			CST cst = cstService.getByCustomerId(entity.getId());
			if (cst != null) {
				JSONObject document_cst_data = new JSONObject();
				document_cst_data.put("document", "CST");
				document_cst_data.put("number", (cst).getDocument());
				document_cst_data.put("status", (cst).getApiValidation());
				document_cst_data.put("status_as_per_source", (cst).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((cst).getStatusAsPerSource())) {
					document_information.put("Dealer Name", cst.getDealerName());
					document_information.put("Tin Number", cst.getTinNo());
					document_information.put("Effective/Canceled Date", cst.getEffectiveCancelledDate());
					document_information.put("Address1", cst.getAddress1());
					document_information.put("Street Name", cst.getStreetName());
					document_information.put("Address2", cst.getAddress2());
					document_information.put("Address3", cst.getAddress3());
					document_information.put("Taluka Name", cst.getTalukaName());
					document_information.put("District Name", cst.getDistrictName());
					document_information.put("City Name", cst.getCityName());
					document_information.put("State Name", cst.getStateName());
					document_information.put("Pin Code", cst.getPinCode());
					document_information.put("Old RC No", cst.getOldRCNo());
					document_information.put("Location Name", cst.getLocationName());
					document_information.put("Act Name", cst.getActName());

					document_cst_data.put("document_information", document_information);

					JSONArray frequencies = new JSONArray();
					List<CSTFrequency> cstFrequencies = cstFrequencyService.getByCstId(cst.getId());
					if (cstFrequencies != null) {
						for (int icst = 0; icst < cstFrequencies.size(); icst++) {
							JSONObject frequency;
							CSTFrequency cstFrequency = cstFrequencies.get(icst);
							if (cstFrequency != null) {
								frequency = new JSONObject(cstFrequency);
								frequencies.put(frequency);
							}
						}
					}
					if (frequencies.length() > 0) {
						document_cst_data.put("frequencies", frequencies);
					}
				}
				detailed_document_report_data.add(document_cst_data);
				customer_document_information.put("cst", document_cst_data);
			}

			ProfessionalTax professionalTax = professionalTaxService.getByCustomerId(entity.getId());
			if (professionalTax != null) {
				JSONObject document_professionalTax_data = new JSONObject();
				document_professionalTax_data.put("document", "Professional Tax");
				document_professionalTax_data.put("number", (professionalTax).getDocument());
				document_professionalTax_data.put("status", (professionalTax).getApiValidation());
				document_professionalTax_data.put("status_as_per_source",
						(professionalTax).getStatusAsPerSource());
				Map document_information = new LinkedHashMap();
				if (CustomerEntityHelper.showDetailedDocumentData((professionalTax).getStatusAsPerSource())) {
					document_information.put("Dealer Name", professionalTax.getDealerName());
					document_information.put("Tin Number", professionalTax.getTinNo());
					document_information.put("Effective/Canceled Date",
							professionalTax.getEffectiveCancelledDate());
					document_information.put("Address1", professionalTax.getAddress1());
					document_information.put("Street Name", professionalTax.getStreetName());
					document_information.put("Address2", professionalTax.getAddress2());
					document_information.put("Address3", professionalTax.getAddress3());
					document_information.put("Taluka Name", professionalTax.getTalukaName());
					document_information.put("District Name", professionalTax.getDistrictName());
					document_information.put("City Name", professionalTax.getCityName());
					document_information.put("State Name", professionalTax.getStateName());
					document_information.put("Pin Code", professionalTax.getPinCode());
					document_information.put("Old RC No", professionalTax.getOldRCNo());
					document_information.put("Location Name", professionalTax.getLocationName());
					document_information.put("Act Name", professionalTax.getActName());

					document_professionalTax_data.put("document_information", document_information);

					JSONArray frequencies = new JSONArray();
					List<ProfessionalTaxFrequency> professionalTaxFrequencies = professionalTaxFrequencyService
							.getByProfessionalTaxId(professionalTax.getId());
					if (professionalTaxFrequencies != null) {
						for (int ipt = 0; ipt < professionalTaxFrequencies.size(); ipt++) {
							JSONObject frequency;
							ProfessionalTaxFrequency professionalTaxFrequency = professionalTaxFrequencies
									.get(ipt);
							if (professionalTaxFrequency != null) {
								frequency = new JSONObject(professionalTaxFrequency);
								frequencies.put(frequency);
							}
						}
					}
					if (frequencies.length() > 0) {
						document_professionalTax_data.put("frequencies", frequencies);
					}
				}
				detailed_document_report_data.add(document_professionalTax_data);
				customer_document_information.put("pt", document_professionalTax_data);
			}

			Passport passport = passportService.getByCustomerId(entity.getId());
			if (passport != null) {
				JSONObject document_passport_data = new JSONObject();
				document_passport_data.put("document", "PASSPORT");
				document_passport_data.put("number", (passport).getDocument());
				document_passport_data.put("status", "valid");
				// document_passport_data.put("status_as_per_source",
				// "valid");
				try {
					JSONObject pasportChecksum = new ApiCallHelper().getPassportChecksum(passport.getDocument(),
							entity.getDob(), passport.getExpiryDate());
					document_passport_data.put("checksum", pasportChecksum);
					document_passport_data.put("expiry_date", passport.getExpiryDate());
					document_passport_data.put("status_as_per_source", "Bar Code string generated");
				} catch (Exception e) {
					document_passport_data.put("status_as_per_source", "invalid");
					System.out.print("Error :- " + e);
				}
				detailed_document_report_data.add(document_passport_data);
				customer_document_information.put("passport", document_passport_data);
			}

			entity_data.put("detailed_document_report_data", detailed_document_report_data);
			entity_data.put("customer_document_information", customer_document_information);
			entity_data.put("isEdited", request1.getIsEdited());
			response.put(entity_data);
			return response;
		}
		return response;
	}

}
