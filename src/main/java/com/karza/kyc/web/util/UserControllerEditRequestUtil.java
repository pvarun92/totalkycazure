package com.karza.kyc.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.engine.transaction.jta.platform.internal.JtaSynchronizationStrategy;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;

public class UserControllerEditRequestUtil {
	public static int checkAndSaveDraftInfoForPAN(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,PanCardService panCardService,ItrService itrService) throws JSONException, ParseException{
		String check_pan = ((JSONObject) requestJSONData.get("pan")).get("check").toString();
		if (Boolean.parseBoolean(check_pan)) {
			no_of_documents++;
			String pan_document = ((JSONObject) requestJSONData.get("pan")).get("unique_no").toString();
			String entity_type = requestJSONData.get("entity_type").toString();
			try {
				PanCard panCard = null;
				PanCard existingPanCard = panCardService.getByCustomerId(custEntityFrmReqJSON.getId());
				panCard = new PanCard(pan_document, entity_type, custEntityFrmReqJSON.getId());
				panCard.setAPICount(0);
				if (existingPanCard == null) {
					panCardService.save(panCard);
				} else {
					panCard.setId(existingPanCard.getId());
					panCardService.update(panCard);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			PanCard panCard = panCardService.getByCustomerId(custEntityFrmReqJSON.getId());
			if (panCard != null) {
				String check_itr = ((JSONObject) requestJSONData.get("itr")).get("check").toString();
				if (Boolean.parseBoolean(check_itr)) {
					no_of_documents++;
					JSONArray itr_fields = ((JSONObject) requestJSONData.get("itr"))
							.getJSONArray("itr_fields");
					List<Itr> existingItrs = itrService.getByPanId(panCard.getId());
					if (existingItrs != null) {
						for (int k = 0; k < existingItrs.size(); k++) {
							Itr temItr = existingItrs.get(k);
							itrService.delete(temItr);
						}
					}
					for (int j = 0; j < itr_fields.length(); j++) {
						String acknowledgmentNumber = (itr_fields.getJSONObject(j))
								.get("acknowledgement_number").toString();
						String dateObj = (itr_fields.getJSONObject(j).get("date_of_filling").toString());
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						Date dateOfFilling = null;
						if (!dateObj.isEmpty()) {
							String day = dateObj.split("/")[0];
							String month = dateObj.split("/")[1];
							String year = dateObj.split("/")[2];
							dateOfFilling = formatter.parse(year + "-" + month + "-" + day);
						} else {
							dateOfFilling = null;
						}
						String assessmentYear = (itr_fields.getJSONObject(j)).get("assesment_year").toString();
						if (acknowledgmentNumber != "" && assessmentYear != "") {
							try {
								Itr itrObj = new Itr(acknowledgmentNumber, assessmentYear, panCard.getId(),
										dateOfFilling, custEntityFrmReqJSON.getId());
								itrService.save(itrObj);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForVoter(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,VoterIDService voterIDService) throws JSONException{
		try{
		String check_voterId = ((JSONObject) requestJSONData.get("voting")).get("check").toString();
		if (Boolean.parseBoolean(check_voterId)) {
			no_of_documents++;
			String voter_document = ((JSONObject) requestJSONData.get("voting")).get("unique_no").toString();
		//	String District = ((JSONObject) requestJSONData.get("voting")).get("voter_district").toString();
			//String state = ((JSONObject) requestJSONData.get("voting")).get("voter_state").toString();
			//String assembly_consituency = ((JSONObject) requestJSONData.get("voting")).get("voter_assembly").toString();
			
				VoterID voterID = null;
				VoterID existingVoterID = voterIDService.getByCustomerId(custEntityFrmReqJSON.getId());
				/*voterID = new VoterID(voter_document, custEntityFrmReqJSON.getId(), District, state,
						assembly_consituency);*/
				voterID = new VoterID(voter_document,custEntityFrmReqJSON.getId());
				voterID.setAPICount(0);
				if (existingVoterID == null) {
					voterIDService.save(voterID);
				} else {
					voterID.setId(existingVoterID.getId());
					voterIDService.update(voterID);
				}
			} 
		return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForAadhar(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,AadharCardService aadharCardService) throws JSONException{
		try {
		String check_aadhar = ((JSONObject) requestJSONData.get("aadhar")).get("check").toString();
		if (Boolean.parseBoolean(check_aadhar)) {
			no_of_documents++;
			String aadhar_document = ((JSONObject) requestJSONData.get("aadhar")).get("unique_no").toString();
			
				AadharCard aadharCard;
				AadharCard existingAadharCard = aadharCardService.getByCustomerId(custEntityFrmReqJSON.getId());
				aadharCard = new AadharCard(aadhar_document, custEntityFrmReqJSON.getId());
				if (existingAadharCard == null) {
					aadharCardService.save(aadharCard);
				} else {
					aadharCard.setId(existingAadharCard.getId());
					aadharCardService.update(aadharCard);
				}
			} 
		return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForTelephone(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON, TelephoneService telephoneService) throws JSONException{
		try {
		String check_telephone = ((JSONObject) requestJSONData.get("telephone")).get("check").toString();
		if(Boolean.parseBoolean(check_telephone)){
			no_of_documents++;
			String telephone_document = ((JSONObject) requestJSONData.get("telephone")).get("unique_no").toString();
			
				Telephone telephone;
				Telephone existingTelephoneBill = telephoneService.getByCustomerId(custEntityFrmReqJSON.getId());
				telephone = new Telephone(telephone_document,custEntityFrmReqJSON.getId());
				telephone.setAPICount(0);
				if(existingTelephoneBill == null){
					telephoneService.save(telephone);
				} else{
					telephone.setId(existingTelephoneBill.getId());
					telephoneService.update(telephone);
				}
			} 
		return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForIEC(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,IECService iecService) throws JSONException{
		try
		{
		String check_iec = ((JSONObject) requestJSONData.get("iec")).get("check").toString();
		if (Boolean.parseBoolean(check_iec)) {
			no_of_documents++;
			String iec_document = ((JSONObject) requestJSONData.get("iec")).get("unique_no").toString();
				IEC iec;
				IEC existingIec = iecService.getByCustomerId(custEntityFrmReqJSON.getId());
				iec = new IEC(custEntityFrmReqJSON.getId(), iec_document);
				iec.setAPICount(0);
				if (existingIec == null) {
					iecService.save(iec);
				} else {
					iec.setId(existingIec.getId());
					iecService.update(iec);
				}
			} 
		return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForDL(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,DrivingLicenceMasterService dlService)throws JSONException{
		try {
		String check_dl = ((JSONObject) requestJSONData.get("dl")).get("check").toString();
		if (Boolean.parseBoolean(check_dl)) {
			no_of_documents++;
			String dl_document = ((JSONObject) requestJSONData.get("dl")).get("unique_no").toString();
		
				DrivingLicenceMaster existingDl = dlService.getByCustomerId(custEntityFrmReqJSON.getId());
				DrivingLicenceMaster dl = new DrivingLicenceMaster(dl_document, custEntityFrmReqJSON.getId());
				dl.setAPICount(0);
				if (existingDl == null) {
					dl = dlService.save(dl);
				} else {
					dl.setId(existingDl.getId());
					dlService.update(dl);
				}
			}
			return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}

	public static int checkAndSaveDraftInfoForElectricity(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,ElectricityService electricityService,Logger logger) throws JSONException{
		String check_electricity = ((JSONObject) requestJSONData.get("electricity")).get("check").toString();
		if (Boolean.parseBoolean(check_electricity)) {
			no_of_documents++;
			boolean isTrue = ElectricityWebUtil.saveElectrictyData(requestJSONData, electricityService,custEntityFrmReqJSON,logger);
			if (!isTrue) {
				// Varun Take Care
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForServiceTax(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,ServiceTaxService serviceTaxService)throws JSONException{
		try {
		String check_service_tax = ((JSONObject) requestJSONData.get("service_tax")).get("check").toString();
		if (Boolean.parseBoolean(check_service_tax)) {
			int apiCount = 0;
			no_of_documents++;
			String service_tax_document = ((JSONObject) requestJSONData.get("service_tax")).get("unique_no")
					.toString();
		
				ServiceTax serviceTax;
				ServiceTax existingServiceTax = serviceTaxService.getByCustomerId(custEntityFrmReqJSON.getId());
				serviceTax = new ServiceTax(custEntityFrmReqJSON.getId(), service_tax_document,apiCount);
				serviceTax.setAPICount(0);
				if (existingServiceTax == null) {
					serviceTaxService.save(serviceTax);
				} else {
					serviceTax.setId(existingServiceTax.getId());
					serviceTaxService.update(serviceTax);
				}
			} 
		return no_of_documents;
		}
		
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}

	public static int checkAndSaveDraftInfoForExcise(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,ExciseService exciseService) throws JSONException{
		try {
		String check_excise = ((JSONObject) requestJSONData.get("excise")).get("check").toString();
		if (Boolean.parseBoolean(check_excise)) {
			Integer apiCount = 0;
			no_of_documents++;
			String excise_document = ((JSONObject) requestJSONData.get("excise")).get("unique_no").toString();
			
				Excise excise;
				Excise existingExcise = exciseService.getByCustomerId(custEntityFrmReqJSON.getId());
				excise = new Excise(excise_document, custEntityFrmReqJSON.getId(),apiCount);
				excise.setAPICount(0);
				if (existingExcise == null) {
					exciseService.save(excise);
				} else {
					excise.setId(existingExcise.getId());
					exciseService.update(excise);
				}
			
			}
			return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForLLPIN(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,LLPINService llpinService,FLLPINService fllpinService,CINService cinService,FCRNService fcrnService) throws JSONException{
		try {
			String check_llpin = ((JSONObject) requestJSONData.get("llpin")).get("check").toString();
			if (Boolean.parseBoolean(check_llpin)) {
				int apiCount = 0;
				no_of_documents++;
				String llpin_document = ((JSONObject) requestJSONData.get("llpin")).get("unique_no").toString();
	
				LLPIN llpin;
				LLPIN existingllpin = llpinService.getByCustomerId(custEntityFrmReqJSON.getId());
				llpin = new LLPIN(llpin_document, custEntityFrmReqJSON.getId(), apiCount);
				llpin.setAPICount(0);
				if (existingllpin == null) {
					llpinService.save(llpin);
				} else {
					llpin.setId(existingllpin.getId());
					llpinService.update(llpin);
				}
				FLLPIN existingFllpin = fllpinService.getByCustomerId(custEntityFrmReqJSON.getId());
				if (existingFllpin != null) {
					fllpinService.delete(existingFllpin);
				}
	
				CIN existingCin = cinService.getByCustomerId(custEntityFrmReqJSON.getId());
				if (existingCin != null) {
					cinService.delete(existingCin);
				}
	
				FCRN existingFcrn = fcrnService.getByCustomerId(custEntityFrmReqJSON.getId());
				if (existingFcrn != null) {
					fcrnService.delete(existingFcrn);
				}
			}
			return no_of_documents;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}

	public static int checkAndSaveDraftInfoForVAT(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,VATService vatService) throws JSONException{
		try
		{
			String check_vat = ((JSONObject) requestJSONData.get("vat")).get("check").toString();
		if (Boolean.parseBoolean(check_vat)) {
			no_of_documents++;
			String vat_document = ((JSONObject) requestJSONData.get("vat")).get("unique_no").toString();
			
				VAT vat;
				VAT existingVat = vatService.getByCustomerId(custEntityFrmReqJSON.getId());
				vat = new VAT(vat_document, custEntityFrmReqJSON.getId());
				if (existingVat == null) {
					vat = vatService.save(vat);
				} else {
					vat.setId(existingVat.getId());
					vatService.update(vat);
				}
		}
		return no_of_documents;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForCST(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,CSTService cstService) throws JSONException{
		try
		{
		String check_cst = ((JSONObject) requestJSONData.get("cst")).get("check").toString();
		if (Boolean.parseBoolean(check_cst)) {
			no_of_documents++;
			String cst_document = ((JSONObject) requestJSONData.get("cst")).get("unique_no").toString();
			
				CST cst;
				CST existingCst = cstService.getByCustomerId(custEntityFrmReqJSON.getId());
				cst = new CST(cst_document, custEntityFrmReqJSON.getId());
				if (existingCst == null) {
					cst = cstService.save(cst);
				} else {
					cst.setId(existingCst.getId());
					cstService.update(cst);
				}
		}
		return no_of_documents;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForPT(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,ProfessionalTaxService professionalTaxService) throws JSONException{
		try
		{
		String check_pt = ((JSONObject) requestJSONData.get("pt")).get("check").toString();
		if (Boolean.parseBoolean(check_pt)) {
			no_of_documents++;
			String pt_document = ((JSONObject) requestJSONData.get("pt")).get("unique_no").toString();
			
				ProfessionalTax professionalTax;
				ProfessionalTax existingProfessionalTax = professionalTaxService
						.getByCustomerId(custEntityFrmReqJSON.getId());
				professionalTax = new ProfessionalTax(pt_document, custEntityFrmReqJSON.getId());
				if (existingProfessionalTax == null) {
					professionalTax = professionalTaxService.save(professionalTax);
				} else {
					professionalTax.setId(existingProfessionalTax.getId());
					professionalTaxService.update(professionalTax);
				}
		}
		return no_of_documents;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}

	public static int checkAndSaveDraftInforForPassport(JSONObject requestJSONData,int no_of_documents,CustomerEntity custEntityFrmReqJSON,PassportService passportService) throws JSONException{
		try 
		{
		String check_passport = ((JSONObject) requestJSONData.get("passport")).get("check").toString();
		if (Boolean.parseBoolean(check_passport)) {
			
				no_of_documents++;
				String passport_document = ((JSONObject) requestJSONData.get("passport")).get("unique_no")
						.toString();
				Date passport_expiry =null;
				String passport_expiry_input="";
				try {
					 passport_expiry_input= ((JSONObject) requestJSONData.get("passport")).get("expiry_date")
							.toString();
					if(passport_expiry_input!=null&&!passport_expiry_input.equals("")){
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
						passport_expiry = formatter.parse(passport_expiry_input);
					}
					
				} catch (ParseException e) {
					System.out.println(e.toString());
				}
				Passport passport;
				Passport existingPassport = passportService.getByCustomerId(custEntityFrmReqJSON.getId());
				passport = new Passport(passport_document, custEntityFrmReqJSON.getId(), custEntityFrmReqJSON.getCountry(),
						custEntityFrmReqJSON.getAddress(), passport_expiry, custEntityFrmReqJSON.getDob());
				if (existingPassport == null) {
					passportService.save(passport);
				} else {
					passport.setId(existingPassport.getId());
					passportService.update(passport);
				}
		}
		return no_of_documents;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForEPFO(JSONObject requestJSONData,int no_of_documents,
			CustomerEntity custEntityFrmReqJSON, EPFORequestService epfoService) throws JSONException{
		try {
		String check_uan = ((JSONObject) requestJSONData.get("uan")).get("check").toString();
		if(Boolean.parseBoolean(check_uan)){
			no_of_documents++;
			String uan_document = ((JSONObject) requestJSONData.get("uan")).get("unique_no").toString();
			
				EPFORequest epfo ;
				EPFORequest existingEpfo = epfoService.getByCustomerID(custEntityFrmReqJSON.getId());
				epfo = new EPFORequest(uan_document,custEntityFrmReqJSON.getId());
				epfo.setAPICount(0);
				if(existingEpfo == null){
					epfoService.Save(epfo);
				} else{
					epfo.setId(existingEpfo.getId());
					epfoService.update(epfo);
				}
			} 
		return no_of_documents;
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForLPG(JSONObject requestJSONData, int no_of_documents,
			CustomerEntity custEntityFrmReqJSON, LPGService lpgService) throws JSONException {
		try {
			String check_lpg = ((JSONObject) requestJSONData.get("lpg")).get("check").toString();
			if (Boolean.parseBoolean(check_lpg)) {
				no_of_documents++;
				String lpg_document = ((JSONObject) requestJSONData.get("lpg")).get("unique_no").toString();
				LPG lpg;
				LPG existingLpg = lpgService.getByCustomerId(custEntityFrmReqJSON.getId());
				lpg = new LPG(lpg_document, custEntityFrmReqJSON.getId());
				lpg.setAPICount(0);
				if (existingLpg == null) {
					lpgService.save(lpg);
				} else {
					lpg.setId(existingLpg.getId());
					lpgService.update(lpg);
				}
			}
			return no_of_documents;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
	
	public static int checkAndSaveDraftInfoForESIC(JSONObject requestJSONData, int no_of_documents,
			CustomerEntity custEntityFrmReqJSON,ESICService esicService){
		try {
			  String check_esic = ((JSONObject) requestJSONData.get("esic")).get("check").toString();
			  if(Boolean.parseBoolean(check_esic)){
				  no_of_documents++;
				  String esic_document = ((JSONObject) requestJSONData.get("esic")).get("unique_no").toString();
				  ESIC esic;
				  ESIC existingEsic = esicService.getByCustomerId(custEntityFrmReqJSON.getId());
				  esic = new ESIC(esic_document,custEntityFrmReqJSON.getId());
				  esic.setAPICount(0);
				  if(existingEsic == null){
					  esicService.save(esic);
				  }
				  else{
					  esic.setId(existingEsic.getId());
					  esicService.update(esic);
				  }
			  }
			  return no_of_documents;
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		return no_of_documents;
	}
}
