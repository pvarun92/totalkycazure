package com.karza.kyc.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.BranchMasterService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTFrequencyService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.CustomerEntityService;
import com.karza.kyc.service.CustomerMasterService;
import com.karza.kyc.service.DrivingLicenceCOVService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.EPFOResponseService;
import com.karza.kyc.service.ESICContriService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityBoardService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.LocationMasterService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxFrequencyService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.RequestService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.SubscriptionMasterService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.UserMasterService;
import com.karza.kyc.service.UserService;
import com.karza.kyc.service.UserSessionDetailService;
import com.karza.kyc.service.VATFrequencyService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;

public class UserControlllerEntityUtil {

	public static JSONArray saveEntityData(List<CustomerEntity> entities,Request request1,BranchMasterService branchMasterService,UserMasterService userMasterService,VoterIDService voterIDService,
			PanCardService panCardService,AadharCardService aadharCardService,ItrService itrService,ServiceTaxService serviceTaxService,
			ESICService esicService,ESICContriService esicContriService,UserService userService,IECService iecService,TelephoneService
			telephoneService,EPFORequestService epfoReqService,LPGService lpgService,
			DrivingLicenceMasterService dlService,ElectricityService electricityService,LLPINService llpinService,
			CINService cinService,FCRNService fcrnService,FLLPINService fllpinService,VATService vatService,CSTService cstService,
			ProfessionalTaxService professionalTaxService,PassportService passportService,ExciseService exciseService,
			EPFOResponseService epfoResService,DrivingLicenceCOVService drivingLicenceCOVService,
			VATFrequencyService vatFrequencyService,CSTFrequencyService cstFrequencyService,
			ProfessionalTaxFrequencyService professionalTaxFrequencyService,CustomerEntityService customerEntityService ) throws JSONException{
		JSONArray entities_array = new JSONArray();
		for (int i = 0; i < entities.size(); i++) {
			CustomerEntity entity = entities.get(i);
			customerEntityService.update(entity);
			// System.out.println("Entity number " + entity.getId() +
			// "is updated by user " + current_user.getUserName());
			JSONObject entity_data = new JSONObject(entity);
			entity_data.put("branch", entity.getBranchId().toString());
			entity_data.put("request_status", request1.getRequest_status().toString());
			JSONObject document_data = new JSONObject();

			PanCard panCard = panCardService.getByCustomerId(entity.getId());
			if (panCard != null) {
				JSONObject document_pan_data = new JSONObject();
				document_pan_data.put("check", true);
				document_pan_data.put("unique_no", (panCard).getDocument());
				entity_data.put("pan", document_pan_data);
			}

			VoterID voterID = voterIDService.getByCustomerId(entity.getId());
			if (voterID != null) {
				JSONObject document_voter_data = new JSONObject();
				document_voter_data.put("check", true);
				document_voter_data.put("unique_no", (voterID).getDocument());
				entity_data.put("voting", document_voter_data);
			}

			List<Itr> itrs = itrService.getByCustomerId(entity.getId());
			if (itrs != null) {
				if (itrs.size() > 0) {
					JSONArray itrDataList = new JSONArray();
					for (int j = 0; j < itrs.size(); j++) {
						Itr itr = itrs.get(j);
						if (itr != null) {
							JSONObject document_itr_data = new JSONObject();
							document_itr_data.put("check", true);
							document_itr_data.put("number", (itrs.get(j)).getDocument());
							document_itr_data.put("assessment_year", (itrs.get(j)).getAssessmentYear());
							document_itr_data.put("date_of_filling", (itrs.get(j)).getDateOfFilling());
							itrDataList.put(document_itr_data);
						}
					}
					entity_data.put("itr", itrDataList);
				}
			}

			AadharCard aadharCard = aadharCardService.getByCustomerId(entity.getId());
			if (aadharCard != null) {
				JSONObject document_aadhar_data = new JSONObject();
				document_aadhar_data.put("check", true);
				document_aadhar_data.put("unique_no", (aadharCard).getDocument());
				entity_data.put("aadhar", document_aadhar_data);
			}
			Telephone telephone = telephoneService.getByCustomerId(entity.getId());
			if (telephone != null) {
				JSONObject document_telephone_data = new JSONObject();
				document_telephone_data.put("check", true);
				document_telephone_data.put("unique_no", (telephone).getDocument());
				entity_data.put("telephone", document_telephone_data);
			}
			EPFORequest epfo = epfoReqService.getByCustomerID(entity.getId());
			if(epfo != null){
				JSONObject document_uan_data = new JSONObject();
				document_uan_data.put("check", true);
				document_uan_data.put("unique_no", (epfo).getDocument());
				entity_data.put("uan", document_uan_data);
			}
			LPG lpg = lpgService.getByCustomerId(entity.getId());
			if(lpg != null){
				JSONObject document_lpg_data = new JSONObject();
				document_lpg_data.put("check", true);
				document_lpg_data.put("unique_no", (lpg).getDocument());
				entity_data.put("lpg", document_lpg_data);
			}
			ESIC esic = esicService.getByCustomerId(entity.getId());
			if(esic != null){
				JSONObject document_esic_data = new JSONObject();
				document_esic_data.put("check", true);
				document_esic_data.put("unique_no", (esic).getDocument());
				entity_data.put("esic", document_esic_data);
			}
			
			DrivingLicenceMaster drivingLicenceMaster = dlService.getByCustomerId(entity.getId());
			if (drivingLicenceMaster != null) {
				JSONObject document_dl_data = new JSONObject();
				document_dl_data.put("check", true);
				document_dl_data.put("unique_no", (drivingLicenceMaster).getDocument());
				entity_data.put("dl", document_dl_data);
			}

			ServiceTax serviceTax = serviceTaxService.getByCustomerId(entity.getId());
			if (serviceTax != null) {
				JSONObject document_serviceTax_data = new JSONObject();
				document_serviceTax_data.put("check", true);
				document_serviceTax_data.put("unique_no", (serviceTax).getDocument());
				entity_data.put("service_tax", document_serviceTax_data);
			}

			Excise excise = exciseService.getByCustomerId(entity.getId());
			if (excise != null) {
				JSONObject document_excise_data = new JSONObject();
				document_excise_data.put("check", true);
				document_excise_data.put("unique_no", (excise).getDocument());
				entity_data.put("excise", document_excise_data);
			}

			LLPIN llpin = llpinService.getByCustomerId(entity.getId());
			if (llpin != null) {
				JSONObject document_llpin_data = new JSONObject();
				document_llpin_data.put("check", true);
				document_llpin_data.put("unique_no", (llpin).getDocument());
				entity_data.put("llpin", document_llpin_data);
			}

			CIN cin = cinService.getByCustomerId(entity.getId());
			if (cin != null) {
				JSONObject document_cin_data = new JSONObject();
				document_cin_data.put("check", true);
				document_cin_data.put("unique_no", (cin).getDocument());
				entity_data.put("llpin", document_cin_data);
			}

			FCRN fcrn = fcrnService.getByCustomerId(entity.getId());
			if (fcrn != null) {
				JSONObject document_fcrn_data = new JSONObject();
				document_fcrn_data.put("check", true);
				document_fcrn_data.put("unique_no", (fcrn).getDocument());
				entity_data.put("llpin", document_fcrn_data);
			}

			FLLPIN fllpin = fllpinService.getByCustomerId(entity.getId());
			if (fllpin != null) {
				JSONObject document_fllpin_data = new JSONObject();
				document_fllpin_data.put("check", true);
				document_fllpin_data.put("unique_no", (fllpin).getDocument());
				entity_data.put("llpin", document_fllpin_data);
			}

			IEC iec = iecService.getByCustomerId(entity.getId());
			if (iec != null) {
				JSONObject document_iec_data = new JSONObject();
				document_iec_data.put("check", true);
				document_iec_data.put("unique_no", (iec).getDocument());
				entity_data.put("iec", document_iec_data);
			}

			VAT vat = vatService.getByCustomerId(entity.getId());
			if (vat != null) {
				JSONObject document_vat_data = new JSONObject();
				document_vat_data.put("check", true);
				document_vat_data.put("unique_no", (vat).getDocument());
				entity_data.put("vat", document_vat_data);
			}

			CST cst = cstService.getByCustomerId(entity.getId());
			if (cst != null) {
				JSONObject document_cst_data = new JSONObject();
				document_cst_data.put("check", true);
				document_cst_data.put("unique_no", (cst).getDocument());
				entity_data.put("cst", document_cst_data);
			}

			ProfessionalTax professionalTax = professionalTaxService.getByCustomerId(entity.getId());
			if (professionalTax != null) {
				JSONObject document_pt_data = new JSONObject();
				document_pt_data.put("check", true);
				document_pt_data.put("unique_no", (professionalTax).getDocument());
				entity_data.put("pt", document_pt_data);
			}

			Passport passport = passportService.getByCustomerId(entity.getId());
			if (passport != null) {
				JSONObject document_passport_data = new JSONObject();
				document_passport_data.put("check", true);
				document_passport_data.put("unique_no", (passport).getDocument());
				document_passport_data.put("expiry_date", (passport).getExpiryDate());
				entity_data.put("passport", document_passport_data);
			}
			Electricity electricity = electricityService.getByCustomerId(entity.getId());
			//Get the customer details for which the report is to be generated
			if (electricity != null) {
				JSONObject document_electricity_data = new JSONObject();
				document_electricity_data.put("check", true);
				document_electricity_data.put("board", electricity.getServiceProvider().toString());
				document_electricity_data.put("unique_no", electricity.getDocument());
				entity_data.put("electricity", document_electricity_data);
			}
			document_data.put("check", false);
			document_data.put("unique_no", "");
			entity_data.put("nrega", document_data);
			entities_array.put(entity_data);
			return entities_array;
	}
		return entities_array;
	}
}
