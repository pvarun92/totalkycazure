package com.karza.kyc.web.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.h2.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.Request;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.RequestService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.UserMasterService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;

public class UserControllerUtil {
	
	public static Request newKYCRequest(Request kycReq, UserMaster current_user, RequestService requestService,
			UserMasterService userMasterService) {
		kycReq.setIsEdited(Boolean.FALSE);
		kycReq.setIsRead(Boolean.FALSE);
		kycReq.setCustomerMasterId(current_user.getCustomerMasterId());// Entity(Karza Clients)Id
		kycReq.setUserMasterId(current_user.getId());// ClientId(Associated with Karza Clients)-logged into TotalKYC
		kycReq = requestService.save(kycReq);
		if (current_user.getMaxRequestLimit() >= 0) {
            current_user.setRemainingLimit(current_user.getRemainingLimit() + 1);
            userMasterService.update(current_user);
        }
		return kycReq;
	}

	public static Request existingKYCRequest(Request kycReq, Request existingRequest, UserMaster current_user,
			RequestService requestService, UserMasterService userMasterService) {
			kycReq.setIsEdited(Boolean.FALSE);
			kycReq.setIsRead(Boolean.FALSE);
			kycReq.setCustomerMasterId(existingRequest.getCustomerMasterId());
			kycReq.setUserMasterId(existingRequest.getUserMasterId());
			kycReq.setId(existingRequest.getId());
			requestService.update(kycReq);
			return kycReq;
	}

	public static Request existingKYCRequestVer1(Request kycReq, Request existingRequest, UserMaster current_user,
			RequestService requestService, UserMasterService userMasterService) {
		// Existing KYC-Request : With Status Submitted [Had been correctly
		// processed]
		Request parentRequest = new Request();
		if (StringUtils.equals(existingRequest.getRequest_status().toString(), "submitted")) {
			kycReq.setIsEdited(Boolean.FALSE);
			kycReq.setIsRead(Boolean.FALSE);
			kycReq.setCustomerMasterId(current_user.getCustomerMasterId());
			kycReq.setUserMasterId(current_user.getId());
			// Assigning the Existing RequestID with a new request id as its Parent
			kycReq.setParentRequestId(existingRequest.getId());
			kycReq = requestService.save(kycReq);
			existingRequest.setIsEdited(true);
			requestService.update(existingRequest);// May be club this Update
													// with above Insert itself.
		} else {
			{
			// Existing KYC-Request : [Couldn't process last time]
			if (existingRequest.getParentRequestId() == null) {
				// This request has not been edited Yet
				kycReq.setIsEdited(Boolean.FALSE);
				kycReq.setIsRead(Boolean.FALSE);
				kycReq.setCustomerMasterId(current_user.getCustomerMasterId());
				kycReq.setUserMasterId(current_user.getId());
				kycReq.setId(existingRequest.getId());
				requestService.update(kycReq);
			} else {
				// This request was previously edited
				Long parentRequestId = existingRequest.getParentRequestId();
				kycReq.setIsEdited(Boolean.FALSE);
				kycReq.setIsRead(Boolean.FALSE);
				kycReq.setCustomerMasterId(current_user.getCustomerMasterId());
				kycReq.setUserMasterId(current_user.getId());
				kycReq.setId(existingRequest.getId());
				kycReq.setParentRequestId(parentRequestId.longValue());
				requestService.update(kycReq);

				 parentRequest = requestService.get(parentRequestId);
				parentRequest.setIsEdited(Boolean.TRUE);
				 requestService.update(parentRequest);
				 parentRequest= requestService.get(parentRequestId);
			 }
			}
		}
		if (current_user.getRemainingLimit() < current_user.getMaxRequestLimit())// Confirm
		// the
		// Logic
		{
			current_user.setRemainingLimit(current_user.getRemainingLimit() + 1);
			userMasterService.update(current_user);
		}
			return kycReq;
	}
	
	public static int checkAndInsertKYCRequestForPAN(JSONObject kycReqFormJSON, int no_of_documents,
			PanCardService panCardService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger,
			ItrService itrService) throws JSONException, IOException{
			int apiCount = 0;
		String check_pan = ((JSONObject) kycReqFormJSON.get("pan")).get("check").toString();
		if (Boolean.parseBoolean(check_pan)) {
			no_of_documents++;
			String pan_document = ((JSONObject) kycReqFormJSON.get("pan")).get("unique_no").toString();
			pan_document = pan_document.toUpperCase();
			String entity_type = kycReqFormJSON.get("entity_type").toString();
			try {
				PanCard panCard = null;
				PanCard existingPanCard = panCardService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("pan").toString(), "")) {
					panCard = new PanCard(pan_document, entity_type, customerEntity.getId(), "pending",apiCount);
				} else {
					panCard = new PanCard(pan_document, entity_type, customerEntity.getId(), "invalid", "Invalid PAN",apiCount);
				}
				if (existingPanCard == null) {
					panCardService.save(panCard);
				} else {
					panCard.setId(existingPanCard.getId());
					panCardService.update(panCard);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				System.out.println("JSONException from PAN ==>"+e.getMessage().toString());
				//logger.info(e.getMessage());
			}
			catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.println("JSONException from PAN ==>"+e.getMessage().toString());
				//logger.info(e.getMessage());
			}

			PanCard panCard = panCardService.getByCustomerId(customerEntity.getId());
			if (panCard != null) {
				String check_itr = ((JSONObject) kycReqFormJSON.get("itr")).get("check").toString();
				if (Boolean.parseBoolean(check_itr)) {
					JSONObject errorList = new JSONObject(selfValidatorMap.getString("ITR"));
					no_of_documents++;
					JSONArray itr_fields = ((JSONObject) kycReqFormJSON.get("itr")).getJSONArray("itr_fields");
					List<Itr> existingItrs = itrService.getByPanId(panCard.getId());
					if (existingItrs != null) {
						for (int k = 0; k < existingItrs.size(); k++) {
							Itr temItr = existingItrs.get(k);
							itrService.delete(temItr);
						}
					}
					for (int j = 0; j < itr_fields.length(); j++) {
						String acknowledgmentNumber = (itr_fields.getJSONObject(j)).get("acknowledgement_number")
								.toString();
						String dateObj = "";
						Date dateOfFilling;
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						try {
							dateObj = (itr_fields.getJSONObject(j).get("date_of_filling").toString());
							String day = dateObj.split("/")[0];
							String month = dateObj.split("/")[1];
							String year = dateObj.split("/")[2];
							dateOfFilling = formatter.parse(year + "-" + month + "-" + day);
						} catch (ParseException e) {
							dateOfFilling = null;
							e.printStackTrace();
							System.out.println("Date Parse Exception from the UserControllerUtil class"+e.getMessage().toString());
						}
						String assessmentYear = (itr_fields.getJSONObject(j)).get("assesment_year").toString();
						if (acknowledgmentNumber != "" && assessmentYear != "") {
							Itr itrObj = null;
							try {
								if (StringUtils.equals(errorList.get(acknowledgmentNumber).toString(), "")) {
									if (StringUtils.equals(selfValidatorMap.getString("pan").toString(), "")) {
										itrObj = new Itr(acknowledgmentNumber, assessmentYear, panCard.getId(),
												dateOfFilling, customerEntity.getId(), "pending");
									} else {
										itrObj = new Itr(acknowledgmentNumber, assessmentYear, panCard.getId(),
												dateOfFilling, customerEntity.getId(), "pending");
										itrObj.setStatusAsPerSourcePan("Invalid PAN.");
									}
								} else {
									if (StringUtils.equals(selfValidatorMap.getString("pan").toString(), "")) {
										itrObj = new Itr(acknowledgmentNumber, assessmentYear, panCard.getId(),
												dateOfFilling, customerEntity.getId(), "pending");
										itrObj.setStatusAsPerSourceAck("Invalid Acknowledgement Number");
									} else {
										itrObj = new Itr(acknowledgmentNumber, assessmentYear, panCard.getId(),
												dateOfFilling, customerEntity.getId(), "invalid");
										itrObj.setStatusAsPerSourceAck("Invalid Acknowledgement Number");
										itrObj.setStatusAsPerSourcePan("Invalid PAN.");
									}
								}
								itrService.save(itrObj);
							} catch (JSONException e) {
								e.printStackTrace();
								System.out.println("JSONException from PAN ==>"+e.getMessage().toString());
								//logger.info(e.getMessage());
							}
						}
					}
				}
			}
		}

		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForVoterID(JSONObject kycReqFormJSON, int no_of_documents,
			VoterIDService voterIDService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		int apiCount = 0;
		String check_voterId = ((JSONObject) kycReqFormJSON.get("voting")).get("check").toString();
		if (Boolean.parseBoolean(check_voterId)) {
			no_of_documents++;
			String voter_document = ((JSONObject) kycReqFormJSON.get("voting")).get("unique_no").toString();
			voter_document = voter_document.toUpperCase();
			try {
				VoterID voterID = null;
				VoterID existingVoterID = voterIDService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("voting").toString(), "")) {
					voterID = new VoterID(voter_document, customerEntity.getId(), "pending",apiCount);
				} else {
					voterID = new VoterID(voter_document, customerEntity.getId(), "invalid", "invalid voter id number",apiCount);
				}
				if (existingVoterID == null) {
					voterIDService.save(voterID);
				} else {
					voterID.setId(existingVoterID.getId());
					voterIDService.update(voterID);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				System.out.println("JSONException from Voter ==>"+e.getMessage().toString());
			//	logger.info(e.getMessage());
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
				System.out.println("Other Exception from Voter ==>"+e.getMessage().toString());
				
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForAadhaar(JSONObject kycReqFormJSON, int no_of_documents,
			AadharCardService aadharCardService, CustomerEntity customerEntity, JSONObject selfValidatorMap,
			Logger logger) throws JSONException {
			
		String check_aadhar = ((JSONObject) kycReqFormJSON.get("aadhar")).get("check").toString();
		if (Boolean.parseBoolean(check_aadhar)) {
			no_of_documents++;
			String aadhar_document = ((JSONObject) kycReqFormJSON.get("aadhar")).get("unique_no").toString();
			aadhar_document = aadhar_document.toUpperCase();
			try {
				AadharCard aadharCard = null;
				AadharCard existingAadharCard = aadharCardService.getByCustomerId(customerEntity.getId());

				if (StringUtils.equals(selfValidatorMap.getString("aadhar").toString(), "")) {
					aadharCard = new AadharCard(aadhar_document, customerEntity.getId(), "pending",null,null);
				} else {
					aadharCard = new AadharCard(aadhar_document, customerEntity.getId(), "invalid",
							"invalid aadhaar number",null);
				}
				if (existingAadharCard == null) {
					aadharCardService.save(aadharCard);
				} else {
					aadharCard.setId(existingAadharCard.getId());
					aadharCardService.update(aadharCard);
				}

			} catch (JSONException e) {
				e.printStackTrace();
				System.out.println("JSONException from Aadhaar ==>"+e.getMessage().toString());
				//logger.info(e.getMessage());
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
				System.out.println("Other Exception from Aadhaar ==>"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForIEC(JSONObject kycReqFormJSON, int no_of_documents,
			IECService iecService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		Integer apiCount = 0;
		String check_iec = ((JSONObject) kycReqFormJSON.get("iec")).get("check").toString();
		if (Boolean.parseBoolean(check_iec)) {
			no_of_documents++;
			String iec_document = ((JSONObject) kycReqFormJSON.get("iec")).get("unique_no").toString();
			iec_document = iec_document.toUpperCase();
			try {
				IEC iec = null;
				IEC existingIec = iecService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("iec").toString(), "")) {
					iec = new IEC(iec_document, customerEntity.getId(), "pending",apiCount);
				} else {
					iec = new IEC(iec_document, customerEntity.getId(), "invalid", "IEC Code is Invalid",apiCount);
				}
				if (existingIec == null) {
					iecService.save(iec);
				} else {
					iec.setId(existingIec.getId());
					iecService.update(iec);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				System.out.println("JSONException from IEC ==>"+e.getMessage().toString());
				//logger.info(e.getMessage());
			}
			catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.out.println("other Exception from IEC ==>"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForDL(JSONObject kycReqFormJSON, int no_of_documents,
			DrivingLicenceMasterService dlService, CustomerEntity customerEntity, JSONObject selfValidatorMap,
			Logger logger) throws JSONException{
				int apiCount =0;
		String check_dl = ((JSONObject) kycReqFormJSON.get("dl")).get("check").toString();
		if (Boolean.parseBoolean(check_dl)) {
			no_of_documents++;
			String dl_document = ((JSONObject) kycReqFormJSON.get("dl")).get("unique_no").toString();
			dl_document = dl_document.toUpperCase();
			try {
				DrivingLicenceMaster existingDl = dlService.getByCustomerId(customerEntity.getId());
				DrivingLicenceMaster dl = new DrivingLicenceMaster(dl_document, customerEntity.getId(), "pending",apiCount);
				if (existingDl == null) {
					dl = dlService.save(dl);
				} else {
					dl.setId(existingDl.getId());
					dlService.update(dl);
				}
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
				System.out.println("JSONException from Driving Licence  ==>"+e.getMessage().toString());
			}
			
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForElectricty(JSONObject kycReqFormJSON, int no_of_documents,
			ElectricityService electricityService, CustomerEntity customerEntity, JSONObject selfValidatorMap,
			Logger logger) throws JSONException {
		int apiCount =0;
		JSONObject electricityJSONObject=(JSONObject) kycReqFormJSON.get("electricity");
		String check_electricity = electricityJSONObject.get("check").toString();
		if (Boolean.parseBoolean(check_electricity)) {
			no_of_documents++;
			String selectedElectricityBoard = electricityJSONObject.get("board").toString();
			String consumerNumber=electricityJSONObject.get("unique_no").toString();
			selectedElectricityBoard = selectedElectricityBoard.toUpperCase();
			consumerNumber=consumerNumber.toUpperCase();
			try {
				Electricity existingBill = electricityService.getByCustomerId(customerEntity.getId());
				Electricity elec = new Electricity(consumerNumber, selectedElectricityBoard, customerEntity.getId(), "pending",apiCount);
				if (existingBill == null) {
					elec = electricityService.save(elec);
				} else {
					elec.setId(existingBill.getId());
					electricityService.update(elec);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor Electricity From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForTelephone(JSONObject kycReqFormJSON, int no_of_documents,
			TelephoneService telephoneService, CustomerEntity customerEntity, JSONObject selfValidatorMap,
			Logger logger) throws JSONException{
		int apiCount = 0;
		String check_telephone = ((JSONObject) kycReqFormJSON.get("telephone")).get("check").toString();
		if(Boolean.parseBoolean(check_telephone)){
			no_of_documents++;
			String telephone_document = ((JSONObject)kycReqFormJSON.get("telephone")).get("unique_no").toString();
			telephone_document = telephone_document.toUpperCase();
			try{
				Telephone existingTelephone = telephoneService.getByCustomerId(customerEntity.getId());
				Telephone tel = new Telephone(telephone_document,customerEntity.getId(),"pending",apiCount);
				if(existingTelephone == null){
					tel = telephoneService.save(tel);
				}
				else{
					tel.setId(existingTelephone.getId());
					telephoneService.update(tel);
				}
			}
			catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor Telephone From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForEPFO(JSONObject kycReqFormJSON, int no_of_documents,
			EPFORequestService epfoService, CustomerEntity customerEntity, JSONObject selfValidatorMap,
			Logger logger) throws JSONException{
		int apiCount = 0;
		String check_uan = ((JSONObject) kycReqFormJSON.get("uan")).get("check").toString();
		if(Boolean.parseBoolean(check_uan)){
			no_of_documents++;
			String uan_document = ((JSONObject)kycReqFormJSON.get("uan")).get("unique_no").toString();
			uan_document = uan_document.toUpperCase();
			try{
				EPFORequest existingEPFO = epfoService.getByCustomerID(customerEntity.getId());
				EPFORequest epf = new EPFORequest(uan_document,customerEntity.getId(),"pending",apiCount);
				if(existingEPFO == null){
					epf = epfoService.Save(epf);
				}
				else{
					epf.setId(existingEPFO.getId());
					epfoService.update(epf);
				}
			}
			catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor EPFO From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForServiceTax(JSONObject kycReqFormJSON, int no_of_documents,
			ServiceTaxService serviceTaxService, CustomerEntity customerEntity, JSONObject selfValidatorMap,
			Logger logger) throws JSONException {
		int apiCount=0;
		String check_service_tax = ((JSONObject) kycReqFormJSON.get("service_tax")).get("check").toString();
		if (Boolean.parseBoolean(check_service_tax)) {
			no_of_documents++;
			String service_tax_document = ((JSONObject) kycReqFormJSON.get("service_tax")).get("unique_no").toString();
			service_tax_document = service_tax_document.toUpperCase();
			try {
				ServiceTax serviceTax = null;
				ServiceTax existingServiceTax = serviceTaxService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("service_tax").toString(), "")) {
					serviceTax = new ServiceTax(service_tax_document, customerEntity.getId(), "pending",apiCount);
				} else {
					serviceTax = new ServiceTax(service_tax_document, customerEntity.getId(), "invalid",
							"Invalid Service Tax Assessee Code",apiCount);
				}
				if (existingServiceTax == null) {
					serviceTaxService.save(serviceTax);
				} else {
					serviceTax.setId(existingServiceTax.getId());
					serviceTaxService.update(serviceTax);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor ServiceTax From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForExcise(JSONObject kycReqFormJSON, int no_of_documents,
			ExciseService exciseService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		int apiCount = 0;
		String check_excise = ((JSONObject) kycReqFormJSON.get("excise")).get("check").toString();
		if (Boolean.parseBoolean(check_excise)) {
			no_of_documents++;
			String excise_document = ((JSONObject) kycReqFormJSON.get("excise")).get("unique_no").toString();
			excise_document = excise_document.toUpperCase();
			try {
				Excise excise = null;
				Excise existingExcise = exciseService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("excise").toString(), "")) {
					excise = new Excise(excise_document, customerEntity.getId(), "pending",apiCount);
				} else {
					excise = new Excise(excise_document, customerEntity.getId(), "invalid",
							"Invalid Excise Tax Assessee Code", "Inactive",apiCount);
				}
				if (existingExcise == null) {
					exciseService.save(excise);
				} else {
					excise.setId(existingExcise.getId());
					exciseService.update(excise);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor Excise From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForLLP(JSONObject kycReqFormJSON, int no_of_documents,
			LLPINService llpinService, FLLPINService fllpinService, CINService cinService, FCRNService fcrnService,
			CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger) throws JSONException {
		String check_llpin = ((JSONObject) kycReqFormJSON.get("llpin")).get("check").toString();
		if (Boolean.parseBoolean(check_llpin)) {
			int apiCount=0;
			no_of_documents++;
			String llpin_document = ((JSONObject) kycReqFormJSON.get("llpin")).get("unique_no").toString();
			llpin_document = llpin_document.toUpperCase();
			try {
				LLPIN llpin;
				CIN cin = null;
				LLPIN existingllpin = llpinService.getByCustomerId(customerEntity.getId());
				llpin = new LLPIN(llpin_document, customerEntity.getId(), "pending",apiCount);
				FLLPIN existingFllpin = fllpinService.getByCustomerId(customerEntity.getId());
				if (existingFllpin != null) {
					fllpinService.delete(existingFllpin);
				}
				CIN existingCin = cinService.getByCustomerId(customerEntity.getId());
				if (existingCin != null) {
					cinService.delete(existingCin);
				}
				FCRN existingFcrn = fcrnService.getByCustomerId(customerEntity.getId());
				if (existingFcrn != null) {
					fcrnService.delete(existingFcrn);
				}

				if (StringUtils.equals(selfValidatorMap.getString("llpin").toString(), "")) {
					llpin = new LLPIN(llpin_document, customerEntity.getId(), "pending",apiCount);
					if (existingllpin == null) {
						llpinService.save(llpin);
					} else {
						llpin.setId(existingllpin.getId());
						llpinService.update(llpin);
					}
				} else {
					if (StringUtils.equals(customerEntity.getEntity_type(), "Company")) {
						cin = new CIN(llpin_document, customerEntity.getId(), "invalid", "Invalid CIN/FCRN Number",apiCount);
						if (existingllpin == null) {
							cinService.save(cin);
						} else {
							llpinService.delete(existingllpin);
							cinService.save(cin);
						}
					} else {
						llpin = new LLPIN(llpin_document, customerEntity.getId(), "invalid",
								"Invalid LLPIN/FLLPIN number",apiCount);
						if (existingllpin == null) {
							llpinService.save(llpin);
						} else {
							llpin.setId(existingllpin.getId());
							llpinService.update(llpin);
						}
					}
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor LLPIN/FLLPIN From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForVAT(JSONObject kycReqFormJSON, int no_of_documents,
			VATService vatService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		String check_vat = ((JSONObject) kycReqFormJSON.get("vat")).get("check").toString();
		if (Boolean.parseBoolean(check_vat)) {
			no_of_documents++;
			String vat_document = ((JSONObject) kycReqFormJSON.get("vat")).get("unique_no").toString();
			vat_document = vat_document.toUpperCase();
			try {
				VAT vat = null;
				VAT existingVat = vatService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("vat").toString(), "")) {
					vat = new VAT(vat_document, customerEntity.getId(), "pending");

				} else {
					vat = new VAT(vat_document, customerEntity.getId(), "invalid", "VAT number is incorrect");
				}
				if (existingVat == null) {
					vat = vatService.save(vat);
				} else {
					vat.setId(existingVat.getId());
					vatService.update(vat);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor VAT From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForCST(JSONObject kycReqFormJSON, int no_of_documents,
			CSTService cstService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		String check_cst = ((JSONObject) kycReqFormJSON.get("cst")).get("check").toString();
		if (Boolean.parseBoolean(check_cst)) {
			no_of_documents++;
			String cst_document = ((JSONObject) kycReqFormJSON.get("cst")).get("unique_no").toString();
			cst_document = cst_document.toUpperCase();
			try {
				CST cst;
				CST existingCst = cstService.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("cst").toString(), "")) {
					cst = new CST(cst_document, customerEntity.getId(), "pending");
				} else {
					cst = new CST(cst_document, customerEntity.getId(), "invalid", "cst number is incorrect");
				}
				if (existingCst == null) {
					cst = cstService.save(cst);
				} else {
					cst.setId(existingCst.getId());
					cstService.update(cst);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor CST From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForProfTax(JSONObject kycReqFormJSON, int no_of_documents,
			ProfessionalTaxService professionalTaxService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		String check_pt = ((JSONObject) kycReqFormJSON.get("pt")).get("check").toString();
		if (Boolean.parseBoolean(check_pt)) {
			no_of_documents++;
			String pt_document = ((JSONObject) kycReqFormJSON.get("pt")).get("unique_no").toString();
			pt_document = pt_document.toUpperCase();
			try {
				ProfessionalTax professionalTax;
				ProfessionalTax existingProfessionalTax = professionalTaxService
						.getByCustomerId(customerEntity.getId());
				if (StringUtils.equals(selfValidatorMap.getString("pt").toString(), "")) {
					professionalTax = new ProfessionalTax(pt_document, customerEntity.getId(), "pending");
				} else {
					professionalTax = new ProfessionalTax(pt_document, customerEntity.getId(), "pending",
							"professional tax number is incorrect");
				}
				if (existingProfessionalTax == null) {
					professionalTax = professionalTaxService.save(professionalTax);
				} else {
					professionalTax.setId(existingProfessionalTax.getId());
					professionalTaxService.update(professionalTax);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor ProfessionalTax From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}

	public static int checkAndInsertKYCRequestForPassport(JSONObject kycReqFormJSON, int no_of_documents,
			PassportService passportService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger)
			throws JSONException {
		String check_passport = ((JSONObject) kycReqFormJSON.get("passport")).get("check").toString();
		if (Boolean.parseBoolean(check_passport)) {
			try {
				no_of_documents++;
				String passport_document = ((JSONObject) kycReqFormJSON.get("passport")).get("unique_no")
						.toString();
				passport_document = passport_document.toUpperCase();
				Date passport_expiry = new Date();
				try {
					String date = ((JSONObject) kycReqFormJSON.get("passport")).get("expiry_date")
							.toString();
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					passport_expiry = formatter.parse(date);
				} catch (ParseException e) {
					System.out.println(e.toString());
					e.printStackTrace();
				}
				Passport passport;
				Passport existingPassport = passportService.getByCustomerId(customerEntity.getId());
				passport = new Passport(passport_document, customerEntity.getId(), customerEntity.getCountry(),
						customerEntity.getAddress(), passport_expiry, customerEntity.getDob());
				if (existingPassport == null) {
					passportService.save(passport);
				} else {
					passport.setId(existingPassport.getId());
					passportService.update(passport);
				}
			} catch (IllegalArgumentException e) {
				//logger.info(e.getMessage());
				e.printStackTrace();
				System.out.print("Exception from checkAndInsertKYCRequestFor Passport From UserControllerUtil Class"+e.getMessage().toString());
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForLPG(JSONObject kycReqFormJSON, int no_of_documents,
			LPGService lpgService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger) throws JSONException{
		
		int api_Count = 0;
		String check_lpg = ((JSONObject)kycReqFormJSON.get("lpg")).get("check").toString();
		if(Boolean.parseBoolean(check_lpg)){
			no_of_documents++;
			String lpg_document = ((JSONObject)kycReqFormJSON.get("lpg")).get("unique_no").toString();
			lpg_document = lpg_document.toUpperCase();
			try{
				LPG existingLPG = lpgService.getByCustomerId(customerEntity.getId());
				LPG lpg = new LPG(lpg_document,customerEntity.getId(),"pending",api_Count);
				if(existingLPG == null){
					lpg = lpgService.save(lpg);
				}
				else{
					lpg.setId(existingLPG.getId());
					lpgService.update(lpg);
				}
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
				
			}
		}
		return no_of_documents;
	}
	
	public static int checkAndInsertKYCRequestForESIC(JSONObject kycReqFormJSON, int no_of_documents,
			ESICService esicService, CustomerEntity customerEntity, JSONObject selfValidatorMap, Logger logger) throws JSONException{
		int api_Count = 0;
		String check_esic = ((JSONObject) kycReqFormJSON.get("esic")).get("check").toString();
		if(Boolean.parseBoolean(check_esic)){
			no_of_documents++;
			String esic_document = ((JSONObject)kycReqFormJSON.get("esic")).get("unique_no").toString();
			esic_document = esic_document.toUpperCase();
			try {
				ESIC existingESIC = esicService.getByCustomerId(customerEntity.getId());
				ESIC esic = new ESIC(esic_document,customerEntity.getId(),"pending",api_Count);
				if(existingESIC == null){
					esic = esicService.save(esic);
				}
				else{
					esic.setId(existingESIC.getId());
					esicService.update(esic);
				}
			} catch (IllegalArgumentException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
		}
		return no_of_documents;
	}

}
