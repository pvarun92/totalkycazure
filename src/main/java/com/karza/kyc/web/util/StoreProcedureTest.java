
package com.karza.kyc.web.util;
import java.sql.CallableStatement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.model.SuperAdmin;

/*This method is to get the value for the Karza admin, in order to bill to it's customers*/
public class StoreProcedureTest {
	
	static SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
	static SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
		public static List<SuperAdmin> getMisDetails(MisReportDTO misReportDTO){
			String query = "{ call get_mis_details(?,?,?,?,?,?,?,?)}";
			ResultSet resultSet = null;
			List<SuperAdmin> admins = new ArrayList<SuperAdmin>();
			try(
					Connection connection = getconnection();
				CallableStatement callableStatement = connection.prepareCall(query)){
				callableStatement.setString(1, misReportDTO.getCustomerName());
				callableStatement.setString(2, misReportDTO.getUniqueId());
				callableStatement.setString(3, misReportDTO.getEntity_type());
				callableStatement.setString(4, misReportDTO.getBranchId().toString());
				
				if(misReportDTO.getFilter_from() != null){
			    Date newFilterDate_from =  dateFormat.parse(misReportDTO.getFilter_from().toString());
			    String newFormattedDate = dateFormat2.format(newFilterDate_from);
			    callableStatement.setString(5, newFormattedDate);
				}
				else{
					callableStatement.setString(5, null);
				}
				if(misReportDTO.getFilter_to() != null){
				Date newFilterDate_to = (Date) dateFormat.parse(misReportDTO.getFilter_to().toString());
				String newFormattedDateTo = dateFormat2.format(newFilterDate_to);
				callableStatement.setString(6,newFormattedDateTo);
				}
				else
				{
					callableStatement.setString(6, null);
				}
				callableStatement.setString(7, misReportDTO.getValidity());
				callableStatement.setString(8, misReportDTO.getReport_status());
				resultSet = callableStatement.executeQuery();
				while(resultSet.next()){
					SuperAdmin sAdmin = new SuperAdmin();
					sAdmin.setRequest_status(resultSet.getString("request_status"));
					sAdmin.setCustomer_entity_type(resultSet.getString("entity_type"));
					sAdmin.setCustomer_entity_name(resultSet.getString("entity_name"));
					sAdmin.setRequest_id(resultSet.getInt("id"));
					sAdmin.setCustomer_entity_created_date(resultSet.getDate("created_date"));
					sAdmin.setCustomer_entity_status(resultSet.getString("entity_status"));
					sAdmin.setCustomer_entity_no_of_documents(resultSet.getString("no_of_documents"));
					sAdmin.setCustomer_entity_no_of_parties(resultSet.getString("no_of_parties"));
					sAdmin.setCustomer_entity_validity(resultSet.getString("validity"));
					sAdmin.setCustomer_entity_itr_validity(resultSet.getString("itr_validity"));
					sAdmin.setCustomer_entity_branch_id(resultSet.getLong("branch_id"));
					admins.add(sAdmin);
				}
				
			}
			catch(SQLException e){
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				if(resultSet != null)try{resultSet.close();}catch(SQLException e1){};
			}
			return admins;
		}
		
	/*public static List<SuperAdmin> getSkills(String cus_name,String unique_id,String entity_type,String branch_id,String from,String to,String validity,String report_status){
		String query = "{ call get_mis_details(?,?,?,?,?,?,?,?)}";
		ResultSet resultSet ;
		List<SuperAdmin> superAdmins = new ArrayList<SuperAdmin>();
		try(Connection connection = getconnection(); CallableStatement callableStatement = connection.prepareCall(query)){
			
			callableStatement.setString(1,cus_name);
			callableStatement.setString(2, unique_id);
			callableStatement.setString(3,entity_type);
			callableStatement.setString(4, branch_id);
			callableStatement.setString(5, from);
			callableStatement.setString(6, to);
			callableStatement.setString(7, validity);
			callableStatement.setString(8, report_status);
			callableStatement.executeQuery();
				ResultSet rs ;
				
				rs = callableStatement.executeQuery();
				while(rs.next()){
			//	customer_name = rs.getString(6);
					SuperAdmin admin = new SuperAdmin();
				String request_id = rs.getString(1);
//				System.out.println("Customer Name ->  "+customer_name);
				System.out.println("Request Id  -> "+request_id);
				admin.setCustomer_entity_request_id(rs.getString("id"));
				superAdmins.add(admin);
				System.out.println("Admin "+admin.getCustomer_entity_request_id());
				}
				return superAdmins;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	
		return superAdmins;
	}
*/
	
	
	
		public static Connection getconnection () throws SQLException{
			
			Connection conn = null;
			// This is the Connection String in order to Connect with the Azure App Storage Account
			//String url = "jdbc:mysql://karzadbqa.centralindia.cloudapp.azure.com";
			String url = "jdbc:mysql://localhost:3306/instakyc_db";
			//String url = "jdbc:mysql://localhost:3306/instakyc_db?useUnicode=yes&characterEncoding=UTF-8";
		//	String url = "jdbc:mysql://52.66.148.51:3306/instakyc_db?useUnicode=yes&characterEncoding=UTF-8";
		//	String userName = "root";
		//	String password = "root";
			String userName = "admin";
	    	String password = "Karza@1234567";
			conn = DriverManager.getConnection(url,userName,password);
			return conn;
		}
		
		public static void main(String[] args) {
		//	MisReportDTO misReportDTO=new MisReportDTO("State Bank of India","1001000001","Individual","1","2017-01-01","2017-02-01","valid","submitted");
		//	getMisDetails(misReportDTO);
		//	getSkills("State Bank of India","1001000001","Individual","1","2017-01-01","2017-02-01","valid","submitted");


		}
	

	
}
