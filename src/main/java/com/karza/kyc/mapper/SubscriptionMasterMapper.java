package com.karza.kyc.mapper;

import com.karza.kyc.dto.SubscriptionMasterDTO;
import com.karza.kyc.model.SubscriptionMaster;

/**
 * Created by Fallon on 4/28/2016.
 */
public class SubscriptionMasterMapper {

    public SubscriptionMaster createSubscriptionMasterForDTO(SubscriptionMasterDTO subscriptionMasterDTO) {
        SubscriptionMaster subscriptionMaster = new SubscriptionMaster();
        subscriptionMaster.setId(subscriptionMasterDTO.getId());
        subscriptionMaster.setApplication(subscriptionMasterDTO.getApplication());
        subscriptionMaster.setClientId(subscriptionMasterDTO.getClientId());
        subscriptionMaster.setPlan(subscriptionMasterDTO.getPlan());
        subscriptionMaster.setOneTimeFees(subscriptionMasterDTO.getOneTimeFees());
        subscriptionMaster.setPerEntityRate(subscriptionMasterDTO.getPerEntityRate());
        subscriptionMaster.setPerUserIdFees(subscriptionMasterDTO.getPerUserIdFees());
        subscriptionMaster.setContractDate(subscriptionMasterDTO.getContractDate());
        subscriptionMaster.setBillingPeriodicity(subscriptionMasterDTO.getBillingPeriodicity());
        subscriptionMaster.setBillingStartDate(subscriptionMasterDTO.getBillingStartDate());
        subscriptionMaster.setBillingEndDate(subscriptionMasterDTO.getBillingEndDate());
        subscriptionMaster.setValidFromDate(subscriptionMasterDTO.getValidFromDate());
        subscriptionMaster.setValidTillDate(subscriptionMasterDTO.getValidFromDate());
        subscriptionMaster.setCustomerMasterId(subscriptionMasterDTO.getCustomerMasterId());
        subscriptionMaster.setCreatedAt(subscriptionMasterDTO.getCreatedAt());
        subscriptionMaster.setUpdatedAt(subscriptionMasterDTO.getUpdatedAt());

        return subscriptionMaster;
    }

    public SubscriptionMasterDTO createSubscriptionMasterDTOFromModel(SubscriptionMaster subscriptionMaster) {
        SubscriptionMasterDTO subscriptionMasterDTO = new SubscriptionMasterDTO();
        subscriptionMasterDTO.setId(subscriptionMaster.getId());
        subscriptionMasterDTO.setApplication(subscriptionMaster.getApplication());
        subscriptionMasterDTO.setClientId(subscriptionMaster.getClientId());
        subscriptionMasterDTO.setPlan(subscriptionMaster.getPlan());
        subscriptionMasterDTO.setOneTimeFees(subscriptionMaster.getOneTimeFees());
        subscriptionMasterDTO.setPerUserIdFees(subscriptionMaster.getPerUserIdFees());
        subscriptionMasterDTO.setPerEntityRate(subscriptionMaster.getPerEntityRate());
        subscriptionMasterDTO.setContractDate(subscriptionMaster.getContractDate());
        subscriptionMasterDTO.setBillingPeriodicity(subscriptionMaster.getBillingPeriodicity());
        subscriptionMasterDTO.setBillingStartDate(subscriptionMaster.getBillingStartDate());
        subscriptionMasterDTO.setBillingEndDate(subscriptionMaster.getBillingEndDate());
        subscriptionMasterDTO.setValidFromDate(subscriptionMaster.getValidFromDate());
        subscriptionMasterDTO.setValidTillDate(subscriptionMaster.getValidFromDate());
        subscriptionMasterDTO.setCustomerMasterId(subscriptionMaster.getCustomerMasterId());
        subscriptionMasterDTO.setCreatedAt(subscriptionMaster.getCreatedAt());
        subscriptionMasterDTO.setUpdatedAt(subscriptionMaster.getUpdatedAt());

        return subscriptionMasterDTO;

    }
}
