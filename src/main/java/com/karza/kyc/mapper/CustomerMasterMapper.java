package com.karza.kyc.mapper;

import com.karza.kyc.dto.CustomerMasterDTO;
import com.karza.kyc.model.CustomerMaster;

/**
 * Created by Fallon on 4/28/2016.
 */
public class CustomerMasterMapper {

    public CustomerMaster createCustomerMasterForDTO(CustomerMasterDTO customerMasterDTO) {
        CustomerMaster customerMaster = new CustomerMaster();
        customerMaster.setId(customerMasterDTO.getId());
        customerMaster.setCreatedAt(customerMasterDTO.getCreatedAt());
        customerMaster.setUpdatedAt(customerMasterDTO.getUpdatedAt());
        customerMaster.setCustomerName(customerMasterDTO.getCustomerName());

        customerMaster.setCustomerType(customerMasterDTO.getCustomerType());
        customerMaster.setAddress(customerMasterDTO.getAddress());
        customerMaster.setContactPerson(customerMasterDTO.getContactPerson());
        customerMaster.setDesignation(customerMasterDTO.getDesignation());
        customerMaster.setContactNumber(customerMasterDTO.getContactNumber());
        customerMaster.setEmail(customerMasterDTO.getEmail());
        customerMaster.setPanNumber(customerMasterDTO.getPanNumber());
        customerMaster.setTanNumber(customerMasterDTO.getTanNumber());
        customerMaster.setServiceTaxNumber(customerMasterDTO.getServiceTaxNumber());
        customerMaster.setKarzaProducts(customerMasterDTO.getKarzaProducts());
        customerMaster.setCreditPeriod(customerMasterDTO.getCreditPeriod());
        customerMaster.setAccountNumber(customerMasterDTO.getAccountNumber());
        customerMaster.setAddress1(customerMasterDTO.getAddress1());
        customerMaster.setState(customerMasterDTO.getState());
        customerMaster.setPinCode(customerMasterDTO.getPinCode());

        return customerMaster;
    }

    public CustomerMasterDTO createCustomerMasterDTOFromModel(CustomerMaster customerMaster) {
        CustomerMasterDTO customerMasterDTO = new CustomerMasterDTO();
        customerMasterDTO.setId(customerMaster.getId());
        customerMasterDTO.setCreatedAt(customerMaster.getCreatedAt());
        customerMasterDTO.setUpdatedAt(customerMaster.getUpdatedAt());

        customerMasterDTO.setCustomerName(customerMaster.getCustomerName());
        customerMasterDTO.setCustomerType(customerMaster.getCustomerType());
        customerMasterDTO.setAddress(customerMaster.getAddress());
        customerMasterDTO.setContactPerson(customerMaster.getContactPerson());
        customerMasterDTO.setDesignation(customerMaster.getDesignation());
        customerMasterDTO.setContactNumber(customerMaster.getContactNumber());
        customerMasterDTO.setEmail(customerMaster.getEmail());
        customerMasterDTO.setPanNumber(customerMaster.getPanNumber());
        customerMasterDTO.setTanNumber(customerMaster.getTanNumber());
        customerMasterDTO.setServiceTaxNumber(customerMaster.getServiceTaxNumber());
        customerMasterDTO.setKarzaProducts(customerMaster.getKarzaProducts());
        customerMasterDTO.setCreditPeriod(customerMaster.getCreditPeriod());
        customerMasterDTO.setAccountNumber(customerMaster.getAccountNumber());
        customerMasterDTO.setAddress1(customerMaster.getAddress1());
        customerMasterDTO.setState(customerMaster.getState());
        customerMasterDTO.setPinCode(customerMaster.getPinCode());
        return customerMasterDTO;

    }
}
