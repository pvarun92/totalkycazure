package com.karza.kyc.mapper;

import com.karza.kyc.dto.LocationMasterDTO;
import com.karza.kyc.model.LocationMaster;

/**
 * Created by Fallon on 4/28/2016.
 */
public class LocationMasterMapper {
    public LocationMaster createLocationMasterForDTO(LocationMasterDTO locationMasterDTO) {
        LocationMaster locationMaster = new LocationMaster();
        locationMaster.setId(locationMasterDTO.getId());
        locationMaster.setCreatedAt(locationMasterDTO.getCreatedAt());
        locationMaster.setUpdatedAt(locationMasterDTO.getUpdatedAt());
        locationMaster.setLocationOrZone(locationMasterDTO.getLocationOrZone());
        locationMaster.setLinkedBranches(locationMasterDTO.getLinkedBranches());
        locationMaster.setCustomerMasterId(locationMasterDTO.getCustomerMasterId());
        locationMaster.setParentLocation(locationMasterDTO.getParentLocation());
        return locationMaster;
    }

    public LocationMasterDTO createLocationMasterDTOFromModel(LocationMaster locationMaster) {
        LocationMasterDTO locationMasterDTO = new LocationMasterDTO();
        locationMasterDTO.setId(locationMaster.getId());
        locationMasterDTO.setCreatedAt(locationMaster.getCreatedAt());
        locationMasterDTO.setUpdatedAt(locationMaster.getUpdatedAt());
        locationMasterDTO.setLocationOrZone(locationMaster.getLocationOrZone());
        locationMasterDTO.setLinkedBranches(locationMaster.getLinkedBranches());
        locationMasterDTO.setCustomerMasterId(locationMaster.getCustomerMasterId());
        locationMasterDTO.setParentLocation(locationMaster.getParentLocation());
        return locationMasterDTO;
    }
}
