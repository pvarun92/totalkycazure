package com.karza.kyc.mapper;

import com.karza.kyc.dto.BranchMasterDTO;
import com.karza.kyc.model.BranchMaster;

/**
 * Created by Fallon on 4/28/2016.
 */
public class BranchMasterMapper {
    public BranchMaster createBranchMasterForDTO(BranchMasterDTO branchMasterDTO) {
        BranchMaster branchMaster = new BranchMaster();
        branchMaster.setId(branchMasterDTO.getId());

        branchMaster.setCreatedAt(branchMasterDTO.getCreatedAt());
        branchMaster.setUpdatedAt(branchMasterDTO.getUpdatedAt());
        branchMaster.setBranchName(branchMasterDTO.getBranchName());

        branchMaster.setAddress(branchMasterDTO.getAddress());
        branchMaster.setCity(branchMasterDTO.getCity());
        branchMaster.setState(branchMasterDTO.getState());
        branchMaster.setPincode(branchMasterDTO.getPincode());
        branchMaster.setCountry(branchMasterDTO.getCountry());
        branchMaster.setIfscCode(branchMasterDTO.getIfscCode());
        branchMaster.setCreatedBy(branchMasterDTO.getCreatedBy());
        branchMaster.setModifiedBy(branchMasterDTO.getModifiedBy());
        branchMaster.setLocation(branchMasterDTO.getLocation());
        branchMaster.setCustomerMasterId(branchMasterDTO.getCustomerMasterId());
        return branchMaster;
    }

    public BranchMasterDTO createBranchMasterDTOFromModel(BranchMaster branchMaster) {
        BranchMasterDTO branchMasterDTO = new BranchMasterDTO();
        branchMasterDTO.setId(branchMaster.getId());
        branchMasterDTO.setCreatedAt(branchMaster.getCreatedAt());
        branchMasterDTO.setUpdatedAt(branchMaster.getUpdatedAt());
        branchMasterDTO.setBranchName(branchMaster.getBranchName());
        branchMasterDTO.setAddress(branchMaster.getAddress());
        branchMasterDTO.setCity(branchMaster.getCity());
        branchMasterDTO.setState(branchMaster.getState());
        branchMasterDTO.setPincode(branchMaster.getPincode());
        branchMasterDTO.setCountry(branchMaster.getCountry());
        branchMasterDTO.setIfscCode(branchMaster.getIfscCode());
        branchMasterDTO.setCreatedBy(branchMaster.getCreatedBy());
        branchMasterDTO.setModifiedBy(branchMaster.getModifiedBy());
        branchMasterDTO.setLocation(branchMaster.getLocation());
        branchMasterDTO.setCustomerMasterId(branchMaster.getCustomerMasterId());

        return branchMasterDTO;
    }
}
