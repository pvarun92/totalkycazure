package com.karza.kyc.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Varun_Prakash on 07-12-2016.
 */
public class ImageSize {

    public static void main(String[] args) throws IOException {
        BufferedImage img = null;
        File f = null;

        //read image
        try {
            f = new File("C:\\Users\\Varun_Prakash\\Pictures\\Screenshots\\a.jpg");
            img = ImageIO.read(f);
        } catch (IOException e) {
            System.out.println(e);
        }

        //get image width and height
        int width = img.getWidth();
        int height = img.getHeight();

        int p = img.getRGB(0, 0);

        //get alpha
        int a = (p >> 24) & 0xff;

        //get red
        int r = (p >> 16) & 0xff;

        //get green
        int g = (p >> 8) & 0xff;

        //get blue
        int a1 = p & 0xff;

        System.out.println("Width  " + width);
        System.out.println("Height " + height);
        System.out.println("Image " + p);
        System.out.println("Alpha Value " + a);
        System.out.println("Red Valud " + r);
        System.out.println("Green " + g);
        System.out.println("Blue " + a1);


        //some code goes here...
    }
}
