package com.karza.kyc.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.Telephone;
import com.karza.kyc.service.TelephoneService;

public class TelephoneHelper {

	@Autowired
	TelephoneService telephoneService;
	
	public static Telephone saveTelephoneDetails(Telephone telObj,JSONObject telResponse,
			Long customerId,TelephoneService telephoneService) throws JSONException{
		
		Telephone telephoneResponseObj = new Telephone(telObj.getDocument(),customerId,(JSONObject)KYCUtil.checkKeyAndGetValueFromJSON(telResponse,"telephone"));
		telephoneResponseObj.setId(telObj.getId());
		telephoneService.update(telephoneResponseObj);
		return telephoneResponseObj;
		
	}
}
