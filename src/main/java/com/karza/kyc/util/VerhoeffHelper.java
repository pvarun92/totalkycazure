package com.karza.kyc.util;

/**
 * @author Colm Rice
 * @see <a href="http://en.wikipedia.org/wiki/Verhoeff_algorithm">More Info</a>
 * @see <a href="http://en.wikipedia.org/wiki/Dihedral_group">Dihedral Group</a>
 * @see <a href="http://mathworld.wolfram.com/DihedralGroupD5.html">Dihedral Group Order 10</a>
 */
public class VerhoeffHelper {


    // The multiplication table
    static int[][] d = new int[][]
            {
                    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                    {1, 2, 3, 4, 0, 6, 7, 8, 9, 5},
                    {2, 3, 4, 0, 1, 7, 8, 9, 5, 6},
                    {3, 4, 0, 1, 2, 8, 9, 5, 6, 7},
                    {4, 0, 1, 2, 3, 9, 5, 6, 7, 8},
                    {5, 9, 8, 7, 6, 0, 4, 3, 2, 1},
                    {6, 5, 9, 8, 7, 1, 0, 4, 3, 2},
                    {7, 6, 5, 9, 8, 2, 1, 0, 4, 3},
                    {8, 7, 6, 5, 9, 3, 2, 1, 0, 4},
                    {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
            };

    // The permutation table
    static int[][] p = new int[][]
            {
                    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                    {1, 5, 7, 6, 2, 8, 3, 0, 9, 4},
                    {5, 8, 0, 3, 7, 9, 6, 1, 4, 2},
                    {8, 9, 1, 6, 0, 4, 3, 5, 2, 7},
                    {9, 4, 5, 3, 1, 2, 6, 8, 7, 0},
                    {4, 2, 8, 6, 5, 7, 3, 9, 0, 1},
                    {2, 7, 9, 3, 8, 0, 6, 4, 1, 5},
                    {7, 0, 4, 6, 9, 1, 3, 2, 5, 8}
            };

    // The inverse table
    static int[] inv = {0, 4, 3, 2, 1, 5, 6, 7, 8, 9};


    /* 
     * For a given number generates a Verhoeff digit
     * 
     */
    public static String generateVerhoeff(String num) {

        int c = 0;
        int[] myArray = stringToReversedIntArray(num);

        for (int i = 0; i < myArray.length; i++) {
            c = d[c][p[((i + 1) % 8)][myArray[i]]];
        }

        return Integer.toString(inv[c]);
    }


    /*
     * Validates that an entered number is Verhoeff compliant.
     * NB: Make sure the check digit is the last one.
     */
    public static boolean validateVerhoeff(String num) {

        int c = 0;
        int[] myArray = stringToReversedIntArray(num);

        for (int i = 0; i < myArray.length; i++) {
            c = d[c][p[(i % 8)][myArray[i]]];
        }

        return (c == 0);
    }


    /*
     * Converts a string to a reversed integer array.
     */
    private static int[] stringToReversedIntArray(String num) {

        int[] myArray = new int[num.length()];

        for (int i = 0; i < num.length(); i++) {
            myArray[i] = Integer.parseInt(num.substring(i, i + 1));
        }

        myArray = reverse(myArray);

        return myArray;

    }

    /*
     * Reverses an int array
     */
    private static int[] reverse(int[] myArray) {
        int[] reversed = new int[myArray.length];

        for (int i = 0; i < myArray.length; i++) {
            reversed[i] = myArray[myArray.length - (i + 1)];
        }

        return reversed;
    }

    public static void main(String args[]) {

        System.out.println("Generated Verheoff Checksum::::" + generateVerhoeff("75872"));
        System.out.println("Verheoff Checksum validation is " + validateVerhoeff("758722"));

        System.out.println("Generated Verheoff Checksum::::" + generateVerhoeff("12345"));
        System.out.println("Verheoff Checksum validation is " + validateVerhoeff("123451"));

        System.out.println("Generated Verheoff Checksum::::" + generateVerhoeff("142857"));
        System.out.println("Verheoff Checksum validation is " + validateVerhoeff("1428570"));

        System.out.println("Generated Verheoff Checksum::::" + generateVerhoeff("123456789012"));
        System.out.println("Verheoff Checksum validation is " + validateVerhoeff("1234567890120"));

        System.out.println("Generated Verheoff Checksum::::" + generateVerhoeff("8473643095483728456789"));
        System.out.println("Verheoff Checksum validation is " + validateVerhoeff("84736430954837284567892"));

    }

    public Boolean isAadharCheckSumValidation(String document) {
        int dctLen = document.length();
        String generatorString = document.substring(0, dctLen - 1);
        String calculatedCheckSum = generateVerhoeff(generatorString);
        String expectedCheckSum = document.substring(dctLen - 1, dctLen);
        return calculatedCheckSum.equals(expectedCheckSum);
    }
    
    public Boolean isUANCheckSumValidation(String document) {
        int dctLen = document.length();
        String generatorString = document.substring(0, dctLen - 1);
        String calculatedCheckSum = generateVerhoeff(generatorString);
        String expectedCheckSum = document.substring(dctLen - 1, dctLen);
        return calculatedCheckSum.equals(expectedCheckSum);
    } 
    
}