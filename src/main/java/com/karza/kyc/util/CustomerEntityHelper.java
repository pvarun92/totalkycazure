package com.karza.kyc.util;

import com.karza.kyc.model.CustomerEntity;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerEntityHelper {

    public static Boolean showDetailedDocumentData(String status_as_per_source) {
        return (StringUtils.containsIgnoreCase(status_as_per_source, "Active")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Pending")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Cancelled")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Rejected")
                || StringUtils.containsIgnoreCase(status_as_per_source, "INACTIVE")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Converted to LLP")
                || StringUtils.containsIgnoreCase(status_as_per_source, "valid")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Dormant")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Under liquidation")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Amalgamated")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Liquidated")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Dissolved")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Not available for e-filing")
                || StringUtils.containsIgnoreCase(status_as_per_source, "To be migrated")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Captured")
                || StringUtils.containsIgnoreCase(status_as_per_source, "NORMAL")
                || StringUtils.containsIgnoreCase(status_as_per_source, "AMENDMENT")
                || StringUtils.containsIgnoreCase(status_as_per_source, "BLACK LISTED")
                || StringUtils.containsIgnoreCase(status_as_per_source, "SUSPENDED")
                || StringUtils.containsIgnoreCase(status_as_per_source, "CANCELLED")
                || StringUtils.containsIgnoreCase(status_as_per_source, "CLEAR FROM BLACK LIST")
                || StringUtils.containsIgnoreCase(status_as_per_source, "REVOKE SUSPENSION")
                || StringUtils.containsIgnoreCase(status_as_per_source, "REVOKE CANCELLATION")
                || StringUtils.containsIgnoreCase(status_as_per_source, "AMENDMENT")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Defunct")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Strike Off")
                || StringUtils.containsIgnoreCase(status_as_per_source, "Under Process of Striking Off"))
                && !StringUtils.containsIgnoreCase(status_as_per_source, "enter")
                && (!StringUtils.containsIgnoreCase(status_as_per_source, "No Record Found"))
                && (!StringUtils.containsIgnoreCase(status_as_per_source, "Multiple records found"))
                && (!StringUtils.containsIgnoreCase(status_as_per_source, "Invalid Voter ID number"))
                && (!StringUtils.containsIgnoreCase(status_as_per_source, "IEC Code is Invalid"))
                && !StringUtils.containsIgnoreCase(status_as_per_source, "Invalid")
                && !(StringUtils.containsIgnoreCase(status_as_per_source, "No Record found with the given DL number"));
    }

    public CustomerEntity getCustomerEntity(JSONObject entityData, Long req_id, int no_of_parties) throws JSONException {
        String entity_type = entityData.get("entity_type").toString();
        String relation = entityData.get("relation").toString();
        Long branchId = Long.parseLong(entityData.get("branch").toString());
        String first_name = ((JSONObject) entityData.get("full_name")).get("first_name").toString();
        String middle_name = ((JSONObject) entityData.get("full_name")).get("middle_name").toString();
        String last_name = ((JSONObject) entityData.get("full_name")).get("last_name").toString();
        String address = ((JSONObject) entityData.get("address")).get("address_line1").toString() + " ";
        if (((JSONObject) entityData.get("address")).has("address_line2")) {
            address = address + ((JSONObject) entityData.get("address")).get("address_line2").toString();
        }
        String state = ((JSONObject) entityData.get("address")).get("state").toString();
        String pincode = ((JSONObject) entityData.get("address")).get("pincode").toString();
        String dob = "";
        if (entityData.has("dob")) {
            dob = entityData.get("dob").toString();
        }
        String email = entityData.get("email").toString();
        String contactNumber = entityData.get("contact_number").toString();
        String gender = entityData.get("gender").toString();
        String father_husband_first_name = ((JSONObject) entityData.get("father_husband_name")).get("first_name").toString();
        String father_husband_middle_name = ((JSONObject) entityData.get("father_husband_name")).get("middle_name").toString();
        String father_husband_last_name = ((JSONObject) entityData.get("father_husband_name")).get("last_name").toString();
        String country = ((JSONObject) entityData.get("address")).get("country").toString();
        String entity_status = "";
        if (entityData.has("entity_status")) {
            entity_status = entityData.get("entity_status").toString();
        }
        if (!StringUtils.equals(entity_status, "parent")) {
            entity_status = "child";
        }

        String entity_name = "";
        if ((entityData.has("entity"))) {
            entity_name = entityData.get("entity").toString();
        }

        Date today_date = new Date();

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setEntity_type(entity_type);
        customerEntity.setRelation(relation);
        customerEntity.setBranchId(branchId);
        customerEntity.setFirst_name(first_name);
        customerEntity.setMiddle_name(middle_name);
        customerEntity.setLast_name(last_name);
        customerEntity.setAddress(address);
        customerEntity.setState(state);
        customerEntity.setPinCode(pincode);
        customerEntity.setEmail(email);
        customerEntity.setContactNumber(contactNumber);
        customerEntity.setGender(gender);
        customerEntity.setFather_husband_first_name(father_husband_first_name);
        customerEntity.setFather_husband_middle_name(father_husband_middle_name);
        customerEntity.setFather_husband_last_name(father_husband_last_name);
        customerEntity.setCountry(country);
        customerEntity.setCreated_date(today_date);
        customerEntity.setEntity_status(entity_status);
        customerEntity.setRequest_id(req_id);
        customerEntity.setEntity_name(entity_name);
        customerEntity.setNo_of_parties(no_of_parties);
        try {
            if (!dob.isEmpty()) {
                String date = dob;
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date utilDate = formatter.parse(date);
                customerEntity.setDob(utilDate);
            }
            return customerEntity;
        } catch (ParseException e) {
            System.out.println(e.toString());
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return null;
    }
}
