package com.karza.kyc.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.ESICContriDetails;
import com.karza.kyc.service.ESICContriService;
import com.karza.kyc.service.ESICService;

public class ESICHelper {

	@Autowired
	ESICService esicService;
	@Autowired
	ESICContriService contriService;
	
	public static ESIC saveESICDetails(ESIC esic, JSONObject esicResponse, Long customerID,ESICService esicService,ESICContriService contriDetails){
		
		try {
			ESIC esicSaveObject = new ESIC(esic.getDocument(),customerID,(JSONObject)KYCUtil.checkKeyAndGetValueFromJSON(esicResponse, "esic"));
			//save This Object
			esicSaveObject.setId(esic.getId());
			esicService.update(esicSaveObject);
			
			//List<JSONObject>
			JSONArray esicContributions=(JSONArray)KYCUtil.checkKeyAndGetValueFromJSON(esicResponse, "Contribution");
			if(esicContributions != null){
			for(int loop1=0;loop1<esicContributions.length();loop1++){
				JSONObject esicContribution= (JSONObject)esicContributions.get(loop1);
				ESICContriDetails esicContriDetails=new ESICContriDetails(esic.getId(), esicContribution);
				//save this object
				contriDetails.save(esicContriDetails);
			}
		}
			return esicSaveObject;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return esic;
		
	}
	
}
