package com.karza.kyc.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.collect.Lists;
import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.AadharCardService;
import com.karza.kyc.service.CINService;
import com.karza.kyc.service.CSTService;
import com.karza.kyc.service.DrivingLicenceMasterService;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.ESICService;
import com.karza.kyc.service.ElectricityService;
import com.karza.kyc.service.ExciseService;
import com.karza.kyc.service.FCRNService;
import com.karza.kyc.service.FLLPINService;
import com.karza.kyc.service.IECService;
import com.karza.kyc.service.ItrService;
import com.karza.kyc.service.LLPINService;
import com.karza.kyc.service.LPGService;
import com.karza.kyc.service.PanCardService;
import com.karza.kyc.service.PassportService;
import com.karza.kyc.service.ProfessionalTaxService;
import com.karza.kyc.service.ServiceTaxService;
import com.karza.kyc.service.TelephoneService;
import com.karza.kyc.service.VATService;
import com.karza.kyc.service.VoterIDService;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class DocumentHelper {

    public JSONObject getvalidityAndCount(CustomerEntity entity, FCRNService fcrnService, FLLPINService fllpinService, 
    		IECService iecService, DrivingLicenceMasterService dlService, CINService cinService,
    		PanCardService panCardService, ServiceTaxService serviceTaxService, ExciseService exciseService,
    		LLPINService llpinService, AadharCardService aadharCardService, PassportService passportService, 
    		VATService vatService, CSTService cstService, ProfessionalTaxService professionalTaxService, 
    		ItrService itrService, VoterIDService voterIDService,ElectricityService electricityService,
    		TelephoneService telephoneService,EPFORequestService epfoService,LPGService lpgService,ESICService esicService) throws JSONException {
        PanCard panCard = panCardService.getByCustomerId(entity.getId());
        AadharCard aadharCard = aadharCardService.getByCustomerId(entity.getId());
        ServiceTax serviceTax = serviceTaxService.getByCustomerId(entity.getId());
        Excise excise = exciseService.getByCustomerId(entity.getId());
        IEC iec = iecService.getByCustomerId(entity.getId());
        DrivingLicenceMaster dl = dlService.getByCustomerId(entity.getId());
        LLPIN llpin = llpinService.getByCustomerId(entity.getId());
        CIN cin = cinService.getByCustomerId(entity.getId());
        FCRN fcrn = fcrnService.getByCustomerId(entity.getId());
        FLLPIN fllpin = fllpinService.getByCustomerId(entity.getId());
        Passport passport = passportService.getByCustomerId(entity.getId());
        VAT vat = vatService.getByCustomerId(entity.getId());
        CST cst = cstService.getByCustomerId(entity.getId());
        ProfessionalTax professionalTax = professionalTaxService.getByCustomerId(entity.getId());
        List<Itr> itrs = itrService.getByCustomerId(entity.getId());
        VoterID voterID = voterIDService.getByCustomerId(entity.getId());
        Electricity electricity = electricityService.getByCustomerId(entity.getId());
        Telephone telephone = telephoneService.getByCustomerId(entity.getId());
        EPFORequest epfoReq = epfoService.getByCustomerID(entity.getId());
        LPG lpg = lpgService.getByCustomerId(entity.getId());
        ESIC esic = esicService.getByCustomerId(entity.getId());
        
        int no_of_documents = 0;
        String validity = "valid";
        String itrValidity = "valid";

        ArrayList<String> validStatusList = Lists.newArrayList("active", "normal", "amendment", "valid",
        		"itr-v received", "e-return has been digitally signed");
        ArrayList<String> partialValidStatusList = Lists.newArrayList("pending", "rejected",
        		"converted to llp", "dormant", "active in progress", "under liquidation",
        		"under process of striking off", "amalgamated", "not available for e-filing", 
        		"to be migrated", "captured", "dormant under section 455 of companies act, 2013",
        		"clear from black list", "revoke suspension", "revoke cancellation", 
        		"under process of strike off", "multiple records found", "pending()", 
        		"dormant under section 455");
        ArrayList<String> invalidStatusList = Lists.newArrayList("cancelled", "vat/cst tin number is incorrect", "inactive", "m v a t rc no should be number",
        		"m v a t rc no should end with 'v'or'c'or'p'", "m v a t rc no is invalid number", "invalid charectors should not be entered", 
        		"m v a t rc no should begin with 27 or 99", "service tax assessee code is incorrect", "service tax assessee code is incorrect",
        		"no records available", "cin/fcrn is invalid.", "cin/fcrn number is incorrect.", "strike off", "liquidated", "dissolved",
        		"converted to llp and dissolved", "iec code is invalid", "no record found", "black listed", "suspended", "cancelled",
        		"iec code is not proper", "iec code or applicant name is incorrect.", "iec code or applicant name is incorrect.", 
        		"iec code or applicant name is incorrect.", "no data available for The given iec", "defunct", "llpin/fllpin number is invalid",
        		"no record found with the given dl number", "invalid pan", "pan does not exist.", "itr-v not received", 
        		"no e-return has been filed.", "invalid acknowlwdgement number", "invalid pan. itr-v can not be verified based on pan.",
        		"assessment year is invalid. itr-v can not be verified based on assessment year.", "no record found", "invalid voter id number",
        		"no record found with the given DL number", "records not found", "invalid aadhaar number","No Record Found","invalid",
        		"Name or DOB Mismatch","Aadhaar Suspended","Request Expired","Invalid Aadhaar Number","address mismatch",
        		"invalid aadhaar number","name or dob mismatch","request expired","aadhaar suspended","aadhaar sub-aua couldn't process this request",
        		"invalid cin/fcrn number", "invalid llpin/fllpin number", "invalid service tax assessee code", "invalid excise tax assessee code",
                "vat number is incorrect", "cst number is incorrect", "professional tax number is incorrect", "vat/cst tin number is invalid",
                "invalid", "invalid acknowlwdgement number", "please enter valid 15 digit assessee code", "no records available for given assessee code");

        if (panCard != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(panCard.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(panCard.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(panCard.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(panCard.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (itrs != null) {
            if (itrs.size() > 0) {
                no_of_documents++;
                for (int i = 0; i < itrs.size(); i++) {
                    Itr itr = itrs.get(i);
                    if (itr != null) {
                        if (itrValidity.equals("valid") &&
                                (
                                        StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "ITR-V not received")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "ITR-V not received")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "No e-Return has been filed")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "No e-Return has been filed")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Please enter a valid Acknowledgement Number")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "Please enter a valid Acknowledgement Number")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Invalid PAN. Please retry")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Invalid PAN.")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Invalid PAN. ITR-V can not be verified based on PAN.")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "Invalid PAN. ITR-V can not be verified based on PAN.")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "Invalid PAN. Please retry")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "Invalid PAN.")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Please select the assessment Year.")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourcePan(), "Please select the assessment Year.")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Invalid Acknowledgement number")
                                                || StringUtils.containsIgnoreCase(itr.getStatusAsPerSourceAck(), "Invalid Acknowlwdgement number")
                                                || StringUtils.isBlank(itr.getStatusAsPerSourceAck())
                                                || StringUtils.isBlank(itr.getStatusAsPerSourcePan())
                                                || StringUtils.isEmpty(itr.getStatusAsPerSourceAck())
                                                || StringUtils.isEmpty(itr.getStatusAsPerSourcePan())
                                )
                                ) {
                            itrValidity = "invalid";
                        }
                    }
                }
            } else {
                itrValidity = "-";
            }
        } else {
            itrValidity = "-";
        }
        if (aadharCard != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(aadharCard.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(aadharCard.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(aadharCard.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(aadharCard.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (serviceTax != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(serviceTax.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(serviceTax.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(serviceTax.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(serviceTax.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (excise != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(excise.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(excise.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(excise.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(excise.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (iec != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(iec.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(iec.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(iec.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(iec.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (voterID != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(voterID.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(voterID.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(voterID.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(voterID.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (dl != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(dl.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(dl.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(dl.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(dl.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        //validate the electricity
        if (electricity != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(electricity.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(electricity.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(electricity.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(electricity.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        // validate the Telephone
         if(telephone != null){
        	 no_of_documents++;
        	 if(StringUtils.isNotBlank(telephone.getStatusAsPerSource())) {
        		 if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(telephone.getStatusAsPerSource().toLowerCase())) {
                     validity = "valid";
                 } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(telephone.getStatusAsPerSource().toLowerCase())) {
                     validity = "partial_valid";
                 } else if (invalidStatusList.contains(telephone.getStatusAsPerSource().toLowerCase())) {
                     validity = "invalid";
                 }
        	 }
         }
        
        if (llpin != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(llpin.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(llpin.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(llpin.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(llpin.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (cin != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(cin.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(cin.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(cin.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(cin.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (fcrn != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(fcrn.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(fcrn.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(fcrn.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(fcrn.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (fllpin != null) {
            no_of_documents++;
            if (StringUtils.isNotBlank(fllpin.getStatusAsPerSource())) {
                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(fllpin.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(fllpin.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(fllpin.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (vat != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(vat.getStatusAsPerSource())) {
                if (vat.getStatusAsPerSource().toLowerCase().contains("(")) {
                    vat.setStatusAsPerSource((vat.getStatusAsPerSource().split("\\(")[0]).toLowerCase());
                }

                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(vat.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(vat.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(vat.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        
        if(epfoReq != null){
        	no_of_documents++;
        	if(StringUtils.isNotBlank(epfoReq.getStatusAsPerSource())) {
        		if(!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid")
        				&& validStatusList.contains(epfoReq.getStatusAsPerSource().toLowerCase())){
        			
        			validity = "valid";
        		}
        		else if(!StringUtils.equalsIgnoreCase(validity,"invalid") &&
        				partialValidStatusList.contains(epfoReq.getStatusAsPerSource().toLowerCase())){
        			validity = "partial_valid";
        		}
        		else if(invalidStatusList.contains(epfoReq.getStatusAsPerSource().toLowerCase())){
        			validity = "invalid";
        		}
        	}
        }
        
        if(lpg != null){
        	no_of_documents++;
        	if(StringUtils.isNotBlank(lpg.getStatusAsPerSource())){
        		if(!StringUtils.equalsIgnoreCase(validity, "invalid")&& !StringUtils.equalsIgnoreCase(validity, "partial_valid")
        			&&	validStatusList.contains(lpg.getStatusAsPerSource().toLowerCase())){
        			
        			validity = "valid";
        		}
        		else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && 
        				partialValidStatusList.contains(lpg.getStatusAsPerSource().toLowerCase())){
        				validity = "partial_valid";
        		}
        		else if (invalidStatusList.contains(lpg.getStatusAsPerSource().toLowerCase())){
        			validity = "invalid";
        		}
        	}
        }
        
        if(esic != null){
        	no_of_documents++;
        	if(StringUtils.isNotBlank(esic.getStatusAsPerSource())){
        		if(!StringUtils.equalsIgnoreCase(validity, "invalid")&& !StringUtils.equalsIgnoreCase(validity, "partial_valid")
        			&&	validStatusList.contains(esic.getStatusAsPerSource().toLowerCase())){
        			
        			validity = "valid";
        		}
        		else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && 
        				partialValidStatusList.contains(esic.getStatusAsPerSource().toLowerCase())){
        				validity = "partial_valid";
        		}
        		else if (invalidStatusList.contains(esic.getStatusAsPerSource().toLowerCase())){
        			validity = "invalid";
        		}
        	}
        }
        
        
        if (cst != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(cst.getStatusAsPerSource())) {

                if (cst.getStatusAsPerSource().toLowerCase().contains("(")) {
                    cst.setStatusAsPerSource((cst.getStatusAsPerSource().split("\\(")[0]).toLowerCase());
                }

                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(cst.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(cst.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(cst.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (professionalTax != null) {
            no_of_documents++;

            if (StringUtils.isNotBlank(professionalTax.getStatusAsPerSource())) {
                if (professionalTax.getStatusAsPerSource().toLowerCase().contains("(")) {
                    professionalTax.setStatusAsPerSource((professionalTax.getStatusAsPerSource().split("\\(")[0]).toLowerCase());
                }

                if (!StringUtils.equalsIgnoreCase(validity, "invalid") && !StringUtils.equalsIgnoreCase(validity, "partial_valid") && validStatusList.contains(professionalTax.getStatusAsPerSource().toLowerCase())) {
                    validity = "valid";
                } else if (!StringUtils.equalsIgnoreCase(validity, "invalid") && partialValidStatusList.contains(professionalTax.getStatusAsPerSource().toLowerCase())) {
                    validity = "partial_valid";
                } else if (invalidStatusList.contains(professionalTax.getStatusAsPerSource().toLowerCase())) {
                    validity = "invalid";
                }
            }
        }
        if (passport != null) {
            no_of_documents++;
        }
        if (no_of_documents <= 0) {
            validity = "invalid";
        }
        
        JSONObject data = new JSONObject();
        data.put("no_of_documents", no_of_documents);
        data.put("validity", validity);
        data.put("itrValidity", itrValidity);
        return data;
    }

    /*
     * IMPORTANT: This method should only be called for Drafted Requests[CROSS CHECK - N NO OF TIMES]
     */
    public void removeUncheckedKYCDocsFromDraft(JSONObject request_form_data, CustomerEntity entity, FCRNService fcrnService,
    		FLLPINService fllpinService, IECService iecService, DrivingLicenceMasterService dlService, CINService cinService, 
    		PanCardService panCardService, ServiceTaxService serviceTaxService, ExciseService exciseService, LLPINService llpinService,
    		AadharCardService aadharCardService, PassportService passportService, VATService vatService, CSTService cstService, 
    		ProfessionalTaxService professionalTaxService, ItrService itrService, VoterIDService voterIDService,
    		ElectricityService electricityService,TelephoneService telephoneService,
    		EPFORequestService epfoReqService,LPGService lpgService,ESICService esicService) throws JSONException {
        String check_pan = ((JSONObject) request_form_data.get("pan")).get("check").toString();
        String check_aadhar = ((JSONObject) request_form_data.get("aadhar")).get("check").toString();
        String check_iec = ((JSONObject) request_form_data.get("iec")).get("check").toString();
        String check_dl = ((JSONObject) request_form_data.get("dl")).get("check").toString();
        String check_service_tax = ((JSONObject) request_form_data.get("service_tax")).get("check").toString();
        String check_excise = ((JSONObject) request_form_data.get("excise")).get("check").toString();
        String check_llpin = ((JSONObject) request_form_data.get("llpin")).get("check").toString();
        String check_vat = ((JSONObject) request_form_data.get("vat")).get("check").toString();
        String check_cst = ((JSONObject) request_form_data.get("cst")).get("check").toString();
        String check_pt = ((JSONObject) request_form_data.get("pt")).get("check").toString();
        String check_passport = ((JSONObject) request_form_data.get("passport")).get("check").toString();
        String check_itr = ((JSONObject) request_form_data.get("itr")).get("check").toString();
        String check_voting = ((JSONObject) request_form_data.get("voting")).get("check").toString();
        String check_telephone = ((JSONObject) request_form_data.get("telephone")).get("check").toString();
        String check_electricity = ((JSONObject) request_form_data.get("electricity")).get("check").toString();
        String check_uan = ((JSONObject) request_form_data.get("uan")).get("check").toString();
        String check_lpg = ((JSONObject) request_form_data.get("lpg")).get("check").toString();
        String check_esic = ((JSONObject) request_form_data.get("esic")).get("check").toString();
        if (!Boolean.parseBoolean(check_pan)) {
            PanCard existingPanCard = panCardService.getByCustomerId(entity.getId());
            if (existingPanCard != null) {
                panCardService.delete(existingPanCard);//Delete this entry from PAN : As In the current draft It is Unchecked
            }
        }

        if (!Boolean.parseBoolean(check_aadhar)) {
            AadharCard document = aadharCardService.getByCustomerId(entity.getId());
            if (document != null) {
                aadharCardService.delete(document);
            }
        }

        if (!Boolean.parseBoolean(check_iec)) {
            IEC document = iecService.getByCustomerId(entity.getId());
            if (document != null) {
                iecService.delete(document);
            }
        }

        if (!Boolean.parseBoolean(check_dl)) {
            DrivingLicenceMaster document = dlService.getByCustomerId(entity.getId());
            if (document != null) {
                dlService.delete(document);
            }
        }
        
        if(!Boolean.parseBoolean(check_telephone)) {
        	Telephone document = telephoneService.getByCustomerId(entity.getId());
        	if(document != null){
        		telephoneService.Delete(document);
        	}
        	
        }
        if(!Boolean.parseBoolean(check_uan)) {
        	EPFORequest document = epfoReqService.getByCustomerID(entity.getId());
        	if(document != null){
        		epfoReqService.delete(document);
        	}
        	
        }

        if (!Boolean.parseBoolean(check_service_tax)) {
            ServiceTax document = serviceTaxService.getByCustomerId(entity.getId());
            if (document != null) {
                serviceTaxService.delete(document);
            }
        }

        if (!Boolean.parseBoolean(check_excise)) {
            Excise document = exciseService.getByCustomerId(entity.getId());
            if (document != null) {
                exciseService.delete(document);
            }
        }

        if (!Boolean.parseBoolean(check_llpin)) {
            LLPIN llpin = llpinService.getByCustomerId(entity.getId());
            if (llpin != null) {
                llpinService.delete(llpin);
            }

            FLLPIN fllpin = fllpinService.getByCustomerId(entity.getId());
            if (fllpin != null) {
                fllpinService.delete(fllpin);
            }

            CIN cin = cinService.getByCustomerId(entity.getId());
            if (cin != null) {
                cinService.delete(cin);
            }

            FCRN fcrn = fcrnService.getByCustomerId(entity.getId());
            if (fcrn != null) {
                fcrnService.delete(fcrn);
            }
        }

        if (!Boolean.parseBoolean(check_vat)) {
            VAT document = vatService.getByCustomerId(entity.getId());
            if (document != null) {
                vatService.delete(document);
            }
        }

        if (!Boolean.parseBoolean(check_cst)) {
            CST document = cstService.getByCustomerId(entity.getId());
            if (document != null) {
                cstService.delete(document);
            }
        }

        if (!Boolean.parseBoolean(check_pt)) {
            ProfessionalTax document = professionalTaxService.getByCustomerId(entity.getId());
            if (document != null) {
                professionalTaxService.delete(document);
            }
        }
        if(!Boolean.parseBoolean(check_electricity)){
        	Electricity document = electricityService.getByCustomerId(entity.getId());
        	if(document != null){
        		electricityService.delete(document);//Explanation Awaited!
        	}
        	
        }

        if (!Boolean.parseBoolean(check_passport)) {
            Passport document = passportService.getByCustomerId(entity.getId());
            if (document != null) {
                passportService.delete(document);
            }
        }
        if (!Boolean.parseBoolean(check_itr)) {
            List<Itr> documents = itrService.getByCustomerId(entity.getId());
            if (documents != null) {
                if (documents.size() > 0) {
                    for (int i = 0; i < documents.size(); i++) {
                        if (documents.get(i) != null) {
                            itrService.delete(documents.get(i));
                        }
                    }
                }
            }
        }
        if (!Boolean.parseBoolean(check_voting)) {
            VoterID voterID = voterIDService.getByCustomerId(entity.getId());
            if (voterID != null) {
                voterIDService.delete(voterID);
            }
        }
        
        if(!Boolean.parseBoolean(check_lpg)){
        	LPG lpg = lpgService.getByCustomerId(entity.getId());
        	if(lpg != null){
        		lpgService.delete(lpg);
        	}
        }
        
        if(!Boolean.parseBoolean(check_esic)){
        	ESIC esic = esicService.getByCustomerId(entity.getId());
        	if(esic != null){
        		esicService.delete(esic);
       }
        }
    }
}
