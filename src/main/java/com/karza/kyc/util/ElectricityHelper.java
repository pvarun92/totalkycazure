package com.karza.kyc.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.Electricity;
import com.karza.kyc.service.ElectricityService;

public class ElectricityHelper {

	@Autowired
	ElectricityService electricityService;
	// method to save the Electricity JSON Data, that to persist into database
	public static Electricity saveElectricity(Electricity electricityReqObj, JSONObject electricityResponse,
			Long customerId, ElectricityService electricityService) throws JSONException

	{
		/*
		 * This method should only get called if we have a Valid(i.e. Non-NULL) Electricty Request Object and a 
		 * Valid JSON Response from the ELectricty API
		 */
		Electricity electricityResObj = new Electricity(electricityReqObj.getDocument(),
				electricityReqObj.getServiceProvider(), customerId,electricityResponse);
		electricityResObj.setId(electricityReqObj.getId());
		electricityService.update(electricityResObj);
		return electricityResObj;
	}
}
