package com.karza.kyc.util;

import com.karza.kyc.model.IEC;
import com.karza.kyc.model.IEC_Branch;
import com.karza.kyc.service.IECService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 7/25/16
 * Time: 3:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class IECHelper {

    @Autowired
    IECService iecService;

    public static IEC saveIEC(IEC iec, String iecId, Long customerEntityId, JSONObject iecApiResponse, IECService iecService) throws IOException,JSONException{
    	Integer apiCount = 0;
    	IEC iec1 = new IEC(iecId, customerEntityId, iecApiResponse);
        iec1.setId(iec.getId());
        iecService.update(iec1);
        List<IEC_Branch> iec_branchList = iecService.getBranchByIECID(iec.getId());
        if (iec_branchList != null) {
            if (iec_branchList.size() > 0) {
                for (int i = 0; i < iec_branchList.size(); i++) {
                    IEC_Branch iec_branch = iec_branchList.get(i);
                    iecService.deleteBranch(iec_branch);
                }
            }
        }
        if (iecApiResponse.has("branches")) {
            JSONArray branchList = ((JSONArray) (iecApiResponse.get("branches")));
            if (branchList.length() > 0) {
                for (int i = 0; i < branchList.length(); i++) {
                    JSONObject branchObj = new JSONObject(branchList.get(i));
                    IEC_Branch iec_branch = new IEC_Branch(iec.getId(), ((JSONObject) (branchList.get(i))).get("ADDRESS").toString(), ((JSONObject) (branchList.get(i))).get("Branch Serial Number").toString());
                    iecService.saveIECBranch(iec_branch);
                }
            }
        }
        return iec1;
    }
}
