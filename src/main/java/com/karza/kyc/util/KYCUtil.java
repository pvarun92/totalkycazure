package com.karza.kyc.util;

import org.json.JSONException;
import org.json.JSONObject;

public class KYCUtil {
	  public static Object checkKeyAndGetValueFromJSON(JSONObject jsonObject,String key){
		  if(jsonObject.has(key)){
	    	  try {
				return jsonObject.get(key);
			} catch (JSONException e) {
				e.printStackTrace();
			}
	      }
		  return null;
	  }
	  
}
