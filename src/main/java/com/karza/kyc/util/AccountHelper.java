package com.karza.kyc.util;

import com.karza.kyc.model.Account;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Fallon Software on 4/13/2016.
 */
public class AccountHelper {
    public Account getAccount(JSONObject accountData) throws JSONException {
        String name = accountData.get("name").toString();
        String password = "";
        if (accountData.has("password")) {
            password = accountData.get("password").toString();
        }
        String email = accountData.get("email").toString();
        String address = accountData.get("address").toString();

        Account accountUser = new Account();
        accountUser.setName(name);
        if (!password.isEmpty()) {
            accountUser.setPassword(password);
        } else {
            accountUser.setPassword("password");
        }
        accountUser.setEmail(email);
        accountUser.setAddress(address);
        return accountUser;
    }
}
