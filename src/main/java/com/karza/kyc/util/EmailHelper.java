package com.karza.kyc.util;

import com.karza.kyc.dto.EmailAttributeDTO;
import com.karza.kyc.model.UserMaster;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Admin on 7/11/2016.
 */
public class EmailHelper {

    EmailAttributeDTO emailAttributeDTO = new EmailAttributeDTO();

    public String sendEmailToUser(String TO, String Password, UserMaster userMaster, String mailType) throws MessagingException {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", emailAttributeDTO.getHOST());
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.port", emailAttributeDTO.getPORT());
        properties.setProperty("mail.smtp.socketFactory.port", emailAttributeDTO.getPORT());
        properties.put("mail.smtp.startssl.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.debug", "true");
        properties.put("mail.store.protocol", "pop3");
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.debug.auth", "true");
        properties.setProperty("mail.pop3.socketFactory.fallback", "false");
        String content = "http://www.totalkyc.com";
        String BODY = "Your account is created successfully with TotalKYC.  Your UserName is '" + userMaster.getUniqueId().toString() + " 'and Password is  '" + Password +
                "'. Thank you";
        String subject = "";
        if (mailType.equalsIgnoreCase("createUser")) {
            BODY = "Hello " + userMaster.getUserName() + ",\n" +
                    "\n" +
                    "You are just a step away from accessing your TotalKYC account.\n" +
                    "We are sharing a unique User ID and password. The password is valid for 24 hours and usable only once. \n" +
                    "Once you login, you’ll be prompted to set a new password immediately. This is to ensure that only you have access to your account.\n" +
                    "\n" +
                    "User ID:     " + userMaster.getUniqueId() + "\n" +
                    "Password: " + Password + "\n" +
                    "\n" +
                    "To login your account Click here " + content +
                    "\n" +
                    "\n" +
                    "Need Help? Please reach out to the TotalKYC administrator of your organisation or feel free to write to us on admin@karza.in and we will get back to ASAP!\n" +
                    "\n" +
                    "\n" +
                    "Best Regards,\n" +
                    "Team TotalKYC\n" +
                    "\n" +
                    "Note: This is a system generated email. Please do NOT reply to this email.\n";
            subject = "User Account created with TotalKYC";
        }

        if (mailType.equalsIgnoreCase("activateUser")) {
            BODY = "Hello " + userMaster.getUserName() + ",\n" +
                    "\n" +
                    "Your User ID: " + userMaster.getUniqueId() + " with TotalKYC is activated. You are just a step away from accessing your TotalKYC account.\n" +
                    "We are sharing a unique password. The password is valid for 24 hours and usable only once. \n" +
                    "Once you login, you’ll be prompted to set a new password immediately. This is to ensure that only you have access to your account.\n" +
                    "Password: " + Password + "\n" +
                    "To login your account Click here " + content +
                    "\n" +
                    "Need Help? Please reach out to the TotalKYC administrator of your organisation or feel free to write to us on admin@karza.in and we will get back to ASAP!\n" +
                    "\n" +
                    "\n" +
                    "Best Regards,\n" +
                    "Team TotalKYC\n" +
                    "\n" +
                    "Note: This is a system generated email. Please do NOT reply to this email.\n" +
                    "\n";
            subject = "Activation of User ID: " + userMaster.getUniqueId() + " with TotalKYC";
        }

        if (mailType.equalsIgnoreCase("deactivateUser")) {
            BODY = "Hello " + userMaster.getUserName() + ",\n" +
                    "\n" +
                    "Your User ID: " + userMaster.getUniqueId() + " with TotalKYC is deactivated. \n" + "\n" +
                    "You will not be allowed to login and operate your account with the existing password.\n" +
                    "To Activate your account reach out to the TotalKYC administrator for your organisation or feel free to write to us on admin@karza.in and we will get back to you ASAP!\n" +
                    "\n" +
                    "Best Regards,\n" +
                    "Team TotalKYC\n" +
                    "\n" +
                    "Note: This is a system generated email. Please do NOT reply to this email.\n" +
                    "\n";
            subject = "Deactivation of User ID: " + userMaster.getUniqueId() + " with TotalKYC";
        }

        if (mailType.equalsIgnoreCase("changePassword")) {
            BODY = "\n" +
                    "Hello " + userMaster.getUserName() + ",\n" +
                    "\n" +
                    "Password has been changed successfully of your user ID Number: " + userMaster.getUniqueId() + " with TotalKYC.\n" + "\n" +
                    "We advise you not to share it with anyone else. This is to ensure that only you have access to your account.\n" +
                    "To login your account Click here " + content +
                    "\n" +
                    "Need Help? Please reach out to the TotalKYC administrator for your organisation or feel free to write to us on admin@karza.in and we will get back to you ASAP!\n" +
                    "\n" +
                    "\n" +
                    "Best Regards,\n" +
                    "Team TotalKYC\n" +
                    "\n" +
                    "Note: This is a system generated email. Please do NOT reply to this email.\n" +
                    "\n";
            subject = "Change in Password of User ID: " + userMaster.getUniqueId() + " with TotalKYC";
        }

        if (mailType.equalsIgnoreCase("forgotPassword")) {
            BODY = "\n" +
                    "Hello " + userMaster.getUserName() + ",\n" +
                    "\n" +
                    "You are just a step away from accessing your TotalKYC account.\n" +
                    "We are sharing a unique password. The password is valid for 24 hours and usable only once. \n" +
                    "Once you login, you’ll be prompted to set a new password immediately. This is to ensure that only you have access to your account.\n" +
                    "Password: " + Password + "\n" +
                    "To login your account Click here " + content +
                    "\n" +
                    "Need Help? Please reach out to the TotalKYC administrator of your organisation or feel free to write to us on admin@karza.in and we will get back to ASAP!\n" +
                    "\n" +
                    "Best Regards,\n" +
                    "Team TotalKYC\n" +
                    "\n" +
                    "Note: This is a system generated email. Please do NOT reply to this email.\n" +
                    "\n";
            subject = "New Password of User ID: " + userMaster.getUniqueId() + " with TotalKYC";
        }

        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailAttributeDTO.getSMTP_USERNAME(), emailAttributeDTO.getSMTP_PASSWORD());
            }
        });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailAttributeDTO.getFROM()));
            message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(TO));
            message.setSubject(subject);
            message.setText(BODY);
            Transport.send(message);
            return ("Success");
        } catch (MessagingException e) {
            e.printStackTrace();
            return ("Error: " + e.getMessage());
        }

    }

    public String sendPendingEntityDetailToUser(String TO, String userName, JSONArray entityData) throws MessagingException,JSONException {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", emailAttributeDTO.getHOST());
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.port", emailAttributeDTO.getPORT());
        properties.setProperty("mail.smtp.socketFactory.port", emailAttributeDTO.getPORT());
        properties.put("mail.smtp.startssl.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.debug", "true");
        properties.put("mail.store.protocol", "pop3");
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.debug.auth", "true");
        properties.setProperty("mail.pop3.socketFactory.fallback", "false");
        StringBuilder table = new StringBuilder();
        table.append("<table border=1>");
        table.append("<tr>");
        table.append("<th>Report ID</th>");
        table.append("<th>Entity Name</th>");
        table.append("<th>Entity Type</th>");
        table.append("<th>Reason of Pending</th>");
        table.append("</tr>");
        for (int tr_index = 0; tr_index < entityData.length(); tr_index++) {
            JSONObject entity = entityData.getJSONObject(tr_index);
            table.append("<tr>");
            table.append("<td>" + entity.get("requestId").toString() + "</td>");
            table.append("<td>" + entity.get("entityName").toString() + "</td>");
            table.append("<td>" + entity.get("entityType").toString() + "</td>");
            table.append("<td>" + entity.get("PendingDocumentList").toString() + "</td>");
            table.append("</tr>");
        }
        table.append("</table>");
        String BODY = "Main Content of the mail:<br><br><br>" +
                " Hi " + userName + ",<br>" +
                " <br><br>You have total " + entityData.length() + " pending requests as at " + new Date().toString() + " .<br><br>"

                + table

                + " <br><br>To know more about the issues and to post any query reach out to us at info@karza.in or call at <br><br> +91-22–XXXXXXXX.<br><br>Thanks<br>";
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailAttributeDTO.getSMTP_USERNAME(), emailAttributeDTO.getSMTP_PASSWORD());
            }
        });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailAttributeDTO.getFROM()));
            message.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(TO));
            message.setSubject("Status updates for total 6 pending request");
            message.setContent(BODY, "text/html; charset=utf-8");
            Transport.send(message);
            return ("Success");
        } catch (MessagingException e) {
            e.printStackTrace();
            return ("Error: " + e.getMessage());
        }

    }

}
