package com.karza.kyc.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.service.PanCardService;
public class PANHelper {

	@Autowired
	PanCardService cardService;
	
	public static PanCard savePanDetails(PanCard panObj,JSONObject panResponse,
			String entity,PanCardService cardService,CustomerEntity customerEntity) throws JSONException{
		
		//PanCard panResponseObj = new PanCard(panObj.getDocument(),entity,customerEntity.getId(),(JSONObject)KYCUtil.checkKeyAndGetValueFromJSON(panResponse,"pan"));
		PanCard panResponseObj = new PanCard(panObj.getDocument(),entity,customerEntity.getId(),panResponse);
		panResponseObj.setId(panObj.getId());
		cardService.update(panResponseObj);
		return panResponseObj;
	//  PanCard panCard1 = new PanCard(panCard.getDocument(), entity_type, customerEntity.getId(), panApiResponse);
	}
	
}
