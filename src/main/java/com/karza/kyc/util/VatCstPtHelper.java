package com.karza.kyc.util;

import com.karza.kyc.model.*;
import com.karza.kyc.service.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/30/16
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 */

public class VatCstPtHelper {

    public static VAT saveVat(JSONObject vatApiResponse, CustomerEntity customerEntity, String vat_document, VATService vatService, VATFrequencyService vatFrequencyService) throws JSONException {
        VAT vat;
        VAT existingVat = vatService.getByCustomerId(customerEntity.getId());
        JSONObject vatData = new JSONObject();
        if (vatApiResponse.has("vatData")) {
            vatData = vatApiResponse.getJSONObject("vatData");
        }
        String api_status = "invalid";
        if (vatData.has("api_status")) {
            api_status = vatData.get("api_status").toString();
        }
        vat = new VAT(vat_document, customerEntity.getId(), api_status, vatData);
        if (existingVat == null) {
            vat.setCreatedAt(new Date());
            vat = vatService.save(vat);
        } else {
            vat.setId(existingVat.getId());
            vatService.update(vat);
            vat = existingVat;
        }

        VATFrequency vatFrequency;
        List<VATFrequency> existingVatFrequencies = vatFrequencyService.getByVatId(vat.getId());
        if (existingVatFrequencies != null) {
            for (int i = 0; i < existingVatFrequencies.size(); i++) {
                VATFrequency vatFrequency1 = existingVatFrequencies.get(i);
                vatFrequencyService.delete(vatFrequency1);
            }
        }
        JSONArray frequencies = new JSONArray();
        if (vatApiResponse.has("frequency")) {
            frequencies = vatApiResponse.getJSONArray("frequency");
        }
        for (int i = 0; i < frequencies.length(); i++) {
            JSONObject frequency = new JSONObject();
            frequency = frequencies.getJSONObject(i);
            vatFrequency = new VATFrequency(vat.getId(), frequency);
            vatFrequency.setCreatedAt(new Date());
            vatFrequencyService.save(vatFrequency);
        }
        return vat;
    }

    public static CST saveCst(JSONObject cstApiResponse, CustomerEntity customerEntity, String cst_document, CSTService cstService, CSTFrequencyService cstFrequencyService) throws IOException, JSONException {
        CST cst;
        CST existingCst = cstService.getByCustomerId(customerEntity.getId());
        JSONObject cstData = new JSONObject();
        if (cstApiResponse.has("vatData")) {
            cstData = cstApiResponse.getJSONObject("vatData");
        }
        String api_status = "invalid";
        if (cstData.has("api_status")) {
            api_status = cstData.get("api_status").toString();
        }
        cst = new CST(cst_document, customerEntity.getId(), api_status, cstData);
        if (existingCst == null) {
            cst.setCreatedAt(new Date());
            cst = cstService.save(cst);
        } else {
            cst.setId(existingCst.getId());
            cstService.update(cst);
            cst = existingCst;
        }

        CSTFrequency cstFrequency;
        List<CSTFrequency> existingCstFrequencies = cstFrequencyService.getByCstId(cst.getId());
        if (existingCstFrequencies != null) {
            for (int i = 0; i < existingCstFrequencies.size(); i++) {
                CSTFrequency cstFrequency1 = existingCstFrequencies.get(i);
                cstFrequencyService.delete(cstFrequency1);
            }
        }
        JSONArray frequencies = new JSONArray();
        if (cstApiResponse.has("frequency")) {
            frequencies = cstApiResponse.getJSONArray("frequency");
        }
        for (int i = 0; i < frequencies.length(); i++) {
            JSONObject frequency = new JSONObject();
            frequency = frequencies.getJSONObject(i);
            cstFrequency = new CSTFrequency(cst.getId(), frequency);
            cstFrequency.setCreatedAt(new Date());
            cstFrequencyService.save(cstFrequency);
        }
        return cst;
    }

    public static ProfessionalTax savePt(JSONObject ptApiResponse, CustomerEntity customerEntity, String pt_document, ProfessionalTaxService professionalTaxService, ProfessionalTaxFrequencyService professionalTaxFrequencyService) throws IOException,JSONException {
        ProfessionalTax professionalTax;
        ProfessionalTax existingProfessionalTax = professionalTaxService.getByCustomerId(customerEntity.getId());
        JSONObject ptData = new JSONObject();
        if (ptApiResponse.has("vatData")) {
            ptData = ptApiResponse.getJSONObject("vatData");
        }
        String api_status = "invalid";
        if (ptData.has("api_status")) {
            api_status = ptData.get("api_status").toString();
        }
        professionalTax = new ProfessionalTax(pt_document, customerEntity.getId(), api_status, ptData);
        if (existingProfessionalTax == null) {
            professionalTax.setCreatedAt(new Date());
            professionalTax = professionalTaxService.save(professionalTax);
        } else {
            professionalTax.setId(existingProfessionalTax.getId());
            professionalTaxService.update(professionalTax);
            professionalTax = existingProfessionalTax;
        }

        ProfessionalTaxFrequency professionalTaxFrequency;
        List<ProfessionalTaxFrequency> existingPtFrequencies = professionalTaxFrequencyService.getByProfessionalTaxId(professionalTax.getId());
        if (existingPtFrequencies != null) {
            for (int i = 0; i < existingPtFrequencies.size(); i++) {
                ProfessionalTaxFrequency professionalTaxFrequency1 = existingPtFrequencies.get(i);
                professionalTaxFrequencyService.delete(professionalTaxFrequency1);
            }
        }
        JSONArray frequencies = new JSONArray();
        if (ptApiResponse.has("frequency")) {
            frequencies = ptApiResponse.getJSONArray("frequency");
        }
        for (int i = 0; i < frequencies.length(); i++) {
            JSONObject frequency = new JSONObject();
            frequency = frequencies.getJSONObject(i);
            professionalTaxFrequency = new ProfessionalTaxFrequency(professionalTax.getId(), frequency);
            professionalTaxFrequency.setCreatedAt(new Date());
            professionalTaxFrequencyService.save(professionalTaxFrequency);
        }
        return professionalTax;
    }
}
