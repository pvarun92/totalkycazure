package com.karza.kyc.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.LPG;
import com.karza.kyc.service.LPGService;

public class LPGHelper {

	@Autowired
	LPGService lpgService;
	
	public static LPG saveLPGDetails(LPG lpgObject,JSONObject lpgResponse, CustomerEntity customerEntity, LPGService lpgService) throws JSONException{
			
			LPG lpg = new LPG(lpgObject.getDocument(),customerEntity.getId(),lpgResponse);
			lpg.setId(lpgObject.getId());
			lpgService.update(lpg);
			return lpg;
	}
	
}
