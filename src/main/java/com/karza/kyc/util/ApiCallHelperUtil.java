package com.karza.kyc.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.model.CIN;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.validators.CinValidator;
import com.karza.kyc.validators.DocumentValidatorException;
import com.karza.kyc.validators.FcrnValidator;
import com.karza.kyc.validators.FllpinValidator;
import com.karza.kyc.validators.LlpinValidator;

public class ApiCallHelperUtil {

	public static JSONObject saveCINDetails(JSONObject llpinJSONObject, Integer apiCount) {
		JSONObject llpinData = new JSONObject();
		JSONObject llpinDetailData = new JSONObject();
		String cin = "", /* Cin Details */
				listed_or_not = "", company_status = "", roc_code = "", company_subcategory = "", email_id = "",
				suspended_at_stock_exchange = "", alternative_address = "", date_of_bal_sheet = "",
				authorised_capital = "", company_category = "", date_of_last_agm = "", company_name = "",
				paid_up_capital = "", reg_address = "", no_of_members = "", class_of_company = "", reg_number = "",
				date_of_incorporation = "";
		try {
			listed_or_not = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Whether_Listed_or_not");
			llpinData.put("Whether Listed or not", listed_or_not);
			company_status = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Company_Status");
			llpinData.put("Company Status", company_status);
			roc_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "ROC_Code");
			llpinData.put("ROC Code", roc_code);
			company_subcategory = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Company_SubCategory");
			llpinData.put("Company SubCategory", company_subcategory);
			email_id = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Email_Id");
			llpinData.put("Email Id", email_id);
			suspended_at_stock_exchange = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Suspended_at_stock_exchange");
			llpinData.put("Suspended At Stock", suspended_at_stock_exchange);
			alternative_address = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "alternative_address");
			llpinData.put("Alternative Address", alternative_address);
			date_of_bal_sheet = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Date_of_Balance_Sheet");
			llpinData.put("Date of Balance Sheet", date_of_bal_sheet);
			authorised_capital = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Authorised_Capital(Rs)");
			llpinData.put("Authorised Capital(Rs)", authorised_capital);
			company_category = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Company_Category");
			llpinData.put("Company Category", company_category);
			date_of_last_agm = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Date_of_last_AGM");
			llpinData.put("Date of last AGM", date_of_last_agm);
			company_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Company_Name");
			llpinData.put("Company Name", company_name);
			paid_up_capital = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Paid_up_Capital(Rs)");
			reg_address = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Registered_Address");
			no_of_members = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Number_of_Members");
			class_of_company = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Class_of_Company");
			reg_number = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Registration_Number");
			date_of_incorporation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Date_of_Incorporation");
			cin = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "cin");
			llpinData.put("Paid up Capital(Rs)", paid_up_capital);
			llpinData.put("Registered Address", reg_address);
			llpinData.put("Number of Members", no_of_members);
			llpinData.put("Class of Company", class_of_company);
			llpinData.put("Registration Number", reg_number);
			llpinData.put("Date of Incorporation", date_of_incorporation);
			llpinData.put("statusAsPerSource", company_status);
			llpinData.put("cin", cin);
			llpinData.put("documentType", "cin");
			llpinData.put("apiCount", apiCount);
			llpinDetailData.put("statusAsPerSource", "valid");
			llpinDetailData.put("status", "valid");
			llpinDetailData.put("apiCount", apiCount);
			llpinDetailData.put("llpin", llpinData);
			return llpinDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return llpinDetailData;
	}
	
	public static JSONObject saveFLLPIN(JSONObject llpinJSONObject, Integer apiCount) {
		JSONObject llpinData = new JSONObject();
		JSONObject llpinDetailData = new JSONObject();
		String fllpin = "", /* Details for FLLPIN */
				foreign_llp_name = "", type_of_details = "", main_div_of_business_activity = "", details = "",
				country_of_incorportation = "", desc_of_main_div = "", email_id = "", date_of_incorporation = "",
				date_of_last_fyed_soa = "", fllp_status = "", no_of_representative = "", type_of_office = "",
				reg_address = "";

		try {
			foreign_llp_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Foreign_LLP_Name");
			type_of_office = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Type_of_Office");
			details = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Details");
			main_div_of_business_activity = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Main_division_of_business_activity");
			date_of_last_fyed_soa = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"date_of_last_fyed_SOA");
			country_of_incorportation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Country_of_Incorporation");
			desc_of_main_div = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Description_of_main_division");
			fllp_status = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "FLLP_Status");
			no_of_representative = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"no_of_representatives");
			email_id = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Email_Id");
			date_of_incorporation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Date_of_Incorporation");
			reg_address = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Registered_Address");
			llpinData.put("Email Id", email_id);
			llpinData.put("FLLPIN", fllpin);
			llpinData.put("Foreign LLP Name", foreign_llp_name);
			llpinData.put("Type of Office", type_of_office);
			llpinData.put("Details", details);
			llpinData.put("Main division of business activity", main_div_of_business_activity);
			llpinData.put("Date of last financial year", date_of_last_fyed_soa);
			llpinData.put("Country of Incorporation", country_of_incorportation);
			llpinData.put("Description of main division", desc_of_main_div);
			llpinData.put("FLLP Status", fllp_status);
			llpinData.put("Date of Incorporation", date_of_incorporation);
			llpinData.put("Number of Authorised Representatives", no_of_representative);
			llpinData.put("Registered Address", reg_address);
			fllpin = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "FLLPIN");
			llpinData.put("fllpin", fllpin);
			llpinData.put("documentType", "fllpin");
			llpinData.put("statusAsPerSource", fllp_status);
			llpinData.put("apiCount", apiCount);
			llpinDetailData.put("status", "valid");
			llpinDetailData.put("apiCount", apiCount);
			llpinDetailData.put("llpin", llpinData);
			return llpinDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return llpinDetailData;
	}

	public static JSONObject saveFCRN(JSONObject llpinJSONObject, Integer apiCount) {
		String fcrn = "", /* FCRN Deatils */
				main_div_out_of_india = "", type_of_office = "", country_of_incorportation = "", company_name = "",
				details = "", foreign_share_capital = "", email_id = "", company_status = "", desc_of_main_div = "",
				reg_address = "", date_of_incorporation = "";
		JSONObject llpinData = new JSONObject();
		JSONObject llpinDetailData = new JSONObject();
		try {
			main_div_out_of_india = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"main_division_outof_india");
			type_of_office = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Type_of_Office");
			foreign_share_capital = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Foreign_Share_Capital");
			country_of_incorportation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Country_of_Incorporation");
			details = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Details");
			email_id = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Email_Id");
			company_status = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Company_Status");
			desc_of_main_div = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Description_of_main_division");
			company_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Company_Name");
			reg_address = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Registered_Address");
			date_of_incorporation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Date_of_Incorporation");
			llpinData.put("Company Name", company_name);
			llpinData.put("Foreign Company with Share Capital", foreign_share_capital);
			llpinData.put("Main division of business activity to be carried out in India", main_div_out_of_india);
			llpinData.put("Type of Office", type_of_office);
			llpinData.put("Details", details);
			llpinData.put("Email Id", email_id);
			llpinData.put("Company Status", company_status);
			llpinData.put("Country of Incorporation", country_of_incorportation);
			llpinData.put("Description of main division", desc_of_main_div);
			llpinData.put("Registered Address", reg_address);
			llpinData.put("Date of Incorporation", date_of_incorporation);
			fcrn = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "FCRN");
			llpinData.put("fcrn", fcrn);
			llpinData.put("documentType", "fcrn");
			llpinData.put("statusAsPerSource", company_status);
			llpinData.put("apiCount", apiCount);
			llpinDetailData.put("status", "valid");
			llpinDetailData.put("apiCount", apiCount);
			llpinDetailData.put("llpin", llpinData);
			return llpinDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return llpinDetailData;
	}

	public static JSONObject saveLLPIN(JSONObject llpinJSONObject, Integer apiCount) {
		JSONObject llpinData = new JSONObject();
		JSONObject llpinDetailData = new JSONObject();
		String llpin = "", /* LLPin Details */
				total_obigation = "", previous_firm = "", no_of_partners = "", annual_returned_filed = "",
				solvency_filed = "", llp_status = "", no_of_designated_partners = "", roc_code = "", email_id = "",
				date_of_incorporation = "", main_division = "", llp_name = "", desc_of_main_div = "", reg_address = "";

		try {
			total_obigation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Total_Obligation_of_Contribution");
			previous_firm = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Previous_firm");
			no_of_partners = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Number_of_Partners");
			annual_returned_filed = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Annual_Return_filed");
			solvency_filed = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Solvency_filed");
			llp_status = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "LLP_Status");
			no_of_designated_partners = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Number_of_Designated_Partners");
			main_division = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Main_division");
			llp_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "LLP_Name");
			desc_of_main_div = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Description_of_main_division");
			roc_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "ROC_Code");
			email_id = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Email_Id");
			date_of_incorporation = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject,
					"Date_of_Incorporation");
			reg_address = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "Registered_Address");
			llpinData.put("Date of Incorporation", date_of_incorporation);
			llpinData.put("ROC Code", roc_code);
			llpinData.put("LLP Name", llp_name);
			llpinData.put("Number of Partners", no_of_partners);
			llpinData.put("Number of Designated Partners", no_of_designated_partners);
			llpinData.put("Previous firm/ company details, if applicable", previous_firm);
			llpinData.put("Total Obligation of Contribution", total_obigation);
			llpinData.put("Description of main division", main_division);
			llpinData.put("Email Id", email_id);
			llpinData.put("LLP Status", llp_status);
			llpinData.put("Description of main division", desc_of_main_div);
			llpinData.put("Date of last financial year end date for which Statement of Accounts and Solvency filed",
					solvency_filed);
			llpinData.put("Date of last financial year end date for which Statement of Accounts",
					annual_returned_filed);
			llpin = (String) KYCUtil.checkKeyAndGetValueFromJSON(llpinJSONObject, "LLPIN");
			llpinData.put("llpin", llpin);
			llpinData.put("documentType", "llpin");
			llpinData.put("statusAsPerSource", llp_status);
			llpinData.put("apiCount", apiCount);
			llpinData.put("Registered Address", reg_address);
			llpinDetailData.put("status", "valid");
			llpinDetailData.put("apiCount", apiCount);
			llpinDetailData.put("llpin", llpinData);
			return llpinDetailData;
		} catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return llpinDetailData;
	}

	private static String handleLLPDocumentType(String id, String entity_type) {
		String firstChars = "";
		Boolean dashContains = id.contains("-");
		// FLLPIN/LLPIN validation
		if (dashContains) {
			firstChars = id.split("-")[0];
			if (id.length() == 8) {
				// LLPIN validation
				if (dashContains && id.length() <= 8 && firstChars.length() <= 3) {
					LLPIN p = new LLPIN(id);
					try {
						boolean isLLPPIN = new LlpinValidator().isValid(p);
						if (isLLPPIN) {
							return "llpin";
						}
					} catch (DocumentValidatorException e) {
					}
				}
			} else if (id.length() == 9) {
				// FLLPIN validation
				if (dashContains) {
					FLLPIN p = new FLLPIN(id);
					try {
						boolean isFllPin = new FllpinValidator().isValid(p);
						if (isFllPin) {
							return "fllpin";
						}
					} catch (DocumentValidatorException e) {

					}
				}
			}
		} else {
			// CIN/FCRN validation
			if (dashContains) {
				firstChars = id.split("-")[0];
			}
			boolean isValidDoc = false;
			if (!dashContains && id.length() > 6) {
				CIN p = new CIN(id);
				try {
					isValidDoc = new CinValidator().isValid(p);
					if (isValidDoc) {
						return "cin";
					}
				} catch (DocumentValidatorException e) {
				}
			} else if (!dashContains && id.length() <= 6) {
				FCRN p = new FCRN(id);
				try {
					isValidDoc = new FcrnValidator().isValid(p);
					if (isValidDoc) {
						return "fcrn";
					}
				} catch (DocumentValidatorException e) {
				}
			}
		}
		return "";
	}

	private static String handleNonLLPDocument(String id, String entity_type) {
		Boolean dashContains = id.contains("-");
		if (dashContains) {
			return "";
		} else {
			boolean isDoc = false;
			if (id.length() > 6) {
				CIN p = new CIN(id);
				try {
					isDoc = new CinValidator().isValid(p);
					if (isDoc)
						return "cin";
				} catch (DocumentValidatorException e) {
				}
			} else if (id.length() <= 6) {
				FCRN p = new FCRN(id);
				try {
					isDoc = new FcrnValidator().isValid(p);
					if (isDoc)
						return "fcrn";
				} catch (DocumentValidatorException e) {
				}
			}
		}
		return "";
	}

	private static String getDocumentType(String id, String entity_type) {
		if (entity_type.equals("Limited Liability Partnership (LLP)")) {
			return handleLLPDocumentType(id, entity_type);
		} else {
			return handleNonLLPDocument(id, entity_type);
		}
	}
	
	public static JSONObject saveInvalidLLPIN(Integer apiCount,String cinId,String customer_entity){
		JSONObject responseMetaData = new JSONObject();
		JSONObject responseDomainData = new JSONObject();
		try {
			responseDomainData.put("status", "invalid");
			responseDomainData.put("apiResponse", "invalid");
			responseDomainData.put("apiCount", apiCount);
			responseDomainData.put("statusAsPerSource", "invalid");
			responseMetaData.put("llpin", responseDomainData);
			responseMetaData.put("documentType", getDocumentType(cinId, customer_entity));
			responseMetaData.put("apiCount", apiCount);
			responseMetaData.put("api_status", "invalid");
			return responseMetaData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseMetaData;
	}
	
	public static JSONObject saveLPG(JSONObject lpgJSONObject, Integer apiCount) {
		JSONObject lpgData = new JSONObject();
		JSONObject lpgDetailData = new JSONObject();
		String status_of_lpg = "", approxSubsAvailed = "", subsidizedRefillConsumed = "", pin = "", consumerEmail = "",
				distributorCode = "", bankName = "", ifscCode = "", city = "", aadharNumber = "", consumerContact = "",
				distributorAdd = "", consumerName = "", consumerNo = "", distributorName = "", bankAccountNo = "",
				givenUpSubSidy = "", consumerAdd = "", lastBookingDate = "", totalRefillConsumed = "";
		try {
			status_of_lpg = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "status");
			approxSubsAvailed = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject,
					"ApproximateSubsidyAvailed");
			subsidizedRefillConsumed = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject,
					"SubsidizedRefillConsumed");
			pin = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "pin");
			consumerEmail = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "ConsumerEmail");
			distributorCode = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "DistributorCode");
			bankName = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "BankName");
			ifscCode = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "IFSCCode");
			city = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "city/town");
			aadharNumber = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "AadhaarNo");
			consumerContact = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "ConsumerContact");
			distributorAdd = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "DistributorAddress");
			consumerName = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "ConsumerName");
			consumerNo = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "ConsumerNo");
			distributorName = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "DistributorName");
			bankAccountNo = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "BankAccountNo");
			givenUpSubSidy = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "GivenUpSubsidy");
			consumerAdd = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "ConsumerAddress");
			lastBookingDate = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "LastBookingDate");
			totalRefillConsumed = (String) KYCUtil.checkKeyAndGetValueFromJSON(lpgJSONObject, "TotalRefillConsumed");
			lpgData.put("Status Of LPG", status_of_lpg);
			lpgData.put("ApproximateSubsidyAvailed", approxSubsAvailed);
			lpgData.put("SubsidizedRefillConsumed", subsidizedRefillConsumed);
			lpgData.put("pin", pin);
			lpgData.put("ConsumerEmail", consumerEmail);
			lpgData.put("DistributorCode", distributorCode);
			lpgData.put("BankName", bankName);
			lpgData.put("IFSCCode", ifscCode);
			lpgData.put("city/town", city);
			lpgData.put("AadhaarNo", aadharNumber);
			lpgData.put("ConsumerContact", consumerContact);
			lpgData.put("DistributorAddress", distributorAdd);
			lpgData.put("ConsumerName", consumerName);
			lpgData.put("ConsumerNo", consumerNo);
			lpgData.put("DistributorNames", distributorName);
			lpgData.put("BankAccountNo", bankAccountNo);
			lpgData.put("GivenUpSubsidy", givenUpSubSidy);
			lpgData.put("ConsumerAddress", consumerAdd);
			lpgData.put("LastBookingDate", lastBookingDate);
			lpgData.put("TotalRefillConsumed", totalRefillConsumed);
			lpgData.put("apiCount", apiCount);
			lpgData.put("status", "valid");
			lpgData.put("statusAsPerSource", "valid");
			lpgData.put("apiResponse", "valid");
			lpgDetailData.put("apiCount", apiCount);
			lpgDetailData.put("status", "valid");
			lpgDetailData.put("statusAsPerSource", "valid");
			lpgDetailData.put("apiResponse", "valid");
			lpgDetailData.put("lpg", lpgData);
			return lpgDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return lpgDetailData;
	}
	
	public static JSONObject saveInvalidLPG(Integer apiCount){
		JSONObject lpgData = new JSONObject();
		JSONObject lpgDetailData = new JSONObject();
		try {
			lpgData.put("status", "invalid");
			lpgData.put("apiResponse", "invalid");
			lpgData.put("apiCount", apiCount);
			lpgData.put("statusAsPerSource", "invalid");
			lpgDetailData.put("apiCount", apiCount);
			lpgDetailData.put("status", "invalid");
			lpgDetailData.put("apiResponse", "invalid");
			lpgDetailData.put("statusAsPerSource", "invalid");
			lpgDetailData.put("lpg", lpgData);
			return lpgDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return lpgDetailData;
	}
	
	public static JSONObject saveESIC(JSONObject esicJSONResult, Integer apiCount) {
		JSONObject esicData = null;
		JSONObject esicDetailData = null;
		JSONArray esicContriArray = new JSONArray();
		List<JSONObject> esicList = new ArrayList<JSONObject>();
		String uhid = "", dob = "", aadharStatus = "", relation_with_ip = "", current_date_of_app = "",
				nominee_add = "", present_add = "", disability = "", aadharNo = "", phoneNo = "", marital_status = "",
				name = "", gender = "", nominee_adhaar_no = "", father_or_husband = "", reg_date = "",
				permanent_address = "", nominee_name = "", insurance_no = "", first_date_of_appointment = "",
				dispensary_name = "", employee_contribution = "", wage_period = "", total_monthly_wages = "",
				number_of_days_wages_paid = "";
		try {
			uhid = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "UHID").toString();
			dob = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "DOB").toString();
			aadharStatus = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "AdhaarStatus").toString();
			relation_with_ip = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Relation_with_IP").toString();
			current_date_of_app = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "CurrentDateOfAppointment")
					.toString();
			nominee_add = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Nominee_Address").toString();
			present_add = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "PresentAddress").toString();
			disability = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "DisabilityType").toString();
			aadharNo = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "AdhaarNO").toString();
			phoneNo = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "PhoneNO").toString();
			marital_status = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "MaritialStatus").toString();
			name = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Name").toString();
			gender = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Gender").toString();
			nominee_adhaar_no = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Nominee_AdhaarNO").toString();
			father_or_husband = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Father_or_Husband").toString();
			reg_date = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "RegistrationDate").toString();
			permanent_address = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "PermanentAddress").toString();
			nominee_name = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "Nominee_Name").toString();
			insurance_no = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "InsuranceNO").toString();
			first_date_of_appointment = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "FirstDateOfAppointment")
					.toString();
			dispensary_name = KYCUtil.checkKeyAndGetValueFromJSON(esicJSONResult, "DispensaryName").toString();
			// JSONArray for the value of the contribution Details
			esicContriArray = esicJSONResult.getJSONArray("Contribution_Details");
			for (int i = 0; i < esicContriArray.length(); i++) {
				JSONObject esicContribJSONObject = new JSONObject();
				esicData = esicContriArray.getJSONObject(i);
				employee_contribution = KYCUtil.checkKeyAndGetValueFromJSON(esicData, "Employee_Conrtibution")
						.toString();
				wage_period = KYCUtil.checkKeyAndGetValueFromJSON(esicData, "Wage_Period").toString();
				total_monthly_wages = KYCUtil.checkKeyAndGetValueFromJSON(esicData, "Total_Monthly_Wages").toString();
				number_of_days_wages_paid = KYCUtil.checkKeyAndGetValueFromJSON(esicData, "Number_of_Days_wages_paid")
						.toString();
				JSONObject esicDataTempObj = new JSONObject();
				esicDataTempObj.put("Employee_Conrtibution", employee_contribution);
				esicDataTempObj.put("Wage_Period", wage_period);
				esicDataTempObj.put("total_monthly_wages", total_monthly_wages);
				esicDataTempObj.put("Number_of_Days_wages_paid", number_of_days_wages_paid);
				esicContribJSONObject.put("contribution", esicDataTempObj);
				esicList.add(esicContribJSONObject);
			}
			esicData.put("Name", name);
			esicData.put("UHID", uhid);
			esicData.put("DOB", dob);
			esicData.put("AdhaarStatus", aadharStatus);
			esicData.put("Relation_with_IP", relation_with_ip);
			esicData.put("CurrentDateOfAppointment", current_date_of_app);
			esicData.put("Nominee_Address", nominee_add);
			esicData.put("PresentAddress", present_add);
			esicData.put("DisabilityType", disability);
			esicData.put("AdhaarNO", aadharNo);
			esicData.put("PhoneNO", phoneNo);
			esicData.put("MaritialStatus", marital_status);
			esicData.put("Gender", gender);
			esicData.put("Nominee_AdhaarNO", nominee_adhaar_no);
			esicData.put("Father_or_Husband", father_or_husband);
			esicData.put("RegistrationDate", reg_date);
			esicData.put("PermanentAddress", permanent_address);
			esicData.put("Nominee_Name", nominee_name);
			esicData.put("InsuranceNO", insurance_no);
			esicData.put("FirstDateOfAppointment", first_date_of_appointment);
			esicData.put("DispensaryName", dispensary_name);
			esicData.put("apiCount", apiCount);
			esicData.put("status", "valid");
			esicData.put("statusAsPerSource", "valid");
			esicData.put("apiValidation", "valid");
			esicDetailData.put("apiCount", apiCount);
			esicDetailData.put("status", "valid");
			esicDetailData.put("statusAsPerSource", "valid");
			esicDetailData.put("apiValidation", "valid");
			esicDetailData.put("esic", esicData);
			esicDetailData.put("Contribution", esicList);
			return esicDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return esicDetailData;
	}

	public static JSONObject saveInvalidESIC(Integer apiCount){
		JSONObject esicData = null;
		JSONObject esicDetailData = null;
		try {
			esicData = new JSONObject();
			esicDetailData = new JSONObject();
			esicData.put("status", "invalid");
			esicData.put("statusAsPerSource", "invalid");
			esicData.put("apiCount", apiCount);
			esicData.put("apiValidation", "invalid");
			esicDetailData.put("status", "invalid");
			esicDetailData.put("statusAsPerSource", "invalid");
			esicDetailData.put("apiValidation", "invalid");
			esicDetailData.put("apiCount", apiCount);
			esicDetailData.put("esic", esicData);
			return esicDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return esicDetailData;
	}
	
	public static JSONObject saveTelephone(JSONObject teleJSONObject, Integer apiCount) {
		JSONObject telData = new JSONObject();
		JSONObject telephoneData = new JSONObject();

		String name = "", category = "", telephone = "", installation = "", address = "";
		try {
			name = (String) KYCUtil.checkKeyAndGetValueFromJSON(teleJSONObject, "name");
			telData.put("name", name);
			category = (String) KYCUtil.checkKeyAndGetValueFromJSON(teleJSONObject, "category");
			telData.put("category", category);
			telephone = (String) KYCUtil.checkKeyAndGetValueFromJSON(teleJSONObject, "TelephoneNo");
			telData.put("telephone", telephone);
			installation = (String) KYCUtil.checkKeyAndGetValueFromJSON(teleJSONObject, "Installation_Type");
			telData.put("installation", installation);
			address = (String) KYCUtil.checkKeyAndGetValueFromJSON(teleJSONObject, "address");
			telData.put("address", address);
			telData.put("status", "valid");
			telData.put("apiResponse", "valid");
			telData.put("apiCount", apiCount);
			telephoneData.put("telephone", telData);
			telephoneData.put("apiResponse", "valid");
			telephoneData.put("status", "valid");
			telephoneData.put("apiCount", apiCount);
			return telephoneData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return telephoneData;
	}

	public static JSONObject saveInvalidTelephone(Integer apiCount){
		JSONObject telData = new JSONObject();
		JSONObject telephoneData = new JSONObject();
		try {
			telData.put("status", "invalid");
			telData.put("apiResponse", "invalid");
			telData.put("apiCount", apiCount);
			telephoneData.put("telephone", telData);
			telephoneData.put("apiCount", apiCount);
			return telephoneData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return telephoneData;
	}
	
	public static JSONObject savePan(JSONObject panJSONObject, Integer apiCount) {
		JSONObject panData = new JSONObject();
		JSONObject panDetailData = new JSONObject();
		String firstName = "", lastName = "", middleName = "", status = "";
		try {
			firstName = (String) KYCUtil.checkKeyAndGetValueFromJSON(panJSONObject, "first_name");
			panData.put("firstName", firstName);
			middleName = (String) KYCUtil.checkKeyAndGetValueFromJSON(panJSONObject, "middle_name");
			panData.put("middleName", middleName);
			lastName = (String) KYCUtil.checkKeyAndGetValueFromJSON(panJSONObject, "name");
			panData.put("lastName", lastName);
			status = (String) KYCUtil.checkKeyAndGetValueFromJSON(panJSONObject, "status");
			panData.put("status",status);
			panData.put("apiResponse", "valid");
			panData.put("statusAsPerSource", "valid");
		//	panDetailData.put("status", "valid");
			//panDetailData.put("apiResponse", "valid");
			panData.put("apiCount", apiCount);
			//panDetailData.put("statusAsPerSource", "valid");
			panDetailData.put("pan", panData);
			//panDetailData.put("apiCount", apiCount);
			return panDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return panDetailData;
	}
	
	public static JSONObject saveInvalidPan(Integer apiCount){
		JSONObject panData = new JSONObject();
		JSONObject panDetailData = new JSONObject();
		try {
	//		panData.put("status", "invalid");
			panData.put("apiResponse", "invalid");
			panData.put("statusAsPerSource", "invalid");
			panData.put("apiCount", apiCount);
			panDetailData.put("pan", panData);
		//	panDetailData.put("apiCount", apiCount);
			return panDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return panDetailData;
		
	}
	
	public static List<JSONObject> saveEPFO(JSONObject epfoResultJSON, Integer apiCount) {
		JSONObject epfoData = new JSONObject();
		JSONObject epfoDetailData = new JSONObject();
		JSONArray epfoDetailsArray = new JSONArray();
		JSONObject epfData = null;
		List<JSONObject> epfList = new ArrayList<JSONObject>();
		String statusOfSettlement = "", name = "", dob = "", employer_share = "", mid = "", employee_share = "",
				last_contributed_amount = "", uan = "", balance = "", last_transaction_date = "";
		try {
			epfoDetailsArray = epfoResultJSON.getJSONArray("epf_details");
			for (int i = 0; i < epfoDetailsArray.length(); i++) {
				epfoDetailData = new JSONObject();
				epfoData = epfoDetailsArray.getJSONObject(i);
				statusOfSettlement = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "status").toString();
				name = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "name").toString();
				System.out.println("Name is " + name);
				dob = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "dob").toString();
				employer_share = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "employer_share").toString();
				mid = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "mid").toString();
				employee_share = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "employee_share").toString();
				last_transaction_date = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "last_transaction_date")
						.toString();
				last_contributed_amount = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "last_contributed_amount")
						.toString();
				uan = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "uan").toString();
				balance = KYCUtil.checkKeyAndGetValueFromJSON(epfoData, "balance").toString();
				epfData = new JSONObject();
				epfData.put("statusOfSettlement", statusOfSettlement);
				epfData.put("name", name);
				epfData.put("dob", dob);
				epfData.put("employer_share", employer_share);
				epfData.put("mid", mid);
				epfData.put("employee_share", employee_share);
				epfData.put("last_transaction_date", last_transaction_date);
				epfData.put("last_contributed_amount", last_contributed_amount);
				epfData.put("uan", uan);
				epfData.put("balance", balance);
				epfData.put("status", "valid");
				epfData.put("apiResponse", "valid");
				epfData.put("statusAsPerSource", "valid");
				epfData.put("apiCount", apiCount);
				epfoDetailData.put("epf", epfData);
				epfoDetailData.put("apiCount", apiCount);
				epfoDetailData.put("apiResponse", "valid");
				epfoDetailData.put("statusAsPerSource", "valid");
				epfoDetailData.put("status", "valid");
				epfList.add(epfoDetailData);
				return epfList;
			}
			return epfList;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return epfList;
	}

	public  static List<JSONObject> saveInvalidEPFO(Integer apiCount){
			JSONObject epfData = null;
			JSONObject epfoDetailData = null;
			List<JSONObject> epfList = new ArrayList<JSONObject>();
		try {
			epfData = new JSONObject();
			epfoDetailData = new JSONObject();
			epfData.put("status", "invalid");
			epfData.put("apiResponse", "invalid");
			epfData.put("statusAsPerSource", "invalid");
			epfData.put("apiCount", apiCount);
			epfoDetailData.put("epf", epfData);
			epfoDetailData.put("apiCount", apiCount);
			epfList.add(epfoDetailData);
			return epfList;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return epfList;
	}
	
	public static JSONObject saveExcise(JSONObject exciseObject, Integer apiCount) {
		JSONObject exciseData = new JSONObject();
		JSONObject exciseDetailData = new JSONObject();
		String assessee_belong = "", status = "", name_of_assessee = "", location_code = "", status_as_on = "",
				address_of_assessee = "", comissionarate_code = "", range_code = "", division_code = "";
		try {
			assessee_belong = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Assessee_Belongs_To");
			exciseData.put("Assessee Belongs To", assessee_belong);
			status = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Status");
			exciseData.put("Status", status);
			name_of_assessee = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Name_of_Assessee");
			exciseData.put("Name of Assessee", name_of_assessee);
			location_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Location_Code");
			exciseData.put("Location Code", location_code);
			status_as_on = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Status_As_On");
			exciseData.put("status_as_on", status_as_on);
			address_of_assessee = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Address_of_Assessee");
			exciseData.put("Address of Assessee", address_of_assessee);
			comissionarate_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Commissionerate_Code");
			exciseData.put("Commissionerate Code", comissionarate_code);
			range_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Range_Code");
			exciseData.put("Range Code", range_code);
			division_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(exciseObject, "Division");
			exciseData.put("Division Code", division_code);
			exciseData.put("status", status);
			exciseData.put("apiResponse", "valid");
			exciseData.put("apiCount", apiCount);
			exciseDetailData.put("status", status);
			exciseDetailData.put("apiResponse", "valid");
			exciseDetailData.put("excise", exciseData);
			exciseDetailData.put("apiCount", apiCount);
			return exciseDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return exciseDetailData;
	}
	
	public static JSONObject saveInvalidExcise(Integer apiCount){
		JSONObject exciseData = new JSONObject();
		JSONObject exciseDetailData = new JSONObject();
		try {
			exciseData.put("status", "invalid");
			exciseData.put("apiResponse", "invalid");
			exciseDetailData.put("excise", exciseData);
			exciseDetailData.put("status", "invalid");
			exciseDetailData.put("apiResponse", "invalid");
			exciseDetailData.put("apiCount", apiCount);
			return exciseDetailData;

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return exciseDetailData;
	}
	
	public static JSONObject saveServiceTax(JSONObject serviceTaxJSON, Integer apiCount) {
		JSONObject serviceTaxData = new JSONObject();
		JSONObject serviceTaxDetailData = new JSONObject();
		String assessee_belong = "", status = "", name_of_assessee = "", location_code = "", status_as_on = "",
				address_of_assessee = "", comissionarate_code = "", range_code = "", division_code = "";
		try {
			assessee_belong = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Assessee_Belongs_To");
			serviceTaxData.put("Assessee Belongs To", assessee_belong);
			status = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Status");
			serviceTaxData.put("Status", status);
			name_of_assessee = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Name_of_Assessee");
			serviceTaxData.put("Name of Assessee", name_of_assessee);
			location_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Location_Code");
			serviceTaxData.put("Location Code", location_code);
			status_as_on = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Status_As_On");
			serviceTaxData.put("status_as_on", status_as_on);
			address_of_assessee = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Address_of_Assessee");
			serviceTaxData.put("Address of Assessee", address_of_assessee);
			comissionarate_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Commissionerate_Code");
			serviceTaxData.put("Commissionerate Code", comissionarate_code);
			range_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Range_Code");
			serviceTaxData.put("Range Code", range_code);
			division_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(serviceTaxJSON, "Division");
			serviceTaxData.put("Division Code", division_code);
			serviceTaxData.put("apiCount", apiCount);
			serviceTaxData.put("status", "valid");
			serviceTaxData.put("apiResponse", "valid");
			serviceTaxDetailData.put("status", "valid");
			serviceTaxDetailData.put("apiResponse", "valid");
			serviceTaxDetailData.put("service_tax", serviceTaxData);
			serviceTaxDetailData.put("apiCount", apiCount);
			return serviceTaxDetailData;

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return serviceTaxDetailData;
	}
	
	
	public static JSONObject saveInvalidServiceTax(Integer apiCount){
		JSONObject serviceTaxData = new JSONObject();
		JSONObject serviceTaxDetailData = new JSONObject();
		try {
			serviceTaxData.put("status", "invalid");
			serviceTaxData.put("apiResponse", "invalid");
			serviceTaxData.put("Status", "invalid");
			serviceTaxDetailData.put("service_tax", serviceTaxData);
			serviceTaxDetailData.put("Status", "invalid");
			serviceTaxDetailData.put("apiResponse", "invalid");
			serviceTaxDetailData.put("status", "invalid");
			serviceTaxDetailData.put("apiCount", apiCount);
			return serviceTaxDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return serviceTaxDetailData;
	}
	
	
	public static JSONObject saveDL(JSONObject dlJSONObject, Integer apiCount) {
		JSONObject dlData = new JSONObject();
		JSONObject dlObj = new JSONObject();
		JSONObject dlcov = new JSONObject();
		JSONObject dlCovs = new JSONObject();
		JSONObject transportJSONArray = new JSONObject();
		String name = "", dob = "", nonTransportValidFrom = "", nonTransportValidTo = "", transportValidFrom = "",
				transportValidTo = "", issue_date = "", father = "", address = "", fatherName = "", cov = "",
				erromsg = "", issueAuth = "", cov1 = "", nonTransportStr = "", trasportStr = "", cov2 = "";
		String dlNtValdfrDt = "", dlNtValdtoDt = "", dlValidFrTr = "", dlValidToTr = "", dlHazvalidtill = "",
				dlHillvalidtill = "";
		try {
			name = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "name");
			dlObj.put("dlName", name);
			dob = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "dob");
			dlObj.put("dob", dob);
			issue_date = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "issue_date");
			dlObj.put("dlIssuedt", issue_date);
			fatherName = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "father/husband");
			dlObj.put("father", fatherName);
			address = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "address");
			dlObj.put("address", address);
			dlHazvalidtill = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "dlHazvalidtill");
			dlObj.put("dlHazvalidtill", dlHazvalidtill);
			dlHillvalidtill = (String) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "dlHillvalidtill");
			dlObj.put("dlHillvalidtill", dlHillvalidtill);
			transportJSONArray = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "validity");
			nonTransportStr = (String) KYCUtil.checkKeyAndGetValueFromJSON(transportJSONArray, "non-transport");
			if (nonTransportStr.length() > 0) {
				String tmp[] = nonTransportStr.split("to");
				System.out.println("Non-Transport DL Components : " + tmp.length + " For String " + nonTransportStr);
				if (tmp != null && tmp.length == 2) {
					dlNtValdfrDt = tmp[0];
					dlNtValdtoDt = tmp[1];
				} else {
					dlNtValdtoDt = nonTransportStr;
				}
			}
			dlObj.put("dlNtValdfrDt", dlNtValdfrDt);
			dlObj.put("dlNtValdtoDt", dlNtValdtoDt);
			trasportStr = (String) KYCUtil.checkKeyAndGetValueFromJSON(transportJSONArray, "transport");
			if (trasportStr.length() > 0) {
				String tmp[] = trasportStr.split("to");
				if (tmp != null && tmp.length == 2) {
					dlValidFrTr = tmp[0];
					dlValidToTr = tmp[1];
				} else {
					dlValidToTr = trasportStr;
				}
			}
			dlObj.put("dlValidFrTr", dlValidFrTr);
			dlObj.put("dlValidToTr", dlValidToTr);
			dlObj.put("dlStatus", "valid");
			dlData.put("dlobj", dlObj);
			dlcov = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(dlJSONObject, "cov_details");
			JSONArray covabbrv = new JSONArray();
			JSONArray vecatg = new JSONArray();
			JSONArray dcIssuedt = new JSONArray();
			Iterator<String> x = dlcov.keys();
			while (x.hasNext()) {
				String key1 = x.next();
				String value1 = dlcov.optString(key1);
				covabbrv.put(key1);
				vecatg.put(value1);
				dcIssuedt.put(value1);
			}
			dlCovs.put("covabbrv", covabbrv);
			dlCovs.put("vecatg", vecatg);
			dlCovs.put("dcIssuedt", dcIssuedt);
			dlCovs.put("status", "valid");
			dlData.put("dlCovs", dlCovs);
			dlData.put("status", "valid");
			dlData.put("apiCount", apiCount);
			return dlData;

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return dlData;
	}

	
	public static JSONObject saveInvalidDL(Integer apiCount){
		JSONObject dlData = new JSONObject();
		try {
			dlData.put("status", "invalid");
			dlData.put("errormsg", "invalid");
			dlData.put("apiCount", apiCount);
			return dlData;
	
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return dlData;
	}
	
	public static JSONObject saveElectricity(JSONObject electricityJSONObj, Integer apiCount) {
		JSONObject elData = new JSONObject();
		JSONObject eleData = new JSONObject();

		String consumerName = "", billNo = "", billDueDate = "", consumerNumber = "", billAmount = "",
				billIssueDate = "", mobileNo = "", amountPayable = "", totalAmount = "", address = "", email = "",
				billDate = "", amount = "";
		try {

			// fetching all the data from the api
			consumerNumber = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "consumer_number");
			eleData.put("consumer_number", consumerNumber);
			consumerName = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "consumer_name");
			// fetch the consumer Name
			eleData.put("consumer_name", consumerName);
			billNo = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "bill_no");
			// fetch the bill No
			eleData.put("bill_no", billNo);
			billDueDate = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "bill_due_date");
			// fetch the bill due date
			eleData.put("bill_due_date", billDueDate);
			billAmount = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "bill_amount");
			// fetch the bill amount
			eleData.put("bill_amount", billAmount);
			billIssueDate = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "bill_issue_date");
			// fetch the bill Issue date
			eleData.put("bill_issue_date", billIssueDate);
			mobileNo = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "mobile_number");
			// fetch the mobile number
			eleData.put("mobile_number", mobileNo);
			amount = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "amount");
			eleData.put("amount", amount);
			amountPayable = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "amount_payable");
			// fetch the amount payable
			eleData.put("amount_payable", amountPayable);
			totalAmount = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "total_amount");
			// fetch the total amount
			eleData.put("total_amount", totalAmount);
			address = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "address");
			// fetch the address
			eleData.put("address", address);
			email = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "email_address");
			// fetch the email
			eleData.put("email_address", email);
			billDate = (String) KYCUtil.checkKeyAndGetValueFromJSON(electricityJSONObj, "bill_date");
			// fetch the bill date
			eleData.put("bill_date", billDate);
			eleData.put("status", "valid");
			eleData.put("apiResponse", "valid");
			eleData.put("apiCount", apiCount);
			eleData.put("statusAsPerSource", "valid");
			elData.put("electricity", eleData);
			elData.put("status", "valid");
			elData.put("apiResponse", "valid");
			elData.put("statusAsPerSource", "valid");
			elData.put("apiCount", apiCount);
			return elData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return elData;
	}


	public static JSONObject saveInvalidElectricity(Integer apiCount){
			JSONObject eleData = new JSONObject();
			JSONObject elData = new JSONObject();
		try {
			eleData.put("status", "invalid");
			eleData.put("errormsg", "invalid");
			eleData.put("apiResponse", "invalid");
			// Says that this request is processed completely :DB status
			// for this request should be processed
			eleData.put("statusAsPerSource", "invalid");
			elData.put("electricity", eleData);
			elData.put("status", "invalid");
			elData.put("apiCount", apiCount);
			return elData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return elData;
	}
	
	public static JSONObject saveVoter(JSONObject voterIDDataJSON, Integer apiCount) {
		JSONObject vData = new JSONObject();
		JSONObject voterData = new JSONObject();
		Date dateTimeRequested = new Date();
		String age = "", assembly_constituency = "", district = "", epic = "", rln_name = "", rln_name_v1 = "",
				gender = "", name = "", name_v1 = "", polling_station = "", state = "", address = "", ac_no = "",
				part_no = "", sr_no = "", lastUpdatedAt = "", pc_name = "", dob = "", part_name = "", house_no = "",
				slno_inpart = "", st_code = "";
		try {
			rln_name_v1 = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "rln_name_v1");
			rln_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "rln_name");
			rln_name = rln_name + " " + rln_name_v1; // Merged both the
														// Names
														// Hindi(Regional
														// Language/English)
			vData.put("Father's/Husband's Name", rln_name); // Name both
															// in
															// Hindi/English
			name = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "name");
			name_v1 = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "name_v1");
			name = name + " " + name_v1;
			vData.put("Name", name);
			district = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "district");
			vData.put("District", district);
			age = KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "age").toString();
			vData.put("Age", age);
			assembly_constituency = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "ac_name");
			vData.put("Assembly Constituency", assembly_constituency);
			epic = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "epic_no");
			vData.put("EPIC No", epic);
			gender = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "gender");
			vData.put("gender", gender);
			polling_station = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "ps_name");
			vData.put("Polling Station", polling_station);
			state = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "state");
			vData.put("State", state);
			address = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "address");
			vData.put("address", address);
			ac_no = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "ac_no");
			vData.put("AC NO", ac_no);
			part_no = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "part_no");
			vData.put("PART NO", part_no);
			sr_no = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "section_no");
			vData.put("SR NO", sr_no);
			lastUpdatedAt = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "last_update");
			vData.put("lastUpdatedAt", lastUpdatedAt);
			pc_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "pc_name");
			vData.put("parliamentryConstituency", pc_name);
			dob = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "dob");
			vData.put("dob", dob);
			part_name = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "part_name");
			vData.put("partName", part_name);
			house_no = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "house_no");
			vData.put("house_no", house_no);
			slno_inpart = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "slno_inpart");
			vData.put("slno_inpart", slno_inpart);
			st_code = (String) KYCUtil.checkKeyAndGetValueFromJSON(voterIDDataJSON, "st_code");
			vData.put("st_code", st_code);
			vData.put("status", "valid");
			vData.put("statusAsPerSource", "valid");
			Date dateTimeReceived = new Date();
			voterData.put("dateTimeRequested", dateTimeRequested);
			voterData.put("dateTimeReceived", dateTimeReceived);
			voterData.put("status", "valid");
			voterData.put("statusAsPerSource", "valid");
			voterData.put("voter", vData);
			voterData.put("apiCount", apiCount);
			return voterData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return voterData;
	}

	
	public static JSONObject saveInvalidVoter(Integer apiCount,Date dateTimeRequested){
			JSONObject vData = new JSONObject();
			JSONObject voterData = new JSONObject();
		try {
			vData.put("status", "invalid");
			vData.put("statusAsPerSource", "No Record Found");
			vData.put("apiCount", apiCount);
			voterData.put("voter", vData);
			Date dateTimeReceived = new Date();
			voterData.put("dateTimeRequested", dateTimeRequested);
			voterData.put("dateTimeReceived", dateTimeReceived);
			voterData.put("statusAsPerSource", "No Record Found");
			voterData.put("status", "invalid");
			voterData.put("apiCount", apiCount);
			return voterData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return voterData;
	}
	
	public static JSONObject saveIEC(JSONObject iecJSONResult, Integer apiCount) {
		JSONObject iecData = new JSONObject();
		JSONObject iecDetailData = new JSONObject();
		String name = "", address = "", iecCode = "", no_of_branches = "", iec_status = "", pan = "";
		try {
			name = (String) KYCUtil.checkKeyAndGetValueFromJSON(iecJSONResult, "Name");
			iecData.put("Name", name);
			address = (String) KYCUtil.checkKeyAndGetValueFromJSON(iecJSONResult, "Address");
			iecData.put("Address", address);
			iecCode = (String) KYCUtil.checkKeyAndGetValueFromJSON(iecJSONResult, "IE Code");
			iecData.put("iec", iecCode);
			no_of_branches = (String) KYCUtil.checkKeyAndGetValueFromJSON(iecJSONResult, "No_of_branches");
			iecData.put("branches", no_of_branches);
			iec_status = (String) KYCUtil.checkKeyAndGetValueFromJSON(iecJSONResult, "IEC Status");
			iecData.put("statusAsPerSource", iec_status);
			pan = (String) KYCUtil.checkKeyAndGetValueFromJSON(iecJSONResult, "PAN");
			iecData.put("PAN", pan);
			iecData.put("status", "valid");
			iecData.put("apiResponse", "valid");
			iecData.put("statusAsPerSource", iec_status);
			iecData.put("apiCount", apiCount);
			iecDetailData.put("iec", iecData);
			iecDetailData.put("apiCount", apiCount);
			iecDetailData.put("status", "valid");
			iecDetailData.put("apiResponse", "valid");
			iecDetailData.put("statusAsPerSource", iec_status);
			return iecDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return iecDetailData;
	}
	/*Utility Method in order to save the detail of the Result which is 200 but the response body returns no records found*/
	
	public static JSONObject saveInvalidIEC(Integer apiCount)
	{
		JSONObject iecData = new JSONObject();
		JSONObject iecDetailData = new JSONObject();
		try {
			iecData.put("status", "invalid");
			iecData.put("apiResponse", "invalid");
			iecData.put("statusAsPerSource", "IEC Code is Invalid");
			iecDetailData.put("iec", iecData);
			iecDetailData.put("apiCount", apiCount);
			iecDetailData.put("status", "invalid");
			iecDetailData.put("apiResponse", "invalid");
			iecDetailData.put("statusAsPerSource", "IEC Code is Invalid");
			return iecDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return iecDetailData;
	}
	
	public static String getKey() {
		String key = "\"12ed34fgddfg\"";
		return key;
	}
	
	public static String getAzureCloudKey(){
		final String storageConnectionString =
			    "DefaultEndpointsProtocol=https;" +
			    "AccountName=totalkycwebappcontainer;" +
			    "AccountKey=RrQNdlrJ4IPqXas/LqL9lfamBlSsFXnCRfs7pbJ9GrN/sOsw8KC+IqPdZxpN0+hu0UMzMQBsRlfukpAVGlbRmw==";
		return storageConnectionString;
	}
}