/*package com.karza.kyc.util;

import com.karza.kyc.model.*;
import com.karza.kyc.service.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Date;
import java.util.List;


*//**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/30/16
 * Time: 12:19 PM
 * To change this template use File | Settings | File Templates.
 *//*

public class DLHelper_Bak {

    public static DrivingLicenceMaster saveDL(JSONObject dlApiResponse, CustomerEntity customerEntity, String dl_document, DrivingLicenceMasterService dlMasterService, DrivingLicenceCOVService dlCOVService) throws Exception {
        DrivingLicenceMaster dl;
        DrivingLicenceMaster existingDl = dlMasterService.getByCustomerId(customerEntity.getId());
        JSONObject dlData = new JSONObject();
        String api_status = dlApiResponse.get("status").toString();
        dl = new DrivingLicenceMaster(dl_document, customerEntity.getId(), api_status, dlApiResponse);
        if (existingDl == null) {
            dl.setCreatedAt(new Date());
            dl = dlMasterService.save(dl);
        } else {
            dl.setId(existingDl.getId());
            dlMasterService.update(dl);
            dl = existingDl;
        }

        DrivingLicenceCOV dlCOV;
        List<DrivingLicenceCOV> existingDLCOVs = dlCOVService.getByLicence(dl.getId());
        if (existingDLCOVs != null) {
            for (int i = 0; i < existingDLCOVs.size(); i++) {
                DrivingLicenceCOV dlCOV1 = existingDLCOVs.get(i);
                dlCOVService.delete(dlCOV1);
            }
        }
        if (dlApiResponse.has("dlCovs")) {
            String dataDlcov_Obj = dlApiResponse.get("dlCovs").toString();
            Object json = new JSONTokener(dataDlcov_Obj).nextValue();
            if (json instanceof JSONObject) {
                JSONObject dlCovDataJson = (JSONObject) dlApiResponse.get("dlCovs");
                //DrivingLicenceCOV dl_cov = new DrivingLicenceCOV(dl.getId(), dlCovDataJson1);
                DrivingLicenceCOV dl_cov = new DrivingLicenceCOV(dl.getId(), dlApiResponse);
                dl_cov.setCreatedAt(new Date());
                dlCOVService.save(dl_cov);
            } else if (json instanceof JSONArray) {
                JSONArray dl_covDatas = new JSONArray();
                dl_covDatas = (JSONArray) dlApiResponse.get("dlCovs");
                for (int index = 0; index < dl_covDatas.length(); index++) {
                    JSONObject jsonObject = (JSONObject) dl_covDatas.get(index);
                    DrivingLicenceCOV dl_cov = new DrivingLicenceCOV(dl.getId(), jsonObject);
                    dl_cov.setCreatedAt(new Date());
                    dlCOVService.save(dl_cov);
                }
            }
        }
        return dl;
    }

    public static void saveCst(JSONObject cstApiResponse, CustomerEntity customerEntity, String cst_document, CSTService cstService, CSTFrequencyService cstFrequencyService) throws Exception {
        CST cst;
        CST existingCst = cstService.getByCustomerId(customerEntity.getId());
        JSONObject cstData = new JSONObject();
        if (cstApiResponse.has("vatData")) {
            cstData = cstApiResponse.getJSONObject("vatData");
        }
        String api_status = "invalid";
        if (cstData.has("api_status")) {
            api_status = cstData.get("api_status").toString();
        }
        cst = new CST(cst_document, customerEntity.getId(), api_status, cstData);
        if (existingCst == null) {
            cst.setCreatedAt(new Date());
            cst = cstService.save(cst);
        } else {
            cst.setId(existingCst.getId());
            cstService.update(cst);
            cst = existingCst;
        }

        CSTFrequency cstFrequency;
        List<CSTFrequency> existingCstFrequencies = cstFrequencyService.getByCstId(cst.getId());
        if (existingCstFrequencies != null) {
            for (int i = 0; i < existingCstFrequencies.size(); i++) {
                CSTFrequency cstFrequency1 = existingCstFrequencies.get(i);
                cstFrequencyService.delete(cstFrequency1);
            }
        }
        JSONArray frequencies = new JSONArray();
        if (cstApiResponse.has("frequency")) {
            frequencies = cstApiResponse.getJSONArray("frequency");
        }
        for (int i = 0; i < frequencies.length(); i++) {
            JSONObject frequency = new JSONObject();
            frequency = frequencies.getJSONObject(i);
            cstFrequency = new CSTFrequency(cst.getId(), frequency);
            cstFrequency.setCreatedAt(new Date());
            cstFrequencyService.save(cstFrequency);
        }
    }

    public static void savePt(JSONObject ptApiResponse, CustomerEntity customerEntity, String pt_document, ProfessionalTaxService professionalTaxService, ProfessionalTaxFrequencyService professionalTaxFrequencyService) throws Exception {
        ProfessionalTax professionalTax;
        ProfessionalTax existingProfessionalTax = professionalTaxService.getByCustomerId(customerEntity.getId());
        JSONObject ptData = new JSONObject();
        if (ptApiResponse.has("vatData")) {
            ptData = ptApiResponse.getJSONObject("vatData");
        }
        String api_status = "invalid";
        if (ptData.has("api_status")) {
            api_status = ptData.get("api_status").toString();
        }
        professionalTax = new ProfessionalTax(pt_document, customerEntity.getId(), api_status, ptData);
        if (existingProfessionalTax == null) {
            professionalTax.setCreatedAt(new Date());
            professionalTax = professionalTaxService.save(professionalTax);
        } else {
            professionalTax.setId(existingProfessionalTax.getId());
            professionalTaxService.update(professionalTax);
            professionalTax = existingProfessionalTax;
        }

        ProfessionalTaxFrequency professionalTaxFrequency;
        List<ProfessionalTaxFrequency> existingPtFrequencies = professionalTaxFrequencyService.getByProfessionalTaxId(professionalTax.getId());
        if (existingPtFrequencies != null) {
            for (int i = 0; i < existingPtFrequencies.size(); i++) {
                ProfessionalTaxFrequency professionalTaxFrequency1 = existingPtFrequencies.get(i);
                professionalTaxFrequencyService.delete(professionalTaxFrequency1);
            }
        }
        JSONArray frequencies = new JSONArray();
        if (ptApiResponse.has("frequency")) {
            frequencies = ptApiResponse.getJSONArray("frequency");
        }
        for (int i = 0; i < frequencies.length(); i++) {
            JSONObject frequency = new JSONObject();
            frequency = frequencies.getJSONObject(i);
            professionalTaxFrequency = new ProfessionalTaxFrequency(professionalTax.getId(), frequency);
            professionalTaxFrequency.setCreatedAt(new Date());
            professionalTaxFrequencyService.save(professionalTaxFrequency);
        }
    }
}
*/