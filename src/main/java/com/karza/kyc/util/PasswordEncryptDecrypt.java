package com.karza.kyc.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class PasswordEncryptDecrypt {
	static Cipher cipher;
	
	public static String SecretKey(){
		String secretKey = "KarzaKYC@0123456789!#$%&*@(+-)omkalokravugR/51*&%@!0270VaArAnVeAkSuZe";
		return secretKey;
	}
	
	/* Method for Password Encryption , this method is not being used anymore but useful one*/
	public static String encrypt(String plainText, SecretKey secretKey) throws Exception {
		byte[] plainTextByte = plainText.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		Base64.Encoder encoder = Base64.getEncoder();
		String encryptedText = encoder.encodeToString(encryptedByte);
		return encryptedText;
	}

	/* Method to Decrypt the Password ,this method is not being used anymore,but useful one*/
	public static String decrypt(String encryptedText, SecretKey secretKey) throws Exception {
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] encryptedTextByte = decoder.decode(encryptedText);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
	}

	/*MD5 Algorithm is being used to store the value in the Datbase*/
	public static String md5Hash(String message) {
        String md5 = "";
        if(null == message) 
            return null;
         String salt = PasswordEncryptDecrypt.SecretKey();
        message = message+salt;//adding a salt to the string before it gets hashed.
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");//Create MessageDigest object for MD5
            digest.update(message.getBytes(), 0, message.length());//Update input string in message digest
            md5 = new BigInteger(1, digest.digest()).toString(16);//Converts message digest value in base 16 (hex)
  
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }
	
}
