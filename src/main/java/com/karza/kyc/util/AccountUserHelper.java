package com.karza.kyc.util;

import com.karza.kyc.model.AccountUser;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Fallon on 4/26/2016.
 */
public class AccountUserHelper {
    public AccountUser getAccountUser(JSONObject accountUserData) throws JSONException {
        String firstName = "";
        String lastName = "";
        String phoneNumber = "";
        String altPhone = "";
        String userName = "";
        String accountId = "";
        if (accountUserData.has("firstName")) {
            firstName = accountUserData.get("firstName").toString();
        }
        if (accountUserData.has("lastName")) {
            lastName = accountUserData.get("lastName").toString();
        }
        if (accountUserData.has("phoneNumber")) {
            phoneNumber = accountUserData.get("phoneNumber").toString();
        }
        if (accountUserData.has("altPhone")) {
            altPhone = accountUserData.get("altPhone").toString();
        }
        String email = accountUserData.get("email").toString();
        if (accountUserData.has("userName")) {
            userName = accountUserData.get("userName").toString();
        }
        if (accountUserData.has("accountId")) {
            accountId = accountUserData.get("accountId").toString();
        }

        AccountUser accountUser = new AccountUser();
        accountUser.setEmail(email);
        accountUser.setFirstName(firstName);
        accountUser.setPhoneNumber(phoneNumber);
        accountUser.setAltPhone(altPhone);
        accountUser.setLastName(lastName);
        accountUser.setAccountId(Long.parseLong(accountId));
        accountUser.setUserName(userName);
        return accountUser;
    }
}