package com.karza.kyc.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.karza.kyc.dao.impl.UserSessionDetailDaoImpl;
import com.karza.kyc.decaptcher.APIMain;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.validators.CinValidator;
import com.karza.kyc.validators.DocumentValidatorException;
import com.karza.kyc.validators.FcrnValidator;
import com.karza.kyc.validators.FllpinValidator;
import com.karza.kyc.validators.LlpinValidator;

import okhttp3.MediaType;
import okhttp3.OkHttpClient.Builder;

public class ApiCallHelper {
	static final Logger LOG = LoggerFactory.getLogger(ApiCallHelper.class);

	public static String postToURL(String url, String message, DefaultHttpClient httpClient)
			throws IOException, RuntimeException {
		HttpPost postRequest = new HttpPost(url);

		StringEntity input = new StringEntity(message);
		input.setContentType("application/json");
		postRequest.setEntity(input);

		HttpResponse response = httpClient.execute(postRequest);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

		String output;
		StringBuffer totalOutput = new StringBuffer();
		while ((output = br.readLine()) != null) {
			totalOutput.append(output);
		}
		return totalOutput.toString();
	}

	private static boolean validateAadhaarStatus(String aadharResponse, List<String> subAuAError, List<String> ref)
			throws JSONException {
		JSONObject jsonObject = new JSONObject(aadharResponse);
		// Save the Reference Code
		String refField = "aadhaar-reference-code";
		if (jsonObject.has("success")) {
			if (jsonObject.getBoolean("success") == true) {
				if (jsonObject.has(refField)) {
					ref.set(0, jsonObject.getString(refField));
				}
				return true;
			} else {
				if (jsonObject.has(refField)) {
					ref.set(0, jsonObject.getString(refField));
				}
				subAuAError.set(0, "Invalid Aadhaar Number");
				if (jsonObject.has("aadhaar-status-code")) {
					String auaStatus = jsonObject.getString("aadhaar-status-code");
					if (auaStatus.trim().equals("100")) {
						subAuAError.set(0, "Name or DOB Mismatch");
					} else if (auaStatus.trim().equals("997")) {
						subAuAError.set(0, "Aadhaar Suspended");
					} else if (auaStatus.trim().equals("561")) {
						subAuAError.set(0, "Request Expired");
					} else if (auaStatus.trim().equals("200")) {
						subAuAError.set(0, "Address Mismatch");
					}
				} else if (jsonObject.has("aua-status-code")) {
					String auaStatus = jsonObject.getString("aua-status-code");
					if (auaStatus.trim().equals("462")) {
						subAuAError.set(0, "Invalid Aadhaar Number");
					}
				}
				return false;
			}
		} else {
			subAuAError.set(0, "Aadhaar Sub-AUA couldn't process this request");
			if (jsonObject.has(refField)) {
				ref.set(0, jsonObject.getString(refField));
			}
			return false;
		}
	}

	private static String prepareAadharJSONString(String aadharID, String adhaarName, String dob, String pincode,
			String state) {
		String aadharJSONRequestStr = "{" + "\"aadhaar-id\":\"" + aadharID + "\","
				+ "\"location\": {\"type\": \"pincode\",\"pincode\": \"560067\"},"
				+ "\"modality\": \"demo\",\"certificate-type\": \"preprod\"," + "\"demographics\": {" + "\"name\": {"
				+ "\"matching-strategy\": \"exact\"," + "\"name-value\": \"" + adhaarName + "\"" + "}," + "\"dob\":  {"
				+ "\"format\":\"YYYYMMDD\"," + "\"dob-value\":\"" + dob + "\"" + "},"
				+ "\"address-format\": \"structured\"," + "\"address-structured\":{" + "\"state\":\"" + state + "\","
				+ "\"pincode\":\"" + pincode + "\"" + "}" + "}" + "}";
		LOG.info("JSON String for Aadhar Verification has been Created");
		return aadharJSONRequestStr;
	}

	public static Boolean getAadharStatus(String aadharID, String adhaarName, String dob, String state, String pincode,
			List<String> subAuAError, List<String> khoslaRef) throws JSONException, IOException {

		final okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json; charset=utf-8");
		String aadharRequestStr = prepareAadharJSONString(aadharID, adhaarName, dob, pincode, state);
		System.out.println(aadharRequestStr);
		okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, aadharRequestStr);

		Builder b = new Builder();
		b.readTimeout(120, TimeUnit.SECONDS);
		b.writeTimeout(120, TimeUnit.SECONDS);
		okhttp3.OkHttpClient client = b.build();
		okhttp3.Request request = new okhttp3.Request.Builder().url("http://localhost:9091/auth/raw").post(body)
				.build();
		okhttp3.Response response = client.newCall(request).execute();
		if (response == null || response.code() != 200 || response.body() == null) {
			subAuAError.set(0, "CouldNot Connect to Aadhaar Authentication Services");
			// Need to Handle the Retries
			return false;
		} else {
			return validateAadhaarStatus(response.body().string(), subAuAError, khoslaRef);
		}
	}

	/* Method to get the IEC Code Status */
	public static JSONObject getIecStatus(String iec, Integer apiCount) throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject iecData = new JSONObject();
		JSONObject iecDetailData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		iec = "\"" + iec + "\"";
		String value = "{\"iec\":" + iec + "," + "\"key\":" + key + "" + "}";
		iecDetailData.put("status", "pending");
		iecDetailData.put("apiResponse", "invalid");
		iecDetailData.put("statusAsPerSource", "invalid");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/iec").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object iecResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (iecResult == null || iecResult.equals("No Record Found") || iecResult.equals("NO RECORD FOUND")) {
					iecDetailData = ApiCallHelperUtil.saveInvalidIEC(apiCount);
					return iecDetailData;
				}
				if (jsonObject != null && !iecResult.equals("No Record Found")) {
					JSONObject iecJSONResult = new JSONObject();
					iecJSONResult = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
					iecDetailData = ApiCallHelperUtil.saveIEC(iecJSONResult, apiCount);
					return iecDetailData;
				}
				return iecDetailData;
			}
			if (response.code() >= 500) {
				iecDetailData.put("apiCount", apiCount);
				if (apiCount >= 5) {
					iecDetailData.put("status", "invalid");
					iecDetailData.put("apiResponse", "invalid");
					iecDetailData.put("statusAsPerSource", "IEC Code is Invalid");
				}
				return iecDetailData;
			}
			if (response.code() == 400 || response.code() == 401) {
				iecDetailData.put("apiCount", apiCount);
				iecDetailData.put("status", "invalid");
				iecDetailData.put("apiResponse", "invalid");
				iecDetailData.put("statusAsPerSource", "IEC Code is Invalid");
				return iecDetailData;
			}
			return iecDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return iecDetailData;
	}

	/* Method to get the ServiceTax Status */
	public static JSONObject getServiceTaxStatus(String service_tax_document, Integer apiCount)
			throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject serviceTaxDetailData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		service_tax_document = "\"" + service_tax_document + "\"";
		String value = "{\"code\":" + service_tax_document + "," + "\"key\":" + key + "" + "}";
		serviceTaxDetailData.put("status", "pending");
		serviceTaxDetailData.put("apiResponse", "invalid");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/cbec").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object serviceTaxJSONData = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (serviceTaxJSONData == null || serviceTaxJSONData.equals("No Record Found")
						|| serviceTaxJSONData.equals("NO RECORD FOUND")) {
					serviceTaxDetailData = ApiCallHelperUtil.saveInvalidServiceTax(apiCount);
					return serviceTaxDetailData;
				}
				if (jsonObject != null && !serviceTaxJSONData.equals("No Record Found")) {
					JSONObject serviceTaxJSON = new JSONObject();
					serviceTaxJSON = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
					serviceTaxDetailData = ApiCallHelperUtil.saveServiceTax(serviceTaxJSON, apiCount);
					return serviceTaxDetailData;
				}
				return serviceTaxDetailData;
			}
			if (response.code() >= 500) {
				serviceTaxDetailData.put("apiCount", apiCount);
				if (apiCount >= 5) {
					serviceTaxDetailData.put("status", "invalid");
					serviceTaxDetailData.put("errormsg", "invalid");
				}
				return serviceTaxDetailData;
			}
			if (response.code() == 400 || response.code() == 401) {
				serviceTaxDetailData.put("apiCount", apiCount);
				serviceTaxDetailData.put("status", "invalid");
				serviceTaxDetailData.put("errormsg", "invalid");
				return serviceTaxDetailData;
			}
			return serviceTaxDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return serviceTaxDetailData;
	}

	/* Method to get the Excise Data */
	public static JSONObject getExcisetatus(String excise_document, int apiCount) throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject exciseDetailData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		excise_document = "\"" + excise_document + "\"";
		String value = "{\"code\":" + excise_document + "," + "\"key\":" + key + "" + "}";
		exciseDetailData.put("status", "pending");
		exciseDetailData.put("apiResponse", "invalid");
		// exciseDetailData.put("statusAsPerSource", "invalid");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/cbec").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object exciseJSONResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (exciseJSONResult == null || exciseJSONResult.equals("No Record Found")
						|| exciseJSONResult.equals("NO RECORD FOUND")) {
					exciseDetailData = ApiCallHelperUtil.saveInvalidExcise(apiCount);
					return exciseDetailData;
				}
				if (jsonObject != null && !exciseJSONResult.equals("No Record Found")) {
					JSONObject exciseJSONData = new JSONObject();
					exciseJSONData = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
					exciseDetailData = ApiCallHelperUtil.saveExcise(exciseJSONData, apiCount);
					return exciseDetailData;
				}
				return exciseDetailData;
			}
			if (response.code() >= 500) {
				exciseDetailData.put("apiCount", apiCount);
				if (apiCount >= 5) {
					exciseDetailData.put("status", "invalid");
					exciseDetailData.put("errormsg", "invalid");
				}
				return exciseDetailData;
			}
			if (response.code() == 400 || response.code() == 401) {
				exciseDetailData.put("apiCount", apiCount);
				exciseDetailData.put("status", "invalid");
				exciseDetailData.put("errormsg", "invalid");
				return exciseDetailData;
			}
			return exciseDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return exciseDetailData;
	}

	/* Method to verify and fetch the PAN data */
	public static JSONObject getPanStatus(String pan, Integer apiCount) throws JSONException, IOException {
		LOG.warn("Came to Verify the Pan Data with Following Pan Number -> ", pan);
		JSONObject jsonObject = new JSONObject();
		JSONObject panDetailData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		pan = "\"" + pan + "\"";
		String value = "{\"pan\":" + pan + "," + "\"key\":" + key + "" + "}";
		//panDetailData.put("status", "pending");
		panDetailData.put("apiResponse", "invalid");
		panDetailData.put("statusAsPerSource", "invalid");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/pan").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object panResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (panResult == null || panResult.equals("No Record Found") || panResult.equals("NO RECORD FOUND")) {
					panDetailData = ApiCallHelperUtil.saveInvalidPan(apiCount);
					LOG.trace("Returning the Invalid PAN data",panDetailData);
					return panDetailData;
				}
				if (jsonObject != null && !panResult.equals("No Record Found")) {
					JSONObject panJSONObject = new JSONObject();
					panJSONObject = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
					panDetailData = ApiCallHelperUtil.savePan(panJSONObject, apiCount);
					LOG.trace("Returning the valid PAN data",panDetailData);
					return panDetailData;
				}
				return panDetailData;
			}
			if (response.code() >= 500) {
				panDetailData.put("apiCount", apiCount);
				if (apiCount >= 5) {
				//	panDetailData.put("status", "invalid");
					panDetailData.put("apiResponse", "invalid");
					panDetailData.put("statusAsPerSource", "invalid");
				}
				return panDetailData;
			}
			if (response.code() == 400 || response.code() == 401) {
				panDetailData.put("apiCount", apiCount);
				//panDetailData.put("status", "invalid");
				panDetailData.put("apiResponse", "invalid");
				panDetailData.put("statusAsPerSource", "invalid");
				return panDetailData;
			}
			return panDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return panDetailData;
	}

	public static JSONObject getItrStatus(String acknowledgmentNumber, String assessmentYear, String panNumber,
			String statusAsPerPan, String statusAsPerAck) throws IOException, JSONException {
		JSONObject itrData = new JSONObject();
		itrData.put("status", "pending");
		FirefoxDriver driver = null;
		String statusAsPerSourcePan = "";
		String statusAsPerSourceAcknowledment = "";
		if (StringUtils.containsIgnoreCase(statusAsPerPan, "Invalid PAN")) {
			try {
				Document doc;
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				String site_address = "https://incometaxindiaefiling.gov.in/e-Filing/Services/ITRVStatusLink.html";

				driver.navigate().to(site_address);
				byte[] arrScreenForAck = driver.getScreenshotAs(OutputType.BYTES);
				BufferedImage imageScreenForAck = ImageIO.read(new ByteArrayInputStream(arrScreenForAck));
				WebElement capForAck = driver.findElementById("captchaImg");
				Dimension capDimensionForAck = capForAck.getSize();
				Point capLocationForAck = capForAck.getLocation();
				BufferedImage imgCapForAck = imageScreenForAck.getSubimage(capLocationForAck.x, capLocationForAck.y,
						capDimensionForAck.width, capDimensionForAck.height);
				ByteArrayOutputStream osForAck = new ByteArrayOutputStream();
				ImageIO.write(imgCapForAck, "png", osForAck);
				String imgStringForAck = Base64.encodeBase64String(osForAck.toByteArray());
				byte[] dataForAck = Base64.decodeBase64(imgStringForAck);
				/*
				 * try (OutputStream stream = new FileOutputStream("/" +
				 * acknowledgmentNumber + ".bmp")) { stream.write(dataForAck); }
				 * String textForAck = APIMain.decodeCaptcha("/" +
				 * acknowledgmentNumber + ".bmp");
				 */
				// File file11 = new File("C:\\Users\\Varun_Prakash\\captcha");
				File file11 = new File("C:\\Users\\Administrator\\captcha");
				try (OutputStream stream = new FileOutputStream(file11 + "/acknowledgmentNumber1.bmp")) {
					// stream.write(data);
					// }
					// String path = "C:\\Users\\Varun_Prakash\\captcha";
					String path = "C:\\Users\\Administrator\\captcha";
					File newFile = new File(path + "\\" + acknowledgmentNumber + ".bmp");
					OutputStream outputStream = new FileOutputStream(newFile);
					outputStream.write(dataForAck);
					String text = APIMain.decodeCaptcha(newFile.toString().toUpperCase());

					driver.findElement(By.id("ITR-V-Status_chooseOptionackNum")).click();
					driver.findElementByXPath("//input[@name='itrvStatusDetails.acknowledgementNumber']")
							.sendKeys(acknowledgmentNumber);
					driver.findElementByXPath("//input[@name='captchaCode']").sendKeys(text);
					driver.findElementByXPath("//input[@type='submit']").click();
					String bodyForAck = driver.getPageSource();
					doc = Jsoup.parse(bodyForAck);
					Element detdispForAck = doc.select("#staticContentsUrl").first();
					Elements tableDataForAck = detdispForAck.select("table");
					Elements trsForAck = tableDataForAck.get(0).select("tr");
					statusAsPerSourceAcknowledment = trsForAck.get(0).text();

					if (!statusAsPerSourceAcknowledment.isEmpty() || !statusAsPerSourceAcknowledment.trim().isEmpty()) {
						if (statusAsPerSourceAcknowledment.contains(
								"e-Return for this Assessment Year or Acknowledgement Number has been Digitally signed")) {
							statusAsPerSourceAcknowledment = "e-Return has been Digitally signed";
						}
					}

					if (statusAsPerSourceAcknowledment.isEmpty() || statusAsPerSourceAcknowledment.trim().isEmpty()) {
						try {
							statusAsPerSourceAcknowledment = trsForAck.get(2).select("table").get(1).select("tr").get(0)
									.select("div").get(0).text();
							if (!statusAsPerSourceAcknowledment.isEmpty()
									|| !statusAsPerSourceAcknowledment.trim().isEmpty()) {
								if (statusAsPerSourceAcknowledment
										.contains("Please enter a valid Acknowledgement Number")) {
									statusAsPerSourceAcknowledment = "Invalid Acknowlwdgement number";
								} else {
									statusAsPerSourceAcknowledment = "";
								}
							}
						} catch (Exception e) {
							System.out.println("Something went wrong in Itr api => :" + e);
						}
					}
					if (!statusAsPerSourceAcknowledment.isEmpty() || !statusAsPerSourceAcknowledment.trim().isEmpty()) {
						if (statusAsPerSourceAcknowledment
								.contains("No e-Return has been filed for this PAN / Acknowledgement number")) {
							statusAsPerSourceAcknowledment = "No e-return has been filed.";
						}
					}

					if (statusAsPerSourceAcknowledment.isEmpty() || statusAsPerSourceAcknowledment.trim().isEmpty()) {
						boolean captchaFailed = false;
						if (statusAsPerSourceAcknowledment.isEmpty()) {
							try {
								statusAsPerSourceAcknowledment = trsForAck.select(".error").first().text();
								if (!statusAsPerSourceAcknowledment.isEmpty() && statusAsPerSourceAcknowledment
										.contains("Please enter the number as appearing in the Image.")) {
									captchaFailed = true;
									statusAsPerSourceAcknowledment = "";
								} else if (!statusAsPerSourceAcknowledment.isEmpty() && statusAsPerSourceAcknowledment
										.contains("Please enter the code as appearing in the Image.")) {
									captchaFailed = true;
									statusAsPerSourceAcknowledment = "";
								}
							} catch (Exception e) {
								System.out.println("Something went wrong in Itr api => :" + e);
							}
						}
						if (captchaFailed) {
							itrData.put("statusAsPerSourceAcknowledment", statusAsPerSourceAcknowledment);
							itrData.put("statusAsPerSourcePan", statusAsPerPan);
							itrData.put("status", "pending");
							return itrData;
						}
					}

					if ((!statusAsPerSourceAcknowledment.isEmpty()
							|| !statusAsPerSourceAcknowledment.trim().isEmpty())) {
						itrData.put("status", "invalid");
						itrData.put("statusAsPerSourcePan", statusAsPerPan);
						itrData.put("statusAsPerSourceAcknowledment", statusAsPerSourceAcknowledment);
					} else {
						itrData.put("statusAsPerSourcePan", statusAsPerPan);
						itrData.put("statusAsPerSourceAcknowledment", statusAsPerSourceAcknowledment);
					}
					return itrData;
				}
			} catch (Exception e) {
				System.out.println("Something went wrong in pancard api => :" + e);
			} finally {
				if (driver != null) {
					driver.close();
				}
			}
			return itrData;
		} else {
			if (StringUtils.containsIgnoreCase(statusAsPerAck, "Invalid Acknowledgement Number")) {
				try {
					driver = new FirefoxDriver();
					driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
					String site_address = "https://incometaxindiaefiling.gov.in/e-Filing/Services/ITRVStatusLink.html";
					// Status for Pan and assessment year
					driver.navigate().to(site_address);
					byte[] arrScreen = driver.getScreenshotAs(OutputType.BYTES);
					BufferedImage imageScreen = ImageIO.read(new ByteArrayInputStream(arrScreen));
					WebElement cap = driver.findElementById("captchaImg");
					Dimension capDimension = cap.getSize();
					Point capLocation = cap.getLocation();
					BufferedImage imgCap = imageScreen.getSubimage(capLocation.x, capLocation.y, capDimension.width,
							capDimension.height);
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					ImageIO.write(imgCap, "png", os);
					String imgString = Base64.encodeBase64String(os.toByteArray());
					byte[] data = Base64.decodeBase64(imgString);
					/*
					 * try (OutputStream stream = new FileOutputStream("/" +
					 * acknowledgmentNumber + ".bmp")) { stream.write(data); }
					 * String text = APIMain.decodeCaptcha("/" +
					 * acknowledgmentNumber + ".bmp");
					 */
					// File file11 = new
					// File("C:\\Users\\Varun_Prakash\\captcha");
					File file11 = new File("C:\\Users\\Administrator\\captcha");
					try (OutputStream stream = new FileOutputStream(file11 + "/acknowledgmentNumber2.bmp")) {
						// stream.write(data);
						// }
						// String path = "C:\\Users\\Varun_Prakash\\captcha";
						String path = "C:\\Users\\Administrator\\captcha";
						File newFile = new File(path + "\\" + acknowledgmentNumber + ".bmp");
						OutputStream outputStream = new FileOutputStream(newFile);
						outputStream.write(data);
						String text = APIMain.decodeCaptcha(newFile.toString());
						driver.findElementByXPath("//input[@name='itrvStatusDetails.pan']").sendKeys(panNumber);
						WebElement mySelectElm = driver
								.findElement(By.id("ITR-V-Status_itrvStatusDetails_assessmentYear"));
						Select mySelect = new Select(mySelectElm);
						mySelect.selectByVisibleText(assessmentYear);

						Select entity_type = new Select(
								driver.findElement(By.id("ITR-V-Status_itrvStatusDetails_assessmentYear")));
						if (assessmentYear.isEmpty() || assessmentYear.trim().isEmpty()) {
							entity_type.selectByValue("-1");
						} else {
							entity_type.selectByVisibleText(assessmentYear);
						}
						driver.findElementByXPath("//input[@name='captchaCode']").sendKeys(text);
						driver.findElementByXPath("//input[@type='submit']").click();
						String body = driver.getPageSource();
						Document doc;
						doc = Jsoup.parse(body);
						Element detdisp = doc.select("#staticContentsUrl").first();
						Elements tableData = detdisp.select("table");
						Elements trs = tableData.get(0).select("tr");
						statusAsPerSourcePan = trs.get(0).text();

						if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							try {
								statusAsPerSourcePan = trs.get(3).select("td").select("div").get(0).text();
								if (statusAsPerSourcePan.contains("Invalid PAN. Please retry")) {
									statusAsPerSourcePan = "Invalid PAN. ITR-V can not be verified based on PAN.";
								} else {
									statusAsPerSourcePan = "";
								}
							} catch (Exception e) {
								System.out.println("Something went wrong in Itr api => :" + e);
							}
						} else if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							try {
								statusAsPerSourcePan = trs.select(".error").first().text();

								if (statusAsPerSourcePan.contains("Please select the Assessment Year")) {
									statusAsPerSourcePan = "Assessment year is invalid. ITR-V can not be verified based on assessment year.";
								} else {
									statusAsPerSourcePan = "";
								}
							} catch (Exception e) {
								System.out.println("Something went wrong in Itr api => :" + e);
								statusAsPerSourcePan = "";
							}
						} else if (statusAsPerSourcePan.contains("Please select the Assessment Year")) {
							statusAsPerSourcePan = "Assessment year is invalid. ITR-V can not be verified based on assessment year.";
						} else if (!statusAsPerSourcePan.isEmpty() || !statusAsPerSourcePan.trim().isEmpty()) {
							if (statusAsPerSourcePan.contains(
									"e-Return for this Assessment Year or Acknowledgement Number has been Digitally signed")) {
								statusAsPerSourcePan = "e-Return has been Digitally signed";
							}
						} else if (!statusAsPerSourcePan.isEmpty() || !statusAsPerSourcePan.trim().isEmpty()) {
							if (statusAsPerSourcePan
									.contains("No e-Return has been filed for this PAN / Acknowledgement number")) {
								statusAsPerSourcePan = "No e-return has been filed.";
							}
						} else if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							boolean captchaFailed = false;
							if (statusAsPerSourcePan.isEmpty()) {
								try {
									statusAsPerSourcePan = trs.select(".error").first().text();
									if (!statusAsPerSourcePan.isEmpty() && statusAsPerSourcePan
											.contains("Please enter the number as appearing in the Image.")) {
										captchaFailed = true;
										statusAsPerSourcePan = "";
									} else if (!statusAsPerSourcePan.isEmpty() && statusAsPerSourcePan
											.contains("Please enter the code as appearing in the Image.")) {
										captchaFailed = true;
										statusAsPerSourcePan = "";
									}
								} catch (Exception e) {
									System.out.println("Something went wrong in Itr api => :" + e);
									statusAsPerSourcePan = "";
								}
							}
							if (captchaFailed) {
								itrData.put("statusAsPerSourcePan", statusAsPerSourcePan);
								itrData.put("statusAsPerSourceAcknowledment", statusAsPerAck);
								itrData.put("status", "pending");
								return itrData;
							} else {
								itrData.put("status", "invalid");
							}
						} else if (!statusAsPerSourcePan.isEmpty() || !statusAsPerSourcePan.trim().isEmpty()) {
							itrData.put("status", "invalid");
							itrData.put("statusAsPerSourcePan", statusAsPerSourcePan);
							itrData.put("statusAsPerSourceAcknowledment", statusAsPerAck);
						}
						return itrData;
					} catch (Exception e) {
						e.printStackTrace();
					}
					return itrData;
				} catch (Exception e) {
					System.out.println("Something went wrong in pancard api => :" + e);
				} finally {
					if (driver != null) {
						driver.close();
					}
				}
				return itrData;
			} else {
				try {
					driver = new FirefoxDriver();
					driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
					String site_address = "https://incometaxindiaefiling.gov.in/e-Filing/Services/ITRVStatusLink.html";
					// Status for Pan and assessment year
					driver.navigate().to(site_address);
					byte[] arrScreen = driver.getScreenshotAs(OutputType.BYTES);
					BufferedImage imageScreen = ImageIO.read(new ByteArrayInputStream(arrScreen));
					WebElement cap = driver.findElementById("captchaImg");
					Dimension capDimension = cap.getSize();
					Point capLocation = cap.getLocation();
					BufferedImage imgCap = imageScreen.getSubimage(capLocation.x, capLocation.y, capDimension.width,
							capDimension.height);
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					ImageIO.write(imgCap, "png", os);
					String imgString = Base64.encodeBase64String(os.toByteArray());
					byte[] data = Base64.decodeBase64(imgString);
					/*
					 * try (OutputStream stream = new FileOutputStream("/" +
					 * acknowledgmentNumber + ".bmp")) { stream.write(data); }
					 * String text = APIMain.decodeCaptcha("/" +
					 * acknowledgmentNumber + ".bmp");
					 */
					// File file11 = new
					// File("C:\\Users\\Varun_Prakash\\captcha");
					File file11 = new File("C:\\Users\\Administrator\\captcha");
					try (OutputStream stream = new FileOutputStream(file11 + "/acknowledgmentNumber3.bmp")) {
						// stream.write(data);
						// }
						// String path = "C:\\Users\\Varun_Prakash\\captcha";
						String path = "C:\\Users\\Administrator\\captcha";
						File newFile = new File(path + "\\" + acknowledgmentNumber + ".bmp");
						OutputStream outputStream = new FileOutputStream(newFile);
						outputStream.write(data);
						String text = APIMain.decodeCaptcha(newFile.toString());
						driver.findElementByXPath("//input[@name='itrvStatusDetails.pan']").sendKeys(panNumber);
						Select entity_type = new Select(
								driver.findElement(By.id("ITR-V-Status_itrvStatusDetails_assessmentYear")));
						if (assessmentYear.isEmpty() || assessmentYear.trim().isEmpty()) {
							entity_type.selectByValue("-1");
						} else {
							entity_type.selectByVisibleText(assessmentYear);
						}
						driver.findElementByXPath("//input[@name='captchaCode']").sendKeys(text);
						driver.findElementByXPath("//input[@type='submit']").click();
						String body = driver.getPageSource();
						Document doc;
						doc = Jsoup.parse(body);
						Element detdisp = doc.select("#staticContentsUrl").first();
						Elements tableData = detdisp.select("table");
						Elements trs = tableData.get(0).select("tr");
						statusAsPerSourcePan = trs.get(0).text();
						if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							try {
								statusAsPerSourcePan = tableData.get(0).select("tr").get(0).text();
							} catch (Exception e) {
								System.out.println("Something went wrong in Itr api => :" + e);
								statusAsPerSourcePan = "";
							}
						} else if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							try {
								statusAsPerSourcePan = trs.get(3).select("td").select("div").get(0).text();
								if (statusAsPerSourcePan.contains("Invalid PAN. Please retry")) {
									statusAsPerSourcePan = "Invalid PAN. ITR-V can not be verified based on PAN.";
								} else {
									statusAsPerSourcePan = "";
								}
							} catch (Exception e) {
								System.out.println("Something went wrong in Itr api => :" + e);
								statusAsPerSourcePan = "";
							}
						} else if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							try {
								statusAsPerSourcePan = trs.select(".error").first().text();
								if (statusAsPerSourcePan.contains("Please select the Assessment Year")) {
									statusAsPerSourcePan = "Assessment year is invalid. ITR-V can not be verified based on assessment year.";
								} else {
									statusAsPerSourcePan = "";
								}
							} catch (Exception e) {
								System.out.println("Something went wrong in Itr api => :" + e);
								statusAsPerSourcePan = "";
							}
						} else if (statusAsPerSourcePan.contains("Please select the Assessment Year")) {
							statusAsPerSourcePan = "Assessment year is invalid. ITR-V can not be verified based on assessment year.";
						} else if (!statusAsPerSourcePan.isEmpty() || !statusAsPerSourcePan.trim().isEmpty()) {
							if (statusAsPerSourcePan.contains(
									"e-Return for this Assessment Year or Acknowledgement Number has been Digitally signed")) {
								statusAsPerSourcePan = "e-Return has been Digitally signed";
							}
						} else if (!statusAsPerSourcePan.isEmpty() || !statusAsPerSourcePan.trim().isEmpty()) {
							if (statusAsPerSourcePan
									.contains("No e-Return has been filed for this PAN / Acknowledgement number")) {
								statusAsPerSourcePan = "No e-return has been filed.";
							}
						} else if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							boolean captchaFailed = false;
							if (statusAsPerSourcePan.isEmpty()) {
								try {
									statusAsPerSourcePan = trs.select(".error").first().text();
									if (!statusAsPerSourcePan.isEmpty() && statusAsPerSourcePan
											.contains("Please enter the number as appearing in the Image.")) {
										captchaFailed = true;
										statusAsPerSourcePan = "";
									} else if (!statusAsPerSourcePan.isEmpty() && statusAsPerSourcePan
											.contains("Please enter the code as appearing in the Image.")) {
										captchaFailed = true;
										statusAsPerSourcePan = "";
									}
								} catch (Exception e) {
									System.out.println("Something went wrong in Itr api => :" + e);
									statusAsPerSourcePan = "";
								}
							}
							if (captchaFailed) {
								itrData.put("statusAsPerSourcePan", statusAsPerSourcePan);
								itrData.put("status", "pending");
								return itrData;
							} else {
								itrData.put("status", "invalid");
							}
						}
						// Status for acknowledgement Number

						if (statusAsPerSourcePan.isEmpty() || statusAsPerSourcePan.trim().isEmpty()) {
							itrData.put("statusAsPerSourcePan", "Pan Status not captured");
							itrData.put("status", "pending");
							return itrData;
						} else {
							driver.navigate().to(site_address);
							byte[] arrScreenForAck = driver.getScreenshotAs(OutputType.BYTES);
							BufferedImage imageScreenForAck = ImageIO.read(new ByteArrayInputStream(arrScreenForAck));
							WebElement capForAck = driver.findElementById("captchaImg");
							Dimension capDimensionForAck = capForAck.getSize();
							Point capLocationForAck = capForAck.getLocation();
							BufferedImage imgCapForAck = imageScreenForAck.getSubimage(capLocationForAck.x,
									capLocationForAck.y, capDimensionForAck.width, capDimensionForAck.height);
							ByteArrayOutputStream osForAck = new ByteArrayOutputStream();
							ImageIO.write(imgCapForAck, "png", osForAck);
							String imgStringForAck = Base64.encodeBase64String(osForAck.toByteArray());
							byte[] dataForAck = Base64.decodeBase64(imgStringForAck);
							/*
							 * try (OutputStream stream = new
							 * FileOutputStream("/" + acknowledgmentNumber +
							 * ".bmp")) { stream.write(dataForAck); } String
							 * textForAck = APIMain.decodeCaptcha("/" +
							 * acknowledgmentNumber + ".bmp");
							 */
							// File file11_ = new
							// File("C:\\Users\\Varun_Prakash\\captcha");
							File file11_ = new File("C:\\Users\\Administrator\\captcha");
							try (OutputStream stream_ = new FileOutputStream(file11 + "/acknowledgmentNumber4.bmp")) {
								// stream.write(data);
								// }
								// String path_ =
								// "C:\\Users\\Varun_Prakash\\captcha";
								String path_ = "C:\\Users\\Administrator\\captcha";
								File newFile_ = new File(path_ + "\\" + acknowledgmentNumber + ".bmp");
								OutputStream outputStream_ = new FileOutputStream(newFile_);
								outputStream_.write(dataForAck);
								String text_ = APIMain.decodeCaptcha(newFile_.toString());
								driver.findElement(By.id("ITR-V-Status_chooseOptionackNum")).click();
								driver.findElementByXPath("//input[@name='itrvStatusDetails.acknowledgementNumber']")
										.sendKeys(acknowledgmentNumber);
								driver.findElementByXPath("//input[@name='captchaCode']").sendKeys(text_);
								driver.findElementByXPath("//input[@type='submit']").click();
								String bodyForAck = driver.getPageSource();
								doc = Jsoup.parse(bodyForAck);
								Element detdispForAck = doc.select("#staticContentsUrl").first();
								Elements tableDataForAck = detdispForAck.select("table");
								Elements trsForAck = tableDataForAck.get(0).select("tr");
								statusAsPerSourceAcknowledment = trsForAck.get(0).text();
								if (statusAsPerSourceAcknowledment.isEmpty()
										|| statusAsPerSourceAcknowledment.trim().isEmpty()) {
									try {
										statusAsPerSourceAcknowledment = tableDataForAck.get(0).select("tr").get(0)
												.text();
									} catch (Exception e) {
										System.out.println("Something went wrong in Itr api => :" + e);
										statusAsPerSourceAcknowledment = "";
									}
								} else if (!statusAsPerSourceAcknowledment.isEmpty()
										|| !statusAsPerSourceAcknowledment.trim().isEmpty()) {
									if (statusAsPerSourceAcknowledment.contains(
											"e-Return for this Assessment Year or Acknowledgement Number has been Digitally signed")) {
										statusAsPerSourceAcknowledment = "e-Return has been Digitally signed";
									}
								} else if (statusAsPerSourceAcknowledment.isEmpty()
										|| statusAsPerSourceAcknowledment.trim().isEmpty()) {
									try {
										statusAsPerSourceAcknowledment = trsForAck.get(2).select("table").get(1)
												.select("tr").get(0).select("div").get(0).text();
										if (!statusAsPerSourceAcknowledment.isEmpty()
												|| !statusAsPerSourceAcknowledment.trim().isEmpty()) {
											if (statusAsPerSourceAcknowledment
													.contains("Please enter a valid Acknowledgement Number")) {
												statusAsPerSourceAcknowledment = "Invalid Acknowlwdgement number";
											} else {
												statusAsPerSourceAcknowledment = "";
											}
										}
									} catch (Exception e) {
										System.out.println("Something went wrong in Itr api => :" + e);
										statusAsPerSourceAcknowledment = "";
									}
								} else if (statusAsPerSourceAcknowledment.isEmpty()
										|| statusAsPerSourceAcknowledment.trim().isEmpty()) {
									boolean captchaFailed = false;
									if (statusAsPerSourceAcknowledment.isEmpty()) {
										try {
											statusAsPerSourceAcknowledment = trsForAck.select(".error").first().text();
											if (!statusAsPerSourceAcknowledment.isEmpty()
													&& statusAsPerSourceAcknowledment.contains(
															"Please enter the number as appearing in the Image.")) {
												captchaFailed = true;
												statusAsPerSourceAcknowledment = "";
											} else if (!statusAsPerSourceAcknowledment.isEmpty()
													&& statusAsPerSourceAcknowledment.contains(
															"Please enter the code as appearing in the Image.")) {
												captchaFailed = true;
												statusAsPerSourceAcknowledment = "";
											}
										} catch (Exception e) {
											System.out.println("Something went wrong in Itr api => :" + e);
											statusAsPerSourceAcknowledment = "";
										}
									}
									if (captchaFailed) {
										itrData.put("statusAsPerSourcePan", statusAsPerSourceAcknowledment);
										itrData.put("status", "pending");
										return itrData;
									} else {
										itrData.put("status", "invalid");
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							if ((!statusAsPerSourcePan.isEmpty() || !statusAsPerSourcePan.trim().isEmpty())
									&& (!statusAsPerSourceAcknowledment.isEmpty()
											|| !statusAsPerSourceAcknowledment.trim().isEmpty())) {
								itrData.put("status", "valid");
								itrData.put("statusAsPerSourcePan", statusAsPerSourcePan);
								itrData.put("statusAsPerSourceAcknowledment", statusAsPerSourceAcknowledment);
							} else {
								itrData.put("statusAsPerSourcePan", statusAsPerSourcePan);
								itrData.put("statusAsPerSourceAcknowledment", statusAsPerSourceAcknowledment);
							}
							return itrData;
						}

					} catch (Exception e) {
						System.out.println("Something went wrong in Itr api => :" + e);
					} finally {
						if (driver != null) {
							driver.close();
						}
					}
					return itrData;
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					if (driver != null) {
						driver.close();
					}
				}
			}
		}

		return itrData;
	}

	/* Method to get the data from the API for DL data */
	public static JSONObject getDLStatus(String dlNumber, String dob1, Integer apiCount)
			throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject dlData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		dob1 = "\"" + dob1 + "\"";
		dlNumber = "\"" + dlNumber + "\"";
		String value = "{\"dl_no\":" + dlNumber + "," + "\"dob\":" + dob1 + "," + "\"key\":" + key + "" + "}";
		dlData.put("status", "pending");
		final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/dl").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object dlResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (dlResult == null || dlResult.equals("No Record Found") || dlResult.equals("NO RECORD FOUND")) {
					dlData = ApiCallHelperUtil.saveInvalidDL(apiCount);
					return dlData;
				}
				if (jsonObject != null && !dlResult.equals("No Record Found")) {
					JSONObject dlJSONObject = (JSONObject) dlResult;
					dlData = ApiCallHelperUtil.saveDL(dlJSONObject, apiCount);
					return dlData;
				}
				return dlData;
			}
			if (response.code() >= 500) {
				dlData.put("apiCount", apiCount);
				if (apiCount >= 5) {
					dlData.put("status", "invalid");
					dlData.put("errormsg", "invalid");
				}
				return dlData;
			}
			if (response.code() == 400 || response.code() == 401) {
				dlData.put("apiCount", apiCount);
				dlData.put("status", "invalid");
				dlData.put("errormsg", "invalid");
				return dlData;
			}
			return dlData;
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from Driving Licence " + e.getMessage());
		}
		return dlData;
	}

	public static JSONObject getIncomeTaxStatus(String incomeTax_document) throws IOException, JSONException {
		JSONObject incomeTaxData = new JSONObject();
		incomeTaxData.put("status", "pending");
		FirefoxDriver driver = null;
		try {

			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			String site_address = "https://incometaxindiaefiling.gov.in/e-Filing/Services/ITRVStatusLink.html";
			driver.navigate().to(site_address);
			byte[] arrScreen = driver.getScreenshotAs(OutputType.BYTES);
			BufferedImage imageScreen = ImageIO.read(new ByteArrayInputStream(arrScreen));
			WebElement cap = driver.findElementById("captchaImg");
			Dimension capDimension = cap.getSize();
			Point capLocation = cap.getLocation();
			BufferedImage imgCap = imageScreen.getSubimage(capLocation.x, capLocation.y, capDimension.width,
					capDimension.height);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(imgCap, "png", os);

			String imgString = Base64.encodeBase64String(os.toByteArray());

			byte[] data = Base64.decodeBase64(imgString);

			/*
			 * try (OutputStream stream = new FileOutputStream("/" +
			 * incomeTax_document + ".bmp")) { stream.write(data); }
			 * 
			 * String text = APIMain.decodeCaptcha("/" + incomeTax_document +
			 * ".bmp");
			 */
			// File file11 = new File("C:\\Users\\Varun_Prakash\\captcha");
			File file11 = new File("C:\\Users\\Administrator\\captcha");
			try (OutputStream stream = new FileOutputStream(file11 + "/incomeTax_document.bmp")) {
				// stream.write(data);
				// }
				// String path = "C:\\Users\\Varun_Prakash\\captcha";
				String path = "C:\\Users\\Administrator\\captcha";
				File newFile = new File(path + "\\" + incomeTax_document + ".bmp");
				OutputStream outputStream = new FileOutputStream(newFile);
				outputStream.write(data);
				String text = APIMain.decodeCaptcha(newFile.toString());
				driver.findElementById("ITR-V-Status_chooseOptionackNum").click();
				driver.findElementByXPath("//input[@name='itrvStatusDetails.acknowledgementNumber']")
						.sendKeys(incomeTax_document);
				driver.findElementByXPath("//input[@name='captchaCode']").sendKeys(text);
				driver.findElementByXPath("//input[@type='submit']").click();
				String body = driver.getPageSource();
				return incomeTaxData;
			}
		} catch (Exception e) {
			System.out.println("Something went wrong in pancard api => :" + e);
		} finally {
			if (driver != null) {
				driver.close();
			}
		}
		return incomeTaxData;

	}

	private static String handleLLPDocumentType(String id, String entity_type) {
		String firstChars = "";
		Boolean dashContains = id.contains("-");
		// FLLPIN/LLPIN validation
		if (dashContains) {
			firstChars = id.split("-")[0];
			if (id.length() == 8) {
				// LLPIN validation
				if (dashContains && id.length() <= 8 && firstChars.length() <= 3) {
					LLPIN p = new LLPIN(id);
					try {
						boolean isLLPPIN = new LlpinValidator().isValid(p);
						if (isLLPPIN) {
							return "llpin";
						}
					} catch (DocumentValidatorException e) {
					}
				}
			} else if (id.length() == 9) {
				// FLLPIN validation
				if (dashContains) {
					FLLPIN p = new FLLPIN(id);
					try {
						boolean isFllPin = new FllpinValidator().isValid(p);
						if (isFllPin) {
							return "fllpin";
						}
					} catch (DocumentValidatorException e) {

					}
				}
			}
		} else {
			// CIN/FCRN validation
			if (dashContains) {
				firstChars = id.split("-")[0];
			}
			boolean isValidDoc = false;
			if (!dashContains && id.length() > 6) {
				CIN p = new CIN(id);
				try {
					isValidDoc = new CinValidator().isValid(p);
					if (isValidDoc) {
						return "cin";
					}
				} catch (DocumentValidatorException e) {
				}
			} else if (!dashContains && id.length() <= 6) {
				FCRN p = new FCRN(id);
				try {
					isValidDoc = new FcrnValidator().isValid(p);
					if (isValidDoc) {
						return "fcrn";
					}
				} catch (DocumentValidatorException e) {
				}
			}
		}
		return "";
	}

	private static String handleNonLLPDocument(String id, String entity_type) {
		Boolean dashContains = id.contains("-");
		if (dashContains) {
			return "";
		} else {
			boolean isDoc = false;
			if (id.length() > 6) {
				CIN p = new CIN(id);
				try {
					isDoc = new CinValidator().isValid(p);
					if (isDoc)
						return "cin";
				} catch (DocumentValidatorException e) {
				}
			} else if (id.length() <= 6) {
				FCRN p = new FCRN(id);
				try {
					isDoc = new FcrnValidator().isValid(p);
					if (isDoc)
						return "fcrn";
				} catch (DocumentValidatorException e) {
				}
			}
		}
		return "";
	}

	private static String getDocumentType(String id, String entity_type) {
		if (entity_type.equals("Limited Liability Partnership (LLP)")) {
			return handleLLPDocumentType(id, entity_type);
		} else {
			return handleNonLLPDocument(id, entity_type);
		}
	}

	/* Method to get the FLLPIN/CIN/LLP/FCRN/ */
	public static JSONObject getLlPinStatus(String cinId, String customer_entity, Integer apiCount)
			throws JSONException {
		JSONObject jsonObject = new JSONObject();
		JSONObject responseMetaData = new JSONObject();
		responseMetaData.put("statusAsPerSource", "invalid");
		String key = ApiCallHelperUtil.getKey();
		String cinIdStrForRESTCall = "\"" + cinId + "\"";
		String value = "{\"cin\":" + cinIdStrForRESTCall + "," + "\"key\":" + key + "" + "}";
		responseMetaData.put("api_status", "pending");
		responseMetaData.put("documentType", "");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/mca").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object llpinResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (llpinResult == null || llpinResult.equals("No Record Found")
						|| llpinResult.equals("NO RECORD FOUND")) {
					responseMetaData = ApiCallHelperUtil.saveInvalidLLPIN(apiCount, cinId, customer_entity);
					return responseMetaData;
				}
				if (jsonObject != null && !llpinResult.equals("No Record Found")) {
					JSONObject llpinJSONObject = new JSONObject();
					llpinJSONObject = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
					/* Details for the CIN */
					if (llpinJSONObject.has("cin")) {
						responseMetaData = ApiCallHelperUtil.saveCINDetails(llpinJSONObject, apiCount);
						responseMetaData.put("documentType", getDocumentType(cinId, customer_entity));
						responseMetaData.put("api_status", "valid");
						return responseMetaData;
					} else if (llpinJSONObject.has("FLLPIN")) {
						/* Details for the FLLPIN */
						responseMetaData = ApiCallHelperUtil.saveFLLPIN(llpinJSONObject, apiCount);
						responseMetaData.put("documentType", getDocumentType(cinId, customer_entity));
						responseMetaData.put("api_status", "valid");
						return responseMetaData;
					} else if (llpinJSONObject.has("FCRN")) {
						/* Details for the FCRN */
						responseMetaData = ApiCallHelperUtil.saveFCRN(llpinJSONObject, apiCount);
						responseMetaData.put("documentType", getDocumentType(cinId, customer_entity));
						responseMetaData.put("api_status", "valid");
						return responseMetaData;
					} else if (llpinJSONObject.has("LLPIN")) {
						/* Details for the LLPin */
						responseMetaData = ApiCallHelperUtil.saveLLPIN(llpinJSONObject, apiCount);
						responseMetaData.put("documentType", getDocumentType(cinId, customer_entity));
						responseMetaData.put("api_status", "valid");
						return responseMetaData;
					}
				}
			}
			if (response.code() >= 500) {
				responseMetaData.put("apiCount", apiCount);
				if (apiCount >= 5) {
					responseMetaData.put("api_status", "invalid");
					responseMetaData.put("documentType", "invalid");
					responseMetaData.put("api_status", "invalid");
				}
				responseMetaData.put("llpin", responseMetaData);
				return responseMetaData;
			}
			if (response.code() == 400 || response.code() == 401) {
				responseMetaData.put("apiCount", apiCount);
				responseMetaData.put("api_status", "invalid");
				responseMetaData.put("documentType", "invalid");
				responseMetaData.put("status", "invalid");
				responseMetaData.put("errormsg", "invalid");
				responseMetaData.put("llpin", responseMetaData);
				return responseMetaData;
			}
			return responseMetaData;
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Exception from the API " + e.getMessage().toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseMetaData;
	}

	public static JSONObject getVatStatus(String vat_document) throws JSONException {
		JSONObject vatData = new JSONObject();
		JSONArray frequency = new JSONArray();
		JSONObject responseData = new JSONObject();
		vatData.put("api_status", "pending");
		responseData.put("vatData", vatData);
		responseData.put("frequency", frequency);

		try {

			Connection connection = Jsoup.connect("http://mahavat.gov.in/Tin_Search/TinSearch");
			Connection.Response response1 = Jsoup //
					.connect("http://mahavat.gov.in/Tin_Search/TinSearch")
					// Extract image absolute URL
					.data("tin", vat_document).data("pan", "").data("rc_no", "").data("fptecno", "").data("bptecno", "")
					.data("DEALERNAME", "").data("Submit", "SEARCH").cookies(connection.response().cookies()) // Grab
																												// cookies

					.ignoreContentType(true) // Needed for fetching image
					.method(Method.POST).execute();

			String body1 = response1.body();
			Document doc1;
			doc1 = Jsoup.parse(body1);
			String statusAsPerSource = "";
			try {
				statusAsPerSource = doc1.select("form").first().select("table").last().select("tr").last().select("td")
						.last().text();
			} catch (Exception e) {
				System.out.println("Faced Issue while Searching Tin Number on MahaPortal in VAT API => :" + e);
				statusAsPerSource = "";
			}

			if (statusAsPerSource.trim().isEmpty()) {
				try {
					statusAsPerSource = doc1.select("table").get(12).select("tr").get(3).select("td").last().text();
				} catch (Exception e) {
					System.out.println("Issue While Fetching Status As Per Source on Mahaportal in VAT API => :" + e);
					statusAsPerSource = "";
				}
			}

			LinkedHashMap cookie = (LinkedHashMap) response1.cookies();
			Connection.Response response2 = Jsoup //
					.connect("http://mahavat.gov.in/Tin_Search/Tin_display?tinnumber=" + vat_document)
					// .data("tinnumber", vat_document)
					.cookies(cookie) // Grab cookies
					.ignoreContentType(true) // Needed for fetching image
					.method(Method.GET).execute();
			String body = response2.body();

			Document doc;
			doc = Jsoup.parse(body);

			Element responseForm;
			responseForm = doc.select("body").first();

			Elements tables = responseForm.select("table");
			if (tables != null) {
				Element table1 = tables.get(0);
				if (table1 != null) {
					Elements trs_table1 = table1.select("tr");
					if (trs_table1 != null) {
						for (int i = 0; i < trs_table1.size(); i++) {
							if (trs_table1.get(i) != null) {
								if (trs_table1.get(i).select("td").size() > 1) {
									Element td1 = trs_table1.get(i).select("td").get(0);
									Element td2 = trs_table1.get(i).select("td").get(1);
									if (td1 != null && td2 != null) {
										vatData.put(td1.text(), td2.text());
									}
								}
							}
						}
						vatData.put("api_status", "valid");
					} else {
						vatData.put("api_status", "invalid");
					}
				}

				Element table2 = tables.get(1);
				if (table2 != null) {
					Elements trs_table2 = table2.select("tr");
					if (trs_table2 != null) {
						for (int i = 0; i < trs_table2.size(); i++) {
							if (trs_table2.get(i) != null) {
								if (trs_table2.get(i).select("th").size() > 1) {
									Element th1 = trs_table2.get(i).select("th").get(0);
									Element th2 = trs_table2.get(i).select("th").get(1);
									if (th1 != null && th2 != null) {
										Element input1 = th1.select("input").first();
										Element input2 = th2.select("input").first();
										if (input1 != null && input2 != null) {
											JSONObject jsonObject = new JSONObject();
											String val1 = input1.val().replace("\n", "");
											String val2 = input2.val().replace("\n", "");
											jsonObject.put("financialYear", val1);
											jsonObject.put("frequencyName", val2);
											frequency.put(jsonObject);
										}
									}
								}
							}
						}
					}
				}

			} else {
				vatData.put("api_status", "invalid");
			}
			vatData.put("statusAsPerSource", statusAsPerSource);
			if (StringUtils.equals(statusAsPerSource,
					"M V A T R C No Should be 11 Digits, Begin With 27 or 99,ends with 'V'or'C'or'P'")
					|| org.h2.util.StringUtils.equals(statusAsPerSource, "M V A T RC No Should End With 'V'or'C'or'P'")
					|| org.h2.util.StringUtils.equals(statusAsPerSource, "Invalid charectors should not be entered")
					|| org.h2.util.StringUtils.equals(statusAsPerSource, "M V A T RC No Should Begin With 27 or 99")) {
				vatData.put("statusAsPerSource", "VAT/CST TIN number is incorrect");
				vatData.put("api_status", "invalid");
			}
			if (StringUtils.equals(statusAsPerSource, "M V A T RC No is Invalid Number")) {
				vatData.put("statusAsPerSource", "VAT/CST TIN number is Invalid");// Need
																					// to
																					// Revisit
																					// this
																					// Line
				vatData.put("api_status", "invalid");
			}

			responseData.put("vatData", vatData);
			responseData.put("frequency", frequency);

			if (StringUtils.equals(statusAsPerSource, "Records not found")) {
				vatData.put("api_status", "invalid");
			}

			return responseData;

		} catch (Exception ae) {
			vatData.put("statusAsPerSource", "VAT/CST TIN number is Invalid");
			System.out.println("Something went wrong while Fetching TIN Specific Information => :" + ae);

		}

		return responseData;
	}

	public static JSONObject getPassportChecksum(String document, Date dob, Date expiry_date)
			throws IOException, JSONException {
		JSONObject checksum = new JSONObject();
		LOG.info("Came to Verify the Passport Data With Following Data -> ", document);
		String passportStr = document + "<";
		String dobStr = new SimpleDateFormat("yyyyMMdd").format(dob).substring(2);
		String expiryStr = new SimpleDateFormat("yyyyMMdd").format(expiry_date).substring(2);

		char passportno[] = passportStr.toCharArray();

		int[] birthdate = new int[dobStr.length()];
		for (int i = 0; i < dobStr.length(); i++) {
			birthdate[i] = Integer.parseInt(dobStr.charAt(i) + "");
		}

		int[] expdate = new int[expiryStr.length()];
		for (int i = 0; i < expiryStr.length(); i++) {
			expdate[i] = Integer.parseInt(expiryStr.charAt(i) + "");
		}

		int[] pass = new int[9];

		int[] passWeightR1 = { 7, 3, 1, 7, 3, 1, 7, 3, 1 };
		int[] birthWeightR1 = { 7, 3, 1, 7, 3, 1 };
		int[] expWeightR1 = { 7, 3, 1, 7, 3, 1 };

		int[] passWeightR2 = { 7, 3, 1, 7, 3, 1, 7, 3, 1, 7 };
		int[] birthWeightR2 = { 3, 1, 7, 3, 1, 7, 3 };
		int[] expWeightR2 = { 1, 7, 3, 1, 7, 3, 1 };

		int[] passProductR1 = new int[9];
		int[] birthProductR1 = new int[6];
		int[] expProductR1 = new int[6];

		int[] passProductR2 = new int[10];
		int[] birthProductR2 = new int[7];
		int[] expProductR2 = new int[7];

		int sum = 0, sum1 = 0;
		int chkdgt1, chkdgt2, chkdgt3, chkdgt4;
		Map<Character, Integer> hm = new HashMap<Character, Integer>();
		int ascii = (int) Character.toUpperCase(passportno[0]);
		hm.put(Character.toUpperCase(passportno[0]), ascii - 55);
		hm.put('<', new Integer(0));

		for (int i = 0; i < 9; i++) {
			if (hm.containsKey(passportno[i]))
				pass[i] = Integer.parseInt(hm.get(passportno[i]).toString());
			else
				pass[i] = Character.getNumericValue(passportno[i]);
		}

		for (int i = 0; i < 9; i++) {
			passProductR1[i] = pass[i] * passWeightR1[i];
			sum += passProductR1[i];
		}
		chkdgt1 = sum % 10;
		sum = 0;
		for (int i = 0; i < 6; i++) {
			birthProductR1[i] = birthdate[i] * birthWeightR1[i];
			sum += birthProductR1[i];
			expProductR1[i] = expdate[i] * expWeightR1[i];
			sum1 += expProductR1[i];
		}
		chkdgt2 = sum % 10;
		chkdgt3 = sum1 % 10;
		checksum.put("checkdigit1", chkdgt1);
		checksum.put("checkdigit2", chkdgt2);
		checksum.put("checkdigit3", chkdgt3);
		pass = addChkDgt(pass, chkdgt1);
		birthdate = addChkDgt(birthdate, chkdgt2);
		expdate = addChkDgt(expdate, chkdgt3);
		sum = 0;
		for (int i = 0; i < 10; i++) {
			passProductR2[i] = pass[i] * passWeightR2[i];
			sum += passProductR2[i];
		}
		for (int i = 0; i < 7; i++) {
			birthProductR2[i] = birthdate[i] * birthWeightR2[i];
			sum += birthProductR2[i];
			expProductR2[i] = expdate[i] * expWeightR2[i];
			sum += expProductR2[i];
		}
		chkdgt4 = sum % 10;
		checksum.put("checkdigit4", chkdgt4);
		LOG.debug("Here is what i'm returning",checksum);
		return checksum;
	}

	private static int[] addChkDgt(int arr[], int chkdgt) {
		int arr1[] = new int[arr.length + 1];
		System.arraycopy(arr, 0, arr1, 0, arr.length);
		arr1[arr.length] = chkdgt;
		return arr1;
	}

	public static JSONArray getAssessmentYears() {
		JSONArray assesssmentYears = new JSONArray();
		try {
			String site_address = "https://incometaxindiaefiling.gov.in/e-Filing/Services/ITRVStatusLink.html";
			Connection connection = Jsoup.connect(site_address);

			Connection.Response response1 = Jsoup //
					.connect(site_address) // Extract image absolute URL
					.cookies(connection.response().cookies()) // Grab cookies
					.ignoreContentType(true) // Needed for fetching image
					.method(Method.GET).execute();

			String body = response1.body();
			Document doc = null;
			doc = Jsoup.parse(body);
			List assessementYrs = doc.select("select").get(0).getAllElements();
			if (assessementYrs != null) {
				// System.out.println("Size of the Assessment Year
				// "+assessementYrs.size());
				if (assessementYrs.size() >= 2) {
					// System.out.println("Size of the Assessment Year -->
					// "+assessementYrs.size());
					for (int i = 2; i < assessementYrs.size(); i++) {
						String assessmentYear = ((Element) assessementYrs.get(i)).select("option").get(0).text();
						// System.out.println("Assessment Yarea --->
						// "+assessmentYear);
						if (StringUtils.isNoneBlank(assessmentYear)) {
							assesssmentYears.put(assessmentYear);
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Something went wrong when fetching assessment year => :" + e);
		}

		return assesssmentYears;
	}

	/* Method to Fetch the Voter Details */
	public static JSONObject getVoterIdStatus(String voter_document, int apiCount) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		JSONObject voterData = new JSONObject();
		Date dateTimeRequested = new Date();
		String key = ApiCallHelperUtil.getKey();
		voter_document = "\"" + voter_document + "\"";
		// Epic number to be verified
		String value = "{\"epic_no\":" + voter_document + "," + "\"key\":" + key + "" + "}";
		voterData.put("status", "pending");
		// voterData.put("statusAsPerSource", "");
		LOG.info("Verifying  the Voter Data -> ", voter_document);
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/voter").post(body)
					.build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object voterIdData = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (voterIdData == null || voterIdData.equals("No Record Found")
						|| voterIdData.equals("NO RECORD FOUND")) {
					voterData = ApiCallHelperUtil.saveInvalidVoter(apiCount, dateTimeRequested);
					LOG.error("Didn't Found Any Record for the voter document ",voterData);
					return voterData;
				}
				if (jsonObject != null && !voterIdData.equals("No Record Found")) {
					JSONObject voterIDDataJSON = (JSONObject) voterIdData;
					voterData = ApiCallHelperUtil.saveVoter(voterIDDataJSON, apiCount);
					LOG.info(" Found the Record for the voter document ",voterData);
					return voterData;
				}
				return voterData;
			}
			if (response.code() >= 500) {
				voterData.put("apiCount", apiCount);
				if (apiCount >= 5) {
					voterData.put("status", "invalid");
					voterData.put("errormsg", "invalid");
				}

				return voterData;
			}
			if (response.code() == 400 || response.code() == 401) {
				voterData.put("apiCount", apiCount);
				voterData.put("status", "invalid");
				voterData.put("errormsg", "invalid");
				return voterData;
			}
			return voterData;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return voterData;
	}

	public static String getEmail(String email) throws IOException {
		String success = "is valid";
		return success;
	}

	/*
	 * Method written in order to fetch the Electricity data from different
	 * distributors
	 */
	public static JSONObject getElectricityStatus(String consumer_id, String service_provider, Integer apiCount)
			throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject elData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		// this is the Key to fetch the data from the API
		consumer_id = "\"" + consumer_id + "\"";
		// this is the consumer_number inscribed on the electricity bill
		service_provider = "\"" + service_provider + "\"";
		// State Electricity distribution agency
		String value = "{\"consumer_id\":" + consumer_id + "," + "\"service_provider\":" + service_provider + ","
				+ "\"key\":" + key + "" + "}";
		elData.put("status", "pending");
		elData.put("apiResponse", "invalid");
		elData.put("statusAsPerSource", "invalid");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			/* Have to change the API from testapi to api.karza.in */
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/elec").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object electricityResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (electricityResult == null || electricityResult.equals("No Record Found")
						|| electricityResult.equals("NO RECORD FOUND")) {
					elData = ApiCallHelperUtil.saveInvalidElectricity(apiCount);
					return elData;
				}
				if (jsonObject != null && !electricityResult.equals("No Record Found")) {
					JSONObject electricityJSONObj = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject,
							"result");
					elData = ApiCallHelperUtil.saveElectricity(electricityJSONObj, apiCount);
					return elData;
				}
				return elData;
			}
			if (response.code() >= 500) {
				elData.put("apiCount", apiCount);
				elData.put("statusAsPerSource", "invalid");
				elData.put("status", "invalid");
				if (apiCount >= 5) {
					elData.put("apiResponse", "invalid");
				} else {
					elData.put("apiResponse", "pending");
				}
				elData.put("electricity", elData);
				return elData;
			}
			if (response.code() == 400 || response.code() == 401) {
				elData.put("apiCount", apiCount);
				elData.put("status", "invalid");
				elData.put("apiResponse", "invalid");
				elData.put("statusAsPerSource", "invalid");
				elData.put("electricity", elData);
				return elData;
			}
			return elData;
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Hi i'm Exception from Electricity" + e.getMessage());
		}
		return elData;
	}

	/* Method written in order to fetch the Telephone Bill data across India */
	public static JSONObject getTelephoneStatus(String tel_no, Integer apiCount) throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject telData = new JSONObject();
		JSONObject telephoneData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		tel_no = "\"" + tel_no + "\""; // this is the number, which will be
										// passed to fetch the data for
		String value = "{\"tel_no\":" + tel_no + "," + "\"key\":" + key + "" + "}";
		telephoneData.put("status", "pending");
		telephoneData.put("apiResponse", "invalid");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			/* Have to change the API from testapi to api.karza.in */
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/tele").post(body).build();
			apiCount++;
			okhttp3.Response response = client.newCall(request).execute();
			jsonObject = new JSONObject(response.body().string());
			Object telephoneBillResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
			if (telephoneBillResult == null || telephoneBillResult.equals("No Record Found")
					|| telephoneBillResult.equals("NO RECORD FOUND")) {
				telephoneData = ApiCallHelperUtil.saveInvalidTelephone(apiCount);
				return telephoneData;
			}
			if (jsonObject != null && !telephoneBillResult.equals("No Record Found")) {
				JSONObject teleJSONObject = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				telephoneData = ApiCallHelperUtil.saveTelephone(teleJSONObject, apiCount);
				return telephoneData;
			}
			return telephoneData;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return telephoneData;

	}

	/* Method to fetch the UAN Details */
	public static List<JSONObject> getEPFODetails(String uan_number, String contact_no, Integer apiCount)
			throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		List<JSONObject> epfList = new ArrayList<JSONObject>();
		String key = ApiCallHelperUtil.getKey();
		uan_number = "\"" + uan_number + "\"";
		contact_no = "\"" + contact_no + "\"";
		String value = "{\"uanid\":" + uan_number + "," + "\"mob\":" + contact_no + "," + "\"key\":" + key + "" + "}";
		JSONObject epfoDetailData = null;
		epfoDetailData = new JSONObject();
		epfoDetailData.put("status", "pending");
		epfoDetailData.put("apiResponse", "invalid");
		epfoDetailData.put("statusAsPerSource", "invalid");
		epfoDetailData.put("epf", epfoDetailData);
		// epfList.add(epfoDetailData);
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://api.karza.in/epf").post(body).build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			JSONObject epfData = null;
			if (response.code() == 200) {
				epfData = new JSONObject();
				epfoDetailData = new JSONObject();
				jsonObject = new JSONObject(response.body().string());
				Object epfoResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (epfoResult == null || epfoResult.equals("No Record Found")
						|| epfoResult.equals("NO RECORD FOUND")) {
					epfList = ApiCallHelperUtil.saveInvalidEPFO(apiCount);
					return epfList;
				}
				if (jsonObject != null && !epfoResult.equals("No Record Found")) {
					/*
					 * Get the Array from the Details of the Employee, their
					 * could be possibility of having multiple value for the
					 * same person
					 */
					JSONObject epfoResultJSON = (JSONObject) epfoResult;
					epfList = ApiCallHelperUtil.saveEPFO(epfoResultJSON, apiCount);
					return epfList;
				}
				return epfList;
			}
			if (response.code() >= 500) {
				epfoDetailData = new JSONObject();
				epfoDetailData.put("apiCount", apiCount);
				epfoDetailData.put("status", "invalid");
				epfoDetailData.put("epf", epfoDetailData);
				epfoDetailData.put("apiResponse", "invalid");
				if (apiCount >= 5) {
					epfoDetailData.put("statusAsPerSource", "invalid");
				} else {
					epfoDetailData.put("statusAsPerSource", "pending");
				}
				epfList.add(epfoDetailData);
				return epfList;
			}
			if (response.code() == 400 || response.code() == 401) {
				epfoDetailData = new JSONObject();
				epfoDetailData.put("apiCount", apiCount);
				epfoDetailData.put("status", "invalid");
				epfoDetailData.put("statusAsPerSource", "invalid");
				epfoDetailData.put("apiResponse", "invalid");
				epfoDetailData.put("epf", epfoDetailData);
				epfList.add(epfoDetailData);
				return epfList;
			}
			return epfList;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return epfList;
	}

	/* Method to fetch the LPG Details */
	public static JSONObject getLPGStatus(String lpg_id, Integer apiCount) throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject lpgDetailData = new JSONObject();
		String key = ApiCallHelperUtil.getKey();
		lpg_id = "\"" + lpg_id + "\"";
		String value = "{\"lpg_id\":" + lpg_id + "," + "\"key\":" + key + "" + "}";
		lpgDetailData.put("api_status", "pending");
		lpgDetailData.put("documentType", "");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			LOG.debug("Came to Verify the LPG with following ID ", lpg_id);
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://testapi.karza.in/lpg").post(body)
					.build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				jsonObject = new JSONObject(response.body().string());
				Object lpgResult = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (lpgResult == null || lpgResult.equals("No Record Found") || lpgResult.equals("NO RECORD FOUND")) {
					lpgDetailData = ApiCallHelperUtil.saveInvalidLPG(apiCount);
					LOG.error("No Record Found for the LPG ",lpg_id);
					return lpgDetailData;
				}
				if (jsonObject != null && !lpgResult.equals("No Record Found")) {
					JSONObject lpgJSONObject = new JSONObject();
					lpgJSONObject = (JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
					lpgDetailData = ApiCallHelperUtil.saveLPG(lpgJSONObject, apiCount);
					LOG.info(" Record Found for the LPG ",lpg_id);
					LOG.trace("Record Found for the LPG and Data Being Returned is ",lpgDetailData);

					return lpgDetailData;
				}
			}
			if (response.code() > 500) {
				lpgDetailData.put("apiCount", apiCount);
				lpgDetailData.put("status", "invalid");
				lpgDetailData.put("apiResponse", "invalid");

				if (apiCount >= 5) {
					lpgDetailData.put("statusAsPerSource", "invalid");
				} else {
					lpgDetailData.put("statusAsPerSource", "pending");
				}
				lpgDetailData.put("lpg", lpgDetailData);
				return lpgDetailData;
			}
			if (response.code() == 400 || response.code() == 401) {
				lpgDetailData.put("apiCount", apiCount);
				lpgDetailData.put("status", "invalid");
				lpgDetailData.put("apiResponse", "invalid");
				lpgDetailData.put("statusAsPerSource", "invalid");
				return lpgDetailData;
			}
			return lpgDetailData;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lpgDetailData;
	}

	/* Method to Fetch the ESIC Details */

	public static JSONObject getESICStatus(String esic_id, Integer apiCount) throws JSONException, IOException {
		JSONObject jsonObject = new JSONObject();
		JSONObject esicDetailData = null;
		String key = ApiCallHelperUtil.getKey();
		esic_id = "\"" + esic_id + "\"";
		String value = "{\"esic_id\":" + esic_id + "," + "\"key\":" + key + "" + "}";
		esicDetailData = new JSONObject();
		esicDetailData.put("status", "pending");
		esicDetailData.put("statusAsPerSource", "");
		final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json;charset=utf-8");
		try {
			Builder b = new Builder();
			b.readTimeout(120, TimeUnit.SECONDS);
			b.writeTimeout(120, TimeUnit.SECONDS);
			okhttp3.OkHttpClient client = b.build();
			okhttp3.RequestBody body = okhttp3.RequestBody.create(JSON, value);
			okhttp3.Request request = new okhttp3.Request.Builder().url("https://testapi.karza.in/esic").post(body)
					.build();
			okhttp3.Response response = client.newCall(request).execute();
			apiCount++;
			if (response.code() == 200) {
				String esicResponseStr = response.body().string();
				jsonObject = new JSONObject(esicResponseStr);
				Object esicDataObject = KYCUtil.checkKeyAndGetValueFromJSON(jsonObject, "result");
				if (esicDataObject == null || esicDataObject.equals("No Record Found")
						|| esicDataObject.equals("NO RECORD FOUND")) {
					esicDetailData = ApiCallHelperUtil.saveInvalidESIC(apiCount);
					return esicDetailData;
				}
				if (jsonObject != null && !esicDataObject.equals("No Record Found")) {
					JSONObject esicJSONResult = (JSONObject) esicDataObject;
					esicDetailData = ApiCallHelperUtil.saveESIC(esicJSONResult, apiCount);
					return esicDetailData;
				}
				return esicDetailData;
			}
			if (response.code() >= 500) {
				esicDetailData = new JSONObject();
				esicDetailData.put("apiCount", apiCount);
				esicDetailData.put("status", "invalid");
				if (apiCount >= 5) {
					esicDetailData.put("statusAsPerSource", "invalid");
					esicDetailData.put("apiValidation", "invalid");
				} else {
					esicDetailData.put("statusAsPerSource", "pending");
					esicDetailData.put("apiValidation", "pending");
				}
				esicDetailData.put("esic", esicDetailData);
				return esicDetailData;
			}
			if (response.code() == 400 || response.code() == 401) {
				esicDetailData = new JSONObject();
				esicDetailData.put("apiCount", apiCount);
				esicDetailData.put("staus", "invalid");
				esicDetailData.put("statusAsPerSource", "invalid");
				esicDetailData.put("apiValidation", "invalid");
				return esicDetailData;
			}
			return esicDetailData;

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return esicDetailData;
	}

	public static void main(String[] args) throws Exception {
		ApiCallHelper http = new ApiCallHelper();
		// List<JSONObject> result =
		// ApiCallHelper.getEPFODetails("100426507321", "9824531078", 1);
		// JSONObject result = http.getLPGStatus("10000000050431060", 1);
		// JSONObject result = ApiCallHelper.getESICStatus("2612316891", 1);
		// System.out.println("Result --> " + result.toString());
		// Boolean result = new ApiCallHelper().getAadharStatus("391033230766",
		// "Varun Prakash", "1992", "M", "802103");
		// JSONObject result =
		JSONObject result1 = ApiCallHelper.getPanStatus("BSJPP1939F", 2);
		System.out.println("Result  -> " + result1.toString());
		// JSONObject result1 =
		// ApiCallHelper.getLlPinStatus("U74120MH3015PTC265316","1010",0);
		// System.out.println(result1.toString());
	}

}
