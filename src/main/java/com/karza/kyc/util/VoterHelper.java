package com.karza.kyc.util;

import com.karza.kyc.model.VoterFamily;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.VoterIDService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Admin
 * Date: 7/27/16
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class VoterHelper {

    public static VoterID saveVoter(VoterID voterID, Long customerEntityId, JSONObject voterApiResponse, VoterIDService voterIDService) throws JSONException {
        VoterID voterID1 = new VoterID(voterID.getDocument(), customerEntityId, voterApiResponse);
        voterID1.setId(voterID.getId());
        voterIDService.update(voterID1);
        List<VoterFamily> voterFamilyList = voterIDService.getFamilyByVoterID(voterID.getId().toString());
        if (voterFamilyList != null) {
            if (voterFamilyList.size() > 0) {
                for (int i = 0; i < voterFamilyList.size(); i++) {
                    VoterFamily voterFamily = voterFamilyList.get(i);
                    voterIDService.deleteFamily(voterFamily);
                }
            }
        }
        if (voterApiResponse.has("family")) {
            JSONArray familyList = ((JSONArray) (voterApiResponse.get("family")));
            if (familyList.length() > 0) {
                for (int i = 0; i < familyList.length(); i++) {
                    JSONObject familyObj = (JSONObject) familyList.get(i);
                    VoterFamily voterFamily = new VoterFamily(voterID.getId().toString(), familyObj);
                    voterFamily.setCreatedAt(voterID1.getDateTimeReceived());
                    voterIDService.saveVoterFamily(voterFamily);
                }
            }
        }
        return voterID1;
    }
}
