package com.karza.kyc.util;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.model.EPFOResponse;
import com.karza.kyc.service.EPFORequestService;
import com.karza.kyc.service.EPFOResponseService;

public class EPFOResponseHelper {

	@Autowired
	EPFORequestService epfoService;
	@Autowired
	EPFOResponseService epfoResponseService;

	static JSONObject epfoApiResponseNew = null;
	static EPFOResponse epfoResponseObject;
	static EPFORequest epfoRequestObject;

	public static EPFORequest saveEPFODetails(EPFORequest epfoObject, List<JSONObject> epfoApiResponse, Long customerId,
			EPFORequestService epfoService, EPFOResponseService epfoResponseService) throws JSONException {
		
		String epfoAPIResponseStatus;
		/*To get the status from the response where document is invalid */
		epfoAPIResponseStatus = epfoApiResponse.get(0).getJSONObject("epf").getString("statusAsPerSource");
		/*TO get the API Count, for any request how many times it has hit the API*/
		Integer apiCount = Integer.valueOf(epfoApiResponse.get(0).getJSONObject("epf").getString("apiCount"));

		if (epfoApiResponse == null || epfoApiResponse.size() < 1) {
			epfoObject.setAPICount(apiCount);
			epfoObject.setStatus(epfoAPIResponseStatus);
			epfoObject.setApiValidation(epfoAPIResponseStatus);
			epfoObject.setStatusAsPerSource(epfoAPIResponseStatus);
			epfoService.update(epfoObject);
		} else if (epfoApiResponse.size() >= 1) {
			for (int i = 0; i < epfoApiResponse.size(); i++) {
				epfoApiResponseNew = epfoApiResponse.get(i);
				epfoResponseObject = new EPFOResponse(epfoObject.getDocument(),
						(JSONObject) KYCUtil.checkKeyAndGetValueFromJSON(epfoApiResponseNew, "epf"));
				epfoResponseObject.setEpfoRequestID(epfoObject.getId());
				epfoResponseService.save(epfoResponseObject);
			}
		}

		epfoObject.setAPICount(apiCount);
		epfoObject.setStatus(epfoAPIResponseStatus);
		epfoObject.setApiValidation(epfoAPIResponseStatus);
		epfoObject.setStatusAsPerSource(epfoAPIResponseStatus);
		epfoService.update(epfoObject);
		return epfoRequestObject;
	}
}