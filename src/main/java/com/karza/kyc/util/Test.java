package com.karza.kyc.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

import com.karza.kyc.model.BulkVerification;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageUri;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.microsoft.windowsazure.core.utils.Utility;

/**
 * Created by Varun_Prakash on 07-12-2016.
 */
public class Test {

	// Returns maximum repeating element in arr[0..n-1].
	// The array elements are in range from 0 to k-1
	/*
	 * static int maxRepeating(int arr[], int n, int k) { // Iterate though
	 * input array, for every element // arr[i], increment arr[arr[i]%k] by k
	 * for (int i = 0; i< n; i++) arr[(arr[i]%k)] += k;
	 * 
	 * // Find index of the maximum repeating element int max = arr[0], result =
	 * 0; for (int i = 1; i < n; i++) { if (arr[i] > max) { max = arr[i]; result
	 * = i; } }
	 * 
	 * Uncomment this code to get the original array back for (int i = 0; i< n;
	 * i++) arr[i] = arr[i]%k;
	 * 
	 * // Return index of the maximum element return result; }
	 * 
	 * private static String getKey(){ String key = "\"12ed34fgddfg\""; return
	 * key; }
	 * 
	 * 
	 * Driver function to check for above function public static void main
	 * (String[] args) { String abc = getKey();
	 * System.out.println("Key   "+abc);
	 * 
	 * // int arr[] = {2, 3, 3, 5, 3, 4, 1, 7}; // int n = arr.length; // int
	 * k=8; // System.out.println("Maximum repeating element is: " + //
	 * maxRepeating(arr,n,k));
	 * 
	 * String aadhaarID="76767"; String name="ALOK KUMAR"; String dob="1975611";
	 * String pin="763123"; String state="MAHA"; String ghStr="{"+
	 * "\"aadhaar-id\":\""+aadhaarID+"\","+
	 * "\"location\": {\"type\": \"pincode\",\"pincode\": \"560067\"},"+
	 * "\"modality\": \"demo\",\"certificate-type\": \"preprod\","+
	 * "\"demographics\": {"+ "\"name\": {"+
	 * "\"matching-strategy\": \"exact\","+ "\"name-value\": \""+name +"\"" +
	 * "},"+ "\"dob\":  {"+ "\"format\":\"YYYYMMDD\","+
	 * "\"dob-value\":\""+dob+"\""+ "},"+ "\"address-format\": \"structured\","+
	 * "\"address-structured\":{"+ "\"state\":\""+state+"\","+
	 * "\"pincode\":\""+pin+"\""+ "}"+ "}"+ "}"; System.out.println(ghStr);
	 * 
	 * 
	 * }
	 */

	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_DELIMETER = "\n";
	private static final String File_Header = "id,firstName,lastName,document,gender,age";

	public static void writeCSVFile(String fileName) {

		BulkVerification bulkVerification1 = new BulkVerification(1, "Varun", "PRAKASH", "PAN", "M", 24);
		BulkVerification bulkVerification2 = new BulkVerification(2, "Omkar", "Shirhatti", "PAN", "M", 30);
		BulkVerification bulkVerification3 = new BulkVerification(3, "Gaurav", "Samdaria", "AADHAR", "M", 30);
		BulkVerification bulkVerification4 = new BulkVerification(4, "ALOK", "Kumar", "PAN", "M", 27);
		BulkVerification bulkVerification5 = new BulkVerification(5, "Ankit", "SHAH", "PAN", "M", 24);

		List<BulkVerification> bulkList = new ArrayList<>();
		bulkList.add(bulkVerification1);
		bulkList.add(bulkVerification2);
		bulkList.add(bulkVerification3);
		bulkList.add(bulkVerification4);
		bulkList.add(bulkVerification5);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName);
			fileWriter.append(File_Header.toString());
			fileWriter.append(NEW_LINE_DELIMETER);
			for (BulkVerification bulk : bulkList) {
				fileWriter.append(String.valueOf(bulk.getId()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(bulk.getFirstName()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(bulk.getLastName()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(bulk.getDocument()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(bulk.getGender()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(bulk.getAge()));
				fileWriter.append(NEW_LINE_DELIMETER);
			}
			System.out.println("CSV file was created successfully !!!");
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");

			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	private static final int ID = 0;
	private static final int FNAME = 1;
	private static final int LNAME = 2;
	private static final int DOCUMENT = 3;
	private static final int GENDER = 4;
	private static final int AGE = 5;

	public static void readCSVFile(String fileName) {
		BufferedReader fileReader = null;
		try {
			List<BulkVerification> bulkVerifications = new ArrayList<>();
			String line = "";
			fileReader = new BufferedReader(new FileReader(fileName));
			fileReader.readLine();
			while ((line = fileReader.readLine()) != null) {
				String[] tokens = line.split(COMMA_DELIMITER);
				if (tokens.length > 0) {
					BulkVerification verification = new BulkVerification(Long.parseLong(tokens[ID]), tokens[FNAME],
							tokens[LNAME], tokens[DOCUMENT], tokens[GENDER], Integer.parseInt(tokens[AGE]));
					bulkVerifications.add(verification);
				}
			}
			for (BulkVerification bulkVerification : bulkVerifications) {
				System.out.println(bulkVerification.toString());
			}
		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}

	}

	private static void createQueue() {
		try {
			String storageConnectionString = ApiCallHelperUtil.getAzureCloudKey();
			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
			System.out.println("Obtained Handle to Storage Account ");
			// Create the queue client.
			CloudQueueClient queueClient = storageAccount.createCloudQueueClient();
			System.out.println("Created Queue Client");
			// Retrieve a reference to a queue.
			CloudQueue queue = queueClient.getQueueReference("myqueue1");
			System.out.println("Obtained a ref to Queue");
			// Create the queue if it doesn't already exist.

			queue.createIfNotExists();
			System.out.println("Created Queue");
		} catch (Exception e) {
			// Output the stack trace.
			e.printStackTrace();
		}
	}

	private static void putMessage() {
		try {
			String storageConnectionString = ApiCallHelperUtil.getAzureCloudKey();

			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

			// Create the queue client.
			CloudQueueClient queueClient = storageAccount.createCloudQueueClient();

			// Retrieve a reference to a queue.
			CloudQueue queue = queueClient.getQueueReference("myqueue");
			Iterable<CloudQueueMessage> cloudQuemsgs = queue.retrieveMessages(1);
			System.out.println("Total messages ");
			for (CloudQueueMessage cqm : cloudQuemsgs) {
				System.out.println("Hi");
				System.out.println(
						cqm.getMessageId() + " " + cqm.getDequeueCount() + " " + cqm.getMessageContentAsString());
			}
			System.out.println("Obtained Ref to CloudQu ");
			// Create the queue if it doesn't already exist.
			queue.createIfNotExists();
			System.out.println("Going tos add message ");
			// Create a message and add it to the queue.
			CloudQueueMessage message = new CloudQueueMessage("Hello, World Again ");
			queue.addMessage(message);
			System.out.println("Added msg");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void testBlob() {

		// Creating the directory to store file
		// String rootPath = System.getProperty("user.home");
		String filePath = "F:\\Upload File\\tests.csv";

		String storageConnectionString = ApiCallHelperUtil.getAzureCloudKey();
		// Retrieve storage account from connection-string.
		CloudStorageAccount storageAccount;
		try {
			storageAccount = CloudStorageAccount.parse(storageConnectionString);

			// Create the blob client.
			CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
			System.out.println("Blob Client Caeated ");

			// Retrieve reference to a previously created container.
			CloudBlobContainer container = blobClient.getContainerReference("totalkycwebappstoragecontainer");

			// Define the path to a local file.

			// Create or overwrite the "myimage.jpg" blob with contents from a
			// local file.
			CloudBlockBlob blob = container.getBlockBlobReference(filePath);
			StorageUri storageURI = blob.getQualifiedStorageUri();
			System.out.println(storageURI.getPrimaryUri().toString());
			// Storage blob.getQualifiedStorageUri()
			File source = new File(filePath);
			blob.upload(new FileInputStream(source), source.length());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		testBlob();
		//createQueue();
		// putMessage();
		// String fileName =
		// System.getProperty("C://Users//Varun_Prakash//Desktop")+"/varun.csv";
		// String fileName = System.getProperty("user.home")+"/Sample1.csv";
		// System.out.println("FileName --> "+fileName);
		// String fileName = "F:\\Upload File\\Aadhar.csv";
		// System.out.println("Write CSV file");
		// Test.writeCSVFile(fileName);
		// System.out.println("\n Read CSV file");
		// Test.readCSVFile(fileName);
	}

}
