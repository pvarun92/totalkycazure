package com.karza.kyc.service;

import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.UserSessionDetail;

import java.util.List;

/**
 * Created by Fallon Software on 4/14/2016.
 */
public interface UserMasterService {
    UserMaster get(Long id);

    UserMaster save(UserMaster userMaster);

    void update(UserMaster userMaster);

    void delete(UserMaster userMaster);

    List<UserMaster> findAll();

    List<UserMaster> findByCustomerId(Long customerMasterId);

    List<UserMaster> findByCustomerIdAndRole(Long customerMasterId, String role);

    List<UserMaster> findByRole(String role);

    UserMaster getUserByUsernameAndPassword(String userName, String password);

    UserMaster getUserByUsernameAndPasswordForChangePassword(String userName, String password);

    String IsExistUser(String email, String contactNo);

    String IsEmailExist(String email);

    String IsContactExist(String contactNo);

    Integer getMaxUserAccountNumber(Long customerMasterId);

    UserMaster getUserByUsername(String username);

    List<UserSessionDetail> getInactiveUserList();

    List<UserMaster> getUserIdAndEmailByPendingEntities();

    Integer replaceLocation(Long newLocation, Long oldLocation);

    Integer getUserCountByCustomerId(Long customerMasterId);

    List<UserMaster> getAll();
    
    List<UserMaster> getUsersOfCustomer(String customerName);

	List<BranchMaster> getBranchOfCustomer(String customerName);
}
