package com.karza.kyc.service;

import java.util.List;

import com.karza.kyc.model.ESICContriDetails;

public interface ESICContriService {
	// To get the details related to the ESICContriDetails
		ESICContriDetails get(Long id);
		
		// To save the Details of the Frequency for any Particular ESIC ID personnel
		ESICContriDetails save(ESICContriDetails contriDetails);
		// To Update the details of the Frequency for any Particular ESIC ID personnel
		void update(ESICContriDetails contriDetails);
		// To Delete the details of the Frequency for any Particular ESIC ID personnel
		void delete(ESICContriDetails contriDetails);
		// To fetch all the details of the Frequency for all ESIC Id Personnel
		List<ESICContriDetails> findAll();
		//Fetch the list clubbed together for a particular ESICRequest Id
		List<ESICContriDetails> getByESICReqId(Long id);

}
