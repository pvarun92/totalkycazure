package com.karza.kyc.service;

import com.karza.kyc.model.Passport;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface PassportService {
    Passport get(Long id);

    void save(Passport passport);

    void update(Passport passport);

    Passport getByCustomerId(Long CustomerId);

    void delete(Passport passport);

    List<Passport> findAll();
}
