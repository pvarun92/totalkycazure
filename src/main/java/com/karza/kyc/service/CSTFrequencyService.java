package com.karza.kyc.service;

import com.karza.kyc.model.CSTFrequency;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
public interface CSTFrequencyService {
    CSTFrequency get(Long id);

    List<CSTFrequency> getByCstId(Long cstId);

    void save(CSTFrequency cstFrequency);

    void update(CSTFrequency cstFrequency);

    void delete(CSTFrequency cstFrequency);

    List<CSTFrequency> findAll();
}
