package com.karza.kyc.service;

import com.karza.kyc.model.VATFrequency;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
public interface VATFrequencyService {
    VATFrequency get(Long id);

    List<VATFrequency> getByVatId(Long vatId);

    void save(VATFrequency vat);

    void update(VATFrequency vat);

    void delete(VATFrequency vat);

    List<VATFrequency> findAll();
}
