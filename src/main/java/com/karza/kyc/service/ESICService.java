package com.karza.kyc.service;

import java.util.List;

import com.karza.kyc.model.ESIC;

public interface ESICService {

	// Fetch the Related Data of the ESIC
	ESIC get(Long id);
	
	//Save the Data related to ESIC
	ESIC save(ESIC esic);
	// Update the Data related to ESIC
	void update(ESIC esic);
	// Delete the data related to ESIC
	void delete(ESIC esic);
	// List all the data of the ESIC
	List<ESIC> findAll();
	//Fetch the ESIC data related to Customer Id
	ESIC getByCustomerId(Long customerId);
	
}
