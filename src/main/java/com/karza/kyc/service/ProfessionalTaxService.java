package com.karza.kyc.service;

import com.karza.kyc.model.ProfessionalTax;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
public interface ProfessionalTaxService {
    ProfessionalTax get(Long id);

    ProfessionalTax getByCustomerId(Long CustomerId);

    ProfessionalTax save(ProfessionalTax professionalTax);

    void update(ProfessionalTax professionalTax);

    void delete(ProfessionalTax professionalTax);

    List<ProfessionalTax> findAll();
}
