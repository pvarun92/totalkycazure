package com.karza.kyc.service;

import com.karza.kyc.model.FCRN;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface FCRNService {
    FCRN get(Long id);

    void save(FCRN fcrn);

    void update(FCRN fcrn);

    FCRN getByCustomerId(Long CustomerId);

    void delete(FCRN fcrn);

    List<FCRN> findAll();
}
