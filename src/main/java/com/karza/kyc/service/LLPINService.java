package com.karza.kyc.service;

import com.karza.kyc.model.LLPIN;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface LLPINService {
    LLPIN get(Long id);

    LLPIN getByCustomerId(Long CustomerId);

    void save(LLPIN llpin);

    void update(LLPIN llpin);

    void delete(LLPIN llpin);

    List<LLPIN> findAll();
}
