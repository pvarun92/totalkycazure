package com.karza.kyc.service;

import com.karza.kyc.model.CustomerMaster;

import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public interface CustomerMasterService {
    CustomerMaster get(Long id);

    CustomerMaster save(CustomerMaster customerMaster);

    void update(CustomerMaster customerMaster);

    void delete(CustomerMaster customerMaster);

    List<CustomerMaster> findAll();

    List<CustomerMaster> findAllNew();

    Integer getMaxAccountNumber();

    String changeEmailDomain(Long customer_id, String old_domain, String new_domain);

    List<CustomerMaster> getAll();

}
