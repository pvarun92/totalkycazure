package com.karza.kyc.service;

import java.util.List;

import com.karza.kyc.model.LPG;

public interface LPGService {

	// Fetch the LPG data
	LPG get(Long id);
	// Save the LPG Related Data to the DB
	LPG save(LPG lpg);
	// Update the LPG related to LPG Number
	void update(LPG lpg);
	// delete the lpg data
	void delete(LPG lpg);
	// List all the LPG number
	List<LPG> findAll();
	// fetch the data related to any particular customer 
	LPG getByCustomerId(Long customerId);
	
	
}
