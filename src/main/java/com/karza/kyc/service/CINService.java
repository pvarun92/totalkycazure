package com.karza.kyc.service;

import com.karza.kyc.model.CIN;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public interface CINService {
    CIN get(Long id);

    void save(CIN cin);

    void update(CIN panCard);

    CIN getByCustomerId(Long CustomerId);

    void delete(CIN cin);

    List<CIN> findAll();
}
