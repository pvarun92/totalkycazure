package com.karza.kyc.service;

import com.karza.kyc.model.AccountUser;

import java.util.List;

/**
 * Created by Fallon Software on 4/14/2016.
 */
public interface AccountUserService {

    AccountUser get(Long id);

    void save(AccountUser accountUser);

    void update(AccountUser accountUser);

    void delete(AccountUser accountUser);

    List<AccountUser> findAll();

//    AccountUser findByAccountUserName(String accountUserName);
}
