package com.karza.kyc.service;

import com.karza.kyc.model.FLLPIN;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
public interface FLLPINService {
    FLLPIN get(Long id);

    FLLPIN getByCustomerId(Long CustomerId);

    void save(FLLPIN fllpin);

    void update(FLLPIN fllpin);

    void delete(FLLPIN fllpin);

    List<FLLPIN> findAll();
}
