package com.karza.kyc.service;

import com.karza.kyc.model.IEC;
import com.karza.kyc.model.IEC_Branch;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface IECService {
    IEC get(Long id);

    void save(IEC iec);

    void update(IEC iec);

    void delete(IEC iec);

    List<IEC> findAll();

    IEC getByCustomerId(Long CustomerId);

    void saveIECBranch(IEC_Branch iec_branch);

    List<IEC_Branch> getBranchByIECID(Long iecID);

    void deleteBranch(IEC_Branch iec_branch);
}
