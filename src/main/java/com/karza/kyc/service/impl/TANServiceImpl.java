package com.karza.kyc.service.impl;

import com.karza.kyc.dao.TANDao;
import com.karza.kyc.model.TAN;
import com.karza.kyc.service.TANService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class TANServiceImpl implements TANService {


    @Autowired
    TANDao tanDao;

    @Override
    @Transactional(readOnly = true)
    public TAN get(Long id) {
        return tanDao.get(id);
    }

    @Override
    @Transactional
    public void save(TAN tan) {
        tanDao.save(tan);
    }

    @Override
    @Transactional
    public void delete(TAN tan) {
        tanDao.delete(tan);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TAN> findAll() {
        return tanDao.findAll();
    }
}
