package com.karza.kyc.service.impl;

import com.karza.kyc.dao.UserSessionDetailDao;
import com.karza.kyc.model.UserSessionDetail;
import com.karza.kyc.service.UserSessionDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Admin on 7/12/2016.
 */
@Service
public class UserSessionDetailServiceImpl implements UserSessionDetailService {

    @Autowired
    UserSessionDetailDao userSessionDetailDao;

    @Override
    @Transactional
    public Long save(UserSessionDetail userSessionDetail) {
        return userSessionDetailDao.save(userSessionDetail);
    }

    @Override
    @Transactional
    public void update(UserSessionDetail userSessionDetail) {
//        userSessionDetail.setUpdatedAt(new Date());
        userSessionDetailDao.update(userSessionDetail);
    }

    @Override
    @Transactional
    public void delete(UserSessionDetail userSessionDetail) {
        userSessionDetailDao.delete(userSessionDetail);
    }

    @Override
    @Transactional
    public Integer getLoggedInUserCount(Long userId) {
        return userSessionDetailDao.getLoggedInUserCount(userId);
    }

    @Override
    @Transactional
    public UserSessionDetail getBySessionId(String sessionId) {
        return userSessionDetailDao.getBySessionId(sessionId);
    }

    @Override
    @Transactional
    public UserSessionDetail getByUserId(Long userId) {
        return userSessionDetailDao.getByUserId(userId);
    }

    @Override
    @Transactional
    public UserSessionDetail getLastLoggedInUserById(Long userId) {
        return userSessionDetailDao.getLastLoggedInUserById(userId);
    }
}
