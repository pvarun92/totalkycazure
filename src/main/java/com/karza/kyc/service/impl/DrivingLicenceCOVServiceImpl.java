package com.karza.kyc.service.impl;

import com.karza.kyc.dao.DrivingLicenceCOVDao;
import com.karza.kyc.model.DrivingLicenceCOV;
import com.karza.kyc.service.DrivingLicenceCOVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Service
public class DrivingLicenceCOVServiceImpl implements DrivingLicenceCOVService {

    @Autowired
    DrivingLicenceCOVDao drivingLicenceCOVDao;

    @Override
    @Transactional(readOnly = true)
    public DrivingLicenceCOV get(Long id) {
        return drivingLicenceCOVDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DrivingLicenceCOV> getByLicence(Long CustomerId) {
        return drivingLicenceCOVDao.getByLicence(CustomerId);
    }

    @Override
    @Transactional
    public void save(DrivingLicenceCOV drivingLicenceCOV) {
        drivingLicenceCOVDao.save(drivingLicenceCOV);
    }

    @Override
    @Transactional
    public void update(DrivingLicenceCOV drivingLicenceCOV) {
        drivingLicenceCOVDao.update(drivingLicenceCOV);
    }

    @Override
    @Transactional
    public void delete(DrivingLicenceCOV drivingLicenceCOV) {
        drivingLicenceCOVDao.delete(drivingLicenceCOV);

    }

    @Override
    @Transactional(readOnly = true)
    public List<DrivingLicenceCOV> findAll() {
        return drivingLicenceCOVDao.findAll();
    }
}
