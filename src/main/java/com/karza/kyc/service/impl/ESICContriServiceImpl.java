package com.karza.kyc.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karza.kyc.dao.ESICContriDao;
import com.karza.kyc.model.ESICContriDetails;
import com.karza.kyc.service.ESICContriService;

@Service
public class ESICContriServiceImpl implements ESICContriService{

	@Autowired
	ESICContriDao esicContriDao;
	
	@Override
	@Transactional (readOnly = true)
	public ESICContriDetails get(Long id) {
		return esicContriDao.get(id);
	}

	@Override
	@Transactional
	public ESICContriDetails save(ESICContriDetails contriDetails) {
		return esicContriDao.save(contriDetails);
	}

	@Override
	@Transactional
	public void update(ESICContriDetails contriDetails) {
		esicContriDao.update(contriDetails);
	}

	@Override
	@Transactional
	public void delete(ESICContriDetails contriDetails) {
		esicContriDao.delete(contriDetails);
	}

	@Override
	@Transactional (readOnly = true)
	public List<ESICContriDetails> findAll() {
		return esicContriDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<ESICContriDetails> getByESICReqId(Long id) {
		return esicContriDao.getByESICReqId(id);
	}

}
