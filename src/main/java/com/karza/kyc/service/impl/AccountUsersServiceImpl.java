package com.karza.kyc.service.impl;

import com.karza.kyc.dao.AccountUserDao;
import com.karza.kyc.model.AccountUser;
import com.karza.kyc.service.AccountUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/26/2016.
 */
@Service
public class AccountUsersServiceImpl implements AccountUsersService {
    @Autowired
    AccountUserDao accountUserDao;

    @Override
    @Transactional(readOnly = true)
    public AccountUser get(Long id) {
        return accountUserDao.get(id);
    }

    @Override
    @Transactional
    public void save(AccountUser accountUser) {
        accountUserDao.save(accountUser);
    }

    @Override
    @Transactional
    public void update(AccountUser accountUser) {
        accountUserDao.update(accountUser);
    }

    @Override
    @Transactional
    public void delete(AccountUser accountUser) {
        accountUserDao.delete(accountUser);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AccountUser> findAll() {
        return accountUserDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<AccountUser> findByAccount(Long accountID) {
        return accountUserDao.findByAccount(accountID);
    }
}