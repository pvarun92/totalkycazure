package com.karza.kyc.service.impl;

import com.karza.kyc.dao.PassportDao;
import com.karza.kyc.model.Passport;
import com.karza.kyc.service.PassportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class PassportServiceImpl implements PassportService {

    @Autowired
    PassportDao passPortDao;

    @Override
    @Transactional(readOnly = true)
    public Passport get(Long id) {
        return passPortDao.get(id);
    }

    @Override
    @Transactional
    public void save(Passport passport) {
        passPortDao.save(passport);
    }

    @Override
    @Transactional
    public void delete(Passport passport) {
        passPortDao.delete(passport);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Passport> findAll() {
        return passPortDao.findAll();
    }

    @Override
    @Transactional
    public void update(Passport passport) {
        passPortDao.update(passport);
    }

    @Override
    @Transactional(readOnly = true)
    public Passport getByCustomerId(Long CustomerId) {
        return passPortDao.getByCustomerId(CustomerId);
    }
}
