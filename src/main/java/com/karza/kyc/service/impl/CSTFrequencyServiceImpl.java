package com.karza.kyc.service.impl;

import com.karza.kyc.dao.CSTFrequencyDao;
import com.karza.kyc.model.CSTFrequency;
import com.karza.kyc.service.CSTFrequencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class CSTFrequencyServiceImpl implements CSTFrequencyService {
    @Autowired
    CSTFrequencyDao cstFrequencyDao;

    @Transactional(readOnly = true)
    @Override
    public CSTFrequency get(Long id) {
        return cstFrequencyDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CSTFrequency> getByCstId(Long cstId) {
        return cstFrequencyDao.getByCstId(cstId);
    }

    @Override
    @Transactional
    public void save(CSTFrequency cstFrequency) {
        cstFrequencyDao.save(cstFrequency);

    }

    @Override
    @Transactional
    public void update(CSTFrequency cstFrequency) {
        cstFrequencyDao.update(cstFrequency);

    }

    @Override
    @Transactional
    public void delete(CSTFrequency cstFrequency) {
        cstFrequencyDao.delete(cstFrequency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CSTFrequency> findAll() {
        return cstFrequencyDao.findAll();
    }
}
