package com.karza.kyc.service.impl;

import com.karza.kyc.dao.SubscriptionMasterDao;
import com.karza.kyc.model.SubscriptionMaster;
import com.karza.kyc.service.SubscriptionMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
@Service
public class SubscriptionMasterServiceImpl implements SubscriptionMasterService {
    @Autowired
    SubscriptionMasterDao subscriptionMasterDao;

    @Override
    @Transactional(readOnly = true)
    public SubscriptionMaster get(Long id) {
        return subscriptionMasterDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public SubscriptionMaster getByCustomerId(Long CustomerId) {
        return subscriptionMasterDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(SubscriptionMaster subscriptionMaster) {
        subscriptionMaster.setCreatedAt(new Date());
        subscriptionMaster.setUpdatedAt(new Date());
        subscriptionMasterDao.save(subscriptionMaster);
    }

    @Override
    @Transactional
    public void update(SubscriptionMaster subscriptionMaster) {
        subscriptionMaster.setUpdatedAt(new Date());
        subscriptionMasterDao.update(subscriptionMaster);
    }

    @Override
    @Transactional
    public void delete(SubscriptionMaster subscriptionMaster) {
        subscriptionMasterDao.delete(subscriptionMaster);
    }

    @Override
    public List<SubscriptionMaster> findAll() {
        return subscriptionMasterDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubscriptionMaster> findByCustomerId(Long customerMasterId) {
        return subscriptionMasterDao.findByCustomerId(customerMasterId);
    }

    @Override
    public List<SubscriptionMaster> getSubscriptionsByCustomerMasterId(Long customerMasterId) {
        return subscriptionMasterDao.getSubscriptionsByCustomerMasterId(customerMasterId);
    }

}
