package com.karza.kyc.service.impl;

import com.karza.kyc.dao.VATFrequencyDao;
import com.karza.kyc.model.VATFrequency;
import com.karza.kyc.service.VATFrequencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class VATFrequencyServiceImpl implements VATFrequencyService {
    @Autowired
    VATFrequencyDao vatFrequencyDao;

    @Transactional(readOnly = true)
    @Override
    public VATFrequency get(Long id) {
        return vatFrequencyDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VATFrequency> getByVatId(Long vatId) {
        return vatFrequencyDao.getByVatId(vatId);
    }

    @Override
    @Transactional
    public void save(VATFrequency vatFrequency) {
        vatFrequencyDao.save(vatFrequency);

    }

    @Override
    @Transactional
    public void update(VATFrequency vatFrequency) {
        vatFrequencyDao.update(vatFrequency);

    }

    @Override
    @Transactional
    public void delete(VATFrequency vatFrequency) {
        vatFrequencyDao.delete(vatFrequency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VATFrequency> findAll() {
        return vatFrequencyDao.findAll();
    }
}
