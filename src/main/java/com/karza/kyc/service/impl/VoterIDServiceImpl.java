package com.karza.kyc.service.impl;

import com.karza.kyc.dao.VoterIDDao;
import com.karza.kyc.model.VoterFamily;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.service.VoterIDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class VoterIDServiceImpl implements VoterIDService {
    @Autowired
    VoterIDDao voterIDDao;

    @Override
    @Transactional(readOnly = true)
    public VoterID get(Long id) {
        return voterIDDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public VoterID getByCustomerId(Long CustomerId) {
        return voterIDDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(VoterID voterID) {
        voterIDDao.save(voterID);
    }

    @Override
    @Transactional
    public void update(VoterID voterID) {
        voterIDDao.update(voterID);
    }

    @Override
    @Transactional
    public void delete(VoterID voterID) {
        voterIDDao.delete(voterID);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VoterID> findAll() {
        return voterIDDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<VoterFamily> getFamilyByVoterID(String voterID) {
        return voterIDDao.getFamilyByVoterID(voterID);
    }

    @Override
    public void deleteFamily(VoterFamily voterFamily) {
        voterIDDao.deleteFamily(voterFamily);
    }

    @Override
    public void saveVoterFamily(VoterFamily voterFamily) {
        voterIDDao.saveVoterFamily(voterFamily);
    }
}
