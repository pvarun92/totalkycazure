package com.karza.kyc.service.impl;

import com.karza.kyc.dao.ServiceTaxDao;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.service.ServiceTaxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class ServiceTaxServiceImpl implements ServiceTaxService {

    @Autowired
    ServiceTaxDao serviceTaxDao;


    @Override
    @Transactional(readOnly = true)
    public ServiceTax get(Long id) {
        return serviceTaxDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ServiceTax getByCustomerId(Long CustomerId) {
        return serviceTaxDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(ServiceTax serviceTax) {
        serviceTax.setCreatedAt(new Date());
        serviceTaxDao.save(serviceTax);
    }

    @Override
    @Transactional
    public void update(ServiceTax serviceTax) {
        serviceTaxDao.update(serviceTax);
    }

    @Override
    @Transactional
    public void delete(ServiceTax serviceTax) {
        serviceTaxDao.delete(serviceTax);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ServiceTax> findAll() {
        return serviceTaxDao.findAll();
    }
}
