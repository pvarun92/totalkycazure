package com.karza.kyc.service.impl;

import com.karza.kyc.dao.CustomerEntityDao;
import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.service.CustomerEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CustomerEntityServiceImpl implements CustomerEntityService {

    @Autowired
    CustomerEntityDao customerEntityDao;

    @Override
    @Transactional(readOnly = true)
    public CustomerEntity get(Long id) {
        return customerEntityDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerEntity> getByRequestId(Long request_id) {
        return customerEntityDao.getByRequestId(request_id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerEntity> getByEntityStatus(String entity_status) {
        return customerEntityDao.getByEntityStatus(entity_status);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerEntity> getByBranchId(Long branchId) {
        return customerEntityDao.getByBranchId(branchId);
    }

    @Override
    public List<CustomerEntity> getEntitiesByBranchAndFilter(MisReportDTO misReportDTO, HttpSession httpSession) {
        List<CustomerEntity> filteredCustomerEntities = customerEntityDao.getEntitiesByBranchAndFilter(misReportDTO, httpSession);
        return filteredCustomerEntities;
    }

    /*Method to return the values based on the filter made on the branches*/
    @Override
    public List<CustomerEntity> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO, HttpSession httpSession) {

        List<CustomerEntity> filteredEntities = customerEntityDao.getEntitiesByBranchAndFilterAdmin(misReportDTO, httpSession);
        return filteredEntities;
    }

    @Override
    public List<CustomerEntity> getEntitiesByUserMasterId(Long userMaster_id) {
        List<CustomerEntity> filteredCustomerEntities = customerEntityDao.getEntitiesByUserMasterId(userMaster_id);
        return filteredCustomerEntities;
    }

    @Override
    public List<CustomerEntity> getIdAndUserMasterIdByRequestStatus(Long userMasterId) {
        List<CustomerEntity> filteredCustomerEntities = customerEntityDao.getIdAndUserMasterIdByRequestStatus(userMasterId);
        return filteredCustomerEntities;
    }

    @Override
    public Integer mergeBranchId(Long newBranchId, Long oldBrnachId1, Long oldBrnchId2) {
        Integer UpdatedEntities_count = customerEntityDao.mergeBranchId(newBranchId, oldBrnachId1, oldBrnchId2);
        return UpdatedEntities_count;
    }

    @Override
    public Integer replaceBranchId(Long oldBranchId, Long id) {
        Integer UpdatedEntities_count = customerEntityDao.replaceBranchId(oldBranchId, id);
        return UpdatedEntities_count;
    }

    @Override
    public List<CustomerEntity> getByCustomerMasterId(Long customerMasterId) {
        List<CustomerEntity> filteredCustomerEntities = customerEntityDao.getByCustomerMasterId(customerMasterId);
        return filteredCustomerEntities;
    }


    @Override
    @Transactional
    public CustomerEntity save(CustomerEntity customerEntity) {
        return customerEntityDao.save(customerEntity);
    }

    @Override
    @Transactional
    public void update(CustomerEntity customerEntity) {
        customerEntityDao.update(customerEntity);
    }

    @Override
    @Transactional
    public void delete(CustomerEntity customerEntity) {
        customerEntityDao.delete(customerEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerEntity> findAll() {
        return customerEntityDao.findAll();
    }
}
