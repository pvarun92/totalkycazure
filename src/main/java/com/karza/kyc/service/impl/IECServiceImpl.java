package com.karza.kyc.service.impl;

import com.karza.kyc.dao.IECDao;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.IEC_Branch;
import com.karza.kyc.service.IECService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Service
public class IECServiceImpl implements IECService {

    @Autowired
    IECDao iecDao;

    @Override
    @Transactional(readOnly = true)
    public IEC get(Long id) {
        return iecDao.get(id);
    }

    @Override
    @Transactional
    public void save(IEC iec) {
        iec.setCreatedAt(new Date());
        iecDao.save(iec);
    }

    @Override
    @Transactional
    public void update(IEC iec) {
        iecDao.update(iec);
    }

    @Override
    @Transactional
    public void delete(IEC iec) {
        iecDao.delete(iec);

    }

    @Override
    @Transactional(readOnly = true)
    public IEC getByCustomerId(Long CustomerId) {
        return iecDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<IEC> findAll() {
        return iecDao.findAll();
    }

    @Override
    @Transactional
    public void saveIECBranch(IEC_Branch iec_branch) {
        iec_branch.setCreatedAt(new Date());
        iecDao.saveIECBranch(iec_branch);
    }

    @Override
    public List<IEC_Branch> getBranchByIECID(Long iecID) {
        return iecDao.getBranchByIECID(iecID);
    }

    @Override
    @Transactional
    public void deleteBranch(IEC_Branch iec_branch) {
        iecDao.deleteBranch(iec_branch);

    }
}
