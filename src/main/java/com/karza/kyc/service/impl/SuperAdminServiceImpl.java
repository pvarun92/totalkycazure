package com.karza.kyc.service.impl;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karza.kyc.dao.SuperAdminDao;
import com.karza.kyc.dto.MisReportDTO;

import com.karza.kyc.model.SuperAdmin;
import com.karza.kyc.service.SuperAdminService;

@Service
public class SuperAdminServiceImpl implements SuperAdminService{
	@Autowired
	SuperAdminDao superAdminDao;
	@Transactional
	@Override
	public List<SuperAdmin> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO, HttpSession httpSession) {

		List<SuperAdmin> filteredEntities = superAdminDao.getEntitiesByBranchAndFilterAdmin(misReportDTO, httpSession);
        
		return filteredEntities;
	}

}
