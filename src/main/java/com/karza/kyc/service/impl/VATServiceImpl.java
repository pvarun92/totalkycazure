package com.karza.kyc.service.impl;

import com.karza.kyc.dao.VATDao;
import com.karza.kyc.model.VAT;
import com.karza.kyc.service.VATService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class VATServiceImpl implements VATService {
    @Autowired
    VATDao vatDao;

    @Transactional(readOnly = true)
    @Override
    public VAT get(Long id) {
        return vatDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public VAT getByCustomerId(Long CustomerId) {
        return vatDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public VAT save(VAT vat) {
        vat.setCreatedAt(new Date());
        return vatDao.save(vat);
    }

    @Override
    @Transactional
    public void update(VAT vat) {
        vatDao.update(vat);

    }

    @Override
    @Transactional
    public void delete(VAT vat) {
        vatDao.delete(vat);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VAT> findAll() {
        return vatDao.findAll();
    }
}
