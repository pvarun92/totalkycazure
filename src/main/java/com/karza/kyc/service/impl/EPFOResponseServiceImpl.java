package com.karza.kyc.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karza.kyc.dao.EPFOResponseDao;
import com.karza.kyc.model.EPFOResponse;
import com.karza.kyc.service.EPFOResponseService;

@Service
public class EPFOResponseServiceImpl implements EPFOResponseService {

	@Autowired 
	EPFOResponseDao epfoResponseDao;
	
	@Override
	@Transactional (readOnly = true)
	public EPFOResponse get(Long id) {
		return epfoResponseDao.get(id);
	}

	@Override
	@Transactional
	public EPFOResponse save(EPFOResponse epfoResponse) {
		return epfoResponseDao.save(epfoResponse);
	}

	@Override
	@Transactional
	public void update(EPFOResponse epfoResponse) {
		epfoResponseDao.update(epfoResponse);
	}

	@Override
	@Transactional
	public void delete(EPFOResponse epfoResponse) {
		epfoResponseDao.delete(epfoResponse);
	}

	@Override
	@Transactional (readOnly = true)
	public List<EPFOResponse> findAll() {
		return epfoResponseDao.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public EPFOResponse getByCustomerId(Long customerId) {
		return epfoResponseDao.getByCustomerId(customerId);
	}

	@Override
	   @Transactional(readOnly = true)
	public List<EPFOResponse> getByEPFOReqId(Long id) {
		// TODO Auto-generated method stub
		return epfoResponseDao.getByEPFOReqId(id);
	}

}
