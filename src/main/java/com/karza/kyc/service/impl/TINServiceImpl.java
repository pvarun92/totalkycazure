package com.karza.kyc.service.impl;

import com.karza.kyc.dao.TINDao;
import com.karza.kyc.model.TIN;
import com.karza.kyc.service.TINService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Service
public class TINServiceImpl implements TINService {

    @Autowired
    TINDao tinDao;

    @Override
    @Transactional(readOnly = true)
    public TIN get(Long id) {
        return tinDao.get(id);
    }

    @Override
    @Transactional
    public void save(TIN tin) {
        tinDao.save(tin);
    }

    @Override
    @Transactional
    public void delete(TIN tin) {
        tinDao.delete(tin);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TIN> findAll() {
        return tinDao.findAll();
    }
}
