package com.karza.kyc.service.impl;

import com.karza.kyc.dao.UserMasterDao;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.UserSessionDetail;
import com.karza.kyc.service.UserMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class UserMasterServiceImpl implements UserMasterService {

    @Autowired
    UserMasterDao userMasterDao;

    @Override
    @Transactional(readOnly = true)
    public UserMaster get(Long id) {
        return userMasterDao.get(id);
    }

    @Override
    @Transactional
    public void delete(UserMaster userMaster) {
        userMasterDao.delete(userMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserMaster> findAll() {
        return userMasterDao.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public List<UserMaster> findByCustomerId(Long customerMasterId) {
        return userMasterDao.findByCustomerId(customerMasterId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserMaster> findByCustomerIdAndRole(Long customerMasterId, String role) {
        return userMasterDao.findByCustomerIdAndRole(customerMasterId, role);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserMaster> findByRole(String role) {
        return userMasterDao.findByRole(role);
    }

    @Override
    public UserMaster getUserByUsernameAndPassword(String userName, String password) {
        return userMasterDao.getUserByUsernameAndPassword(userName, password);
    }

    @Override
    public UserMaster getUserByUsername(String username) {
        return userMasterDao.getUserByUsername(username);
    }

    @Override
    @Transactional
    public List<UserSessionDetail> getInactiveUserList() {
        return userMasterDao.getInactiveUserList();
    }

    @Override
    public List<UserMaster> getUserIdAndEmailByPendingEntities() {
        return userMasterDao.getUserIdAndEmailByPendingEntities();
    }

    @Override
    public Integer replaceLocation(Long newLocation, Long oldLocation) {
        Integer count = userMasterDao.replaceLocation(newLocation, oldLocation);
        return count;
    }

    @Override
    public Integer getUserCountByCustomerId(Long customerMasterId) {
        return userMasterDao.getUserCountByCustomerId(customerMasterId);
    }

    @Override
    public UserMaster getUserByUsernameAndPasswordForChangePassword(String userName, String password) {
        return userMasterDao.getUserByUsernameAndPasswordForChangePassword(userName, password);
    }

    @Override
    @Transactional
    public UserMaster save(UserMaster userMaster) {
        userMaster.setCreatedAt(new Date());
        userMaster.setUpdatedAt(new Date());
        userMaster.setRemainingLimit(0);
        return userMasterDao.save(userMaster);
    }

    @Override
    @Transactional
    public void update(UserMaster userMaster) {
        userMaster.setUpdatedAt(new Date());
        userMasterDao.update(userMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public String IsExistUser(String email, String contactNo) {
        return userMasterDao.IsExistUser(email, contactNo);
    }

    @Override
    @Transactional(readOnly = true)
    public String IsEmailExist(String email) {
        return userMasterDao.IsEmailExist(email);
    }

    @Override
    @Transactional(readOnly = true)
    public String IsContactExist(String contactNo) {
        return userMasterDao.IsContactExist(contactNo);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getMaxUserAccountNumber(Long customerMasterId) {
        Integer maxUserAccountNumber = userMasterDao.getMaxUserAccountNumber(customerMasterId);
        return maxUserAccountNumber;
    }
    @Override
    @Transactional
    public List<UserMaster> getUsersOfCustomer(String customerName){
    	return userMasterDao.getUsersOfCustomer(customerName);
    }

    @Override
    @Transactional
    public List<BranchMaster> getBranchOfCustomer(String customerName){
    	return userMasterDao.getBranchOfCustomer(customerName);
    }
    
    //
    @Override
    @Transactional
    public List<UserMaster> getAll() {

        return userMasterDao.getAll();
        /*List<UserMaster> userMasters = new ArrayList<UserMaster>();
        UserMaster userMaster = new UserMaster();
        try {
            String myDriver = "com.mysql.jdbc.Driver";
            String url ="jdbc:mysql://localhost:3306/instakyc_db";
            Class.forName(myDriver);
            Connection connection = DriverManager.getConnection(url,"root","root");
            String query = "select * from  user_master";
            Statement statement = connection.createStatement();
            ResultSet rs;
            rs= statement.executeQuery(query);
            while (rs.next()){
                userMaster.setId(rs.getLong("id"));
                userMaster.setAccessRights(rs.getString("access_rights"));
                userMaster.setApplication(rs.getString("application"));
                userMaster.setContactNumber(rs.getString("contact_number"));
                userMaster.setCreatedAt(rs.getDate("created_at"));
                userMaster.setCreatedBy(rs.getString("created_by"));
                userMaster.setCustomerMasterId(rs.getLong("customer_master_id"));
                userMaster.setDesignation(rs.getString("designation"));
                userMaster.setEmail(rs.getString("email"));
                userMaster.setEmployeeId(rs.getString("employee_id"));
                userMaster.setActive(rs.getBoolean("is_active"));
                userMaster.setLocation(rs.getLong("location"));
                userMaster.setLocationRights(rs.getString("location_rights"));
                userMaster.setLoginCount(rs.getInt("login_count"));
                userMaster.setUserName(rs.getString("user_name"));
                userMaster.setUserRole(rs.getString("user_role"));
                userMasters.add(userMaster);

            }
            statement.close();

            return userMasters;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        //return userMasterDao.getAll();
		return userMasters;

    }*/
    }

}
