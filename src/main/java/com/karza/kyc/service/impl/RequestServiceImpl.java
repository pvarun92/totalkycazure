package com.karza.kyc.service.impl;

import com.karza.kyc.dao.RequestDao;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.Request;
import com.karza.kyc.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class RequestServiceImpl implements RequestService {

    @Autowired
    RequestDao requestDao;

    @Override
    @Transactional(readOnly = true)
    public Request get(Long id) {
        return requestDao.get(id);
    }

    @Override
    @Transactional
    public Request save(Request request) {
        return requestDao.save(request);
    }

    @Override
    @Transactional
    public void update(Request request) {
        requestDao.update(request);
    }

    @Override
    @Transactional
    public void delete(Request request) {
        requestDao.delete(request);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Request> findAll() {
        return requestDao.findAll();
    }

    @Override
    public List<Request> getRequestsByRequestStatus(String request_status) {
        return requestDao.getRequestsByRequestStatus(request_status);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerEntity> getEntitiesByRequestStatus(String request_status) {
        return requestDao.getEntitiesByRequestStatus(request_status);
    }

    @Override
    public List<CustomerEntity> getEntityByBranchesAndStatus(List<BranchMaster> branchMasters, String request_status, HttpSession httpSession) {
        List<CustomerEntity> customerEntityList = new ArrayList<>();
        for (int i = 0; i < branchMasters.size(); i++) {
            List<CustomerEntity> customerEntities = requestDao.getEntitiesByBranchAndStatus(branchMasters.get(i).getId(), request_status, httpSession);
            if (customerEntities != null) {
                customerEntityList.addAll(customerEntities);
            }
        }
        return customerEntityList;
    }

    @Override
    public Integer getRequsetCountByCustomerMasterId(Long customerMasterId) {
        Integer count = requestDao.getRequsetCountByCustomerMasterId(customerMasterId);
        return count;
    }

    @Override
    public Integer getSubmittedEntitesByCustomerMasterId(Long customerMasterId) {
        Integer count = requestDao.getSubmittedEntitesByCustomerMasterId(customerMasterId);
        return count;
    }

    @Override
    public Integer getPendingEntitesByCustomerMasterId(Long customerMasterId) {
        Integer count = requestDao.getPendingEntitesByCustomerMasterId(customerMasterId);
        return count;
    }

    @Override
    public Integer getTotalEntitesByCustomerMasterId(Long customerMasterId) {
        Integer count = requestDao.getTotalEntitesByCustomerMasterId(customerMasterId);
        return count;
    }

    @Override
    public Integer getRequsetCountByUserMasterId(Long userMasterId) {
        Integer count = requestDao.getRequsetCountByUserMasterId(userMasterId);
        return count;
    }

    @Override
    public Integer getSubmittedEntitesByUserMasterId(Long userMasterId) {
        Integer count = requestDao.getSubmittedEntitesByUserMasterId(userMasterId);
        return count;
    }

    @Override
    public Integer getPendingEntitesByUserMasterId(Long userMasterId) {
        Integer count = requestDao.getPendingEntitesByUserMasterId(userMasterId);
        return count;
    }

    @Override
    public Integer getTotalEntitesByUserMasterId(Long userMasterId) {
        Integer count = requestDao.getTotalEntitesByUserMasterId(userMasterId);
        return count;
    }

    @Override
    public Integer getRequestCountOfAllUser() {
        Integer count = requestDao.getRequestCountOfAllUser();
        return count;
    }

    @Override
    public Integer getPendingCountofAllUser() {
        Integer count = requestDao.getPendingCountofAllUser();
        return count;
    }

    @Override
    public Integer getSubmittedCountofAllUser() {
        Integer count = requestDao.getSubmittedCountofAllUser();
        return count;
    }
    
    @Override
    public Integer getDraftCountofAllUser(){
    	Integer count = requestDao.getDraftCountofAllUser();
    	return count;
    }
    
    @Override
    public Integer getTotalValidatedDocument(){
    	Integer count = requestDao.getTotalValidatedDocument();
    	return count;
    }

}
