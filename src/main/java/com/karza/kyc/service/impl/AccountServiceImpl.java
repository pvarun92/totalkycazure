package com.karza.kyc.service.impl;

import com.karza.kyc.dao.AccountDao;
import com.karza.kyc.model.Account;
import com.karza.kyc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountDao accountDao;

    @Override
    @Transactional(readOnly = true)
    public Account get(Long id) {
        return accountDao.get(id);
    }

    @Override
    @Transactional
    public void delete(Account accountUser) {
        accountDao.delete(accountUser);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Account> findAll() {
        return accountDao.findAll();
    }

    @Override
    @Transactional
    public void save(Account accountUser) {
        accountDao.save(accountUser);
    }

    @Override
    @Transactional
    public void update(Account accountUser) {
        accountDao.update(accountUser);
    }

//	@Override
//	@Transactional(readOnly = true)
//	public Account findByAccountUserName(String accountUserName) {
//		return accountUserDao.findByAccountUserName(accountUserName);
//	}
}