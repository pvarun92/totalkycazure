package com.karza.kyc.service.impl;

import com.karza.kyc.dao.DrivingLicenceMasterDao;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.service.DrivingLicenceMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Service
public class DrivingLicenceMasterServiceImpl implements DrivingLicenceMasterService {

    @Autowired
    DrivingLicenceMasterDao drivingLicenceMasterDao;

    @Override
    @Transactional(readOnly = true)
    public DrivingLicenceMaster get(Long id) {
        return drivingLicenceMasterDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public DrivingLicenceMaster getByCustomerId(Long CustomerId) {
        return drivingLicenceMasterDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public DrivingLicenceMaster save(DrivingLicenceMaster drivingLicenceMaster) {
        drivingLicenceMaster.setCreatedAt(new Date());
        return drivingLicenceMasterDao.save(drivingLicenceMaster);
    }

    @Override
    @Transactional
    public void update(DrivingLicenceMaster drivingLicenceMaster) {
        drivingLicenceMasterDao.update(drivingLicenceMaster);
    }

    @Override
    @Transactional
    public void delete(DrivingLicenceMaster drivingLicenceMaster) {
        drivingLicenceMasterDao.delete(drivingLicenceMaster);

    }

    @Override
    @Transactional(readOnly = true)
    public List<DrivingLicenceMaster> findAll() {
        return drivingLicenceMasterDao.findAll();
    }
}
