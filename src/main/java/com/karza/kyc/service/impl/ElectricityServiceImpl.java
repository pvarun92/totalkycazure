package com.karza.kyc.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.karza.kyc.dao.ElectricityDao;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.service.ElectricityService;

@Service
public class ElectricityServiceImpl implements ElectricityService {

	@Autowired
	ElectricityDao electricityDao;
	
	@Override
	@Transactional (readOnly = true)
	public Electricity get(Long id) {
		return electricityDao.get(id);
	}

	@Override
	@Transactional
	public Electricity save(Electricity electricity) {	
		return	electricityDao.save(electricity);
		
	}

	@Override
	@Transactional
	public void update(Electricity electricity) {
		electricityDao.update(electricity);
		
	}

	@Override
	@Transactional
	public void delete(Electricity electricity) {
		electricityDao.delete(electricity);
		
	}

	@Override
	@Transactional (readOnly = true)
	public List<Electricity> findAll() {
		return electricityDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Electricity getByCustomerId(Long customerId) {
		return electricityDao.getByCustomerId(customerId);
	}
	

}
