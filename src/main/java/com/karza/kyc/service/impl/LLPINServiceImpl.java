package com.karza.kyc.service.impl;

import com.karza.kyc.dao.LLPINDao;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.service.LLPINService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Service
public class LLPINServiceImpl implements LLPINService {
    @Autowired
    LLPINDao llpinDao;


    @Override
    @Transactional(readOnly = true)
    public LLPIN get(Long id) {
        return llpinDao.get(id);
    }

    @Override
    @Transactional
    public void save(LLPIN llpin) {
        llpin.setCreatedAt(new Date());
        llpinDao.save(llpin);
    }

    @Override
    @Transactional
    public void update(LLPIN llpin) {
        llpinDao.update(llpin);
    }

    @Override
    @Transactional
    public void delete(LLPIN llpin) {
        llpinDao.delete(llpin);

    }

    @Override
    @Transactional(readOnly = true)
    public List<LLPIN> findAll() {
        return llpinDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public LLPIN getByCustomerId(Long CustomerId) {
        return llpinDao.getByCustomerId(CustomerId);
    }
}
