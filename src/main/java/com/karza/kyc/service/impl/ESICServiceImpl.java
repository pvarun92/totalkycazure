package com.karza.kyc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.karza.kyc.dao.ESICDao;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.service.ESICService;

@Service
public class ESICServiceImpl implements ESICService{

	@Autowired
	ESICDao esicDao;
	
	@Override
	@Transactional (readOnly = true)
	public ESIC get(Long id) {
		return esicDao.get(id);
	}

	@Override
	@Transactional
	public ESIC save(ESIC esic) {
		return esicDao.save(esic);
	}

	@Override
	@Transactional
	public void update(ESIC esic) {
		esicDao.update(esic);
	}

	@Override
	@Transactional
	public void delete(ESIC esic) {
		esicDao.delete(esic);
	}

	@Override
	@Transactional (readOnly = true)
	public List<ESIC> findAll() {
		return esicDao.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public ESIC getByCustomerId(Long customerId) {

		return esicDao.getByCustomerId(customerId);
	}

}
