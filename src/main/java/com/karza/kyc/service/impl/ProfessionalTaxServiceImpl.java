package com.karza.kyc.service.impl;

import com.karza.kyc.dao.ProfessionalTaxDao;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.service.ProfessionalTaxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class ProfessionalTaxServiceImpl implements ProfessionalTaxService {
    @Autowired
    ProfessionalTaxDao professionalTaxDao;

    @Transactional(readOnly = true)
    @Override
    public ProfessionalTax get(Long id) {
        return professionalTaxDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ProfessionalTax getByCustomerId(Long CustomerId) {
        return professionalTaxDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public ProfessionalTax save(ProfessionalTax professionalTax) {
        professionalTax.setCreatedAt(new Date());
        return professionalTaxDao.save(professionalTax);
    }

    @Override
    @Transactional
    public void update(ProfessionalTax professionalTax) {
        professionalTaxDao.update(professionalTax);

    }

    @Override
    @Transactional
    public void delete(ProfessionalTax professionalTax) {
        professionalTaxDao.delete(professionalTax);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfessionalTax> findAll() {
        return professionalTaxDao.findAll();
    }
}
