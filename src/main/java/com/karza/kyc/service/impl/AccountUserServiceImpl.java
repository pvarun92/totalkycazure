package com.karza.kyc.service.impl;

import com.karza.kyc.dao.AccountUserDao;
import com.karza.kyc.model.AccountUser;
import com.karza.kyc.service.AccountUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountUserServiceImpl implements AccountUserService {

    @Autowired
    AccountUserDao accountUserDao;

    @Override
    @Transactional(readOnly = true)
    public AccountUser get(Long id) {
        return accountUserDao.get(id);
    }

    @Override
    @Transactional
    public void delete(AccountUser accountUser) {
        accountUserDao.delete(accountUser);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AccountUser> findAll() {
        return accountUserDao.findAll();
    }

    @Override
    @Transactional
    public void save(AccountUser accountUser) {
        accountUserDao.save(accountUser);
    }

    @Override
    @Transactional
    public void update(AccountUser accountUser) {
        accountUserDao.update(accountUser);
    }

//	@Override
//	@Transactional(readOnly = true)
//	public AccountUser findByAccountUserName(String accountUserName) {
//		return accountUserDao.findByAccountUserName(accountUserName);
//	}
}
