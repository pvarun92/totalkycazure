package com.karza.kyc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.karza.kyc.dao.LPGDao;
import com.karza.kyc.model.LPG;
import com.karza.kyc.service.LPGService;

@Service
public class LPGServiceImpl implements LPGService {

	@Autowired
	LPGDao lpgDao;

	@Override
	@Transactional(readOnly = true)
	public LPG get(Long id) {
		return lpgDao.get(id);
	}

	@Override
	@Transactional
	public LPG save(LPG lpg) {
		return lpgDao.save(lpg);
	}

	@Override
	@Transactional
	public void update(LPG lpg) {
		lpgDao.update(lpg);
	}

	@Override
	@Transactional
	public void delete(LPG lpg) {
		lpgDao.delete(lpg);
	}

	@Override
	@Transactional(readOnly = true)
	public List<LPG> findAll() {
		return lpgDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public LPG getByCustomerId(Long customerId) {
		return lpgDao.getByCustomerId(customerId);
	}

}
