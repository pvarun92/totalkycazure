package com.karza.kyc.service.impl;

import com.karza.kyc.dao.LocationMasterDao;
import com.karza.kyc.model.LocationMaster;
import com.karza.kyc.service.LocationMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LocationMasterServiceImpl implements LocationMasterService {

    @Autowired
    LocationMasterDao locationMasterDao;

    @Override
    @Transactional(readOnly = true)
    public LocationMaster get(Long id) {
        return locationMasterDao.get(id);
    }

    @Override
    @Transactional
    public void delete(LocationMaster locationMaster) {
        locationMasterDao.delete(locationMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocationMaster> findAll() {
        return locationMasterDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocationMaster> findByCustomerId(Long customerMasterId) {
        return locationMasterDao.findByCustomerId(customerMasterId);
    }

    @Override
    public List<LocationMaster> getChildLocations(Long locationId) {
        return locationMasterDao.getChildLocations(locationId);
    }

    @Override
    @Transactional
    public void save(LocationMaster locationMaster) {
        locationMasterDao.save(locationMaster);
    }

    @Override
    @Transactional
    public LocationMaster saveAndGet(LocationMaster locationMaster) {
        return locationMasterDao.saveAndGet(locationMaster);
    }

    @Override
    public LocationMaster getParentLocation(Long customerMasterId) {
        return locationMasterDao.getParentLocation(customerMasterId);
    }

    @Override
    @Transactional
    public void update(LocationMaster locationMaster) {
        locationMasterDao.update(locationMaster);
    }

    @Override
    public Integer getUserCountByCustomerId(Long customerMasterId) {
        return locationMasterDao.getUserCountByCustomerId(customerMasterId);
    }

}
