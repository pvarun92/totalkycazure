package com.karza.kyc.service.impl;

import com.karza.kyc.dao.DINDao;
import com.karza.kyc.model.DIN;
import com.karza.kyc.service.DINService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class DINServiceImpl implements DINService {


    @Autowired
    DINDao dinDao;


    @Override
    @Transactional(readOnly = true)
    public DIN get(Long id) {
        return dinDao.get(id);
    }

    @Override
    @Transactional
    public void save(DIN din) {
        dinDao.save(din);
    }

    @Override
    @Transactional
    public void delete(DIN din) {
        dinDao.delete(din);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DIN> findAll() {
        return dinDao.findAll();
    }
}
