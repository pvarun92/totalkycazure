package com.karza.kyc.service.impl;

import com.karza.kyc.dao.ExciseDao;
import com.karza.kyc.model.Excise;
import com.karza.kyc.service.ExciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class ExciseServiceImpl implements ExciseService {


    @Autowired
    ExciseDao exciseDao;

    @Override
    @Transactional(readOnly = true)
    public Excise get(Long id) {
        return exciseDao.get(id);
    }

    @Override
    @Transactional
    public void save(Excise excise) {
        excise.setCreatedAt(new Date());
        exciseDao.save(excise);
    }

    @Override
    @Transactional
    public void update(Excise excise) {
        exciseDao.update(excise);
    }

    @Override
    @Transactional(readOnly = true)
    public Excise getByCustomerId(Long CustomerId) {
        return exciseDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void delete(Excise excise) {
        exciseDao.delete(excise);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Excise> findAll() {
        return exciseDao.findAll();
    }
}
