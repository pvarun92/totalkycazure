package com.karza.kyc.service.impl;

import com.karza.kyc.dao.CSTDao;
import com.karza.kyc.model.CST;
import com.karza.kyc.service.CSTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class CSTServiceImpl implements CSTService {
    @Autowired
    CSTDao cstDao;

    @Transactional(readOnly = true)
    @Override
    public CST get(Long id) {
        return cstDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public CST getByCustomerId(Long CustomerId) {
        return cstDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public CST save(CST cst) {
        cst.setCreatedAt(new Date());
        return cstDao.save(cst);
    }

    @Override
    @Transactional
    public void update(CST cst) {
        cstDao.update(cst);

    }

    @Override
    @Transactional
    public void delete(CST cst) {
        cstDao.delete(cst);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CST> findAll() {
        return cstDao.findAll();
    }
}
