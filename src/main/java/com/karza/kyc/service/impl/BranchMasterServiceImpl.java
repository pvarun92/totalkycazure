package com.karza.kyc.service.impl;

import com.karza.kyc.dao.BranchMasterDao;
import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.LocationMaster;
import com.karza.kyc.service.BranchMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
@Service
public class BranchMasterServiceImpl implements BranchMasterService {
    @Autowired
    BranchMasterDao branchMasterDao;

    @Override
    @Transactional(readOnly = true)
    public BranchMaster get(Long id) {
        return branchMasterDao.get(id);
    }

    @Override
    @Transactional
    public void save(BranchMaster branchMaster) {
        branchMaster.setCreatedAt(new Date());
        branchMaster.setUpdatedAt(new Date());
        branchMaster.setCreatedBy("Anonyms");
        branchMaster.setModifiedBy("Anonyms");
        branchMasterDao.save(branchMaster);
    }

    @Override
    @Transactional
    public void update(BranchMaster branchMaster) {
        branchMaster.setUpdatedAt(new Date());
        branchMaster.setModifiedBy("Anonyms");
        branchMasterDao.update(branchMaster);
    }

    @Override
    @Transactional
    public void delete(BranchMaster branchMaster) {
        branchMasterDao.delete(branchMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BranchMaster> findAll() {
        return branchMasterDao.findAll();
    }

    @Override
    public List<BranchMaster> findByUserLocation(List<LocationMaster> locationMasters) {
        return branchMasterDao.findByUserLocation(locationMasters);
    }

    @Override
    public List<BranchMaster> findByCustomerId(Long customerMasterId) {
        return branchMasterDao.findByCustomerId(customerMasterId);
    }

    @Override
    public BranchMaster createBranch(BranchMaster newBranchMaster) {
        return branchMasterDao.createBranch(newBranchMaster);
    }

    @Override
    public Integer getUserCountByCustomerId(Long customerMasterId) {
        return branchMasterDao.getUserCountByCustomerId(customerMasterId);
    }

    @Override
    public List<BranchMaster> getAll() {
        return branchMasterDao.getAll();
    }
}