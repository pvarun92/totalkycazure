package com.karza.kyc.service.impl;

import com.karza.kyc.dao.OtpGenerateDao;
import com.karza.kyc.model.OtpGenerate;
import com.karza.kyc.service.OtpGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Fallon-Software on 7/19/2016.
 */
@Service
public class OtpGenerateServiceImpl implements OtpGenerateService {
    @Autowired
    OtpGenerateDao otpGenerateDao;

    @Override
    @Transactional(readOnly = true)
    public OtpGenerate get(Long id) {
        return otpGenerateDao.get(id);
    }

    @Override
    public OtpGenerate findByOtp(Long otp) {
        return otpGenerateDao.findByOtp(otp);
    }

    @Override
    @Transactional
    public OtpGenerate createOtpGenerate(OtpGenerate otpGenerate) {
        otpGenerate.setCreatedAt(new Date());
        return otpGenerateDao.createOtpGenerate(otpGenerate);
    }

}

