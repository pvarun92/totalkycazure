package com.karza.kyc.service.impl;

import com.karza.kyc.dao.PanCardDao;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.service.PanCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
@Service
public class PanCardServiceImpl implements PanCardService {

    @Autowired
    PanCardDao panCardDao;

    @Override
    @Transactional(readOnly = true)
    public PanCard get(Long id) {
        return panCardDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public PanCard getByCustomerId(Long CustomerId) {
        return panCardDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(PanCard panCard) {
        panCard.setCreatedAt(new Date());
        panCardDao.save(panCard);
    }

    @Override
    @Transactional
    public void update(PanCard panCard) {
        panCardDao.update(panCard);
    }

    @Override
    @Transactional
    public void delete(PanCard panCard) {
        panCardDao.delete(panCard);

    }

    @Override
    @Transactional(readOnly = true)
    public List<PanCard> findAll() {
        return panCardDao.findAll();
    }
}
