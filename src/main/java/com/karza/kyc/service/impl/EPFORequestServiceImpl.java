package com.karza.kyc.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karza.kyc.dao.EPFORequestDao;
import com.karza.kyc.model.EPFORequest;
import com.karza.kyc.service.EPFORequestService;

@Service
public class EPFORequestServiceImpl implements EPFORequestService{

	@Autowired
	EPFORequestDao epfoDao;
	
	@Override
	@Transactional (readOnly = true)
	public EPFORequest get(Long id) {
		return epfoDao.get(id);
	}

	@Override
	@Transactional
	public EPFORequest Save(EPFORequest epfo) {
		return epfoDao.Save(epfo);
	}

	@Override
	@Transactional
	public void update(EPFORequest epfo) {
		epfoDao.update(epfo);
	}

	@Override
	@Transactional
	public void delete(EPFORequest epfo) {
		epfoDao.delete(epfo);
	}

	@Override
	@Transactional (readOnly = true)
	public List<EPFORequest> findAll() {
		return epfoDao.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public EPFORequest getByCustomerID(Long customerID) {
		return epfoDao.getByCustomerID(customerID);
	}

}
