package com.karza.kyc.service.impl;

import com.karza.kyc.dao.FLLPINDao;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.service.FLLPINService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class FLLPINServiceImpl implements FLLPINService {
    @Autowired
    FLLPINDao fllpinDao;

    @Override
    @Transactional(readOnly = true)
    public FLLPIN get(Long id) {
        return fllpinDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public FLLPIN getByCustomerId(Long CustomerId) {
        return fllpinDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(FLLPIN fllpin) {
        fllpinDao.save(fllpin);
    }

    @Override
    @Transactional
    public void update(FLLPIN fllpin) {
        fllpinDao.update(fllpin);
    }

    @Override
    @Transactional
    public void delete(FLLPIN fllpin) {
        fllpinDao.delete(fllpin);
    }

    @Override
    public List<FLLPIN> findAll() {
        return fllpinDao.findAll();
    }
}
