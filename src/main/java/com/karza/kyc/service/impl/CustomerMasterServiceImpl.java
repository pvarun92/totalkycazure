package com.karza.kyc.service.impl;

import com.karza.kyc.dao.CustomerMasterDao;
import com.karza.kyc.model.CustomerMaster;
import com.karza.kyc.service.CustomerMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/16
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CustomerMasterServiceImpl implements CustomerMasterService {

    @Autowired
    CustomerMasterDao customerMasterDao;

    @Override
    @Transactional(readOnly = true)
    public CustomerMaster get(Long id) {
        return customerMasterDao.get(id);
    }

    @Override
    @Transactional
    public CustomerMaster save(CustomerMaster customerMaster) {
        customerMaster.setCreatedAt(new Date());
        customerMaster.setUpdatedAt(new Date());
        return customerMasterDao.save(customerMaster);
    }

    @Override
    @Transactional
    public void update(CustomerMaster customerMaster) {
        customerMaster.setUpdatedAt(new Date());
        customerMasterDao.update(customerMaster);
    }

    @Override
    @Transactional
    public void delete(CustomerMaster customerMaster) {
        customerMasterDao.delete(customerMaster);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerMaster> findAll() {
        return customerMasterDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerMaster> findAllNew() {
        return customerMasterDao.findAllNew();
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getMaxAccountNumber() {
        Integer maxAccountNumber = customerMasterDao.getMaxAccountNumber();
        return maxAccountNumber;
    }

    @Override
    @Transactional
    public String changeEmailDomain(Long customer_id, String old_domain, String new_domain) {
        String updateEmail = customerMasterDao.changeEmailDomain(customer_id, old_domain, new_domain);
        return updateEmail;
    }

    /*wrote this function in consultation with Shiv, since the lazy loading was not workking,
    * this function is something related to fetch the data from the customer_master to populate
     * it on frontend side.
    * */
    @Override
    @Transactional
    public List<CustomerMaster> getAll() {
        /*List<CustomerMaster> customerMasters = new ArrayList<CustomerMaster>();
        CustomerMaster customerMaster = new CustomerMaster();
        try {
            String myDriver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/instakyc_db";
            Class.forName(myDriver);
            Connection connection = DriverManager.getConnection(url, "root", "root");
            String query = "select * from  customer_master";
            Statement statement = connection.createStatement();
            ResultSet rs;
            rs = statement.executeQuery(query);
            while (rs.next()) {
                String temp;
                customerMaster.setAccountNumber(rs.getInt("account_number"));
                customerMaster.setId(rs.getLong("id"));
                customerMaster.setAddress(rs.getString("address"));
                customerMaster.setAddress1(rs.getString("address1"));
                customerMaster.setContactNumber(rs.getString("contact_number"));
                customerMaster.setCustomerName(rs.getString("customer_name"));
                customerMaster.setContactPerson(rs.getString("contact_person"));
                customerMaster.setDesignation(rs.getString("designation"));
                customerMaster.setCreatedAt(rs.getDate("created_at"));
                customerMaster.setCreditPeriod(rs.getString("credit_period"));
                customerMaster.setCustomerType(rs.getString("customer_type"));
                customerMaster.setEmail(rs.getString("email"));
                customerMaster.setKarzaProducts(rs.getString("karza_products"));
                customerMaster.setPanNumber(rs.getString("pan_number"));
                customerMaster.setPinCode(rs.getString("pin_code"));
                customerMaster.setServiceTaxNumber(rs.getString("service_tax_number"));
                customerMaster.setState(rs.getString("state"));
                customerMaster.setTanNumber(rs.getString("tan_number"));
                customerMaster.setUpdatedAt(rs.getDate("updated_at"));
                temp = rs.getString("customer_name");
                System.out.println(" Hi this is customer " + temp);
                customerMasters.add(customerMaster);
            }
            statement.close();
            return customerMasters;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
      /*  System.out.println("Came here B");
        return customerMasterDao.getAll();*/
        return customerMasterDao.getAll();

    }


}
