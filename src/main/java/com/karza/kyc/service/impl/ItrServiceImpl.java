package com.karza.kyc.service.impl;

import com.karza.kyc.dao.ItrDao;
import com.karza.kyc.model.Itr;
import com.karza.kyc.service.ItrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * Created by Admin on 7/16/2016.
 */

@Service
public class ItrServiceImpl implements ItrService {

    @Autowired
    ItrDao itrDao;

    @Override
    @Transactional(readOnly = true)
    public Itr get(Long id) {
        return itrDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Itr> getByCustomerId(Long CustomerId) {
        return itrDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(Itr itr) {
        itr.setCreatedAt(new Date());
        itrDao.save(itr);
    }

    @Override
    @Transactional
    public void update(Itr itr) {
        itrDao.update(itr);
    }

    @Override
    @Transactional
    public void delete(Itr itr) {
        itrDao.delete(itr);

    }

    @Override
    @Transactional(readOnly = true)
    public List<Itr> findAll() {
        return itrDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Itr> getByPanId(Long panId) {
        return itrDao.getByPanId(panId);
    }
}
