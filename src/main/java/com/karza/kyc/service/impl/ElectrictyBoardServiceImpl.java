package com.karza.kyc.service.impl;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karza.kyc.dao.ElectricityBoardDao;
import com.karza.kyc.model.ElectricityBoard;
import com.karza.kyc.service.ElectricityBoardService;

@Service
public class ElectrictyBoardServiceImpl implements ElectricityBoardService{

	@Autowired
	ElectricityBoardDao electricityBoardDao;

	@Override
	public ElectricityBoard get(Long id) {
		return electricityBoardDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ElectricityBoard> findAll() {
		return electricityBoardDao.findAll();
	}

	@Override
	public void save(ElectricityBoard board) {
		electricityBoardDao.save(board);
	}

	@Override
	public String getBoardName(String electricityBoardCode) {
		return electricityBoardDao.getBoardName(electricityBoardCode);
	}
	

	
}
