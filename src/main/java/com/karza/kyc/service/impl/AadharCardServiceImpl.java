package com.karza.kyc.service.impl;

import com.karza.kyc.dao.AadharCardDao;
import com.karza.kyc.model.AadharCard;
import com.karza.kyc.service.AadharCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
@Service
public class AadharCardServiceImpl implements AadharCardService {

    @Autowired
    AadharCardDao aadharCardDao;

    @Override
    @Transactional(readOnly = true)
    public AadharCard get(Long id) {
        return aadharCardDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public AadharCard getByCustomerId(Long CustomerId) {
        return aadharCardDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void save(AadharCard aadharCard) {
        aadharCard.setCreatedAt(new Date());
        aadharCardDao.save(aadharCard);
    }

    @Override
    @Transactional
    public void update(AadharCard aadharCard) {
        aadharCardDao.update(aadharCard);
    }

    @Override
    @Transactional
    public void delete(AadharCard aadharCard) {
        aadharCardDao.delete(aadharCard);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AadharCard> findAll() {
        return aadharCardDao.findAll();
    }
}
