package com.karza.kyc.service.impl;

import com.karza.kyc.dao.CINDao;
import com.karza.kyc.model.CIN;
import com.karza.kyc.service.CINService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class CINServiceImpl implements CINService {


    @Autowired
    CINDao cinDao;


    @Override
    @Transactional(readOnly = true)
    public CIN get(Long id) {
        return cinDao.get(id);
    }

    @Override
    @Transactional
    public void save(CIN cin) {
        cinDao.save(cin);
    }

    @Override
    @Transactional
    public void delete(CIN cin) {
        cinDao.delete(cin);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CIN> findAll() {
        return cinDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public CIN getByCustomerId(Long CustomerId) {
        return cinDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void update(CIN cin) {
        cinDao.update(cin);
    }
}
