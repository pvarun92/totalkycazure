package com.karza.kyc.service.impl;

import com.karza.kyc.dao.FCRNDao;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.service.FCRNService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
@Service
public class FCRNServiceImpl implements FCRNService {


    @Autowired
    FCRNDao FCRNDao;


    @Override
    @Transactional(readOnly = true)
    public FCRN get(Long id) {
        return FCRNDao.get(id);
    }

    @Override
    @Transactional
    public void save(FCRN FCRN) {
        FCRNDao.save(FCRN);
    }

    @Override
    @Transactional
    public void delete(FCRN FCRN) {
        FCRNDao.delete(FCRN);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FCRN> findAll() {
        return FCRNDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public FCRN getByCustomerId(Long CustomerId) {
        return FCRNDao.getByCustomerId(CustomerId);
    }

    @Override
    @Transactional
    public void update(FCRN FCRN) {
        FCRNDao.update(FCRN);
    }
}
