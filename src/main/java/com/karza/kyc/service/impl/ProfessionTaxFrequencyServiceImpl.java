package com.karza.kyc.service.impl;

import com.karza.kyc.dao.ProfessionalTaxFrequencyDao;
import com.karza.kyc.model.ProfessionalTaxFrequency;
import com.karza.kyc.service.ProfessionalTaxFrequencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
@Service
public class ProfessionTaxFrequencyServiceImpl implements ProfessionalTaxFrequencyService {
    @Autowired
    ProfessionalTaxFrequencyDao professionalTaxFrequencyDao;

    @Transactional(readOnly = true)
    @Override
    public ProfessionalTaxFrequency get(Long id) {
        return professionalTaxFrequencyDao.get(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfessionalTaxFrequency> getByProfessionalTaxId(Long professionalTaxId) {
        return professionalTaxFrequencyDao.getByProfessionalTaxId(professionalTaxId);
    }

    @Override
    @Transactional
    public void save(ProfessionalTaxFrequency professionalTaxFrequency) {
        professionalTaxFrequencyDao.save(professionalTaxFrequency);

    }

    @Override
    @Transactional
    public void update(ProfessionalTaxFrequency professionalTaxFrequency) {
        professionalTaxFrequencyDao.update(professionalTaxFrequency);

    }

    @Override
    @Transactional
    public void delete(ProfessionalTaxFrequency professionalTaxFrequency) {
        professionalTaxFrequencyDao.delete(professionalTaxFrequency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfessionalTaxFrequency> findAll() {
        return professionalTaxFrequencyDao.findAll();
    }
}
