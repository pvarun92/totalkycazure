package com.karza.kyc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.karza.kyc.dao.TelephoneDao;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.service.TelephoneService;

@Service
public class TelephoneServiceImpl implements TelephoneService {
	
	@Autowired
	TelephoneDao telephoneDao;

	@Override
	@Transactional (readOnly = true)
	public Telephone get(Long id) {
		return telephoneDao.get(id);
	}

	@Override
	@Transactional
	public Telephone save(Telephone telephone) {
		return telephoneDao.save(telephone);
	}

	@Override
	@Transactional
	public void update(Telephone telephone) {
		telephoneDao.update(telephone);
	}

	@Override
	@Transactional
	public void Delete(Telephone telephone) {
		telephoneDao.Delete(telephone);
	}

	@Override
	@Transactional (readOnly = true)
	public List<Telephone> findAll() {
		return telephoneDao.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public Telephone getByCustomerId(Long customerId) {
		return telephoneDao.getByCustomerId(customerId);
	}
	
}
