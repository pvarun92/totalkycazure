package com.karza.kyc.service;

import java.util.List;

import com.karza.kyc.model.EPFOResponse;

public interface EPFOResponseService {

	// To get the Detail of any Particular data related to the EPFO
	EPFOResponse get(Long id);

	// To save the EPFOResponse details
	EPFOResponse save(EPFOResponse epfoResponse);

	// To update the EPFOResponse details
	void update(EPFOResponse epfoResponse);

	// To delete the EPFOResponse details
	void delete(EPFOResponse epfoResponse);

	// To findall the EPFOResponse details
	List<EPFOResponse> findAll();

	// Get the EPFOResponse detail with Customer Detail
	EPFOResponse getByCustomerId(Long customerId);
	
	List<EPFOResponse> getByEPFOReqId(Long id);

}
