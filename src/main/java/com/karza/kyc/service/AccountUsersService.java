package com.karza.kyc.service;

import com.karza.kyc.model.AccountUser;

import java.util.List;

/**
 * Created by Fallon on 4/26/2016.
 */

public interface AccountUsersService {

    AccountUser get(Long id);

    void save(AccountUser accountUser);

    void update(AccountUser accountUser);

    void delete(AccountUser accountUser);

    List<AccountUser> findAll();

    List<AccountUser> findByAccount(Long accountID);

//    AccountUser findByAccountUserName(String accountUserName);
}
