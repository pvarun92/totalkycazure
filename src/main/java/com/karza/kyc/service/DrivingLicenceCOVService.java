package com.karza.kyc.service;

import com.karza.kyc.model.DrivingLicenceCOV;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface DrivingLicenceCOVService {
    DrivingLicenceCOV get(Long id);

    List<DrivingLicenceCOV> getByLicence(Long dlId);

    void save(DrivingLicenceCOV drivingLicenceCOV);

    void update(DrivingLicenceCOV drivingLicenceCOV);

    void delete(DrivingLicenceCOV drivingLicenceCOV);

    List<DrivingLicenceCOV> findAll();
}
