package com.karza.kyc.service;

import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.LocationMaster;

import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
public interface BranchMasterService {
    BranchMaster get(Long id);

    void save(BranchMaster branchMaster);

    void update(BranchMaster branchMaster);

    void delete(BranchMaster branchMaster);

    List<BranchMaster> findAll();

    List<BranchMaster> findByUserLocation(List<LocationMaster> locationMasters);

    List<BranchMaster> findByCustomerId(Long customerMasterId);

    BranchMaster createBranch(BranchMaster newBranchMaster);//create branch and return branchId

    Integer getUserCountByCustomerId(Long customerMasterId);

    List<BranchMaster> getAll();
}
