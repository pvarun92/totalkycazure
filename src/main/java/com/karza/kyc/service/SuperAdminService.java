package com.karza.kyc.service;

import java.util.List;

import javax.servlet.http.HttpSession;
import com.karza.kyc.model.*;
import com.karza.kyc.dto.MisReportDTO;

public interface SuperAdminService {
	
	List<SuperAdmin> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO,HttpSession httpSession);

}
