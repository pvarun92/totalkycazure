package com.karza.kyc.service;

import com.karza.kyc.dto.MisReportDTO;
import com.karza.kyc.model.CustomerEntity;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public interface CustomerEntityService {
    CustomerEntity get(Long id);

    CustomerEntity save(CustomerEntity customerEntity);

    void delete(CustomerEntity customerEntity);

    void update(CustomerEntity customerEntity);

    List<CustomerEntity> findAll();

    List<CustomerEntity> getByRequestId(Long request_id);

    List<CustomerEntity> getByEntityStatus(String entity_status);

    List<CustomerEntity> getByBranchId(Long branchId);

    List<CustomerEntity> getEntitiesByBranchAndFilter(MisReportDTO misReportDTO, HttpSession httpSession);

    List<CustomerEntity> getEntitiesByUserMasterId(Long userMaster_id);

    List<CustomerEntity> getIdAndUserMasterIdByRequestStatus(Long userMasterId);

    Integer mergeBranchId(Long newBranchId, Long oldBrnachId1, Long oldBrnchId2);

    Integer replaceBranchId(Long oldBranchId, Long id);

    List<CustomerEntity> getByCustomerMasterId(Long customerMasterId);

    List<CustomerEntity> getEntitiesByBranchAndFilterAdmin(MisReportDTO misReportDTO, HttpSession httpSession);
}
