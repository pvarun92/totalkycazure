package com.karza.kyc.service;

import com.karza.kyc.model.DrivingLicenceMaster;

import java.util.List;

/**
 * Created by Fallon on 3/16/2016.
 */
public interface DrivingLicenceMasterService {
    DrivingLicenceMaster get(Long id);

    DrivingLicenceMaster getByCustomerId(Long CustomerId);

    DrivingLicenceMaster save(DrivingLicenceMaster drivingLicenceMaster);

    void update(DrivingLicenceMaster drivingLicenceMaster);

    void delete(DrivingLicenceMaster drivingLicenceMaster);

    List<DrivingLicenceMaster> findAll();
}
