package com.karza.kyc.service;

import com.karza.kyc.model.AadharCard;

import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public interface AadharCardService {
    AadharCard get(Long id);

    AadharCard getByCustomerId(Long CustomerId);

    void save(AadharCard aadharCard);

    void update(AadharCard aadharCard);

    void delete(AadharCard aadharCard);

    List<AadharCard> findAll();
}
