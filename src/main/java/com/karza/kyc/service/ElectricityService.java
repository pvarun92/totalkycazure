package com.karza.kyc.service;

import java.util.List;

import com.karza.kyc.model.Electricity;

public interface ElectricityService {
	 
    Electricity get(Long id); // Get the Electricity data
    
    Electricity save(Electricity electricity); // Save the Electricity data
  
    void update(Electricity electricity);   //Update the electricity data
    
    void delete(Electricity electricity); //Delete the electricity data
    
    List<Electricity> findAll(); //List all the Electricity bills along with details
    
    Electricity getByCustomerId(Long customerId); //Get the Electricity by Customer ID
}
