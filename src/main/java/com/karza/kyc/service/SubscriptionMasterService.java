package com.karza.kyc.service;

import com.karza.kyc.model.SubscriptionMaster;

import java.util.List;

/**
 * Created by Fallon on 4/28/2016.
 */
public interface SubscriptionMasterService {
    SubscriptionMaster get(Long id);

    SubscriptionMaster getByCustomerId(Long CustomerId);

    void save(SubscriptionMaster subscriptionMaster);

    void update(SubscriptionMaster subscriptionMaster);

    void delete(SubscriptionMaster subscriptionMaster);

    List<SubscriptionMaster> findAll();

    List<SubscriptionMaster> findByCustomerId(Long customerMasterId);

    List<SubscriptionMaster> getSubscriptionsByCustomerMasterId(Long customerMasterId);
}