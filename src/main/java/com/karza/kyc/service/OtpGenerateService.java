package com.karza.kyc.service;

import com.karza.kyc.model.OtpGenerate;

/**
 * Created by Fallon-Software on 7/19/2016.
 */
public interface OtpGenerateService {

    OtpGenerate createOtpGenerate(OtpGenerate otpGenerate);

    OtpGenerate get(Long id);

    OtpGenerate findByOtp(Long otp);
}
