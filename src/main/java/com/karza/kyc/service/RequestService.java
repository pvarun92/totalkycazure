package com.karza.kyc.service;

import com.karza.kyc.model.BranchMaster;
import com.karza.kyc.model.CustomerEntity;
import com.karza.kyc.model.Request;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Amit on 15-Mar-16.
 */
public interface RequestService {
    Request get(Long id);

    Request save(Request request);

    void update(Request request);

    void delete(Request request);

    List<Request> findAll();

    List<Request> getRequestsByRequestStatus(String request_status);

    List<CustomerEntity> getEntitiesByRequestStatus(String request_status);

    List<CustomerEntity> getEntityByBranchesAndStatus(List<BranchMaster> branchMasters, String request_status, HttpSession httpSession);

    Integer getRequsetCountByCustomerMasterId(Long customerMasterId);

    Integer getSubmittedEntitesByCustomerMasterId(Long customerMasterId);

    Integer getPendingEntitesByCustomerMasterId(Long customerMasterId);

    Integer getTotalEntitesByCustomerMasterId(Long customerMasterId);

    Integer getRequsetCountByUserMasterId(Long userMasterId);

    Integer getSubmittedEntitesByUserMasterId(Long userMasterId);

    Integer getPendingEntitesByUserMasterId(Long userMasterId);

    Integer getTotalEntitesByUserMasterId(Long userMasterId);

    Integer getRequestCountOfAllUser();

    Integer getPendingCountofAllUser();

    Integer getSubmittedCountofAllUser();
    
    Integer getDraftCountofAllUser();

	Integer getTotalValidatedDocument();
}
