package com.karza.kyc.service;

import com.karza.kyc.model.User;
import com.karza.kyc.web.UserCommand;
import com.karza.kyc.web.UserGrid;

public interface UserService {

    User get(Long id);

    void save(UserCommand userCommand);

    void delete(User user);

    UserGrid findAll();

    void saveAll(UserGrid userGrid);

    void updateWithAll(UserGrid userGrid);

}
