package com.karza.kyc.service;

import com.karza.kyc.model.CST;

import java.util.List;

/**
 * Created by Fallon on 4/19/2016.
 */
public interface CSTService {
    CST get(Long id);

    CST getByCustomerId(Long CustomerId);

    CST save(CST cst);

    void update(CST cst);

    void delete(CST cst);

    List<CST> findAll();
}
