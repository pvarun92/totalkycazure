package com.karza.kyc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.karza.kyc.apivalidator.ApiValidator;
import com.karza.kyc.apivalidator.PeriodicRequestValidator;
import com.karza.kyc.model.UserMaster;
import com.karza.kyc.model.UserSessionDetail;
import com.karza.kyc.service.UserSessionDetailService;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	ApiValidator apiValidator;
	@Autowired
	UserSessionDetailService userSessionDetailService;
	static final Logger LOG = LoggerFactory.getLogger(AuthenticationInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

					String prefix="/instakyc-1.0.67";
					//String prefix="/instakyc";
					//String prefix="";
					if (!request.getRequestURI().equals(prefix+"/login.do")
					&& !request.getRequestURI().equals(prefix+"/login")
					&& !request.getRequestURI().equals(prefix+"/login.failed")
					&& !request.getRequestURI().equals(prefix+"/forgotPassword")
					&& !request.getRequestURI().equals(prefix+"/generateOTP")
					&& !request.getRequestURI().equals(prefix+"/generateOtpPassword")
					&& !request.getRequestURI().equals(prefix+"/terms")
					&& !request.getRequestURI().equals(prefix+"/disclaimer")
		/*
		 * && !request.getRequestURI().equals("/contactus")
		 */
		) {
			System.out.println("AuthenticationInterceptor : ~~> "+ request.getRequestURI() +" "+new Date());
			//Logger slf4jLogger = LoggerFactory.getLogger(AuthenticationInterceptor.class);
		//	slf4jLogger.warn("AuthenticationInterceptor : ~~> "+ request.getRequestURI() +" "+new Date());

			UserMaster userData = (UserMaster) request.getSession().getAttribute("currentUser");
			UserSessionDetail userSessionDetail = userSessionDetailService.getBySessionId(request.getSession().getId());
			if (userSessionDetail == null) {
				response.sendRedirect("login");
				return false;
			} else {
				LOG.warn("User attempted to Login");
				Date targetTime = DateUtils.addMinutes(userSessionDetail.getUpdatedAt(), 15);
				if (new Date().after(targetTime)) {
					userSessionDetail.setUpdatedAt(new Date());
					userSessionDetail.setUserStatus("InActive");
					userSessionDetailService.update(userSessionDetail);
					request.getSession().invalidate();
					response.sendRedirect("login");
					return false;
				} else {
					String automated_call = request.getParameter("automated_call");
					if (!StringUtils.isNoneBlank(automated_call) && !StringUtils.equals(automated_call, "true")) {
						userSessionDetail.setUpdatedAt(new Date());
						userSessionDetailService.update(userSessionDetail);
					}
				}
			}
			if (userData == null) {
				response.sendRedirect("login");
				return false;
			} else {
				PeriodicRequestValidator.getPeriodicRequestValidator().startPeriodicReader(apiValidator);
				if (request.getRequestURI().equals(prefix+"/sysadmin")) {
					if (!userData.getUserRole().equals("Sysadmin")) {
						response.sendRedirect("");
						return false;
					}
				}
			}
		}
		return true;
	}
}