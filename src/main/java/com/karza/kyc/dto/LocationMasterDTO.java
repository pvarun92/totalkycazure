package com.karza.kyc.dto;

import java.util.Date;

/**
 * Created by Mahananda on 28-April-16.
 */
public class LocationMasterDTO {
    private Long id;
    private Long customerMasterId;
    private Date createdAt;
    private Date updatedAt;
    private String locationOrZone;
    private String linkedBranches;
    private Long parentLocation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLocationOrZone() {
        return locationOrZone;
    }

    public void setLocationOrZone(String locationOrZone) {
        this.locationOrZone = locationOrZone;
    }

    public String getLinkedBranches() {
        return linkedBranches;
    }

    public void setLinkedBranches(String linkedBranches) {
        this.linkedBranches = linkedBranches;
    }

    public Long getParentLocation() {
        return parentLocation;
    }

    public void setParentLocation(Long parentLocation) {
        this.parentLocation = parentLocation;
    }

    public Long getCustomerMasterId() {
        return customerMasterId;
    }

    public void setCustomerMasterId(Long customerMasterId) {
        this.customerMasterId = customerMasterId;
    }
}
