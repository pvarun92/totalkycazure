package com.karza.kyc.dto;

import com.karza.kyc.model.UserMaster;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Admin on 5/3/2016.
 */
public class MisReportDTO {
    String entity_name;
    Long branchId;
    String entity_type;
    Date filter_from;
    Date filter_to;
    String report_status;
    String validity;
    String viewType;
    String customerName;
    String uniqueId;

	public MisReportDTO(Long branchId, String entityName, String entity_type, Date filter_from, Date filter_to, String validity, String report_status) {
        this.entity_name = entityName;
        this.branchId = branchId;
        this.entity_type = entity_type;
        this.filter_from = filter_from;
        this.filter_to = filter_to;
        this.report_status = report_status;
        this.validity = validity;
    }

	public MisReportDTO(String entity_name, Long branchId, String entity_type, Date filter_from, Date filter_to,
			String report_status, String validity, String viewType, String customerName, String uniqueId) {
		this(branchId,entity_name,entity_type,filter_from,filter_to,validity,report_status);
		this.viewType = viewType;
		this.customerName = customerName;
		this.uniqueId = uniqueId;
	}

    public MisReportDTO(String customerName, String uniqueId, Long branchId, String entityName, String entity_type, Date filter_from, Date filter_to, String validity, String report_status) {
    	this(branchId,entityName,entity_type,filter_from,filter_to,validity,report_status);
        this.customerName = customerName;
        this.uniqueId = uniqueId;
    }


    public MisReportDTO() {
    }

    public String getEntity_name() {
        return entity_name;
    }

    public void setEntity_name(String entity_name) {
        this.entity_name = entity_name;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public Date getFilter_from() {
        return filter_from;
    }

    public void setFilter_from(Date filter_from) {
        this.filter_from = filter_from;
    }

    public Date getFilter_to() {
        return filter_to;
    }

    public void setFilter_to(Date filter_to) {
        this.filter_to = filter_to;
    }

    public String getReport_status() {
        return report_status;
    }

    public void setReport_status(String report_status) {
        this.report_status = report_status;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }
    public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

    public String getSQL(HttpSession httpSession) {
        StringBuilder where = new StringBuilder();
        String worklog = httpSession.getAttribute("workLog").toString();
        Long request_id = null;
        try {
            request_id = Long.parseLong(entity_name);
        } catch (Exception e) {
            request_id = null;
        }
        if (StringUtils.isNotEmpty(entity_name) && request_id == null)
            appendToQuery(where, "(c.entity_name like :entityName or c.first_name like :entityName or c.middle_name like :entityName or c.last_name like :entityName)");
        if (request_id != null)
            appendToQuery(where, "r.id =:request_id");
        if (StringUtils.isNotEmpty(entity_type))
            appendToQuery(where, "c.entity_type =:entity_type");
        if (filter_from != null)
            appendToQuery(where, "c.created_date >=:filter_from");
        if (filter_to != null)
            appendToQuery(where, "c.created_date <=:filter_to");
        if (StringUtils.isNoneEmpty(report_status))
            appendToQuery(where, "r.request_status =:report_status");
        if (branchId != null || this.branchId != null) {
            appendToQuery(where, "c.branchId =:branchId");
        }
        if (StringUtils.equalsIgnoreCase(worklog, "MY")) {
            appendToQuery(where, "c.userMaster_id=:userMaster_id");
        }
        appendToQuery(where, "c.entity_status='parent' and r.id = c.request_id");

        StringBuilder sb = new StringBuilder();

        sb.append("select c from CustomerEntity c, Request r ");
        sb.append(" where ").append(where.toString());
        return sb.toString();
    }

    public String getSQL1(HttpSession httpSession) {
        StringBuilder where = new StringBuilder();
        // String worklog = httpSession.getAttribute("workLog").toString();
        Long request_id = null;
        try {
            request_id = Long.parseLong(entity_name);
        } catch (Exception e) {
            request_id = null;
        }
        if (StringUtils.isNotEmpty(entity_type))
            appendToQuery(where, "c.entity_type =:entity_type");
        if (filter_from != null)
            appendToQuery(where, "c.created_date >=:filter_from");
        if (filter_to != null)
            appendToQuery(where, "c.created_date <=:filter_to");
        if (StringUtils.isNoneEmpty(report_status))
            appendToQuery(where, "r.request_status =:report_status");
        if (branchId != null || this.branchId != null)
            appendToQuery(where, "c.branchId =:branchId");
        if (StringUtils.isNotEmpty(customerName))
            appendToQuery(where, "cm.customerName =:customerName");
        if (StringUtils.isNotEmpty(uniqueId))
            appendToQuery(where, "u.uniqueId =:uniqueId");

        appendToQuery(where, "c.entity_status='parent'");

        StringBuilder sb = new StringBuilder();
        String hql ="select  c from Request as r"
        		+ " left join UserMaster as u on r.userMasterId = u.id"
        		+ " left join CustomerMaster as cm on u.customerMasterId = cm.id "
        		+ "left join CustomerEntity as c on r.id = c.request_id";
        sb.append(hql);
        sb.append(" AND ").append(where.toString());
        return sb.toString();
    }


    public void setValues(Query query, HttpSession httpSession) throws Exception {
        Long request_id = null;
        String worklog = httpSession.getAttribute("workLog").toString();
        UserMaster userMaster = (UserMaster) httpSession.getAttribute("currentUser");
        Long current_user_id = userMaster.getId();
        try {
            request_id = Long.parseLong(entity_name);
        } catch (Exception e) {
            request_id = null;
        }
        if (StringUtils.isNotEmpty(entity_name) && request_id == null){
            query.setString("entityName", "%" + entity_name + "%");
            }
        if (request_id != null){
            query.setLong("request_id", request_id);
        }
        if (StringUtils.isNotEmpty(entity_type)){
            query.setString("entity_type", entity_type);
        }
        if (filter_from != null){
            query.setDate("filter_from", filter_from);
        }
        if (filter_to != null){
            query.setDate("filter_to", getDateInFormat(filter_to, true));
        }
        if (StringUtils.isNoneEmpty(report_status)){
            query.setString("report_status", report_status);
        }
        if (branchId != null) {
            query.setLong("branchId", branchId);
        }
        if (this.branchId != null) {
            query.setLong("branchId", this.branchId);
        }
        if (StringUtils.equalsIgnoreCase(worklog, "MY")) {
            //setUsermaster id
            query.setLong("userMaster_id", current_user_id);
        }
    }
    
    public void setValues1(Query query, HttpSession httpSession) throws Exception {
        Long request_id = null;
       
        try {
            request_id = Long.parseLong(entity_name);
        } catch (Exception e) {
            request_id = null;
        }
        if (StringUtils.isNotEmpty(entity_name) && request_id == null){
            query.setString("entityName", "%" + entity_name + "%");
            }
        if (request_id != null){
            query.setLong("request_id", request_id);
        }
        if (StringUtils.isNotEmpty(entity_type)){
            query.setString("entity_type", entity_type);
        }
        if (filter_from != null){
            query.setDate("filter_from", filter_from);
        }
        if (filter_to != null){
            query.setDate("filter_to", getDateInFormat(filter_to, true));
        }
        if (StringUtils.isNoneEmpty(report_status)){
            query.setString("report_status", report_status);
        }
        if (branchId != null) {
            query.setLong("branchId", branchId);
        }
        if (this.branchId != null) {
            query.setLong("branchId", this.branchId);
        }
        System.out.println("Hi Query -->>>"+query.toString());
    }

    public Date getDateInFormat(Date dateObj, boolean endOfDay) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date newDateObj = dateObj;
        if (endOfDay) {
            Calendar c = Calendar.getInstance();
            c.setTime(newDateObj);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            newDateObj = c.getTime();
        }
        return newDateObj;
    }

    private void appendToQuery(StringBuilder sb, String clause) {
        if (sb.length() > 0)
            sb.append(" and ").append(clause);
        else
            sb.append(clause);
    }

    public void formateData() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String dateInString = getFilter_from().toString();
//            setFilter_from(formatter.parse(dateInString));

        } catch (Exception e) {
            setFilter_from(null);
        }

        try {
            String dateInString = getFilter_to().toString();
//            setFilter_to(formatter.parse(dateInString));
            Calendar c = Calendar.getInstance();
            c.setTime(getFilter_to());
            c.add(Calendar.DATE, 1);
            setFilter_to(c.getTime());

        } catch (Exception e) {
            setFilter_to(null);
        }
    }
}
