package com.karza.kyc.dto;

import java.util.Date;

/**
 * Created by Mahananda on 28-April-16.
 */
public class SubscriptionMasterDTO {

    private Long id;
    private String application;
    private Date billingStartDate;
    private String billingPeriodicity;
    private Date billingEndDate;
    private Date contractDate;
    private String oneTimeFees;
    private String perEntityRate;
    private String perUserIdFees;
    private Date validFromDate;
    private Date validTillDate;
    private String clientId;
    private String plan;
    private Date updatedAt;
    private Date createdAt;
    private Long customerMasterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public Date getBillingStartDate() {
        return billingStartDate;
    }

    public void setBillingStartDate(Date billingStartDate) {
        this.billingStartDate = billingStartDate;
    }

    public String getBillingPeriodicity() {
        return billingPeriodicity;
    }

    public void setBillingPeriodicity(String billingPeriodicity) {
        this.billingPeriodicity = billingPeriodicity;
    }

    public Date getBillingEndDate() {
        return billingEndDate;
    }

    public void setBillingEndDate(Date billingEndDate) {
        this.billingEndDate = billingEndDate;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getOneTimeFees() {
        return oneTimeFees;
    }

    public void setOneTimeFees(String oneTimeFees) {
        this.oneTimeFees = oneTimeFees;
    }

    public String getPerEntityRate() {
        return perEntityRate;
    }

    public void setPerEntityRate(String perEntityRate) {
        this.perEntityRate = perEntityRate;
    }

    public String getPerUserIdFees() {
        return perUserIdFees;
    }

    public void setPerUserIdFees(String perUserIdFees) {
        this.perUserIdFees = perUserIdFees;
    }

    public Date getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(Date validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Date getValidTillDate() {
        return validTillDate;
    }

    public void setValidTillDate(Date validTillDate) {
        this.validTillDate = validTillDate;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getCustomerMasterId() {
        return customerMasterId;
    }

    public void setCustomerMasterId(Long customerMasterId) {
        this.customerMasterId = customerMasterId;
    }
}
