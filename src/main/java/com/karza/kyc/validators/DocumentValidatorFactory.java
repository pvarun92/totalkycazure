package com.karza.kyc.validators;

/**
 * Created by Admin on 01-03-2016.
 */
public class DocumentValidatorFactory {
    public static final int SIZE_ERROR = 101;
    public static final int CHARACTER_ERROR = 102;
    public static final int PATTERN_ERROR = 103;
    public static final int INVALID_DATA_ERROR = 104;

    public static DocumentValidator getPanCardValidator() {
        return new PanCardValidator();
    }

    public static DocumentValidator getAadharCardValidator() {
        return new AadharCardValidator();
    }

    public static DocumentValidator getCinValidator() {
        return new CinValidator();
    }

    public static DocumentValidator getExciseValidator() {
        return new ExciseValidator();
    }

    public static DocumentValidator getDrivingLicenceValidator() {
        return new DrivingLicenceValidator();
    }

    public static DocumentValidator getIecValidator() {
        return new IecValidator();
    }

    public static DocumentValidator getLlpinValidator() {
        return new LlpinValidator();
    }

    public static DocumentValidator getFllpinValidator() {
        return new FllpinValidator();
    }

    public static DocumentValidator getFcrnValidator() {
        return new FllpinValidator();
    }

    public static DocumentValidator getServiceTaxValidator() {
        return new ServiceTaxValidator();
    }

    public static DocumentValidator getVoterIdValidator() {
        return new VoterIdValidator();
    }

    public static DocumentValidator getPassportValidator() {
        return new PassportValidator();
    }

    public static DocumentValidator getDinValidator() {
        return new DinValidator();
    }

    public static DocumentValidator getTanValidator() {
        return new TanValidator();
    }
}
