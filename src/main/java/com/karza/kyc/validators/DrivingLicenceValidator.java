package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.DrivingLicenceMaster;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 4/7/16
 * Time: 5:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class DrivingLicenceValidator extends BaseDocumentValidator implements DocumentValidator {

    public DrivingLicenceValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getDrivingLicenceValidator();

        // length validation
        validate("MH01 20090091406", true, validator);
        validate("MH04 20100049006", true, validator);
        validate("HR-31N/24586/10-11", true, validator);
        validate("DL-0420110149646", true, validator);
        validate("MP09N-2016-0088578", true, validator);
        validate("RJ-14/DLC/11/559707", true, validator);


        validate("000", false, validator);
        validate("00000", false, validator);
        validate("00000000", false, validator);
        validate("000000000000", false, validator);
    }

    private static void validate(String drivingLicence, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new DrivingLicenceMaster(drivingLicence));
            System.out.println((valid == isValid) + " for Driving Licence + " + drivingLicence);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for Driving Licence + " + drivingLicence + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        if (isOnlyInvalidChar(document.getDocument(), '0'))
            throw new DocumentValidatorException(DocumentValidatorFactory.INVALID_DATA_ERROR, "Driving Licence card is having invalid data", document.getDocument());

        return true;
    }
}
