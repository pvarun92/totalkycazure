package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.FLLPIN;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class FllpinValidator extends BaseDocumentValidator implements DocumentValidator {

    public FllpinValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getFllpinValidator();

        // length validation is 9
        validate("FAAA-1234", true, validator);
        validate("FAAA-123", false, validator);

        //regular expression validation
        validate("FAAA-1234", true, validator);
        validate("FAAA-12A1", false, validator);
        validate("AAAA-1211", false, validator);

        //6th to 9th character string validation from list
        validate("FAAA-1234", true, validator);
        validate("FAAA-0000", false, validator);

    }

    private static void validate(String fllPin, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new FLLPIN(fllPin));
            System.out.println((valid == isValid) + " for fllPin + " + fllPin);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for fllPin + " + fllPin + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // LLPIN Pattern
        String regularExpression = "[fF][a-zA-Z]{3}\\-\\d{4}";

        if (!isSizeValidExact(document.getDocument(), 9))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "FLLPIN must be 9 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "FLLPIN pattern is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 5, 4))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "FLLPIN invalid data from 6th to 9th", document.getDocument());

        return true;
    }


}
