package com.karza.kyc.validators;

/**
 * Created by Admin on 01-03-2016.
 */
public class DocumentValidatorException extends Exception {
    public final int errorCode;
    public final String errorDescription;
    public final String document;

    public DocumentValidatorException(int errorCode, String errorDescription, String document) {
        super(errorDescription);
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.document = document;
    }
}
