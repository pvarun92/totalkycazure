package com.karza.kyc.validators;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.CIN;
import com.karza.kyc.model.CST;
import com.karza.kyc.model.DrivingLicenceMaster;
import com.karza.kyc.model.ESIC;
import com.karza.kyc.model.Electricity;
import com.karza.kyc.model.Excise;
import com.karza.kyc.model.FCRN;
import com.karza.kyc.model.FLLPIN;
import com.karza.kyc.model.IEC;
import com.karza.kyc.model.Itr;
import com.karza.kyc.model.LLPIN;
import com.karza.kyc.model.LPG;
import com.karza.kyc.model.PanCard;
import com.karza.kyc.model.Passport;
import com.karza.kyc.model.ProfessionalTax;
import com.karza.kyc.model.ServiceTax;
import com.karza.kyc.model.Telephone;
import com.karza.kyc.model.VAT;
import com.karza.kyc.model.VoterID;
import com.karza.kyc.util.VerhoeffHelper;

/**
 * Created with IntelliJ IDEA. User: Vinay Date: 3/22/16 Time: 3:49 PM To change
 * this template use File | Settings | File Templates.
 */
public class ValidatorHelper {

	public String panCardValidation(JSONObject panCardData) throws JSONException {
		JSONObject panData = (JSONObject) panCardData.get("pan");
		Boolean isPanCardChecked = (Boolean) panData.get("check");
		String data = "";
		String panNumber = panData.get("unique_no").toString();
		String entity_type = panCardData.get("entity_type").toString();
		if (isPanCardChecked && panNumber != "") {
			PanCard p = new PanCard(panNumber, entity_type);
			try {
				new PanCardValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public JSONObject itrValidation(JSONObject itr) throws JSONException {
		JSONObject itrData = ((JSONObject) itr.get("itr"));
		Boolean isItrChecked = (Boolean) itrData.get("check");
		JSONArray itr_fields = itrData.getJSONArray("itr_fields");
		String data = "";
		JSONObject errorList = new JSONObject();
		for (int i = 0; i < itr_fields.length(); i++) {
			String acknowledgmentNumber = (itr_fields.getJSONObject(i)).get("acknowledgement_number").toString();
			String dateObj;
			Date dateOfFilling;
			try {
				dateObj = (itr_fields.getJSONObject(i).get("date_of_filling").toString());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				String day = dateObj.split("/")[0];
				String month = dateObj.split("/")[1];
				String year = dateObj.split("/")[2];
				dateOfFilling = formatter.parse(year + "-" + month + "-" + day);
			} catch (Exception e) {
				dateOfFilling = null;
			}
			String assessmentYear = (itr_fields.getJSONObject(i)).get("assesment_year").toString();
			if (isItrChecked && acknowledgmentNumber != "" && assessmentYear != "") {
				Itr itrObj = new Itr(acknowledgmentNumber, assessmentYear, dateOfFilling);
				try {
					new ItrValidator().isValid(itrObj);
				} catch (DocumentValidatorException e) {
					data = e.errorDescription;
				} catch (Exception e) {
					data = e.getMessage();
				}
			}
			errorList.put(acknowledgmentNumber, data.toString());
		}
		return errorList;
	}

	public String aadharCardValidation(JSONObject aadharCardData) throws JSONException {
		JSONObject aadharData = (JSONObject) aadharCardData.get("aadhar");
		Boolean isAadharCardChecked = (Boolean) aadharData.get("check");
		String data = "";
		String aadharNumber = aadharData.get("unique_no").toString();
		if (isAadharCardChecked && aadharNumber != "") {
			AadharCard p = new AadharCard(aadharNumber);
			try {
				new AadharCardValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String serviceTaxCardValidation(JSONObject serviceTaxCardData) throws JSONException {
		JSONObject serviceTaxData = (JSONObject) serviceTaxCardData.get("service_tax");
		Boolean isServiceTaxCardChecked = (Boolean) serviceTaxData.get("check");
		String data = "";
		String serviceTaxNumber = serviceTaxData.get("unique_no").toString();
		if (isServiceTaxCardChecked && serviceTaxNumber != "") {
			ServiceTax p = new ServiceTax(serviceTaxNumber);
			try {
				new ServiceTaxValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String votingCardValidation(JSONObject votingCardData) throws JSONException {
		JSONObject votingData = (JSONObject) votingCardData.get("voting");
		Boolean isVotingCardChecked = (Boolean) votingData.get("check");
		String data = "";
		System.out.println("Hi i'm data from Voting  "+votingData.toString());
		String votingNumber = votingData.get("unique_no").toString();
		if (isVotingCardChecked && votingNumber != "") {
			VoterID p = new VoterID(votingNumber);
			try {
				new VoterIdValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String iecCardValidation(JSONObject iecCardData) throws JSONException {
		JSONObject iecData = (JSONObject) iecCardData.get("iec");
		Boolean isIecCardChecked = (Boolean) iecData.get("check");
		String data = "";
		String iecNumber = iecData.get("unique_no").toString();
		if (isIecCardChecked && iecNumber != "") {
			IEC p = new IEC(iecNumber);
			try {
				new IecValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String exciseCardValidation(JSONObject exciseCardData) throws JSONException {
		JSONObject exciseData = (JSONObject) exciseCardData.get("excise");
		Boolean isExciseCardChecked = (Boolean) exciseData.get("check");
		String data = "";
		String exciseNumber = exciseData.get("unique_no").toString();
		if (isExciseCardChecked && exciseNumber != "") {
			Excise p = new Excise(exciseNumber);
			try {
				new ExciseValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String passportCardValidation(JSONObject passportCardData) throws JSONException {
		JSONObject passportData = (JSONObject) passportCardData.get("passport");
		Boolean isPassportCardChecked = (Boolean) passportData.get("check");
		String data = "";
		String passportNumber = passportData.get("unique_no").toString();
		String country = "INDIA";
		if (isPassportCardChecked && passportNumber != "") {
			Passport p = new Passport(passportNumber, country);
			try {
				new PassportValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	/* Validate the Electricity Bill */
	public String electricityValidation(JSONObject electricityData) throws JSONException {
		JSONObject electricitybill = (JSONObject) electricityData.get("electricity");
		Boolean iselectricityChecked = (Boolean) electricitybill.get("check");
		String data = "";
		String billNumber = electricitybill.get("unique_no").toString();
		String electricityBoard =(String) electricitybill.get("board").toString();
		if (iselectricityChecked) {
			Electricity p = new Electricity();
			p.setBill_number(billNumber);
			p.setElectricityBoardId(electricityBoard);
			try {
				new ElectricityValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String llpinCardValidation(JSONObject llpinCardData) throws JSONException {
		JSONObject llpinData = (JSONObject) llpinCardData.get("llpin");
		Boolean isLlpinCardChecked = (Boolean) llpinData.get("check");
		String data = "";
		String llpinNumber = llpinData.get("unique_no").toString();
		if (isLlpinCardChecked && llpinNumber != "") {
			String firstChars = "";
			Boolean dashContains = llpinNumber.contains("-");

			if (StringUtils.equals(llpinCardData.getString("entity_type").toString(),
					"Limited Liability Partnership (LLP)")) {
				// FLLPIN/LLPIN validation
				if (dashContains) {
					firstChars = llpinNumber.split("-")[0];
					if (llpinNumber.length() == 8) {
						// LLPIN validation
						if (dashContains && llpinNumber.length() <= 8 && firstChars.length() <= 3) {
							LLPIN p = new LLPIN(llpinNumber);
							try {
								new LlpinValidator().isValid(p);
							} catch (DocumentValidatorException e) {
								data = e.errorDescription;
							} catch (Exception e) {
								data = e.getMessage();
							}
						}
					} else if (llpinNumber.length() == 9) {
						// FLLPIN validation
						if (dashContains) {
							FLLPIN p = new FLLPIN(llpinNumber);
							try {
								new FllpinValidator().isValid(p);
							} catch (DocumentValidatorException e) {
								data = e.errorDescription;
							} catch (Exception e) {
								data = e.getMessage();
							}
						}
					} else {
						data = "Re-enter the LLPIN/FLLPIN Number.";
					}
				} else {
					data = "Re-enter the LLPIN/FLLPIN Number.";
				}

			} else {
				// CIN/FCRN validation
				if (dashContains) {
					firstChars = llpinNumber.split("-")[0];
				}

				if (!dashContains && llpinNumber.length() > 6) {
					CIN p = new CIN(llpinNumber);
					try {
						new CinValidator().isValid(p);
					} catch (DocumentValidatorException e) {
						data = e.errorDescription;
					} catch (Exception e) {
						data = e.getMessage();
					}
				} else if (!dashContains && llpinNumber.length() <= 6) {
					FCRN p = new FCRN(llpinNumber);
					try {
						new FcrnValidator().isValid(p);
					} catch (DocumentValidatorException e) {
						data = e.errorDescription;
					} catch (Exception e) {
						data = e.getMessage();
					}
				} else {
					data = "Re-enter the CIN/FCRN Number.";
				}

			}
		}
		return data;
	}

	public String DrivingLicenceCardValidation(JSONObject drivingLicenceCardData) throws JSONException {
		JSONObject drivingLicenceData = (JSONObject) drivingLicenceCardData.get("dl");
		Boolean isDrivingLicenceChecked = (Boolean) drivingLicenceData.get("check");
		String data = "";
		String drivingLicenceNumber = drivingLicenceData.get("unique_no").toString();
		if (isDrivingLicenceChecked && drivingLicenceNumber != "") {
			DrivingLicenceMaster p = new DrivingLicenceMaster(drivingLicenceNumber);
			try {
				new DrivingLicenceValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String VATCardValidation(JSONObject vatCardData) throws JSONException {
		JSONObject vatData = (JSONObject) vatCardData.get("vat");
		Boolean isVatChecked = (Boolean) vatData.get("check");
		String data = "";
		String vatNumber = vatData.get("unique_no").toString();
		if (isVatChecked && vatNumber != "") {
			VAT p = new VAT(vatNumber);
			try {
				new VATValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String CSTCardValidation(JSONObject cstCardData) throws JSONException {
		JSONObject cstData = (JSONObject) cstCardData.get("cst");
		Boolean isVatChecked = (Boolean) cstData.get("check");
		String data = "";
		String cstNumber = cstData.get("unique_no").toString();
		if (isVatChecked && cstNumber != "") {
			CST p = new CST(cstNumber);
			try {
				new CSTValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}

	public String PTCardValidation(JSONObject ptCardData) throws JSONException {
		JSONObject ptData = (JSONObject) ptCardData.get("pt");
		Boolean isVatChecked = (Boolean) ptData.get("check");
		String data = "";
		String ptNumber = ptData.get("unique_no").toString();
		if (isVatChecked && ptNumber != "") {
			ProfessionalTax p = new ProfessionalTax(ptNumber);
			try {
				new ProfessionTaxValidator().isValid(p);
			} catch (DocumentValidatorException e) {
				data = e.errorDescription;
			} catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
	}
	/*
	 * Below Method validates the Telephone check data, whether the checkbox has been checked or not	
	 * data has been provided properly or not
	 */
	
	public String telephoneValidation(JSONObject telephoneBillData) throws JSONException {
		JSONObject telData = (JSONObject) telephoneBillData.get("telephone");
		Boolean isTelephoneChecked = (Boolean) telData.get("check");
		String data = "";
		String telNumber = telData.get("unique_no").toString();
		if(isTelephoneChecked && telNumber != ""){
			Telephone telephone = new Telephone(telNumber);
			try{
				new TelephoneValidator().isValid(telephone);
			}
			catch (DocumentValidatorException e) {
			data = e.errorDescription;
			}
			catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
		
	}
	
	/*
	 * Below Method validates the LPG check data, whether the checkbox has been checked or not	
	 * data has been provided properly or not
	 */
	
	
	public String lpgValidation(JSONObject lpgJSONData) throws JSONException {
		JSONObject lpgData = (JSONObject) lpgJSONData.get("lpg");
		Boolean isLPGChecked = (Boolean) lpgData.get("check");
		String data = "";
		String lpgNumber = lpgData.get("unique_no").toString();
		if(isLPGChecked && lpgNumber != ""){
			LPG lpg = new LPG(lpgNumber);
			try{
				new LPGValidator().isValid(lpg);
			}
			catch (DocumentValidatorException e) {
			data = e.errorDescription;
			}
			catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
		
	}

	/*
	 * Below Method validates the ESIC check data, whether the checkbox has been checked or not	
	 * data has been provided properly or not
	 */
	
	public String esicValidation(JSONObject esicJSONData) throws JSONException {
		JSONObject esicData = (JSONObject) esicJSONData.get("esic");
		Boolean isESICChecked = (Boolean) esicData.get("check");
		String data = "";
		String esicNumber = esicData.get("unique_no").toString();
		if(isESICChecked && esicNumber != ""){
			ESIC esic = new ESIC(esicNumber);
			try{
				new ESICValidator().isValid(esic);
			}
			catch (DocumentValidatorException e) {
			data = e.errorDescription;
			}
			catch (Exception e) {
				data = e.getMessage();
			}
		}
		return data;
		
	}


	/*
	 * Below Method only valiadtes whether the submitted KYC Request JSON Data is Valid or Not
	 * It does't make any updates/writes in the database
	 */
	public JSONObject doSelfValidation(JSONObject request_form_data) throws JSONException {
		JSONObject selfValidatorMap = new JSONObject();

		if ((Boolean) ((JSONObject) request_form_data.get("pan")).get("check")) {
			selfValidatorMap.put("pan", panCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("itr")).get("check")) {
			selfValidatorMap.put("ITR", itrValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("aadhar")).get("check")) {
			selfValidatorMap.put("aadhar", aadharCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("service_tax")).get("check")) {
			selfValidatorMap.put("service_tax", serviceTaxCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("voting")).get("check")) {
			selfValidatorMap.put("voting", votingCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("iec")).get("check")) {
			selfValidatorMap.put("iec", iecCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("excise")).get("check")) {
			selfValidatorMap.put("excise", exciseCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("passport")).get("check")) {
			selfValidatorMap.put("passport", passportCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("llpin")).get("check")) {
			selfValidatorMap.put("llpin", llpinCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("dl")).get("check")) {
			selfValidatorMap.put("dl", DrivingLicenceCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("vat")).get("check")) {
			selfValidatorMap.put("vat", VATCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("cst")).get("check")) {
			selfValidatorMap.put("cst", CSTCardValidation(request_form_data));
		}

		if ((Boolean) ((JSONObject) request_form_data.get("pt")).get("check")) {
			selfValidatorMap.put("pt", PTCardValidation(request_form_data));
		}
		if ((Boolean) ((JSONObject) request_form_data.get("electricity")).get("check")) {
			selfValidatorMap.put("electricity", electricityValidation(request_form_data));
		}
		if ((Boolean) ((JSONObject) request_form_data.get("telephone")).get("check")) {
			selfValidatorMap.put("telephone", telephoneValidation(request_form_data));
		}
		if ((Boolean) ((JSONObject) request_form_data.get("lpg")).get("check")) {
			selfValidatorMap.put("lpg", lpgValidation(request_form_data));
		}
		if ((Boolean) ((JSONObject) request_form_data.get("esic")).get("check")) {
			selfValidatorMap.put("esic", esicValidation(request_form_data));
		}

		return selfValidatorMap;
	}

	public JSONObject doCheckSumValidationForAadhaar(JSONObject request_form_data) throws JSONException {

		JSONObject selfCheckSumValidatorMap = new JSONObject();

		if ((Boolean) ((JSONObject) request_form_data.get("aadhar")).get("check")) {
			String document = ((JSONObject) request_form_data.get("aadhar")).get("unique_no").toString();
			if (!StringUtils.isEmpty(document)) {
				Boolean validationCheck = new VerhoeffHelper().isAadharCheckSumValidation(document);
				if (!validationCheck) {
					selfCheckSumValidatorMap.put("aadhar", "invalid aadhar card number");
				}
				else{
					selfCheckSumValidatorMap.put("aadhar", "");
				}
			}

		}
		return selfCheckSumValidatorMap;
	}

/*In order to validate the UAN Number based on verhoeff algorithm*/
	public JSONObject doCheckSumValidationForUAN(JSONObject request_form_data) throws JSONException {

		JSONObject selfCheckValidatorMapUAN = new JSONObject();

		if ((Boolean) ((JSONObject) request_form_data.get("uan")).get("check")) {
			String document = ((JSONObject) request_form_data.get("uan")).get("unique_no").toString();
			if (!StringUtils.isEmpty(document)) {
				Boolean validationCheck = new VerhoeffHelper().isUANCheckSumValidation(document);
				if (!validationCheck) {
					selfCheckValidatorMapUAN.put("uan", "Invalid UAN Number");
				}
			}

		}
		return selfCheckValidatorMapUAN;
	}
	
	
	
	
}
