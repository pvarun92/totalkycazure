package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.VoterID;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class VoterIdValidator extends BaseDocumentValidator implements DocumentValidator {

    public VoterIdValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getVoterIdValidator();

        //---------------------------------------------

        // length validation is 10
        validate("AAA1234567", true, validator);
        validate("AAA12345", false, validator);

        // voter id with length 10 pattern check
        validate("AAA1234567", true, validator);
        validate("AAA1A34567", false, validator);

        // voter invalid data check of length 10
        validate("AAA1234567", true, validator);
        validate("AAA0000000", false, validator);

        //---------------------------------------------

        // length validation is 17
        validate("AA/22/222/22", true, validator);
        validate("AA/22/222/", false, validator);

        // voter id with length 17 pattern check
        validate("AA/22/222/2", true, validator);
        validate("AA/22/222/22A2222", false, validator);

        // voter invalid data check of length 17
        validate("AA/22/222/222", true, validator);
        validate("AA/00/222/2222222", false, validator);

        validate("AA/22/222/2222", true, validator);
        validate("AA/22/000/2222222", false, validator);

        validate("AA/22/222/2222222", true, validator);

        validate("AA/22/222/0", false, validator);
        validate("AA/22/222/00", false, validator);
        validate("AA/22/222/000", false, validator);

    }

    private static void validate(String voterId, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new VoterID(voterId));
            System.out.println((valid == isValid) + " for voterId + " + voterId);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for voterId + " + voterId + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {
        VoterID vid = (VoterID) document;
        return true;
    }
}
