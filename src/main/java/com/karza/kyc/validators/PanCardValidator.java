package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.PanCard;

import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

/**
 * Created by Admin on 01-03-2016.
 */
public class PanCardValidator extends BaseDocumentValidator implements DocumentValidator {
    protected PanCardValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getPanCardValidator();

        // length validation
        validate("DFGPD2434A", "Individual", true, validator);
        validate("sfdwe", "Proprietory Concern", false, validator);

        //regular expression validation first five are alphabets then 4 digits and then alphabet
        validate("AXSPA4542L", "Individual", true, validator);
        validate("AXAP44542L", "Individual", false, validator);

        //4th character validation
        validate("AXAPA4542L", "Individual", true, validator);
        validate("AXAMP4542L", "Individual", false, validator);

        //Invalid data check
        validate("AXAPA4542L", "Individual", true, validator);
        validate("AXAPA0000L", "Individual", false, validator);
    }

    private static void validate(String pancard, String entity_type, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            PanCard p = new PanCard(pancard, entity_type);
            valid = validator.isValid(p);
            System.out.println((valid == isValid) + " for pancard + " + pancard);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for pancard + " + pancard + " " + e.getMessage());
        }

    }

    public boolean isValid(BaseDocument document) throws DocumentValidatorException {


        PanCard pa = (PanCard) document;
        String entity_type = pa.getEntityType();

        // pan card pattern first five alphabets then 4 digits and then alphabet
        String regularExpression = "[a-zA-Z]{5}\\d{4}[a-zA-Z]{1}";

        //Character list to validate
        List<Character> validCharList = Arrays.asList('P', 'C', 'H', 'F', 'E', 'A', 'T', 'B', 'L', 'J', 'G');

        if (!isSizeValidExact(document.getDocument(), 10))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Pancard must be 10 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "Pancard pattern is not matched", document.getDocument());

        if (!isCharMatched(document.getDocument(), validCharList, 3))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Invalid 4th charecter in pan card", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 5, 4))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Pancard invalid data from 6th to 9th", document.getDocument());

        if(StringUtils.endsWithIgnoreCase(entity_type,"Individual") || StringUtils.endsWithIgnoreCase(entity_type,"Proprietory Concern")){
            if(!isExpectedCharMatched(document.getDocument(),"P",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Company")){
            if(!isExpectedCharMatched(document.getDocument(),"C",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Hindu Undivided Family (HUF)")){
            if(!(isExpectedCharMatched(document.getDocument(),"H",3) || isExpectedCharMatched(document.getDocument(),"F",3)))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
       }else if(StringUtils.endsWithIgnoreCase(entity_type,"Partnership")){
            if(!isExpectedCharMatched(document.getDocument(),"F",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Limited Liability Partnership (LLP)")){
            if(!isExpectedCharMatched(document.getDocument(),"F",3) && !isExpectedCharMatched(document.getDocument(),"E",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Association of Persons (AOP)")){
            if(!isExpectedCharMatched(document.getDocument(),"A",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Trust")){
            if(!isExpectedCharMatched(document.getDocument(),"T",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Body of Individuals (BOI)")){
            if(!isExpectedCharMatched(document.getDocument(),"B",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Local Authority")){
            if(!isExpectedCharMatched(document.getDocument(),"L",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else if(StringUtils.endsWithIgnoreCase(entity_type,"Government")){
            if(!isExpectedCharMatched(document.getDocument(),"G",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }else {
            if(!isExpectedCharMatched(document.getDocument(),"J",3))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "invalid 4th charecter in pan card", document.getDocument());
        }
        return true;
    }
}
