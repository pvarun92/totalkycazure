package com.karza.kyc.validators;

/**
 * Created by Admin on 7/16/2016.
 */

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.Itr;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ItrValidator extends BaseDocumentValidator implements DocumentValidator {

    public ItrValidator() {
    }

    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        Itr itr = (Itr) document;
        String regularExpression = "[a-zA-Z]{5}\\d{4}[a-zA-Z]{1}";
        List<Character> validCharList = Arrays.asList('P', 'C', 'H', 'F', 'E', 'A', 'T', 'B', 'L', 'J', 'G');
        String dateOfFillingStr = "";
        try {
            Date date = new Date(itr.getDateOfFilling().getTime());
            SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
            dateOfFillingStr = df2.format(date);
            dateOfFillingStr = dateOfFillingStr.replace("/", "");
        } catch (Exception e) {
            System.out.print("Error :- " + e);
        }
        if (!isSizeValidExact(((Itr) document).getAcknowledgmentNumber(), 15))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Invalid Number", document.getDocument());

        if (!StringUtils.substringMatch(((Itr) document).getAcknowledgmentNumber(), 9, dateOfFillingStr))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Invalid Number", document.getDocument());

        if (dateOfFillingStr.isEmpty() || dateOfFillingStr == null)
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Invalid Number", document.getDocument());

        return true;
    }
}
