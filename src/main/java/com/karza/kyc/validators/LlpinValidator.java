package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.LLPIN;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class LlpinValidator extends BaseDocumentValidator implements DocumentValidator {

    public LlpinValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getLlpinValidator();

        // length validation is 8
        validate("AAA-1234", true, validator);
        validate("AAA-123", false, validator);

        //regular expression validation
        validate("AAA-1234", true, validator);
        validate("AAA-12A1", false, validator);

        //5nd to 8th character string validation from list
        validate("AAA-1234", true, validator);
        validate("AAA-0000", false, validator);

    }

    private static void validate(String llPin, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new LLPIN(llPin));
            System.out.println((valid == isValid) + " for llPin + " + llPin);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for llPin + " + llPin + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // LLPIN Pattern
        String regularExpression = "[a-zA-Z]{3}\\-\\d{4}";

        if (!isSizeValidExact(document.getDocument(), 8))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "LLPIN must be 8 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "LLPIN pattern is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 4, 4))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "LLPIN invalid data from 5th to 8th", document.getDocument());

        return true;
    }


}
