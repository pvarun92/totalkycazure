package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.DrivingLicenceMaster;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 4/7/16
 * Time: 5:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class VATValidator extends BaseDocumentValidator implements DocumentValidator {

    public VATValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getDrivingLicenceValidator();

        // length validation
        validate("MH01 20090091406", true, validator);
        validate("MH04 20100049006", true, validator);
        validate("HR-31N/24586/10-11", true, validator);
        validate("DL-0420110149646", true, validator);
        validate("MP09N-2016-0088578", true, validator);
        validate("RJ-14/DLC/11/559707", true, validator);


        validate("000", false, validator);
        validate("00000", false, validator);
        validate("00000000", false, validator);
        validate("000000000000", false, validator);
    }

    private static void validate(String drivingLicence, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new DrivingLicenceMaster(drivingLicence));
            System.out.println((valid == isValid) + " for Driving Licence + " + drivingLicence);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for Driving Licence + " + drivingLicence + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // VAT Pattern
        String regularExpression = "(27|33)\\d{9}(V|v|P|p|C|c)";

        if (!isSizeValidExact(document.getDocument(), 12))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "VAT must be 12 characters long", document.getDocument());

        if (!isInValidStringData(document.getDocument(), "27", 0, 2))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "VAT invalid first two numbers", document.getDocument());

        if (!isInValidStringData(document.getDocument(), "V", 11, 1) && !isInValidStringData(document.getDocument(), "v", 11, 1))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "VAT invalid Last Character", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "VAT pattern is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "000000000", 2, 9))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "VAT is invalid from 3rd to 11th digit", document.getDocument());

        return true;
    }
}
