package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.Passport;
import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/9/16
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class PassportValidator extends BaseDocumentValidator implements DocumentValidator {

    public PassportValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getPassportValidator();

        // when country is india

        // length validation
        validate("A1234567", "India", true, validator);
        validate("A123456", "India", false, validator);

        // pattern validation
        validate("A1234567", "India", true, validator);
        validate("A1234A67", "India", false, validator);

        // invalid data
        validate("A1234567", "India", true, validator);
        validate("A0000000", "India", false, validator);

        // when country is not india

        // length validation
        validate("12345678", "USA", true, validator);
        validate("123456789", "USA", true, validator);
        validate("1234567890", "USA", false, validator);

        // pattern validation
        validate("123789", "USA", true, validator);
        validate("123A45789", "USA", false, validator);

        // invalid data
        validate("000001", "USA", true, validator);
        validate("0000", "USA", false, validator);


    }

    private static void validate(String passport, String country, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            Passport passport1 = new Passport(passport, country);
            valid = validator.isValid(passport1);
            System.out.println((valid == isValid) + " for passport + " + passport);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for passport + " + passport + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        Passport pa = (Passport) document;
        String country = pa.getCountry();

        if (StringUtils.equalsIgnoreCase(country, "India") &&  (isSizeValidExact(document.getDocument(), 8))) {

            // passport card pattern all characters are digits
            String regularExpression = "[a-zA-Z]{1}\\d{7}";

            if (!isSizeValidExact(document.getDocument(), 8))
                throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Passport Number must be 8 characters long", document.getDocument());

            if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
                throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "passport pattern is not matched", document.getDocument());

            if (isInValidStringData(document.getDocument(), "0000000", 1, 7))
                throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "passport invalid data from 2nd to 8th", document.getDocument());

        } else if (StringUtils.equalsIgnoreCase(country, "India")) {

            // passport card pattern all characters are digits
          //  String regularExpression = "\\d{1,9}+$";
        //	  String regularExpression = "\\d+";
        	String regularExpression = "[a-zA-Z0-9]+$";
        	
        	if (!isSizeValidExact(document.getDocument(), 9))
                 throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Passport Number must be less than 9 digits", document.getDocument());

           /* if (!isSizeValidMaximum(document.getDocument(), 9))
                throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Passport Number must be less than 9 digits", document.getDocument());
*/
            if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
                throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "passport pattern is not matched", document.getDocument());

            if (isOnlyInvalidChar(document.getDocument(), '0'))
                throw new DocumentValidatorException(DocumentValidatorFactory.INVALID_DATA_ERROR, "passport is invalid", document.getDocument());

        }

        return true;
    }
}
