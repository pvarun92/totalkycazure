package com.karza.kyc.validators;


import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 01-03-2016.
 */
public abstract class BaseDocumentValidator implements DocumentValidator {

    // check exact size of string document
    protected boolean isSizeValidExact(String document, int size) throws DocumentValidatorException {
        return StringUtils.length(document) == size;
    }

    // check size of document string greater than or equal to minimum length
    protected boolean isSizeValidMinimum(String document, int size) throws DocumentValidatorException {
        return StringUtils.length(document) >= size;
    }

    // check size of document string smaller than or equal to maximum length
    protected boolean isSizeValidMaximum(String document, int size) throws DocumentValidatorException {
        return StringUtils.length(document) <= size;
    }

    //  check size of string is valid between range
    protected boolean isSizeValidRange(String document, int min, int max) throws DocumentValidatorException {
        return StringUtils.length(document) >= min && StringUtils.length(document) <= max;
    }

    // check specific format of document with regular expression
    protected boolean isRegularExpressionMatched(String document, String regularExp) throws DocumentValidatorException {
        return document.matches(regularExp);
    }

    // check given data is valid with invalid data
    protected boolean isInValidData(String document, String invalidData) {
        return StringUtils.equals(document, invalidData);
    }

    protected boolean isInValidSubstringData(String document, String invalidData, int startingPt, int endPt) {
        return StringUtils.equals(document.substring(startingPt, endPt), invalidData);
    }

    // check given invalid string is in string at specific location
    protected boolean isInValidStringData(String document, String invalidData, int strLoc, int lengthToAddInStartLoc) {
        return StringUtils.equals(document.substring(strLoc, strLoc + lengthToAddInStartLoc), invalidData);
    }

    // matches specific location character in string is in character list
    protected boolean isCharMatched(String document, List<Character> charList, int charLoc) throws DocumentValidatorException {
        List<Character> lowerCaseCharList = lowerCaseCharList(charList);
        return lowerCaseCharList.contains(document.toLowerCase().charAt(charLoc));
    }

    protected boolean isExpectedCharMatched(String document, String expectChar, int charLoc) throws DocumentValidatorException {
        return expectChar.equalsIgnoreCase(document.toLowerCase().substring(charLoc, charLoc + 1));
    }

    protected boolean isOnlyInvalidChar(String document, char invalidCharacter) throws DocumentValidatorException {
        int len = document.length();
        String dynamicRegularExpression = "(" + invalidCharacter + "{" + len + "})";
        return document.matches(dynamicRegularExpression);
    }

    protected boolean isOnlySubstringWithInvalidChar(String document, char invalidCharacter, String splitCharacter, int SplitLocation) throws DocumentValidatorException {
        String[] substringParts = document.split(splitCharacter);
        if (substringParts.length > 0) {
            String subString = substringParts[SplitLocation];
            int len = subString.length();
            String dynamicRegularExpression = "(" + invalidCharacter + "{" + len + "})";
            return subString.matches(dynamicRegularExpression);
        }
        return true;
    }

    // matches substring in a string if substring matched from document indexOf return start index , if no match then indexOf return index as -1
    protected boolean isSubStringMatched(String document, List<String> testStrings, int startLoc, int lengthToAddInStartLoc) throws DocumentValidatorException {
        List<String> lowerCaseStrList = lowerCaseStringList(testStrings);
        return lowerCaseStrList.contains(document.toLowerCase().substring(startLoc, startLoc + lengthToAddInStartLoc));
    }

    // helper methods

    // make each element in  character arrayList to lowercase
    protected List<Character> lowerCaseCharList(List<Character> charList) {
        List<Character> lowerCaseCharList = new ArrayList();
        for (char ch : charList) {
            lowerCaseCharList.add(Character.toLowerCase(ch));
        }
        return lowerCaseCharList;
    }

    // make each element in string arrayList to lowercase
    protected List<String> lowerCaseStringList(List<String> stringList) {
        List<String> lowerCaseStringList = new ArrayList();
        for (String str : stringList) {
            lowerCaseStringList.add(str.toLowerCase());
        }
        return lowerCaseStringList;
    }
}
