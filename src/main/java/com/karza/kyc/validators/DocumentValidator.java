package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;

/**
 * Created by Admin on 01-03-2016.
 */
public interface DocumentValidator {

    boolean isValid(BaseDocument document) throws DocumentValidatorException;
}
