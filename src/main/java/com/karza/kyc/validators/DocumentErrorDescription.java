package com.karza.kyc.validators;

/**
 * Created by Admin on 01-03-2016.
 */
public class DocumentErrorDescription {

    public final String getInvalidSizeException(int size) {
        return "The input should be of size " + size;
    }
}
