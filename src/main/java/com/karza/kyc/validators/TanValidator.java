package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.TAN;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/11/16
 * Time: 11:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class TanValidator extends BaseDocumentValidator implements DocumentValidator {
    public TanValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getTanValidator();

        // length validation is 10
        validate("ABCD56789A", true, validator);
        validate("1234567", false, validator);

        //regular expression validation all are digits
        validate("ABCD56789A", true, validator);
        validate("ABCDA6789A", false, validator);

        //validate with invalid Tan number
        validate("ABCD56789A", true, validator);
        validate("ABCD00000A", false, validator);

    }

    private static void validate(String taNumber, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new TAN(taNumber));
            System.out.println((valid == isValid) + " for taNumber + " + taNumber);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for taNumber + " + taNumber + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // Tan card pattern all characters are digits
        String regularExpression = "[a-zA-Z]{4}\\d{5}[a-zA-Z]{1}";

        if (!isSizeValidExact(document.getDocument(), 10))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "TAN number must be 10 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "TAN number pattern is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "00000", 4, 5))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "TAN number data from 5nd to 9th", document.getDocument());
        return true;
    }
}
