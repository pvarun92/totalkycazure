package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.CIN;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CinValidator extends BaseDocumentValidator implements DocumentValidator {


    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getCinValidator();

        // length validation is 21
        validate("U12312AP1234FTC123123", true, validator);
        validate("A12312AP1234A", false, validator);

        //regular expression validation
        validate("L12312AP1234FTC123123", true, validator);
        validate("AA2312AP1234FTC12A123", false, validator);

        //first character validation from list
        validate("L12312AP1234FTC123123", true, validator);
        validate("A12312AP1234FTC123123", false, validator);

        //7th and 8th character string validation from list
        validate("L12312AP1234FTC123123", true, validator);
        validate("L12312AA1234FTC123123", false, validator);

        //13th and 15th character string validation from list
        validate("L12312AP1234FTC123123", true, validator);
        validate("L12312AP1234FTT123123", false, validator);

        //2nd to 6th character string validation from list
        validate("L12312AP1234FTC123123", true, validator);
        validate("L00000AP1234FTC123123", false, validator);

        //9th and 12th character string validation from list
        validate("L12312AP1234FTC123123", true, validator);
        validate("L12345AP0000FTC123123", false, validator);

        //16th and 21th character string validation from list
        validate("L12312AP1234FTC123123", true, validator);
        validate("L12345AP1234FTC000000", false, validator);
    }

    private static void validate(String ciNumber, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new CIN(ciNumber));
            System.out.println((valid == isValid) + " for ciNumber + " + ciNumber);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for ciNumber + " + ciNumber + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // CIN Pattern
        String regularExpression = "[a-zA-Z]{1}\\d{5}[a-zA-Z]{2}\\d{4}[a-zA-Z]{3}\\d{6}";

        //Character list to validate
        List<Character> validCharList = Arrays.asList('U', 'L');

        // two character string to validate
        List<String> validTwoCharStringList = Arrays.asList("AN", "AP", "AR", "AS", "BR", "CH", "CT", "DN", "DD", "DL", "GA", "GJ", "HR", "HP", "JK", "JH", "KA", "KL", "LD", "MP", "MH", "MN", "ML", "MZ", "NL", "OR", "PY", "PB", "RJ", "TN", "TG", "TR", "UR", "UP", "WB");

        // three character string to validate
        List<String> validThreeCharStringList = Arrays.asList("FTC", "FLC", "GAP", "GAT", "GOI", "NPL", "OPC", "PTC", "PLC", "SGC", "ULL", "ULT");

        if (!isSizeValidExact(document.getDocument(), 21))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "CIN must be 21 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "CIN pattern is not matched", document.getDocument());

        if (!isCharMatched(document.getDocument(), validCharList, 0))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "CIN character is not matched", document.getDocument());

        if (!isSubStringMatched(document.getDocument(), validTwoCharStringList, 6, 2))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "CIN two level character is not matched", document.getDocument());

        if (!isSubStringMatched(document.getDocument(), validThreeCharStringList, 12, 3))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "CIN three level character is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "00000", 1, 5))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "CIN invalid data from 2nd to 6th", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 8, 4))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "CIN invalid data from 9th to 12th", document.getDocument());

        if (isInValidStringData(document.getDocument(), "000000", 15, 6))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "CIN invalid data from 16th to 21th", document.getDocument());

        return true;
    }
}
