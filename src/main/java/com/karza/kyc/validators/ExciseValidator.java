package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.Excise;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 4:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExciseValidator extends BaseDocumentValidator implements DocumentValidator {

    protected ExciseValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getExciseValidator();

        // length validation
        validate("ABCDE1234AEM123", true, validator);
        validate("ABCDE1234EMC12", false, validator);

        //regular expression validation first five are alphabets then 4 digits and then three alphabet and then three digits
        validate("ABCDE1234AED123", true, validator);
        validate("ABCDE1234AED12A", false, validator);

        //11th and 12th character string validation
        validate("ABCDE1234AXM123", true, validator);
        validate("ABCDE1234ABC123", false, validator);

        //6th to 9th character string validation
        validate("ABCDE1234AXM123", true, validator);
        validate("ABCDE0000AXD123", false, validator);

        //13th and 15th character string validation
        validate("ABCDE1234AXM123", true, validator);
        validate("ABCDE1234AXM000", false, validator);
    }

    private static void validate(String excise, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new Excise(excise));
            System.out.println((valid == isValid) + " for excise + " + excise);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for excise + " + excise + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // Excise Pattern
        String regularExpression = "[a-zA-Z]{5}\\d{4}[a-zA-Z]{3}\\d{3}";

        // two character string to validate
        List<String> validTwoCharStringList = Arrays.asList("EM", "ED", "XM", "XD", "CE");


        if (!isSizeValidExact(document.getDocument(), 15))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Excise must be 15 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "Excise pattern is not matched", document.getDocument());

        if (!isSubStringMatched(document.getDocument(), validTwoCharStringList, 10, 2))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Excise two level character is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 5, 4))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Excise invalid data from 6th to 9th", document.getDocument());

        if (isInValidStringData(document.getDocument(), "000", 12, 3))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Excise invalid data from 13th to 15th", document.getDocument());


        return true;
    }
}
