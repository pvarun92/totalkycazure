package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.IEC;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class IecValidator extends BaseDocumentValidator implements DocumentValidator {

    public IecValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getIecValidator();

        // length validation is 10
        validate("1231231231", true, validator);
        validate("12312312", false, validator);

        //regular expression validation all are digits
        validate("1231231231", true, validator);
        validate("AXAP44542L", false, validator);

        //validate with invalid IEC number
        validate("1231231231", true, validator);
        validate("0000000000", false, validator);
    }

    private static void validate(String iec, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new IEC(iec));
            System.out.println((valid == isValid) + " for iec + " + iec);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for iec + " + iec + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // IEC card pattern all characters are digits
        String regularExpression = "(\\d{10})";

        if (!isSizeValidExact(document.getDocument(), 10))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "IEC card must be 10 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "IEC card pattern is not matched", document.getDocument());

        if (isInValidData(document.getDocument(), "0000000000"))
            throw new DocumentValidatorException(DocumentValidatorFactory.INVALID_DATA_ERROR, "IEC card is invalid", document.getDocument());

        return true;
    }


}
