package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.DIN;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/11/16
 * Time: 11:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class DinValidator extends BaseDocumentValidator implements DocumentValidator {

    public DinValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getDinValidator();

        // length validation is 8
        validate("12345678", true, validator);
        validate("1234567", false, validator);

        //regular expression validation all are digits
        validate("12345678", true, validator);
        validate("12345A78", false, validator);

        //validate with invalid DIN number
        validate("12345678", true, validator);
        validate("00000000", false, validator);
    }

    private static void validate(String diNumber, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new DIN(diNumber));
            System.out.println((valid == isValid) + " for diNumber + " + diNumber);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for diNumber + " + diNumber + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // DIN number pattern all characters are digits
        String regularExpression = "(\\d{8})";

        if (!isSizeValidExact(document.getDocument(), 8))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "DIN number must be 8 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "DIN number pattern is not matched", document.getDocument());

        if (isInValidData(document.getDocument(), "00000000"))
            throw new DocumentValidatorException(DocumentValidatorFactory.INVALID_DATA_ERROR, "DIN number is invalid", document.getDocument());

        return true;
    }
}
