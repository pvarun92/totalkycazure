package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.ServiceTax;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class ServiceTaxValidator extends BaseDocumentValidator implements DocumentValidator {

    public ServiceTaxValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getServiceTaxValidator();

        // length validation is 15
        validate("AAAAA1234AST123", true, validator);
        validate("AAAAA1234ASD12", false, validator);

        //regular expression validation
        validate("AAAAA1234ASD123", true, validator);
        validate("AAAAA1234AAST23", false, validator);

        //6th to 9th character string validation from list
        validate("AAAAA1234AST123", true, validator);
        validate("AAAAA0000ASD123", false, validator);

        //13nd to 15th character string validation from list
        validate("AAAAA1234ASD123", true, validator);
        validate("AAAAA1234AST000", false, validator);

    }

    private static void validate(String serviceTax, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new ServiceTax(serviceTax));
            System.out.println((valid == isValid) + " for serviceTax + " + serviceTax);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for serviceTax + " + serviceTax + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // ServiceTax Pattern
        String regularExpression = "[a-zA-Z]{5}\\d{4}[a-zA-Z]{3}\\d{3}";

        // two character string to validate
        List<String> validTwoCharStringList = Arrays.asList("ST", "SD");

        if (!isSizeValidExact(document.getDocument(), 15))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Service Tax Number must be 15 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "Service Tax Number pattern is not matched", document.getDocument());

        if (!isSubStringMatched(document.getDocument(), validTwoCharStringList, 10, 2))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Service Tax Number two level character is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 5, 4))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Service Tax Number invalid data from 6th to 9th", document.getDocument());

        if (isInValidStringData(document.getDocument(), "000", 12, 3))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Service Tax Number invalid data from 13th to 15th", document.getDocument());

        return true;
    }
}
