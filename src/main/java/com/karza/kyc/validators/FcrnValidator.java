package com.karza.kyc.validators;

import com.karza.kyc.model.BaseDocument;
import com.karza.kyc.model.FCRN;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class FcrnValidator extends BaseDocumentValidator implements DocumentValidator {

    public FcrnValidator() {
    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getFcrnValidator();

        // length validation is 9
        validate("F12345", true, validator);
        validate("F1234", false, validator);

        //regular expression validation
        validate("F12345", true, validator);
        validate("F12A45", false, validator);
        validate("A12345", false, validator);

        //6th to 9th character string validation from list
        validate("F12345", true, validator);
        validate("F00000", false, validator);

    }

    private static void validate(String fcrn, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new FCRN(fcrn));
            System.out.println((valid == isValid) + " for fcrn + " + fcrn);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for fcrn + " + fcrn + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // FCRN Pattern
        String regularExpression = "[fF]\\d{5}";

        if (!isSizeValidExact(document.getDocument(), 6))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "FCRN must be 6 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "FCRN pattern is not matched", document.getDocument());

        if (isInValidStringData(document.getDocument(), "0000", 1, 5))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "FCRN invalid data from 2nd to 6th", document.getDocument());

        return true;
    }


}
