package com.karza.kyc.validators;

import com.karza.kyc.model.AadharCard;
import com.karza.kyc.model.BaseDocument;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/8/16
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class AadharCardValidator extends BaseDocumentValidator implements DocumentValidator {

    protected AadharCardValidator() {

    }

    public static void main(String[] a) {
        DocumentValidator validator = DocumentValidatorFactory.getAadharCardValidator();

        // length validation is 12
        validate("123123123123", true, validator);
        validate("12312312", false, validator);

        //regular expression validation all are digits
        validate("123123123123", true, validator);
        validate("AXAP44542L12", false, validator);

        //validate with invalid aadhar number
        validate("123123123123", true, validator);
        validate("000000000000", false, validator);
    }

    private static void validate(String aadharCard, boolean isValid, DocumentValidator validator) {
        boolean valid = false;
        try {
            valid = validator.isValid(new AadharCard(aadharCard));
            System.out.println((valid == isValid) + " for aadharCard + " + aadharCard);
        } catch (DocumentValidatorException e) {
            System.out.println((!isValid) + " for aadharCard + " + aadharCard + " " + e.getMessage());
        }

    }

    @Override
    public boolean isValid(BaseDocument document) throws DocumentValidatorException {

        // aadhar card pattern all characters are digits
        String regularExpression = "(\\d{12})";

        if (!isSizeValidExact(document.getDocument(), 12))
            throw new DocumentValidatorException(DocumentValidatorFactory.SIZE_ERROR, "Aadhar card must be 12 characters long", document.getDocument());

        if (!isRegularExpressionMatched(document.getDocument(), regularExpression))
            throw new DocumentValidatorException(DocumentValidatorFactory.PATTERN_ERROR, "Aadhar card pattern is not matched", document.getDocument());

        if (isInValidSubstringData(document.getDocument(), "00000000000", 0, document.getDocument().length() - 1))
            throw new DocumentValidatorException(DocumentValidatorFactory.INVALID_DATA_ERROR, "Aadhar card is having invalid data", document.getDocument());

        if (!VerhoeffAlgorithm.validateVerhoeff(document.getDocument()))
            throw new DocumentValidatorException(DocumentValidatorFactory.CHARACTER_ERROR, "Aadhar card is invalid", document.getDocument());

        return true;
    }


}
