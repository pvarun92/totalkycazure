<script type="text/ng-template" id="crudBranchPopup.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{heading}}</h3>
    </div>
    <div class="modal-body text-center">
        <div class="span4 offset4 well">
            <form name="form" novalidate>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Branch Name*</label>
                        <input ng-model="selected.branchName" type="text" data-ng-disabled="view" class="form-control"
                               name="branchName" placeholder="Branch Name*"
                               ng-class="{true:'invalid-field',false:''}[form.branchName.$error.required && (form.branchName.$touched || form.$submitted)]"
                               required/>
                        <span class="error-message no-border"
                              ng-show="form.branchName.$error.required && (form.branchName.$touched || form.$submitted)">Branch Name is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">IFSC Code*</label>
                        <input ng-model="selected.ifscCode" type="text" data-ng-disabled="view" class="form-control"
                               name="ifscCode" placeholder="IFSC Code*"
                               ng-class="{true:'invalid-field',false:''}[form.ifscCode.$error.required && (form.ifscCode.$touched || form.$submitted)]"
                               required/>
                        <span class="error-message no-border"
                              ng-show="form.ifscCode.$error.required && (form.ifscCode.$touched || form.$submitted)">IFSC Code is required</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Address</label>
                        <input ng-model="selected.address" type="text" data-ng-disabled="view" class="form-control"
                               name="address" placeholder="Address"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">City</label>
                        <input ng-model="selected.city" type="text" data-ng-disabled="view" class="form-control"
                               name="city" placeholder="City"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Location*</label>
                        <select id="location" data-ng-model="selected.location" data-ng-disabled="view"
                                class="form-control" name="location"
                                ng-class="{true:'invalid-field',false:''}[form.location.$error.required && (form.location.$touched || form.$submitted)]"
                                required>
                            <option value="">Location*</option>
                            <option ng-repeat="location in locations" value="{{location.id}}">
                                {{location.locationOrZone}}
                            </option>
                        </select>
                        <span class="error-message no-border"
                              ng-show="form.location.$error.required && (form.location.$touched || form.$submitted)">Location is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Pincode</label>
                        <input ng-model="selected.pincode" type="text" data-ng-disabled="view" class="form-control"
                               name="pincode" placeholder="Pincode" only-numeric/>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" data-ng-show="new" ng-disabled="!form.$valid"
                        ng-click="save(selected);"> Save
                </button>
                <button type="button" class="btn btn-primary" data-ng-show="edit" ng-disabled="!form.$valid"
                        ng-click="update(selected);"> Update
                </button>
                <button class="btn btn-primary" data-ng-click="close()"> Cancel</button>
            </form>
        </div>
    </div>
</script>