<script type="text/ng-template" id="changePassword.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{heading}}</h3>
    </div>
    <div class="modal-body text-center">
        <div class="span4 offset4 well">
            <form name="form" novalidate>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">User Name</label>
                        <input ng-model="selected.user_name" type="text" data-ng-disabled="true" autofocus
                               class="form-control" name="userName" placeholder="User Name" required/>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Current Password</label>
                        <input type="password" class="form-control" ng-model="selected.current_password"
                               data-ng-disabled="view" name="currentPassword" placeholder="Current Password"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">New Password</label>
                        <input type="password" class="form-control" ng-model="selected.new_password" name="newPassword"
                               placeholder="New Password" required/>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Confirm New Password</label>
                        <input class="form-control" type="password" ng-model="selected.confirm_password"
                               name="confirmPassword" placeholder="Password Confirmation"
                               data-ng-blur="isconfirmPasswordMatch(selected)" required/>
                        <span class="error-message no-border" ng-show="confirmPasswordNotMatched">New password and confirm password doesn't match</span>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" ng-disabled="!form.$valid"
                        ng-click="resetPassword(selected)"> Change Password
                </button>
                <button class="btn btn-primary" data-ng-click="close()"> Cancel</button>
            </form>
        </div>
    </div>
</script>