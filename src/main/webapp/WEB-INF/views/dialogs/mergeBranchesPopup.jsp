<script type="text/ng-template" id="mergeBranchesPopup.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{heading}}</h3>
    </div>
    <div class="modal-body text-center">
        <div class="span4 offset4 well">
            <form name="form" novalidate>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left">Branch Name</label>
                        <ui-select multiple id="branchName" data-ng-model="selected.branchName" class="form-control"
                                   name="branchName" theme="bootstrap" style="width: 300px; float: right" required/>
                        <ui-select-match placeholder="Select branches...">{{$item.branchName}}</ui-select-match>
                        <ui-select-choices repeat="branch in branch_data | filter:$select.search">
                            {{branch.branchName}}
                        </ui-select-choices>
                        </ui-select>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left">Branch</label>
                        <select id="branch" data-ng-model="selected.branch" ng-change="createBranchIfNotExist()"
                                class="form-control" name="branch" style="width: 300px; float: right" required/>
                        <option value="">select</option>
                        <option value="createNewBranch">Create new branch</option>
                        <option ng-repeat="branch in branch_data" value="{{branch.id}}">{{branch.branchName}}</option>
                        </select>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" ng-disabled="!form.$valid"
                        ng-click="mergeBranches(selected);"> Merge Branches
                </button>
                <button class="btn btn-primary" data-ng-click="close()"> Cancel</button>
            </form>
        </div>
    </div>
</script>