<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/ng-template" id="searchRequestPopup.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
    </div>
    <div class="modal-body">
        <div class="navbar navbar-default no-margin page-header-block no-radius" style="height: 50px">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="col-xs-6">
                <H3>Searched Requests</H3>
            </div>
            <div class="col-xs-6">
            </div>
        </div>
        <div class="page-container">
            <div class="pull-right">
                <%--<uib-pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged(currentPage)" class="pagination-sm" boundary-links="true" force-ellipses="true"></uib-pagination>--%>
                <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)"
                                         template-url="dirPagination.tpl.html"></dir-pagination-controls>
            </div>
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th>
                        <a href="" ng-click="orderByField='report_id'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='report_id';">
                            Report ID
                            <span ng-show="orderByField == 'report_id'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='report_date'; reverseSort = !reverseSort">
                            Report Date
                            <span ng-show="orderByField == 'report_date'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='entity_name'; reverseSort = !reverseSort">
                            Entity Name
                            <span ng-show="orderByField == 'entity_name'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='entity_type'; reverseSort = !reverseSort">
                            Entity Type
                            <span ng-show="orderByField == 'entity_type'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='branch'; reverseSort = !reverseSort">
                            Branch
                            <span ng-show="orderByField == 'branch'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='no_of_parties'; reverseSort = !reverseSort">
                            No of Parties
                            <span ng-show="orderByField == 'no_of_parties'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='no_of_documents'; reverseSort = !reverseSort">
                            No of Documents
                            <span ng-show="orderByField == 'no_of_documents'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='report_status'; reverseSort = !reverseSort">
                            Report Status
                            <span ng-show="orderByField == 'report_status'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='validity'; reverseSort = !reverseSort">
                            KYC Validity
                            <span ng-show="orderByField == 'validity'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='itrValidity'; reverseSort = !reverseSort">
                            ITR Validity
                            <span ng-show="orderByField == 'itrValidity'">
                           <%--<span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>--%>
                            <%--<span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>--%>
                        </span>
                        </a>
                    </th>
                    <th>
                        Edit
                    </th>
                </Tr>
                <Tr data-ng-repeat="data in searchedRequest | orderBy:orderByField:reverseSort">
                    <td data-ng-show="data.report_status == 'Generated'"><a
                            href="simple_sidebar_menu?navigate_to=kyc_validation_report?id={{data.report_id}}"
                            target="_parent">{{data.report_id}} </a></td>
                    <td data-ng-show="data.report_status != 'Generated'"><a
                            href="simple_sidebar_menu?navigate_to=new_request?id={{data.report_id}}" target="_parent">
                        <span data-ng-show="data.report_status == 'Pending'">{{data.report_id}}</span><span
                            data-ng-show="data.report_status == 'In draft'">(...)</span> </a></td>
                    <td> {{getConvertedDate(data.report_date)}}</td>
                    <td> {{data.entity_name}}</td>
                    <td> {{data.entity_type}}</td>
                    <td> {{data.branch}}</td>
                    <td> {{data.no_of_parties}}</td>
                    <td> {{data.no_of_documents}}</td>
                    <td> {{data.report_status}}</td>

                    <td>
                        <img data-ng-show="data.validity == 'valid'" src="<c:url value='/images/valid_entity.png'/>"
                             class="valid status-image-size">
                        <img data-ng-show="data.validity == 'invalid'" src="<c:url value='/images/invalid_entity.png'/>"
                             class="invalid status-image-size">
                        <img data-ng-show="data.validity == 'partial_valid'"
                             src="<c:url value='/images/partial_valid_entity.png'/>" class="invalid status-image-size">
                    </td>
                    <td>
                        <img data-ng-show="data.itrValidity == 'valid'" src="<c:url value='/images/valid_entity.png'/>"
                             class="valid status-image-size">
                        <img data-ng-show="data.itrValidity == 'invalid'"
                             src="<c:url value='/images/invalid_entity.png'/>" class="invalid status-image-size">
                    </td>

                    <%--<td> {{data.validity}} </td>--%>
                    <%--<td> {{data.itrValidity}} </td>--%>
                    <td>
                        <a href="simple_sidebar_menu?navigate_to=new_request?id={{data.report_id}}" target="_parent">
                            <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                        </a>
                    </td>
                </Tr>
            </Table>
        </div>
    </div>
    <div class="modal-footer">
    </div>
</script>