<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>--%>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<%--<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>--%>
<div id="updateUsersModal" class="modal fade" role="dialog" tab-index="-1" aria-labelledby="updateUsersModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="users">
        <div class="modal-content">
            <div class="modal-header">
                Update&nbsp;User
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="updateUserForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>User Name*</label>
                                <input ng-model="selectedUser.userName" autofocus type="text" class="form-control"
                                       name="userName" placeholder="User Name*"
                                       ng-class="{true:'invalid-field',false:''}[updateUserForm.userName.$error.required && (updateUserForm.userName.$touched || updateUserForm.$submitted)]"
                                       ng-disabled="true" required/>
                                <span class="error-message no-border"
                                      ng-show="updateUserForm.userName.$error.required && (updateUserForm.userName.$touched || updateUserForm.$submitted)">User Name is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Designation</label>
                                <input type="text" class="form-control" ng-model="selectedUser.designation"
                                       name="designation" placeholder="Designation"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Number*</label>
                                <input type="text" class="form-control" ng-model="selectedUser.contactNumber"
                                       name="contactNumber" placeholder="Contact Number*"
                                       ng-class="{true:'invalid-field',false:''}[updateUserForm.contactNumber.$error.required && (updateUserForm.contactNumber.$touched || updateUserForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="updateUserForm.contactNumber.$error.required && (updateUserForm.contactNumber.$touched || updateUserForm.$submitted)">Contact Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email*</label>
                                <input class="form-control" type="text" ng-model="selectedUser.email" name="email"
                                       placeholder="Email*" data-ng-blur="validateEmailApi(selectedUser.email)"
                                       ng-class="{true:'invalid-field',false:''}[(updateUserForm.email.$error.required || updateUserForm.email.$error.pattern || emailNotExist) && (updateUserForm.email.$touched || updateUserForm.$submitted)]"
                                       ng-pattern="/^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9-]{1,255})$/"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateUserForm.email.$error.required && (updateUserForm.email.$touched || updateUserForm.$submitted)">Email is required</span>
                                <span class="error-message no-border"
                                      ng-show="updateUserForm.email.$error.pattern && (updateUserForm.email.$touched || updateUserForm.$submitted)">Email is Invalid</span>
                                <span class="error-message no-border" ng-show="emailNotExist">{{emailNotExist}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>User Request Limit*</label>
                                <input type="text" class="form-control" ng-model="selectedUser.maxRequestLimit"
                                       name="maxRequestLimit" placeholder="User Request Limit*"
                                       ng-class="[updateUserForm.maxRequestLimit.$error.required && (updateUserForm.maxRequestLimit.$touched || updateUserForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="updateUserForm.maxRequestLimit.$error.pattern && (updateUserForm.maxRequestLimit.$touched || newUserForm.$submitted)">Request Limit is Required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Parallel Sessions Limit*</label>
                                <input type="text" class="form-control" ng-model="selectedUser.parallelSessionLimit"
                                       name="parallelSessionLimit" placeholder="Parallel Sessions Limit*"
                                       ng-class="[updateUserForm.parallelSessionLimit.$error.required && (updateUserForm.parallelSessionLimit.$touched || updateUserForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="updateUserForm.parallelSessionLimit.$error.pattern && (updateUserForm.parallelSessionLimit.$touched || newUserForm.$submitted)">Parallel Sessions Limit is Required</span>
                            </div>
                        </div>
                        <button type="submit" data-dismiss="modal" ng-disabled="!updateUserForm.$valid"
                                class="btn btn-primary" ng-click="updateUser(selectedUser);" value='update'> Update
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                    <span class="alert alert-error dialogErrorMessage" ng-show="errorOnSubmit"> <spring:message
                            code="request.error"/> </span>
                    <span class="alert alert-error dialogErrorMessage"
                          ng-show="errorOnUpdate">sorry error in update</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addUsersModal" class="modal fade" role="dialog" tab-index="-1" aria-labelledby="addUsersModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="users">
        <div class="modal-content">
            <div class="modal-header"> Create&nbsp;User</div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="newUserForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedUser.userName" autofocus type="text" class="form-control"
                                       name="userName" placeholder="User Name*"
                                       ng-class="{true:'invalid-field',false:''}[newUserForm.userName.$error.required && (newUserForm.userName.$touched || newUserForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newUserForm.userName.$error.required && (newUserForm.userName.$touched || newUserForm.$submitted)">User Name is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" ng-model="selectedUser.designation"
                                       name="designation" placeholder="Designation"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" ng-model="selectedUser.contactNumber"
                                       name="contactNumber" placeholder="Contact Number*"
                                       ng-class="{true:'invalid-field',false:''}[newUserForm.contactNumber.$error.required && (newUserForm.contactNumber.$touched || newUserForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="newUserForm.contactNumber.$error.required && (newUserForm.contactNumber.$touched || newUserForm.$submitted)">Contact Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input class="form-control" type="text" ng-model="selectedUser.email" name="email"
                                       placeholder="Email*"
                                       ng-class="{true:'invalid-field',false:''}[(newUserForm.email.$error.required ||newUserForm.email.$error.pattern) && (newUserForm.email.$touched || newUserForm.$submitted)]"
                                       ng-pattern="/^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z]{2,4})$/"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newUserForm.email.$error.required && (newUserForm.email.$touched || newUserForm.$submitted)">Email is required</span>
                                <span class="error-message no-border"
                                      ng-show="newUserForm.email.$error.pattern && (newUserForm.email.$touched || newUserForm.$submitted)">Email is Invalid</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" ng-model="selectedUser.maxRequestLimit"
                                       name="maxRequestLimit" placeholder="User Request Limit*"
                                       ng-class="{true:'invalid-field',false:''}[newUserForm.maxRequestLimit.$error.required && (newUserForm.maxRequestLimit.$touched || newUserForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="newUserForm.maxRequestLimit.$error.pattern && (newUserForm.maxRequestLimit.$touched || newUserForm.$submitted)">Request Limit is Required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" ng-model="selectedUser.parallelSessionLimit"
                                       name="parallelSessionLimit" placeholder="Parallel Sessions Limit*"
                                       ng-class="[newUserForm.parallelSessionLimit.$error.required && (newUserForm.parallelSessionLimit.$touched || newUserForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="newUserForm.parallelSessionLimit.$error.pattern && (newUserForm.parallelSessionLimit.$touched || newUserForm.$submitted)">Parallel Sessions Limit is Required</span>
                            </div>
                        </div>
                        <button type="submit" data-dismiss="modal" class="btn btn-primary"
                                ng-disabled="!newUserForm.$valid" ng-click="createUser(selectedUser);" value='Create'>
                            Create
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
            <span class="alert alert-error dialogErrorMessage" ng-show="errorOnSubmit">Request Error. Try again in a few minutes.</span>
            <span class="alert alert-error dialogErrorMessage" ng-show="errorOnAlreadyPresent">username already taken please enter new one</span>
        </div>
    </div>
</div>

<div id="viewUsersModal" class="modal fade" role="dialog" tab-index="-1" aria-labelledby="viewUsersModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="users">
        <div class="modal-content">
            <div class="modal-header">
                View&nbsp;User
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="updateUserForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>User Name</label>
                                <input readonly ng-model="selectedUser.userName" autofocus type="text"
                                       class="form-control" name="userName" placeholder="User Name"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Designation</label>
                                <input readonly type="text" class="form-control" ng-model="selectedUser.designation"
                                       name="designation" placeholder="Designation"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Number</label>
                                <input readonly type="text" class="form-control" ng-model="selectedUser.contactNumber"
                                       name="contactNumber" placeholder="Contact Number" only-numeric/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input readonly class="form-control" type="text" ng-model="selectedUser.email"
                                       name="email" placeholder="Email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>User Request Limit</label>
                                <input readonly type="text" class="form-control" ng-model="selectedUser.maxRequestLimit"
                                       name="maxRequestLimit" placeholder="User Request Limit" only-numeric/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Parallel Sessions Limit</label>
                                <input readonly type="text" class="form-control"
                                       ng-model="selectedUser.parallelSessionLimit" name="parallelSessionLimit"
                                       placeholder="Parallel Sessions Limit" only-numeric/>
                            </div>
                        </div>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                    <span class="alert alert-error dialogErrorMessage" ng-show="errorOnSubmit"> <spring:message
                            code="request.error"/> </span>
                    <span class="alert alert-error dialogErrorMessage"
                          ng-show="errorOnUpdate">sorry error in update</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="deleteUsersModal" class="modal fade" tab-index="-1" role="dialog" aria-labelledby="deleteUserssModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="users">
        <div class="modal-content">
            <div class="modal-header">
                Delete User
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="deleteUserForm" novalidate>
                        <p>Are you sure to Delete User :&nbsp;&nbsp;&nbsp;{{selectedUser.name}}?</p>
                        <button type="submit" data-dismiss="modal" class="btn btn-primary"
                                ng-click="deleteUser(selectedUser);"> Delete
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>