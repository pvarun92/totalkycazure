<script type="text/ng-template" id="crudUserPopup.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{heading}}</h3>
    </div>
    <div class="modal-body text-center">
        <div class="span4 offset4 well">
            <form name="form" novalidate>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">User Name*</label>
                        <input ng-model="selected.userName" type="text" data-ng-disabled="view_name" autofocus
                               class="form-control" name="userName" placeholder="User Name*"
                               ng-class="{true:'invalid-field',false:''}[form.userName.$error.required && (form.userName.$touched || form.$submitted)]"
                               required/>
                        <span class="error-message no-border"
                              ng-show="form.userName.$error.required && (form.userName.$touched || form.$submitted)">User Name is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Designation</label>
                        <input type="text" class="form-control" ng-model="selected.designation" data-ng-disabled="view"
                               name="designation" placeholder="Designation"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Contact Number*</label>
                        <input type="text" class="form-control" ng-model="selected.contactNumber"
                               data-ng-disabled="view" name="contactNumber" placeholder="Contact Number*"
                               ng-class="{true:'invalid-field',false:''}[form.contactNumber.$error.required && (form.contactNumber.$touched || form.$submitted)]"
                               required only-numeric/>
                        <span class="error-message no-border"
                              ng-show="form.contactNumber.$error.required && (form.contactNumber.$touched || form.$submitted)">Contact Number is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Email*</label>
                        <input class="form-control" type="text" ng-model="selected.email" data-ng-disabled="view"
                               name="email" placeholder="Email*" data-ng-blur="validateEmailApi(selected.email)"
                               ng-class="{true:'invalid-field',false:''}[(form.email.$error.required || form.email.$error.pattern || emailNotExist) && (form.email.$touched || form.$submitted)]"
                               ng-pattern="/^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,255})$/"
                               required/>
                        <span class="error-message no-border"
                              ng-show="form.email.$error.required && (form.email.$touched || form.$submitted)">Email is required</span>
                        <span class="error-message no-border"
                              ng-show="form.email.$error.pattern && (form.email.$touched || form.$submitted)">Email is Invalid</span>
                        <span class="error-message no-border" ng-show="emailNotExist">{{emailNotExist}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Role*</label>
                        <select id="userRole" data-ng-model="selected.userRole" data-ng-disabled="view"
                                class="form-control" name="userRole"
                                ng-class="{true:'invalid-field',false:''}[form.userRole.$error.required && (form.userRole.$touched || form.$submitted)]"
                                required/>
                        <option value="">Role*</option>
                        <option ng-repeat="role in userRoles" value="{{role}}">{{role}}</option>
                        </select>
                        <span class="error-message no-border"
                              ng-show="form.userRole.$error.required && (form.userRole.$touched || form.$submitted)">Role is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Location/Branch*</label>
                        <select id="location" data-ng-model="selected.location" data-ng-disabled="view"
                                class="form-control" name="location"
                                ng-class="{true:'invalid-field',false:''}[form.location.$error.required && (form.location.$touched || form.$submitted)]"
                                required/>
                        <option value="">Location/Branch*</option>
                        <option ng-repeat="location in locations" value="{{location.id}}">{{location.locationOrZone}}
                        </option>
                        </select>
                        <span class="error-message no-border"
                              ng-show="form.location.$error.required && (form.location.$touched || form.$submitted)">Location is required</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">User Request Limit*</label>
                        <input type="text" class="form-control" ng-model="selected.maxRequestLimit"
                               data-ng-disabled="view" name="maxRequestLimit" placeholder="User Request Limit*"
                               ng-class="{true:'invalid-field',false:''}[form.maxRequestLimit.$error.required && (form.maxRequestLimit.$touched || form.$submitted)]"
                               required only-numeric/>
                        <span class="error-message no-border"
                              ng-show="form.maxRequestLimit.$error.required && (form.maxRequestLimit.$touched || form.$submitted)">Max Request Limit is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Parallel Sessions Limit*</label>
                        <input type="text" class="form-control" ng-model="selectedUser.parallelSessionLimit"
                               data-ng-disabled="view" name="parallelSessionLimit"
                               placeholder="Parallel Sessions Limit*"
                               ng-class="{true:'invalid-field',false:''}[form.parallelSessionLimit.$error.required && (form.parallelSessionLimit.$touched || form.$submitted)]"
                               required only-numeric/>
                        <span class="error-message no-border"
                              ng-show="form.parallelSessionLimit.$error.required && (form.parallelSessionLimit.$touched || form.$submitted)">Parallel Sessions Limit is required</span>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" data-ng-show="new" ng-disabled="!form.$valid"
                        ng-click="save(selected);"> Save
                </button>
                <button type="button" class="btn btn-primary" data-ng-show="edit" ng-disabled="!form.$valid"
                        ng-click="update(selected);"> Update
                </button>
                <button class="btn btn-primary" data-ng-click="close()"> Cancel</button>
            </form>
        </div>
    </div>
</script>