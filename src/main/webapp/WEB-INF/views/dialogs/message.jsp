<script type="text/ng-template" id="message.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{message_heading}}</h3>
    </div>
    <div class="modal-body text-center">
        {{message}}
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-block button view-historical-button submit-button" data-ng-click="close()">Ok</button>
    </div>
</script>