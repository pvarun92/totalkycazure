<div id="addCustomersModal" class="modal fade" tab-index="-1" role="dialog" aria-labelledby="addCustomersModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="account">
        <div class="modal-content">
            <div class="modal-header">
                Create&nbsp;Customer
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="newAccountForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.customerName" type="text" class="form-control"
                                       id="customerName" name="customerName" placeholder="Customer Name*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.customerName.$error.required && (newAccountForm.customerName.$touched || newAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.customerName.$error.required && (newAccountForm.customerName.$touched || newAccountForm.$submitted)">Customer Name is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <select id="customerType" name="customerType"
                                        data-ng-model="selectedCustomer.customerType" class="form-control"
                                        ng-class="{true:'invalid-field',false:''}[newAccountForm.customerType.$error.required && (newAccountForm.customerType.$touched || newAccountForm.$submitted)]"
                                        required>
                                    <option value="">Customer Type*</option>
                                    <option ng-repeat="customerType in customerTypes" value="{{customerType}}">
                                        {{customerType}}
                                    </option>
                                </select>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.customerType.$error.required && (newAccountForm.customerType.$touched || newAccountForm.$submitted)">Customer Type is required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.address" type="text" class="form-control" id="address"
                                       placeholder="Address"/>
                            </div>
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.creditPeriod" type="text" class="form-control"
                                       id="creditPeriod" name="creditPeriod" placeholder="Credit Period*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.creditPeriod.$error.required && (newAccountForm.creditPeriod.$touched || newAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.creditPeriod.$error.required && (newAccountForm.creditPeriod.$touched || newAccountForm.$submitted)">Credit Period is required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.contactPerson" type="text" class="form-control"
                                       id="contactPerson" name="contactPerson" placeholder="Contact Person*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.contactPerson.$error.required && (newAccountForm.contactPerson.$touched || newAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.contactPerson.$error.required && (newAccountForm.contactPerson.$touched || newAccountForm.$submitted)">Contact Person is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.designation" type="text" class="form-control"
                                       id="designation" placeholder="Designation"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.contactNumber" type="text" class="form-control"
                                       id="contactNumber" name="contactNumber" placeholder="Contact Number*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.contactNumber.$error.required && (newAccountForm.contactNumber.$touched || newAccountForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.contactNumber.$error.required && (newAccountForm.contactNumber.$touched || newAccountForm.$submitted)">Contact Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.email" type="text" class="form-control" id="email"
                                       name="email" placeholder="Email*"
                                       data-ng-blur="validateEmailApi(selectedCustomer.email)"
                                       ng-class="{true:'invalid-field',false:''}[(newAccountForm.email.$error.required || newAccountForm.email.$error.pattern || emailNotExist) && (newAccountForm.email.$touched || newAccountForm.$submitted)]"
                                       ng-pattern="/^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,255})$/"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.email.$error.required && (newAccountForm.email.$touched || newAccountForm.$submitted)">Email is required</span>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.email.$error.pattern && (newAccountForm.email.$touched || newAccountForm.$submitted)">Email is Invalid</span>
                                <span class="error-message no-border" ng-show="emailNotExist">{{emailNotExist}}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.panNumber" type="text" class="form-control"
                                       id="panNumber" name="panNumber" placeholder="Pan Number*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.panNumber.$error.required && (newAccountForm.panNumber.$touched || newAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.panNumber.$error.required && (newAccountForm.panNumber.$touched || newAccountForm.$submitted)">Pan Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.tanNumber" type="text" class="form-control"
                                       id="tanNumber" name="tanNumber" placeholder="Tan Number*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.tanNumber.$error.required && (newAccountForm.tanNumber.$touched || newAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.tanNumber.$error.required && (newAccountForm.tanNumber.$touched || newAccountForm.$submitted)">Tan Number is required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.serviceTaxNumber" type="text" class="form-control"
                                       id="serviceTaxNumber" name="serviceTaxNumber" placeholder="ServiceTax Number*"
                                       ng-class="{true:'invalid-field',false:''}[newAccountForm.serviceTaxNumber.$error.required && (newAccountForm.serviceTaxNumber.$touched || newAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="newAccountForm.serviceTaxNumber.$error.required && (newAccountForm.serviceTaxNumber.$touched || newAccountForm.$submitted)">Service Tax Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <input ng-model="selectedCustomer.karzaProducts" type="text" class="form-control"
                                       id="karzaProducts" placeholder="Karza Products"/>
                            </div>
                        </div>
                        <button type="submit" data-dismiss="modal" ng-disabled="!newAccountForm.$valid"
                                class="btn btn-primary" ng-click="createCustomer(selectedCustomer);"> Create
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="viewCustomersModal" class="modal fade" tab-index="-1" role="dialog" aria-labelledby="viewCustomersModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="account">
        <div class="modal-content">
            <div class="modal-header">
                View&nbsp;Account
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="viewAccountForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Customer Name</label>
                                <input readonly ng-model="selectedCustomer.customerName" type="text"
                                       class="form-control" name="customerName" placeholder="Customer Name"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Customer Type</label>
                                <input readonly ng-model="selectedCustomer.customerType" type="text"
                                       class="form-control" name="customerType" placeholder="Customer Type"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input readonly ng-model="selectedCustomer.address" type="text" class="form-control"
                                       name="address" placeholder="Address"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Credit Period</label>
                                <input readonly ng-model="selectedCustomer.creditPeriod" type="text"
                                       class="form-control" name="creditPeriod" placeholder="Credit Period"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Person</label>
                                <input readonly ng-model="selectedCustomer.contactPerson" type="text"
                                       class="form-control" name="contactPerson" placeholder="Contact Person"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Designation</label>
                                <input readonly ng-model="selectedCustomer.designation" type="text" class="form-control"
                                       name="designation" placeholder="Designation"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Number</label>
                                <input readonly ng-model="selectedCustomer.contactNumber" type="text"
                                       class="form-control" name="contactNumber" placeholder="Contact Number"
                                       only-numeric/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input readonly ng-model="selectedCustomer.email" type="text" class="form-control"
                                       name="email" placeholder="Email"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Pan Number</label>
                                <input readonly ng-model="selectedCustomer.panNumber" type="text" class="form-control"
                                       name="panNumber" placeholder="Pan Number"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tan Number</label>
                                <input readonly ng-model="selectedCustomer.tanNumber" type="text" class="form-control"
                                       name="tanNumber" placeholder="Tan Number"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>ServiceTax Number</label>
                                <input readonly ng-model="selectedCustomer.serviceTaxNumber" type="text"
                                       class="form-control" name="serviceTaxNumber" placeholder="ServiceTax Number"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Karza Products</label>
                                <input readonly ng-model="selectedCustomer.karzaProducts" type="text"
                                       class="form-control" name="karzaProducts" placeholder="Karza Products"/>
                            </div>
                        </div>
                        <%--<button type="submit" class="btn btn-primary" ng-click="createAccount(selectedCustomer);"> Create </button>--%>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="updateCustomersModal" class="modal fade" tab-index="-1" role="dialog"
     aria-labelledby="updateCustomersModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="account">
        <div class="modal-content">
            <div class="modal-header">
                Update&nbsp;Account
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="updateAccountForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Customer Name*</label>
                                <input ng-model="selectedCustomer.customerName" type="text" class="form-control"
                                       name="customerName" placeholder="Customer Name*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.customerName.$error.required && (updateAccountForm.customerName.$touched || updateAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.customerName.$error.required && (updateAccountForm.customerName.$touched || updateAccountForm.$submitted)">Customer Name is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Customer Type*</label>
                                <select data-ng-model="selectedCustomer.customerType" name="customerType"
                                        class="form-control"
                                        ng-class="{true:'invalid-field',false:''}[updateAccountForm.customerType.$error.required && (updateAccountForm.customerType.$touched || updateAccountForm.$submitted)]"
                                        required/>
                                <option value="">Customer Type*</option>
                                <option ng-repeat="customerType in customerTypes" value="{{customerType}}">
                                    {{customerType}}
                                </option>
                                </select>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.customerType.$error.required && (updateAccountForm.customerType.$touched || updateAccountForm.$submitted)">Customer Type is required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input ng-model="selectedCustomer.address" type="text" class="form-control"
                                       name="address" placeholder="Address"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Credit Period*</label>
                                <input ng-model="selectedCustomer.creditPeriod" type="text" class="form-control"
                                       name="creditPeriod" placeholder="Credit Period*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.creditPeriod.$error.required && (updateAccountForm.creditPeriod.$touched || updateAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.creditPeriod.$error.required && (updateAccountForm.creditPeriod.$touched || updateAccountForm.$submitted)">Credit Period is required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Person*</label>
                                <input ng-model="selectedCustomer.contactPerson" type="text" class="form-control"
                                       name="contactPerson" placeholder="Contact Person*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.contactPerson.$error.required && (updateAccountForm.contactPerson.$touched || updateAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.contactPerson.$error.required && (updateAccountForm.contactPerson.$touched || updateAccountForm.$submitted)">Contact Person is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Designation</label>
                                <input ng-model="selectedCustomer.designation" type="text" class="form-control"
                                       name="designation" placeholder="Designation"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Contact Number*</label>
                                <input ng-model="selectedCustomer.contactNumber" type="text" class="form-control"
                                       name="contactNumber" placeholder="Contact Number*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.contactNumber.$error.required && (updateAccountForm.contactNumber.$touched || updateAccountForm.$submitted)]"
                                       required only-numeric/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.contactNumber.$error.required && (updateAccountForm.contactNumber.$touched || updateAccountForm.$submitted)">Contact Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email*</label>
                                <input ng-model="selectedCustomer.email" type="text" class="form-control" name="email"
                                       placeholder="Email*" data-ng-blur="validateEmailApi(selectedCustomer.email)"
                                       ng-class="{true:'invalid-field',false:''}[(updateAccountForm.email.$error.required || updateAccountForm.email.$error.pattern || emailNotExist) && (updateAccountForm.email.$touched || updateAccountForm.$submitted)]"
                                       ng-pattern="/^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,255})$/"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.email.$error.required && (updateAccountForm.email.$touched || updateAccountForm.$submitted)">Email is required</span>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.email.$error.pattern && (updateAccountForm.email.$touched || updateAccountForm.$submitted)">Email is Invalid</span>
                                <span class="error-message no-border" ng-show="emailNotExist">{{emailNotExist}}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Pan Number*</label>
                                <input ng-model="selectedCustomer.panNumber" type="text" class="form-control"
                                       name="panNumber" placeholder="Pan Number*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.panNumber.$error.required && (updateAccountForm.panNumber.$touched || updateAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.panNumber.$error.required && (updateAccountForm.panNumber.$touched || updateAccountForm.$submitted)">Pan Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tan Number*</label>
                                <input ng-model="selectedCustomer.tanNumber" type="text" class="form-control"
                                       name="tanNumber" placeholder="Tan Number*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.tanNumber.$error.required && (updateAccountForm.tanNumber.$touched || updateAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.tanNumber.$error.required && (updateAccountForm.tanNumber.$touched || updateAccountForm.$submitted)">Tan Number is required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>ServiceTax Number*</label>
                                <input ng-model="selectedCustomer.serviceTaxNumber" type="text" class="form-control"
                                       name="serviceTaxNumber" placeholder="ServiceTax Number*"
                                       ng-class="{true:'invalid-field',false:''}[updateAccountForm.serviceTaxNumber.$error.required && (updateAccountForm.serviceTaxNumber.$touched || updateAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="updateAccountForm.serviceTaxNumber.$error.required && (updateAccountForm.serviceTaxNumber.$touched || updateAccountForm.$submitted)">Service Tax Number is required</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Karza Products</label>
                                <input ng-model="selectedCustomer.karzaProducts" type="text" class="form-control"
                                       name="karzaProducts" placeholder="Karza Products"/>
                            </div>
                        </div>
                        <button type="submit" data-dismiss="modal" ng-disabled="!newAccountForm.$valid"
                                class="btn btn-primary" ng-click="updateCustomer(selectedCustomer);"> update
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="deleteCustomersModal" class="modal fade" tab-index="-1" role="dialog"
     aria-labelledby="deleteCustomersModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="account">
        <div class="modal-content">
            <div class="modal-header">
                Delete Customer
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="deleteCustomerForm" novalidate>
                        <p>Are you sure to Delete Customer :&nbsp;&nbsp;&nbsp;{{selectedCustomer.name}}?</p>
                        <button type="submit" data-dismiss="modal" class="btn btn-primary"
                                ng-click="deleteCustomer(selectedCustomer);"> Delete
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="changeDomainModal" class="modal fade" tab-index="-1" role="dialog" aria-labelledby="changeDomainModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="account">
        <div class="modal-content">
            <div class="modal-header">
                Change&nbsp;Domain
            </div>
            <div class="modal-body">
                <div class="span4 offset4 well">
                    <form name="viewAccountForm" novalidate>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Current domain</label>
                                <input readonly ng-model="selectedCustomerDomain.currentDomain" type="text"
                                       class="form-control" name="email"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label>New domain*</label>
                                <input ng-model="selectedCustomerDomain.newDomain" type="text" class="form-control"
                                       name="newDomain" placeholder="New Domain*"
                                       ng-class="{true:'invalid-field',false:''}[viewAccountForm.newDomain.$error.required && (viewAccountForm.newDomain.$touched || viewAccountForm.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="viewAccountForm.newDomain.$error.required && (viewAccountForm.newDomain.$touched || viewAccountForm.$submitted)">New domain is required</span>
                            </div>
                        </div>
                        <button type="submit" ng-disabled="!viewAccountForm.$valid" data-dismiss="modal"
                                class="btn btn-primary" ng-click="changeDomain(selectedCustomerDomain);"> Change
                        </button>
                        <button data-dismiss="modal" class="btn btn-primary" aria-hidden="true"> Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
