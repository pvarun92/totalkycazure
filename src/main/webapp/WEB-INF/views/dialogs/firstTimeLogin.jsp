<script type="text/ng-template" id="firstTimeLogin.html">
    <div class="modal-header text-center">
        <%--<a class="close" data-ng-click="close()">×</a>--%>
        <h3 class="modal-title">Message</h3>
    </div>
    <div class="modal-body text-center">
        You have loggedIn successfully. please reset your password before navigating to other page.
    </div>
    <div class="modal-footer text-center">
        <button type="button" class="btn btn-primary" ng-click="resetPasswordPopUp()"> Change Password</button>
    </div>
</script>