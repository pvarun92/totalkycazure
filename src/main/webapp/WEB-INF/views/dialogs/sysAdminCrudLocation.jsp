<script type="text/ng-template" id="sysAdminCrudLocation.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{heading}}</h3>
    </div>
    <div class="modal-body text-center">
        <div class="span4 offset4 well">
            <form name="form" novalidate>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Location Name*</label>
                        <input ng-model="selected.locationOrZone" type="text" data-ng-disabled="view"
                               class="form-control" name="locationOrZone" placeholder="Location Name*"
                               ng-class="{true:'invalid-field',false:''}[form.locationOrZone.$error.required && (form.locationOrZone.$touched || form.$submitted)]"
                               required/>
                        <span class="error-message no-border"
                              ng-show="form.locationOrZone.$error.required && (form.locationOrZone.$touched || form.$submitted)">Location Name is required</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="pull-left" data-ng-hide="new">Linked Branches</label>
                        <input ng-model="selected.linkedBranches" type="text" data-ng-disabled="view"
                               class="form-control" name="linkedBranches" placeholder="Linked Branches"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="pull-left select-label" data-ng-hide="new">Parent Location*</label>
                        <select id="parentLocation" data-ng-model="selected.parentLocation" data-ng-disabled="view"
                                class="form-control" name="parentLocation"
                                ng-class="{true:'invalid-field',false:''}[form.parentLocation.$error.required && (form.parentLocation.$touched || form.$submitted)]"
                                required>
                            <option value="">Location*</option>
                            <option ng-repeat="parentLocation in location_data" value="{{parentLocation.id}}">
                                {{parentLocation.locationOrZone}}
                            </option>
                        </select>
                        <%--<label class="select-label" style="margin-top: 0px">--%>
                        <%--<select name="masterListBy" class="input-field" data-ng-model="masterListBy" data-ng-change="getMasterList(masterListBy)">--%>
                        <%--<option value="branch">Branch Master</option>--%>
                        <%--<option value="location">Location Master</option>--%>
                        <%--</select>--%>
                        <%--</label>--%>
                        <span class="error-message no-border"
                              ng-show="form.parentLocation.$error.required && (form.parentLocation.$touched || form.$submitted)">Parent Location is required</span>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" data-ng-show="new" ng-disabled="!form.$valid"
                        ng-click="save(selected);"> Save
                </button>
                <button type="button" class="btn btn-primary" data-ng-show="edit" ng-disabled="!form.$valid"
                        ng-click="update(selected);"> Update
                </button>
                <button class="btn btn-primary" data-ng-click="close()"> Cancel</button>
            </form>
        </div>
    </div>
</script>