<script type="text/ng-template" id="mergeBranchesPopUpNew.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{heading}}</h3>
    </div>
    <div class="modal-body text-center">
        <div class="span4 offset4 well">
            <form name="form" novalidate>
                <div class="row">
                    <div class="form-group col-md-6">
                        <div class="col-xs-4">
                            <label class="pull-left">Merging Branches*</label>
                        </div>
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <div ng-dropdown-multiselect="" options="branches" selected-model="selected.branchName"
                                     checkboxes="true" extra-settings="multipleDropDownSetting"></div>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-xs-4">
                            <label class="pull-left">Merged Branch*</label>
                        </div>
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <select id="branch" data-ng-model="selected.branch"
                                        ng-change="createBranchIfNotExist(selected.branch)"
                                        class="input-field selectboxit-arrow-container" name="branch" required>
                                    <option value="">select</option>
                                    <option value="createNewBranch">Create new branch</option>
                                    <option ng-repeat="branch in branches" value="{{branch.id}}">{{branch.branchName}}
                                    </option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary" ng-disabled="!form.$valid"
                        ng-click="mergeBranches(selected);"> Merge Branches
                </button>
                <button class="btn btn-primary" data-ng-click="close()"> Cancel</button>
            </form>
        </div>
    </div>
</script>