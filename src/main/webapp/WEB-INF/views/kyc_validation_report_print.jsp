<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript"
            src="<c:url value='/js/controllers/kycValidationReportPrintController.js'/>"></script>

    <style>
        @page {
            size: auto;
            margin: 20px;
        }

        @media print {
            body {
                -webkit-print-color-adjust: exact !important;
            }
        }

        body {
            -webkit-print-color-adjust: exact !important;
        }

        .blue-backgroung-header td, .blue-backgroung-header th {
            background-color: #0e76bc !important;
            color: #ffffff !important;
        }

        .valid {
            width: 20px;
        }

        .invalid {
            width: 20px;
        }
    </style>
</head>
<body data-ng-controller="kycValidationReportPrintController" ng-cloak>
<div class="report-page-container">
    <div class="report-page-header">
        <div class="row margin-0">
            <div class="pull-right">
            </div>
        </div>
        <div class="row margin-0 report-page-header-text">
				<div class="col-xs-4 pull-left">
					<h4>KYC Validation Report</h4>
				</div>
				<div class="col-xs-4 bold-center">
					<h4 style="color: #f91818 !important;"
						data-ng-show="header_data.isEdited">Edited</h4>
				</div>
				<div class="col-xs-4">
					<h4 class="login-header pull-right">
						<span class="insta">Total</span><span class="kyc">KYC</span>
					</h4>
				</div>
			</div>

        <div class="row margin-0 report-page-header-data">
            <table>
                <tr>
                    <th>User ID</th>
                    <th>Report ID</th>
                    <th>Report Date</th>
                    <th>Branch</th>
                </tr>
                <tr>
                    <td>{{header_data.user_id}}</td>
                    <td>{{header_data.report_id}}</td>
                    <td>{{getSplittedDate(header_data.report_date) | date: 'dd-MM-yyyy'}}</td>
                    <td>{{header_data.branch}}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="overall-summary-block">
        <h4 class="bold-center">Overall Summary</h4>
        <table class="table activity">
            <tr class="blue-backgroung-header">
                <th>Sr. No</th>
                <th> Name</th>
                <th> Relation</th>
                <th> No Of Document</th>
                <th> KYC Validity</th>
                <th> ITR Validity</th>
            </tr>
            <tr data-ng-repeat="entity in allEntities">
                <td>{{entity.overall_summary_data.sr_no}}</td>
                <td>{{entity.overall_summary_data.name}}</td>
                <td>{{entity.overall_summary_data.relation}}</td>
                <td>{{entity.overall_summary_data.no_of_documents}}</td>
               <td><img
						data-ng-show="entity.overall_summary_data.validity == 'valid'"
						src="<c:url value='/images/valid_entity.png'/>" class="valid">
						<img
						data-ng-show="entity.overall_summary_data.validity == 'invalid'"
						src="<c:url value='/images/invalid_entity.png'/>" class="invalid">
						<img
						data-ng-show="entity.overall_summary_data.validity == 'partial_valid'"
						src="<c:url value='/images/partial_valid_entity.png'/>"
						class="invalid"></td>
					<td><img
						data-ng-show="entity.overall_summary_data.itrValidity == 'valid'"
						src="<c:url value='/images/valid_entity.png'/>" class="valid">
						<img
						data-ng-show="entity.overall_summary_data.itrValidity == 'invalid'"
						src="<c:url value='/images/invalid_entity.png'/>" class="invalid">
						<span
						data-ng-show="entity.overall_summary_data.itrValidity == '-'">N.A.</span>
					</td>
            </tr>
        </table>
    </div>

    <div class="report-page-body" data-ng-repeat="entity in allEntities">
        <div class="customer-information-block">
            <h4 class="bold-center">Entity - {{entity.overall_summary_data.sr_no}}</h4>
            <div class="customer-info-table">
                <table class="table activity">
                    <tr>
                        <td class="key">Name</td>
                        <td class="value">{{entity.customer_information.name}}</td>
                    </tr>
                    <tr>
                        <td class="key">Entity Type</td>
                        <td class="value">{{entity.customer_information.entity_type}}</td>
                    </tr>
                    <tr data-ng-show="entity.customer_information.entity_type == 'Proprietory Concern' || entity.customer_information.entity_type == 'Hindu Undivided Family (HUF)'">
                        <td class="key">Name of âKarta/Proprietorâ</td>
                        <td class="value">{{entity.customer_information.kartaName}}</td>
                    </tr>
                    <tr>
                        <td class="key">Address</td>
                        <td class="value">{{entity.customer_information.address}}</td>
                    </tr>
                    <tr>
                        <td class="key">{{getDOBLabel(entity)}}</td>
                        <td class="value">{{entity.customer_information.dob | date: 'dd-MM-yyyy' }}</td>
                    </tr>
                    <tr data-ng-show="entity.show_gender">
                        <td class="key">Gender</td>
                        <td class="value">{{entity.customer_information.gender}}</td>
                    </tr>
                    <tr>
                        <td class="key">Contact No</td>
                        <td class="value">{{entity.customer_information.mobile}}</td>
                    </tr>
                    <tr>
                        <td class="key">Email ID</td>
                        <td class="value">{{entity.customer_information.email}}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="customer-document-info-table">
            <table class="table activity">
                <tr class="blue-backgroung-header">
                    <th> Document</th>
                    <th> Number</th>
                    <th> Status as per source</th>
                </tr>
                <tr data-ng-repeat="data in entity.customer_document_information">
                    <td>{{data.document}}</td>
                    <td>{{data.number}}</td>
                    <td>{{data.status_as_per_source}}</td>
                </tr>
            </table>
        </div>

        <div class="customer-document-info-table" data-ng-show="itr_details.document=='ITR'">
            <h4 class="bold-center">ITR Validation summary</h4>
            <div class="detailed-document-desc" data-ng-show="itr_details.document=='ITR'">
                <div class="document-desc">
                    <span>Document : </span>
                    <span>ITR</span>
                </div>
            </div>
            <table class="table activity" data-ng-show="itr_details.document=='ITR'">
                <tr class="blue-backgroung-header">
                    <th> Assessment Year</th>
                    <th> Status as per pan card</th>
                    <th> Status as per acknowledgement number</th>
                </tr>
                <tr data-ng-repeat="itr in itr_details.itr_details">
                    <td>{{itr.assessment_year}}</td>
                    <td>{{itr.status_as_per_source_pan}}</td>
                    <td>{{itr.status_as_per_source_ack}}</td>
                </tr>
            </table>
        </div>

        <div class="detailed-report-block">
            <h4 class="bold-center">Detailed Report</h4>
            <div class="detailed-document-desc" data-ng-repeat="document_data in entity.detailed_document_report_data">
                <div class="document-desc">
                    <span>Document : </span>
                    <span>{{document_data.document}}</span>
                </div>
                <div class="document-desc">
                    <span>Document Number : </span>
                    <span class="font-regular size-8">{{document_data.number}}</span>
                </div>
                <div class="document-desc">
                    <span>Status as per source : </span>
                    <span ng-class="{false:'color-red',true:'color-green'}[isValidStatus(document_data.status_as_per_source)]">{{document_data.status_as_per_source}}</span>
                </div>
                <div class="customer-info-table" data-ng-show="document_data.document_information"
                     style="padding-bottom: 10px">
                    <table class="table activity">
                        <tbody data-ng-repeat="(key,val) in document_data.document_information">
                        <tr data-ng-if="key!='validity_details' && key!='contribution_details'&& key!='epfo_details'">
                            <td class="key">{{key}}</td>
                            <td class="value">{{getValue(key,val)}}</td>
                        </tr>
                        <tr class="blue-backgroung-header"
                            data-ng-if="document_data.document_information.validity_details && key=='validity_details'">
                            <th class="blue-backgroung-header bold-center" colspan="2">Validity Details</th>
                        </tr>
                        <tr data-ng-if="document_data.document_information.validity_details && key=='validity_details'">
                            <td style="padding: 0px" class="width-full" colspan="2">
                                <table class="table margin-0 activity no-border">
                                    <tr class="validity-colspan">
                                        <td class="key">Class 0f Vehicle</td>
                                        <td class="key">Issue Date/Issuing Authority</td>
                                        <%--<td class="value">Valid To</td>--%>
                                    </tr>
                                    <tr class="validity-colspan"
                                        data-ng-repeat="validity_data in document_data.document_information.validity_details">
                                        <!-- <td class="value">{{validity_data.cov_category}}</td> -->
                                         <td class="value">{{validity_data.class_of_vehicle}}</td>
                                        <td class="value">{{validity_data.issue_date}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                 <div class="customer-info-table" data-ng-show="document_data.document_information"
                     style="padding-bottom: 10px">
                    <table class="table activity">
                        <tbody data-ng-repeat="(key,val) in document_data.document_information">
                        <tr class="blue-backgroung-header"
									data-ng-if="document_data.document_information.contribution_details && key=='contribution_details'">
									<th class="blue-backgroung-header bold-center" colspan="2">Contribution
										Details</th>
								</tr>
                       <tr
						 data-ng-if="document_data.document_information.contribution_details && key=='contribution_details'">
									<td style="padding: 0px" class="width-full" colspan="2">
										<table class="table margin-0 activity no-border">
											<tr class="validity-colspan">
												<td class="key">Employee Contribution</td>
												<td class="key">Total Monthly Wages</td>
												<td class="key">Wage Period</td>
												<td class="key">No Of Days For Wages Paid</td>
											</tr>
											<tr class="validity-colspan"
												data-ng-repeat="contribution_data in document_data.document_information.contribution_details">
												<td class="value">{{contribution_data.employee_contribution}}</td>
												<td class="value">{{contribution_data.no_of_days_wages_paid}}</td>
												<td class="value">{{contribution_data.wages_period}}</td>
												<td class="value">{{contribution_data.total_wages}}</td>
											</tr>
										</table>
									</td>
								</tr>
                        </tbody>
                    </table>
                </div>
                
                 <div class="customer-info-table" data-ng-show="document_data.document_information"
                     style="padding-bottom: 10px">
                    <table class="table activity">
                        <tbody data-ng-repeat="(key,val) in document_data.document_information">
                       
                        <tr class="blue-backgroung-header"
                            data-ng-if="document_data.document_information.epfo_details && key=='epfo_details'">
                            <th class="blue-backgroung-header bold-center" colspan="2">EPFO Details</th>
                        </tr>
                        <tr data-ng-if="document_data.document_information.epfo_details && key=='epfo_details'">
                            <td style="padding: 0px" class="width-full" colspan="2">
                                <table class="table margin-0 activity no-border">
                                    <tr class="validity-colspan">
                                        <td class="key">Name  </td>
                                        <td class="key">Employer ID </td>
                                        <td class="key">Balanace </td>
                                        <td class="key">Date of Birth </td>
                                         <td class="key">Employee Share  </td>
                                        <td class="key">Employer Share </td>
                                        <td class="key">Last Contributed Amount</td>
                                        <td class="key">Last Transaction Date</td>
                                        <td class="key">Settlement Status </td>
                                    </tr>
                                    <tr class="validity-colspan"
                                        data-ng-repeat="epfo_data in document_data.document_information.epfo_details">
                                        <td class="value">{{epfo_data.name}}</td>
                                        <td class="value">{{epfo_data.employer_id}}</td>
                                        <td class="value">{{epfo_data.balance}}</td>
                                        <td class="value">{{epfo_data.date_of_birth}}</td>
                                        <td class="value">{{epfo_data.employee_share}}</td>
                                        <td class="value">{{epfo_data.employer_share}}</td>
                                        <td class="value">{{epfo_data.last_contributed_amount}}</td>
                                        <td class="value">{{epfo_data.last_transaction_date}}</td>
                                        <td class="value">{{epfo_data.settlement_status}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                
                
                
                <table data-ng-show="document_data.checksum">
                    <tr data-ng-if="document_data.checksum">
                        <td style="padding: 0px" class="width-full">
                            <table class="table margin-0 activity">
                                <tr class="validity-colspan">
                                    <td data-ng-repeat="name_char in getNameRow(entity, document_data) track by $index"
                                        class="barcode">{{name_char}}
                                    </td>
                                </tr>
                                <tr class="validity-colspan">
                                    <td data-ng-repeat="checksum_char in getChecksumRow(entity, document_data) track by $index"
                                        class="barcode">{{checksum_char}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="table activity" data-ng-show="document_data.frequencies">
                    <Tr class="blue-backgroung-header">
                        <TH colspan="2" class="blue-backgroung-header bold-center">Frequencies</TH>
                    </Tr>
                    <tr>
                        <th>Financial Year</th>
                        <th>Frequency Name</th>
                    </tr>
                    <tr data-ng-repeat="frequency in document_data.frequencies">
                        <td class="key">
                            {{frequency.financialYear}}
                        </td>
                        <td class="value">
                            {{frequency.frequencyName}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>