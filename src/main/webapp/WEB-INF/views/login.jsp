<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title>TotalKYC</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/loginController.js'/>"></script>
     <script type="application/javascript" src="<c:url value='/js/controllers/utilController.js'/>"></script>
     		<%
			response.setHeader("Pragma","no-cache");
			response.setHeader("Cache-Control","no-store");
			response.setHeader("Expires","0");
			response.setDateHeader("Expires",-1);
			%>
   
</head>
<body data-ng-controller="loginController" ng-cloak>
<div class="login-page">
    <div class="text-center">
        <div>
            <div class="login-block">
                <div>
                    <h1 class="login-header"><span class="insta">Total</span><span class="kyc">KYC</span></h1>
                    <h6 class="caption">Karza web app framework @ 2016</h6>
                </div>
                <div class="no-padding">
                    <form class="form-signin" action="login.do" method="post">
                        <label for="email" class="sr-only">User ID</label>
                        <input type="text" id="email" name="email" class="form-control" data-ng-model="user.email"
                               placeholder="User ID" autofocus="">
                        <label for="password" class="sr-only">Password</label>
                        <input type="password" id="password" name="password" class="form-control"
                               data-ng-model="user.password" placeholder="Password">
                        <span class="error-message no-border" ng-show="error">Invalid Login ID or Password</span>
                        <span class="error-message no-border" ng-show="suspended">Account suspended</span>
                        <span class="error-message no-border" ng-show="invalidSession">max logIn reached</span>
                        <button class="btn btn-lg btn-primary btn-block login-btn" type="submit">Log In</button>
                        <div class="margin-20"></div>
                        <div class="col-xs-12">
                            <a href="forgotPassword" class="forgot-password">Forgot Password?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div style="margin-top: 20px;">
            <a href="http://www.karza.in" style="text-decoration: none;">
                <img src="<c:url value='/images/logo.png'/>" class="logo">
            </a>
        </div>

        <div class="help-block">
            <a href="terms"><u>Terms of Use</u></a>
            <span class="pie">|</span>
            <a href=""><u>Privacy Policy</u></a>
            <span class="pie">|</span>
            <a href="disclaimer"><u>Disclaimer</u></a>
            <span class="pie">|</span>
            <a href="http://www.karza.in/contact-us.html"><u>Contact Us</u></a>
        </div>
    </div>

</div>
<div id="spinnerDiv" ng-show="isLoadingInProgress || showLoadingInProgress">
    <img src="<c:url value='/images/spinner.gif'/>" class="ajaxloader"/>
</div>
</body>
</html>