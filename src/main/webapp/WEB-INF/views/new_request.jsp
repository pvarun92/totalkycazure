<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
<meta charset="UTF-8">
<title></title>

<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/bootstrap.min.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/common.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/select.css'/>" />
<link
	href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>"
	rel="stylesheet">

<script type="application/javascript"
	src="<c:url value='/js/static/angular.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/select_files/select.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/lodash.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/ngMask.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/dirPagination.js'/>"></script>
<script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/controllers/newRequestController.js'/>"></script>

<style>
.close {
	line-height: 0;
	position: relative;
	left: 10px;
}

.tab-label {
	display: block !important;
	width: 120px !important;
	min-width: 100px;
	text-overflow: ellipsis;
	white-space: nowrap;
	overflow: hidden;
	float: left;
}
</style>
</head>
<body data-ng-controller="newRequestController" ng-cloak>
	<div
		class="navbar navbar-default no-margin page-header-block margin-0 no-radius">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="col-xs-6">
			<H3>{{reques_label}}</H3>
		</div>
		<div class="col-xs-6"></div>
	</div>
	<div class="page-container" style="padding: 1%;">
		<form name="new_request{{form_data[selectedTab].form_id}}" novalidate
			class="css-form">
			<fieldset
				data-ng-disabled="userRole == 'View' || request_status == 'in_queue'">
				<div class="col-xs-12 no-padding" style="display: inline-flex">
					<label class="color-blue size-10 font-bold"
						style="min-width: 150px">Customer Information</label>
					<hr style="margin-top: 14px;">
					<span new-request-form-buttons style="min-width: 80px;"
						data-ng-hide="userRole == 'View' || request_status == 'in_queue'">
						<button class="fa fa-lg fa-floppy-o save-form"
							data-ng-click="saveForm()" title="Save Request"></button>
						<button type="button" class="fa fa-lg fa-trash-o save-form"
							data-ng-click="deleteForm()" title="Delete Request"
							data-ng-hide="request_status == 'submitted'"></button>
					</span>
				</div>
				<ul class="nav nav-tabs no-border" role="tablist">
					<li role="presentation"
						ng-repeat="tab in form_data track by $index"
						ng-click="selectTab($index)"
						ng-class="{'active':selectedTab == $index}"><a
						data-target="#tab" aria-controls="home" role="tab"
						data-toggle="tab"> <span class="tab-label"> <span
								data-ng-show="(tab.entity_type == 'Individual') && (tab.full_name.first_name || tab.full_name.middle_name || tab.full_name.last_name) && !tab.entity">
									<span>{{tab.full_name.first_name}} </span> <span>{{tab.full_name.middle_name}}
								</span> <span>{{tab.full_name.last_name}}</span>
							</span> <span data-ng-show="tab.entity"> <span>{{tab.entity}}</span>
							</span> <span
								data-ng-hide="tab.entity || (tab.entity_type == 'Individual' && (tab.full_name.first_name || tab.full_name.middle_name || tab.full_name.last_name))">
									<span>New Entity</span>
							</span>
						</span> <span class="close"
							data-ng-hide="userRole == 'View' || request_status == 'in_queue'"
							ng-click="deleteTab($index)">×</span>
					</a></li>
					<!--<li role="presentation" ng-click="addTab()">-->
					<!--<a aria-controls="home" role="tab" data-toggle="tab">( + )</a>-->
					<!--</li>-->
					<li role="presentation"
						data-ng-hide="userRole == 'View' || request_status == 'in_queue'"
						class="add-new-tab" data-ng-click="addTab()"
						title="Add Related Entity"><a> <i class="fa fa-user-plus"></i>
					</a></li>
				</ul>
				<div class="tab-content" data-ng-show="form_data.length">
					<div role="tabpanel" class="tab-pane active" id="tab">
						<div class="new-request-container">
							<div class="col-xs-12" style="padding-top: 15px">
								<div>
									<div class="field-block col-xs-12">
										<div class="col-xs-4 no-padding">
											<label class="col-xs-6 no-padding">Entity Type:</label> <label
												class="select-label no-padding col-xs-6"
												style="margin-top: 0px"> <select id="entity_type"
												name="entity_type" class="input-field"
												data-ng-model="form_data[selectedTab].entity_type"
												data-ng-change="selectForm(selectedTab);clearForm(selectedTab)">
													<option value="">Select</option>
													<option value="Individual">Individual</option>
													<option value="Proprietory Concern">Proprietory
														Concern</option>
													<option value="Hindu Undivided Family (HUF)">Hindu
														Undivided Family (HUF)</option>
													<option value="Partnership">Partnership</option>
													<option value="Limited Liability Partnership (LLP)">Limited
														Liability Partnership (LLP)</option>
													<option value="Company">Company</option>
													<option value="Trust">Trust</option>
													<option value="Association of Persons (AOP)">Association
														of Persons (AOP)</option>
													<option value="Body of Individuals (BOI)">Body of
														Individuals (BOI)</option>
													<option value="Society">Society</option>
											</select>
											</label>
										</div>
										<div class="col-xs-4">
											<label class="col-xs-6 no-padding">Relation:</label> <label
												class="select-label no-padding col-xs-6"
												style="margin-top: 0px"> <select id="relation"
												name="relation" class="input-field"
												data-ng-model="form_data[selectedTab].relation">
													<option value="">Select</option>
													<option value="Applicant">Applicant</option>
													<option value="Co-applicant">Co-Applicant</option>
													<option value="Guarantor">Guarantor</option>
													<option value="Nominee/Assignee">Nominee/Assignee</option>
													<option value="Proprietor/Karta/Partner/Director/Trustee">
														Proprietor/Karta/Partner/Director/Trustee</option>
													<option value="Authorised Representative/Signatory">Authorised
														Representative/Signatory</option>
													<option value="Beneficiary">Beneficiary</option>
													<option value="Customer/Supplier">Customer/Supplier</option>
													<option value="Group Entity">Group Entity</option>
													<option value="Court Appointed Official">Court
														Appointed Official</option>
													<option value="Other">Other</option>

											</select>
											</label>
										</div>
										<div class="col-xs-4">
											<label class="col-xs-6 no-padding">Branch:</label> <label
												class="select-label no-padding col-xs-6"
												style="margin-top: 0px"> <select id="branch"
												name="branch" class="input-field"
												data-ng-model="form_data[selectedTab].branch">
													<option value="">Select</option>
													<option data-ng-repeat="branch in branches"
														id="{{branch.branchName}}" value="{{branch.id}}">{{branch.branchName}}
													</option>
											</select>
											</label>
										</div>
									</div>
									<div class="col-xs-12 no-padding"
										data-ng-show="form_data[selectedTab].entity_type && form_data[selectedTab].relation && form_data[selectedTab].branch">
										<div class="field-block col-xs-12"
											data-ng-hide="form_type[selectedTab] == 'individuals'">
											<div class="col-xs-2 no-padding">
												<label>Entity:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<input type="text" name="entity" id="entity"
													data-ng-model="form_data[selectedTab].entity"
													class="input-field"
													data-ng-blur="validateEntity(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.entity]" />
												<span class="error-message"
													ng-show="form_data[selectedTab].errorRequired.entity">Entity
													is required</span>
											</div>
										</div>

										<div class="field-block col-xs-12"
											data-ng-hide="form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-2 no-padding">
												<label>{{form_data[selectedTab].name_label}}:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="group-3-text-boxes">
													<input type="text" name="first_name" id="first_name"
														data-ng-model="form_data[selectedTab].full_name.first_name"
														placeholder="First Name" class="col-xs-4 first input-font"
														data-ng-blur="validateName(selectedTab)" maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.full_name]" />
													<input type="text" name="middle_name" id="middle_name"
														data-ng-model="form_data[selectedTab].full_name.middle_name"
														placeholder="Middle Name"
														class="col-xs-4 second input-font"
														data-ng-blur="validateName(selectedTab)" maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.full_name]" />
													<input type="text" name="last_name" id="last_name"
														data-ng-model="form_data[selectedTab].full_name.last_name"
														placeholder="Last Name" class="col-xs-4 third input-font"
														data-ng-blur="validateName(selectedTab)" maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.full_name]" />
												</div>
												<div class="no-border">
													<span class="error-message no-border"
														ng-show="form_data[selectedTab].invalid.full_name">Please
														enter complete name</span>
												</div>
											</div>
										</div>

										<div class="field-block col-xs-12"
											data-ng-show="form_data[selectedTab].entity_type == 'Individual'">
											<div class="col-xs-2 no-padding">
												<label>Fathers/Husband Name:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="group-3-text-boxes">
													<input type="text" name="fh_first_name" id="fh_first_name"
														data-ng-model="form_data[selectedTab].father_husband_name.first_name"
														placeholder="First Name" class="col-xs-4 first input-font"
														data-ng-blur="validateFatherName(selectedTab)"
														maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.father_husband_name]" />
													<input type="text" name="fh_middle_name"
														id="fh_middle_name"
														data-ng-model="form_data[selectedTab].father_husband_name.middle_name"
														placeholder="Middle Name"
														class="col-xs-4 second input-font"
														data-ng-blur="validateFatherName(selectedTab)"
														maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.father_husband_name]" />
													<input type="text" name="fh_last_name" id="fh_last_name"
														data-ng-model="form_data[selectedTab].father_husband_name.last_name"
														placeholder="Last Name" class="col-xs-4 third input-font"
														data-ng-blur="validateFatherName(selectedTab)"
														maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.father_husband_name]" />
												</div>
												<div class="no-border">
													<span class="error-message no-border"
														ng-show="form_data[selectedTab].invalid.father_husband_name">Please
														enter complete name</span>
												</div>
											</div>
										</div>

										<div class="field-block col-xs-12">
											<div class="col-xs-2 no-padding">
												<label>Address:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<input type="text" name="address1" id="address1"
													data-ng-model="form_data[selectedTab].address.address_line1"
													class="input-field input-field"
													data-ng-blur="validateAddress1(selectedTab)"
													maxlength="255"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.address.address_line1]" />
												<span class="error-message"
													ng-show="form_data[selectedTab].errorRequired.address.address_line1">Address
													Line 1 is required</span>
											</div>
										</div>
										<div class="field-block col-xs-12">
											<div class="col-xs-2 no-padding"></div>
											<div class="col-xs-6 no-padding">
												<input type="text" name="address2" id="address2"
													maxlength="255"
													data-ng-model="form_data[selectedTab].address.address_line2"
													class="input-field" />
											</div>
										</div>
										<div class="field-block col-xs-12">
											<div class="col-xs-2 no-padding">
												<label>State:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="col-xs-6"
													style="padding: 0px; padding-right: 1px">
													<label class="select-label col-xs-12 no-padding"
														style="margin: 0px"
														data-ng-hide="form_data[selectedTab].address.country != 'India'">
														<select id="state" name="state"
														data-ng-model="form_data[selectedTab].address.state"
														class="input-field selectboxit-arrow-container"
														data-ng-blur="validateState(selectedTab)"
														data-ng-change="validateState(selectedTab)"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.address.state]">
															<option value="">Select</option>
															<option data-ng-repeat="state in states"
																value="{{state}}">{{state}}</option>
													</select>
													</label> <input type="text" name="state1" id="state1"
														maxlength="255"
														data-ng-show="form_data[selectedTab].address.country != 'India'"
														data-ng-model="form_data[selectedTab].address.state"
														class="input-field"
														data-ng-blur="validateState(selectedTab)"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.address.state]" />
													<span class="error-message"
														ng-show="form_data[selectedTab].errorRequired.address.state">State
														is required</span>
												</div>
												<div class="col-xs-2 no-padding text-center">
													<label>Pin Code:</label>
												</div>
												<div class="col-xs-4"
													style="padding: 0px; padding-left: 1px">
													<input type="text" maxlength="6" name="pincode"
														id="pincode"
														data-ng-model="form_data[selectedTab].address.pincode"
														class="input-field" only-numeric ng-maxlength="6"
														data-ng-blur="validatePincode(selectedTab)"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.address.pincode || form_data[selectedTab].invalid.address.pincode.invalid_pin]"
														required only-numeric /> <span
														class="error-message no-border"
														ng-show="form_data[selectedTab].errorRequired.address.pincode">Pincode
														is required</span> <span class="error-message no-border"
														ng-show="form_data[selectedTab].invalid.address.pincode.select_state && !form_data[selectedTab].errorRequired.address.pincode">Please
														select State first</span> <span class="error-message no-border"
														ng-show="form_data[selectedTab].invalid.address.pincode.invalid_pin && !form_data[selectedTab].invalid.address.pincode.select_state && !form_data[selectedTab].errorRequired.address.pincode">Invalid
														pincode</span>
												</div>
											</div>
										</div>

										<div class="field-block col-xs-12">
											<div class="col-xs-2 no-padding">
												<label>Country:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="col-xs-6"
													style="padding: 0px; padding-right: 1px">
													<label class="select-label col-xs-12 no-padding"
														style="margin: 0px"> <select id="country"
														name="country"
														data-ng-model="form_data[selectedTab].address.country"
														class="input-field selectboxit-arrow-container"
														data-ng-blur="validateCountry(selectedTab)"
														data-ng-change="validateCountry(selectedTab)"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.address.country]">
															<option value="">Select</option>
															<option data-ng-repeat="country in countries"
																value="{{country.name}}">{{country.name}}</option>
													</select>
													</label> <span class="error-message"
														ng-show="form_data[selectedTab].errorRequired.address.country">Country
														is required</span>
												</div>
												<div class="col-xs-2 no-padding text-center"></div>
												<div class="col-xs-4"
													style="padding: 0px; padding-left: 1px"></div>
											</div>
										</div>

										<div class="field-block col-xs-12">

											<div class="col-xs-2 no-padding">
												<label>{{form_data[selectedTab].dob_label}}:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="col-md-12 no-padding">
													<p class="input-group datepicker-block" data-ng-hide="true">
													<div class="col-xs-6 no-padding date-picker">
														<span data-ng-hide="true"
															class="form-control datepicker-text" data-ng-model="dob"
															data-ng-change="selectDOBDate(selectedTab)"
															data-ng-minlength="10" data-ng-maxlength="10"
															uib-datepicker-popup is-open="dob_datepicker.opened"
															datepicker-options="dateOptions" min-date="minDate"
															max-date="maxDate" date-disabled="disabled(date, mode)"
															ng-required="true" close-text="Close"></span>
														<div class="col-xs-12 no-padding">

															<input type="text" name="dob"
																data-ng-model="form_data[selectedTab].dob"
																class="input-field ng-pristine ng-touched ng-valid ng-valid-required ng-invalid ng-invalid-required"
																placeholder="DD/MM/YYYY"
																data-ng-blur="validateDob(selectedTab)"
																data-ng-minlength="10" data-ng-maxlength="10"
																mask="31/12/9999" clean="false" mask-restrict="accept"
																mask-validate="false" only-numeric-with-slash=""
																ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.dob]"
																<%--ng-disabled=true--%>
                                                               required="" "/>
														</div>
													</div>
													<div class="col-xs-1 no-padding text-center">
														<span class="input-group-btn">
															<button type="button" class="btn btn-default"
																ng-click="showDOBCalendar()" title="Date Of Birth"
																style="padding: 0px; border: none;">
																<i class="fa fa-lg fa-calendar"></i>
															</button>
														</span>
													</div>
													</p>
													<div class="col-xs-12 no-padding">
														<div class="col-xs-12 no-padding"
															data-ng-show="form_data[selectedTab].invalid.dob">
															<span class="error-message no-border">Invalid Date</span>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="field-block col-xs-12"
											data-ng-show="form_data[selectedTab].entity_type == 'Individual'">
											<div class="col-xs-2 no-padding">
												<label>Gender:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="col-xs-6"
													style="padding: 0px; padding-right: 1px">
													<label class="select-label col-xs-12 no-padding"
														style="margin: 0px"> <select id="gender"
														name="gender"
														data-ng-model="form_data[selectedTab].gender"
														class="input-field selectboxit-arrow-container"
														data-ng-change="validateGender(selectedTab)"
														data-ng-blur="validateGender(selectedTab)"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].errorRequired.gender]">
															<option value="">Select</option>
															<option value="male">Male</option>
															<option value="female">Female</option>
															<option value="transgender">Transgender</option>
													</select>
													</label> <span class="error-message"
														ng-show="form_data[selectedTab].errorRequired.address.gender">State
														is required</span>
												</div>
												<div class="col-xs-2 no-padding text-center"></div>
												<div class="col-xs-4"
													style="padding: 0px; padding-left: 1px"></div>
											</div>
										</div>

										<div class="field-block col-xs-12">
											<div class="col-xs-2 no-padding">
												<label>Email ID:</label>
											</div>
											<div class="col-xs-6 no-padding">
												<div class="col-xs-6"
													style="padding: 0px; padding-right: 1px">
													<input type="text" name="email" id="email"
														class="input-field"
														data-ng-model="form_data[selectedTab].email"
														data-ng-blur="validateEmailApi(selectedTab)"
														maxlength="255"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.email || invalidFields[selectedTab].email]" />
													<span class="error-message third no-border"
														ng-show="form_data[selectedTab].invalid.email">Invalid
														Email</span> <span class="error-message"
														ng-show="invalidFields[selectedTab].email">{{invalidFields[selectedTab].email}}</span>
												</div>
												<div class="col-xs-2 no-padding text-center">
													<label>Contact No:</label>
												</div>
												<div class="col-xs-4"
													style="padding: 0px; padding-left: 1px">
													<input type="text" name="contact_number"
														id="contact_number"
														data-ng-model="form_data[selectedTab].contact_number"
														class="input-field"
														data-ng-blur="validateContactNumber(selectedTab)"
														maxlength="10"
														ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].invalid.contact_number]"
														only-numeric /> <span
														class="error-message third no-border"
														ng-show="form_data[selectedTab].invalid.contact_number && !form_data[selectedTab].errorRequired.contact_number">Invalid
														number</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12"
								data-ng-show="form_data[selectedTab].entity_type && form_data[selectedTab].relation && form_data[selectedTab].branch"
								style="margin-bottom: 15px">
								<div class="col-xs-12 no-padding" style="display: inline-flex">
									<label style="min-width: 85px; color: #337ab7">KYC
										Document</label>
									<hr style="margin-top: 14px;">
								</div>
								<div>
									<div class="col-xs-12 KYC-document-block no-padding">
										<div class="in-line-block col-xs-12 no-padding  KYC-document">
											<div class="col-xs-1  no-padding">
												<input type="checkbox" name="pan_check" id="pan_check"
													data-ng-model="form_data[selectedTab].pan.check"
													data-ng-change="changeItrStatus(selectedTab)" />
											</div>
											<div class="col-xs-2  no-padding">
												<label>PAN:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="pan" id="pan"
													data-ng-disabled="!form_data[selectedTab].pan.check"
													data-ng-model="form_data[selectedTab].pan.unique_no"
													class="input-field" data-ng-blur="validatePan(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].pan.check && (form_data[selectedTab].errorRequired.pan.unique_no || invalidFields[selectedTab].pan)]"
													maxlength="10" no-special-char required />
												<!-- form_data[selectedTab].pan.check && form_data[selectedTab].errorRequired.pan.unique_no  -->

												<span class="error-message"
													ng-show="form_data[selectedTab].pan.invalid && form_data[selectedTab].pan.check">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].pan.check && invalidFields[selectedTab].pan">{{invalidFields[selectedTab].pan}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="pan_upload" id="pan_upload"
														data-ng-model="form_data[selectedTab].pan.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> Enter the PAN Number
														mentioned just below the “Permanent Account Number” on
														the PAN card at the bottom on the LHS.<br> It is 10
														digit alphanumeric number.<br> Format should be
														AAAAANNNNA.<br> A=Alphabet<br> N=Numerical
												</span></a> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding"
												ng-if="form_data[selectedTab].reg_certificate_label=='Registration Certificate'">
												<input type="checkbox" name="llpin_check" id="llpin_check"
													data-ng-model="form_data[selectedTab].llpin.check"
													ng-change="freezeDocument('registration',selectedTab)" />
											</div>
											<div class="col-xs-1 no-padding"
												ng-if="form_data[selectedTab].reg_certificate_label!='Registration Certificate'">
												<input type="checkbox" name="llpin_check" id="llpin_check"
													data-ng-model="form_data[selectedTab].llpin.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>{{form_data[selectedTab].reg_certificate_label}}:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="llpin" id="llpin" maxlength="21"
													data-ng-disabled="!form_data[selectedTab].llpin.check"
													data-ng-model="form_data[selectedTab].llpin.unique_no"
													class="input-field"
													data-ng-blur="validateLlpin(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].llpin.check && (form_data[selectedTab].errorRequired.llpin.unique_no || invalidFields[selectedTab].llpin)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].llpin.check && form_data[selectedTab].llpin.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].llpin.check && invalidFields[selectedTab].llpin">{{invalidFields[selectedTab].llpin}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="llpin_upload" id="llpin_upload"
														data-ng-model="form_data[selectedTab].llpin.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span
													data-ng-show="form_data[selectedTab].reg_certificate_label == 'LLPIN/FLLPIN'"
													style="text-align: left"> It is 8 or 9 digits
														alphanumeric number.<br> Format should be<br>
														“AAA-NNNN” in case of 8 digits number<br>
														“AAAA-NNNN” in case of 9 digits number<br> Enter
														the Exact number as mentioned on the LLP/FLLP registration
														certificate including special<br> character (-).<br>
														A=Alphabet<br> N=Numerical
												</span> <span
													data-ng-show="form_data[selectedTab].reg_certificate_label == 'CIN/FCRN'"
													style="text-align: left"> It is 6 or 21 digits
														alphanumeric number.<br> Format should be<br>
														“ANNNNN” in case of 6 digits number<br>
														“ANNNNNAANNNNAAANNNNNN” in case of 21 digits number<br>
														A=Alphabet<br> N=Numerical
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="vat_check" id="vat_check"
													data-ng-model="form_data[selectedTab].vat.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>VAT Registration:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="vat" id="vat" maxlength="12"
													data-ng-disabled="!form_data[selectedTab].vat.check"
													data-ng-model="form_data[selectedTab].vat.unique_no"
													class="input-field" data-ng-blur="validateVat(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].vat.check && (form_data[selectedTab].errorRequired.vat.unique_no || invalidFields[selectedTab].vat)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].vat.check && form_data[selectedTab].vat.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].vat.check && invalidFields[selectedTab].vat">{{invalidFields[selectedTab].vat}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="vat_upload" id="vat_upload"
														data-ng-model="form_data[selectedTab].vat.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> It is 11 or 12 or 14
														digits alphanumeric number.<br> Format should be<br>
														“NNNNNNNNNNN” in case of 11 digits number<br>
														“NNNNNNNNNNNA” in case of 12 digits number<br>
														“AAANNNNNNNNNNN” in case of 14 digits number<br>
														Enter the Exact number as mentioned on the VAT
														registration Certificate including “0” as the first
														digit.<br> A=Alphabet<br> N=Numerical
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="cst_check" id="cst_check"
													data-ng-model="form_data[selectedTab].cst.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>CST Registration:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="cst" id="cst" maxlength="12"
													data-ng-disabled="!form_data[selectedTab].cst.check"
													data-ng-model="form_data[selectedTab].cst.unique_no"
													class="input-field" data-ng-blur="validateCst(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].cst.check && (form_data[selectedTab].errorRequired.vat.unique_no || invalidFields[selectedTab].cst)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].cst.check && form_data[selectedTab].cst.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].cst.check && invalidFields[selectedTab].cst">{{invalidFields[selectedTab].cst}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="cst_upload" id="cst_upload"
														data-ng-model="form_data[selectedTab].cst.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> It is the
														alphanumeric number. <br> Enter the Exact number as
														mentioned on the CST registration certificate including
														“0” as the first digit, space & special characters (/,
														-, \).

												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="pt_check" id="pt_check"
													data-ng-model="form_data[selectedTab].pt.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Profession Tax:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="pt" id="pt" maxlength="12"
													data-ng-disabled="!form_data[selectedTab].pt.check"
													data-ng-model="form_data[selectedTab].pt.unique_no"
													class="input-field" data-ng-blur="validatePt(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].pt.check && (form_data[selectedTab].errorRequired.pt.unique_no || invalidFields[selectedTab].pt)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].pt.check && form_data[selectedTab].pt.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].pt.check && invalidFields[selectedTab].pt">{{invalidFields[selectedTab].pt}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="pt_upload" id="pt_upload"
														data-ng-model="form_data[selectedTab].pt.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="min-width: 150px; margin-left: -5px;">Information</span></a>
												</a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="iec_check" id="iec_check"
													data-ng-model="form_data[selectedTab].iec.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Import Export Reg.:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="iec" id="iec" maxlength="10"
													data-ng-disabled="!form_data[selectedTab].iec.check "
													no-special-char
													data-ng-model="form_data[selectedTab].iec.unique_no"
													class="input-field" data-ng-blur="validateIec(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].iec.check && (form_data[selectedTab].errorRequired.iec.unique_no || invalidFields[selectedTab].iec)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].iec.check && form_data[selectedTab].iec.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].iec.check && invalidFields[selectedTab].iec">{{invalidFields[selectedTab].iec}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="iec_upload" id="iec_upload"
														data-ng-model="form_data[selectedTab].iec.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> It is 10 digits
														numerical number.<br> Enter the Exact number as
														mentioned on the Import Export registration Certificate
														including “0” as the first digit.
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="excise_check" id="excise_check"
													data-ng-model="form_data[selectedTab].excise.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Excise Registration:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="excise" id="excise" maxlength="15"
													data-ng-disabled="!form_data[selectedTab].excise.check"
													data-ng-model="form_data[selectedTab].excise.unique_no"
													class="input-field"
													data-ng-blur="validateExcise(selectedTab)" uppercased
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].excise.check && (form_data[selectedTab].errorRequired.excise.unique_no || invalidFields[selectedTab].excise)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].excise.check && form_data[selectedTab].excise.invalid ">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].excise.check && invalidFields[selectedTab].excise">{{invalidFields[selectedTab].excise}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="excise_upload" id="excise_upload"
														data-ng-model="form_data[selectedTab].excise.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> It is 15 digits
														alphanumeric Number.<br> Format should be
														AAAAANNNNAAANNN.<br> 11th & 12th Characters can be
														any of these: “XM” or “XD” or “CE” or “EM”
														or “ED”.<br> A=Alphabet <br> N=Numerical
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-show="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="service_tax_check"
													id="service_tax_check"
													data-ng-model="form_data[selectedTab].service_tax.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Service Tax:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="service_tax" id="service_tax"
													maxlength="15"
													data-ng-disabled="!form_data[selectedTab].service_tax.check"
													data-ng-model="form_data[selectedTab].service_tax.unique_no"
													class="input-field"
													data-ng-blur="validateServiceTax(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].service_tax.check && (form_data[selectedTab].errorRequired.service_tax.unique_no || invalidFields[selectedTab].service_tax)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].service_tax.check && form_data[selectedTab].service_tax.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].service_tax.check && invalidFields[selectedTab].service_tax">{{invalidFields[selectedTab].service_tax}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="service_tax_upload"
														id="service_tax_upload"
														data-ng-model="form_data[selectedTab].service_tax.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> It is 15 digits
														alphanumeric Number.<br> Format should be
														AAAAANNNNAAANNN.<br> 11th & 12th Characters can be
														either “ST” or “SD”.<br> A=Alphabet<br>
														N=Numerical
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="aadhar_check" id="aadhar_check"
													data-ng-model="form_data[selectedTab].aadhar.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Aadhar:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="aadhar" id="aadhar" maxlength="12"
													data-ng-disabled="!form_data[selectedTab].aadhar.check"
													data-ng-model="form_data[selectedTab].aadhar.unique_no"
													class="input-field"
													data-ng-blur="validateAadhar(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].aadhar.check && (form_data[selectedTab].errorRequired.aadhar.unique_no || invalidFields[selectedTab].aadhar)]"
													no-special-char only-numeric required /> <span
													class="error-message"
													ng-show="form_data[selectedTab].aadhar.check && form_data[selectedTab].aadhar.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].aadhar.check && invalidFields[selectedTab].aadhar">{{invalidFields[selectedTab].aadhar}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="aadhar_upload" id="aadhar_upload"
														data-ng-model="form_data[selectedTab].aadhar.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> Enter the Aadhaar
														Number mentioned at the Bottom of the Aadhaar Card.<br>
														It is 12 digit numerical number.<br> 12th digit is
														the check digit.
												</span> </a> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="dl_check" id="dl_check"
													data-ng-model="form_data[selectedTab].dl.check" <%-- ng-change="freezeDocument('dl',selectedTab)"--%>/>
											</div>
											<div class="col-xs-2 no-padding">
												<label>Driver's License:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="dl" id="dl" maxlength="30"
													data-ng-disabled="!form_data[selectedTab].dl.check"
													alpha-numeric-with-slash-and-hyphen
													data-ng-model="form_data[selectedTab].dl.unique_no"
													class="input-field" data-ng-blur="validateDl(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].dl.check && (form_data[selectedTab].errorRequired.dl.unique_no || invalidFields[selectedTab].dl)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].dl.check && form_data[selectedTab].dl.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].dl.check && invalidFields[selectedTab].dl">{{invalidFields[selectedTab].dl}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="dl_upload" id="dl_upload"
														data-ng-model="form_data[selectedTab].dl.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> Enter the Driving
														License Number mentioned besides the “D/L No” or
														“Driving License No” at the Top of the Driving License
														Card.<br> It is the alphanumeric number.<br>
														Enter the Exact number as mentioned on the Driving License
														slip including space & special characters (*, /, -, \).<br>
														Note: Only Driving License cards with embedded chip can be
														verified.
												</span> </a>
											</div>
											<div class="col-xs-1"></div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="passport_check"
													id="passport_check"
													data-ng-model="form_data[selectedTab].passport.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Passport:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="passport" id="passport"
													maxlength="9"
													data-ng-disabled="!form_data[selectedTab].passport.check"
													data-ng-model="form_data[selectedTab].passport.unique_no"
													class="input-field" no-special-char
													data-ng-blur="validatePassport(selectedTab)" uppercased
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].passport.check && (form_data[selectedTab].errorRequired.passport.unique_no || invalidFields[selectedTab].passport)]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].passport.check && form_data[selectedTab].passport.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].passport.check && invalidFields[selectedTab].passport">{{invalidFields[selectedTab].passport}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="passport_upload"
														id="passport_upload"
														data-ng-model="form_data[selectedTab].passport.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> Enter the Passport
														Number mentioned besides the “Country Code” and under
														“Passport No” at the Top RHS of the Passport Copy.<br>
														It is 8 digit Alphanumeric number.<br> Format should
														be ANNNNNNN.<br> Expiry Date is the date of expiry of
														Passport.<br> A=Alphabet<br> N=Numerical
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding"></div>
											<div class="col-xs-2 no-padding">
												<label>Expiry Date:</label>
											</div>
											<div class="col-xs-4 no-padding">
												<div class="col-md-12">
													<p class="input-group datepicker-block" data-ng-hide="true">
													<div class="col-xs-9 no-padding date-picker">
														<span data-ng-hide="true"
															class="form-control datepicker-text"
															data-ng-model="passport_expiry_date"
															data-ng-change="selectPassportExpiryDate(selectedTab)"
															uib-datepicker-popup
															is-open="passport_expiry_datepicker.opened"
															min-date="minDate" max-date="maxDate"
															datepicker-options="dateOptions"
															date-disabled="disabled(date, mode)" ng-required="true"
															close-text="Close"></span>
														<div class="group-3-text-boxes col-xs-12 no-padding"
															style="min-width: 100px;">
															<input type="text" name="passport_expiry"
																data-ng-model="form_data[selectedTab].passport.expiry_date"
																class="input-field" placeholder="DD/MM/YYYY"
																data-ng-blur="validatePassportValidity(selectedTab)"
																mask="31/12/9999" clean="false" mask-restrict="accept"
																mask-validate="false" only-numeric-with-slash
																data-ng-disabled="!form_data[selectedTab].passport.check"
																ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].passport.check && form_data[selectedTab].invalid.passport]"
																required />
														</div>
													</div>
													<div class="col-xs-2 no-padding text-center">
														<span class="input-group-btn">
															<button type="button" class="btn btn-default"
																ng-click="showPassportCalendar()" title="Expiry Date"
																style="padding: 0px; border: none;">
																<i class="fa fa-lg fa-calendar"></i>
															</button>
														</span>
													</div>
													</p>
													<div class="no-border col-xs-9 no-padding">
														<span class="error-message first no-border"
															ng-show="form_data[selectedTab].passport.check && form_data[selectedTab].invalid.passport">Invalid
															Date</span>
													</div>
												</div>
											</div>
											<div class="col-xs-1"></div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="voting_check" id="voting_check"
													data-ng-model="form_data[selectedTab].voting.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Voters’ ID:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="voting" id="voting" maxlength="25"
													data-ng-disabled="!form_data[selectedTab].voting.check"
													data-ng-model="form_data[selectedTab].voting.unique_no"
													class="input-field"
													data-ng-blur="validateVoting(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].voting.check && (form_data[selectedTab].errorRequired.voting.unique_no || invalidFields[selectedTab].voting)]"
													alpha-numeric-with-slash required /> <span
													class="error-message"
													ng-show="form_data[selectedTab].voting.check && form_data[selectedTab].voting.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].voting.check && invalidFields[selectedTab].voting">{{invalidFields[selectedTab].voting}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="voting_upload" id="voting_upload"
														data-ng-model="form_data[selectedTab].voting.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> Enter the Voter ID
														Number mentioned besides the “Photo” at the LHS of the
														Voter ID.<br> It is 10 or 16 or 17 digit alphanumeric
														Number.<br> Format should be <br>
														“AAANNNNNNN” in case 10 digits number<br>
														“AA/NN/NNN/NNNNNN” in case other than 10 digits number<br>
														Enter the Exact number as mentioned on the Voter ID slip
														including space & special characters (*, /, -, \ ).<br>
														A=Alphabet<br> N=Numerical
												</span> </a>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="nrega_check" id="nrega_check"
													data-ng-model="form_data[selectedTab].nrega.check"
													ng-change="freezeDocument('nrega',selectedTab)" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>NREGA Card:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="nrega" id="nrega"
													data-ng-disabled="!form_data[selectedTab].nrega.check"
													data-ng-model="form_data[selectedTab].nrega.unique_no"
													class="input-field"
													data-ng-blur="validateNrega(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[(form_data[selectedTab].errorRequired.nrega.unique_no || invalidFields[selectedTab].nrega) && form_data[selectedTab].nrega.check]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].errorRequired.nrega.unique_no && form_data[selectedTab].nrega.check">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].nrega.check && invalidFields[selectedTab].nrega">{{invalidFields[selectedTab].nrega}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="nrega_upload" id="nrega_upload"
														data-ng-model="form_data[selectedTab].nrega.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="min-width: 150px; margin-left: -5px;">Information</span></a>
												</a>
											</div>
										</div>




										<%-- Electricity Interface is being defined --%>

<div class="in-line-block col-xs-12 no-padding  KYC-document">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="electricity_check"
													id="electricity_check"
													data-ng-model="form_data[selectedTab].electricity.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Electricity:</label>
											</div>
											<div class="col-xs-3">
												<label class="select-label no-padding col-xs-6"
													style="margin-top: 0px; width: 100%;">
													 <select
													id="board" name="board" class="input-field"
													ng-disabled="!form_data[selectedTab].electricity.check"
													ng-change="changeElectricityBoard()"
													data-ng-model="form_data[selectedTab].electricity.board">
														<option value="">Select</option>
														<option ng-repeat="board in boards track by $index"
															ng-selected="board.electBoardId === electricity.board"
															id="{{board.electricityBoardName}}"
															value="{{board.electBoardId}}">
															{{board.electricityBoardName}}</option>
												</select>
												</label>
											</div>


										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document">
											<div class="col-xs-1 no-padding"></div>
											<div class="col-xs-2 no-padding ">
												<label>Consumer No. :</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="consumer_number" maxlength="25"
													ng-disabled="!form_data[selectedTab].electricity.check"
													alpha-numeric-with-slash-and-hyphen
													data-ng-model="form_data[selectedTab].electricity.unique_no"
													class="input-field"
													data-ng-blur="validateElecConsumer(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].electricity.check && form_data[selectedTab].invalid.electricity]"
													required /> <span class="error-message"
													ng-show="form_data[selectedTab].electricity.invalid && form_data[selectedTab].electricity.check">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].electricity.check && invalidFields[selectedTab].electricity">{{invalidFields[selectedTab].electricity}}</span>
											</div>

											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="electricity_upload"
														id="electricity_upload"
														data-ng-model="form_data[selectedTab].electricity.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span class="font-regular size-9"
													style="min-width: 150px; margin-left: -5px;">
														Information </span> </a>
											</div>
										</div>
										<div class="in-line-block col-xs-12 no-padding KYC-document">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="telephone_check"
													id="telephone_check"
													data-ng-model="form_data[selectedTab].telephone.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>Telephone Bill:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="telephone" id="telephone"
													maxlength="15"
													data-ng-disabled="!form_data[selectedTab].telephone.check"
													only-numeric-with-hyphen
													data-ng-model="form_data[selectedTab].telephone.unique_no"
													class="input-field" onlyNumericWithHyphen
													data-ng-blur="validateTelephone(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].telephone.check && (form_data[selectedTab].errorRequired.telephone.unique_no || invalidFields[selectedTab].telephone)]"
													required />
												<!-- form_data[selectedTab].telephone.check && form_data[selectedTab].errorRequired.telephone.unique_no -->
												<span class="error-message"
													ng-show="form_data[selectedTab].telephone.invalid && form_data[selectedTab].telephone.check">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].telephone.check && invalidFields[selectedTab].telephone">{{invalidFields[selectedTab].telephone}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="telephone_upload"
														id="telephone_upload"
														data-ng-model="form_data[selectedTab].telephone.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span class="font-regular size-9"
													style="min-width: 150px; margin-left: -5px;">Enter
														the Phone Number <br> followed by STD Number with
														Hyphen in between STD Code <br> and Phone Number,
														e.g. : 011-67676767<br>
												</span> </a>
											</div>
										</div>
										
										
										<div class="in-line-block col-xs-12 no-padding KYC-document"
										 data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="lpg_check"
													id="lpg_check"
													data-ng-model="form_data[selectedTab].lpg.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>LPG ID:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="lpg" id="lpg"
													maxlength="17"
													data-ng-disabled="!form_data[selectedTab].lpg.check"
													data-ng-model="form_data[selectedTab].lpg.unique_no"
													class="input-field"
													data-ng-blur="validateLPG(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].lpg.check && (form_data[selectedTab].errorRequired.lpg.unique_no || invalidFields[selectedTab].lpg)]"
													only-numeric required />
												<span class="error-message"
													ng-show="form_data[selectedTab].lpg.invalid && form_data[selectedTab].lpg.check">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].lpg.check && invalidFields[selectedTab].lpg">{{invalidFields[selectedTab].lpg}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="lpg_upload"
														id="telephone_upload"
														data-ng-model="form_data[selectedTab].lpg.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span class="font-regular size-9"
													style="min-width: 150px; margin-left: -5px;">Enter
														the LPG ID <br>
												</span> </a>
											</div>
										</div>
										
										
										
										 <div class="in-line-block col-xs-12 no-padding KYC-document"
										 data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="esic_check"
													id="esic_check"
													data-ng-model="form_data[selectedTab].esic.check" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>ESIC No:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="esic" id="esic"
													maxlength="10"
													data-ng-disabled="!form_data[selectedTab].esic.check"
													data-ng-model="form_data[selectedTab].esic.unique_no"
													class="input-field" 
													data-ng-blur="validateESIC(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].esic.check && (form_data[selectedTab].errorRequired.esic.unique_no || invalidFields[selectedTab].esic)]"
													only-numeric required />
												<span class="error-message"
													ng-show="form_data[selectedTab].esic.invalid && form_data[selectedTab].esic.check">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].esic.check && invalidFields[selectedTab].esic">{{invalidFields[selectedTab].esic}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="esic_upload"
														id="esic_upload"
														data-ng-model="form_data[selectedTab].esic.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span class="font-regular size-9"
													style="min-width: 150px; margin-left: -5px;">Enter
														the ESIC Number <br>
												</span> </a>
											</div>
										</div>
										
										
										
										
										<div class="col-xs-12 no-padding" style="display: inline-flex">
											<label style="min-width: 13%; color: #337ab7">Income
												Authentication </label>
											<hr style="margin-top: 14px;">
										</div>
										<div class="in-line-block col-xs-12 no-padding  KYC-document">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="itr_check" id="itr_check"
													data-ng-model="form_data[selectedTab].itr.check"
													ng-change="isPanSelected(selectedTab)" />
											</div>
											<div class="col-xs-2 no-padding">
												<label>ITR Validation</label>
											</div>
										</div>
										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											ng-hide="!form_data[selectedTab].itr.check">
											<div class="col-xs-12 no-padding"
												data-ng-repeat="itr in form_data[selectedTab].itr.itr_fields track by $index">
												<div class="col-xs-3 no-padding">
													<div class="col-xs-6">
														<label>Assessment Year: </label>
													</div>
													<div class="col-xs-6">
														<select id="assesment_year" name="assesment_year"
															data-ng-model="itr.assesment_year"
															class="input-field selectboxit-arrow-container"
															ng-options="opt for opt in assessment_years"
															data-ng-blur="itr.assesment_year_touched =true"
															ng-class="{true:'invalid-field',false:''}[(itr.assesment_year_touched && !itr.assesment_year)]"
															required>
															<option value="">Select</option>
														</select> <span class="error-message"
															ng-show="itr.assesment_year_touched && !itr.assesment_year">Invalid
															input</span>
													</div>
												</div>
												<div class="col-xs-3 no-padding">
													<div class="col-xs-4 no-padding">
														<label>Date of filling: </label>
													</div>
													<div class="col-xs-8 no-padding">
														<div class="col-md-12">
															<p class="input-group datepicker-block"
																data-ng-hide="true">
															<div class="col-xs-8 no-padding date-picker">
																<span data-ng-hide="true"
																	class="form-control datepicker-text"
																	data-ng-model="dateOfFilling"
																	data-ng-change="selectDateOfFilling(itr,dateOfFilling);validateAcknowledgementNumber(itr,selectedTab)"
																	uib-datepicker-popup
																	is-open="itr.date_of_filling_datepicker.opened"
																	min-date="minDate" max-date="maxDate"
																	datepicker-options="dateOptions" ng-required="true"
																	close-text="Close"></span> <input type="text"
																	data-ng-focus="itr.date_of_filling_touched =true"
																	name="date_of_filling"
																	data-ng-model="itr.date_of_filling" class="input-field"
																	placeholder="DD/MM/YYYY" mask="31/12/9999"
																	clean="false" mask-restrict="accept"
																	mask-validate="false"
																	ng-class="{true:'invalid-field',false:''}[(itr.date_of_filling_touched) && (itr.errorRequired.date_of_filling.unique_no || invalidFields[selectedTab].date_of_filling)]"
																	data-ng-blur="validateAcknowledgementNumber(itr,selectedTab)"
																	only-numeric-with-slash required /> <span
																	class="error-message"
																	ng-show="itr.errorRequired.date_of_filling.unique_no && itr.date_of_filling_touched">Invalid
																	input</span>
															</div>
															<div class="col-xs-4 no-padding text-center">
																<span class="input-group-btn">
																	<button type="button" class="btn btn-default"
																		ng-click="showDateOfFillingCalendar(itr)"
																		style="padding: 0px; border: none;"
																		title="Date Of Filling">
																		<i class="fa fa-lg fa-calendar"></i>
																	</button>
																</span>
															</div>
															</p>
															<div class="no-border col-xs-9 no-padding">
																<span class="error-message first no-border"
																	ng-show="itr.errorRequired.invalid_date && !itr.errorRequired.date_of_filling.unique_no">Invalid
																	Date</span>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-4 no-padding">
													<div class="col-xs-6 no-padding">
														<label>Acknowledgement Number:</label>
													</div>
													<div class="col-xs-6">
														<input type="text"
															data-ng-focus="itr.acknowledgement_number_touched =true"
															name="acknowledgement_number" id="acknowledgement_number"
															data-ng-model="itr.acknowledgement_number"
															class="input-field" maxlength="15"
															data-ng-blur="validateAcknowledgementNumber(itr,selectedTab)"
															ng-class="{true:'invalid-field',false:''}[(itr.errorRequired.acknowledgement_number.unique_no || invalidFields[selectedTab].acknowledgement_number) && (itr.acknowledgement_number_touched)]"
															only-numeric required /> <span class="error-message"
															ng-show="form_invalid && itr.acknowledgement_number_touched">Invalid
															input</span> <span class="error-message" ng-show="itr.invalid">{{itr.invalid}}</span>
													</div>
												</div>
												<div class="col-xs-1 no-padding">
													<button class="file-upload"
														data-ng-click="doNotChooseFile()">
														<input type="text" name="itr_upload" id="itr_upload"
															data-ng-model="itr.file" class="file-input"
															class="input-field"> Choose File
													</button>
												</div>
												<div class="col-xs-1"
													data-ng-hide="form_data[selectedTab].itr.itr_fields.length==1 || userRole == 'View' || request_status == 'in_queue'">
													<span> <%--<img src="<c:url value='/images/remove_button.jpg'/>" width="20px" height="20px" data-ng-click="removeItr(selectedTab,$index)">--%>
														<i class="fa fa-times" aria-hidden="true"
														data-ng-click="removeItr(selectedTab,$index)"></i>
													</span>
												</div>
											</div>
										</div>
										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											ng-show="form_data[selectedTab].itr.check">
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													ng-click="addMoreItrRow(selectedTab)"
													ng-disabled="form_data[selectedTab].itr.itr_fields.length == assessment_years.length">
													<input type="button" name="itr_add" id="itr_add"
														data-ng-model="form_data[selectedTab].itr.file"
														class="file-input"> Add more
												</button>
											</div>
										</div>

										<div class="in-line-block col-xs-12 no-padding  KYC-document"
											data-ng-hide="form_type[selectedTab] == 'others' || form_type[selectedTab] == 'non-individuals'">
											<div class="col-xs-1 no-padding">
												<input type="checkbox" name="uan_check" id="uan_check"
													data-ng-model="form_data[selectedTab].uan.check"
													 >
											</div>
											<div class="col-xs-2 no-padding">
												<label>EPFO UAN:</label>
											</div>
											<div class="col-xs-3">
												<input type="text" name="uan" id="uan" maxlength="12"
													data-ng-disabled="!form_data[selectedTab].uan.check"
													data-ng-model="form_data[selectedTab].uan.unique_no"
													class="input-field" data-ng-blur="validateUAN(selectedTab)"
													ng-class="{true:'invalid-field',false:''}[form_data[selectedTab].uan.check && (form_data[selectedTab].errorRequired.uan.unique_no || invalidFields[selectedTab].uan)]"
													no-special-char only-numeric required /> <span
													class="error-message"
													ng-show="form_data[selectedTab].uan.check && form_data[selectedTab].uan.invalid">Invalid
													input</span> <span class="error-message"
													ng-show="form_data[selectedTab].uan.check && invalidFields[selectedTab].uan">{{invalidFields[selectedTab].uan}}</span>
											</div>
											<div class="col-xs-1 no-padding">
												<button class="file-upload"
													data-ng-click="doNotChooseFile()">
													<input type="text" name="uan_upload"
														id="uan_upload"
														data-ng-model="form_data[selectedTab].uan.file"
														class="file-input"> Choose File
												</button>
											</div>
											<div class="col-xs-1 text-center" style="padding-top: 5px">
												<a href="" class="tooltips"><img
													src="<c:url value='/images/help-icon.png'/>" class="help">
													<span style="text-align: left"> Enter the UAN Number
														mentioned by the Employee.<br> It is 12 digit
														numerical number.<br> 12th digit is the check digit.
												</span> </a>
											</div>
										</div>
									</div>
									<div
										class="in-line-block col-xs-12 no-padding new-request-form-buttons"
										style="padding-bottom: 0px;"
										data-ng-hide="userRole == 'View' || request_status == 'in_queue'">
										<div class="col-xs-3 no-padding"></div>
										<div class="col-xs-2 ">
											<button type="button" id="related"
												class="btn btn-group-justified button submit-button"
												data-ng-click="addTab()">Add Related Entity</button>
										</div>
										<div class="col-xs-2 ">
											<button type="button" id="submit"
												class="btn btn-group-justified button submit-button"
												data-ng-click="submitForm(selectedTab)">Submit</button>
										</div>
										<div class="col-xs-2 ">
											<button type="button" id="save"
												class="btn btn-group-justified button submit-button"
												data-ng-click="saveForm()">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<script type="text/ng-template" id="invalid_input_msgbox.html">
    <div class="modal-body text-center">
        <a class="close" data-ng-click="no()">×</a>
        <h3 class="modal-title">Alert</h3>
        Invalid inputs in the form. Are you sure you want to submit?
    </div>
    <div class="modal-footer">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        id="yesSubmit" data-ng-click="yes()">Yes
                </button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        id="noSubmit" data-ng-click="no()">No
                </button>
            </div>
        </div>
    </div>
</script>

	<script type="text/ng-template" id="delete_request.html">
    <div class="modal-body text-center">
        <a class="close" data-ng-click="no()">×</a>
        <h3 class="modal-title">Alert</h3>
        Are you sure you want to delete this request?
    </div>
    <div class="modal-footer">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="yes()">Yes
                </button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="no()">No
                </button>
            </div>
        </div>
    </div>
</script>

	<script type="text/ng-template" id="deleteTab.html">
    <div class="modal-body text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">Alert</h3>
        Are you sure want to delete the entity tab?
    </div>
    <div class="modal-footer">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="yes()">Yes
                </button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="close()">No
                </button>
            </div>
        </div>
    </div>
</script>

	<script type="text/ng-template" id="message.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{message_heading}}</h3>
    </div>
    <div class="modal-body text-center">
        {{message}}
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-block button view-historical-button submit-button" data-ng-click="close()">Ok</button>
    </div>
</script>

	<script type="text/ng-template" id="captcha.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">Please enter captcha</h3>
    </div>
    <div class="modal-body text-center">
        <div class="row">
            <div class="col-xs-12">
                <div class="row" data-ng-repeat="captcha in captchaDetails" style="padding: 10px;">
                    <img class="col-xs-4" src="data:image/png;base64,{{captcha.captcha}}">
                    <input class="col-xs-4" type="text" data-ng-model="captcha.captcha_text">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer text-center">
        <button class="btn button view-historical-button submit-button" data-ng-click="setCaptcha()">Ok</button>
    </div>
</script>
	<script type="text/ng-template" id="restrictedMessage.html">
    <div class="modal-header text-center">
        <h3 class="modal-title">{{message_heading}}</h3>
    </div>
    <div class="modal-body text-center">
        {{message}}
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-block button view-historical-button submit-button" id="okButtonForNewRequest"
                data-ng-click="close()">Ok
        </button>
    </div>
</script>
</body>
</html>
