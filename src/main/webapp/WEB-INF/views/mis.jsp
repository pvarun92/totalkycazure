<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/misController.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/FileSaver.js'/>"></script>
    <style>
        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }

        .wrap-value {
            width: 95px;
        }
    </style>
</head>
<body data-ng-controller="misController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>MIS</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div>
        <div class="col-xs-12">
            <div class="col-lg-12 mis-filter-block">
                <div style="margin: auto;width: 70%;">
                    <div class="col-xs-12" style="margin-bottom: 10px;">
                        <div style="padding-right: 0px">
                            <div class="col-xs-12 no-padding group-3-elem">
                                <input type="text" style="margin: 0px"
                                       class="input-field first col-xs-4 no-padding media-width"
                                       data-ng-model="filter.entity_name" placeholder="Search By Entity Name">
                                <label class="select-label col-xs-4 no-padding media-width" style="margin: 0px">
                                    <select name="filter_entity_type" id="filter_entity_type" class="input-field second"
                                            data-ng-model="filter.entity_type">
                                        <option value="">Select by entity</option>
                                        <option value="Individual">Individual</option>
                                        <option value="Proprietory Concern">Proprietory Concern</option>
                                        <option value="Hindu Undivided Family (HUF)">Hindu Undivided Family (HUF)
                                        </option>
                                        <option value="Partnership">Partnership</option>
                                        <option value="Limited Liability Partnership (LLP)">Limited Liability
                                            Partnership (LLP)
                                        </option>
                                        <option value="Company">Company</option>
                                        <option value="Trust">Trust</option>
                                        <option value="Association of Persons (AOP)">Association of Persons (AOP)
                                        </option>
                                        <option value="Body of Individuals (BOI)">Body of Individuals (BOI)</option>
                                        <option value="Society">Society</option>
                                    </select>
                                </label>
                                <label class="select-label col-xs-4 no-padding media-width" style="margin: 0px">
                                    <select name="filter_branch" id="filter_branch" class="input-field third"
                                            data-ng-model="filter.branchId">
                                        <option value="">Select by Branch</option>
                                        <option data-ng-repeat="branch in branches" value="{{branch.id}}">
                                            {{branch.branchName}}
                                        </option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-bottom: 10px;">
                        <div class="" style="margin-bottom: 10px;">
                            <div class="col-xs-7 no-padding media-width">
                                <div class="col-xs-6 no-padding">
                                    <div class="row col-xs-12 no-padding">
                                        <span class="normal-text col-xs-12 label-height">From Date</span>
                                    </div>
                                    <div class="row col-xs-12 no-padding">
                                        <p class="input-group datepicker-block" data-ng-hide="true">
                                        <div class="col-xs-10">
                                            <span data-ng-hide="true" class="form-control datepicker-text"
                                                  data-ng-model="filter.filter_from" data-ng-change="selectFromDate()"
                                                  uib-datepicker-popup is-open="from_datepicker.opened"
                                                  min-date="minDate" max-date="maxDate" datepicker-options="dateOptions"
                                                  date-disabled="disabled(date, mode)" ng-required="true"
                                                  close-text="Close"></span>
                                            <div class="group-3-text-boxes">
                                                <input type="text" name="filter_from" data-ng-model="filter.filter_from"
                                                       class="input-field" placeholder="DD/MM/YYYY"
                                                       data-ng-blur="validateFromDate()"
                                                       mask="31/12/9999" clean="false" mask-restrict="accept"
                                                       mask-validate="false" only-numeric-with-slash
                                                       ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"
                                                       ng-disabled=true required/>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showFromCalendar()"
                                                        style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        </p>
                                    </div>
                                    <div class="col-xs-12 no-padding"
                                         data-ng-show="invalid_filter.filter_from || invalid_filter.filter_from_required">
                                        <span class="error-message" data-ng-show="invalid_filter.filter_from">
                                            From date cannot be the future date.
                                        </span>
                                        <span class="error-message" data-ng-show="invalid_filter.filter_from_required">
                                From date is required.
                            </span>
                                    </div>
                                </div>
                                <div class="col-xs-6 no-padding">
                                    <div>
                                        <div class="row col-xs-12 no-padding">
                                            <span class="normal-text col-xs-12 label-height">To Date</span>
                                        </div>
                                        <div class="row col-xs-12 no-padding">
                                            <p class="input-group datepicker-block" data-ng-hide="true">
                                            <div class="col-xs-10">
                                                <span data-ng-hide="true" class="form-control datepicker-text"
                                                      data-ng-model="filter.filter_to" data-ng-change="selectToDate()"
                                                      uib-datepicker-popup is-open="to_datepicker.opened"
                                                      min-date="minDate" max-date="maxDate"
                                                      datepicker-options="dateOptions"
                                                      date-disabled="disabled(date, mode)" ng-required="true"
                                                      close-text="Close"></span>
                                                <div class="group-3-text-boxes">
                                                    <input type="text" name="filter_to" data-ng-model="filter.filter_to"
                                                           class="input-field" placeholder="DD/MM/YYYY"
                                                           data-ng-blur="validateToDate()"
                                                           mask="31/12/9999" clean="false" mask-restrict="accept"
                                                           mask-validate="false" only-numeric-with-slash
                                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"
                                                           ng-disabled=true required/>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showToCalendar()" style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                                            </div>
                                            </p>
                                        </div>
                                        <div class="row col-xs-12"
                                             data-ng-show="invalid_filter.filter_to || invalid_filter.filter_to_less_than_from || invalid_filter.filter_to_required">
                                            <span class="error-message" data-ng-show="invalid_filter.filter_to">
                                                    To date cannot be greater than today's date
                                                </span>
                                            <span class="error-message"
                                                  data-ng-show="invalid_filter.filter_to_less_than_from">
                                                    To date cannot be less than from date
                                                </span>
                                            <span class="error-message"
                                                  data-ng-show="invalid_filter.filter_to_required">
                                    To date is required
                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 no-padding media-width space-block">
                                <div class="col-xs-6 media-width no-padding block1">
                                    <div class="row margin-0">
                                        <span class="normal-text label-height no-padding">Validity</span>
                                    </div>
                                    <div class="row margin-0">
                                        <label class="select-label" style="margin-top: 0px">
                                            <select name="filter_validity" id="filter_validity" class="input-field"
                                                    data-ng-model="filter.validity">
                                                <option value="">Select</option>
                                                <option value="valid">Valid</option>
                                                <option value="partial_valid">Flagged</option>
                                                <option value="invalid">Invalid</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-xs-6 media-width no-padding block2">
                                    <div class="row margin-0">
                                        <span class="normal-text label-height no-padding">Report Status</span>
                                    </div>
                                    <div class="row margin-0">
                                        <label class="select-label" style="margin-top: 0px">
                                            <select name="filter_report_status" id="filter_report_status"
                                                    class="input-field" data-ng-model="filter.report_status">
                                                <option value="">Select</option>
                                                <option value="submitted">Generated</option>
                                                <option value="in_draft">Pending</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-bottom: 10px;">
                        <div class="no-padding submit-button-div" style="width: 30%; margin: auto">
                            <button class="btn btn-group-justified btn-block button submit-button"
                                    data-ng-click="getFilteredData(filter)">
                                Search
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 no-padding" style="background-color: #ffffff;padding-top: 10px;">
                <div class="col-xs-12" style="display: inline-flex">
                    <label class="color-blue size-10 font-bold" style="min-width: 110px;">Summary Count</label>
                    <hr style="margin-top: 14px;">
                </div>
                <div class="col-sm-12 text-center">
                    <Table class="table activity text-left dashboard-table">
                        <Tr class="gray-header">
                            <th>
                                Total Requests
                            </th>
                            <th>
                                Generated
                            </th>
                            <th>
                                Pending
                            </th>
                            <th>
                                In Draft
                            </th>
                            <th>
                                KYC Valid
                            </th>
                            <th>
                                ITR Valid
                            </th>
                            <th>
                                KYC Invalid
                            </th>
                             <th>
                                KYC Partial Valid
                            </th>
                            <th>
                                ITR Invalid
                            </th>
                        </Tr>
                        <Tr>
                            <td>{{summary_count_data.total_request}}</td>
                            <td> {{summary_count_data.generated}}</td>
                            <td> {{summary_count_data.pending}}</td>
                            <td> {{summary_count_data.draft}}</td>
                            <td> {{summary_count_data.valid}}</td>
                            <td> {{summary_count_data.itrValid}}</td>
                            <td> {{summary_count_data.invalid}}</td>
                            <td> {{summary_count_data.partial_valid}}</td>
                            <td> {{summary_count_data.itrInvalid}}</td>
                        </Tr>
                    </Table>
                </div>
                <div class="col-xs-12" style="display: inline-flex">
                    <label class="color-blue size-10 font-bold" style="min-width: 95px;">Listing Details</label>
                    <hr style="margin-top: 14px;">
                </div>
                <div class="col-sm-12 text-center" id="exportData">
                    <Table class="table activity text-left dashboard-table history-table" style="width: 100%;">
                        <Tr class="gray-header">
                            <th>
                                <a href="" ng-click="orderByField='report_id'; reverseSort = !reverseSort"
                                   data-ng-init="orderByField='report_id';">
                                    Report ID
                                    <span ng-show="orderByField == 'report_id'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='report_date'; reverseSort = !reverseSort">
                                    Report Date
                                    <span ng-show="orderByField == 'report_date'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='entity_name'; reverseSort = !reverseSort">
                                    Entity Name
                                    <span ng-show="orderByField == 'entity_name'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='entity_type'; reverseSort = !reverseSort">
                                    Entity Type
                                    <span ng-show="orderByField == 'entity_type'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='branch'; reverseSort = !reverseSort">
                                    Branch
                                    <span ng-show="orderByField == 'branch'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='no_of_parties'; reverseSort = !reverseSort">
                                    No of Parties
                                    <span ng-show="orderByField == 'no_of_parties'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='no_of_documents'; reverseSort = !reverseSort">
                                    No of Documents
                                    <span ng-show="orderByField == 'no_of_documents'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='report_status'; reverseSort = !reverseSort">
                                    Report Status
                                    <span ng-show="orderByField == 'report_status'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='validity'; reverseSort = !reverseSort">
                                    KYC Validity
                                    <span ng-show="orderByField == 'validity'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                            <th>
                                <a href="" ng-click="orderByField='itrValidity'; reverseSort = !reverseSort">
                                    ITR Validity
                                    <span ng-show="orderByField == 'itrValidity'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                                </a>
                            </th>
                        </Tr>
                        <Tr dir-paginate="data in filtered_searched_data | orderBy:orderByField:reverseSort | itemsPerPage: pageSize"
                            current-page="currentPage">
                            <td data-ng-show="data.report_status == 'Generated'"><span class="wrap-value"><a
                                    href="simple_sidebar_menu?navigate_to=kyc_validation_report?id={{data.report_id}}"
                                    target="_parent"> {{data.report_id}} </a></span></td>
                            <td data-ng-show="data.report_status != 'Generated'"><a
                                    href="simple_sidebar_menu?navigate_to=new_request?id={{data.report_id}}"
                                    target="_parent"> <span data-ng-show="data.report_status == 'Pending'">{{data.report_id}}</span><span
                                    data-ng-show="data.report_status == 'In draft'">(...)</span> </a></td>
                            <td><span class="wrap-value"> {{getConvertedDate(data.report_date)}} </span></td>
                            <td><span class="wrap-value"> {{data.entity_name}}</span></td>
                            <td><span class="wrap-value"> {{data.entity_type}}</span></td>
                            <td><span class="wrap-value"> {{data.branch}} </span></td>
                            <td><span class="wrap-value"> {{data.no_of_parties}} </span></td>
                            <td><span class="wrap-value"> {{data.no_of_documents}} </span></td>
                            <td><span class="wrap-value"> {{data.report_status}} </span></td>

                            <td>
                                <img data-ng-show="data.validity == 'valid'"
                                     src="<c:url value='/images/valid_entity.png'/>" class="valid status-image-size">
                                <img data-ng-show="data.validity == 'invalid'"
                                     src="<c:url value='/images/invalid_entity.png'/>"
                                     class="invalid status-image-size">
                                <img data-ng-show="data.validity == 'partial_valid'"
                                     src="<c:url value='/images/partial_valid_entity.png'/>"
                                     class="invalid status-image-size">
                            </td>
                            <td>
                                <img data-ng-show="data.itrValidity == 'valid'"
                                     src="<c:url value='/images/valid_entity.png'/>" class="valid status-image-size">
                                <img data-ng-show="data.itrValidity == 'invalid'"
                                     src="<c:url value='/images/invalid_entity.png'/>"
                                     class="invalid status-image-size">
                            </td>

                            <%--<td><span class="wrap-value"> {{data.validity}} </span></td>--%>
                            <%--<td><span class="wrap-value"> {{data.itrValidity}} </span></td>--%>
                        </Tr>
                    </Table>
                </div>
            </div>
            <div class="col-xs-12 no-padding" style="margin-top: 10px; padding-bottom: 20px">
                <div class="col-xs-2">
                    <div class="no-padding submit-button-div">
                        <button class="btn btn-group-justified btn-block button-button submit-button"
                                data-ng-click="print()">
                            Print
                        </button>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="no-padding submit-button-div">
                        <button class="btn btn-group-justified btn-block button submit-button background-green"
                                data-ng-click="exportToXLS()">
                            Export
                        </button>
                    </div>
                </div>
                <div class="col-xs-8 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>