<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/newLocationNewController.js'/>"></script>

    <style>
        .close {
            line-height: 0;
            position: relative;
            left: 10px;
        }

        .tab-label {
            display: block !important;
            width: 120px !important;
            min-width: 100px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            float: left;
        }
    </style>
</head>
<body data-ng-controller="newLocationNewController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block margin-0 no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Admin Panel</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container" style="padding: 1%;">
    <form name="newCustomerForm" novalidate class="css-form">
        <div class="col-xs-12 no-padding" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 200px">New Branch Request</label>
            <hr style="margin-top: 14px;">
        </div>
        <div class="new-request-container">
            <div class="col-xs-12" style="padding-top: 15px">
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Location Name </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="locationName" id="locationName"
                                   data-ng-model="newLocation.locationName" class="input-field"/>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Branches </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="col-xs-6 no-padding select-label" style="margin: 0px">
                            <div ng-dropdown-multiselect="" options="branch_data" selected-model="newLocation.branches"
                                 checkboxes="true" extra-settings="multipleDropDownSetting"></div>
                        </label>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Location </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="col-xs-6 no-padding select-label" style="margin: 0px">
                            <select id="locations" name="locations" data-ng-model="newUser.locations"
                                    class="input-field selectboxit-arrow-container"
                            >
                                <option value="">Select</option>
                                <option data-ng-repeat="location in locations" value="{{location.id}}">
                                    {{location.locationOrZone}}
                                </option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="in-line-block col-xs-10 no-padding new-request-form-buttons margin-bottom"
                 style="padding-bottom: 0px;">
                <div class="col-xs-2">
                    <button type="button" id="submit" class="btn btn-group-justified button submit-button"
                            style="background-color: #0e76bc" data-ng-click="submitForm(selectedTab)">Save
                    </button>
                </div>
                <div class="col-xs-2 ">
                    <button type="button" id="save" class="btn btn-group-justified button submit-button"
                            data-ng-click="saveForm()">Submit
                    </button>
                </div>
            </div>
    </form>
</div>
</body>
</html>
