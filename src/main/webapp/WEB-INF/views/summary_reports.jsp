<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>

    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/summaryReportsController.js'/>"></script>
</head>
<body ng-cloak>
<div class="page-container">
    <div class="container">
        <ul class="nav nav-tabs col-xs-offset-1">
            <li class="active"><a data-toggle="tab" href="#check_count">Check Count</a></li>
            <li><a data-toggle="tab" href="#status">Status</a></li>
            <li><a data-toggle="tab" href="#document_type">Document Type</a></li>
            <li><a data-toggle="tab" href="#entity_type">Entity Type</a></li>
        </ul>

        <div class="tab-content">
            <div id="check_count" class="tab-pane fade in active" data-ng-controller="checkCountController">
                <div class="col-sm-12 text-center no-padding panel">
                    <Table class="summary-report activity text-left">
                        <Tr>
                            <th>Period</th>
                            <th>Total Requests</th>
                            <th>Completed</th>
                            <th>Pending</th>
                        </Tr>
                        <Tr data-ng-repeat="data in check_count_data | orderBy:orderByField:reverseSort">
                            <td>{{data.period}}</td>
                            <td>{{data.total_requests}}</td>
                            <td> {{data.completed}}</td>
                            <td> {{data.pending}}</td>
                        </Tr>
                    </Table>
                </div>
            </div>
            <div id="status" class="tab-pane fade" data-ng-controller="statusController">
                <div class="col-sm-12 text-center no-padding panel">
                    <Table class="summary-report activity text-left">
                        <Tr>
                            <th>Period</th>
                            <th>Total Requests</th>
                            <th>Valid Inputs</th>
                            <th>Invalid Inputs</th>
                        </Tr>
                        <Tr data-ng-repeat="data in status_data | orderBy:orderByField:reverseSort">
                            <td>{{data.period}}</td>
                            <td>{{data.total_requests}}</td>
                            <td> {{data.valid_inputs}}</td>
                            <td> {{data.invalid_inputs}}</td>
                        </Tr>
                    </Table>
                </div>
            </div>
            <div id="document_type" class="tab-pane fade" data-ng-controller="documentTypeController">
                <div class="col-sm-12 text-center no-padding panel">
                    <Table class="summary-report activity text-left">
                        <Tr>
                            <th>Period</th>
                            <th>PAN</th>
                            <th>Aadhar</th>
                            <th>Drivers' License</th>
                            <th>Passport</th>
                            <th>Voters' ID</th>
                            <th>Electricity Bill</th>
                            <th>Telephone Bill</th>
                            <th>Total</th>
                        </Tr>
                        <Tr data-ng-repeat="data in document_type_data | orderBy:orderByField:reverseSort">
                            <td>{{data.period}}</td>
                            <td>{{data.pan}}</td>
                            <td> {{data.aadhar}}</td>
                            <td> {{data.dl}}</td>
                            <td>{{data.passport}}</td>
                            <td>{{data.voters_id}}</td>
                            <td>{{data.electricityBill}}</td>
                            <td>{{data.telephone_bill}}</td>
                            <td>{{data.total}}</td>

                        </Tr>
                    </Table>
                </div>
            </div>
            <div id="entity_type" class="tab-pane fade" data-ng-controller="entityTypeController">
                <div class="col-sm-12 text-center no-padding panel">
                    <Table class="summary-report activity text-left">
                        <Tr>
                            <th>Period</th>
                            <th>Individuals</th>
                            <th>Proprietorship</th>
                            <th>Partnerships</th>
                            <th>HUF</th>
                            <th>Companies</th>
                            <th>LLPs</th>
                            <th>Others</th>
                            <th>Total</th>
                        </Tr>
                        <Tr data-ng-repeat="data in entity_type_data | orderBy:orderByField:reverseSort">
                            <td>{{data.period}}</td>
                            <td>{{data.individuals}}</td>
                            <td> {{data.proprietorship}}</td>
                            <td> {{data.partnerships}}</td>
                            <td>{{data.huf}}</td>
                            <td>{{data.companies}}</td>
                            <td> {{data.llps}}</td>
                            <td> {{data.others}}</td>
                            <td>{{data.total}}</td>
                        </Tr>
                    </Table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
