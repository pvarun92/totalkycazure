<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Management</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>">
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/admin-project_style.css'/>">--%>
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/bootstrap.template.min.css'/>">--%>
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/sb-admin.css'/>">--%>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="<c:url value='/js/static/jquery.js'/>"></script>
    <script src="<c:url value='/js/static/jquery-1.9.1.min.js'/>"></script>
    <script src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script src="<c:url value='/js/app.js'/>"></script>
    <script src="<c:url value='/js/controllers/userManagementControllerNew.js' />"></script>

    <style>
        .active {
            background-color: #666666 !important;
        }

        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }
    </style>
</head>
<body data-ng-controller="userManagementControllerNew" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>User Management</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background" data-ng-show="showUserManagement">
        <div class="col-xs-12 no-padding">
            <div class="col-xs-6 no-padding media-width" style="padding-right: 0px">
                <div class="no-padding add-new-button">
                    <div class="col-xs-5 filter-search">
                        <span>
                            <div class="form-group input-group">
                                <input type="text" class="form-control search" data-ng-model="searchUser"
                                       placeholder="Search By Name">
                                <span class="input-group-btn no-radius">
                                    <button class="btn btn-default" style="padding-top: 5px;background: #f4f7fa"
                                            type="submit" title="Search Box"><i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </span>
                    </div>
                    <div class="col-xs-3 no-padding" style="padding: 5px">
                        <span class="heading-text" style="color: #0e76bc;font-weight: bold;">Total Users : </span>
                        {{users.length}}
                    </div>
                    <div class="col-xs-3 no-padding" style="padding: 5px">
                        <span class="heading-text" style="color: #0e76bc;font-weight: bold;">Active Users : </span>
                        {{activeUsers.length}}
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                </div>
            </div>
            <div class="col-xs-6 media-width" style="padding-right: 0px">
                <div class="col-xs-4 pull-right" style="padding-right: 0px">
                    <div class="row col-xs-12 no-padding add-new-button">
                        <a href="" data-ng-click="newCreateUserNew()" id="newUser"
                           class="btn btn-group-justified button submit-button" target="_parent"> Add A New User </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th>
                        <a href="" ng-click="orderByField='id'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='isActive';">
                            User Id
                            <span ng-show="orderByField == 'id'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='userName'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='customerName';">
                            User Name
                            <span ng-show="orderByField == 'userName'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='userRole'; reverseSort = !reverseSort">
                            User Access Type
                            <span ng-show="orderByField == 'userRole'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='status'; reverseSort = !reverseSort">
                            Status
                            <span ng-show="orderByField == 'status'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>Edit</th>
                    <th>
                        <a href="" ng-click="orderByField='isActive'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='isActive';">
                            Active User
                            <span ng-show="orderByField == 'isActive'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                </Tr>
                <Tr dir-paginate="user in users | orderBy:orderByField:reverseSort| filter: searchUser | itemsPerPage: pageSize"
                    current-page="currentPage">
                    <td style="width: 10%"><span class="wrap-value"><a href="" style="text-decoration:none">{{user.uniqueId}}</a></span>
                    </td>
                    <td><span class="wrap-value"><a href="simple_sidebar_menu?navigate_to=userProfileNew?id={{user.id}}"
                                                    target="_parent">  {{user.userName}}</a></span></td>
                    <td><span class="wrap-value"> {{user.userRole}}</span></td>
                    <td>
                        <span class="wrap-value">
                            <span data-ng-show="user.active">Active</span>
                            <span data-ng-show="!user.active">Inactive</span>
                        </span>
                    </td>
                    <td>
                        <a href="" data-ng-click="editUser(user)" style="text-decoration: none" title="Edit User">
                            <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                        </a>
                        <%--<a href="" data-ng-click="deleteUser(user)" style="text-decoration: none" title="Delete User">--%>
                        <%--<span class="fa-stack fa-lg"><i class="fa fa-trash-o"></i></span>--%>
                        <%--</a>--%>
                        <%--<a href="" data-ng-click="crudUserPopup('refresh',data)" style="text-decoration: none">--%>
                        <%--<span class="fa-stack fa-lg"><i class="fa fa-refresh"></i></span>--%>
                        </a>
                    </td>
                    <td style="width: 10%"><span class="wrap-value">
                        <input type="checkbox" name="isActive" id="isActive" data-ng-model="user.active"
                               data-ng-change="setUserStatus(user)" title="Activate or Deactivate">
                    </span></td>
                </Tr>
            </Table>
            <div class="col-xs-12 no-padding media-width">
                <div class="col-xs-5 media-width ">
                </div>

                <div class="col-xs-7 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form name="form" novalidate class="css-form" data-ng-hide="showUserManagement">
        <div class="col-xs-12 no-padding" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 150px">{{heading}}</label>
            <hr style="margin-top: 14px;">
        </div>
        <div class="new-request-container">
            <div class="col-xs-12" style="padding-top: 15px;padding-bottom: 10px;">
                <div class="field-block col-xs-12 margin-bottom" data-ng-show="editUserData">
                    <div class="col-xs-2 no-padding">
                        <label>User ID </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="userID" id="userID" data-ng-model="newUser.uniqueId"
                                   ng-disabled="editUserData" autofocus class="input-field"/>
                            <span class="error-message no-border"
                                  ng-show="form.userID.$error.required && (form.userID.$touched || form.$submitted)">User ID is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>User Name </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="userName" id="userName" data-ng-model="newUser.userName"
                                   ng-disabled="viewUserData" autofocus class="input-field" placeholder="User Name"
                                   ng-class="{true:'invalid-field',false:''}[form.userName.$error.required && (form.userName.$touched || form.$submitted)]"
                                   required/>
                            <span class="error-message no-border"
                                  ng-show="form.userName.$error.required && (form.userName.$touched || form.$submitted)">User Name is required</span>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Designation </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="designation" id="designation" data-ng-model="newUser.designation"
                                   ng-disabled="viewUserData" class="input-field input-field" maxlength="255"/>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Employee ID </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="employeeId" id="employeeId" data-ng-model="newUser.employeeId"
                                   ng-disabled="viewUserData" class="input-field input-field" maxlength="255"/>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Email ID </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <input type="text" name="email" id="email" data-ng-model="newUser.email"
                                   ng-disabled="viewUserData" class="input-field"
                                   ng-class="{true:'invalid-field',false:''}[(form.email.$error.required || form.email.$error.pattern || emailNotExist) && (form.email.$touched || form.$submitted)]"
                                   maxlength="255" required/>
                            <span class="error-message no-border"
                                  ng-show="form.email.$error.required && (form.email.$touched || form.$submitted) && !emailNotExist">Email is required</span>
                            <span class="error-message no-border"
                                  ng-show="form.email.$error.pattern && (form.email.$touched || form.$submitted)">Email is Invalid</span>
                            <span class="error-message no-border" ng-show="emailNotExist">{{emailNotExist}}</span>
                        </div>
                        <div class="col-xs-3 no-padding">
                            <span data-ng-show="!emailVerified">
                                <img src="<c:url value='/images/email-spinner.gif'/>" style="height: 20%;width: 20%"/>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Contact </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <input type="text" name="contactNumber" id="contactNumber"
                                   data-ng-model="newUser.contactNumber" ng-disabled="viewUserData" class="input-field"
                                   ng-class="{true:'invalid-field',false:''}[form.contactNumber.$error.required && (form.contactNumber.$touched || form.$submitted)]"
                                   required only-numeric/>
                            <span class="error-message no-border"
                                  ng-show="form.contactNumber.$error.required && (form.contactNumber.$touched || form.$submitted)">Contact Number is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Access Type </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="select-label col-xs-6 no-padding" style="margin: 0px">
                            <select id="accessType" name="accessType" data-ng-model="newUser.userRole"
                                    ng-disabled="viewUserData" class="input-field selectboxit-arrow-container"
                                    ng-class="{true:'invalid-field',false:''}[form.accessType.$error.required && (form.accessType.$touched || form.$submitted)]"
                                    required>
                                <option value="">Select</option>
                                <option data-ng-repeat="type in accessTypes" value="{{type}}">{{type}}</option>
                            </select>
                            <span class="error-message no-border"
                                  ng-show="form.accessType.$error.required && (form.accessType.$touched || form.$submitted)">Access type is required</span>
                        </label>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Location Access </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="col-xs-6 no-padding select-label" style="margin: 0px">
                            <select id="location" name="location" data-ng-model="newUser.location"
                                    ng-disabled="viewUserData" class="input-field selectboxit-arrow-container"
                                    ng-class="{true:'invalid-field',false:''}[form.location.$error.required && (form.location.$touched || form.$submitted)]"
                                    required>
                                <option value="">Select</option>
                                <option data-ng-repeat="location in locations" value="{{location.id}}">
                                    {{location.locationOrZone}}
                                </option>
                            </select>
                            <span class="error-message no-border"
                                  ng-show="form.location.$error.required && (form.location.$touched || form.$submitted)">Location is required</span>
                        </label>
                    </div>
                </div>
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Session Limit </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <input type="text" name="parallelSessionLimit" id="parallelSessionLimit"
                                   data-ng-model="newUser.parallelSessionLimit" ng-disabled="viewUserData"
                                   class="input-field"
                                   ng-class="{true:'invalid-field',false:''}[form.parallelSessionLimit.$error.required && (form.parallelSessionLimit.$touched || form.$submitted)]"
                                   required only-numeric/>
                            <span class="error-message no-border"
                                  ng-show="form.parallelSessionLimit.$error.required && (form.parallelSessionLimit.$touched || form.$submitted)">Session Limit is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Request Limit </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <input type="text" name="maxRequestLimit" id="maxRequestLimit"
                                   data-ng-model="newUser.maxRequestLimit" ng-disabled="viewUserData"
                                   class="input-field"
                                   ng-class="{true:'invalid-field',false:''}[form.maxRequestLimit.$error.required && (form.maxRequestLimit.$touched || form.$submitted)]"
                                   placeholder="User Request Limit" required only-numeric/>
                            <span class="error-message no-border"
                                  ng-show="form.maxRequestLimit.$error.required && (form.maxRequestLimit.$touched || form.$submitted)">Max Request Limit is required</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="in-line-block col-xs-12 no-padding new-request-form-buttons margin-bottom"
                 ng-disabled="viewUserData" style="padding-bottom: 0px;">
                <div class="col-xs-2">
                    <button type="button" id="submit" class="btn btn-group-justified button submit-button"
                            ng-disabled="!form.$valid || !emailVerified" style="background-color: #0e76bc"
                            data-ng-show="newUserData" data-ng-click="createUser(newUser)">Submit
                    </button>
                    <button type="button" id="save" class="btn btn-group-justified button submit-button"
                            ng-disabled="!form.$valid || !emailVerified" style="background-color: #0e76bc"
                            data-ng-show="editUserData" data-ng-click="updateUser(newUser)">Update
                    </button>
                </div>
                <div class="col-xs-2 ">
                    <button type="button" id="save" class="btn btn-group-justified button submit-button"
                            style="background-color: #0e76bc" data-ng-click="closeUserForm()">Cancel
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/ng-template" id="userStatus.html">
    <div class="modal-body text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">Alert</h3>
        {{heading}}
    </div>
    <div class="modal-footer">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="toggleActiveUser()">Yes
                </button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="close()">No
                </button>
            </div>
        </div>
    </div>
</script>

<jsp:include page="dialogs/crudUserpopup.jsp"/>
<jsp:include page="dialogs/message.jsp"/>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>