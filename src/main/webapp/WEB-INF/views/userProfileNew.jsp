<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/userProfileNewController.js'/>"></script>

</head>
<body data-ng-controller="userProfileNewController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block margin-0 no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>User Management</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container" style="padding: 1%;">
    <div class="row">
        <div class="col-xs-12" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 150px">User Profile</label>
            <hr style="margin-top: 14px;">
        </div>
    </div>

    <div class="new-request-container container">
        <section>
            <div class="col-xs-12">
                <div class="col-xs-1" style="min-width: 140px;">
                    <div class="profile-icon">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <span class="fa-character">{{profile.userName[0]}}</span>
                    </div>
                </div>
                <div class="col-xs-8 profile-info">
                    <div class="blue-bold-text">
                        {{profile.userName}}
                    </div>
                    <div>
                        User ID : {{profile.userId}}
                    </div>
                    <div>
                        Status : <span data-ng-show="profile.isActive">Active</span> <span
                            data-ng-show="!profile.isActive">Inactive</span> | User Type : {{profile.userType}}
                    </div>
                    <div>
                        Email : {{profile.email}} | Contact No. : {{profile.contactNumber}}
                    </div>
                    <div data-ng-show="profile.lastLogin">
                        Last Login : {{profile.lastLogin}} | Login Duration :
                        {{timeSince(profile.lastLogin,profile.lastLoginCreatedAt)}}
                    </div>
                </div>
                <div class="pull-right">
                    <span>
                        Activate/Deactivate User :
                        <input type="checkbox" name="isActive" id="isActive" data-ng-model="profile.active"
                               data-ng-change="toggleActiveUser(profile)" title="Activate or Deactivate">
                    </span>
                </div>
            </div>
            <hr style="margin-top: 14px;">
        </section>

        <section>
            <div class="col-xs-12 profile-subscription-block">
                Branch/Location : {{profile.locationName}}
                <hr style="margin-top: 14px;">
            </div>
        </section>

        <section>
            <div class="col-xs-12" style="display: inline-flex">
                <label class="color-blue size-10 font-bold">User Activity</label>
                <label style="font-size: 10px; padding-top: 3px; padding-left: 3px;">for the current month</label>
            </div>

            <div class="col-xs-12">

                <div class="profile-info-table">
                    <table class="table activity">
                        <tr class="blue-backgroung-header">
                            <th> Total Request</th>
                            <th> Total Generated Reports</th>
                            <th> Corresponding Entities</th>
                            <th> Total Pending</th>
                        </tr>
                        <tr>
                            <td>{{profile.customer_activity.total_request}}</td>
                            <td>{{profile.customer_activity.total_generated_report}}</td>
                            <td>{{profile.customer_activity.corresponding_entities}}</td>
                            <td>{{profile.customer_activity.total_pending}}</td>
                        </tr>
                    </table>
                </div>

            </div>
        </section>
    </div>
</div>
<jsp:include page="dialogs/message.jsp"/>
</body>
</html>
