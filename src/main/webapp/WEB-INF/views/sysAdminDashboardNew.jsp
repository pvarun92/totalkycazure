<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/landingController.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/sysMisController.js' />"></script>

    <style>
        .navbar {
            margin-bottom: 1%;
            height: 20px;
            border: none;
            border-radius: 0px;
        }

        .fa-bars:before {
            border: 1px solid;
            padding: 2px;
            font-size: 15px;
        }

        .fa-bars .fa-clock-o {
            font-size: 13px;
            position: absolute;
            left: 13px;
            top: 16px;
            background: transparent;
        }
    </style>
</head>
<body data-ng-controller="landingController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Dashboard</H3>
    </div>
    <div class="col-xs-6">
        <span style="cursor: pointer" data-ng-click="getDashboardData()">
            <span class="pull-right refresh">
            <%--<span class="fa-stack fa-lg"><i class="fa fa-refresh fa-stack-1x "></i></span>--%>
            Karza Technologies
        </span>
        </span>
    </div>
</div>
<div class="Absolute-Center is-Responsive">
    <div class="col-sm-offset-2 col-sm-8 text-center dashboard-block">

        <div class="col-xs-6">
            <div class="dashboard-icon">
                <i class="fa fa-university" aria-hidden="true"></i>
            </div>
            <div>
                <a href="sysadmin?navigate_to=sysAdminCustomerManagement" target="_parent">
                    <button type="button" id="manageCustomers" class="btn btn-group-justified button dashboard-button">
                        Manage Customers
                    </button>
                </a>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="col-xs-12 dashboard-icon">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
            </div>
            <div>
                <a href="sysadmin?navigate_to=sysAdminMis" target="_parent">
                    <button type="button" id="misReports" class="btn btn-group-justified button dashboard-button">MIS
                        Reports
                    </button>
                </a>
            </div>
        </div>
    </div>

</div>
<jsp:include page="dialogs/changePassword.jsp"/>
<jsp:include page="dialogs/firstTimeLogin.jsp"/>
<jsp:include page="dialogs/message.jsp"/>
</body>
</html>