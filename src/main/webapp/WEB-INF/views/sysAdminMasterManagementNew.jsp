<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Management Masters</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>">
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/admin-project_style.css'/>">--%>
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/bootstrap.template.min.css'/>">--%>
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/sb-admin.css'/>">--%>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="<c:url value='/js/static/jquery.js'/>"></script>
    <script src="<c:url value='/js/static/jquery-1.9.1.min.js'/>"></script>
    <script src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script src="<c:url value='/js/app.js'/>"></script>
    <script src="<c:url value='/js/controllers/sysAdminMasterManagementControllerNew.js' />"></script>

    <style>
        .active {
            background-color: #666666 !important;
        }

        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }
    </style>
</head>
<body data-ng-controller="sysAdminMasterManagementControllerNew" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Manage Masters</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background" data-ng-show="masterListBy == 'branch'">
        <div data-ng-show="showBranchMasterManagement">
            <div class="col-xs-12 no-padding">
                <div class="col-xs-6 no-padding media-width add-new-button" style="padding-right: 0px">
                    <div class="col-xs-4 filter-search">
                        <div class="form-group input-group" style="min-width: 160px;">
                            <input type="text" class="form-control input-field" data-ng-model="searchBranch"
                                   placeholder="Search By Branches">
                            <span class="input-group-btn no-radius">
                                    <button class="btn btn-default input-field"
                                            style="padding-top: 5px;background: #f4f7fa;margin: -1px" type="button"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label class="select-label" style="margin-top: 0px">
                            <select name="masterListBy" class="input-field" data-ng-model="masterListBy"
                                    data-ng-change="getMasterList(masterListBy)">
                                <option value="branch">Branch Master</option>
                                <option value="location">Location Master</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-xs-4 no-padding" style="padding: 5px">
                        <span class="heading-text" style="color: #0e76bc;font-weight: bold;">Total Branches : </span>
                        {{branches.length}}
                    </div>
                </div>
                <div class="col-xs-6 media-width" style="padding-right: 0px">
                    <div class="col-xs-4 pull-right" style="padding-right: 0px">
                        <div class="row col-xs-12 no-padding add-new-button">
                            <button type="button" id="mergeBranch" class="btn button submit-button"
                                    data-ng-click="mergeBranchPopup()"> Merge Branches
                            </button>
                        </div>
                    </div>
                    <div class="col-xs-4 pull-right" style="padding-right: 0px">
                        <div class="row col-xs-12 no-padding add-new-button">
                            <button type="button" id="newBranch" class="btn btn-group-justified button submit-button"
                                    data-ng-click="createNewBranch()"> Add A New Branch
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 no-padding text-center">
                <Table class="table activity text-left dashboard-table">
                    <Tr class="gray-header">
                        <th>
                            <a href="" ng-click="orderByField='id'; reverseSort = !reverseSort"
                               data-ng-init="orderByField='branchName';">
                                Branch ID
                                <span ng-show="orderByField == 'id'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                            </a>
                        </th>
                        <th>
                            <a href="" ng-click="orderByField='branchName'; reverseSort = !reverseSort">
                                Branch Name
                                <span ng-show="orderByField == 'branchName'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                            </a>
                        </th>
                        <th>
                            <a href="" ng-click="orderByField='city'; reverseSort = !reverseSort">
                                City
                                <span ng-show="orderByField == 'city'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                            </a>
                        </th>
                        <th>
                            <a href="" ng-click="orderByField='state'; reverseSort = !reverseSort">
                                State
                                <span ng-show="orderByField == 'state'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                            </a>
                        </th>
                        <th>Manage</th>
                    </Tr>
                    <Tr dir-paginate="branch in branches | orderBy:orderByField:reverseSort| filter: searchBranch | itemsPerPage: pageSize"
                        current-page="currentPage">
                        <td style="width: 10%"><span class="wrap-value"><a href="" style="text-decoration: none">{{branch.id}}</a></span>
                        </td>
                        <td><span class="wrap-value"> {{branch.branchName}}</span></td>
                        <td><span class="wrap-value"> {{branch.city}}</span></td>
                        <td><span class="wrap-value"> {{branch.state}}</span></td>
                        <td>
                            <a href="" data-ng-click="editBranch(branch)" style="text-decoration: none">
                                <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                            </a>
                            <a href="" data-ng-click="deleteBranch(branch)" style="text-decoration: none">
                                <span class="fa-stack fa-lg"><i class="fa fa-trash-o"></i></span>
                            </a>
                        </td>
                    </Tr>
                </Table>
                <div class="col-xs-12 no-padding media-width">
                    <div class="col-xs-5 media-width ">
                    </div>

                    <div class="col-xs-7 no-padding media-width">
                        <div class="row col-xs-12 no-padding">
                            <div class="pull-right">
                                <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                                <dir-pagination-controls boundary-links="true"
                                                         on-page-change="pageChangeHandler(newPageNumber)"
                                                         template-url="dirPagination.tpl.html"></dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form name="form" novalidate class="css-form" data-ng-hide="showBranchMasterManagement">
            <div class="col-xs-12 no-padding" style="display: inline-flex">
                <label class="color-blue size-10 font-bold" style="min-width: 200px">New Branch Request</label>
                <hr style="margin-top: 14px;">
            </div>
            <div class="new-request-container">
                <div class="col-xs-12" style="padding-top: 15px">
                    <div class="field-block col-xs-12 margin-bottom">
                        <div class="col-xs-2 no-padding">
                            <label>Branch Name* </label>
                        </div>
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <div class="col-xs-6 no-padding">
                                <input type="text" name="branchName" id="branchName"
                                       data-ng-model="newBranch.branchName" autofocus class="input-field"
                                       placeholder="Branch Name"
                                       ng-class="{true:'invalid-field',false:''}[form.branchName.$error.required && (form.branchName.$touched || form.$submitted)]"
                                       required/>
                                <span class="error-message no-border"
                                      ng-show="form.branchName.$error.required && (form.branchName.$touched || form.$submitted)">Branch Name is required</span>
                            </div>
                        </div>
                    </div>

                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>Address </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="address1" id="address1" data-ng-model="newBranch.address"
                                   class="input-field input-field"
                                   maxlength="255"/>
                        </div>
                    </div>


                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>City </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6 no-padding">
                                <input type="text" name="city" id="city" data-ng-model="newBranch.city"
                                       class="input-field input-field"
                                       maxlength="255"/>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                <label>State </label>
                            </div>
                            <div class="col-xs-4" style="padding: 0px;padding-left: 1px">
                                <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                    <select id="state" name="state" data-ng-model="newBranch.state"
                                            class="input-field selectboxit-arrow-container">
                                        <option value="">Select</option>
                                        <option data-ng-repeat="state in states" value="{{state}}">{{state}}</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>Country </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                    <select id="country" name="country" data-ng-model="newBranch.country"
                                            class="input-field selectboxit-arrow-container">
                                        <option value="">Select</option>
                                        <option data-ng-repeat="country in countries" value="{{country.name}}">
                                            {{country.name}}
                                        </option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                <label>Pin Code* </label>
                            </div>
                            <div class="col-xs-4" style="padding: 0px;padding-left: 1px">
                                <input type="text" maxlength="6" name="pincode" id="pincode"
                                       data-ng-model="newBranch.pincode" class="input-field" only-numeric
                                       ng-maxlength="6"
                                       required only-numeric/>
                            </div>
                        </div>
                    </div>

                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>IFSC Code </label>
                        </div>
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <div class="col-xs-6 no-padding">
                                <input type="text" name="ifscCode" id="ifscCode" data-ng-model="newBranch.ifscCode"
                                       class="input-field input-field" maxlength="255"
                                       ng-class="{true:'invalid-field',false:''}[form.ifscCode.$error.required && (form.ifscCode.$touched || form.$submitted)]"/>
                                <%--<span class="error-message no-border" ng-show="form.ifscCode.$error.required && (form.ifscCode.$touched || form.$submitted)" >IFSC Code is required</span>--%>
                            </div>
                        </div>
                    </div>

                    <div class="field-block col-xs-12 margin-bottom">
                        <div class="col-xs-2 no-padding">
                            <label>Location Group* </label>
                        </div>
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="col-xs-6 no-padding select-label" style="margin: 0px">
                                <select id="location" name="location" data-ng-model="newBranch.location"
                                        class="input-field selectboxit-arrow-container"
                                        ng-class="{true:'invalid-field',false:''}[form.location.$error.required && (form.location.$touched || form.$submitted)"
                                        required>
                                    <option value="">Select</option>
                                    <option data-ng-repeat="location in locations" value="{{location.id}}">
                                        {{location.locationOrZone}}
                                    </option>
                                </select>
                                <span class="error-message no-border"
                                      ng-show="form.location.$error.required && (form.location.$touched || form.$submitted)">Location is required</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="in-line-block col-xs-10 no-padding new-request-form-buttons margin-bottom"
                     style="padding-bottom: 0px;">
                    <div class="col-xs-2" data-ng-hide="editBranchView">
                        <button type="button" id="submit" class="btn btn-group-justified button submit-button"
                                ng-disabled="!form.$valid" style="background-color: #0e76bc"
                                data-ng-click="saveBranch(newBranch)">Save
                        </button>
                    </div>
                    <div class="col-xs-2" data-ng-show="editBranchView">
                        <button type="button" id="save" class="btn btn-group-justified button submit-button"
                                ng-disabled="!form.$valid" style="background-color: #0e76bc"
                                data-ng-click="updateBranch(newBranch)">Update
                        </button>
                    </div>
                    <div class="col-xs-2 ">
                        <button type="button" id="save" class="btn btn-group-justified button submit-button"
                                style="background-color: #0e76bc" data-ng-click="close()">Cancel
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="col-xs-12 no-padding white-background" data-ng-show="masterListBy == 'location'">
        <div data-ng-show="showLocationMasterManagement">
            <div class="col-xs-12 no-padding">
                <div class="col-xs-6 no-padding media-width add-new-button" style="padding-right: 0px">
                    <div class="col-xs-4 filter-search">
                        <div class="form-group input-group" style="min-width: 160px;">
                            <input type="text" class="form-control input-field" data-ng-model="searchLocation"
                                   placeholder="Search By Locations">
                            <span class="input-group-btn no-radius">
                                    <button class="btn btn-default input-field"
                                            style="padding-top: 5px;background: #f4f7fa;margin: -1px" type="button"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label class="select-label" style="margin-top: 0px">
                            <select name="masterListBy" class="input-field" data-ng-model="masterListBy"
                                    data-ng-change="getMasterList(masterListBy)">
                                <option value="branch">Branch Master</option>
                                <option value="location">Location Master</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-xs-4 no-padding" style="padding: 5px">
                        <span class="heading-text" style="color: #0e76bc;font-weight: bold;">Total Locations : </span>
                        {{location_data.length}}
                    </div>
                </div>
                <div class="col-xs-6 media-width" style="padding-right: 0px">
                    <div class="col-xs-4 pull-right" style="padding-right: 0px">
                        <div class="row col-xs-12 no-padding add-new-button">
                            <button type="button" id="newLocation" class="btn btn-group-justified button submit-button"
                                    data-ng-click="createNewLocation()"> Add A New Location
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 no-padding text-center">
                <Table class="table activity text-left dashboard-table">
                    <Tr class="gray-header">
                        <th>
                            <a href="" ng-click="orderByField='id'; reverseSort = !reverseSort"
                               data-ng-init="orderByField='locationOrZone';">
                                Location ID
                                <span ng-show="orderByField == 'id'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                            </a>
                        </th>
                        <th>
                            <a href="" ng-click="orderByField='locationOrZone'; reverseSort = !reverseSort">
                                Location Name
                                <span ng-show="orderByField == 'locationOrZone'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                            </a>
                        </th>
                        <th>Manage</th>
                    </Tr>
                    <Tr dir-paginate="location in location_data | orderBy:orderByField:reverseSort| filter: searchLocation | itemsPerPage: pageSize"
                        current-page="currentPage">
                        <td style="width: 10%"><span class="wrap-value"><a href="" style="text-decoration: none">{{location.id}}</a></span>
                        </td>
                        <td><span class="wrap-value"> {{location.locationOrZone}}</span></td>
                        <td>
                            <a href="" data-ng-click="editLocation(location)" style="text-decoration: none">
                                <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                            </a>
                            <a href="" data-ng-click="deleteLocation(location)" style="text-decoration: none">
                                <span class="fa-stack fa-lg"><i class="fa fa-trash-o"></i></span>
                            </a>
                        </td>
                    </Tr>
                </Table>
                <div class="col-xs-12 no-padding media-width">
                    <div class="col-xs-5 media-width ">
                    </div>

                    <div class="col-xs-7 no-padding media-width">
                        <div class="row col-xs-12 no-padding">
                            <div class="pull-right">
                                <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                                <dir-pagination-controls boundary-links="true"
                                                         on-page-change="pageChangeHandler(newPageNumber)"
                                                         template-url="dirPagination.tpl.html"></dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form name="newLocationForm" novalidate class="css-form" data-ng-hide="showLocationMasterManagement">
            <div class="col-xs-12 no-padding" style="display: inline-flex">
                <label class="color-blue size-10 font-bold" style="min-width: 200px">New Location Request</label>
                <hr style="margin-top: 14px;">
            </div>
            <div class="new-request-container">
                <div class="col-xs-12" style="padding-top: 15px">
                    <div class="field-block col-xs-12 margin-bottom">
                        <div class="col-xs-2 no-padding">
                            <label>Location Name* </label>
                        </div>
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <div class="col-xs-6 no-padding">
                                <input type="text" name="locationName" id="locationName"
                                       data-ng-model="newLocation.locationOrZone" class="input-field"
                                       ng-class="{true:'invalid-field',false:''}[newLocationForm.locationName.$error.required && (newLocationForm.locationName.$touched || newLocationForm.$submitted)"/>
                            </div>
                            <span class="error-message no-border"
                                  ng-show="newLocationForm.locationName.$error.required && (newLocationForm.locationName.$touched || newLocationForm.$submitted)">Location is required</span>
                        </div>
                    </div>
                    <div class="field-block col-xs-12 margin-bottom">
                        <div class="col-xs-2 no-padding">
                            <label>Linked Branches </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                    <select id="linkedBranches" name="linkedBranches"
                                            data-ng-model="newLocation.linkedBranches"
                                            class="input-field selectboxit-arrow-container">
                                        <option value="">Select</option>
                                        <option data-ng-repeat="branch in branches" value="{{branch.id}}">
                                            {{branch.branchName}}
                                        </option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="field-block col-xs-12 margin-bottom">
                        <div class="col-xs-2 no-padding">
                            <label>Parent Location* </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                    <select id="parenrLocation" name="parenrLocation"
                                            data-ng-model="newLocation.parentLocation"
                                            class="input-field selectboxit-arrow-container"
                                            ng-class="{true:'invalid-field',false:''}[newLocationForm.parenrLocation.$error.required && (newLocationForm.parenrLocation.$touched || newLocationForm.$submitted)"
                                            required>
                                        <option value="">Select</option>
                                        <option data-ng-repeat="location in locations" value="{{location.id}}">
                                            {{location.locationOrZone}}
                                        </option>
                                    </select>
                                    <span class="error-message no-border"
                                          ng-show="newLocationForm.parenrLocation.$error.required && (newLocationForm.parenrLocation.$touched || newLocationForm.$submitted)">Location is required</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="in-line-block col-xs-12 no-padding new-request-form-buttons margin-bottom"
                 style="padding-bottom: 0px;">
                <div class="col-xs-2" data-ng-hide="editLocationView">
                    <button type="button" id="save" ng-disabled="!newLocationForm.$valid"
                            class="btn btn-group-justified button submit-button" style="background-color: #0e76bc"
                            data-ng-click="saveLocation(newLocation)">Save
                    </button>
                </div>
                <div class="col-xs-2" data-ng-show="editLocationView">
                    <button type="button" id="submit" ng-disabled="!newLocationForm.$valid"
                            class="btn btn-group-justified button submit-button" style="background-color: #0e76bc"
                            data-ng-click="updateLocation(newLocation)">Update
                    </button>
                </div>
                <div class="col-xs-2 ">
                    <button type="button" id="save" class="btn btn-group-justified button submit-button"
                            style="background-color: #0e76bc" data-ng-click="close()"> Cancel
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="dialogs/mergeBranchesPopUpNew.jsp"/>
<jsp:include page="dialogs/message.jsp"/>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>