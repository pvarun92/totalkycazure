<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/draftsController.js'/>"></script>
    <style>
        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }

        .select-label select {
            height: 30px
        }
    </style>
</head>
<body data-ng-controller="draftsController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Drafts</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background">
        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
            <div class="col-xs-5 no-padding media-width">
                <div class="col-xs-6 no-padding">
                    <div>
                        <div class="row col-xs-12">
                            <span class="normal-text col-xs-12 label-height">From Date</span>
                        </div>
                        <div class="row col-xs-12">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-10">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="filter.filter_from" data-ng-change="selectFromDate()"
                                      uib-datepicker-popup is-open="from_datepicker.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions"
                                      date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"></span>
                                <div class="group-3-text-boxes">
                                    <%--<input type="text" name="from_date_day" id="from_date_day" data-ng-model="from_date.day" placeholder="DD" class="col-xs-4 first text-center no-padding input-font"--%>
                                    <%--size="2" maxlength="2" move-next-on-maxlength only-numeric data-ng-blur="validateFromDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>--%>
                                    <%--<input type="text" name="from_date_month" id="from_date_month" data-ng-model="from_date.month" placeholder="MM" class="col-xs-4 second text-center no-padding input-font"--%>
                                    <%--size="2" maxlength="2" move-next-on-maxlength only-numeric data-ng-blur="validateFromDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>--%>
                                    <%--<input type="text" name="from_date_year" id="from_date_year" data-ng-model="from_date.year" placeholder="YYYY" class="col-xs-4 third text-center no-padding yyyy input-font"--%>
                                    <%--size="4" maxlength="4" move-next-on-maxlength only-numeric data-ng-blur="validateFromDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>--%>
                                    <input type="text" name="filter_from" data-ng-model="filter.filter_from"
                                           class="input-field" placeholder="DD/MM/YYYY"
                                           data-ng-blur="validateFromDate()"
                                           mask="31/12/9999" clean="false" mask-restrict="accept" mask-validate="false"
                                           only-numeric-with-slash
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showFromCalendar()"
                                                        style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                            </div>
                            </p>
                        </div>
                        <div class="col-xs-12"
                             data-ng-show="invalid_filter.filter_from || invalid_filter.filter_from_required">
                                <span class="error-message" data-ng-show="invalid_filter.filter_from">
                                From date cannot be the future date.
                            </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_from_required">
                                From date is required.
                            </span>
                        </div>
                        <div class="col-xs-12" data-ng-show="filter.invalid.filter_from">
                            <span class="error-message no-border">Invalid Date</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                    <div>
                        <div class="row col-xs-12 no-padding">
                            <span class="normal-text col-xs-12 label-height">To Date</span>
                        </div>
                        <div class="row col-xs-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-10">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="filter.filter_to" data-ng-change="selectToDate()"
                                      uib-datepicker-popup is-open="to_datepicker.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions"
                                      date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"></span>
                                <div class="group-3-text-boxes">
                                    <input type="text" name="filter_to" data-ng-model="filter.filter_to"
                                           class="input-field" placeholder="DD/MM/YYYY" data-ng-blur="validateToDate()"
                                           mask="31/12/9999" clean="false" mask-restrict="accept" mask-validate="false"
                                           only-numeric-with-slash
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showToCalendar()" style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                            </div>
                            </p>
                        </div>
                        <div class="row col-xs-12"
                             data-ng-show="invalid_filter.filter_to || invalid_filter.filter_to_less_than_from || invalid_filter.filter_to_required">
                            <span class="error-message" data-ng-show="invalid_filter.filter_to">
                                    To date cannot be greater than today's date
                                </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_to_less_than_from">
                                    To date cannot be less than from date
                                </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_to_required">
                                    To date is required
                                </span>
                        </div>
                        <div class="col-xs-12 no-padding" data-ng-show="filter.invalid.filter_to">
                            <span class="error-message no-border">Invalid Date</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-7 no-padding media-width">
                <div class="col-xs-4 media-width ">
                    <div>
                        <div class="row col-xs-12">
                            <span class="normal-text label-height no-padding">Filter By Branch</span>
                        </div>
                        <div class="row col-xs-12">
                            <label class="select-label" style="margin-top: 0px">
                                <select name="filter_branch" id="filter_branch" class="input-field"
                                        data-ng-model="filter.branchId">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="branch in branches" value="{{branch.id}}">
                                        {{branch.branchName}}
                                    </option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1 media-width no-padding">
                    <div class="col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">search</span>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <button class="btn btn-primary" style="padding-top: 5px;" type="button"
                                data-ng-click="getFilteredData(filter)"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>

                <div class="col-xs-7 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">pagination</span>
                    </div>
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <%--<uib-pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged(currentPage)" class="pagination-sm" boundary-links="true" force-ellipses="true"></uib-pagination>--%>
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th style="text-align: center; width: 10%;">
                        <input type="checkbox" name="check_all" id="check_all" data-ng-model="check_all"
                               data-ng-change="toggleCheck()">
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='first_name'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='created_date'; reverseSort = !reverseSort">
                            Entity Name
                            <span ng-show="orderByField == 'entity_name'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='branch'; reverseSort = !reverseSort">
                            Branch
                            <span ng-show="orderByField == 'branch'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='no_of_parties'; reverseSort = !reverseSort">
                            No of Parties
                            <span ng-show="orderByField == 'no_of_parties'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='created_date'; reverseSort = !reverseSort">
                            Report Date
                            <span ng-show="orderByField == 'created_date'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>Edit</th>
                </Tr>
                <Tr dir-paginate="data in draft_data | orderBy:orderByField:reverseSort | itemsPerPage: pageSize"
                    current-page="currentPage">
                    <td style="text-align: center; width: 10%;">
                        <input type="checkbox" name="record_check" id="record_check" data-ng-model="data.checked"
                               data-ng-change="checkForAll()">
                    </td>
                    <td><span class="wrap-value">{{showName(data)}}</span></td>
                    <td><span class="wrap-value"> {{data.branch}}</span></td>
                    <td><span class="wrap-value"> <span data-ng-if="!data.no_of_parties">0</span>{{data.no_of_parties}}</span>
                    </td>
                    <td><span class="wrap-value"> {{getSplittedDate(data.created_date) | date: 'dd-MM-yyyy' }}</span>
                    </td>
                    <td>
                        <a href="simple_sidebar_menu?navigate_to=new_request?id={{data.request_id}}" target="_parent">
                            <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                        </a>
                    </td>
                </Tr>
            </Table>
        </div>
    </div>
    <div class="col-xs-12 no-padding" style="margin-top: 10px;">
        <div class="col-xs-12 no-padding">
            <div class="col-xs-1 no-padding submit-button-div">
                <button class="btn btn-group-justified button submit-button" data-ng-click=discardDrafts()>Discard
                    Drafts
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/ng-template" id="message.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{message_heading}}</h3>
    </div>
    <div class="modal-body text-center">
        {{message}}
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-block button view-historical-button submit-button" data-ng-click="close()">Ok</button>
    </div>
</script>
<script type="text/ng-template" id="confirmation.html">
    <div class="modal-body text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{header}}</h3>
        {{message}}
    </div>
    <div class="modal-footer">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="yes()">Yes
                </button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-group-justified btn-block button view-historical-button submit-button"
                        data-ng-click="close()">No
                </button>
            </div>
        </div>
    </div>
</script>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>