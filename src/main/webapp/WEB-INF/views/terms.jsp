<%--
  Created by IntelliJ IDEA.
  User: Varun_Prakash
  Date: 09-11-2016
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="TotalKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title>Terms and Conditions</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <%--<script type="application/javascript" src="<c:url value='/js/controllers/forgotPasswordController.js'/>"></script>--%>
</head>
<body class="terms">
<div class="termslogo">
    <a href="login"> <img src="<c:url value="/images/logo.png" />" alt="logo"> </a>
    <h2 style="text-align: center"><u>TERMS & CONDITIONS</u></h2>
</div>
<h3><b><u>General</u></b></h3>
<p>
    TotalKYC.com does not carry any analysis or commentary nor pass any judgement. The website is completely objective
    in its approach and is merely an information aggregator. The website researches, procures and collates information
    only from official sources, some of which are mentioned in Schedule “A”, and from information that is otherwise
    available in the public domain – albeit in a scattered and non user-friendly manner. For each entry, the source
    document is attached, wherever available, or identified to enable verification and full details.

    We research and aggregate information only from websites, databases, publications, notifications and orders of
    various government departments, authorities, courts, agencies and reputable websites (hereinafter referred to as the
    “sources”) and make available for the businesses who wish to subscribe to such information in a user friendly
    manner. Therefore, a Subscriber is any person who subscribes to the services of this website. A user or third party
    is any person who gains information available on this website through the Subscriber.

    Any opinions, advice, statements, services, offers, or other information or content expressed or made available by
    the sources, Subscribers or any other user of our website, are those of the respective author(s) or distributor(s)
    and not of TotalKYC.com.

    This website is for the personal and non-commercial use by the Subscriber. By using our website, the Subscriber
    agrees to comply with all of the terms and conditions hereof. This Agreement, which incorporates by reference other
    provisions applicable to use of TotalKYC.com, including, but not limited to, supplemental terms and conditions (if
    any), governing the use of material contained in our website, sets forth the terms and conditions that apply to use
    of our website by Subscriber. The right to use our website is personal to Subscriber and is not transferable to any
    other person or entity. Subscriber is responsible for all use of Subscriber's Account (under any screen name or
    password) and for ensuring that all use of Subscriber's Account complies fully with the provisions of this
    Agreement. Subscriber shall be responsible for protecting the confidentiality of Subscriber's password(s), if any.

    Karza Technologies Private Limited (“Karza”) shall have the right at any time to change or discontinue any aspect or
    feature of TotalKYC.com, including, but not limited to, content, hours of availability, and equipment needed for
    access or use.

    On occurrence of any violation of any of the terms and conditions by the Subscriber, Karza reserves the right to
    forthwith cancel the Subscriber’s access rights to TotalKYC.com, without any notice and also block access to all
    users from the violator IP address.
</p>
<h3><b><u>Change / Updation of Information</u></b></h3>
<p>Karza shall have the right at any time to change, modify or delete/ disconnect any aspect or feature / content, hours
    of availability, and equipment needed for access or use of TotalKYC.com or any part thereof, or to, with or without
    notice, add new information or insert into the data such keywords, phrases, indexing fields, technical codes,
    instructions and other applications and to analyze, edit, group and regroup the data/information/company
    names/reasons for action/actions as may be appropriate for cataloguing and indexing and making the data compatible
    with the structure and search methodology of the database and this website.
</p>
<h3><b><u>Changed Terms</u></b></h3>
<p>Karza shall have the right at any time to change or modify the terms and conditions applicable to Subscriber's use of
    TotalKYC.com, or any part thereof, or to impose new conditions, including, but not limited to, adding fees and
    charges for use. Such changes, modifications, additions or deletions shall be effective immediately upon notice
    thereof. Any use of TotalKYC.com by Subscriber after such notice shall be deemed to constitute acceptance by
    Subscriber of such changes, modifications or additions.
</p>
<h3><b><u>Equipment</u></b></h3>
<p>
    Subscriber shall be responsible for obtaining and maintaining all telephone, computer hardware, software and other
    equipment needed for access to and use of TotalKYC.com and all charges related thereto.
</p>
<h3><b><u>Data Distribution</u></b></h3>
<p>
    By using our website, the Subscriber agrees to allow us to use his name in the process of disseminating the data or
    for advertising its services.
</p>
<h3><b><u>Subscriber Conduct</u></b></h3>
<p>Subscriber shall use TotalKYC.com for lawful purposes only. Subscriber shall not post or transmit through
    TotalKYC.com any material which violates or infringes in any way upon the rights of others, which is unlawful,
    threatening, abusive, defamatory, invasive of privacy or publicity rights, vulgar, obscene, profane or otherwise
    objectionable, which encourages conduct that would constitute a criminal offense, give rise to civil liability or
    otherwise violate any law, or which, without Karza’s express prior approval, contains advertising or any
    solicitation with respect to products or services. Any conduct by a Subscriber that in Karza’s discretion restricts
    or inhibits any other Subscriber from using or enjoying TotalKYC.com will not be permitted. Subscriber shall not use
    TotalKYC.com to advertise or perform any commercial solicitation, including, but not limited to, the solicitation of
    users to become subscribers of other on-line information services competitive with TotalKYC.com.

    TotalKYC.com contains copyrighted material, trademarks and other proprietary information, including, but not limited
    to, text, software, photos, video, graphics, music, sound, and the entire contents of TotalKYC.com are copyrighted
    as a collective work under the Indian copyright laws. TotalKYC.com owns a copyright in the selection, coordination,
    arrangement and enhancement of such content, as well as in the content original to it. Subscriber may not modify,
    publish, transmit, participate in the transfer or sale, create derivative works, or in any way exploit, any of the
    content, in whole or in part. Subscriber may download copyrighted material for Subscriber's personal use only.
    Except as otherwise expressly permitted under copyright law, no copying, redistribution, retransmission, publication
    or commercial exploitation of downloaded material will be permitted without the express permission of TotalKYC.com
    and the copyright owner. In the event of any permitted copying, redistribution or publication of copyrighted
    material, no changes in or deletion of author attribution, trademark legend or copyright notice shall be made.
    Subscriber acknowledges that it does not acquire any ownership rights by downloading copyrighted material.

    Subscriber shall not upload, post or otherwise make available on TotalKYC.com any material protected by copyright,
    trademark or other proprietary right without the express permission of the owner of the copyright, trademark or
    other proprietary right and the burden of determining that any material is not protected by copyright rests with
    Subscriber. Subscriber shall be solely liable for any damage resulting from any infringement of copyrights,
    proprietary rights, or any other harm resulting from such a submission. By submitting material to any public area of
    TotalKYC.com, Subscriber automatically grants, or warrants that the owner of such material has expressly granted
    TotalKYC.com the royalty-free, perpetual, irrevocable, non-exclusive right and license to use, reproduce, modify,
    adapt, publish, translate and distribute such material (in whole or in part) worldwide and/or to incorporate it in
    other works in any form, media or technology now known or hereafter developed for the full term of any copyright
    that may exist in such material. Subscriber hereby grants TotalKYC.com the right to edit, copy, publish and
    distribute any material made available on TotalKYC.com by Subscriber.

    The Subscriber shall not be permitted to cache any part of the Site in proxy servers, to be accessed by individuals
    not registered with www.TotalKYC.com

    In case the Subscriber ceases to use his account for a continuous / uninterrupted period of two months, his account
    will be closed.

    The foregoing provisions are for the benefit of TotalKYC.com, its subsidiaries, affiliates and its third party
    content providers and licensors and each shall have the right to assert and enforce such provisions directly or on
    its own behalf.
</p>
<h3><b><u>Disclaimer</u></b></h3>
<p>TotalKYC.com shall not be held responsible for any claims due to errors, omissions or inaccuracies of any information
    / data on this website, if any directly or indirectly. Please refer to the Disclaimer.

    Further, TotalKYC.com, as indicated above, is merely an information aggregator and therefore, takes no
    responsibility for the authenticity, accurateness and/or completeness of the information sourced from elsewhere and
    disclosed on its website. TotalKYC.com has made best efforts to ensure that the information sourced and collected
    from elsewhere, has been accurately reproduced on its website, however it assumes no responsibility, nor does it
    make any representation whatsoever to the Subscribers and the Subscribers shall use the information available on the
    TotalKYC.com website, entirely on its own risk.

    TotalKYC.com similarly takes no responsibility whatsoever to the individuals named on the website, against whom
    orders have been passed by diverse regulatory authorities. TotalKYC.com is providing this service in the public
    interest for the public good and in complete good faith. TotalKYC.com has no interest of any kind whatsoever in any
    of the proceedings listed herein in respect of the aforesaid persons, or against any such persons and TotalKYC.com
    is completely dispassionate in the dissemination of the information.
</p>
<h3><b><u>Amendment to Terms and Conditions</u></b></h3>
<p>TotalKYC.com shall have the right and the sole discretion at any time to change or modify the terms and conditions of
    this Terms and Conditions without notice.
</p>
<h3><b><u>Monitoring</u></b></h3>
<p>
    TotalKYC.com shall have the right, but not the obligation, to monitor the content of the website, including chat
    rooms and forums, to determine compliance with this agreement and any operating rules established by us and to
    satisfy any law, regulation or authorized government request. We shall have the right in our sole discretion to
    edit, refuse to post or remove any material submitted to or posted on our website. Without limiting the foregoing,
    we shall have the right to remove any material that we, in our sole discretion, find to be in violation of the
    provisions hereof or otherwise objectionable.
</p>
<h3><b><u>Copyright</u></b></h3>
<p>
    The information available through this website is the property of Karza Technologies Private Limited (“Karza”) and
    is protected under copyright and other intellectual property laws. Information received from this website may be
    viewed, reformatted and printed for your personal, non-commercial use only.

    The Subscriber agrees not to reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate
    the information received through this website to anyone, excluding to others in the same organization, without the
    express prior written consent of Karza.

    The Subscriber is not authorized to reproduce, modify, copy, distribute, publish, transmit, create derivative
    products/services from, display, perform, license, transfer, or sell any information, software, products or services
    obtained from this website.

    Karza respects the rights of all copyright holders and in this regard, Karza has adopted and implemented a policy
    that provides for the termination in appropriate circumstances of Subscribers / account holders who infringe the
    rights of copyright holders. If you believe that your work has been copied in a way that constitutes copyright
    infringement, please provide us information at info@karza.in
</p>
<h3><b><u>Proprietary Rights</u></b></h3>
<p>
    Any person who is accessing or has accessed any information from this website acknowledges and agrees that all
    proprietary rights, statutory or otherwise, in the information received by such person shall remain the exclusive
    property of Karza. Any unauthorized reproduction, redistribution or transmission, for consideration or otherwise, of
    any such information contained in this website is strictly prohibited and would constitute a breach of the laws of
    India.
</p>
<h3><b><u>Indemnity</u></b></h3>
<p>The Subscriber agrees to defend, indemnify and hold harmless TotalKYC.com, its affiliates and their respective
    directors, officers, employees and agents from and against all claims and expenses, including legal fees, arising
    out of the use of TotalKYC.com by Subscriber including any alleged infringement of any rights of any third party.
</p>
<h3><b><u>Force Majeure</u></b></h3>
<p>We shall have no liability towards the Subscribers of this website for any delays or interruptions in accessing the
    site, for any reason, which is beyond the control of TotalKYC.com.
</p>
<h3><b><u>Right to Change the Terms & Conditions</u></b></h3>
<p>We reserve the exclusive right to change or modify the terms stated above at any time. All changes to the database
    operations will take effect from the time it is published in our site. Every Subscriber agrees to refer to the
    prevalent terms and conditions of this website every time he accesses this service.
</p>
<h3><b><u>Dispute Resolution</u></b></h3>
<p>Any dispute arising out of the Subscriber’s usage of this website shall be resolved through an amicable settlement by
    conciliation, in accordance with the provisions of the Arbitration and Conciliation Act, 1996 and Rules there under
    and shall be conducted by three conciliators to be appointed according to the Conciliation Rules, the third
    conciliator to be appointed jointly by the two conciliators appointed by the parties. Any unresolved disputes shall
    be resolved by arbitration in accordance with the Arbitration and Conciliation Act, 1996.
</p>
<h3><b><u>Law</u></b></h3>
<p>The Subscriber agrees to be governed by Indian Law and the courts in Mumbai only shall have jurisdiction over any
    disputes arising under this Agreement.
</p>
<h3><b><u>Privacy</u></b></h3>
<p>We respect your personal information. However, if you want to use our services, you will need to give us some
    information about yourself. We undertake not to provide your address as an address or as a part of a mailing list to
    anyone if you so expressly desire. Should you have any further questions on our privacy policy, please contact us at
    info@karza.in

    Thank you for visiting / using TotalKYC.com. Your privacy is important to us. To better protect your privacy, we
    provide this notice explaining our online information practices and the choices you can make about the way your
    information is collected and used at this TotalKYC.com site.
</p>
<h3><b><u>The Information We Collect</u></b></h3>
<p>At TotalKYC.com site, you can order products, enter contests, vote in polls or otherwise express an opinion,
    subscribe to one of our services such as our online newsletters, or participate in one of our online forums or
    communities (incase it is interactive and online). The types of personally identifiable information that may be
    collected at these pages include: name, address, e-mail address, telephone number, fax number, passport / credit
    card information (for further verification of identity only), and information about your interests in and use of
    various products, programs, and services.

    At some web ages of TotalKYC.com site, the Subscriber can submit information about other people i.e. requesting
    information about an entity or person the Subscriber might submit the recipient's name and address. The types of
    personally identifiable information that may be collected about other people at these pages include: recipient's
    name, address, e-mail address, and telephone number.

    At certain parts of some of our sites, only persons who provide us with the requested personally identifiable
    information will be able to order products, programs, and services or otherwise participate in the site's activities
    and offerings.

    We also may collect certain non-personally identifiable information when you visit many of our web pages such as the
    type of browser you are using (e.g., Netscape, Internet Explorer), the type of operating system you are using,
    (e.g., Windows '95 or Mac OS) and the domain name of your Internet service provider (e.g., Sify, VSNL etc).
</p>

<h3><b><u>How We Use the Information</u></b></h3>
<p>
    We may use the information you provide about yourself to fulfil your requests for our products, programs, and
    services, to respond to your inquiries about our offerings, and to offer you other products, programs or services
    that we believe may be of interest to you.

    We use the information that you provide about others to enable us to procure information about them. From time to
    time, we also may use this information to offer our products, programs, or services to them.

    If you choose to submit content for publication (e.g., a "letter to our editors"), we may publish your screen name
    and other information you have provided to us.

    We sometimes use the non-personally identifiable information that we collect to improve the design and content of
    our site and to enable us to personalize your Internet experience. We also may use this information in the aggregate
    to analyze site usage, as well as to offer you products, programs, or services.

    We may disclose personally identifiable information in response to legal process, for example, in response to a
    court order or a subpoena. We also may disclose such information in response to a law enforcement agency's request.

    Agents and contractors of Karza who have access to personally identifiable information are required to protect this
    information in a manner that is consistent with this Privacy Policy, for example, not using the information for any
    purpose other than to carry out the services they are performing for TotalKYC.com.

    Although we take appropriate measures to safeguard against unauthorized disclosures of information, we cannot assure
    you that personally identifiable information that we collect will never be disclosed in a manner that is
    inconsistent with this Privacy Policy.

    Karza may disclose personally identifiable information to companies whose practices are not covered by this privacy
    notice (e.g., other marketers, magazine publishers, retailers, participatory databases, and non-profit
    organizations) that want to market products or services to you. If a site shares personally identifiable
    information, it will provide you with an opportunity to opt out or block such uses.

    We may on occasion combine information we receive online with outside records to enhance our ability to market to
    you those products or services that may be of interest to you.

    Finally, the TotalKYC.com site covered by this Privacy Policy will not use or transfer personally identifiable
    information provided to us in ways unrelated to the ones described above without also providing you with an
    opportunity to opt out of these unrelated uses.
</p>
<h3><b><u>Collection of Information by Third-Party Sites and Sponsors</u></b></h3>
<p>Our site may contain links to other sites whose information practices may be different from ours. Visitors should
    consult the other sites' privacy policy/ notices, as we have no control over information that is submitted to, or
    collected by, these third parties.

    The TotalKYC.com site covered by this Privacy Policy sometimes may offer content (e.g., contests, sweepstakes, or
    promotions) that is sponsored by or co-branded with identified third parties. By virtue of these relationships, the
    third parties may obtain personally identifiable information that visitors voluntarily submit to participate in the
    site activity. TotalKYC.com has no control over these third parties' use of this information. The TotalKYC.com site
    will notify you at the time of requesting personally identifiable information if these third parties will obtain
    such information.

    TotalKYC.com website covered by this Privacy Policy may use a reputable third party to present or serve the
    advertisements that you may see at its Web pages. Our privacy policy does not cover any use of information that a
    third-party ad server may have collected from you.
</p>
<h3><b><u>Cookies</u></b></h3>
<p>To enhance your experience with our sites, many of our web pages use "cookies." Cookies are text files we place in
    your computer's browser to store your preferences. Cookies, by themselves, do not tell us your e-mail address or
    other personally identifiable information unless you choose to provide this information to us by, for example,
    registering at one of our sites. However, once you choose to furnish the site with personally identifiable
    information, this information may be linked to the data stored in the cookie.

    We use cookies to understand site usage and to improve the content and offerings on our sites. For example, we may
    use cookies to personalize your experience at our web pages (e.g., to recognize you by name when you return to our
    site), save your password in password-protected areas, and enable you to use shopping carts on our sites. We also
    may use cookies to offer you products, programs, or services.
</p>
<h3><b><u>Our Commitment to Security</u></b></h3>
<p>We have put in place appropriate physical, electronic, and managerial procedures to safeguard and help prevent
    unauthorized access, maintain data security, and correctly use the information we collect online.
</p>
<h3><b><u>How you can Access or Correct Information</u></b></h3>
<p>For instructions on how you can access the personally identifiable information that this site has collected about you
    online, or how to correct factual errors in such information, please contact the webmaster.

    To protect your privacy and security, we will take reasonable steps to help verify your identity before granting
    access or making corrections.
     Schedule ‘A’

    Income Tax Department
    UIDAI
    Central Board of Excise and Customs
    Director General of Foreign Trade
    Electoral Commission
    Ministry of Transport
    Ministry of Corporate Affairs
    Sales / Commercial Tax Departments of States of India
    State Electricity Distribution Companies
    Mahanagar Telecom Nigam Limited
    Bharat Sanchar Nigam Limited
    Other information sources covering the information of interest to creators/users of TotalKYC.com
</p>
<h3><b><u>How to Contact Us</u></b></h3>
<p>If you have any questions or concerns about the TotalKYC.com’s online policy for this site or its implementation you
    may contact us at <b>info@karza.in</b>.

   
</p>
<br/> <br/>
</body>
</html>
