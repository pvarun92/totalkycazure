<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/pendingQueueController.js'/>"></script>
    <style>
        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }

        .select-label select {
            height: 30px
        }
    </style>
</head>
<body data-ng-controller="pendingQueueController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Pending In Queue</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background">
        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
            <div class="col-xs-5 no-padding media-width">
                <div class="col-xs-6 no-padding">
                    <div>
                        <div class="row col-xs-12">
                            <span class="normal-text col-xs-12 label-height">From Date</span>
                        </div>
                        <div class="row col-xs-12">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-10">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="filter.filter_from" data-ng-change="selectFromDate()"
                                      uib-datepicker-popup is-open="from_datepicker.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions"
                                      date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"></span>
                                <div class="group-3-text-boxes">
                                    <%--<input type="text" name="from_date_day" id="from_date_day" data-ng-model="from_date.day" placeholder="DD" class="col-xs-4 first text-center no-padding input-font"--%>
                                    <%--size="2" maxlength="2" move-next-on-maxlength only-numeric data-ng-blur="validateFromDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>--%>
                                    <%--<input type="text" name="from_date_month" id="from_date_month" data-ng-model="from_date.month" placeholder="MM" class="col-xs-4 second text-center no-padding input-font"--%>
                                    <%--size="2" maxlength="2" move-next-on-maxlength only-numeric data-ng-blur="validateFromDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>--%>
                                    <%--<input type="text" name="from_date_year" id="from_date_year" data-ng-model="from_date.year" placeholder="YYYY" class="col-xs-4 third text-center no-padding yyyy input-font"--%>
                                    <%--size="4" maxlength="4" move-next-on-maxlength only-numeric data-ng-blur="validateFromDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>--%>
                                    <input type="text" name="filter_from" data-ng-model="filter.filter_from"
                                           class="input-field" placeholder="DD/MM/YYYY"
                                           data-ng-blur="validateFromDate()"
                                           clean="false"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showFromCalendar()"
                                                        style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                            </div>
                            </p>
                        </div>
                        <div class="col-xs-12"
                             data-ng-show="invalid_filter.filter_from || invalid_filter.filter_from_required">
                                <span class="error-message" data-ng-show="invalid_filter.filter_from">
                                From date cannot be the future date.
                            </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_from_required">
                                From date is required.
                            </span>
                        </div>
                        <div class="col-xs-12" data-ng-show="filter.invalid.filter_from">
                            <span class="error-message no-border">Invalid Date</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                    <div>
                        <div class="row col-xs-12 no-padding">
                            <span class="normal-text col-xs-12 label-height">To Date</span>
                        </div>
                        <div class="row col-xs-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-10">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="filter.filter_to" data-ng-change="selectToDate()"
                                      uib-datepicker-popup is-open="to_datepicker.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions"
                                      date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"></span>
                                <div class="group-3-text-boxes">
                                    <%--<input type="text" name="to_date_day" id="to_date_day" data-ng-model="to_date.day" placeholder="DD" class="col-xs-4 first text-center no-padding input-font"--%>
                                    <%--size="2" maxlength="2" move-next-on-maxlength only-numeric data-ng-blur="validateToDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"/>--%>
                                    <%--<input type="text" name="to_date_month" id="to_date_month" data-ng-model="to_date.month" placeholder="MM" class="col-xs-4 second text-center no-padding input-font"--%>
                                    <%--size="2" maxlength="2" move-next-on-maxlength only-numeric data-ng-blur="validateToDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"/>--%>
                                    <%--<input type="text" name="to_date_year" id="to_date_year" data-ng-model="to_date.year" placeholder="YYYY" class="col-xs-4 third text-center no-padding yyyy input-font"--%>
                                    <%--size="4" maxlength="4" move-next-on-maxlength only-numeric data-ng-blur="validateToDate()"--%>
                                    <%--ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"/>--%>
                                    <input type="text" name="filter_to" data-ng-model="filter.filter_to"
                                           class="input-field" placeholder="DD/MM/YYYY" data-ng-blur="validateToDate()"
                                           mask="31/12/9999" clean="false" mask-restrict="accept" mask-validate="false"
                                           only-numeric-with-slash
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showToCalendar()" style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                            </div>
                            </p>
                        </div>
                        <div class="row col-xs-12"
                             data-ng-show="invalid_filter.filter_to || invalid_filter.filter_to_less_than_from || invalid_filter.filter_to_required">
                            <span class="error-message" data-ng-show="invalid_filter.filter_to">
                                    To date cannot be greater than today's date
                                </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_to_less_than_from">
                                    To date cannot be less than from date
                                </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_to_required">
                                    To date is required
                                </span>
                        </div>
                        <div class="col-xs-12 no-padding" data-ng-show="filter.invalid.filter_to">
                            <span class="error-message no-border">Invalid Date</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-7 no-padding media-width">
                <div class="col-xs-4 media-width ">
                    <div>
                        <div class="row col-xs-12">
                            <span class="normal-text label-height no-padding">Filter By Branch</span>
                        </div>
                        <div class="row col-xs-12">
                            <label class="select-label" style="margin-top: 0px">
                                <select name="filter_branch" id="filter_branch" class="input-field"
                                        data-ng-model="filter.branch">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="branch in branches" value="{{branch.id}}">
                                        {{branch.branchName}}
                                    </option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1 media-width no-padding">
                    <div class="col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">search</span>
                    </div>
                    <div class="col-xs-12 no-padding">
                        <button class="btn btn-primary" style="padding-top: 5px;" type="button"
                                data-ng-click="getFilteredData(filter)"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>

                <div class="col-xs-7 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">pagination</span>
                    </div>
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th style="min-width: 30px">
                        <a href="" ng-click="orderByField='sr'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='sr';">
                            Sr No
                            <span ng-show="orderByField == 'sr'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='request_id'; reverseSort = !reverseSort">
                            Request ID
                            <span ng-show="orderByField == 'request_id'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='entity_name'; reverseSort = !reverseSort">
                            Entity Name
                            <span ng-show="orderByField == 'entity_name'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='branch'; reverseSort = !reverseSort">
                            Branch
                            <span ng-show="orderByField == 'branch'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='request_date'; reverseSort = !reverseSort">
                            Request Date
                            <span ng-show="orderByField == 'request_date'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='set_notification'; reverseSort = !reverseSort">
                            Set Notification
                            <span ng-show="orderByField == 'set_notification'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>View</th>
                </Tr>
                <Tr dir-paginate="data in activity_data | orderBy:orderByField:reverseSort | itemsPerPage: pageSize"
                    current-page="currentPage">
                    <td data-ng-init="data.sr = $index+1">
                        {{data.sr}}
                    </td>
                    <td><span class="wrap-value"> {{data.request_id}}</span></td>
                    <td><span class="wrap-value"> {{showName(data)}}</span></td>
                    <td><span class="wrap-value"> {{data.branch}}</span></td>
                    <td><span class="wrap-value"> {{getSplittedDate(data.created_date) | date: 'dd-MM-yyyy'}} </span>
                    </td>
                    <td style="text-align: center">
                        <span data-ng-click="notifyMe()"
                              ng-class="{true:'set-notification',false:'not-set-notification'}[data.set_notification]"
                              class="fa-stack fa-lg"><i class="fa fa-bell"></i></span>
                    </td>
                    <td>
                        <a href="simple_sidebar_menu?navigate_to=new_request?id={{data.request_id}}&show=true"
                           target="_parent">
                            <span class="fa-stack fa-lg"><i class="fa fa-eye"></i></span>
                        </a>
                    </td>
                </Tr>
            </Table>
        </div>
    </div>
</div>

<script type="text/ng-template" id="notify_me_popup.html">
    <div class="modal-body text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">Notify Me!</h3>
        Apologies for the delay in generating your report. We will notify you when the report is ready. Please provide
        the email id to notify
        <br>
        <label>Email ID : </label>
        <span>
             <Input type="text" id="notification_email" name="notification_email">
            <br>
            <button class="btn btn-primary" type="button" ng-click="submit()">Submit</button>
        </span>
    </div>
</script>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>