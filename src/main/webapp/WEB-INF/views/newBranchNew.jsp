<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/newBranchNewController.js'/>"></script>

    <style>
        .close {
            line-height: 0;
            position: relative;
            left: 10px;
        }

        .tab-label {
            display: block !important;
            width: 120px !important;
            min-width: 100px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            float: left;
        }
    </style>
</head>
<body data-ng-controller="newBranchNewController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block margin-0 no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Admin Panel</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container" style="padding: 1%;">
    <form name="newCustomerForm" novalidate class="css-form">
        <div class="col-xs-12 no-padding" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 200px">New Branch Request</label>
            <hr style="margin-top: 14px;">
        </div>
        <div class="new-request-container">
            <div class="col-xs-12" style="padding-top: 15px">
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Branch Name </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="branchName" id="branchName" data-ng-model="newBranch.branchName"
                                   class="input-field"/>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Address </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <input type="text" name="address1" id="address1" data-ng-model="newBranch.address_line1"
                               class="input-field input-field"
                               maxlength="255"/>
                    </div>
                </div>


                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>City </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <select id="city" name="city" data-ng-model="newBranch.city"
                                        class="input-field selectboxit-arrow-container">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="city in cities" value="{{city}}">{{city}}</option>
                                </select>
                            </label>
                        </div>
                        <div class="col-xs-2 no-padding text-center">
                            <label>State </label>
                        </div>
                        <div class="col-xs-4" style="padding: 0px;padding-left: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <select id="state" name="state" data-ng-model="newBranch.state"
                                        class="input-field selectboxit-arrow-container">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="state in states" value="{{state}}">{{state}}</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Country </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <select id="country" name="country" data-ng-model="newBranch.country"
                                        class="input-field selectboxit-arrow-container">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="country in countries" value="{{country.name}}">
                                        {{country.name}}
                                    </option>
                                </select>
                            </label>
                        </div>
                        <div class="col-xs-2 no-padding text-center">
                            <label>Pin Code </label>
                        </div>
                        <div class="col-xs-4" style="padding: 0px;padding-left: 1px">
                            <input type="text" maxlength="6" name="pincode" id="pincode"
                                   data-ng-model="newBranch.pincode" class="input-field" only-numeric ng-maxlength="6"
                                   required only-numeric/>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>IFSC Code </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="ifscCode" id="ifscCode" data-ng-model="newBranch.ifscCode"
                                   class="input-field input-field" maxlength="255"/>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Location Group </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="col-xs-6 no-padding select-label" style="margin: 0px">
                            <div ng-dropdown-multiselect="" options="locations" selected-model="newBranch.locations"
                                 checkboxes="true" extra-settings="multipleDropDownSetting"></div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="in-line-block col-xs-10 no-padding new-request-form-buttons margin-bottom"
                 style="padding-bottom: 0px;">
                <div class="col-xs-2">
                    <button type="button" id="submit" class="btn btn-group-justified button submit-button"
                            style="background-color: #0e76bc" data-ng-click="submitForm(selectedTab)">Save
                    </button>
                </div>
                <div class="col-xs-2 ">
                    <button type="button" id="save" class="btn btn-group-justified button submit-button"
                            data-ng-click="saveForm()">Submit
                    </button>
                </div>
            </div>
    </form>
</div>
</body>
</html>
