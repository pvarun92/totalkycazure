<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/userController.js'/>"></script>
    <style>
        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }
    </style>
</head>
<body data-ng-controller="userController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Users</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background">
        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
            <div class="col-xs-2 media-width" style="padding-right: 0px">
                <div class="col-xs-12" style="padding-right: 0px">
                    <div class="row col-xs-12 no-padding add-new-button">
                        <button type="button" id="newUser" class="btn btn-group-justified button submit-button"
                                data-ng-click="crudUserPopup('new')"> Add User
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 no-padding media-width" style="padding-right: 0px">
                <div class="col-xs-6 no-padding add-new-button">
                    <span class="heading-text" style="color: #0e76bc;font-weight: bold;">Active Users:</span>
                    {{activeUsers.length}}
                </div>
                <div class="col-xs-6 no-padding">
                </div>
            </div>
            <div class="col-xs-5 no-padding media-width">
                <div class="col-xs-5 media-width ">
                </div>

                <div class="col-xs-7 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">pagination</span>
                    </div>
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th>
                        <a href="" ng-click="orderByField='isActive'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='isActive';">
                            Active User
                            <span ng-show="orderByField == 'isActive'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='userName'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='userName';">
                            User Name
                            <span ng-show="orderByField == 'userName'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='designation'; reverseSort = !reverseSort">
                            Designation
                            <span ng-show="orderByField == 'designation'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='contactNumber'; reverseSort = !reverseSort">
                            Contact Number
                            <span ng-show="orderByField == 'contactNumber'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='email'; reverseSort = !reverseSort">
                            Email
                            <span ng-show="orderByField == 'email'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>Edit</th>
                </Tr>
                <Tr dir-paginate="data in user_data | orderBy:orderByField:reverseSort | itemsPerPage: pageSize"
                    current-page="currentPage">
                    <td style="width: 10%"><span class="wrap-value">
                        <input type="checkbox" name="isActive" id="isActive" data-ng-model="data.active"
                               data-ng-change="toggleActiveUser(data)">
                    </span></td>
                    <td><span class="wrap-value"> <span><a href="" data-ng-click="crudUserPopup('view',data)">{{data.userName}}</a></span></span>
                    </td>
                    <td><span class="wrap-value"> {{data.designation}}</span></td>
                    <td><span class="wrap-value"> {{data.contactNumber}}</span></td>
                    <td><span class="wrap-value"> {{data.email}} </span></td>
                    <td>
                        <a href="" data-ng-click="crudUserPopup('edit',data)">
                            <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                        </a>
                    </td>
                </Tr>
            </Table>
        </div>
    </div>
</div>
<jsp:include page="dialogs/crudUserpopup.jsp"/>
<jsp:include page="dialogs/message.jsp"/>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>