<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript"
            src="<c:url value='/js/controllers/customerManagementController.js'/>"></script>

    <style>
        .close {
            line-height: 0;
            position: relative;
            left: 10px;
        }

        .tab-label {
            display: block !important;
            width: 120px !important;
            min-width: 100px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            float: left;
        }
    </style>
</head>
<body data-ng-controller="customerManagementController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block margin-0 no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Customer Management</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container" style="padding: 1%;">
    <form name="newCustomerForm" novalidate class="css-form" data-ng-show="showNewCustomerPage">
        <div class="col-xs-12 no-padding" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 200px">New Customer Request</label>
            <hr style="margin-top: 14px;">
        </div>
        <div class="new-request-container">
            <div class="col-xs-12" style="padding-top: 15px">
                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Customer Name*</label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="customerName" id="customerName"
                                   data-ng-model="newCustomer.customerName" class="input-field" required
                                   ng-class="{true:'invalid-field',false:''}[newCustomerForm.customerName.$error.required && (newCustomerForm.customerName.$touched || newCustomerForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newCustomerForm.customerName.$error.required && (newCustomerForm.customerName.$touched || newCustomerForm.$submitted)">Customer Name is required</span>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Customer Type* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="select-label col-xs-6 no-padding" style="margin: 0px">
                            <select id="customerType" name="customerType" data-ng-model="newCustomer.customerType"
                                    class="input-field selectboxit-arrow-container" required
                                    ng-class="{true:'invalid-field',false:''}[newCustomerForm.customerType.$error.required && (newCustomerForm.customerType.$touched || newCustomerForm.$submitted)]">
                                <option value="">Select</option>
                                <option data-ng-repeat="type in customerTypes" value="{{type}}">{{type}}</option>
                            </select>
                            <span class="error-message no-border"
                                  ng-show="newCustomerForm.customerType.$error.required && (newCustomerForm.customerType.$touched || newCustomerForm.$submitted)">Customer Type is required</span>
                        </label>
                    </div>
                </div>

                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Address* </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <input type="text" name="address1" id="address1" data-ng-model="newCustomer.address"
                               class="input-field input-field"
                               maxlength="255" required
                               ng-class="{true:'invalid-field',false:''}[newCustomerForm.address1.$error.required && (newCustomerForm.address1.$touched || newCustomerForm.$submitted)]"/>
                        <span class="error-message no-border"
                              ng-show="newCustomerForm.address1.$error.required && (newCustomerForm.address1.$touched || newCustomerForm.$submitted)">Address line 1 is required</span>
                    </div>
                </div>
                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                    </div>
                    <div class="col-xs-6 no-padding">
                        <input type="text" name="address2" id="address2" maxlength="255"
                               data-ng-model="newCustomer.address1" class="input-field"/>
                    </div>
                </div>
                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>State* </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <select id="state" name="state" data-ng-model="newCustomer.state"
                                        class="input-field selectboxit-arrow-container" required
                                        ng-class="{true:'invalid-field',false:''}[newCustomerForm.state.$error.required && (newCustomerForm.state.$touched || newCustomerForm.$submitted)]">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="state in states" value="{{state}}">{{state}}</option>
                                </select>
                            </label>
                            <span class="error-message no-border"
                                  ng-show="newCustomerForm.state.$error.required && (newCustomerForm.state.$touched || newCustomerForm.$submitted)">State is required</span>
                        </div>
                        <div class="col-xs-2 no-padding text-center">
                            <label>Pin Code* </label>
                        </div>
                        <div class="col-xs-4" style="padding: 0px;padding-left: 1px">
                            <input type="text" maxlength="6" name="pincode" id="pincode"
                                   data-ng-model="newCustomer.pinCode" class="input-field" only-numeric ng-maxlength="6"
                                   required only-numeric
                                   ng-class="{true:'invalid-field',false:''}[newCustomerForm.pincode.$error.required && (newCustomerForm.pincode.$touched || newCustomerForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newCustomerForm.pincode.$error.required && (newCustomerForm.pincode.$touched || newCustomerForm.$submitted)">Pincode is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Contact Name* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="contactPerson" id="contactPerson"
                                   data-ng-model="newCustomer.contactPerson" class="input-field" required
                                   ng-class="{true:'invalid-field',false:''}[newCustomerForm.contactPerson.$error.required && (newCustomerForm.contactPerson.$touched || newCustomerForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newCustomerForm.contactPerson.$error.required && (newCustomerForm.contactPerson.$touched || newCustomerForm.$submitted)">Contact Person is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12">
                    <div class="col-xs-2 no-padding">
                        <label>Contact Designation* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="contactDesignation" id="contactDesignation"
                                   data-ng-model="newCustomer.designation" class="input-field" required
                                   ng-class="{true:'invalid-field',false:''}[newCustomerForm.contactDesignation.$error.required && (newCustomerForm.contactDesignation.$touched || newCustomerForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newCustomerForm.contactDesignation.$error.required && (newCustomerForm.contactDesignation.$touched || newCustomerForm.$submitted)">Contact Person is required</span>
                        </div>
                    </div>

                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>Email ID* </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <input type="text" name="email" id="email" data-ng-model="newCustomer.email"
                                       class="input-field" 
                                       maxlength="255"
                                       required
                                       ng-class="{true:'invalid-field',false:''}[(newCustomerForm.email.$error.required && (newCustomerForm.email.$touched || newCustomerForm.$submitted)) || (newCustomerForm.email.$error.pattern && (newCustomerForm.email.$touched || newCustomerForm.$submitted))]"/>
                                <span class="error-message no-border"
                                      ng-show="newCustomerForm.email.$error.required && (newCustomerForm.email.$touched || newCustomerForm.$submitted)">Email is required</span>
                                <span class="error-message no-border"
                                      ng-show="newCustomerForm.email.$error.pattern && (newCustomerForm.email.$touched || newCustomerForm.$submitted)">Email is Invalid</span>
                            </div>
                            <%--<div class="col-xs-1 no-padding">--%>
                            <%--<div class="input-field plus-icon">--%>
                            <%--<i class="fa fa-plus" aria-hidden="true"></i>--%>
                            <%--</div>--%>

                            <%--</div>--%>
                        </div>
                    </div>
                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>Contact Number* </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <input type="text" name="contactNumber" id="contactNumber"
                                       data-ng-model="newCustomer.contactNumber" class="input-field" maxlength="255"
                                       required only-numeric
                                       ng-class="{true:'invalid-field',false:''}[newCustomerForm.contactNumber.$error.required && (newCustomerForm.contactNumber.$touched || newCustomerForm.$submitted)]"/>
                                <span class="error-message no-border"
                                      ng-show="newCustomerForm.contactNumber.$error.required && (newCustomerForm.contactNumber.$touched || newCustomerForm.$submitted)">Contact Number is required</span>
                            </div>
                            <%--<div class="col-xs-1 no-padding">--%>
                            <%--<div class="input-field plus-icon">--%>
                            <%--<i class="fa fa-plus" aria-hidden="true"></i>--%>
                            <%--</div>--%>

                            <%--</div>--%>
                        </div>
                    </div>
                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>Pan Number </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <input type="text" name="panNumber" id="panNumber" data-ng-model="newCustomer.panNumber"
                                       class="input-field" maxlength="255"/>
                            </div>
                        </div>
                    </div>
                    <div class="field-block col-xs-12">
                        <div class="col-xs-2 no-padding">
                            <label>Tan Number </label>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                                <input type="text" name="contactNumber" id="tanNumber"
                                       data-ng-model="newCustomer.tanNumber" class="input-field" maxlength="255"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="customer-document-info-table">
                        <h4 class="bold-center pull-left">Subscription Master </h4>
                        <table class="table activity">
                            <tr class="blue-backgroung-header">
                                <th> Application</th>
                                <th> Effective Date</th>
                                <th> Valid Till</th>
                                <th> Billing Priodicity</th>
                            </tr>
                            <tr data-ng-repeat="data in newCustomer.subscriptionMasters">
                                <td>{{getSplittedDate(data.application) | date: 'dd-MM-yyyy'}}</td>
                                <td>{{getSplittedDate(data.contractDate) | date: 'dd-MM-yyyy'}}</td>
                                <td>{{getSplittedDate(data.validTillDate) | date: 'dd-MM-yyyy'}}</td>
                                <td>{{getSplittedDate(data.billingPeriodicity) | date: 'dd-MM-yyyy'}}</td>
                            </tr>
                        </table>
                        <div class="field-block col-xs-12">
                            <div class="col-xs-1 no-padding" style="margin-bottom: 10px">
                                <u><a href="" data-ng-click="newSubscriptionMaster()">Add New</a></u>
                            </div>
                            <div class="col-xs-1 no-padding">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12" style="margin-bottom: 15px">
                <div class="in-line-block col-xs-12 no-padding new-request-form-buttons" style="padding-bottom: 0px;">
                    <div class="col-xs-2" data-ng-hide="ShowEditCustomer">
                        <button type="button" id="submit" ng-disabled="!newCustomerForm.$valid"
                                class="btn btn-group-justified button submit-button" style="background-color: #0e76bc"
                                data-ng-click="saveCustomer(newCustomer)">Save
                        </button>
                    </div>
                    <div class="col-xs-2 " data-ng-hide="ShowEditCustomer">
                        <button type="button" id="save" ng-disabled="!newCustomerForm.$valid"
                                class="btn btn-group-justified button submit-button"
                                data-ng-click="saveCustomer(newCustomer)">Submit
                        </button>
                    </div>
                    <div class="col-xs-2" data-ng-show="ShowEditCustomer">
                        <button type="button" id="save" ng-disabled="!newCustomerForm.$valid"
                                class="btn btn-group-justified button submit-button" style="background-color: #0e76bc"
                                data-ng-click="updateCustomer(newCustomer)">Update
                        </button>
                    </div>
                    <div class="col-xs-2">
                        <button type="button" id="save" class="btn btn-group-justified button submit-button"
                                style="background-color: #0e76bc" data-ng-click="closeCustomerForm()">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form name="newSubscriptionForm" novalidate class="css-form" data-ng-hide="showNewCustomerPage">
        <div class="col-xs-12 no-padding" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 200px">Subscription Master</label>
            <hr style="margin-top: 14px;">
        </div>
        <div class="new-request-container">
            <div class="col-xs-12" style="padding-top: 15px">
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Application* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <label class="col-xs-6 no-padding select-label" style="margin: 0px">
                            <div id="application" name="application" ng-dropdown-multiselect="" events="customEvents"
                                 options="applicationList" selected-model="newSubscription.application"
                                 checkboxes="true" extra-settings="multipleDropDownSetting"></div>
                            <span class="error-message no-border"
                                  ng-show="applicationRequired">Application is required</span>
                        </label>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Contract Date* </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-md-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-6 no-padding date-picker">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="contractDate" data-ng-change="selectContractDate()"
                                      uib-datepicker-popup is-open="contract_date_popup.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions" ng-required="true"
                                      close-text="Close"></span>
                                <div class="col-xs-12 no-padding">
                                    <input type="text" name="contractDate" data-ng-model="newSubscription.contractDate"
                                           class="input-field" placeholder="DD/MM/YYYY"
                                           data-ng-blur="validateContactDate()"
                                           clean="false"
                                           ng-class="{true:'invalid-field',false:''}[is_contract_date_valid || (newSubscriptionForm.contractDate.$error.required && (newSubscriptionForm.contractDate.$touched || newSubscriptionForm.$submitted))]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-1 no-padding text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"
                                                ng-click="showContractDateCalendar()"
                                                style="padding: 0px;border: none;">
                                            <i class="fa fa-lg fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            </p>
                            <div class="col-xs-12 no-padding">
                                <div class="col-xs-12 no-padding">
                                    <span class="error-message no-border" data-ng-show="is_contract_date_valid">Invalid Date</span>
                                    <span class="error-message no-border"
                                          ng-show="newSubscriptionForm.contractDate.$error.required && (newSubscriptionForm.contractDate.$touched || newSubscriptionForm.$submitted)">Contract Date is required</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Valid From* </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-md-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-6 no-padding date-picker">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="validFromDate" data-ng-change="selectValidFromDate()"
                                      uib-datepicker-popup is-open="valid_from_date_popup.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions" ng-required="true"
                                      close-text="Close"></span>
                                <div class="col-xs-12 no-padding">
                                    <input type="text" name="validFromDate"
                                           data-ng-model="newSubscription.validFromDate" class="input-field"
                                           placeholder="DD/MM/YYYY" data-ng-blur="validateValidFromDate()"
                                           clean="false"
                                           ng-class="{true:'invalid-field',false:''}[is_valid_from_valid || (newSubscriptionForm.validFromDate.$error.required && (newSubscriptionForm.validFromDate.$touched || newSubscriptionForm.$submitted))]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-1 no-padding text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="showValidFromCalendar()"
                                                style="padding: 0px;border: none;">
                                            <i class="fa fa-lg fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            </p>
                            <div class="col-xs-12 no-padding margin-bottom">
                                <div class="col-xs-12 no-padding">
                                    <span class="error-message no-border" data-ng-show="is_valid_from_valid">Invalid Date</span>
                                    <span class="error-message no-border"
                                          ng-show="newSubscriptionForm.validFromDate.$error.required && (newSubscriptionForm.validFromDate.$touched || newSubscriptionForm.$submitted)">Valid From Date is required</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Valid Till* </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-md-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-6 no-padding date-picker">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="validTillDate" data-ng-change="selectValidTillDate()"
                                      uib-datepicker-popup is-open="valid_till_date_popup.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions" ng-required="true"
                                      close-text="Close"></span>
                                <div class="col-xs-12 no-padding">
                                    <input type="text" name="validTillDate"
                                           data-ng-model="newSubscription.validTillDate" class="input-field"
                                           placeholder="DD/MM/YYYY" data-ng-blur="validateValidTillDate()"
                                           clean="false"
                                           ng-class="{true:'invalid-field',false:''}[is_valid_till_valid || (newSubscriptionForm.validTillDate.$error.required && (newSubscriptionForm.validTillDate.$touched || newSubscriptionForm.$submitted))]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-1 no-padding text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="showValidTillCalendar()"
                                                style="padding: 0px;border: none;">
                                            <i class="fa fa-lg fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            </p>
                            <div class="col-xs-12 no-padding">
                                <div class="col-xs-12 no-padding">
                                    <span class="error-message no-border" data-ng-show="is_valid_till_valid">Invalid Date</span>
                                    <span class="error-message no-border"
                                          ng-show="newSubscriptionForm.validTillDate.$error.required && (newSubscriptionForm.validTillDate.$touched || newSubscriptionForm.$submitted)">Valid Till Date is required</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>One Time Fees* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="oneTimeFees" id="oneTimeFees"
                                   data-ng-model="newSubscription.oneTimeFees" class="input-field" required only-numeric
                                   ng-class="{true:'invalid-field',false:''}[newSubscriptionForm.oneTimeFees.$error.required && (newSubscriptionForm.oneTimeFees.$touched || newSubscriptionForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newSubscriptionForm.oneTimeFees.$error.required && (newSubscriptionForm.oneTimeFees.$touched || newSubscriptionForm.$submitted)">One Time Fees is required</span>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Fees Per User ID* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="perUserIdFees" id="perUserIdFees"
                                   data-ng-model="newSubscription.perUserIdFees" class="input-field" required
                                   only-numeric
                                   ng-class="{true:'invalid-field',false:''}[newSubscriptionForm.perUserIdFees.$error.required && (newSubscriptionForm.perUserIdFees.$touched || newSubscriptionForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newSubscriptionForm.perUserIdFees.$error.required && (newSubscriptionForm.perUserIdFees.$touched || newSubscriptionForm.$submitted)">Fees Per User ID is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Fees Per Entity* </label>
                    </div>
                    <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                        <div class="col-xs-6 no-padding">
                            <input type="text" name="perEntityRate" id="perEntityRate"
                                   data-ng-model="newSubscription.perEntityRate" class="input-field" required
                                   only-numeric
                                   ng-class="{true:'invalid-field',false:''}[newSubscriptionForm.perEntityRate.$error.required && (newSubscriptionForm.perEntityRate.$touched || newSubscriptionForm.$submitted)]"/>
                            <span class="error-message no-border"
                                  ng-show="newSubscriptionForm.perEntityRate.$error.required && (newSubscriptionForm.perEntityRate.$touched || newSubscriptionForm.$submitted)">Fees Per Entity is required</span>
                        </div>
                    </div>
                </div>
                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Billing Frequency </label>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <div class="col-xs-6" style="padding: 0px;padding-right: 1px">
                            <label class="select-label col-xs-12 no-padding" style="margin: 0px">
                                <select id="billingFrequency" name="state"
                                        data-ng-model="newSubscription.billingPeriodicity"
                                        class="input-field selectboxit-arrow-container">
                                    <option value="">Select</option>
                                    <option data-ng-repeat="billingFrequency in billingFrequencies"
                                            value="{{billingFrequency}}">{{billingFrequency}}
                                    </option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="field-block col-xs-12 margin-bottom">
                    <div class="col-xs-2 no-padding">
                        <label>Billing Start Date </label>
                    </div>
                    <div class="col-xs-4 no-padding">
                        <div class="col-md-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-9 no-padding date-picker">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="billingStartDate" data-ng-change="selectBillingStartDateDate()"
                                      uib-datepicker-popup is-open="billing_start_date_popup.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions" ng-required="true"
                                      close-text="Close"></span>
                                <div class="col-xs-12 no-padding">
                                    <input type="text" name="billingStartDate"
                                           data-ng-model="newSubscription.billingStartDate" class="input-field"
                                           placeholder="DD/MM/YYYY" data-ng-blur="validateBillingStartDateDate()"
                                           clean="false"
                                           ng-class="{true:'invalid-field',false:''}[is_billing_start_valid]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-1 no-padding text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"
                                                ng-click="showBillingStartDateCalendar()"
                                                style="padding: 0px;border: none;">
                                            <i class="fa fa-lg fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            </p>
                            <div class="col-xs-12 no-padding">
                                <div class="col-xs-12 no-padding" data-ng-show="is_billing_start_valid">
                                    <span class="error-message no-border">Invalid Date</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 no-padding text-center margin-bottom">
                        <label>Billing End Date </label>
                    </div>
                    <div class="col-xs-4 no-padding">
                        <div class="col-md-12 no-padding ">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-6 no-padding date-picker">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="billingEndDate" data-ng-change="selectBillingEndDateDate()"
                                      uib-datepicker-popup is-open="billing_end_date_popup.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions" ng-required="true"
                                      close-text="Close"></span>
                                <div class="col-xs-12 no-padding">
                                    <input type="text" name="billingEndDate"
                                           data-ng-model="newSubscription.billingEndDate" class="input-field"
                                           placeholder="DD/MM/YYYY" data-ng-blur="validateBillingEndDateDate()"
                                           mask="31/12/9999" clean="false" mask-restrict="accept" mask-validate="false"
                                           only-numeric-with-slash
                                           ng-class="{true:'invalid-field',false:''}[is_billing_end_valid]"
                                           ng-disabled=true required/>
                                </div>
                            </div>
                            <div class="col-xs-1 no-padding text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"
                                                ng-click="showBillingEndDateCalendar()"
                                                style="padding: 0px;border: none;">
                                            <i class="fa fa-lg fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            </p>
                            <div class="col-xs-12 no-padding">
                                <div class="col-xs-12 no-padding" data-ng-show="is_billing_end_valid">
                                    <span class="error-message no-border">Invalid Date</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="in-line-block col-xs-12 no-padding new-request-form-buttons" style="padding-bottom: 0px;">
                <div class="col-xs-2">
                    <button type="button" ng-disabled="!newSubscriptionForm.$valid"
                            class="btn btn-group-justified button submit-button" style="background-color: #0e76bc"
                            data-ng-click="saveSubscriptionPlan(newSubscription)">Save
                    </button>
                </div>
            </div>
    </form>
</div>

<jsp:include page="dialogs/message.jsp"/>
</body>
</html>
