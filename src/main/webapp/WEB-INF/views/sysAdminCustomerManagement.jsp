<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>">
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/admin-project_style.css'/>">--%>
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/bootstrap.template.min.css'/>">--%>
    <%--<link rel="stylesheet" type="text/css"  href="<c:url value='/css/sb-admin.css'/>">--%>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="<c:url value='/js/static/jquery.js'/>"></script>
    <script src="<c:url value='/js/static/jquery-1.9.1.min.js'/>"></script>
    <script src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script src="<c:url value='/js/app.js'/>"></script>
    <script src="<c:url value='/js/controllers/customerManagementController.js' />"></script>

    <style>
        .active {
            background-color: #666666 !important;
        }

        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }
    </style>
</head>
<body data-ng-controller="customerManagementController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Customer Management</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background">
        <div class="col-xs-12 no-padding">
            <div class="col-xs-6 no-padding media-width" style="padding-right: 0px">
                <div class="no-padding add-new-button">
                    <div class="col-xs-5 filter-search">
                        <span>
                            <div class="form-group input-group">
                                <input type="text" class="form-control search" data-ng-model="searchCustomer"
                                       placeholder="Search By Name">
                                <span class="input-group-btn no-radius">
                                    <button class="btn btn-default" style="padding-top: 5px;background: #f4f7fa"
                                            type="submit"><i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </span>
                    </div>
                    <div class="col-xs-6 no-padding" style="padding: 5px">
                        <span class="heading-text" style="color: #0e76bc;font-weight: bold;">Total Customers : </span>
                        {{customers.length}}
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                </div>
            </div>
            <div class="col-xs-6 media-width" style="padding-right: 0px">
                <div class="col-xs-4 pull-right" style="padding-right: 0px">
                    <div class="row col-xs-12 no-padding add-new-button">
                        <a href="sysadmin?navigate_to=newCustomerNew" target="_parent">
                            <button type="button" id="newUser" class="btn btn-group-justified button submit-button"> Add
                                A New Customer
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th>
                        <a href="" ng-click="orderByField='isActive'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='isActive';">
                            Customer Id
                            <span ng-show="orderByField == 'isActive'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='customerName'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='customerName';">
                            Customer Name
                            <span ng-show="orderByField == 'customerName'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='designation'; reverseSort = !reverseSort">
                            Customer Type
                            <span ng-show="orderByField == 'designation'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='contactNumber'; reverseSort = !reverseSort">
                            No. OF Users
                            <span ng-show="orderByField == 'contactNumber'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='email'; reverseSort = !reverseSort">
                            Status
                            <span ng-show="orderByField == 'email'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>Manage</th>
                </Tr>
                <Tr dir-paginate="customer in customers | orderBy:orderByField:reverseSort| filter: searchCustomer | itemsPerPage: pageSize"
                    current-page="currentPage">
                    <td style="width: 10%"><span class="wrap-value">{{customer.id}}</span></td>
                    <td><span class="wrap-value"> <span><a
                            href="sysadmin?navigate_to=customerProfileNew?id={{customer.id}}" target="_parent">{{customer.customerName}}</a></span></span>
                    </td>
                    <td><span class="wrap-value"> {{customer.customerType}}</span></td>
                    <td><span class="wrap-value"> {{customer.noOfUsers}}</span></td>
                    <td><span class="wrap-value"> {{customer.status}} </span></td>
                    <td>
                        <a href="sysadmin?navigate_to=newCustomerNew?id={{customer.id}}" target="_parent"
                           style="text-decoration: none">
                            <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                        </a>
                        <a href="" data-ng-click="deleteCustomer(customer)" style="text-decoration: none">
                            <span class="fa-stack fa-lg"><i class="fa fa-trash-o"></i></span>
                        </a>
                        <a href="" data-ng-click="crudUserPopup('refresh',data)" style="text-decoration: none">
                            <span class="fa-stack fa-lg"><i class="fa fa-refresh"></i></span>
                        </a>
                    </td>
                </Tr>
            </Table>
            <div class="col-xs-12 no-padding media-width">
                <div class="col-xs-5 media-width ">
                </div>

                <div class="col-xs-7 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="dialogs/message.jsp"/>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>