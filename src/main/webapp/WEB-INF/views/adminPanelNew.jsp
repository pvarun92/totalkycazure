<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/landingController.js'/>"></script>

    <style>
        .navbar {
            margin-bottom: 1%;
            height: 20px;
            border: none;
            border-radius: 0px;
        }

        .fa-bars:before {
            border: 1px solid;
            padding: 2px;
            font-size: 15px;
        }

        .fa-bars .fa-clock-o {
            font-size: 13px;
            position: absolute;
            left: 13px;
            top: 16px;
            background: transparent;
        }
    </style>
</head>
<body data-ng-controller="landingController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Dashboard</H3>
    </div>
    <div class="col-xs-6">
        <span style="cursor: pointer" data-ng-click="getDashboardData()">
            <span class="pull-right refresh">
            <%--<span class="fa-stack fa-lg"><i class="fa fa-refresh fa-stack-1x "></i></span>--%>
            Karza Technologies
        </span>
        </span>
    </div>
</div>
<div class="Absolute-Center is-Responsive">
    <div class="col-sm-offset-2 col-sm-8 text-center dashboard-block">

        <div class="col-xs-6">
            <div class="dashboard-icon">
                <i class="fa fa-users" aria-hidden="true"></i>
            </div>
            <div>
                <a href="simple_sidebar_menu?navigate_to=userManagementNew" target="_parent" id="userManagement"
                   class="btn btn-group-justified button dashboard-button">User Management</a>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="col-xs-12 dashboard-icon">
                <i class="fa fa-desktop" aria-hidden="true"></i>
            </div>
            <div>
                <a href="simple_sidebar_menu?navigate_to=masterManagementNew" target="_parent" id="manageMasters"
                   class="btn btn-group-justified button dashboard-button">Manage Masters</a>
            </div>
        </div>

        <%--<div class="col-sm-12 no-padding caption button">--%>
        <%--Summary Count For The Month Of {{month}} {{year}}--%>
        <%--</div>--%>
        <%--<div class="col-sm-12 no-padding header button">--%>
        <%--<div class="col-sm-4 no-padding">Total Requests</div>--%>
        <%--<div class="col-sm-4 no-padding">Total Completed</div>--%>
        <%--<div class="col-sm-4 no-padding">Total Pending</div>--%>
        <%--</div>--%>
        <%--<div class="col-sm-12 no-padding values button">--%>
        <%--<div class="col-sm-4 no-padding"><span id="total_request" data-ng-model="total_request">{{total_request}}</span></div>--%>
        <%--<div class="col-sm-4 no-padding"><span id="total_completed" data-ng-model="total_completed">{{total_completed}}</span></div>--%>
        <%--<div class="col-sm-4 no-padding"><span id="total_pending" data-ng-model="total_pending">{{total_pending}}</span></div>--%>
        <%--</div>--%>
    </div>

</div>
<jsp:include page="dialogs/changePassword.jsp"/>
<jsp:include page="dialogs/firstTimeLogin.jsp"/>
<jsp:include page="dialogs/message.jsp"/>
</body>
</html>