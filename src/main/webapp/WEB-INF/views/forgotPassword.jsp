<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/forgotPasswordController.js'/>"></script>
</head>
<body data-ng-controller="forgotPasswordController" ng-cloak>
<div class="login-page">
    <div class="text-center">
        <div>
            <div class="login-block">
                <div>
                    <h1 class="login-header"><span class="insta">Total</span><span class="kyc">KYC</span></h1>
                    <h6 class="caption">Karza web app framework @ 2016</h6>
                </div>
                <div class="no-padding">
                    <form class="form-signin">
                        <input type="text" id="email" name="email" class="form-control" data-ng-model="user.email"
                               placeholder="Email ID / Username" autofocus="" data-ng-show="showGenerateOTP">
                        <input type="text" id="otp" name="otp" class="form-control" data-ng-model="user.otp"
                               placeholder="Enter OTP" autofocus="" data-ng-show="OtpPage">
                        <span class="error-message no-border" ng-show="error && showGenerateOTP">Invalid User ID</span>
                        <div><span class="error-message no-border"
                                   ng-show="invalidOtpMessage">{{invalidOtpMessage}}</span></div>
                        <button class="btn btn-lg btn-primary btn-block login-btn" type="button"
                                data-ng-click="generateOtpPassword(user.email)">Send Password
                        </button>
                        <button class="btn btn-lg btn-primary login-btn" data-ng-show="OtpPage" type="button"
                                data-ng-click="generateOTP(user.email)">Resend OTP
                        </button>
                        <!-- <button class="btn btn-lg btn-primary login-btn" type="button" data-ng-show="OtpPage"
                                data-ng-click="generateOtpPassword(user.email)">Generate Password
                        </button> -->
                    </form>
                </div>
            </div>
        </div>
        <div style="margin-top: 20px;">
            <a href="http://www.karza.in" style="text-decoration: none;">
                <img src="<c:url value='/images/logo.png'/>" class="logo">
            </a>
        </div>

        <div class="help-block">
            <a href="terms"><u>Terms of Use</u></a>
            <span class="pie">|</span>
            <a href=""><u>Privacy Policy</u></a>
            <span class="pie">|</span>
            <a href="disclaimer"><u>Disclaimer</u></a>
            <span class="pie">|</span>
            <a href=""><u>Contact Us</u></a>
        </div>
    </div>

</div>
<div id="spinnerDiv" style="display:none;">
    <img src="<c:url value='/images/spinner.gif'/>" class="ajaxloader"/>
</div>

<script type="text/ng-template" id="message.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{message_heading}}</h3>
    </div>
    <div class="modal-body text-center">
        {{message}}
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-block button view-historical-button submit-button" data-ng-click="displayOtpPage()">Ok
        </button>
    </div>
</script>
</body>
</html>