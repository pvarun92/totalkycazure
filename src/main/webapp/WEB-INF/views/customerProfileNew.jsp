<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="instaKYC">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript"
            src="<c:url value='/js/controllers/customerProfileNewController.js'/>"></script>

</head>
<body data-ng-controller="customerProfileNewController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block margin-0 no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Customer Management</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container" style="padding: 1%;">
    <div class="row">
        <div class="col-xs-12" style="display: inline-flex">
            <label class="color-blue size-10 font-bold" style="min-width: 150px">Customer Profile</label>
            <hr style="margin-top: 14px;">
        </div>
    </div>

    <div class="new-request-container container">
        <section>
            <div class="col-xs-12">
                <div class="col-xs-1" style="min-width: 140px;">
                    <div class="profile-icon">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <span class="fa-character">{{profile.customerName[0]}}</span>
                    </div>
                </div>
                <div class="col-xs-8 profile-info">
                    <div class="blue-bold-text">
                        {{profile.customerName}}
                    </div>
                    <div>
                        Status : <span data-ng-show="profile.isActive">Active</span> <span
                            data-ng-show="!profile.isActive">Inactive</span> | Customer Type : {{profile.customerType}}
                    </div>
                    <div>
                        Contact Name : {{profile.contactPerson}} | Contact No. : {{profile.contactNumber}} | Email :
                        {{profile.email}}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="">
                        <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                    </a>
                    <a href="">
                        <span class="fa-stack fa-lg"><i class="fa fa-trash-o"></i></span>
                    </a>
                    <a data-ng-click="refreshData(customerID)" href="">
                        <span class="fa-stack fa-lg"><i class="fa fa-refresh"></i></span>
                    </a>
                </div>
            </div>
            <hr style="margin-top: 14px;">
        </section>

        <section>
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <div class="profile-data-count">
                        <div class="blue-bold-text">
                            {{profile.userCount}}
                        </div>
                        <label>Total No of Users | <a
                                href="sysadmin?navigate_to=sysAdminUserManagementNew?id={{customer_id}}"
                                target="_parent">manage</a></label>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="profile-data-count">
                        <div class="blue-bold-text">
                            {{profile.branchCount}}
                        </div>
                        <label>Total No of Branches | <a
                                href="sysadmin?navigate_to=sysAdminMasterManagementNew?id={{customer_id}}&masterListBy=branch"
                                target="_parent">manage</a></label>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="profile-data-count">
                        <div class="blue-bold-text">
                            {{profile.locationCount}}
                        </div>
                        <label>Total No of Locations | <a
                                href="sysadmin?navigate_to=sysAdminMasterManagementNew?id={{customer_id}}&masterListBy=location"
                                target="_parent">manage</a></label>
                    </div>
                </div>
            </div>
            <hr style="margin-top: 14px;">
        </section>

        <section>
            <div class="col-xs-12 profile-subscription-block">
                <span>Application Subscribed:</span>
                <label class="checkbox-label">
                    <input type="checkbox" name="chkInstakyc" id="chkInstakyc" data-ng-model="newCustomer.chkInstakyc"
                           class="" value="TotalKYC">
                    <span>TotalKYC</span>
                </label>
                <label class="checkbox-label">
                    <input type="checkbox" name="chkKscan" id="chkKscan" data-ng-model="newCustomer.chkKscan" class=""
                           value="KScan">
                    <span>KScan</span>
                </label>
                <label class="checkbox-label">
                    <input type="checkbox" name="chkWatch" id="chkWatch" data-ng-model="newCustomer.chkWatch" class=""
                           value="KWatch">
                    <span>KWatch</span>
                </label>
            </div>

            <div class="col-xs-12" style="display: inline-flex">
                <label class="size-10 font-bold" style="min-width: 125px">Subscription Master</label>
                <hr style="margin-top: 14px;">
            </div>

            <div class="col-xs-12">

                <div class="profile-info-table">
                    <table class="table activity">
                        <tr class="blue-backgroung-header">
                            <th> Application</th>
                            <th> Effective From</th>
                            <th> Valid Till</th>
                            <th> Billing Priodicity</th>
                            <th> Manage</th>
                        </tr>
                        <tr data-ng-repeat="data in profile.subscription_master">
                            <td>{{data.application}}</td>
                            <td>{{getSplittedDate(data.contractDate) | date: 'dd-MM-yyyy' }}</td>
                            <td>{{getSplittedDate(data.validTillDate) | date: 'dd-MM-yyyy' }}</td>
                            <td>{{data.billingPeriodicity}}</td>
                            <td><a href="">edit</a> | <a href="" style="color: red;">delete</a></td>
                        </tr>
                    </table>
                </div>

            </div>

            <hr style="margin-top: 14px;">

        </section>

        <section>
            <div class="col-xs-12" style="display: inline-flex">
                <label class="color-blue size-10 font-bold">Customer Activity</label>
                <label style="font-size: 10px; padding-top: 3px; padding-left: 3px;">for the current month</label>
            </div>

            <div class="col-xs-12">

                <div class="profile-info-table">
                    <table class="table activity">
                        <tr class="blue-backgroung-header">
                            <th> Total Request</th>
                            <th> Total Generated Reports</th>
                            <th> Corresponding Entities</th>
                            <th> Total Pending</th>
                        </tr>
                        <tr>
                            <td>{{profile.customer_activity.total_request}}</td>
                            <td>{{profile.customer_activity.total_generated_report}}</td>
                            <td>{{profile.customer_activity.corresponding_entities}}</td>
                            <td>{{profile.customer_activity.total_pending}}</td>
                        </tr>
                    </table>
                </div>

            </div>
        </section>
    </div>
</div>
</body>
</html>
