<%--
  Created by IntelliJ IDEA.
  User: Varun_Prakash
  Date: 09-11-2016
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title>Disclaimer</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <%--<script type="application/javascript" src="<c:url value='/js/controllers/forgotPasswordController.js'/>"></script>--%>
</head>
<body class="disclaimer">
<div class="disclaimerlogo">
    <a href="login"> <img src="<c:url value="/images/logo.png" />" alt="logo"> </a>
    <h2 style="text-align: center"><u>DISCLAIMER</u></h2>
</div>
<br/>
<p>Totalkyc.com is only an information aggregator. We research, aggregate and faithfully reproduce information only from
    government authorities, agencies, commissions, courts of law and official websitesand make available for our
    subscribed users to such information in a user friendly manner.</p>
<h3>No Warranties </h3>
<p>
    We do not endorse or take any responsibility for any errors in information from the sources. Accordingly, we have no
    more editorial control over the contents of the sources than does a public library, bookstore, or newsstand.
    While every care has been taken in preparing the information and materials contained in this website including text,
    graphics, links or other items, such information and materials are provided to you "AS IS", “AS AVAILABLE” basis,
    “BEST EFFORT” basis and “WITH ALL FAULTS” without warranty of any kind either express or implied. In particular, no
    warranty regarding non-infringement, security, accuracy, fitness for a particular purpose or freedom from computer
    virus is given in conjunction with such information and materials.
    Moreover, totalkyc.com expressly does not warrant that this website will be uninterrupted or will be free of any
    error, hacking, system failure or computer virus.
</p>
<h3>Change / Updation of Information </h3>
<p>
    The information available on this website and its updation is a continuous process. Due to the large number of
    sources from which the information is obtained for this website, and the inherent hazards of information compilation
    at various information sources and also hazards in electronic/paper copy distribution, there may be omissions,
    inaccuracies or delays in posting such information. Totalkyc.com shall not be responsible or accountable for any
    such omissions, inaccuracies or delays. totalkyc.com takes no responsibility for the information or changes or
    removal of any such information posted by/in the sources and suggests the subscribers / users to check it from the
    original source/s.
</p>
<h3> Use of totalkyc.com </h3>
<p>totalkyc.com does not expressly, impliedly or statutorily, warrant, guarantee or make any representations concerning
    the use, results of use or inability to use or access the information on the website in terms of accuracy,
    reliability, completeness, functionality, performance, continuity, timeliness or otherwise, fitness for any purpose
    and/or non-infringement of third party rights with respect to this website. totalkyc.com shall not be liable for any
    claims or losses of any nature arising directly or indirectly or from any unauthorized access to this website or
    otherwise. totalkyc.com does not provide any advice and shall not be liable to any person who enters into or
    terminates any business relationship with any other party based on any information accessed from this website or
    uses such information or is accessing any information for any investment, company affairs, legal, tax or accounting
    matters or regarding the suitability or profitability of a company, security or an investment. Any information on
    this website must not be construed as business/investment advice and the user should exercise due caution and/or
    seek independent advice before the user enters into or terminates any business relationship with any other party or
    enters into any investment or financial obligation based on any information which is provided on this website.</p>
<h3>Aadhaar AUA Disclaimer</h3>
<p>
Karza Technologies Private Limited brings Aadhaar authetication information using Khosla Labs Pvt Ltd as the underlying AUA/KUA agency.
</p>
</h3>
<h3>Opinions and judgments of information provider, Subscriber or other user not under contract with totalkyc.com</h3>
<p>In many instances, the content available through totalkyc.com represents the opinions and judgments of the respective
    information provider, Subscriber, or other user not under contract with totalkyc.com. totalkyc.com neither endorses
    nor is responsible for the accuracy or reliability of any opinion, advice or statement made on totalkyc.com by
    anyone. Under no circumstances will totalkyc.com be liable for any loss or damage caused by a Subscriber's reliance
    on information obtained through totalkyc.com. totalkyc.com shall not be liable to the Subscriber or to anyone else
    for any decision made or action taken by the Subscriber or for any consequential, special or similar damages or
    expenses caused to the Subscriber in reliance of the information on this website, even if we have been advised of
    the possibility of such damages. The Subscriber / user agrees to evaluate the accuracy, reliability, performance,
    continuity, comprehensiveness, completeness or usefulness, currentness, functionality and timeliness of any
    information, opinion, advice or other content available through totalkyc.com independently. The subscriber/ user is
    advised to take the advice of professionals, as appropriate, regarding the evaluation of any specific information,
    opinion, advice or other content on our website. We shall also not be liable to the subscriber or to anyone for any
    loss or injury caused in whole or part by negligence or contingencies in procuring, compiling, interpreting,
    reporting or delivering the information on this website. The Subscriber / user waives his or her right to any claim
    based on the detrimental effects or results of any reliance placed by the Subscriber / user on any information on
    this website.
</p>
<h3> Hyperlinks </h3>
<p>
    This website may contain links to other websites operated by parties other than totalkyc.com.
</p>
<h3>From totalkyc.com to websites outside </h3>
<p>
    There may be cases where our website provides hyperlinks to other locations or websites on the Internet. These
    hyperlinks lead to websites published or operated by third parties that are not affiliated with or in any way
    related to totalkyc.com. They have been included in our website to enhance your user experience and are presented
    for information purposes only. We endeavor to select reputable websites and sources of information for your
    convenience.
    However, by providing hyperlinks to an external website or webpage, totalkyc.com shall not be deemed to endorse,
    recommend, approve, guarantee or introduce any third parties or the services/ products they provide on their
    websites, or to have any form of cooperation with such third parties and websites unless otherwise stated by
    totalkyc.com.
    We are not in any way responsible for the content of any externally linked website or webpage. The Subscriber may
    use or follow these links at his own risk and totalkyc.com is not responsible for any damages or losses incurred or
    suffered by the Subscriber arising out of or in connection with the Subscriber’s use of the link. totalkyc.com is
    not a party to any contractual arrangements entered into between the Subscriber and the provider of the external
    website unless otherwise expressly specified or agreed to by totalkyc.com.
    Please be mindful that when a Subscriber clicks on a link and leaves our website, he will be subject to the terms of
    use and privacy policies of the other website that he is going to visit.
</p>

<h3>Hyperlinks from External Websites to totalkyc.com</h3>
<p>
    The external websites must always obtain the prior written approval of totalkyc.com before creating a hyperlink in
    any form from their website to totalkyc.com website. totalkyc.com may or may not give such approval at its absolute
    discretion. In normal circumstances, totalkyc.com may only approve a hyperlink, which displays plainly the name and/
    or website address of totalkyc.com. Any use or display of logos, trade names and trademarks of totalkyc.com as a
    hyperlink will not be approved unless in very exceptional circumstances and may be subject to a fee as totalkyc.com
    may determine at its absolute discretion.
    totalkyc.com is not responsible for the setup of any hyperlink from a third party website to any totalkyc.com. Any
    links so set up shall not constitute any form of co-operation with, or endorsement by, totalkyc.com of such third
    party website. Any link to our website shall always be an active and direct link to our website and shall be made
    directly to the home or front page of our website only and that no "framing" or "deep-linking" of our web page or
    content is allowed.
    totalkyc.com is not liable for any loss or damage incurred or suffered by the subscriber or any third party arising
    out of or in connection with such link. For further trademarks and copyright information, please refer to the
    paragraph below.
    totalkyc.com reserves the right to rescind any approval granted to link through a plain-text link or any other type
    of link at its discretion at any time.
</p>
<h3>Our Trademarks and Copyright</h3>
<p>The content and information contained within totalkyc.com or delivered to the Subscriber in connection with the use
    of our website is the property of Karza Technologies Private Limited (“Karza”) and any other third party (where
    applicable). The trademark, trade names and logos (the "Trade Marks") that are used and displayed on our website
    include registered and unregistered Trade Marks of ourselves and other third parties. Nothing on our website should
    be construed as granting any licence or right to use any Trade Marks displayed on our website. We retain all
    proprietary rights on our website. Users are prohibited from using the same without prior written permission of
    Karza or such other third parties.
    The materials on this website are protected by copyright and no part of such materials may be modified, reproduced,
    stored in a retrieval system, transmitted (in any form or by any means), copied, distributed, used for creating
    derivative works or used in any other way for commercial or public purposes without the Karza's prior written
    consent.
</p>
<h3> Privity of Contract </h3>
<p>Nothing contained in the website or this Disclaimer may be construed to create a privity of contract between the
    Subscriber and totalkyc.com.
    Limitation of Liability
    Under no circumstance the functionaries and / or officials of Karza shall be liable to the Subscriber or user or any
    third party for any special, punitive, incidental, indirect, or consequential damages of any kind, or any damages
    whatsoever.
    This disclaimer of liability also applies to any damages or injury caused by any failure of performance, error,
    omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line
    failure, theft or destruction or unauthorized access to, alteration of, or use of record, whether for breach of
    contract, tortious behavior, negligence, or under any other cause of action. Subscriber specifically acknowledges
    that totalkyc.com is not liable for the defamatory, offensive or illegal conduct of other subscribers or
    third-parties and that the risk of injury from the foregoing rests entirely with subscriber.
    In no event will totalkyc.com, or any person or entity involved in creating, producing or distributing totalkyc.com
    software, be liable for any damages, including, without limitation, direct, indirect, incidental, special,
    consequential or punitive damages arising out of the use of or inability to use totalkyc.com. Subscriber hereby
    acknowledges that the aforesaid provisions shall apply to all content on totalkyc.com.
    In addition to the terms set forth above neither, totalkyc.com, nor its affiliates, information providers or content
    partners shall be liable regardless of the cause or duration, for any errors, inaccuracies, omissions, or other
    defects in, or untimeliness or unauthenticated of, the information contained within totalkyc.com, or for any delay
    or interruption in the transmission thereof to the user, or for any claims or losses arising therefrom or occasioned
    thereby. None of the foregoing parties shall be liable for any third-party claims or losses of any nature,
    including, but not limited to, lost profits, punitive or consequential damages. Prior to the execution of an
    investment or business decision, subscribers are advised to consult with their advisers or other financial
    representative to verify the relevant information. totalkyc.com, its affiliates, information providers or content
    partners shall have no liability for investment decisions based on the information provided. Neither, totalkyc.com,
    nor its affiliates, information providers or content partners warrant or guarantee the timeliness, sequence,
    accuracy or completeness of this information. Additionally, there are no warranties as to the results obtained from
    the use of the information.
</p>
<br/>
<br/>
</body>
</html>
