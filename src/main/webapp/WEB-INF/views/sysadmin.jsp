<%@ page import="com.karza.kyc.model.CustomerMaster"%>
<%@ page import="com.karza.kyc.model.UserMaster"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TotalKYC Dashboard</title>
<link href="<c:url value='/css/bootstrap.min.css'/>" rel="stylesheet">
<link href="<c:url value='/css/simple-sidebar.css'/>" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/select.css'/>" />
<link
	href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>"
	rel="stylesheet">
<link href="<c:url value='/css/common.css'/>" rel="stylesheet"
	type="text/css" />
<script type="application/javascript"
	src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/angular.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/lodash.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/ngMask.min.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/dirPagination.js'/>"></script>
<script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/static/select_files/select.js'/>"></script>
<script type="application/javascript"
	src="<c:url value='/js/controllers/sysadminMenuController.js'/>"></script>
	<%
			response.setHeader("Pragma","no-cache");
			response.setHeader("Cache-Control","no-store");
			response.setHeader("Expires","0");
			response.setDateHeader("Expires",-1);
			%>
<style>
.navbar {
	height: 35px;
	min-height: 35px;
}

#bs-example-navbar-collapse-1 {
	height: 35px !important;
	min-height: 35px;
}

.fa-th-list:before {
	border: 1px solid;
	padding: 2px;
	font-size: 12px;
}

.fa-bars:before {
	border: 1px solid;
	padding: 2px;
	font-size: 12px;
}

.fa-bars .fa-clock-o {
	font-size: 13px;
	position: absolute;
	left: -3px;
	top: 5px;
	background: transparent;
}

.nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus
	{
	background: #fff;
}

.dropdown {
	background: #fff;
	border: 1px solid #ccc;
	border-radius: 4px;
	width: 300px;
}

.dropdown-menu>li>a {
	color: #428bca;
	white-space: initial;
	padding: 7px;
}

.dropdown ul.dropdown-menu {
	border-radius: 4px;
	box-shadow: none;
	margin-top: 20px;
	width: 300px;
}

.dropdown ul.dropdown-menu:before {
	content: "";
	border-bottom: 10px solid #fff;
	border-right: 10px solid transparent;
	border-left: 10px solid transparent;
	position: absolute;
	top: -10px;
	right: 16px;
	z-index: 10;
}

.dropdown ul.dropdown-menu:after {
	content: "";
	border-bottom: 12px solid #ccc;
	border-right: 12px solid transparent;
	border-left: 12px solid transparent;
	position: absolute;
	top: -12px;
	right: 14px;
	z-index: 9;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover
	{
	background-color: #64b9f2;
}

.select-label:after {
	top: 10px;
}

.sub-menu-active, .sub-menu-active a {
	color: #64b9f2 !important;
}
</style>
</head>

<body style="background-color: #e7e7e7; overflow: hidden;"
	data-ng-app="instaKYC" data-ng-controller="sysadminMenuController"
	ng-cloak>
	<%@ page session="true"%>
	<nav class="navbar navbar-default no-margin"
		style="background-color: white; border-color: white; height: 15px; border: none">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header fixed-brand">
			<button style="margin: 0px; padding: 3px; height: 35px; width: 35px;"
				type="button" class="navbar-toggle collapsed no-radius"
				data-toggle="collapse" id="menu-toggle"
				data-ng-click="setCollapse()">
				<i data-ng-show="!collapsedMenu"
					class="fa fa-outdent fa-stack-1x hamburger"></i> <i
					data-ng-show="collapsedMenu"
					class="fa fa-outdent fa-rotate-180 fa-stack-1x hamburger"></i>
			</button>
		</div>
		<!-- navbar-header-->

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul id="wrapper_header" class="nav navbar-nav">
				<li class="active">
					<button
						style="margin: 0px; padding: 3px; height: 35px; width: 35px;"
						class="navbar-toggle collapse in no-radius" data-toggle="collapse"
						id="menu-toggle-2" data-ng-click="setCollapse()">
						<i data-ng-show="!collapsedMenu"
							class="fa fa-outdent fa-stack-1x hamburger"></i> <i
							data-ng-show="collapsedMenu"
							class="fa fa-outdent fa-rotate-180 fa-stack-1x hamburger"></i>
					</button>
				</li>
			</ul>
			<div class="col-xs-6 pull-right height-inherit no-padding">
				<div class="col-xs-5 no-padding">
					<div class="col-xs-6 height-inherit no-padding filter-date">
						<%--<label class="select-label height-inherit no-padding col-xs-6" style="margin-top: 0px">--%>
						<%--<select id="workLog" data-ng-model="workLog" data-ng-change="selectWorkLog(workLog)">--%>
						<%--<option value="MY">My workLog</option>--%>
						<%--<option value="ALL">All Record</option>--%>
						<%--</select>--%>
						<%--</label>--%>
					</div>
					<div class="col-xs-6 height-inherit no-padding filter-date">
						<span><input type="text" data-ng-disabled="true"
							data-ng-model="today_date"></span>
					</div>
				</div>
				<div class="col-xs-4 height-inherit no-padding filter-search">
					<span>
						<form ng-submit="searchRequest(searchQuery)">
							<%--<input type="text" class="search" data-ng-model="searchQuery" placeholder="Search">--%>
							<div class="form-group input-group">
								<input type="text" class="form-control search"
									data-ng-model="searchQuery" placeholder="Search"> <span
									class="input-group-btn no-radius">
									<button class="btn btn-default"
										style="padding-top: 5px; background: #f4f7fa" type="submit">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
					</span>
				</div>
				<div class="col-xs-3 height-inherit no-padding user-info">
					<ul class="nav navbar-nav width-full">
						<li class="dropdown width-full"><a href="#" id="usermenu"
							class="dropdown-toggle " data-toggle="dropdown"
							style="background-color: #ffffff !important;"> <span
								class="pull-right"> <span class="col-xs-2"> <img
										ng-src="<c:url value='/images/'/>{{$root.user.profile_pic}}">
								</span> <span class="col-xs-9" style="margin-top: -4px;"> <%
                                              String userName = ((UserMaster) session.getAttribute("currentUser")).getUserName();
                                              String customerName = ((CustomerMaster) session.getAttribute("account")).getCustomerName();
                                          %> <span class="col-xs-12"
										style="height: 15px;"><%= userName %></span> <span
										class="col-xs-12" style="height: 15px;"><%= customerName %></span>
								</span>


							</span>
						</a>
							<ul class="dropdown-menu width-full">
								<li><a data-ng-click="resetPasswordPopUp()"
									style="cursor: pointer">Change Password <span
										class="glyphicon glyphicon-cog pull-right"></span></a></li>
								<li class="divider" style="margin: 0px"></li>
								<li class="divider" style="margin: 0px"></li>
								<li><a href="logout">Logout<span
										class="glyphicon glyphicon-log-out pull-right"></span></a></li>
							</ul></li>
					</ul>

				</div>
			</div>
		</div>
	</nav>
	<div id="wrapper" style="height: 100%; width: 100%">
		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<nav class="navbar navbar-default no-margin"
				style="background-color: white; border-color: white; height: 20px;">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header fixed-brand">
					<a class="navbar-brand" href="#"
						style="color: #337ab7; padding: 5px 60px; padding-top: 0px;">
						<span class="login-header"
						style="font-size: 25px; font-weight: normal;"> <span
							class="insta">Total</span><span class="kyc">KYC</span>
					</span>
					</a>
				</div>
				<!-- navbar-header-->
			</nav>
			<ul class="sidebar-nav nav-pills nav-stacked" id="menu">

				<li ng-class="{'active':$root.active_li=='sysAdminDashboardNew'}">
					<a href="sysadmin?navigate_to=sysAdminDashboardNew" target="_self"><span
						class="fa-stack fa-lg pull-left"><i
							class="fa fa-home fa-stack-1x "></i></span> Home</a>
				</li>
				<li
					ng-class="{'active':$root.active_li=='sysAdminCustomerManagement'}">
					<a href="sysadmin?navigate_to=sysAdminCustomerManagement"
					target="_self"><span class="fa-stack fa-lg pull-left"><i
							class="fa fa-university fa-stack-1x "></i></span> Manage Customer</span></a>
				</li>
				<li ng-class="{active:$root.active_li=='sysAdminMis'}"><a
					href="sysadmin?navigate_to=sysAdminMis" target="_self"><span
						class="fa-stack fa-lg pull-left"><i
							class="fa fa-file-text-o fa-stack-1x "></i></span> MIS</a></li>
							
				<li ng-class="{'active':$root.active_li=='sysAdminAPIMis'}">
					<a href="sysadmin?navigate_to=sysAdminAPIMis"
					target="_self"><span class="fa-stack fa-lg pull-left">
					<i class="fa fa-file-text-o fa-stack-1x "></i></span>API MIS</span></a>
				</li>
							
				<%
                String userRole = ((UserMaster) session.getAttribute("currentUser")).getUserRole();
                if (StringUtils.equalsIgnoreCase(userRole, "admin")) {
            %>
				<%--<li ng-class="{active:$root.active_li=='users'}">--%>
				<%--<a href="simple_sidebar_menu?navigate_to=users" target="_self"><span class="fa-stack fa-lg pull-left"><i class="fa fa-users fa-stack-1x "></i></span> Users <span class="pull-right"><span class="menu-count"><span data-ng-show="$root.data_count.user_count<=9999">{{$root.data_count.user_count}}</span><span data-ng-show="$root.data_count.user_count>9999">9999+</span></span></span></a>--%>
				<%--</li>--%>
				<%--<li ng-class="{active:$root.active_li=='branches'}">--%>
				<%--<a href="simple_sidebar_menu?navigate_to=branches" target="_self"><span class="fa-stack fa-lg pull-left"><i class="fa fa-university fa-stack-1x "></i></span> Branches <span class="pull-right"><span class="menu-count"><span data-ng-show="$root.data_count.branch_count<=9999">{{$root.data_count.branch_count}}</span><span data-ng-show="$root.data_count.branch_count>9999">9999+</span></span></span></a>--%>
				<%--</li>--%>
				<%--<li ng-class="{active:$root.active_li=='locations'}">--%>
				<%--<a href="simple_sidebar_menu?navigate_to=locations" target="_self"><span class="fa-stack fa-lg pull-left"><i class="fa fa-location-arrow fa-stack-1x "></i></span>Locations <span class="pull-right"><span class="menu-count"><span data-ng-show="$root.data_count.location_count<=9999">{{$root.data_count.location_count}}</span><span data-ng-show="$root.data_count.location_count>9999">9999+</span></span></span></a>--%>
				<%--</li>--%>
				<li
					ng-class="{active:$root.active_li=='adminPanelNew' || $root.active_li=='userManagementNew' || $root.active_li=='masterManagementNew'}">
					<a href="simple_sidebar_menu?navigate_to=adminPanelNew"
					target="_self"><span class="fa-stack fa-lg pull-left"><i
							class="fa fa-user fa-stack-1x "></i></span>Admin Panel </a>
				</li>
				<ol
					data-ng-show="$root.active_li=='adminPanelNew' || $root.active_li=='userManagementNew' || $root.active_li=='masterManagementNew'"
					style="display: block; list-style-type: circle;">
					<li
						ng-class="{'sub-menu-active':$root.active_li=='userManagementNew'}">
						<a href="simple_sidebar_menu?navigate_to=userManagementNew"
						target="_self">User Management </a>
					</li>
					<li
						ng-class="{'sub-menu-active':$root.active_li=='masterManagementNew'}">
						<a href="simple_sidebar_menu?navigate_to=masterManagementNew"
						target="_self">Manage Masters </a>
					</li>

				</ol>
				<%
                }

            %>
			</ul>
		</div>
		<!-- /#sidebar-wrapper -->
		<!-- Page Content -->
		<div id="page-content-wrapper" style="height: 100%; padding: 0px">
			<div class="container-fluid xyz" style="height: 100%; padding: 0px">
				<iframe name="frame" src="{{navigate_to}}"
					style="border: none; height: 100% !important; width: 100%; margin-bottom: -5px; padding-bottom: 15px;"></iframe>
			</div>
		</div>

		<!-- /#page-content-wrapper -->

	</div>

	<div id="spinnerDiv" style="display: none;">
		<img src="<c:url value='/images/spinner.gif'/>" class="ajaxloader" />
	</div>
	<jsp:include page="dialogs/pagination.jsp" />
	<jsp:include page="dialogs/search.jsp" />
	<jsp:include page="dialogs/changePassword.jsp" />
	<jsp:include page="dialogs/message.jsp" />
	<!-- /#wrapper -->
	<!-- jQuery -->
	<script src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
	<script src="<c:url value='/js/sidebar_menu.js'/>"></script>
</body>

</html>
