<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>">
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/locationController.js'/>"></script>
    <style>
        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }

        .wrap-value {
            width: 250px;
        }
    </style>
</head>
<body data-ng-controller="locationController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Locations</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background">
        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
            <div class="col-xs-2 media-width" style="padding-right: 0px">
                <div class="col-xs-12" style="padding-right: 0px">
                    <div class="row col-xs-12 no-padding add-new-button">
                        <button type="button" id="newLocation" class="btn btn-group-justified button submit-button"
                                data-ng-click="crudLocationPopup('new')"> Add Location
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 no-padding media-width">
                <div class="col-xs-6 no-padding">
                </div>
                <div class="col-xs-6 no-padding">
                </div>
            </div>
            <div class="col-xs-5 no-padding media-width">
                <div class="col-xs-5 media-width ">
                </div>

                <div class="col-xs-7 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">pagination</span>
                    </div>
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <dir-pagination-controls boundary-links="true"
                                                     on-page-change="pageChangeHandler(newPageNumber)"
                                                     template-url="dirPagination.tpl.html"></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th>
                        <a href="" ng-click="orderByField='locationOrZone'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='locationOrZone';">
                            Location Name
                            <span ng-show="orderByField == 'locationOrZone'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='linkedBranches'; reverseSort = !reverseSort">
                            Linked Branches
                            <span ng-show="orderByField == 'linkedBranches'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"> </span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"> </span>
                        </span>
                        </a>
                    </th>
                    <th>Edit</th>
                </Tr>
                <Tr dir-paginate="data in location_data | orderBy:orderByField:reverseSort | itemsPerPage: pageSize"
                    current-page="currentPage">
                    <td><span class="wrap-value"><a href="" data-ng-click="crudLocationPopup('view',data)">{{data.locationOrZone}}</a></span>
                    </td>
                    <td><span class="wrap-value"> {{data.linkedBranches}}</span></td>
                    <td>
                        <a href="" data-ng-click="crudLocationPopup('edit',data)">
                            <span class="fa-stack fa-lg"><i class="fa fa-pencil-square-o"></i></span>
                        </a>
                    </td>
                </Tr>
            </Table>
        </div>
    </div>
</div>
<jsp:include page="dialogs/crudLocationPopup.jsp"/>
<jsp:include page="dialogs/message.jsp"/>
<jsp:include page="dialogs/pagination.jsp"/>
</body>
</html>