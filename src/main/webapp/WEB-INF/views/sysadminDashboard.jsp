<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en" ng-app="instaKYC">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>

    <link href="<c:url value='/css/common.css'/>" rel="stylesheet"/>

    <!-- Bootstrap Core CSS -->
    <link href="<c:url value='/css/bootstrap.template.min.css'/>" rel="stylesheet"/>

    <!-- Custom CSS -->
    <link href="<c:url value='/css/sb-admin.css'/>" rel="stylesheet"/>

    <link href="<c:url value='/css/admin-project_style.css'/>" rel="stylesheet"/>

    <!-- Custom Fonts -->
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<c:url value='/js/static/jquery.js'/>"></script>

    <script src="<c:url value='/js/static/jquery-1.9.1.min.js'/>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<c:url value='/js/static/bootstrap.min.js'/>"></script>

    <script src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script src="<c:url value='/js/app.js'/>"></script>

    <script src="<c:url value='/js/controllers/sysadminDashBoardController.js' />"></script>

    <style>
        .active {
            background-color: #666666 !important;
        }
    </style>

</head>

<body>

<div class="row-fluid" ng-controller="sysadminDashBoardController">
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${user.name}
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav" id="leftNavigation">
                    <li id="customerClick">
                        <a href="#" ng-click='showCustomers()'><i class="fa fa-fw fa-dashboard"></i> Customers</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- /.row -->
                <div class="row" id="Account">
                    <div>
                        <jsp:include page="dialogs/customersDialogs.jsp"/>
                        <jsp:include page="dialogs/usersDialogs.jsp"/>

                        <div style="width:28%;float:left">
                            <div class="panel panel-default" style="margin-left:2%">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Customers&nbsp;&nbsp; <a title="Create Customer"
                                                                                     data-toggle="modal"
                                                                                     data-target="#addCustomersModal"
                                                                                     ng-click="resetCustomer();"
                                                                                     data-toggle="modal"
                                                                                     class="btn btn-inverse glyphicon glyphicon-plus"
                                                                                     style="float: right;  margin-top: -10px;"></a>
                                    </h3>
                                </div>
                                <div class="panel-body" style="padding: 6px;">
                                    <div class="panel-body" style="padding: 0px;">
                                        <div ng-repeat="customer in page.customers" class="list-group" ng-model="source"
                                             style="margin-bottom:10px">
                                            <a data-toggle="modal" class="customerClass list-group-item filter"
                                               ng-click="selectCustomer(customer)"
                                               ng-class="{'active':selectedCustomer==customer}">
                                                {{customer.customerName}}
                                                <span style="float: right; color: #222">
                                                <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                        aria-label="Left Align" data-toggle="modal"
                                                        data-target="#changeDomainModal"
                                                        ng-click="domainChangePopup(customer);">
                                                    <span class="fa fa-at" aria-hidden="true"></span>
                                                </button>
                                                    <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                            aria-label="Left Align" data-toggle="modal"
                                                            data-target="#viewCustomersModal"
                                                            ng-click="selectCustomer(customer);">
                                                        <span class="glyphicon glyphicon-eye-open"
                                                              aria-hidden="true"></span>
                                                    </button>
                                                    <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                            aria-label="Left Align" data-toggle="modal"
                                                            data-target="#updateCustomersModal"
                                                            ng-click="selectCustomer(customer);">
                                                        <span class="glyphicon glyphicon-edit"
                                                              aria-hidden="true"></span>
                                                    </button>
                                                    <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                            aria-label="Left Align" data-toggle="modal"
                                                            data-target="#deleteCustomersModal"
                                                            ng-click="selectCustomer(customer);">
                                                        <span class="glyphicon glyphicon-trash"
                                                              aria-hidden="true"></span>
                                                    </button>
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width:28%;float:left">
                            <div class="panel panel-default" style="margin-left:2%">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Users&nbsp;&nbsp; <a title="Create Users"
                                                                                 data-toggle="modal"
                                                                                 data-target="#addUsersModal"
                                                                                 ng-click="resetUser();"
                                                                                 data-toggle="modal"
                                                                                 class="btn btn-inverse glyphicon glyphicon-plus"
                                                                                 style="float: right;  margin-top: -10px;"></a>
                                    </h3>
                                </div>
                                <div class="panel-body" style="padding: 6px;">
                                    <div class="panel-body" style="padding: 0px;">
                                        <div ng-repeat="user in page.users" class="list-group" ng-model="source"
                                             style="margin-bottom:10px">
                                            <a data-toggle="modal" class="accountUserClass list-group-item filter">
                                                {{user.userName}}
                                                <span style="float: right;">
                                                    <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                            aria-label="Left Align" data-toggle="modal"
                                                            data-target="#viewUsersModal" ng-click="selectUser(user);">
                                                        <span class="glyphicon glyphicon-eye-open"
                                                              aria-hidden="true"></span>
                                                    </button>
                                                    <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                            aria-label="Left Align" data-toggle="modal"
                                                            data-target="#updateUsersModal"
                                                            ng-click="selectUser(user);">
                                                        <span class="glyphicon glyphicon-edit"
                                                              aria-hidden="true"></span>
                                                    </button>
                                                    <button style="margin-top: -3%" type="button" class="btn btn-sm"
                                                            aria-label="Left Align" data-toggle="modal"
                                                            data-target="#deleteUsersModal"
                                                            ng-click="selectUser(user);">
                                                        <span class="glyphicon glyphicon-trash"
                                                              aria-hidden="true"></span>
                                                    </button>
                                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /#page-wrapper -->
    </div>
</div>
<div id="spinnerDiv">
    <img src="<c:url value='/images/spinner.gif'/>" class="ajaxloader"/>
</div>

</body>

<script type="text/ng-template" id="message.html">
    <div class="modal-header text-center">
        <a class="close" data-ng-click="close()">×</a>
        <h3 class="modal-title">{{message_heading}}</h3>
    </div>
    <div class="modal-body text-center">
        {{message}}
    </div>
    <div class="modal-footer text-center">
        <button class="btn btn-block button view-historical-button submit-button" data-ng-click="close()">Ok</button>
    </div>
</script>

</html>