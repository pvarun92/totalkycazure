<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html data-ng-app="instaKYC">
<head lang="en">
    <meta charset="UTF-8">
    <title></title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/bootstrap.min.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/common.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/select.css'/>"/>
    <link href="<c:url value='/font-awesome-4.3.0/css/font-awesome.min.css'/>" rel="stylesheet">

    <script type="application/javascript" src="<c:url value='/js/static/angular.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/jquery-2.1.4.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/bootstrap.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/ui-bootstrap-tpls-0.14.3.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/lodash.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/select_files/select.js'/>"></script>

    <script type="application/javascript" src="<c:url value='/js/static/ngMask.min.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/static/dirPagination.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/app.js'/>"></script>
    <script type="application/javascript" src="<c:url value='/js/controllers/adminDashBoardController.js'/>"></script>
    <style>
        .dropdown-menu {
            padding: 0px 0px;
            top: 25px !important;
        }

        .fa-download {
            margin-right: 10px;
        }

        .fa-eye {
            margin-left: 10px;
        }
    </style>
</head>
<body data-ng-controller="adminController" ng-cloak>
<div class="navbar navbar-default no-margin page-header-block no-radius">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-xs-6">
        <H3>Admin</H3>
    </div>
    <div class="col-xs-6">
    </div>
</div>
<div class="page-container">
    <div class="col-xs-12 no-padding white-background">
        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
            <div class="col-xs-6 no-padding media-width">
                <div class="col-xs-6 no-padding">
                    <div>
                        <div class="row col-xs-12">
                            <span class="normal-text col-xs-12 label-height">From Date</span>
                        </div>
                        <div class="row col-xs-12">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-10">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="filter.filter_from" data-ng-change="selectFromDate()"
                                      uib-datepicker-popup is-open="from_datepicker.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions"
                                      date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"></span>
                                <div class="group-3-text-boxes">
                                    <input type="text" name="from_date_day" id="from_date_day"
                                           data-ng-model="from_date.day" placeholder="DD"
                                           class="col-xs-4 first text-center no-padding input-font"
                                           size="2" maxlength="2" move-next-on-maxlength only-numeric
                                           data-ng-blur="validateFromDate()"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>
                                    <input type="text" name="from_date_month" id="from_date_month"
                                           data-ng-model="from_date.month" placeholder="MM"
                                           class="col-xs-4 second text-center no-padding input-font"
                                           size="2" maxlength="2" move-next-on-maxlength only-numeric
                                           data-ng-blur="validateFromDate()"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>
                                    <input type="text" name="from_date_year" id="from_date_year"
                                           data-ng-model="from_date.year" placeholder="YYYY"
                                           class="col-xs-4 third text-center no-padding yyyy input-font"
                                           size="4" maxlength="4" move-next-on-maxlength only-numeric
                                           data-ng-blur="validateFromDate()"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_from]"/>
                                </div>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showFromCalendar()"
                                                        style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                            </div>
                            </p>
                        </div>
                        <div class="col-xs-12" data-ng-show="invalid_filter.filter_from">
                                <span class="error-message">
                                From date cannot be the future date.
                            </span>
                        </div>
                        <div class="col-xs-12" data-ng-show="filter.invalid.filter_from">
                            <span class="error-message no-border">Invalid Date</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                    <div>
                        <div class="row col-xs-12 no-padding">
                            <span class="normal-text col-xs-12 label-height">To Date</span>
                        </div>
                        <div class="row col-xs-12 no-padding">
                            <p class="input-group datepicker-block" data-ng-hide="true">
                            <div class="col-xs-10">
                                <span data-ng-hide="true" class="form-control datepicker-text"
                                      data-ng-model="filter.filter_to" data-ng-change="selectToDate()"
                                      uib-datepicker-popup is-open="to_datepicker.opened" min-date="minDate"
                                      max-date="maxDate" datepicker-options="dateOptions"
                                      date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"></span>
                                <div class="group-3-text-boxes">
                                    <input type="text" name="to_date_day" id="to_date_day" data-ng-model="to_date.day"
                                           placeholder="DD" class="col-xs-4 first text-center no-padding input-font"
                                           size="2" maxlength="2" move-next-on-maxlength only-numeric
                                           data-ng-blur="validateToDate()"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"/>
                                    <input type="text" name="to_date_month" id="to_date_month"
                                           data-ng-model="to_date.month" placeholder="MM"
                                           class="col-xs-4 second text-center no-padding input-font"
                                           size="2" maxlength="2" move-next-on-maxlength only-numeric
                                           data-ng-blur="validateToDate()"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"/>
                                    <input type="text" name="to_date_year" id="to_date_year"
                                           data-ng-model="to_date.year" placeholder="YYYY"
                                           class="col-xs-4 third text-center no-padding yyyy input-font"
                                           size="4" maxlength="4" move-next-on-maxlength only-numeric
                                           data-ng-blur="validateToDate()"
                                           ng-class="{true:'invalid-field',false:''}[filter.invalid.filter_to]"/>
                                </div>
                            </div>
                            <div class="col-xs-2 no-padding text-center">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        ng-click="showToCalendar()" style="padding: 0px;border: none;">
                                                    <i class="fa fa-lg fa-calendar"></i>
                                                </button>
                                            </span>
                            </div>
                            </p>
                        </div>
                        <div class="row col-xs-12 no-padding"
                             data-ng-show="invalid_filter.filter_to || invalid_filter.filter_to_less_than_from">
                            <span class="error-message" data-ng-show="invalid_filter.filter_to">
                                    To date cannot be greater than today's date
                                </span>
                            <span class="error-message" data-ng-show="invalid_filter.filter_to_less_than_from">
                                    To date cannot be less than from date
                                </span>
                        </div>
                        <div class="col-xs-12 no-padding" data-ng-show="filter.invalid.filter_to">
                            <span class="error-message no-border">Invalid Date</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 no-padding media-width">
                <div class="col-xs-7 media-width ">
                    <div class="col-xs-6">
                        <div class="row col-xs-12 no-padding">
                            <span class="normal-text label-height no-padding">Filter By Branch</span>
                        </div>
                        <div class="row col-xs-12 no-padding">
                            <label class="select-label" style="margin-top: 0px">
                                <select name="filter_branch" id="filter_branch" class="input-field"
                                        data-ng-model="filter.branch">
                                    <option value="">Select</option>
                                    <option value="Worli">Worli</option>
                                    <option value="Chembur">Chembur</option>
                                    <option value="Lower Parel">Lower Parel</option>
                                    <option value="Churchgate">Churchgate</option>
                                    <option value="Dadar">Dadar</option>
                                </select>
                            </label>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="row col-xs-12 no-padding">
                            <span class="normal-text label-height no-padding">View</span>
                        </div>
                        <div class="row col-xs-12 no-padding">
                            <label class="select-label" style="margin-top: 0px">
                                <select name="filter_view" id="filter_view" class="input-field"
                                        data-ng-model="filter.view">
                                    <option value="">Select</option>
                                    <option value="read">Read</option>
                                    <option value="unread">Unread</option>
                                    <option value="all">All</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-xs-5 no-padding media-width">
                    <div class="row col-xs-12 no-padding">
                        <span class="normal-text label-height no-padding" style="color: transparent">pagination</span>
                    </div>
                    <div class="row col-xs-12 no-padding">
                        <div class="pull-right">
                            <!--<pagination total-items="totalItems"  ng-model="currentPage" ng-change="pageChanged(currentPage)" items-per-page="5"></pagination>-->
                            <uib-pagination total-items="totalItems" ng-model="currentPage"
                                            ng-change="pageChanged(currentPage)" max-size="maxSize"
                                            class="pagination-sm" boundary-links="true"
                                            force-ellipses="true"></uib-pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding text-center">
            <Table class="table activity text-left dashboard-table">
                <Tr class="gray-header">
                    <th style="width: 10%;">
                        <input type="checkbox" name="check_all" id="check_all" data-ng-model="check_all"
                               data-ng-change="toggleCheck()">
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='id'; reverseSort = !reverseSort"
                           data-ng-init="orderByField='id';">
                            Report ID
                            <span ng-show="orderByField == 'id'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='first_name'; reverseSort = !reverseSort">
                            Entity Name
                            <span ng-show="orderByField == 'first_name'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='entity_type'; reverseSort = !reverseSort">
                            Entity Type
                            <span ng-show="orderByField == 'entity_type'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='branch'; reverseSort = !reverseSort">
                            Branch
                            <span ng-show="orderByField == 'branch'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='no_of_parties'; reverseSort = !reverseSort">
                            No of Parties
                            <span ng-show="orderByField == 'no_of_parties'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='created_date'; reverseSort = !reverseSort">
                            Report Date
                            <span ng-show="orderByField == 'created_date'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>
                        <a href="" ng-click="orderByField='validity'; reverseSort = !reverseSort">
                            Validity
                            <span ng-show="orderByField == 'validity'">
                           <span ng-show="!reverseSort"><img src="<c:url value='/images/up_sort_arrow.png'/>"></span>
                            <span ng-show="reverseSort"><img src="<c:url value='/images/down_sort_arrow.png'/>"></span>
                        </span>
                        </a>
                    </th>
                    <th>Download/View</th>
                </Tr>
                <Tr data-ng-repeat="data in filtered_history_data | orderBy:orderByField:reverseSort">
                    <td style="width: 10%;">
                        <input type="checkbox" name="passport_check" id="passport_check" data-ng-model="data.checked"
                               data-ng-change="checkForAll()">
                    </td>
                    <td><a href=""> {{data.id}} </a></td>
                    <td>
                        <span>{{showName(data)}}</span>
                    </td>
                    <td> {{data.entity_type}}</td>
                    <td> {{data.branch}}</td>
                    <td><span data-ng-if="!data.no_of_parties">0</span> {{data.no_of_parties}}</td>
                    <td> {{data.created_date | date: 'dd-MM-yyyy' }}</td>
                    <td> {{data.validity}}</td>
                    <td>
                        <a data-ng-click="downloadMe(data.request_id)"><i class="fa fa-lg fa-download"></i></a>
                        <a href="simple_sidebar_menu?navigate_to=kyc_validation_report?id={{data.request_id}}"
                           target="_parent"><i class="fa fa-lg fa-eye"></i></a>
                    </td>
                </Tr>
            </Table>
        </div>

        <!--</div>-->
    </div>
</div>
</body>
</html>