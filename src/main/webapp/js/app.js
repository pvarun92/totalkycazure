/**
 * Created by Fallon Software on 2/19/2016.
 */
var directiveModule = angular.module('angularjs-dropdown-multiselect', []);

directiveModule.directive('ngDropdownMultiselect', ['$filter', '$document', '$compile', '$parse',

    function ($filter, $document, $compile, $parse) {

        return {
            restrict: 'AE',
            scope: {
                selectedModel: '=',
                options: '=',
                extraSettings: '=',
                events: '=',
                searchFilter: '=?',
                translationTexts: '=',
                groupBy: '@'
            },
            template: function (element, attrs) {
                var checkboxes = attrs.checkboxes ? true : false;
                var groups = attrs.groupBy ? true : false;

                var template = '<div class="multiselect-parent btn-group dropdown-multiselect">';
                template += '<button type="button" class="dropdown-toggle" ng-class="settings.buttonClasses" ng-click="toggleDropdown()">{{getButtonText()}}&nbsp;<span class="caret"></span></button>';
                template += '<ul class="dropdown-menu dropdown-menu-form" ng-style="{display: open ? \'block\' : \'none\', height : settings.scrollable ? settings.scrollableHeight : \'auto\' }" style="overflow: scroll" >';
                template += '<li ng-hide="!settings.showCheckAll || settings.selectionLimit > 0"><a data-ng-click="selectAll()"><span class="glyphicon glyphicon-ok"></span>  {{texts.checkAll}}</a>';
                template += '<li ng-show="settings.showUncheckAll"><a data-ng-click="deselectAll();"><span class="glyphicon glyphicon-remove"></span>   {{texts.uncheckAll}}</a></li>';
                template += '<li ng-hide="(!settings.showCheckAll || settings.selectionLimit > 0) && !settings.showUncheckAll" class="divider"></li>';
                template += '<li ng-show="settings.enableSearch"><div class="dropdown-header"><input type="text" class="form-control" style="width: 100%;" ng-model="searchFilter" placeholder="{{texts.searchPlaceholder}}" /></li>';
                template += '<li ng-show="settings.enableSearch" class="divider"></li>';

                if (groups) {
                    template += '<li ng-repeat-start="option in orderedItems | filter: searchFilter" ng-show="getPropertyForObject(option, settings.groupBy) !== getPropertyForObject(orderedItems[$index - 1], settings.groupBy)" role="presentation" class="dropdown-header">{{ getGroupTitle(getPropertyForObject(option, settings.groupBy)) }}</li>';
                    template += '<li ng-repeat-end role="presentation">';
                } else {
                    template += '<li role="presentation" ng-repeat="option in options | filter: searchFilter">';
                }

                template += '<a role="menuitem" tabindex="-1" ng-click="setSelectedItem(getPropertyForObject(option,settings.idProp))">';

                if (checkboxes) {
                    template += '<div class="checkbox"><label><input class="checkboxInput" type="checkbox" ng-click="checkboxClick($event, getPropertyForObject(option,settings.idProp))" ng-checked="isChecked(getPropertyForObject(option,settings.idProp))" /> {{getPropertyForObject(option, settings.displayProp)}}</label></div></a>';
                } else {
                    template += '<span data-ng-class="{\'glyphicon glyphicon-ok\': isChecked(getPropertyForObject(option,settings.idProp))}"></span> {{getPropertyForObject(option, settings.displayProp)}}</a>';
                }

                template += '</li>';

                template += '<li class="divider" ng-show="settings.selectionLimit > 1"></li>';
                template += '<li role="presentation" ng-show="settings.selectionLimit > 1"><a role="menuitem">{{selectedModel.length}} {{texts.selectionOf}} {{settings.selectionLimit}} {{texts.selectionCount}}</a></li>';

                template += '</ul>';
                template += '</div>';

                element.html(template);
            },
            link: function ($scope, $element, $attrs) {
                var $dropdownTrigger = $element.children()[0];

                $scope.toggleDropdown = function () {
                    $scope.open = !$scope.open;
                };

                $scope.checkboxClick = function ($event, id) {
                    $scope.setSelectedItem(id);
                    $event.stopImmediatePropagation();
                };

                $scope.externalEvents = {
                    onItemSelect: angular.noop,
                    onItemDeselect: angular.noop,
                    onSelectAll: angular.noop,
                    onDeselectAll: angular.noop,
                    onInitDone: angular.noop,
                    onMaxSelectionReached: angular.noop
                };

                $scope.settings = {
                    dynamicTitle: true,
                    scrollable: false,
                    scrollableHeight: '300px',
                    closeOnBlur: true,
                    displayProp: 'label',
                    idProp: 'id',
                    externalIdProp: 'id',
                    enableSearch: true,
                    selectionLimit: 0,
                    showCheckAll: true,
                    showUncheckAll: true,
                    closeOnSelect: false,
                    buttonClasses: 'btn btn-default',
                    closeOnDeselect: false,
                    groupBy: $attrs.groupBy || undefined,
                    groupByTextProvider: null,
                    smartButtonMaxItems: 0,
                    smartButtonTextConverter: angular.noop
                };

                $scope.texts = {
                    checkAll: 'Check All',
                    uncheckAll: 'Uncheck All',
                    selectionCount: 'checked',
                    selectionOf: '/',
                    searchPlaceholder: 'Search...',
                    buttonDefaultText: 'Select              ',
                    dynamicButtonTextSuffix: 'checked'
                };

                $scope.searchFilter = $scope.searchFilter || '';

                if (angular.isDefined($scope.settings.groupBy)) {
                    $scope.$watch('options', function (newValue) {
                        if (angular.isDefined(newValue)) {
                            $scope.orderedItems = $filter('orderBy')(newValue, $scope.settings.groupBy);
                        }
                    });
                }

                angular.extend($scope.settings, $scope.extraSettings || []);
                angular.extend($scope.externalEvents, $scope.events || []);
                angular.extend($scope.texts, $scope.translationTexts);

                $scope.singleSelection = $scope.settings.selectionLimit === 1;

                function getFindObj(id) {
                    var findObj = {};

                    if ($scope.settings.externalIdProp === '') {
                        findObj[$scope.settings.idProp] = id;
                    } else {
                        findObj[$scope.settings.externalIdProp] = id;
                    }

                    return findObj;
                }

                function clearObject(object) {
                    for (var prop in object) {
                        delete object[prop];
                    }
                }

                if ($scope.singleSelection) {
                    if (angular.isArray($scope.selectedModel) && $scope.selectedModel.length === 0) {
                        clearObject($scope.selectedModel);
                    }
                }

                if ($scope.settings.closeOnBlur) {
                    $document.on('click', function (e) {
                        var target = e.target.parentElement;
                        var parentFound = false;

                        while (angular.isDefined(target) && target !== null && !parentFound) {
                            if (_.contains(target.className.split(' '), 'multiselect-parent') && !parentFound) {
                                if (target === $dropdownTrigger) {
                                    parentFound = true;
                                }
                            }
                            target = target.parentElement;
                        }

                        if (!parentFound) {
                            $scope.$apply(function () {
                                $scope.open = false;
                            });
                        }
                    });
                }

                $scope.getGroupTitle = function (groupValue) {
                    if ($scope.settings.groupByTextProvider !== null) {
                        return $scope.settings.groupByTextProvider(groupValue);
                    }

                    return groupValue;
                };

                $scope.getButtonText = function () {
                    if ($scope.settings.dynamicTitle && ($scope.selectedModel.length > 0 || (angular.isObject($scope.selectedModel) && _.keys($scope.selectedModel).length > 0))) {
                        if ($scope.settings.smartButtonMaxItems > 0) {
                            var itemsText = [];

                            angular.forEach($scope.options, function (optionItem) {
                                if ($scope.isChecked($scope.getPropertyForObject(optionItem, $scope.settings.idProp))) {
                                    var displayText = $scope.getPropertyForObject(optionItem, $scope.settings.displayProp);
                                    var converterResponse = $scope.settings.smartButtonTextConverter(displayText, optionItem);

                                    itemsText.push(converterResponse ? converterResponse : displayText);
                                }
                            });

                            if ($scope.selectedModel.length > $scope.settings.smartButtonMaxItems) {
                                itemsText = itemsText.slice(0, $scope.settings.smartButtonMaxItems);
                                itemsText.push('...');
                            }

                            return itemsText.join(', ');
                        } else {
                            var totalSelected;

                            if ($scope.singleSelection) {
                                totalSelected = ($scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp])) ? 1 : 0;
                            } else {
                                totalSelected = angular.isDefined($scope.selectedModel) ? $scope.selectedModel.length : 0;
                            }

                            if (totalSelected === 0) {
                                return $scope.texts.buttonDefaultText;
                            } else {
                                return totalSelected + ' ' + $scope.texts.dynamicButtonTextSuffix;
                            }
                        }
                    } else {
                        return $scope.texts.buttonDefaultText;
                    }
                };

                $scope.getPropertyForObject = function (object, property) {
                    if (angular.isDefined(object) && object.hasOwnProperty(property)) {
                        return object[property];
                    }

                    return '';
                };

                $scope.selectAll = function () {
                    $scope.deselectAll(false);
                    $scope.externalEvents.onSelectAll();

                    angular.forEach($scope.options, function (value) {
                        $scope.setSelectedItem(value[$scope.settings.idProp], true);
                    });
                };

                $scope.deselectAll = function (sendEvent) {
                    sendEvent = sendEvent || true;

                    if (sendEvent) {
                        $scope.externalEvents.onDeselectAll();
                    }

                    if ($scope.singleSelection) {
                        clearObject($scope.selectedModel);
                    } else {
                        $scope.selectedModel.splice(0, $scope.selectedModel.length);
                    }
                };

                $scope.setSelectedItem = function (id, dontRemove) {
                    var findObj = getFindObj(id);
                    var finalObj = null;

                    if ($scope.settings.externalIdProp === '') {
                        finalObj = _.find($scope.options, findObj);
                    } else {
                        finalObj = findObj;
                    }

                    if ($scope.singleSelection) {
                        clearObject($scope.selectedModel);
                        angular.extend($scope.selectedModel, finalObj);
                        $scope.externalEvents.onItemSelect(finalObj);
                        if ($scope.settings.closeOnSelect) $scope.open = false;

                        return;
                    }

                    dontRemove = dontRemove || false;

                    var exists = _.findIndex($scope.selectedModel, findObj) !== -1;

                    if (!dontRemove && exists) {
                        $scope.selectedModel.splice(_.findIndex($scope.selectedModel, findObj), 1);
                        $scope.externalEvents.onItemDeselect(findObj);
                    } else if (!exists && ($scope.settings.selectionLimit === 0 || $scope.selectedModel.length < $scope.settings.selectionLimit)) {
                        $scope.selectedModel.push(finalObj);
                        $scope.externalEvents.onItemSelect(finalObj);
                    }
                    if ($scope.settings.closeOnSelect) $scope.open = false;
                };

                $scope.isChecked = function (id) {
                    if ($scope.singleSelection) {
                        return $scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp]) && $scope.selectedModel[$scope.settings.idProp] === getFindObj(id)[$scope.settings.idProp];
                    }

                    return _.findIndex($scope.selectedModel, getFindObj(id)) !== -1;
                };

                $scope.externalEvents.onInitDone();
            }
        };
    }]);

var instaKYC = angular.module('instaKYC', ['ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngMask', 'ui.select', 'angularjs-dropdown-multiselect']);

instaKYC.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
});

instaKYC.factory('httpInterceptor', function ($q, $rootScope, $window, CommonServices) {

    var numLoadings = 0;
    return {
        request: function (config) {
            if (config.url && config.url.indexOf("automated_call=true") < 0 && config.url.indexOf("validateEmail") < 0) {
                numLoadings++;
                var print = CommonServices.getParameterByName("print");
                if (!print) {
                    if ($window.parent.document.getElementById("page-content-wrapper")) {
                        $window.parent.document.getElementById("spinnerDiv").style.display = "inline";
                    }
                    else {
                        $window.document.getElementById("spinnerDiv").style.display = "inline";
                    }
                }
            }
            return config || $q.when(config)

        },
        response: function (response) {

            if (response.config.url.indexOf("automated_call=true") < 0 && response.config.url.indexOf("validateEmail") < 0) {
                if ((--numLoadings) === 0) {
                    var print = CommonServices.getParameterByName("print");
                    if (!print) {
                        if ($window.parent.document.getElementById("page-content-wrapper")) {
                            $window.parent.document.getElementById("spinnerDiv").style.display = "none";
                        }
                        else {
                            $window.document.getElementById("spinnerDiv").style.display = "none";
                        }
                    }
                }
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if (response.config.url.indexOf("automated_call=true") < 0 && response.config.url.indexOf("validateEmail") < 0) {
                if (!(--numLoadings)) {
                    // Hide loader
                    //$rootScope.$broadcast("loader_hide");
                    var print = CommonServices.getParameterByName("print");
                    if (!print) {
                        if ($window.parent.document.getElementById("page-content-wrapper")) {
                            $window.parent.document.getElementById("spinnerDiv").style.display = "none";
                        }
                        else {
                            $window.document.getElementById("spinnerDiv").style.display = "none";
                        }
                    }
                }
            }
            return $q.reject(response);
        }
    };
});

instaKYC.directive("moveNextOnMaxlength", function () {
    return {
        restrict: "A",
        link: function ($scope, element) {
            element.on("input", function (e) {
                if (element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.next();
                    if ($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});

instaKYC.directive('onlyNumeric', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9]/g, '');
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;  // or return Number(transformedInput)
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

instaKYC.directive('isCombinationOfZeroAndSpecialCharacter', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z1-9/][^a-zA-Z1-9]+$/g, ''); 
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;  // or return Number(transformedInput)
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});


instaKYC.directive('onlyNumericWithSlash', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9/]/g, '');
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;  // or return Number(transformedInput)
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

instaKYC.directive('noSpecialChar', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == null)
                    return '';
                cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
                if (cleanInputValue != inputValue) {
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                }
                return cleanInputValue;
            });
        }
    }
});

instaKYC.directive('onlyNumericWithHyphen', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                //var transformedInput = text.replace(/[^0-9/]/g, '');
                //^[0-9 \-]+$
                var transformedInput = text.replace(/[^0-9-]/g, ''); 
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;  // or return Number(transformedInput)
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

instaKYC.directive('alphaNumericWithSlash', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
            	var transformedInput = text.replace(/[^a-zA-Z0-9/]+$/g, '');
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;  // or return Number(transformedInput)
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});



instaKYC.directive('uppercased', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (input) {
                return input ? input.toUpperCase() : "";
            });
            element.css("text-transform", "uppercase");
        }
    };
});

instaKYC.directive('alphaNumericWithSlashAndHyphen', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z0-9/-]/g, ''); 
                console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;  // or return Number(transformedInput)
            }

            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});



instaKYC.service('CommonServices', function ($filter) {

    this.getParameterByName = function (name, url) {
        if (!url) url = window.location.href;
//        url = url.toLowerCase(); // This is just to avoid case sensitiveness
        name = name.replace(/[\[\]]/g, "\\$&"); // This is just to avoid case sensitiveness for query parameter name
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

    this.getSelectedData = function (all_data) {
        var selected_data = [];
        angular.forEach(all_data, function (data) {
            if (data.checked) {
                selected_data.push(data)
            }
        });
        return selected_data;
    };

    this.toggleCheckAll = function (check_all, all_data, currentPage, pageSize, orderByField, reverseSort) {
        if (all_data) {
            if (all_data.length > 0) {
                all_data = $filter('orderBy')(all_data, orderByField, reverseSort);
                var starting_index = (currentPage * pageSize) - (pageSize);
                var ending_index = (currentPage * pageSize);
                if (check_all) {
                    for (var i = 0; i < all_data.length; i++) {
                        if (i >= starting_index && i < ending_index) {
                            all_data[i].checked = true
                        }
                    }
                }
                else {
                    for (var i = 0; i < all_data.length; i++) {
                        if (i >= starting_index && i < ending_index) {
                            all_data[i].checked = false
                        }
                    }
                }
                return all_data;
            }
            else {
                return []
            }
        }
        else {
            return []
        }
    };

    this.convertDateInArrayDDMMYYY = function (date) {
        if (date) {
            var new_date = new Date(date);
            if (new_date) {
                var dd = new_date.getDate();
                var mm = new_date.getMonth() + 1;
                var yyyy = new_date.getFullYear();
                return {
                    "day": dd,
                    "month": mm,
                    "year": yyyy
                };
            }
            else {
                return {
                    "day": "",
                    "month": "",
                    "year": ""
                }
            }
        }
        else {
            return {
                "day": "",
                "month": "",
                "year": ""
            }
        }
    };

    this.convertInDate = function (day, month, year) {
        if (day && month && year) {
            if (parseInt(day) < 10) {
                day = "0" + day;
            }
            if (parseInt(month) < 10) {
                month = "0" + month;
            }
            var date = year.toString() + "-" + month.toString() + "-" + day.toString() + "T11:51:00";
            var new_date = new Date(date);
            if (new_date) {
                return new_date;
            }
            else {
                return "";
            }
        }
        else {
            return "";
        }
    };

    this.isGreaterWithDate = function (date1, date2) {
        var greater = false;
        var old_date1 = date1;
        var old_date2 = date2;
        if (date1 && "string" == (typeof date1)) {
            date1 = this.getDateFromconvertDDMMYYYYString(old_date1)
        }
        if (date2 && "string" == (typeof date2)) {
            date2 = this.getDateFromconvertDDMMYYYYString(old_date2)
        }
        date1 = new Date(date1);
        date2 = new Date(date2);
        if (!date1 || date1 == "Invalid Date") {
            date1 = this.getDateFromconvertDDMMYYYYString(old_date1)
        }
        if (!date2 || date2 == "Invalid Date") {
            date2 = this.getDateFromconvertDDMMYYYYString(old_date2)
        }
        if (date1 && date2) {
            if (date1 != "Invalid Date" && date2 != "Invalid Date") {
                if (date1 > date2) {
                    greater = true;
                }
            }
        }
        return greater;
    };

    this.showName = function (data) {
        var name = "";
        if (data.entity_type == "Individual") {
            if (data.first_name) {
                name = name + data.first_name + " "
            }
            if (data.middle_name) {
                name = name + data.middle_name + " "
            }
            if (data.last_name) {
                name = name + data.last_name
            }
        }
        else {
            if (data.entity_name) {
                name = data.entity_name
            }
        }
        if (!name || !name.trim()) {
            name = "(Blank)"
        }
        return name;
    };

    this.isInvalidDate = function (dateJson) {
        var invalid = true;
        if (dateJson.day && dateJson.month && dateJson.year) {
            if (dateJson.day > 31 || dateJson.day < 1 || dateJson.month > 12 || dateJson.month < 1 || dateJson.year < 1000) {
                invalid = true;
            }
            else {
                invalid = false;
            }
        }
        else {
            invalid = true;
        }
        return invalid;
    };

    this.getCurrentUser = function () {
        var current_user = angular.fromJson(localStorage['current_user']);
        return current_user;
    };
    this.getCurrentUserRole = function () {
        var current_user = angular.fromJson(localStorage['current_user']);
        return current_user.userRole;
    };
    this.getCurrentWorkLog = function () {
        var workLog = angular.fromJson(localStorage['workLog']);
        return workLog;
    };
    this.getDateFromconvertDDMMYYYYString = function (DDMMYYYYString) {
        if ("string" != (typeof DDMMYYYYString)) {
            return DDMMYYYYString;
        }
        if (DDMMYYYYString) {
            var parts = DDMMYYYYString.match(/(\d+)/g);
            if (parts[2] && parts[2] > 999 && parts[1] && parts[1] < 13 && parts[0] && parts[0] < 32)
                return new Date(parts[2], parts[1] - 1, parts[0]);
            else
                return false
        }
        else {
            return false
        }

    };
    this.getSplittedDate = function (date) {
        var splittedDate = "";
        if (date && "string" == (typeof date) && date.indexOf(" ") >= 0) {
            splittedDate = date.split(" ")[0]
        }
        else {
            splittedDate = date
        }
        if (!splittedDate) {
            splittedDate = ""
        }
        return splittedDate;
    };
    this.isValidStatus = function (status_as_per_source) {
    	if(status_as_per_source==null || status_as_per_source == undefined){
    		return false
    	}
    	//Ideally, What Response is Valid or Not Should Be Given by the Corresponding APIs[@TotalKYC]
        validStatusList = ["active", "normal", "amendment", "valid", "itr-v received", "e-return has been digitally signed"];
        invalidStatusList = ["cancelled", "vat/cst tin number is incorrect", "inactive", "m v a t rc no should be number",
            "m v a t rc no should end with 'v'or'c'or'p'", "m v a t rc no is invalid number", "invalid charectors should not be entered",
            "m v a t rc no should begin with 27 or 99", "service tax assessee code is incorrect", "service tax assessee code is incorrect",
            "no records available", "cin/fcrn is invalid.", "cin/fcrn number is incorrect.", "strike off", "liquidated", "dissolved",
            "converted to llp and dissolved", "iec code is invalid", "no record found", "black listed", "suspended", "cancelled", "iec code is not proper",
            "iec code or applicant name is incorrect.", "iec code or applicant name is incorrect.", "iec code or applicant name is incorrect.",
            "no data available for The given iec", "defunct", "llpin/fllpin number is invalid", "no record found with the given dl number",
            "invalid pan", "pan does not exist.", "itr-v not received", "no e-return has been filed.", "invalid acknowlwdgement number",
            "invalid pan. itr-v can not be verified based on pan.", "assessment year is invalid. itr-v can not be verified based on assessment year.",
            "no record found", "invalid voter id number", "No Record found with the given DL number", "records not found", "invalid aadhaar number",
            "invalid cin/fcrn number", "invalid llpin/fllpin number", "invalid service tax assessee code", "invalid excise tax assessee code",
            "vat number is incorrect", "cst number is incorrect", "professional tax number is incorrect"];
        partialValidStatusList = ["pending", "rejected", "converted to llp", "dormant", "active in progress", "under liquidation", "under process of striking off",
            "amalgamated", "not available for e-filing", "to be migrated", "captured", "dormant under section 455 of companies act, 2013",
            "clear from black list", "revoke suspension", "revoke cancellation", "under process of strike off", "multiple records found", "pending()"];


        if (!invalidStatusList.indexOf(status_as_per_source.toLowerCase()) != -1 && !partialValidStatusList.indexOf(status_as_per_source.toLowerCase()) != -1 && validStatusList.indexOf(status_as_per_source.toLowerCase()) != -1) {
            return true;
        }
        else if (!invalidStatusList.indexOf(status_as_per_source.toLowerCase()) != -1 && partialValidStatusList.indexOf(status_as_per_source.toLowerCase()) != -1) {
            return false;
        }
        else if (invalidStatusList.indexOf(status_as_per_source.toLowerCase()) != -1) {
            return false;
        }
        else if ((status_as_per_source.toLowerCase().indexOf("active") != -1 || status_as_per_source.toUpperCase().indexOf("NORMAL") != -1
            || status_as_per_source.toUpperCase().indexOf("AMENDMENT") != -1 || status_as_per_source.toLowerCase().indexOf("valid") != -1
            || status_as_per_source.toLowerCase().indexOf("bar code string generated") != -1)
            && status_as_per_source.toLowerCase().indexOf("enter") == -1
            && !(status_as_per_source.toLowerCase().indexOf("invalid") != -1)
            && !(status_as_per_source.toLowerCase().indexOf("inactive") != -1)) {
            return true;
        }
        else {
            return false;
        }
    }
});

instaKYC.factory('dataFactory', function ($http, $q) {
    return {
        getCurrentUser: function () {
            var deferred = $q.defer();
            $http.get("getCurrentUser")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        getCustomerProfileData: function (customerId) {
            var deferred = $q.defer();
            $http.post("sysadmin/getCustomerProfileData", customerId)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getCustomerData: function (customerId) {
            var deferred = $q.defer();
            $http.post("sysadmin/getCustomerData", customerId)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getUserProfileData: function (customerId) {
            var deferred = $q.defer();
            $http.post("sysadmin/getUserProfileData", customerId)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getLocationByCustomerId: function (customerId) {
            var deferred = $q.defer();
            $http.post("sysadmin/getAllLocationsByCustomerMasterId", customerId)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        getBranchesByCustomerId: function (customerId) {
            var deferred = $q.defer();
            $http.post("sysadmin/getAllBranchesByCustomerMasterId", customerId)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        getCurrentWorkLog: function () {
            var deferred = $q.defer();
            $http.get("getCurrentWorkLog")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        setCurrentWorkLog: function (workLog) {
            var deferred = $q.defer();
            $http.post("setCurrentWorkLog", workLog)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        getLandingData: function () {
            var deferred = $q.defer();
            $http.get("landingData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getLandingDataAuto: function () {
            var deferred = $q.defer();
            $http.get("landingData?automated_call=true")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getDraftsData: function () {
            var deferred = $q.defer();
            $http.get("draftsData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getHistoryData: function () {
            var deferred = $q.defer();
            $http.get("historyData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getMisData: function () {
            var deferred = $q.defer();
            $http.get("misData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getQueueData: function () {
            var deferred = $q.defer();
            $http.get("queueData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getQueueDataAuto: function () {
            var deferred = $q.defer();
            $http.get("queueData?automated_call=true")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getEntityDataById: function (id) {
            var deferred = $q.defer();
            var data = {id: id};
            $http.post("entityData", data)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        getDataCount: function () {
            var deferred = $q.defer();
            $http.get("dataCount")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

  /*      getDataCountAuto: function () {
            var deferred = $q.defer();
            $http.get("dataCount?automated_call=true")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },*/
        
        getDataCountAuto: function () {
            var deferred = $q.defer();
            $http.get("dataCount")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        saveRequest: function (request_data) {
            var deferred = $q.defer();
            $http.post("saveRequest", request_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        }/*,

        saveRequestAuto: function (request_data) {
            var deferred = $q.defer();
            $http.post("saveRequest?automated_call=true", request_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        }*/,

        submitRequest: function (request_data) {
        	console.debug("Submitting Request Data for KYC : "+request_data)
            var deferred = $q.defer();
            $http.post("submitRequest", request_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        validateDocuments: function (request_data) {
            var deferred = $q.defer();
            $http.post("validateDocuments", request_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        validateItrDocuments: function (request_data) {
            var deferred = $q.defer();
            $http.post("validateItrDocuments", request_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        getKycReport: function (id) {
            var deferred = $q.defer();
            var data = {id: id};
            $http.post("getKycReport", data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        getKycReportPrint: function (id) {
            var deferred = $q.defer();
            var data = {id: id};
            $http.post("getKycReportPrint", data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        getKycReportByEntity: function (id) {
            var deferred = $q.defer();
            var data = {id: id};
            $http.post("getKycReportByEntity", data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        getCaptcha: function (document_without_captcha_in_form) {
            var deferred = $q.defer();
            $http.post("getCaptcha", document_without_captcha_in_form)
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        createCustomer: function (customer_master) {
            var deferred = $q.defer();
            $http.post("sysadmin/createCustomer", JSON.stringify(customer_master))
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        /*Method to return the Customers*/
        showCustomers: function () {
            var deferred = $q.defer();
            $http.get("sysadmin/showCustomers")
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("To err is Humane from customer");
                    deferred.resolve
                });
            return deferred.promise;
        },
        /*Method to return the Users*/
        showUsers: function () {
            var deferred = $q.defer();
            $http.get("showUsers")
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("To err is Human");
                    deferred.resolve
                });
            return deferred.promise;
        },

        showCustomer: function () {
            var deferred = $q.defer();
            $http.get("sysadmin/showCustomer")
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        showCustomerNew: function () {
            var deferred = $q.defer();
            $http.get("sysadmin/showCustomerNew")
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        updateCustomer: function (customer_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/updateCustomer", customer_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        deleteCustomer: function (customer_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/deleteCustomer", customer_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        createCustomerUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/createCustomerUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        createCustomerUserNew: function (user_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/createCustomerUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        getCustomerUsers: function (customerMasterId) {
            var deferred = $q.defer();
            var data = {customerMasterId: customerMasterId};
            $http.post("sysadmin/getCustomerUsers", data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        getCustomerUsersNew: function (customerMasterId) {
            var deferred = $q.defer();
            var data = {customerMasterId: customerMasterId};
            $http.post("sysadmin/getCustomerUsers", data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        updateCustomerUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/updateCustomerUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        updateCustomerUserNew: function (user_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/updateCustomerUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        deleteCustomerUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/deleteCustomerUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        deleteCustomerUserNew: function (user_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/deleteCustomerUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        deleteUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("deleteUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        deleteBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("deleteBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        deleteLocation: function (location_data) {
            var deferred = $q.defer();
            $http.post("deleteLocation", location_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        getBranchesData: function () {
            var deferred = $q.defer();
            $http.get("branchesData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        branchesbyUser: function () {
            var deferred = $q.defer();
            $http.get("branchesbyUser")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },

        /*Method to fetch all the Elctricity Board Names*/
        allElectricityBoard : function(){
        	var deferred = $q.defer();
        	$http.get("allElectricityBoard")
        	.success(deferred.resolve)
        	.error(function (e){
        		console.log("error from Electricity Board");
        		deferred.resolve
        	});
        	return deferred.promise;
        },
        
        /*method written to get the data for branches*/
        allBranches: function () {
            var deferred = $q.defer();
            $http.get("allBranches")
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        createBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("createBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        sysAdminCreateBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("sysAdminCreateBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        sysAdminCreateBranchNew: function (branch_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/sysAdminCreateBranchNew", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        updateBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("updateBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        sysUpdateBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/sysUpdateBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        getLocationsData: function () {
            var deferred = $q.defer();
            $http.get("locationsData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        createLocation: function (location_data) {
            var deferred = $q.defer();
            $http.post("createLocation", location_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        sysAdminCreateLocation: function (location_data) {
            var deferred = $q.defer();
            $http.post("sysAdminCreateLocation", location_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        sysAdminCreateLocationNew: function (location_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/sysAdminCreateLocationNew", location_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        updateLocation: function (location_data) {
            var deferred = $q.defer();
            $http.post("updateLocation", location_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        sysAdminUpdateLocation: function (location_data) {
            var deferred = $q.defer();
            $http.post("sysadmin/sysAdminUpdateLocation", location_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        /*Get the Users Specific to the Customers*/
        getUsersOfCustomer : function(customer_name){
        	var deferred = $q.defer();
        	$http.post("sysadmin/getUsersOfCustomer",customer_name)
        	.success(deferred.resolve)
        	.error(function(e){
        		console.debug("Fetching the Users Based on Customer Selection");
        		deferred.resolve
        	});
        	return deferred.promise;
        },
        /*Get the Branches Specific to the Customer*/
        
        getBranchOfCustomer : function(customer_name){
        	 var deferred = $q.defer();
        	 $http.post("sysadmin/getBranchOfCustomer",customer_name)
        	 .success(deferred.resolve)
        	 .error(function(e){
        		console.debug("Fetching the Branches Based on Custome Selection "); 
        		deferred.resolve
        	 });
        	 return deferred.promise;
        },
        
        getUsersData: function () {
            var deferred = $q.defer();
            $http.get("usersData")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        getUsersDataWithAdmin: function () {
            var deferred = $q.defer();
            $http.get("usersDataWithAdmin")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        createUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("createUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        changePassword: function (user_data) {
            var deferred = $q.defer();
            $http.post("changePassword", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        updateUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("updateUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        toggleUser: function (user_data) {
            var deferred = $q.defer();
            $http.post("toggleUser", user_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        generateOTP: function (email) {
            var deferred = $q.defer();
            var data = {email: email};
            $http.post("otp/generateOTP", email)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        generateOtpPassword: function (email) {
            var deferred = $q.defer();
            var data = {email: email};
            $http.post("otp/generateOtpPassword", data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        /*method to get the data for the system user*/
        getFilteredDataAdmin : function(filter) {
            var deferred = $q.defer();
            console.debug("Filtered Values --> ",filter);
            $http.post("sysadmin/getFilteredDataAdmin", filter)
            	.success(deferred.resolve)
                .error(function (e) {
                    console.log("Error fetching data for the sys Admin");
                    deferred.resolve
                });
            return deferred.promise;
        },
        getRequestCount : function(){
        	var deferred = $q.defer();
        	$http.get("sysadmin/getRequestCount")
        	.success(deferred.resolve)
        	.error(function(e){
        		console.log("Error fetching all the Request made");
        		deferred.resolve
        	});
        	return deferred.promise;
        },
        getFilteredData: function (filter) {
            var deferred = $q.defer();
            $http.post("getFilteredData", filter)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        commonFilter: function (filter) {
            var deferred = $q.defer();
            $http.post("commonFilter", filter)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        discardDrafts: function (selectedDrafts) {
            var deferred = $q.defer();
            $http.post("discardDrafts", selectedDrafts)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        deleteEntity: function (selectedRequest) {
            var deferred = $q.defer();
            $http.post("deleteEntity", selectedRequest)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        deleteRequest: function (request_id) {
            var deferred = $q.defer();
            $http.post("deleteRequest", request_id)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        getAssessmentYears: function () {
            var deferred = $q.defer();
            $http.get("getAssessmentYears")
                .success(deferred.resolve)
                .error(deferred.resolve);
            return deferred.promise;
        },
        validateEmail: function (emailId) {
            var deferred = $q.defer();
            $http.post("validateEmail", emailId)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        changeDomain: function (selectedCustomerDomain) {
            var deferred = $q.defer();
            $http.post("changeEmailDomain", selectedCustomerDomain)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },
        mergeBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("mergeBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        sysMergeBranch: function (branch_data) {
            var deferred = $q.defer();
            $http.post("sysMergeBranch", branch_data)
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        },

        createSubscriptionPlan: function (subscription_detail) {
            var deferred = $q.defer();
            $http.post("createSubscriptionPlan", JSON.stringify(subscription_detail))
                .success(deferred.resolve)
                .error(function (e) {
                    console.log("error");
                    deferred.resolve
                });
            return deferred.promise;
        }

    }
});