instaKYC.controller('newLocationNewController', function ($scope, $uibModal, CommonServices, dataFactory) {

    localStorage.removeItem("mergeBranch");
    localStorage.removeItem("newBranch");
    $scope.userRole = CommonServices.getCurrentUserRole();

    $scope.getBranches = function () {
        dataFactory.getBranchesData()
            .then(function (data) {
                $scope.branch_data = data;
            });
    };

    $scope.getLocations = function () {
        dataFactory.getLocationsData()
            .then(function (data) {
                $scope.locations = data;
            });
    };

    $scope.currentPage = 1;
    $scope.pageSize = 10;

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.newLocation = {
        branches: []
    };

    $scope.multipleDropDownSetting = {
        scrollableHeight: '200px',
        scrollable: true,
        buttonClasses: "input-field multiselect-dropdown-style",
        displayProp: 'branchName',
        idProp: 'id'
    };

    $scope.getBranches();
    $scope.getLocations();
});