instaKYC.controller('locationController', function ($scope, $uibModal, CommonServices, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();

    $scope.getLocations = function () {
        dataFactory.getLocationsData()
            .then(function (data) {
                //console.log(data.result)
                $scope.location_data = data;
            });
    };

    $scope.getLocations();

    var current_date = new Date();
    $scope.invalid_filter = {
        filter_from: false,
        filter_to: false,
        filter_to_less_than_from: false
    };

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end

    //datepicker start
    // Disable weekend selection
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showFromCalendar = function () {
        $scope.from_datepicker.opened = true;
    };

    $scope.showToCalendar = function () {
        $scope.to_datepicker.opened = true;
    };

    $scope.from_date = {
        "day": "",
        "month": "",
        "year": ""
    };

    $scope.selectFromDate = function () {
        var converted_date = CommonServices.convertDateInArrayDDMMYYY($scope.filter.filter_from);
        if (converted_date) {
            $scope.from_date.day = converted_date.day;
            $scope.from_date.month = converted_date.month;
            $scope.from_date.year = converted_date.year
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, new Date())) {
            $scope.invalid_filter.filter_from = true;
        }
        else {
            $scope.invalid_filter.filter_from = false;
        }
    };

    $scope.to_date = {
        "day": "",
        "month": "",
        "year": ""
    };

    $scope.selectToDate = function () {
        var converted_date = CommonServices.convertDateInArrayDDMMYYY($scope.filter.filter_to);
        if (converted_date) {
            $scope.to_date.day = converted_date.day;
            $scope.to_date.month = converted_date.month;
            $scope.to_date.year = converted_date.year
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_to, new Date())) {
            $scope.invalid_filter.filter_to = true;
        }
        else {
            $scope.invalid_filter.filter_to = false;
        }

        if (CommonServices.isGreaterWithDate($scope.filter.filter_to, $scope.filter.filter_from)) {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
    };

    $scope.from_datepicker = {
        opened: false
    };

    $scope.to_datepicker = {
        opened: false
    };
    //datepicker end


    $scope.filter = {
        invalid: {}
    };
    $scope.validateFromDate = function () {
        $scope.filter.invalid.filter_from = CommonServices.isInvalidDate($scope.from_date);
    };
    $scope.validateToDate = function () {
        $scope.filter.invalid.filter_to = CommonServices.isInvalidDate($scope.to_date);
    };

    $scope.crudLocationPopup = function (popup_type, selected) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'crudLocationPopup.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($scope) {
                $scope.selected = selected;
                if (popup_type == 'new') {
                    $scope.heading = "Create new Location";
                    $scope.new = true;
                    $scope.selected = {}
                }
                else if (popup_type == 'edit') {
                    $scope.heading = "Edit Location";
                    $scope.edit = true;
                    if (selected.parentLocation) {
                        selected.parentLocation = selected.parentLocation + "";
                    }
                }
                else if (popup_type == 'view') {
                    $scope.heading = "View Location";
                    $scope.view = true;
                    if (selected.parentLocation) {
                        selected.parentLocation = selected.parentLocation + "";
                    }
                }
                $scope.close = function () {
                    $scope.getLocations();
                    showMessageModal.dismiss('cancel');
                };
                $scope.save = function (selected) {
                    dataFactory.createLocation($scope.selected)
                        .then(function (data) {
                                $scope.getLocations();
                                if (data.result == "Location Saved") {
                                    showMessageModal.dismiss('cancel');
                                    $scope.showMessagePopup("Message", "Record saved successfully");
                                }
                                else {
                                    $scope.showMessagePopup("Message", "Record not saved");
                                }
                            }
                        );
                };
                $scope.update = function (selected) {
                    dataFactory.updateLocation($scope.selected)
                        .then(function (data) {
                                $scope.getLocations();
                                if (data.result == "Location Updated") {
                                    showMessageModal.dismiss('cancel');
                                    $scope.showMessagePopup("Message", "Record updated successfully");
                                }
                                else {
                                    $scope.showMessagePopup("Message", "Record not updated");
                                }
                            }
                        );
                }
            },
            size: 'lg'
        });
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    }
});