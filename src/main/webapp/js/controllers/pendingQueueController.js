/**
 * Created by Fallon Software on 2/19/2016.
 */
instaKYC.controller('pendingQueueController', function ($scope, $uibModal, $filter, $interval, CommonServices, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();
    $scope.showName = CommonServices.showName;
    $scope.getSplittedDate = CommonServices.getSplittedDate;

    $scope.getQueueData = function () {
        dataFactory.getQueueData()
            .then(function (data) {
                $scope.activity_data = data.data;
            });
    };

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end

    $scope.getQueueDataAuto = function () {
        dataFactory.getQueueDataAuto()
            .then(function (data) {
                $scope.activity_data = data.data;
            });
    };
    $interval(function () {
        $scope.getQueueDataAuto();
    }, 15000);

    $scope.branches = [];
    $scope.getBranches = function () {
        dataFactory.branchesbyUser()
            .then(function (data) {
                $scope.branches = data;
            });
    };

    $scope.getFilteredData = function (filter) {
        var search_filter = {
            report_status: "in_queue"
        };
        if (filter.branchId) {
            search_filter["branchId"] = filter.branchId
        }
        if (filter.filter_to || filter.filter_from) {
            if (filter.filter_to && filter.filter_from) {
                search_filter["filter_to"] = CommonServices.getDateFromconvertDDMMYYYYString(filter.filter_to);
                search_filter["filter_from"] = CommonServices.getDateFromconvertDDMMYYYYString(filter.filter_from);
                dataFactory.commonFilter(search_filter)
                    .then(function (data) {
                        $scope.activity_data = data.data;
                        $scope.summary_count_data = data.summary_count_data;
                    });
            }
            else {
                if (!filter.filter_to) {
                    $scope.invalid_filter.filter_to_required = true
                }
                else {
                    $scope.invalid_filter.filter_from_required = true
                }
            }
        }
        else {
            dataFactory.commonFilter(search_filter)
                .then(function (data) {
                    $scope.activity_data = data.data;
                    $scope.summary_count_data = data.summary_count_data;
                });
        }
    };

    var current_date = new Date();
    $scope.activity_data = [];

    $scope.notifyMe = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'notify_me_popup.html',
            controller: 'notifyController',
            size: 'md'
        });
    };

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end

    //datepicker start
    // Disable weekend selection

    var current_date = new Date();
    $scope.invalid_filter = {
        filter_from: false,
        filter_to: false,
        filter_to_less_than_from: false,
        filter_to_required: false,
        filter_from_required: false
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showFromCalendar = function () {
        $scope.from_datepicker.opened = true;
    };

    $scope.showToCalendar = function () {
        $scope.to_datepicker.opened = true;
    };

    $scope.selectFromDate = function () {
        var convertedDate = $filter('date')($scope.filter.filter_from, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.filter.filter_from = convertedDate
        }

        $scope.validateFromDate();
    };

    $scope.selectToDate = function () {
        var convertedDate = $filter('date')($scope.filter.filter_to, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.filter.filter_to = convertedDate
        }
        $scope.validateToDate()
    };

    $scope.from_datepicker = {
        opened: false
    };

    $scope.to_datepicker = {
        opened: false
    };
    //datepicker end


    $scope.toggleCheck = function () {
        $scope.activity_data = CommonServices.toggleCheckAll($scope.check_all, $scope.activity_data, $scope.currentPage, $scope.pageSize, $scope.orderByField, $scope.reverseSort);
    };

    $scope.checkForAll = function () {
        if ($scope.activity_data.length == CommonServices.getSelectedData($scope.activity_data).length) {
            $scope.check_all = true
        }
        else {
            $scope.check_all = false
        }
    };
    $scope.filter = {
        invalid: {}
    };
    $scope.validateFromDate = function () {
        $scope.invalid_filter.filter_from_required = false;
        if ($scope.filter.filter_from) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.filter.filter_from);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.filter.invalid.filter_from = true;
                }
                else {
                    $scope.filter.invalid.filter_from = false;
                }
            }
            else {
                $scope.filter.invalid.filter_from = true;
            }
        }
        else {
            $scope.filter.invalid.filter_from = true;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, new Date())) {
            $scope.invalid_filter.filter_from = true;
        }
        else {
            $scope.invalid_filter.filter_from = false;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, $scope.filter.filter_to)) {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
    };
    $scope.validateToDate = function () {
        $scope.invalid_filter.filter_to_required = false;
        if ($scope.filter.filter_to) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.filter.filter_to);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.filter.invalid.filter_to = true;
                }
                else {
                    $scope.filter.invalid.filter_to = false;
                }
            }
            else {
                $scope.filter.invalid.filter_to = true;
            }
        }
        else {
            $scope.filter.invalid.filter_to = true;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, $scope.filter.filter_to)) {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
    };

    $scope.getQueueData();
    $scope.getBranches()
});

instaKYC.controller('notifyController', function ($scope, $modalInstance) {
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.submit = function () {
        $modalInstance.dismiss('cancel');
    }
});