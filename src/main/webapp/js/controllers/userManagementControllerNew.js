instaKYC.controller('userManagementControllerNew', function ($scope, $uibModal, $window, CommonServices, dataFactory) {
    $scope.users = [];
    $scope.showUserManagement = true;

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end
    $scope.accessTypes = [
        "Admin",
        "Edit",
        "View"
    ];
    $scope.locations = [];
    $scope.emailNotExist = '';

    $scope.getUsers = function () {
        dataFactory.getUsersDataWithAdmin()
            .then(function (data) {
                $scope.users = data;
                $scope.showUserManagement = true;
                getActiveUsers()
            });
    };
    function getActiveUsers() {
        $scope.activeUsers = [];
        for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].active) {
                $scope.activeUsers.push($scope.users[i])
            }
        }
    }

    $scope.getLocations = function () {
        dataFactory.getLocationsData()
            .then(function (data) {
                $scope.locations = data;
            });
    };

    $scope.newCreateUserNew = function () {
        $scope.showUserManagement = false;
        $scope.heading = "New User Request";
        $scope.newUserData = true;
        $scope.editUserData = false;
        $scope.newUser = {};
        $scope.form.$setUntouched();
        $scope.emailVerified = true;
    };

    $scope.editUser = function (selectedUser) {
        $scope.showUserManagement = false;
        $scope.viewUserData = false;
        $scope.newUser = {};
        $scope.newUser = selectedUser;
        $scope.newUser.location = $scope.newUser.location.toString();
        $scope.newUserData = false;
        $scope.editUserData = true;
        $scope.heading = "Edit User Profile";
        $scope.emailVerified = true;
    };

    $scope.createUser = function (selected) {
        selected.maxRequestLimit = parseInt(selected.maxRequestLimit);
        selected.parallelSessionLimit = parseInt(selected.parallelSessionLimit);
        if (!$scope.emailNotExist) {
            dataFactory.createUser(selected)
                .then(function (data) {
                        if (data.result == "User Saved") {
                            $scope.showMessagePopup("Message", "Record saved successfully");
                            $scope.getUsers();
                        }
                        else if (data.result.endsWith("already exist")) {
                            $scope.showMessagePopup("Error", data.result)
                        }
                        else if (data.result == "Email verification fail") {
                            $scope.showMessagePopup("Message", "Email ID not exist")
                        }
                        else {
                            $scope.showMessagePopup("Message", "Record not saved");
                        }
                    }
                );
        }
        else {
            $scope.showMessagePopup("Message", "Email ID not exist")
        }
    };

    $scope.closeUserForm = function () {
        $scope.getUsers();
    };

    $scope.updateUser = function (selected) {
        if (!$scope.emailNotExist) {
            dataFactory.updateUser(selected)
                .then(function (data) {
                        if (data.result == "User Updated") {
                            $scope.showMessagePopup("Message", "Record updated successfully");
                            $scope.getUsers();
                        }
                        else if (data.result.endsWith("already exist")) {
                            $scope.showMessagePopup("Error", data.result)
                        }
                        else if (data.result == "Email verification fail") {
                            $scope.showMessagePopup("Message", "Email ID not exist")
                        }
                        else {
                            $scope.showMessagePopup("Message", "Record not updated");
                        }
                    }
                );
        }
        else {
            $scope.showMessagePopup("Message", "Email ID not exist")
        }
    };

    $scope.deleteUser = function (user) {
        dataFactory.deleteUser(user)
            .then(function (data) {
                    $scope.getUsers();
                    if (data.result == "User Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };

    $scope.validateEmailPattern = function (email) {
        if (email) {
            $scope.emailVerified = false;
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z]{2,4})$/;
            var result = pattern.test(email);
            if (result) {
                email_data = {
                    "email": email
                }
            }
            $scope.emailVerified = true;
        }
    };

    $scope.validateEmailApi = function (email) {
        if (email) {
            $scope.emailVerified = false;
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,255})$/;
            var result = pattern.test(email);
            if (result) {
                email_data = {
                    "email": email
                };
                dataFactory.validateEmail(email_data)
                    .then(function (data) {
                            $scope.emailVerified = true;
                            if (data.result == "fail") {
                                $scope.emailNotExist = "Email Id Doesn't Exist";
                            }
                            else {
                                $scope.emailNotExist = "";
                            }
                        }
                    );
            }
        }
    };

    $scope.setUserStatus = function (user) {
        var modalInstance = $uibModal.open({
            templateUrl: 'userStatus.html',
            controller: function ($scope) {
                var userStatus = user.active;
                if (user.active) {
                    $scope.heading = "Are you sure want to Activate the user profile ?";
                }
                else {
                    $scope.heading = "Are you sure want to Deactivate the user profile ?";
                }
                $scope.toggleActiveUser = function () {
                    if (user.active) {
                        user.active = true;
                    }
                    else {
                        user.active = false;
                    }
                    $scope.toggleUser(user);
                    modalInstance.dismiss('cancel');
                };
                $scope.toggleUser = function (user) {
                    dataFactory.toggleUser(user)
                        .then(function (data) {
                                if (data.result == "User Updated" && data.activeUser) {
                                    $scope.showMessagePopup("Message", "User Activated");
                                }
                                else if (data.result == "User Updated" && !data.activeUser) {
                                    $scope.showMessagePopup("Message", "User Deactivated");
                                }
                                else {
                                    $scope.showMessagePopup("Message", "Record not updated");
                                }
                            }
                        );
                };
                $scope.showMessagePopup = function (message_heading, message, redirect_url) {
                    var showMessageModal = $uibModal.open({
                        templateUrl: 'message.html',
                        controller: function ($scope) {
                            $scope.message_heading = message_heading;
                            $scope.message = message;
                            $scope.close = function () {
                                if (redirect_url) {
                                    parent.location.href = redirect_url;
                                }
                                showMessageModal.dismiss('cancel');
//                    $window.location.reload();
                            }
                        },
                        size: 'sm'
                    });
                };
                $scope.close = function () {
                    user.active = !userStatus;
                    modalInstance.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.showMessagePopup = function (message_heading, message, redirect_url) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    if (redirect_url) {
                        parent.location.href = redirect_url;
                    }
                    showMessageModal.dismiss('cancel');
//                    $window.location.reload();
                }
            },
            size: 'sm'
        });
    };

    $scope.getUsers();
    $scope.getLocations();
});