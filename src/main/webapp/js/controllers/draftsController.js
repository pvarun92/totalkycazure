/**
 * Created by Fallon Software on 2/19/2016.
 */
instaKYC.controller('draftsController', function ($scope, $uibModal, $filter, CommonServices, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();
    $scope.showName = CommonServices.showName;
    $scope.getSplittedDate = CommonServices.getSplittedDate;
    $scope.getDraftsData = function () {
        dataFactory.getDraftsData()
            .then(function (data) {
                //console.log(data.result)
                $scope.draft_data = data.data;
            });
    };

    $scope.branches = [];
    $scope.getBranches = function () {
        dataFactory.branchesbyUser()
            .then(function (data) {
                $scope.branches = data;
            });
    };

    $scope.getFilteredData = function (filter) {
        var search_filter = {
            report_status: "in_draft"
        };
        if (filter.branchId) {
            search_filter["branchId"] = filter.branchId
        }
        if (filter.filter_to || filter.filter_from) {
            if (filter.filter_to && filter.filter_from) {
                search_filter["filter_to"] = CommonServices.getDateFromconvertDDMMYYYYString(filter.filter_to);
                search_filter["filter_from"] = CommonServices.getDateFromconvertDDMMYYYYString(filter.filter_from);
                dataFactory.commonFilter(search_filter)
                    .then(function (data) {
                        $scope.draft_data = data.data;
                        $scope.summary_count_data = data.summary_count_data;
                    });
            }
            else {
                if (!filter.filter_to) {
                    $scope.invalid_filter.filter_to_required = true
                }
                else {
                    $scope.invalid_filter.filter_from_required = true
                }
            }
        }
        else {
            dataFactory.commonFilter(search_filter)
                .then(function (data) {
                    $scope.draft_data = data.data;
                    $scope.summary_count_data = data.summary_count_data;
                });
        }
    };

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end

    //datepicker start
    // Disable weekend selection

    var current_date = new Date();
    $scope.invalid_filter = {
        filter_from: false,
        filter_to: false,
        filter_to_less_than_from: false,
        filter_to_required: false,
        filter_from_required: false
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showFromCalendar = function () {
        $scope.from_datepicker.opened = true;
    };

    $scope.showToCalendar = function () {
        $scope.to_datepicker.opened = true;
    };

    $scope.selectFromDate = function () {
        var convertedDate = $filter('date')($scope.filter.filter_from, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.filter.filter_from = convertedDate
        }

        $scope.validateFromDate();
    };

    $scope.selectToDate = function () {
        var convertedDate = $filter('date')($scope.filter.filter_to, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.filter.filter_to = convertedDate
        }
        $scope.validateToDate()
    };

    $scope.from_datepicker = {
        opened: false
    };

    $scope.to_datepicker = {
        opened: false
    };
    //datepicker end


    $scope.toggleCheck = function () {
        $scope.draft_data = CommonServices.toggleCheckAll($scope.check_all, $scope.draft_data, $scope.currentPage, $scope.pageSize, $scope.orderByField, $scope.reverseSort);
    };

    $scope.checkForAll = function () {
        if ($scope.draft_data.length == CommonServices.getSelectedData($scope.draft_data).length) {
            $scope.check_all = true
        }
        else {
            $scope.check_all = false
        }
    };
    $scope.filter = {
        invalid: {}
    };
    $scope.validateFromDate = function () {
        $scope.invalid_filter.filter_from_required = false;
        if ($scope.filter.filter_from) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.filter.filter_from);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.filter.invalid.filter_from = true;
                }
                else {
                    $scope.filter.invalid.filter_from = false;
                }
            }
            else {
                $scope.filter.invalid.filter_from = true;
            }
        }
        else {
            $scope.filter.invalid.filter_from = true;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, new Date())) {
            $scope.invalid_filter.filter_from = true;
        }
        else {
            $scope.invalid_filter.filter_from = false;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, $scope.filter.filter_to)) {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
    };
    $scope.validateToDate = function () {
        $scope.invalid_filter.filter_to_required = false;
        if ($scope.filter.filter_to) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.filter.filter_to);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.filter.invalid.filter_to = true;
                }
                else {
                    $scope.filter.invalid.filter_to = false;
                }
            }
            else {
                $scope.filter.invalid.filter_to = true;
            }
        }
        else {
            $scope.filter.invalid.filter_to = true;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, $scope.filter.filter_to)) {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
    };

    $scope.discardDrafts = function () {
        var selected_drafts = CommonServices.getSelectedData($scope.draft_data);
        if (selected_drafts.length > 0) {
            var modalInstance = $uibModal.open({
                templateUrl: 'confirmation.html',
                scope: $scope,
                controller: function ($scope) {
                    $scope.header = "Confirm";
                    $scope.message = "Discard selected draft(s)?";
                    $scope.yes = function () {
                        discardSelectedDraft(selected_drafts);
                        modalInstance.dismiss('cancel');
                    },
                        $scope.close = function () {
                            modalInstance.dismiss('cancel');
                        }
                },
                size: 'sm'
            });
            var discardSelectedDraft = function (selected_drafts) {
                dataFactory.discardDrafts({selected_drafts: selected_drafts})
                    .then(function (data) {
                        //console.log(data.result)
                        $scope.showMessagePopup("Message", "Draft(s) discarded");
                        $scope.getDraftsData();
                    });
            }
        }
        else {
            $scope.showMessagePopup("Error", "No drafts selected")
        }

    };
    $scope.showMessagePopup = function (message_heading, message, redirect_url) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    if (redirect_url) {
                        parent.location.href = redirect_url;
                    }
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };
    $scope.getDraftsData();
    $scope.getBranches();
});