instaKYC.controller('customerManagementController', function ($scope, $uibModal, CommonServices, dataFactory, $filter) {
    $scope.customers = [];
    $scope.customerTypes = [
        "Scheduled Commercial Bank (Large)",
        "Urban Co-operative Bank (Small)",
        "Urban Co-operative Bank (Medium)",
        "Retail"
    ];
    $scope.customer_id = CommonServices.getParameterByName("id");
    $scope.getSplittedDate = CommonServices.getSplittedDate;

    $scope.showNewCustomerPage = true;
    $scope.ShowEditCustomer = false;

    $scope.currentPage = 1;
    $scope.pageSize = 10;

    if (!localStorage["current_user"]) {
        dataFactory.getCurrentUser()
            .then(function (data) {
                var current_user = data;
                localStorage['current_user'] = JSON.stringify(current_user);
                $scope.userRole = CommonServices.getCurrentUserRole();
            });
    }

    $scope.getAdminDashboardData = function () {
        dataFactory.showCustomerNew()
            .then(function (data) {
                    $scope.customers = [];
                    for (var i = 0; i < data.length; i++) {
                        var obj = {
                            id: data[i][1],
                            customerName: data[i][2],
                            customerType: data[i][3],
                            noOfUsers: data[i][0]
                        };
                        $scope.customers.push(obj);
                    }
                }
            );
    };

    $scope.newCustomer = {
        subscriptionMasters: []
    };

    $scope.newSubscription = {
        application: []
    };

    if ($scope.customer_id) {
        dataFactory.getCustomerData($scope.customer_id)
            .then(function (data) {
                    $scope.newCustomer = data.customerMaster;
                    $scope.newCustomer.subscriptionMasters = data.subscriptionMaster;
                    $scope.ShowEditCustomer = true;
                }
            );
    }

    $scope.multipleDropDownSetting = {
        scrollableHeight: '200px',
        scrollable: true,
        //enableSearch: true,
        buttonClasses: "input-field multiselect-dropdown-style"
    };

    $scope.customEvents = {
        onItemSelect: function (item) {
            $scope.applicationRequired = false
        }
    };

    $scope.applicationList = [{
        "label": "TotalKYC",
        "id": "TotalKYC"
    }, {
        "label": "KWatch",
        "id": "KWatch"
    }, {
        "label": "KScan",
        "id": "KScan"
    }];

    $scope.billingFrequencies = [
        "Monthly",
        "Quarterly",
        "Annually"
    ];

    $scope.formats = ['dd/MM/yyyy', 'dd-MM-yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.contract_date_popup = {
        opened: false
    };

    $scope.showContractDateCalendar = function () {
        $scope.contract_date_popup.opened = true;
    };

    $scope.selectContractDate = function () {
        var convertedDate = $filter('date')($scope.contractDate, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.newSubscription.contractDate = convertedDate
        }
        $scope.validateContactDate()
    };

    $scope.validateContactDate = function () {
        $scope.is_contract_date_valid = $scope.isInvalidateDate($scope.newSubscription.contractDate)
    };

    //validFrom date start
    $scope.valid_from_date_popup = {
        opened: false
    };

    $scope.showValidFromCalendar = function () {
        $scope.valid_from_date_popup.opened = true;
    };

    $scope.selectValidFromDate = function () {
        var convertedDate = $filter('date')($scope.validFromDate, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.newSubscription.validFromDate = convertedDate
        }
        $scope.validateValidFromDate()
    };

    $scope.validateValidFromDate = function () {
        $scope.is_valid_from_valid = $scope.isInvalidateDate($scope.newSubscription.validFromDate)
    };

    //validFromDate date end


    //valid till date start
    $scope.valid_till_date_popup = {
        opened: false
    };

    $scope.showValidTillCalendar = function () {
        $scope.valid_till_date_popup.opened = true;
    };

    $scope.selectValidTillDate = function () {
        var convertedDate = $filter('date')($scope.validTillDate, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.newSubscription.validTillDate = convertedDate
        }
        $scope.validateValidTillDate()
    };

    $scope.validateValidTillDate = function () {
        $scope.is_valid_till_valid = $scope.isInvalidateDate($scope.newSubscription.validTillDate)
    };

    //valid till date end

    //billingStartDate start
    $scope.billing_start_date_popup = {
        opened: false
    };

    $scope.showBillingStartDateCalendar = function () {
        $scope.billing_start_date_popup.opened = true;
    };

    $scope.selectBillingStartDateDate = function () {
        var convertedDate = $filter('date')($scope.billingStartDate, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.newSubscription.billingStartDate = convertedDate
        }
        $scope.validateBillingStartDateDate()
    };

    $scope.validateBillingStartDateDate = function () {
        $scope.is_billing_start_valid = $scope.isInvalidateDate($scope.newSubscription.billingStartDate)
    };

    //billingStartDate end

    //billingEndDate start
    $scope.billing_end_date_popup = {
        opened: false
    };

    $scope.showBillingEndDateCalendar = function () {
        $scope.billing_end_date_popup.opened = true;
    };

    $scope.selectBillingEndDateDate = function () {
        var convertedDate = $filter('date')($scope.billingEndDate, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.newSubscription.billingEndDate = convertedDate
        }
        $scope.validateBillingEndDateDate()
    };

    $scope.validateBillingEndDateDate = function () {
        $scope.is_billing_end_valid = $scope.isInvalidateDate($scope.newSubscription.billingEndDate)
    };

    //billingEndDate end

    $scope.isInvalidateDate = function (date_requested) {
        if (date_requested) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString(date_requested);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    };
    //date picker code End

    $scope.saveSubscriptionPlan = function (selected) {
//        $scope.newSubscription = {}
        if (!$scope.newSubscription.application || $scope.newSubscription.application.length <= 0) {
            $scope.applicationRequired = true
        }
        if (!$scope.applicationRequired) {
            if ($scope.newCustomer.subscriptionMasters.length <= 0) {
                for (var i = 0; i < selected.application.length; i++) {
                    var app = {
                        application: selected.application[i].id,
                        contractDate: selected.contractDate ? CommonServices.getDateFromconvertDDMMYYYYString(selected.contractDate) : selected.contractDate,
                        billingPeriodicity: selected.billingPeriodicity,
                        billingEndDate: selected.billingEndDate ? CommonServices.getDateFromconvertDDMMYYYYString(selected.billingEndDate) : selected.billingEndDate,
                        perEntityRate: selected.perEntityRate,
                        perUserIdFees: selected.perUserIdFees,
                        validFromDate: selected.validFromDate ? CommonServices.getDateFromconvertDDMMYYYYString(selected.validFromDate) : selected.validFromDate,
                        validTillDate: selected.validTillDate ? CommonServices.getDateFromconvertDDMMYYYYString(selected.validTillDate) : selected.validTillDate,
                        billingStartDate: selected.billingStartDate ? CommonServices.getDateFromconvertDDMMYYYYString(selected.billingStartDate) : selected.billingStartDate,
                        oneTimeFees: selected.oneTimeFees
                    };
                    $scope.newCustomer.subscriptionMasters.push(app);
                }
            }
            $scope.showNewCustomerPage = true
        }
    };

    $scope.newSubscriptionMaster = function () {
        $scope.showNewCustomerPage = false;
        $scope.newCustomer.subscriptionMasters = []

    };

    $scope.closeCustomerForm = function () {
        parent.location.href = "sysadmin?navigate_to=sysAdminCustomerManagement";
    };

    $scope.saveCustomer = function (selected) {

        angular.forEach(selected.subscriptionMasters, function (subscriptionMaster) {
            delete subscriptionMaster["$$hashKey"]
        });
        dataFactory.createCustomer(selected)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Saved") {
                        $scope.showMessagePopup("Message", "Record saved successfully", "sysadmin?navigate_to=sysAdminCustomerManagement");
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not saved");
                    }
                }
            );
    };

    $scope.updateCustomer = function (customer) {
        angular.forEach(customer.subscriptionMasters, function (subscriptionMaster) {
            delete subscriptionMaster["$$hashKey"]
        });

        var customer_info = {
            id: customer.id,
            customerName: customer.customerName,
            customerType: customer.customerType,
            address: customer.address,
            contactPerson: customer.contactPerson,
            designation: customer.designation,
            contactNumber: customer.contactNumber,
            email: customer.email,
            panNumber: customer.panNumber,
            tanNumber: customer.tanNumber,
            serviceTaxNumber: customer.serviceTaxNumber,
            karzaProducts: customer.karzaProducts,
            creditPeriod: customer.creditPeriod,
            address1: customer.address1,
            state: customer.state,
            pinCode: customer.pinCode
//            subscriptionMasters:customer.subscriptionMasters
        };
        dataFactory.updateCustomer(customer_info)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Updated") {
                        $scope.showMessagePopup("Message", "Record updated successfully", "sysadmin?navigate_to=sysAdminCustomerManagement")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not Updated", "sysadmin?navigate_to=sysAdminCustomerManagement")
                    }
                }
            );
    };

    $scope.getCustomerProfileData = function (customerId) {
        dataFactory.getCustomerProfileData(customerId)
            .then(function (data) {
                $scope.customer_profile_data = data;
            });
    };

    $scope.deleteCustomer = function (customer) {
        delete customer.noOfUsers;
        dataFactory.deleteCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };

    $scope.validateEmailApi = function (email) {
        $scope.emailNotExist = "";
        if (email) {
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z]{2,4})$/;
            var result = pattern.test(email);
            if (result) {
                email_data = {
                    "email": email
                };
                dataFactory.validateEmail(email_data)
                    .then(function (data) {
                            if (data.result == "fail") {
                                $scope.emailNotExist = "Email Id Doesn't Exist";
                            }
                            else {
                                $scope.emailNotExist = "";
                            }
                        }
                    );
            }
        }
    };

    $scope.showMessagePopup = function (message_heading, message, redirect_url) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    if (redirect_url) {
                        parent.location.href = redirect_url;
                    }
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.states = [
        'Andhra Pradesh',
        'Arunachal Pradesh',
        'Assam',
        'Bihar',
        'Chhattisgarh',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal Pradesh',
        'Jammu & Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
        'Madhya Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Mizoram',
        'Nagaland',
        'Odisha',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil Nadu',
        'Telangana',
        'Tripura',
        'Uttarakhand',
        'Uttar Pradesh',
        'West Bengal',
        'Andaman & Nicobar',
        'Chandigarh',
        'Dadra and Nagar Haveli',
        'Daman & Diu',
        'Delhi',
        'Lakshadweep',
        'Puducherry'
    ];

    $scope.getAdminDashboardData();
});