instaKYC.controller('userProfileNewController', function ($scope, $uibModal, CommonServices, dataFactory) {

    var userId = CommonServices.getParameterByName("id");
    $scope.profile = {};
    $scope.userProfile = {};

    $scope.getUserProfileData = function (userId) {
        dataFactory.getUserProfileData(userId)
            .then(function (data) {
                $scope.profile = data;
                $scope.userProfile = data.profile;
            });

    };

    $scope.toggleActiveUser = function (user) {
        if (user.active) {
            $scope.userProfile.active = true;
        }
        else {
            $scope.userProfile.active = false;
        }
        $scope.toggleUser($scope.userProfile);
    };

    $scope.toggleUser = function (userProfile) {
        $scope.userData = {
            active: userProfile.active,
            id: userProfile.id,
            contactNumber: userProfile.contactNumber,
            email: userProfile.email,
            location: userProfile.location,
            userName: userProfile.userName,
            userRole: userProfile.userRole,
            customerMasterId: userProfile.customerMasterId,
            designation: userProfile.designation,
            createdBy: userProfile.createdBy,
            password: userProfile.password,
            maxRequestLimit: userProfile.maxRequestLimit,
            currentMonth: userProfile.currentMonth,
            remainingLimit: userProfile.remainingLimit,
//            autoGeneratedPasswordFlag : userProfile.autoGeneratedPasswordFlag,
//            autoGeneratedPasswordExpired : userProfile.autoGeneratedPasswordExpired,
            parallelSessionLimit: userProfile.parallelSessionLimit,
            uniqueId: userProfile.uniqueId,
            loginCount: userProfile.loginCount,
            employeeId: userProfile.employeeId,
//            createdAt : userProfile.createdAt,
//            updatedAt : userProfile.updatedAt,
            application: userProfile.application,
            accessRights: userProfile.accessRights,
            locationRights: userProfile.locationRights
        };
        dataFactory.toggleUser($scope.userData)
            .then(function (data) {
                    if (data.result == "User Updated" && data.activeUser) {
                        $scope.showMessagePopup("Message", "User Activated");
                    }
                    else if (data.result == "User Updated" && !data.activeUser) {
                        $scope.showMessagePopup("Message", "User Deactivated");
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not updated");
                    }
                }
            );
    };

    $scope.timeSince = function (createddate, updatedDate) {

        updatedDate = new Date(updatedDate);
        createddate = new Date(createddate);
        var seconds = Math.floor((createddate - updatedDate) / 1000);

        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes";
        }
        return Math.floor(seconds) + " seconds";
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.getUserProfileData(userId);
});