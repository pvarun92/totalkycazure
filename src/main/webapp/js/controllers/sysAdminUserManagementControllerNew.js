instaKYC.controller('sysAdminUserManagementControllerNew', function ($scope, $uibModal, CommonServices, dataFactory, $window) {
    $scope.users = [];
    $scope.customer_id = CommonServices.getParameterByName("id");
    var url = window.parent.location.href;
    $scope.userId = CommonServices.getParameterByName("userId", url);
    $scope.showUserManagement = true;

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end
    $scope.locations = [];
    $scope.emailNotExist = '';

    $scope.getCustomerUsersNew = function (customer_id) {
        dataFactory.getCustomerUsersNew(customer_id)
            .then(function (data) {
                $scope.users = data;
                $scope.showUserManagement = true;
            });
    };

    $scope.getLocations = function (customer_id) {
        var customerId = {
            id: $scope.customer_id
        };
        dataFactory.getLocationByCustomerId(customerId)
            .then(function (data) {
                $scope.locations = data;
            });
    };

    $scope.accessTypes = [
        "Admin",
        "Edit",
        "View"
    ];

    $scope.closeUserForm = function () {
        $scope.getCustomerUsersNew($scope.customer_id);
    };

    $scope.createUser = function (user) {
        user.customerMasterId = $scope.customer_id;
        if (!$scope.emailNotExist) {
            dataFactory.createCustomerUserNew(user)
                .then(function (data) {
                        if (data.result == "Customer User Saved") {
                            $scope.showMessagePopup("Message", "Record submitted successfully");
                            $scope.getCustomerUsersNew($scope.customer_id);
                        }
                        else if (data.result.endsWith("already exist")) {
                            $scope.showMessagePopup("Error", data.result)
                        }
                        else if (data.result == "Email verification fail") {
                            $scope.showMessagePopup("Message", "Email ID not exist")
                        }
                    }
                );
        }
        else {
            $scope.showMessagePopup("Message", "Email ID not exist")
        }
    };

    $scope.updateUser = function (user) {
        if (!$scope.emailNotExist) {
            dataFactory.updateCustomerUserNew(user)
                .then(function (data) {
                        if (data.result == "Customer User Updated") {
                            $scope.showMessagePopup("Message", "Record updated successfully");
                            $scope.getCustomerUsersNew($scope.customer_id);
                        }
                        else if (data.result.endsWith("already exist")) {
                            $scope.showMessagePopup("Error", data.result)
                        }
                        else if (data.result == "Email verification fail") {
                            $scope.showMessagePopup("Message", "Email ID not exist")
                        }
                        else {
                            $scope.showMessagePopup("Message", "Record not Updated")
                        }
                    }
                );
        }
        else {
            $scope.showMessagePopup("Message", "Email ID not exist")
        }
    };

    $scope.deleteUser = function (user) {
        dataFactory.deleteCustomerUserNew(user)
            .then(function (data) {
                    $scope.getCustomerUsersNew($scope.customer_id);
                    if (data.result == "User Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };
    $scope.showMessagePopup = function (message_heading, message, redirect_url) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    if (redirect_url) {
                        parent.location.href = redirect_url;
                    }
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.changeDomain = function (selectedCustomerDomain) {
        dataFactory.changeDomain(selectedCustomerDomain)
            .then(function (data) {
                    if (data.data) {
                        $scope.getAdminDashboardData();
                        $scope.showMessagePopup("Message", "Domain Changed");
                    }
                    else {
                        $scope.showMessagePopup("Message", "Domain change failed");
                    }
                }
            );
    };

    $scope.validateEmailApi = function (email) {
        $scope.emailNotExist = "";
        if (email) {
            $scope.emailVerified = false;
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,255})$/;
            var result = pattern.test(email);
            if (result) {
                email_data = {
                    "email": email
                };
                dataFactory.validateEmail(email_data)
                    .then(function (data) {
                            $scope.emailVerified = true;
                            if (data.result == "fail") {
                                $scope.emailNotExist = "Email Id Doesn't Exist";
                            }
                            else {
                                $scope.emailNotExist = "";
                            }
                        }
                    );
            }
        }
    };

    $scope.editUser = function (selectedUser) {
        $scope.showUserManagement = false;
        $scope.viewUserData = false;
        $scope.newUser = {};
        $scope.newUser = selectedUser;
        $scope.newUser.location = $scope.newUser.location.toString();
        $scope.newUserData = false;
        $scope.editUserData = true;
        $scope.heading = "Edit User Profile";
        $scope.emailVerified = true;
    };

    $scope.showNewUser = function () {
        $scope.viewUserData = false;
        $scope.showUserManagement = false;
        $scope.editUserData = false;
        $scope.newUserData = true;
        $scope.heading = "New User Request";
        $scope.emailVerified = true;
        $scope.newUser = {};
        $scope.form.$setUntouched();
    };

    $scope.showUserData = function (selectedUser) {
        $scope.heading = "View User Request";
        $scope.showUserManagement = false;
        $scope.newUser = selectedUser;
        $scope.newUser.location = $scope.newUser.location.toString();
        $scope.viewUserData = true;
        $scope.editUserData = false;
        $scope.newUserData = false;

    };

    $scope.getCustomerUsersNew($scope.customer_id);
    $scope.getLocations()
});