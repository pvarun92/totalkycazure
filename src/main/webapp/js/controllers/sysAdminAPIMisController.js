/**
 * Created by Varun_Prakash on 08-12-2016.
 */
instaKYC.controller('sysAdminAPIMisController', function ($scope, $filter, CommonServices, dataFactory) {
	

    /*Function to get all the customers in order to populate it on sysAdmin frontend*/
    $scope.getAllCustomers = function () {
        dataFactory.showCustomers()
            .then(function (data) {
                $scope.customers = data;
            });
    };
   
    $scope.apiName = [];
    $scope.apiSetting = {
            scrollableHeight: '500px',
            scrollable: true,
            enableSearch: true
        };
    $scope.apiList = [{
        "label": "PAN",
        "id": "PAN"
    }, {
        "label": "AADHAR",
        "id": "AADHAR"
    }, {
        "label": "LPG",
        "id": "LPG"
    },
    {
        "label": "EPF",
        "id": "EPF"
    }, {
        "label": "ESIC",
        "id": "ESIC"
    }, {
        "label": "EXCISE",
        "id": "EXCISE"
    }, {
        "label": "CIN",
        "id": "CIN"
    }, {
        "label": "LLPIN",
        "id": "LLPIN"
    }, {
        "label": "FLLPIN",
        "id": "FLLPIN"
    }, {
        "label": "FCRN",
        "id": "FCRN"
    }, {
        "label": "SERVICE TAX",
        "id": "SERVICE"
    }, {
        "label": "PROFESSIONAL TAX",
        "id": "PROFESSIONAL"
    }, {
        "label": "VAT",
        "id": "VAT"
    }];
    
    

    $scope.getFilteredDataAdmin = function (filter) {
        var selected_filter = {};
       
        if (filter.customerName) {
            selected_filter["customerName"] = filter.customerName.customerName
        }
        

        if (filter.filter_to || filter.filter_from) {
            if (filter.filter_from && filter.filter_to) {
                selected_filter["filter_to"] = CommonServices.getDateFromconvertDDMMYYYYString(filter.filter_to);
                selected_filter["filter_from"] = CommonServices.getDateFromconvertDDMMYYYYString(filter.filter_from);
                dataFactory.getFilteredDataAdmin(selected_filter)
                    .then(function (data) {
                        $scope.filtered_searched_data = data.data;
                        $scope.summary_count_data = data.summary_count_data;
                    });
            }
            else {
                if (!filter.filter_to) {
                    $scope.invalid_filter.filter_to_required = true
                }
                else {
                    $scope.invalid_filter.filter_from_required = true
                }
            }
        }
        else {
            dataFactory.getFilteredDataAdmin(selected_filter)
                .then(function (data) {
                    $scope.filtered_searched_data = data.data;
                    $scope.summary_count_data = data.summary_count_data;
                });
        }
    };

    $scope.summary_count_data =
    {
        "total_request": "0",
        "generated": "0",
        "valid": "0",
        "invalid": "0",
        "itrValid":"0",
        "itrInvalid":"0"	
    };
  
    $scope.filtered_searched_data = [];

    var current_date = new Date();
    $scope.invalid_filter = {
        filter_from: false,
        filter_to: false,
        filter_to_less_than_from: false,
        filter_to_required: false,
        filter_from_required: false
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showFromCalendar = function () {
        $scope.from_datepicker.opened = true;
    };

    $scope.showToCalendar = function () {
        $scope.to_datepicker.opened = true;
    };

    $scope.selectFromDate = function () {
        var convertedDate = $filter('date')($scope.filter.filter_from, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.filter.filter_from = convertedDate
        }

        $scope.validateFromDate();
    };

    $scope.selectToDate = function () {
        var convertedDate = $filter('date')($scope.filter.filter_to, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.filter.filter_to = convertedDate
        }
        $scope.validateToDate()
    };

    $scope.from_datepicker = {
        opened: false
    };

    $scope.to_datepicker = {
        opened: false
    };
    //datepicker end


    $scope.filter = {
        invalid: {}
    };
    $scope.validateFromDate = function () {
        $scope.invalid_filter.filter_from_required = false;
        if ($scope.filter.filter_from) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.filter.filter_from);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.filter.invalid.filter_from = true;
                }
                else {
                    $scope.filter.invalid.filter_from = false;
                }
            }
            else {
                $scope.filter.invalid.filter_from = true;
            }
        }
        else {
            $scope.filter.invalid.filter_from = true;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, new Date())) {
            $scope.invalid_filter.filter_from = true;
        }
        else {
            $scope.invalid_filter.filter_from = false;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, $scope.filter.filter_to)) {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
    };
    $scope.validateToDate = function () {
        $scope.invalid_filter.filter_to_required = false;
        if ($scope.filter.filter_to) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.filter.filter_to);
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.filter.invalid.filter_to = true;
                }
                else {
                    $scope.filter.invalid.filter_to = false;
                }
            }
            else {
                $scope.filter.invalid.filter_to = true;
            }
        }
        else {
            $scope.filter.invalid.filter_to = true;
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, $scope.filter.filter_to)) {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
    };

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 5;
    //pagination end

    $scope.getConvertedDate = function (date) {
        if (date) {
            date = date.split(" ")[0];
            date = $filter('date')(date, "dd-MM-yyyy");
        }
        return date
    };

    $scope.exportToXLS = function () {
        var exportabledata = '<table><tr>';
        exportabledata = exportabledata + "<th>Report ID</th>" +
            "<th>Report Date</th>" +
            "<th>Entity Name</th>" +
            "<th>Entity Type</th>" +
            "<th>Branch</th>" +
            "<th>No of Parties</th>" +
            "<th>No of Documents</th>" +
            "<th>Report Status</th>" +
            "<th>KYC Validity</th>" +
            "<th>ITR Validity</th>";
        exportabledata = exportabledata + "</tr>";
        for (var i = 0; i < $scope.filtered_searched_data.length; i++) {
            exportabledata = exportabledata + "<tr>";
            var report_id = '';
            var report_date = '';
            var entity_name = '';
            var entity_type = '';
            var branch = '';
            var no_of_parties = '';
            var no_of_documents = '';
            var report_status = '';
            var validity = '';
            var itrValidity = '';
            if ($scope.filtered_searched_data[i].report_id) {
                report_id = $scope.filtered_searched_data[i].report_id
            }
            if ($scope.filtered_searched_data[i].report_date) {
                report_date = $scope.filtered_searched_data[i].report_date
            }
            if ($scope.filtered_searched_data[i].entity_name) {
                entity_name = $scope.filtered_searched_data[i].entity_name
            }
            if ($scope.filtered_searched_data[i].entity_type) {
                entity_type = $scope.filtered_searched_data[i].entity_type
            }
            if ($scope.filtered_searched_data[i].branch) {
                branch = $scope.filtered_searched_data[i].branch
            }
            if ($scope.filtered_searched_data[i].no_of_parties) {
                no_of_parties = $scope.filtered_searched_data[i].no_of_parties
            }
            if ($scope.filtered_searched_data[i].no_of_documents) {
                no_of_documents = $scope.filtered_searched_data[i].no_of_documents
            }
            if ($scope.filtered_searched_data[i].report_status) {
                report_status = $scope.filtered_searched_data[i].report_status
            }
            if ($scope.filtered_searched_data[i].validity) {
                validity = $scope.filtered_searched_data[i].validity
            }
            if ($scope.filtered_searched_data[i].itrValidity) {
                itrValidity = $scope.filtered_searched_data[i].itrValidity
            }
            else {
                itrValidity = "N.A."
            }
            exportabledata = exportabledata + "<td>" + report_id + "</td>" +
                "<td>" + report_date + "</td>" +
                "<td>" + entity_name + "</td>" +
                "<td>" + entity_type + "</td>" +
                "<td>" + branch + "</td>" +
                "<td>" + no_of_parties + "</td>" +
                "<td>" + no_of_documents + "</td>" +
                "<td>" + report_status + "</td>" +
                "<td>" + validity + "</td>" +
                "<td>" + itrValidity + "</td>";
            exportabledata = exportabledata + "</tr>"
        }
        exportabledata = exportabledata + "</table>";
        var blob = new Blob([exportabledata], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
        saveAs(blob, "sysAdminReportMIS.xls");
    };

    $scope.print = function () {
        window.print();
    };

    $scope.getAllCustomers();

});
