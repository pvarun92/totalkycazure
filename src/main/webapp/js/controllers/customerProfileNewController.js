instaKYC.controller('customerProfileNewController', function ($scope, $uibModal, CommonServices, dataFactory) {

    $scope.getSplittedDate = CommonServices.getSplittedDate;
    var customer_id = CommonServices.getParameterByName("id");
    $scope.customer_id = customer_id;

    $scope.getCustomerProfileData = function (customer_id) {
        var json = {
            id: customer_id
        };
        dataFactory.getCustomerProfileData(json)
            .then(function (data) {
                $scope.profile = data;
            });
    };

    $scope.refreshData = function (customerID) {
        $scope.getCustomerProfileData(customerID);
    };

    $scope.getCustomerProfileData(customer_id);
});