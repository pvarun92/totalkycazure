/**
 * Created by Fallon Software on 3/8/2016.
 */

instaKYC.controller('kycValidationReportController', function ($scope, $timeout, $filter, CommonServices, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();
    $scope.getSplittedDate = CommonServices.getSplittedDate;
    $scope.isValidStatus = CommonServices.isValidStatus;
    var current_date = new Date();
    current_date.toISOString();
    $scope.id = CommonServices.getParameterByName("id");
    if ($scope.id) {
        dataFactory.getKycReport($scope.id)
            .then(function (data) {
                $scope.request_data = data.data;
                $scope.header_data = getHeaderData($scope.request_data[0]);

                $scope.allEntities = [];
                for (var i = 0; i < $scope.request_data.length; i++) {
                    var entity_data = $scope.request_data[i];
                    var entity_type = entity_data.entity_type;
                    var form_type = getFormType(entity_type);
                    var entity = setReportData(entity_data, entity_type, form_type);
                    $scope.allEntities.push(entity);
                }

                $scope.getReportByEntity($scope.allEntities[0])
            });
    }

    $scope.getReportByEntity = function (entity) {
        $scope.selected_entity = entity
        console.debug("Selected Entity is ", entity)
    };

    function setReportData(entity_data, entity_type, form_type) {
        if (entity_data.created_date) {
            entity_data.created_date = entity_data.created_date.split(" ")[0];
        }
        if (entity_data.dob) {
            entity_data.dob = entity_data.dob.split(" ")[0];
        }

        var overall_summary_data = getOverallSummaryData(entity_data, entity_type, form_type);

        entity_data = getCustomerInformation(entity_data, entity_type, form_type);

        var customer_document_information = getCustomerDocumentInformation(entity_data, entity_type, form_type);

        var detailed_document_report_data = entity_data.detailed_document_report_data;

        var itr_details = getCustomerITRDocumentInformation(entity_data, entity_type, form_type);

        entity_data["customer_document_information"] = customer_document_information;
        console.debug("Putting up the Customer Document Info Data",customer_document_information);
        entity_data["detailed_document_report_data"] = detailed_document_report_data;
        console.debug("Putting up the Detailed Document Info Data",detailed_document_report_data);
        entity_data["overall_summary_data"] = overall_summary_data;
        console.debug("Over all Summary Data",overall_summary_data);
        entity_data["itr_details"] = itr_details;
        return entity_data;
    }

    function getHeaderData(data) {
        var header_data =
        {
            "user_id": data.unique_id,
            "report_id": data.request_id,
            "report_date": data.created_date,
            "branch": data.branch,
            "isEdited": data.isEdited
        };
        return header_data;
    }

    var overallindex = 0;

    function getOverallSummaryData(entity_data, entity_type, form_type) {
        overallindex = overallindex + 1;
        var overall_summary_data = {
            "id": entity_data.id,
            "sr_no": overallindex,
            "name": getName(entity_data, form_type),
            "relation": entity_data.relation,
            "no_of_documents": entity_data.no_of_documents,
            "validity": entity_data.validity,
            "itrValidity": entity_data.itrValidity
        };

        return overall_summary_data;
    }

    function getCustomerInformation(entity_data, entity_type, form_type) {
        var customerInformationHeading = getCustomerInformationHeading(entity_type);
        var show_gender;
        if (form_type == "individuals") {
            show_gender = true
        }
        else {
            show_gender = false
        }
        var address = "";
        address = entity_data.address;
        if (entity_data.address) {
            address = entity_data.address
        }
        if (entity_data.address1) {
            address = address + ", " + entity_data.address1
        }
        if (entity_data.city) {
            address = address + ", " + entity_data.city
        }
        if (entity_data.state) {
            address = address + ", " + entity_data.state
        }
        if (entity_data.pinCode) {
            address = address + ", " + entity_data.pinCode
        }
        var customer_information =
        {
            "name": getName(entity_data, form_type),
            "entity_type": entity_data.entity_type,
            "address": address,
            "dob": entity_data.dob,
            "gender": entity_data.gender,
            "mobile": entity_data.contactNumber,
            "email": entity_data.email,
            "kartaName": entity_data.first_name + " " + entity_data.middle_name + " " + entity_data.last_name
        };

        entity_data['customerInformationHeading'] = customerInformationHeading;
        entity_data['show_gender'] = show_gender;
        entity_data['customer_information'] = customer_information;
        return entity_data;
    }

    function getCustomerITRDocumentInformation(entity_data, entity_type, form_type) {
        if (entity_data.customer_document_information.itr) {
            var itr_details = {
                "document": "ITR",
                "itr_details": entity_data.customer_document_information.itr
            }
        }
        return itr_details
    }

    function getCustomerDocumentInformation(entity_data, entity_type, form_type) {
        var customer_document_information = [];
        if (entity_data.customer_document_information) {
            if (entity_data.customer_document_information.pan) {
                var pan_details = {
                    "document": "PAN",
                    "number": entity_data.customer_document_information.pan.number,
                    "status": entity_data.customer_document_information.pan.status,
                    "status_as_per_source": entity_data.customer_document_information.pan.status_as_per_source
                };
                customer_document_information.push(pan_details)
            }
            if (entity_data.customer_document_information.voter) {
                var voter_details = {
                    "document": "Voter ID",
                    "number": entity_data.customer_document_information.voter.number,
                    "status": entity_data.customer_document_information.voter.status,
                    "status_as_per_source": entity_data.customer_document_information.voter.status_as_per_source
                };
                customer_document_information.push(voter_details)
            }
            if (entity_data.customer_document_information.itr) {
                var itr_details = {
                    "document": "ITR",
                    "itr_details": entity_data.customer_document_information.itr
                };
                $scope.itr_details = itr_details
            }
            if (entity_data.customer_document_information.aadhar) {
                var aadhar_details = {
                    "document": "Aadhar",
                    "number": entity_data.customer_document_information.aadhar.number,
                    "status": entity_data.customer_document_information.aadhar.status,
                    "status_as_per_source": entity_data.customer_document_information.aadhar.status_as_per_source
                };
                customer_document_information.push(aadhar_details)
            }
            if (entity_data.customer_document_information.iec) {
                var iec_details = {
                    "document": "IEC",
                    "number": entity_data.customer_document_information.iec.number,
                    "status": entity_data.customer_document_information.iec.status,
                    "status_as_per_source": entity_data.customer_document_information.iec.status_as_per_source
                };
                customer_document_information.push(iec_details)
            }
            if (entity_data.customer_document_information.dl) {
                var dl_details = {
                    "document": "Driving Licence",
                    "number": entity_data.customer_document_information.dl.number,
                    "status": entity_data.customer_document_information.dl.status,
                    "status_as_per_source": entity_data.customer_document_information.dl.status_as_per_source
                };
                customer_document_information.push(dl_details)
            }
            if (entity_data.customer_document_information.service_tax) {
                var service_tax_details = {
                    "document": "Service Tax",
                    "number": entity_data.customer_document_information.service_tax.number,
                    "status": entity_data.customer_document_information.service_tax.status,
                    "status_as_per_source": entity_data.customer_document_information.service_tax.status_as_per_source
                };
                customer_document_information.push(service_tax_details)
            }
            if (entity_data.customer_document_information.llpin) {
                var llpin_details = {
                    "document": "LLPIN",
                    "number": entity_data.customer_document_information.llpin.number,
                    "status": entity_data.customer_document_information.llpin.status,
                    "status_as_per_source": entity_data.customer_document_information.llpin.status_as_per_source
                };
                customer_document_information.push(llpin_details)
            }
            if (entity_data.customer_document_information.cin) {
                var cin_details = {
                    "document": "CIN",
                    "number": entity_data.customer_document_information.cin.number,
                    "status": entity_data.customer_document_information.cin.status,
                    "status_as_per_source": entity_data.customer_document_information.cin.status_as_per_source
                };
                customer_document_information.push(cin_details)
            }
            if (entity_data.customer_document_information.fcrn) {
                var cin_details = {
                    "document": "FCRN",
                    "number": entity_data.customer_document_information.fcrn.number,
                    "status": entity_data.customer_document_information.fcrn.status,
                    "status_as_per_source": entity_data.customer_document_information.fcrn.status_as_per_source
                };
                customer_document_information.push(cin_details)
            }
            if (entity_data.customer_document_information.fllpin) {
                var cin_details = {
                    "document": "FLLPIN",
                    "number": entity_data.customer_document_information.fllpin.number,
                    "status": entity_data.customer_document_information.fllpin.status,
                    "status_as_per_source": entity_data.customer_document_information.fllpin.status_as_per_source
                };
                customer_document_information.push(cin_details)
            }
            if (entity_data.customer_document_information.excise) {
                var cin_details = {
                    "document": "Excise",
                    "number": entity_data.customer_document_information.excise.number,
                    "status": entity_data.customer_document_information.excise.status,
                    "status_as_per_source": entity_data.customer_document_information.excise.status_as_per_source
                };
                customer_document_information.push(cin_details)
            }
            if (entity_data.customer_document_information.vat) {
                var vat_details = {
                    "document": "VAT",
                    "number": entity_data.customer_document_information.vat.number,
                    "status": entity_data.customer_document_information.vat.status,
                    "status_as_per_source": entity_data.customer_document_information.vat.status_as_per_source
                };
                customer_document_information.push(vat_details)
            }
            if (entity_data.customer_document_information.cst) {
                var cst_details = {
                    "document": "CST",
                    "number": entity_data.customer_document_information.cst.number,
                    "status": entity_data.customer_document_information.cst.status,
                    "status_as_per_source": entity_data.customer_document_information.cst.status_as_per_source
                };
                customer_document_information.push(cst_details)
            }
            if (entity_data.customer_document_information.pt) {
                var pt_details = {
                    "document": "Professional Tax",
                    "number": entity_data.customer_document_information.pt.number,
                    "status": entity_data.customer_document_information.pt.status,
                    "status_as_per_source": entity_data.customer_document_information.pt.status_as_per_source
                };
                customer_document_information.push(pt_details)
            }
            if (entity_data.customer_document_information.passport) {
                var passport_details = {
                    "document": "Passport",
                    "number": entity_data.customer_document_information.passport.number,
                    "status": entity_data.customer_document_information.passport.status,
                    "status_as_per_source": entity_data.customer_document_information.passport.status_as_per_source
                };
                customer_document_information.push(passport_details)
            }
            if (entity_data.customer_document_information.electricity) {
                var electricity_details = {
                    "document": "Electricity",
                    "number": entity_data.customer_document_information.electricity.number,
                    "status": entity_data.customer_document_information.electricity.status,
                    "status_as_per_source": entity_data.customer_document_information.electricity.status_as_per_source
                };
                customer_document_information.push(electricity_details)
            }
            
            if(entity_data.customer_document_information.uan){
            	var uan_details = {
            			"document" : "EPFO UAN Data",
            			"number" : entity_data.customer_document_information.uan.number,
            			"status" : entity_data.customer_document_information.uan.status,
            			"status_as_per_source" : entity_data.customer_document_information.uan.status_as_per_source
            	};
            	customer_document_information.push(uan_details);
            }
            
            if (entity_data.customer_document_information.telephone) {
                var telephone_details = {
                    "document": "Telephone",
                    "number": entity_data.customer_document_information.telephone.number,
                    "status": entity_data.customer_document_information.telephone.status,
                    "status_as_per_source": entity_data.customer_document_information.telephone.status_as_per_source
                };
                customer_document_information.push(telephone_details)
            }
            if(entity_data.customer_document_information.lpg){
            	var lpg_details = {
            			"document" : "LPG Detail",
            			"number" : entity_data.customer_document_information.lpg.number,
            			"status" : entity_data.customer_document_information.lpg.status,
            			"status_as_per_source" : entity_data.customer_document_information.lpg.status_as_per_source
            	};
            	customer_document_information.push(lpg_details);
            }
            if(entity_data.customer_document_information.esic){
            	var esic_details = {
            			"document" : "ESIC Detail",
            			"number" : entity_data.customer_document_information.esic.number,
            			"status" : entity_data.customer_document_information.esic.status,
            			"status_as_per_source" : entity_data.customer_document_information.esic.status_as_per_source
            	};
            	customer_document_information.push(esic_details);
            }
            
        }
        console.debug("Returning the Customer Document Information Data",customer_document_information);
        return customer_document_information;
    }

    function getFormType(entity_type) {
        var form_type;
        if (entity_type == 'Company' || entity_type == 'Partnership' || entity_type == 'Limited Liability Partnership (LLP)' || entity_type == 'Association of Persons (AOP)' || entity_type == 'Society' || entity_type == 'Body of Individuals (BOI)' || entity_type == 'Trust') {
            form_type = "non-individuals"
        }
        else if (entity_type == 'Proprietory Concern' || entity_type == 'Hindu Undivided Family (HUF)') {
            form_type = "others"
        }
        else {
            form_type = "individuals"
        }
        return form_type;
    }

    function getName(data, form_type) {
        var name = "";
        if (form_type == "individuals") {
            name = data.first_name + " " + data.middle_name + " " + data.last_name;
        }
        else {
            name = data.entity_name;
        }
        return name;
    }

    $scope.getDOBLabel = function (entity) {
    	/*entity.entity_type="individuals"//Remove this ine later o */       
        if (entity == "individuals") {
            return "Date of Birth";
        }
        else {
            return "Date of Formation";
        }
    };

    function getCustomerInformationHeading(entity_type) {
        var heading = "";
        if (entity_type == "Individual") {
            heading = "Customer Information"
        }
        else if (entity_type == "Proprietory Concern") {
            heading = "Proprietory Information"
        }
        else if (entity_type == "Hindu Undivided Family (HUF)") {
            heading = "HUF Information"
        }
        else if (entity_type == "Partnership") {
            heading = "Partnership Information"
        }
        else if (entity_type == "Limited Liability Partnership (LLP)") {
            heading = "LLP Information"
        }
        else if (entity_type == "Company") {
            heading = "Company Information"
        }
        else if (entity_type == "Trust") {
            heading = "Trust Information"
        }
        else if (entity_type == "Association of Persons (AOP)") {
            heading = "AOP Information"
        }
        else if (entity_type == "Body of Individuals (BOI)") {
            heading = "BOI Information"
        }
        else if (entity_type == "Society") {
            heading = "Society Information"
        }
        return heading;
    }

    $scope.getValue = function (key, value) {
        if (key == "Date of last modification") {
            value = value.split(" ")[0];
            value = $filter('date')(value, "dd-MM-yyyy");
        }
        return value;
    };

    $scope.getNameRow = function (entity_data, document_data) {
        var name_row = [];
        name_row.push("P");
        name_row.push("<");
        var count = 2;
        var country_code = entity_data.country.substring(0, 3).toUpperCase();
        for (var i = 0; i < country_code.length; i++) {
            name_row.push(country_code[i]);
            count++;
        }
        var last_name = entity_data.last_name.toUpperCase();
        for (var i = 0; i < last_name.length; i++) {
            if (last_name[i] == " ") {
                name_row.push("<");
            }
            else {
                name_row.push(last_name[i]);
            }
            count++;
        }
        name_row.push("<");
        name_row.push("<");
        count = count + 2;
        var first_name = entity_data.first_name.toUpperCase();
        for (var i = 0; i < first_name.length; i++) {
            if (first_name[i] == " ") {
                name_row.push("<");
            }
            else {
                name_row.push(first_name[i]);
            }
            count++;
        }
        name_row.push("<");
        count = count + 1;
        var middle_name = entity_data.middle_name.toUpperCase();
        for (var i = 0; i < middle_name.length; i++) {
            if (middle_name[i] == " ") {
                name_row.push("<");
            }
            else {
                name_row.push(middle_name[i]);
            }
            count++;
        }
        for (var i = count; i < 44; i++) {
            name_row.push("<");
        }
        return name_row;
    };

    $scope.getChecksumRow = function (entity_data, document_data) {
        var checksum_row = [];
        var count = 0;
        var passportNo = document_data.number + "";
        for (var i = 0; i < passportNo.length; i++) {
            checksum_row.push(passportNo[i]);
            count++;
        }
        checksum_row.push("<");
        count++;
        checksum_row.push(document_data.checksum.checkdigit1);
        count++;
        var country_code = entity_data.country.substring(0, 3).toUpperCase();
        for (var i = 0; i < country_code.length; i++) {
            checksum_row.push(country_code[i]);
            count++;
        }

        var dob = entity_data.dob;
        dob = $filter('date')(new Date(dob), "yyMMdd");
        for (var i = 0; i < dob.length; i++) {
            checksum_row.push(dob[i]);
            count++;
        }
        checksum_row.push(document_data.checksum.checkdigit2);
        count++;
        var gender = entity_data.gender;
        if (gender) {
            checksum_row.push(gender[0].toUpperCase());
            count++;
        }
        var expiry_date = document_data.expiry_date;
        expiry_date = $filter('date')(new Date(expiry_date), "yyMMdd");
        for (var i = 0; i < expiry_date.length; i++) {
            checksum_row.push(expiry_date[i]);
            count++;
        }
        checksum_row.push(document_data.checksum.checkdigit3);
        count++;
        for (var i = count; i < 43; i++) {
            checksum_row.push("<");
        }
        checksum_row.push(document_data.checksum.checkdigit4);
        return checksum_row;
    };

    $scope.print = function () {
        window.print();
    }
});