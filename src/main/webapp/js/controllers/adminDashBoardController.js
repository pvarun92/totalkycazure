instaKYC.controller('adminDashBoardController', function ($scope, $uibModal, CommonServices, dataFactory) {

    $scope.userRole = CommonServices.getCurrentUserRole();
    $scope.page = {"customers": [], "users": []};

    $scope.customerTypes = [
        "Scheduled Commercial Bank (Large)",
        "Urban Co-operative Bank (Small)",
        "Urban Co-operative Bank (Medium)",
        "Retail"
    ];

    $scope.getAdminDashboardData = function () {
        dataFactory.showCustomer()
            .then(function (data) {
                    $scope.page.customers = data;
                    if (data[0]) {
                        $scope.selectCustomer(data[0])
                    }
                }
            );
    };

    $scope.resetCustomer = function (account) {
        $scope.selectedCustomer = {};
    };

    $scope.selectUser = function (user) {
        $scope.selectedUser = angular.copy(user);
    };

    $scope.resetUser = function (user) {
        $scope.selectedUser = {};
    };

    $scope.createUser = function (user) {
        user.customerMasterId = $scope.selectedCustomer.id;
        dataFactory.createCustomerUser(user)
            .then(function (data) {
                    $scope.getUsersByCustomer($scope.selectedCustomer);
                    if (data.result == "Customer User Saved") {
                        $scope.showMessagePopup("Message", "Record submitted successfully")
                    }
                }
            );
    };

    $scope.updateUser = function (user) {
        dataFactory.updateCustomerUser(user)
            .then(function (data) {
                    $scope.getUsersByCustomer($scope.selectedCustomer);
                    if (data.result == "Customer User Updated") {
                        $scope.showMessagePopup("Message", "Record updated successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not Updated")
                    }
                }
            );
    };

    $scope.deleteUser = function (user) {
        dataFactory.deleteCustomerUser(user)
            .then(function (data) {
                    $scope.getUsersByCustomer($scope.selectedCustomer);
                    if (data.result == "User Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };

    $scope.selectCustomer = function (customer) {
        $scope.selectedCustomer = customer;
        $scope.getUsersByCustomer($scope.selectedCustomer)
    };

    $scope.getUsersByCustomer = function (customer) {
        dataFactory.getCustomerUsers(customer.id)
            .then(function (data) {
                $scope.page.users = data;
            });
    };

    $scope.createCustomer = function (customer) {
        dataFactory.createCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Saved") {
                        $scope.showMessagePopup("Message", "Record submitted successfully")
                    }
                }
            );
    };

    $scope.updateCustomer = function (customer) {
        dataFactory.updateCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Updated") {
                        $scope.showMessagePopup("Message", "Record updated successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not Updated")
                    }
                }
            );
    };

    $scope.deleteCustomer = function (customer) {
        dataFactory.deleteCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.getAdminDashboardData();
});