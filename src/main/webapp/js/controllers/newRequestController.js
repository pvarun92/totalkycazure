/**
 * Created by Fallon Software on 2/24/2016.
 */
instaKYC.controller('newRequestController', function ($scope, $uibModal, $rootScope, $interval, $filter, CommonServices, $http, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();
    $rootScope.active_li = "new_request";
    $scope.branches = []
    var submitted = false
    var form_invalid = false;
    var itrRowCount = 1;
    $scope.assessment_years = [];
    $scope.getBranches = function () {
        dataFactory.branchesbyUser()
            .then(function (data) {
                $scope.branches = data;
            });
    }
    
    $scope.getBoards = function() {
		dataFactory.allElectricityBoard()
		.then(function(data){
			$scope.boards = data;
			console.debug("Hello Elect Data ",$scope.boards);
			console.debug("Hello Data",data);
		});
	}
    
    $scope.changeElectricityBoard = function(){
    //	console.log("Hello logged in changed board value ",$scope.boards[0].electricityBoardName);
    };
    
    $scope.isNumber = function(event){
    	var charCode = (event.which) ? event.which : event.keyCode;
 	   console.log(charCode);
 	   console.log("Here i 'm in inNumber method");
 	      if (charCode != 45 && charCode > 31
 	      && (charCode < 48 || charCode > 57)){
 	       return false;
 	      }
 	    return true;
    	
    }
    
    $scope.checkkey = function(event){
	  var regExp ="/[^a-zA-Z1-9/][^a-zA-Z1-9]+$/g";
	  if(event.matches(regExp)){
		  return true;
	  }
	  return false;
    }

    function isNumber(event){
   	 var charCode = (event.which) ? event.which : event.keyCode;
	   console.log("Here i'm In isNumber Method",charCode);
	      if (charCode != 45 && charCode > 31
	      && (charCode < 48 || charCode > 57)){
	       return false;
	      }
	    return true;
    };
    $scope.changeItrStatus = function (selectedTab) {
        if (!$scope.form_data[selectedTab].pan.check) {
            $scope.form_data[selectedTab].itr.check = false
            $scope.form_data[selectedTab].itr.itr_fields = [
                {
                    "assesment_year": "",
                    "acknowledgement_number": "",
                    "date_of_filling": "",
                    "file": "",
                    date_of_filling_datepicker: {
                        opened: false
                    },
                    "assesment_year_touched": false,
                    "date_of_filling_touched": false,
                    "acknowledgement_number_touched": false,
                    "errorRequired": {
                        "acknowledgement_number": {
                            "unique_no": false
                        },
                        "date_of_filling": {
                            "unique_no": false
                        },
                        "assesment_year": {
                            "unique_no": false
                        },
                        "invalid_date": false
                    },
                    invalid: ""
                }
            ]
        }
    };

    $scope.checkMaxRequestReached = function () {
        $scope.maxRequestReached = CommonServices.getParameterByName("maxRequestReached");
        if ($scope.maxRequestReached) {
            $scope.showMessagePopup("Error", "You have reached max limit to create request", "simple_sidebar_menu?navigate_to=landing");
        }
    };

    $scope.getAssessmentYears = function () {
        dataFactory.getAssessmentYears()
            .then(function (data) {
                $scope.assessment_years = data;
            });
    };

    $scope.withoutSlashandZero = function(event) {
    	var regExp = "(^[^a-zA-Z0-9/]+$)|(^[0/]+$)/g";
    	if(event.match(regExp)){
    		return false;
    	}
    	return true;
    };
    
    $scope.isPanSelected = function (selectedTab) {
        if ($scope.form_data[selectedTab].itr.check == true) {
            if ($scope.form_data[selectedTab].pan.check == true) {
                if ($scope.form_data[selectedTab].pan.unique_no != '') {
//                    $scope.validatePan(selectedTab);
                    $scope.getAssessmentYears();
                }
                else {
                    $scope.form_data[selectedTab].itr.check = false;
                    $scope.showMessagePopup("Error", "Please Enter Valid Pan Number");
                }
            }
            else {
                $scope.form_data[selectedTab].itr.check = false;
                $scope.showMessagePopup("Error", "Please select pan before ITR");
            }
        }

    };

    $scope.checkState = function (selectedTab) {
        if ($scope.form_data[selectedTab].voting.voter_state != "Maharashtra") {
            $scope.form_data[selectedTab].voting.voter_state = "Maharashtra";
            $scope.showMessagePopup("Message", "System is working only for maharashtra state")
        }

    };

    $scope.addMoreItrRow = function (selected_tab) {
        var obj = {
            "assesment_year": "",
            "acknowledgement_number": "",
            "date_of_filling": "",
            "file": "",
            date_of_filling_datepicker: {
                opened: false
            },
            "assesment_year_touched": false,
            "date_of_filling_touched": false,
            "acknowledgement_number_touched": false,
            "errorRequired": {
                "acknowledgement_number": {
                    "unique_no": false
                },
                "date_of_filling": {
                    "unique_no": false
                },
                "assesment_year": {
                    "unique_no": false
                },
                "invalid_date": false
            }
        }
        $scope.form_data[selected_tab].itr.itr_fields.push(obj);
    };

    $scope.removeItr = function (selected_tab, selected_itr_row) {
        $scope.form_data[selected_tab].itr.itr_fields.splice(selected_itr_row, 1)
    };

    $scope.doNotChooseFile = function () {
        $scope.showMessagePopup("Message", "File uploading feature is non functional right now. We will update the application soon");
    };

    $scope.freezeDocument = function (docType, selectedTab) {
        if (docType == 'nrega') {
            $scope.form_data[selectedTab].nrega.check = false;
            $scope.showMessagePopup("Message", "NREGA Card verification service is non functional right now. We will update the application soon");
        }
        else  if (docType == 'uan') {
            $scope.form_data[selectedTab].uan.check = false;
            $scope.showMessagePopup("Message", "EPFO verification service is non functional right now. We will update the application soon");
        }
        else if (docType == 'electricity') {
            $scope.form_data[selectedTab].check = false;
            $scope.showMessagePopup("Message", "Electricity Bill verification service is non functional right now. We will update the application soon");
        }
        else if (docType == 'telephone') {
            $scope.form_data[selectedTab].telephone.check = false;
            $scope.showMessagePopup("Message", "Telephone Bill verification feature is non functional right now. We will update the application soon");
        }
        else if (docType == 'registration') {
            $scope.form_data[selectedTab].llpin.check = false;
            $scope.showMessagePopup("Message", "Registration Certificate verification feature is non functional right now. We will update the application soon");
        } else if (docType == 'aadhar') {
            $scope.form_data[selectedTab].aadhar.check = false;
            $scope.showMessagePopup("Message", "Aadhar card verification feature is non functional right now. We will update the application soon");
        } else if (docType == 'dl') {
            $scope.form_data[selectedTab].dl.check = false;
            $scope.showMessagePopup("Message", "Driving licence verification feature is non functional right now. We will update the application soon");
        }

    };

    /* 
     Below Method initalizeFormDataJSON basically prepares a giant JSON object holding fields corresponding to all fields of the
     New Request Page; Additionally it holds two more fields : "errorRequired" and "invalidField" 
    */
    initalizeFormDataJSON();
    //
    $scope.documentsToValidate = [$scope.documents];
    $scope.id = CommonServices.getParameterByName("id");
    console.debug("Value of ID Field ($scope.id)= ",$scope.id)
    var show = CommonServices.getParameterByName("show");
    console.debug("Value of show= ",show)
    if ($scope.id) {
    	console.debug("Valid ID ",$scope.id)
        $scope.reques_label = "Edit Request"
        dataFactory.getEntityDataById($scope.id)
            .then(function (data) {
                console.debug("Obtained Data from DataFactory ",data," ",data.result)
                var formatted_data = reformatData(data);//Formatted Data is an array of Formatted New-Request JSON Data
                $scope.form_data = formatted_data;////Formatted Data is an array of Formatted New-Request JSON Data
                console.log("Value of Formatted Data =======>>>>>>>> ",$scope.form_data);
                $scope.selectedElectricityBoard = $scope.form_data.electricity;
                console.debug("Hi i'm value Received by the Electricity Board---->>",$scope.selectedElectricityBoard);
                $scope.request_status = data[0].request_status;
                if ($scope.request_status == "in_queue") {
                    $scope.reques_label = "View Request";
                }
                for (var i = 0; i < formatted_data.length; i++) {
                    $scope.selectForm(i);
                }
                dataFactory.getAssessmentYears()
                    .then(function (data) {
                        $scope.assessment_years = data;
                    });
            });
    }
    else {
    	console.debug("InValid ID ",$scope.id)
        $scope.reques_label = "New Request"
        initalizeFormDataJSON();
        $scope.form_data = [$scope.initialNewRequestJSONData]
    }

    $scope.form_type = [""]

    function initalizeFormDataJSON() {
    	//Assigns 3 fields for each document type
    	//"check": boolean (By Default : false): It says whether Checkbox for that particular doc is checked or not 
    	//"unique_no": "", Check is via empty string
        //"file": "" , //check is via empty string: May be Wrong way to do it
    	//"captcha": ""
    	//All Keys in below json are strings(Default Behavior): Can be quoted as Str
        $scope.initialNewRequestJSONData = {
            id: "",
            form_id: 0,
            entity_status: "",
            name_label: "Name",
            dob_label: "Date of Birth",
            "reg_certificate_label": "Registration Certificate",
            "request_id": "",
            "entity_id": "",
            "date": new Date().toDateString(),
            "entity_type": "",
            "relation": "Applicant",
            "branch": "",
            "entity": "",
            "full_name": {
                "first_name": "",
                "middle_name": "",
                "last_name": ""
            },
            "father_husband_name": {
                "first_name": "",
                "middle_name": "",
                "last_name": ""
            },
            "address": {
                "address_line1": "",
                "address_line2": "",
                "state": "",
                "city": "",
                "pincode": "",
                "country": "India"
            },
            "dob": "",
            "gender": "",
            "email": "",
            "contact_number": "",
            "pan": {
                "check": false,
                "unique_no": "",
                "file": "",
                "captcha": ""
            },
            "llpin": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "vat": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "cst": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "pt": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "iec": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "excise": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "service_tax": {
                "check": false,
                "unique_no": "",
                "file": "",
                "captcha": ""
            },
            "aadhar": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "dl": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "passport": {
                "check": false,
                "unique_no": "",
                "file": "",
                "expiry_date": ""
            },
            "voting": {
                "check": false,
                "unique_no": "",
                "file": "",
            },
            "nrega": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "electricity": {
                "check": false,
                "board": "",
                "unique_no":"",
                "file": ""
            },
            "telephone": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "uan": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "lpg": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            "esic": {
                "check": false,
                "unique_no": "",
                "file": ""
            },
            itr: {
                "check": false,
                "itr_fields": [
                    {
                        "assesment_year": "",
                        "acknowledgement_number": "",
                        "date_of_filling": "",
                        "file": "",
                        date_of_filling_datepicker: {
                            opened: false
                        },
                        "assesment_year_touched": false,
                        "date_of_filling_touched": false,
                        "acknowledgement_number_touched": false,
                        "errorRequired": {
                            "acknowledgement_number": {
                                "unique_no": false
                            },
                            "date_of_filling": {
                                "unique_no": false
                            },
                            "assesment_year": {
                                "unique_no": false
                            },
                            "invalid_date": false
                        },
                        invalid: ""
                    }
                ]
            },
            "invalid": {
                "full_name": false,
                "address": {
                    "pincode": {
                        select_state: false,
                        invalid_pin: false
                    }
                },
                "dob": false,
                "email": false,
                "contact_number": false,
                "passport": false
            },
            "errorRequired": {
                "entity": false,
                "full_name": {
                    "first_name": false,
                    "middle_name": false,
                    "last_name": false
                },
                "address": {
                    "address_line1": false,
                    "address_line2": false,
                    "state": false,
                    "pincode": false
                },
                "dob": false,
                "gender": false,
                "email": false,
                "contact_number": false,
                "pan": {
                    "unique_no": false
                },
                "llpin": {
                    "unique_no": false
                },
                "vat": {
                    "unique_no": false
                },
                "cst": {
                    "unique_no": false
                },
                "pt": {
                    "unique_no": false
                },
                "iec": {
                    "unique_no": false
                },
                "excise": {
                    "unique_no": false
                },
                "service_tax": {
                    "unique_no": false
                },
                "aadhar": {
                    "unique_no": false
                },
                "dl": {
                    "unique_no": false
                },
                "passport": {
                    "unique_no": false,
                    "expiry_date": false
                },
                "voting": {
                    "unique_no": false
                },
                "nrega": {
                    "unique_no": false
                },
                "electricity": {
                	"unique_no": false,
                	"board": ""
                },
                "telephone": {
                    "unique_no": false
                },
                "uan": {
                    "unique_no": false
                },
                "lpg": {
                    "unique_no": false
                },
                "esic": {
                    "unique_no": false
                }
            }
        }

        $scope.documents = {
            "pan": "",
            "llpin": "",
            "vat": "",
            "cst": "",
            "pt": "",
            "iec": "",
            "excise": "",
            "service_tax": "",
            "aadhar": "",
            "dl": "",
            "passport": "",
            "voting": "",
            "nrega": "",
            "electricity": "",
            "telephone": "",
            "uan": "",
            "lpg" :"",
            "esic" :"",
            "acknowledgement_number": "",
            "date_of_filling": "",
            "assesment_year": "",
            "assesment_year_touched": false,
            "date_of_filling_touched": false,
            "acknowledgement_number_touched": false,
            "email": ""
        }
    }

    $scope.counter = 0;
    /** Function to add a new tab **/
    $scope.addTab = function () {
        if ($scope.userRole != 'View' && $scope.request_status != 'in_queue') {
            initalizeFormDataJSON(); // initialize the form data for a new Tab
            $scope.counter++;     //increment the counter to reflect another tab has been added
            $scope.initialNewRequestJSONData.form_id = $scope.counter; //Assigning the ID to Form Request data for other tab
            if ($scope.form_data[$scope.form_data.length - 1]) {
                $scope.initialNewRequestJSONData.branch = $scope.form_data[$scope.form_data.length - 1].branch;
            }
            $scope.form_data.push($scope.initialNewRequestJSONData);
            $scope.documentsToValidate.push($scope.documents);//Documents which has to be validated (PAN,AADHAR,DL etc)
            $scope.form_type.push("");
            $scope.selectedTab = $scope.form_data.length - 1; //set the newly added tab active.
        }
    }

    /** Function to delete a tab **/
    $scope.deleteTab = function (index) {
        if ($scope.userRole != 'View' && $scope.request_status != 'in_queue') {
            var modalInstance = $uibModal.open({
                templateUrl: 'deleteTab.html',
                controller: function ($scope) {
                    $scope.yes = function () {
                        deleteTab(index);
                        modalInstance.dismiss('cancel');
                    },
                        $scope.close = function () {
                            modalInstance.dismiss('cancel');
                        }
                },
                size: 'sm'
            });

            var deleteTab = function (index) {
                if ($scope.form_data[index] && $scope.form_data[index].id) {
                	//Below method will delete the Tab and remove the data as well
                    dataFactory.deleteEntity({selectedRequestId: $scope.form_data[index].id})
                        .then(function (data) {
//                        $scope.showMessagePopup("Message","Draft(s) discarded")       ;
//                        $scope.getDraftsData();
                        });
                }
                if ($scope.form_data.length - 1 > index) {
                    for (var i = index; i < $scope.form_data.length - 1; i++) {
                        $scope.form_data[i] = $scope.form_data[i + 1]
                        $scope.documentsToValidate[i] = $scope.documentsToValidate[i + 1]
                     // type of form whether its of individual or non-individual type
                        $scope.form_type[i] = $scope.form_type[i + 1]; 
                    }
                }
	     $scope.form_data.splice($scope.form_data.length - 1, 1); //remove the object from the array based on index
	     $scope.documentsToValidate.splice($scope.documentsToValidate.length - 1, 1);
	     $scope.form_type.splice($scope.form_type.length - 1, 1);// it removes the Individual/Non-Individual form_type
	                $scope.selectTab(index)
            }
        }
    };

    $scope.selectedTab = 0; //set selected tab to the 1st by default.

    /** Function to set selectedTab **/
    $scope.selectTab = function (index) {
        if ($scope.form_data.length - 1 > index) {
            $scope.selectedTab = index;
        }
        else {
            $scope.selectedTab = $scope.form_data.length - 1;
        }
    };

// this method is written in order to select the menu from the drop down menu for Individuals/ Non-Individuals
    $scope.selectForm = function (selectedTab) {
        if ($scope.form_data[selectedTab].entity_type == 'Company' || $scope.form_data[selectedTab].entity_type == 'Partnership' || $scope.form_data[selectedTab].entity_type == 'Limited Liability Partnership (LLP)' || $scope.form_data[selectedTab].entity_type == 'Association of Persons (AOP)' || $scope.form_data[selectedTab].entity_type == 'Society' || $scope.form_data[selectedTab].entity_type == 'Body of Individuals (BOI)' || $scope.form_data[selectedTab].entity_type == 'Trust') {
            $scope.form_type[selectedTab] = "non-individuals"
        }
        else if ($scope.form_data[selectedTab].entity_type == 'Proprietory Concern' || $scope.form_data[selectedTab].entity_type == 'Hindu Undivided Family (HUF)') {
            $scope.form_type[selectedTab] = "others"
        }
        else {
            $scope.form_type[selectedTab] = "individuals"
        }
        $scope.form_data[selectedTab].name_label = getNameDOBLabel($scope.form_data[selectedTab].entity_type).name_label;
        $scope.form_data[selectedTab].dob_label = getNameDOBLabel($scope.form_data[selectedTab].entity_type).dob_label;
        $scope.form_data[selectedTab].reg_certificate_label = getNameDOBLabel($scope.form_data[selectedTab].entity_type).reg_certificate_label;

//        $scope.clearForm(selectedTab);
    }

    $scope.clearForm = function (selectedTab) {

        $scope.documents = {
            "pan": "",
            "llpin": "",
            "vat": "",
            "cst": "",
            "pt": "",
            "iec": "",
            "excise": "",
            "service_tax": "",
            "aadhar": "",
            "dl": "",
            "passport": "",
            "voting": "",
            "nrega": "",
            "electricity": "",
            "telephone": "",
             "uan" : "",
             "lpg":"",
             "esic":"",
            "acknowledgement_number": "",
            "date_of_filling": "",
            "assesment_year": "",
            "assesment_year_touched": false,
            "date_of_filling_touched": false,
            "acknowledgement_number_touched": false,
            "email": ""
        };
        //$scope.documentsToValidate = [$scope.documents];

        $scope.form_data[selectedTab].errorRequired = {
            "entity": false,
            "full_name": {
                "first_name": false,
                "middle_name": false,
                "last_name": false
            },
            "address": {
                "address_line1": false,
                "address_line2": false,
                "state": false,
                "pincode": false
            },
            "dob": false,
            "gender": false,
            "email": false,
            "contact_number": false,
            "pan": {
                "unique_no": false
            },
            "llpin": {
                "unique_no": false
            },
            "vat": {
                "unique_no": false
            },
            "cst": {
                "unique_no": false
            },
            "pt": {
                "unique_no": false
            },
            "iec": {
                "unique_no": false
            },
            "excise": {
                "unique_no": false
            },
            "service_tax": {
                "unique_no": false
            },
            "aadhar": {
                "unique_no": false
            },
            "dl": {
                "unique_no": false
            },
            "passport": {
                "unique_no": false,
                "expiry_date": false
            },
            "voting": {
                "unique_no": false
            },
            "nrega": {
                "unique_no": false
            },
            "electricity": {
                "unique_no": false,
                "board": ""
            },
            "telephone": {
                "unique_no": false
            },
            "uan": {
                "unique_no": false
            },
            "lpg": {
                "unique_no": false
            },
            "esic": {
                "unique_no": false
            }
        };

        $scope.form_data[selectedTab].llpin.check = false;
        $scope.form_data[selectedTab].llpin.unique_no = "";

        $scope.form_data[selectedTab].pan.check = false;
        $scope.form_data[selectedTab].pan.unique_no = "";

        $scope.form_data[selectedTab].aadhar.check = false;
        $scope.form_data[selectedTab].aadhar.unique_no = "";

        $scope.form_data[selectedTab].dl.check = false;
        $scope.form_data[selectedTab].dl.unique_no = "";

        $scope.form_data[selectedTab].passport.check = false;
        $scope.form_data[selectedTab].passport.unique_no = "";
        $scope.form_data[selectedTab].passport.expiry_date = "";

        $scope.form_data[selectedTab].voting.check = false;
        $scope.form_data[selectedTab].voting.unique_no = "";

        $scope.form_data[selectedTab].vat.check = false;
        $scope.form_data[selectedTab].vat.unique_no = "";

        $scope.form_data[selectedTab].cst.check = false;
        $scope.form_data[selectedTab].cst.unique_no = "";

        $scope.form_data[selectedTab].pt.check = false;
        $scope.form_data[selectedTab].pt.unique_no = "";

        $scope.form_data[selectedTab].iec.check = false;
        $scope.form_data[selectedTab].iec.unique_no = "";

        $scope.form_data[selectedTab].excise.check = false;
        $scope.form_data[selectedTab].excise.unique_no = "";
        
        $scope.form_data[selectedTab].telephone.check = false;
        $scope.form_data[selectedTab].telephone.unique_no = "";

        $scope.form_data[selectedTab].electricity.check = false;
        $scope.form_data[selectedTab].electricity.board ="";
        $scope.form_data[selectedTab].electricity.unique_no = "";
        
        $scope.form_data[selectedTab].service_tax.check = false;
        $scope.form_data[selectedTab].service_tax.unique_no = "";
        
        $scope.form_data[selectedTab].uan.check = false;
        $scope.form_data[selectedTab].uan.unique_no = "";
        $scope.form_data[selectedTab].lpg.check = false;
        $scope.form_data[selectedTab].lpg.unique_no = "";
        $scope.form_data[selectedTab].esic.check = false;
        $scope.form_data[selectedTab].esic.unique_no = "";

        $scope.form_data[selectedTab].full_name.first_name = "";
        $scope.form_data[selectedTab].full_name.middle_name = "";
        $scope.form_data[selectedTab].full_name.last_name = "";
        $scope.form_data[selectedTab].father_husband_name.first_name = "";
        $scope.form_data[selectedTab].father_husband_name.middle_name = "";
        $scope.form_data[selectedTab].father_husband_name.last_name = "";
        $scope.form_data[selectedTab].address.address_line1 = "";
        $scope.form_data[selectedTab].address.address_line2 = "";
        $scope.form_data[selectedTab].address.state = "";
        $scope.form_data[selectedTab].address.pincode = "";
        $scope.form_data[selectedTab].dob = "";
        $scope.form_data[selectedTab].gender = "";
        $scope.form_data[selectedTab].email = "";
        $scope.form_data[selectedTab].contact_number = "";
        $scope.form_data[selectedTab].entity = "";

    };


    $scope.discardForm = function () {
        if ($scope.form_data) {
            var entity = $scope.form_data[0];
            if (entity && entity.request_id) {
                dataFactory.deleteRequest({requestId: entity.request_id})
                    .then(function (data) {
                        //console.log(data.result)
                        $scope.showMessagePopup("Message", "Request Discarded", "simple_sidebar_menu?navigate_to=drafts")
                    });
            }
        }
        initalizeFormDataJSON()
        $scope.form_data = [$scope.initialNewRequestJSONData];
        $scope.documentsToValidate = [$scope.documents];
        $scope.form_type = [""]
        $scope.selectedTab = $scope.form_data.length - 1;

    }

    $scope.deleteForm = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'delete_request.html',
            scope: $scope,
            controller: function ($scope) {
                $scope.yes = function () {
                    $scope.discardForm();
                    modalInstance.dismiss('cancel');
                }
                $scope.no = function () {
                    modalInstance.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    }

    $scope.saveForm = function () {
        form_invalid = false;
        for (var i = 0; i < $scope.form_data.length; i++) {
            validateSave(i);
        }
        setTimeout(function () {
            if (form_invalid) {
                $scope.showMessagePopup("Message", "Please enter entity name")
            }
            else {
                $scope.form_data[0].entity_status = "parent"
                	console.debug("Saving the Form Data ",$scope.form_data);
                dataFactory.saveRequest($scope.form_data)
                    .then(function (data) {
                            if (data.result == "Request Saved") {
                                $scope.showMessagePopup("Message", "Record saved successfully")
                                var customerEntityids = data.customerEntityids;
                                for (var i = 0; i < $scope.form_data.length; i++) {
                                    if (!$scope.form_data[i].id) {
                                        $scope.form_data[i].id = customerEntityids[$scope.form_data[i].form_id];
                                    }
                                    if (!$scope.form_data[i].request_id) {
                                        $scope.form_data[i].request_id = customerEntityids.request_id;
                                    }
                                }
                            }
                            else if (data.invalid) {
                                $scope.documentsToValidate = data.invalid;
                            }
                        }
                    );
            }
        }, 2000);
    };

  
    //Create a Utility function and put it in a separate JS File; Util Kind-of 
    $scope.autoSaveFormData = function () {
        var invalid_basic_info = false;
        for (var i = 0; i < $scope.form_data.length; i++) {
            if (!$scope.form_data[i].entity_type || !$scope.form_data[i].branch || !$scope.form_data[i].relation) {
                invalid_basic_info = true
            }
            if ($scope.form_type[i] == "non-individuals" || $scope.form_type[i] == "others") {
                if (!$scope.form_data[i].entity) {
                    invalid_basic_info = true
                }
            }
            if ($scope.form_type[i] == "individuals") {
                if (!$scope.form_data[i].full_name.first_name && !$scope.form_data[i].full_name.last_name && !$scope.form_data[i].full_name.middle_name) {
                    invalid_basic_info = true
                }
            }
        }
        console.debug("Invalid_Basic_Info ~~>", invalid_basic_info)
        setTimeout(function() {
        if (!invalid_basic_info) {
        	console.debug("Going to AutoSave the Form Data")
            $scope.form_data[0].entity_status = "parent"
            	console.debug("Hi i'm form data that 2 b saved",$scope.form_data);
        	console.debug($scope.form_data)
            dataFactory.saveRequest($scope.form_data)
                .then(function (data) {
                	console.debug(data)
                        if (data.result == "Request Saved") {
                        	console.debug("Form Data AutoSaved")
                            var customerEntityids = data.customerEntityids;
                            for (var i = 0; i < $scope.form_data.length; i++) {
                            	console.debug("AutoSaved FormData is ",$scope.form_data[i])
                                if (!$scope.form_data[i].id) {
                                    $scope.form_data[i].id = customerEntityids[$scope.form_data[i].form_id];
                                    console.debug($scope.form_data[i].id);
                                }
                                if (!$scope.form_data[i].request_id) {
                                    $scope.form_data[i].request_id = customerEntityids.request_id;
                                    console.debug($scope.form_data[i].request_id);
                                }
                            }
                            console.debug("Form Data Saved Successfully");
                        }
                    }
                );
           }
        }, 5000);
    };

    $interval(function () {
        if (!submitted && $scope.userRole != 'View' && $scope.request_status != 'in_queue' && $scope.request_status != 'submitted') {
        	$scope.autoSaveFormData();
        }
    }, 30000);

    $scope.submitForm = function (selectedTab) {
        form_invalid = false;
        var dirty = false;
        validateForm(selectedTab);
        if (($scope.form_type[selectedTab] == 'non-individuals' || $scope.form_type[selectedTab] == 'others') && !$scope.form_data[selectedTab].entity) {
            dirty = true;
            $scope.showMessagePopup("Error", "Please fill entity name field")
        }
        else if (($scope.form_type[selectedTab] == 'non-individuals' || $scope.form_type[selectedTab] == 'others') && !$scope.form_data[selectedTab].entity && $scope.form_data[selectedTab].electricity.unique_no && !$scope.form_data[selectedTab].electricity.board && $scope.form_data[selectedTab].electricity.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Electricity Board is required to be selected")
        }
        else if (($scope.form_type[selectedTab] == 'non-individuals' || $scope.form_type[selectedTab] == 'others') && !$scope.form_data[selectedTab].entity && !$scope.form_data[selectedTab].electricity.unique_no && $scope.form_data[selectedTab].electricity.board && $scope.form_data[selectedTab].electricity.check)  {
            dirty = true;
            $scope.showMessagePopup("Error", "Electricity Number is Required to be filled")
        }
        
        else if ($scope.form_type[selectedTab] == 'individuals' && !($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name)) {
            dirty = true;
            $scope.showMessagePopup("Error", "Please fill name field")
        }
        else if ($scope.form_type[selectedTab] == 'individuals' && $scope.form_data[selectedTab].full_name.first_name && $scope.form_data[selectedTab].passport.unique_no && !$scope.form_data[selectedTab].dob && $scope.form_data[selectedTab].passport.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Date of Birth field is required for passport bar code generation")
        }
        else if ($scope.form_data[selectedTab].passport.check && (!$scope.form_data[selectedTab].passport.unique_no || $scope.form_data[selectedTab].passport.unique_no == '')) {
            dirty = true;
            $scope.showMessagePopup("Error", "Passport Number field is required for passport bar code generation")
        }
        else if ($scope.form_data[selectedTab].full_name.first_name && $scope.form_data[selectedTab].passport.unique_no && $scope.form_data[selectedTab].dob && !$scope.form_data[selectedTab].passport.expiry_date) {
            dirty = true;
            $scope.showMessagePopup("Error", "Expiry Date field is required for passport bar code generation")
        }
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].aadhar.unique_no && !$scope.form_data[selectedTab].dob && $scope.form_data[selectedTab].aadhar.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Date of Birth field is required for aadhar card")
        }
       /* else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].aadhar.unique_no && !$scope.form_data[selectedTab].gender && $scope.form_data[selectedTab].aadhar.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Gender field is required for aadhar card")
        }*/
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].aadhar.unique_no && !$scope.form_data[selectedTab].address.state && $scope.form_data[selectedTab].aadhar.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "State is required for aadhar card")
        }
        
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].aadhar.unique_no && !$scope.form_data[selectedTab].address.pincode && $scope.form_data[selectedTab].aadhar.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "PinCode field is required for aadhar card")
        }
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].dl.unique_no && !$scope.form_data[selectedTab].dob && $scope.form_data[selectedTab].dl.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Date of Birth is Required for Driving Licence")
        }
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].uan.unique_no && !$scope.form_data[selectedTab].contact_number && $scope.form_data[selectedTab].uan.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Mobile Number is required for EPFO UAN")
        }
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && $scope.form_data[selectedTab].electricity.unique_no && !$scope.form_data[selectedTab].electricity.board && $scope.form_data[selectedTab].electricity.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Electricity Board is required to be selected")
        }
        else if ($scope.form_type[selectedTab] == 'individuals' && ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.middle_name || $scope.form_data[selectedTab].full_name.last_name) && !$scope.form_data[selectedTab].electricity.unique_no && $scope.form_data[selectedTab].electricity.board && $scope.form_data[selectedTab].electricity.check) {
            dirty = true;
            $scope.showMessagePopup("Error", "Electricity Number is Required to be filled")
        }

        //Additional Dirty Checks specific to ITR
        if (!dirty) {
            if ($scope.form_data[selectedTab].itr.check) {
                for (var i = 0; i < $scope.form_data[selectedTab].itr.itr_fields.length; i++) {
                    if (!$scope.form_data[selectedTab].itr.itr_fields[i].assesment_year) {
                        dirty = true;
                        $scope.showMessagePopup("Error", "Please fill Assessment Year field")
                    }
                    else if (!$scope.form_data[selectedTab].itr.itr_fields[i].date_of_filling) {
                        dirty = true;
                        $scope.showMessagePopup("Error", "Please fill Date Of Filling field")
                    }
                    else if (!$scope.form_data[selectedTab].itr.itr_fields[i].acknowledgement_number) {
                        dirty = true;
                        $scope.showMessagePopup("Error", "Please fill Acknowledgement Year field")
                    }
                }
            }
        }

        //Check whether all fields on the browser are correctly entered
        if (!dirty) {
            setTimeout(function () {
                if (form_invalid) {
                	console.debug("Form_Invalid ",form_invalid ," Hence Need to enter Required Fields");
                    var modalInstance = $uibModal.open({
                        templateUrl: 'invalid_input_msgbox.html',
                        scope: $scope,
                        controller: function ($scope) {
                            $scope.yes = function () {
                                $scope.checkRequiredDocumentsAndSubmit(selectedTab);
                                modalInstance.dismiss('cancel');
                            }

                            $scope.no = function () {
                                modalInstance.dismiss('cancel');
                            }
                        },
                        size: 'sm'
                    });
                }
                else {
                	console.debug("Form is Valid ",form_invalid ," Hence, Submit the request" );
                    $scope.checkRequiredDocumentsAndSubmit(selectedTab);
                }
            }, 2000);
        }
    }

    $scope.checkRequiredDocumentsAndSubmit = function (selectedTab) {
        if (($scope.form_data[selectedTab].pan.check && $scope.form_data[selectedTab].pan.unique_no) ||
            ($scope.form_data[selectedTab].llpin.check && $scope.form_data[selectedTab].llpin.unique_no) ||
            ($scope.form_data[selectedTab].vat.check && $scope.form_data[selectedTab].vat.unique_no) ||
            ($scope.form_data[selectedTab].cst.check && $scope.form_data[selectedTab].cst.unique_no) ||
            ($scope.form_data[selectedTab].pt.check && $scope.form_data[selectedTab].pt.unique_no) ||
            ($scope.form_data[selectedTab].iec.check && $scope.form_data[selectedTab].iec.unique_no) ||
            ($scope.form_data[selectedTab].excise.check && $scope.form_data[selectedTab].excise.unique_no) ||
            ($scope.form_data[selectedTab].service_tax.check && $scope.form_data[selectedTab].service_tax.unique_no) ||
            ($scope.form_data[selectedTab].aadhar.check && $scope.form_data[selectedTab].aadhar.unique_no) ||
            ($scope.form_data[selectedTab].dl.check && $scope.form_data[selectedTab].dl.unique_no) ||
            ($scope.form_data[selectedTab].passport.check && $scope.form_data[selectedTab].passport.unique_no) ||
            ($scope.form_data[selectedTab].voting.check && $scope.form_data[selectedTab].voting.unique_no) ||
            ($scope.form_data[selectedTab].nrega.check && $scope.form_data[selectedTab].nrega.unique_no) ||
            (
            		$scope.form_data[selectedTab].electricity.check 
            		&&($scope.form_data[selectedTab].electricity.unique_no!=""||
            			$scope.form_data[selectedTab].electricity.unique_no!=undefined
            			)
            		&& $scope.form_data[selectedTab].electricity.board 
            ) ||
            ($scope.form_data[selectedTab].telephone.check && $scope.form_data[selectedTab].telephone.unique_no) ||
            ($scope.form_data[selectedTab].uan.check && $scope.form_data[selectedTab].uan.unique_no) ||
            ($scope.form_data[selectedTab].lpg.check && $scope.form_data[selectedTab].lpg.unique_no)||
            ($scope.form_data[selectedTab].esic.check && $scope.form_data[selectedTab].esic.unique_no)
        ) {
        	console.debug("New-Request Form Data is Validated at Client Side : Submitting Request to Server")
            $scope.submitRequest();
        }
        else {
            $scope.showMessagePopup("Error", "Please fill KYC document fields")
        }
    }

    function hasDocumentWithoutCaptchaInForm(form_data) {
        var document_without_captcha_in_form = [];
        for (var i = 0; i < form_data.length; i++) {
            if (form_data[i].pan.check) {
                if (form_data[i].pan.unique_no) {
                    if (!form_data[i].pan.captcha) {
                        var pan_details = {
                            "pan": form_data[i].pan
                        }
                        document_without_captcha_in_form.push(pan_details)
                    }
                }
            }
            else if (form_data[i].service_tax.check) {
                if (form_data[i].service_tax.unique_no) {
                    if (!form_data[i].service_tax.captcha) {
                        var service_tax_details = {
                            "service_tax": form_data[i].service_tax
                        }
                        document_without_captcha_in_form.push(service_tax_details)
                    }
                }
            }
        }
        return document_without_captcha_in_form;
    }

    function getAllCaptcha(document_without_captcha_in_form) {
        dataFactory.getCaptcha(document_without_captcha_in_form)
            .then(function (data) {
                    console.log(data)
                    var captchaDetails = data;
                    var submitRequest = function () {
                        $scope.submitRequest();
                    }
                    var captchaModal = $uibModal.open({
                        templateUrl: 'captcha.html',
                        controller: function ($scope) {
                            $scope.captchaDetails = captchaDetails;
                            $scope.close = function () {
                                captchaModal.dismiss('cancel');
                            }
                            $scope.setCaptcha = function () {
                                for (var i = 0; i < $scope.captchaDetails.length; i++) {
                                    setCaptcha($scope.captchaDetails[i])
                                }
                                submitRequest();
                                captchaModal.dismiss('cancel');
                            }
                        },
                        size: 'md'
                    });
                }
            );
    }

    
    function setCaptcha(captchaDetails) {
        for (var i = 0; i < $scope.form_data.length; i++) {
            if (captchaDetails.document == "pan") {
                if ($scope.form_data[i].pan) {
                    if (captchaDetails.unique_no == $scope.form_data[i].pan.unique_no) {
                        $scope.form_data[i].pan.captcha = captchaDetails.captcha_text;
                    }
                }
            }
            if (captchaDetails.document == "service_tax") {
                if ($scope.form_data[i].service_tax) {
                    if (captchaDetails.unique_no == $scope.form_data[i].service_tax.unique_no) {
                        $scope.form_data[i].service_tax.captcha = captchaDetails.captcha_text;
                    }
                }
            }
        }
    }

    $scope.submitRequest = function () {
    	//entity_status="parent" represents that this is a New-Request
    	//Once edited : entity_status holds the request_id of the original Request
        $scope.form_data[0].entity_status = "parent"
        submitted = true
        dataFactory.submitRequest($scope.form_data)
            .then(function (data) {
                    console.log("Server Response for Submitted Form Data : ",data , "Result ~> ", data.result)
                    if (data.result == "Request Saved") {
                        $scope.showRestrictedMessagePopup("Message", "Record submitted successfully", "simple_sidebar_menu?navigate_to=landing")
                    }
                    else if (data.invalid) {
                        submitted = false
                        $scope.documentsToValidate = data.invalid;
                    }
                }
            );
    }

    //datepicker start
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showPassportCalendar = function () {
        $scope.passport_expiry_datepicker.opened = true;
    };

    $scope.showDateOfFillingCalendar = function (itrObj) {
        itrObj.date_of_filling_datepicker.opened = true;
    }

    $scope.showDOBCalendar = function () {
        $scope.dob_datepicker.opened = true;
    };

    $scope.passport_expiry_datepicker = {
        opened: false
    };

    $scope.dob_datepicker = {
        opened: false
    };

    $scope.selectPassportExpiryDate = function (selectedTab) {
        var converted_date = CommonServices.convertDateInArrayDDMMYYY($scope.passport_expiry_date)
        var convertedDate = $filter('date')($scope.passport_expiry_date, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.form_data[selectedTab].passport.expiry_date = convertedDate
        }
        $scope.validatePassportValidity(selectedTab)
    }

    $scope.selectDateOfFilling = function (itrObj, val) {
        var convertedDate = $filter('date')(val, "dd/MM/yyyy");
        if (convertedDate) {
            itrObj.date_of_filling = convertedDate
        }
    }

    $scope.selectDOBDate = function (selectedTab) {
        var converted_date = CommonServices.convertDateInArrayDDMMYYY($scope.dob)
        var convertedDate = $filter('date')($scope.dob, "dd/MM/yyyy");
        if (convertedDate) {
            $scope.form_data[selectedTab].dob = convertedDate
        }
        $scope.validateDob(selectedTab)
    }
    //datepicker end


    //validation code start
    $scope.validateEntity = function (selectedTab) {
        if (!$scope.form_data[selectedTab].entity) {
            $scope.form_data[selectedTab].errorRequired.entity = true;
            form_invalid = true;
        }
        else {
            $scope.form_data[selectedTab].errorRequired.entity = false;
        }
    }
    
    $scope.validateName = function (selectedTab) {
        if ($scope.form_data[selectedTab].full_name.first_name || $scope.form_data[selectedTab].full_name.last_name || $scope.form_data[selectedTab].full_name.middle_name) {
            $scope.form_data[selectedTab].invalid.full_name = false;
        }
        else {
            $scope.form_data[selectedTab].invalid.full_name = true;
            form_invalid = true;
        }
    }

    $scope.validateFatherName = function (selectedTab) {
        if ($scope.form_data[selectedTab].father_husband_name.first_name || $scope.form_data[selectedTab].father_husband_name.last_name || $scope.form_data[selectedTab].father_husband_name.middle_name) {
            $scope.form_data[selectedTab].invalid.father_husband_name = false;

        }
        else {
            $scope.form_data[selectedTab].invalid.father_husband_name = true;
            form_invalid = true;
        }
    }

    $scope.validateAddress1 = function (selectedTab) {
        if (!$scope.form_data[selectedTab].address.address_line1) {
            $scope.form_data[selectedTab].errorRequired.address.address_line1 = true;
            form_invalid = true;
        }
        else {
            $scope.form_data[selectedTab].errorRequired.address.address_line1 = false;

        }
    }

    $scope.validateState = function (selectedTab) {
        if (!$scope.form_data[selectedTab].address.state) {
            $scope.form_data[selectedTab].errorRequired.address.state = true;
            form_invalid = true;
        }
        else {
            $scope.form_data[selectedTab].errorRequired.address.state = false;
        }
        if ($scope.form_data[selectedTab].address.country) {
            if ($scope.form_data[selectedTab].address.country == "India") {
                if ($scope.form_data[selectedTab].address.pincode) {
                    $scope.validatePincode(selectedTab)
                }
            }
        }
    }

    $scope.validateCountry = function (selectedTab) {
        if (!$scope.form_data[selectedTab].address.country) {
            $scope.form_data[selectedTab].errorRequired.address.country = true;
            form_invalid = true;
        }
        else {
            $scope.form_data[selectedTab].errorRequired.address.country = false;
        }
    }

    
    
    $scope.validatePincode = function (selectedTab) {
        if (!$scope.form_data[selectedTab].address.pincode) {
            $scope.form_data[selectedTab].errorRequired.address.pincode = true;
            form_invalid = true;
        }
        else {
            $scope.form_data[selectedTab].errorRequired.address.pincode = false;
            if ($scope.form_data[selectedTab].address.country) {
                if ($scope.form_data[selectedTab].address.country == "India") {
                    if ($scope.form_data[selectedTab].address.pincode) {
                        $scope.checkPincode(selectedTab);
                    }
                }
            }
        }
    }

    $scope.checkPincode = function (selectedTab) {
        if ($scope.form_data[selectedTab].address.pincode.length < 6) {
            $scope.form_data[selectedTab].invalid.address.pincode.invalid_pin = true
            form_invalid = true;
        }
        else {
            if ($scope.form_data[selectedTab].address.state) {
                $http.get("https://data.gov.in/api/datastore/resource.json?resource_id=6176ee09-3d56-4a3b-8115-21841576b2f6&api-key=58a6787c69ddb1457341dc1e634bbd97&filters[pincode]=" + $scope.form_data[selectedTab].address.pincode)
                    .then(function (response) {
                        var invalid_pincode = false;
                        if (response.status == 200) {
                            if (response.data.records) {
                                if (response.data.records[0]) {
                                    if (response.data.records[0].statename.toLowerCase() != $scope.form_data[selectedTab].address.state.toLowerCase()) {
                                        invalid_pincode = true
                                    }
                                }
                            }
                        }
                        if (invalid_pincode) {
                            $scope.form_data[selectedTab].invalid.address.pincode.invalid_pin = true
                            form_invalid = true;
                        }
                        else {
                            $scope.form_data[selectedTab].invalid.address.pincode.invalid_pin = false
                        }
                        $scope.form_data[selectedTab].invalid.address.pincode.select_state = false
                    });
            }
            else {
                $scope.form_data[selectedTab].invalid.address.pincode.select_state = true
                form_invalid = true;
            }
        }
    };

    $scope.validateDob = function (selectedTab) {
        if ($scope.form_data[selectedTab].dob) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.form_data[selectedTab].dob)
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    $scope.form_data[selectedTab].invalid.dob = true;
                    form_invalid = true;
                }
                else {
                    $scope.form_data[selectedTab].invalid.dob = false;
                }
            }
            else {
                $scope.form_data[selectedTab].invalid.dob = true;
                form_invalid = true;
            }
        }
        else {
            $scope.form_data[selectedTab].invalid.dob = true;
        }
    };

    $scope.validatePassportValidity = function (selectedTab) {
        if ($scope.form_data[selectedTab].passport.check) {
            if ($scope.form_data[selectedTab].passport.expiry_date) {
                var date = CommonServices.getDateFromconvertDDMMYYYYString($scope.form_data[selectedTab].passport.expiry_date)
                if (date && date != "Invalid Date") {
                    var converted_date = $filter('date')(date, "dd/MM/yyyy");
                    if (!converted_date) {
                        $scope.form_data[selectedTab].invalid.passport = true;
                        form_invalid = true;
                    }
                    else {
                        $scope.form_data[selectedTab].invalid.passport = false;
                    }
                }
                else {
                    $scope.form_data[selectedTab].invalid.passport = true;
                    form_invalid = true;
                }
            }
            else {
                $scope.form_data[selectedTab].invalid.passport = true;
            }
        }
    };

    $scope.validateGender = function (selectedTab) {
        if (!$scope.form_data[selectedTab].gender) {
            $scope.form_data[selectedTab].errorRequired.gender = true;
            form_invalid = true;
        }
        else {
            $scope.form_data[selectedTab].errorRequired.gender = false;
        }
    };

    $scope.validateEmailApi = function (selectedTab) {
        if ($scope.form_data[selectedTab].email) {
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,30})$/;
            var result = pattern.test($scope.form_data[selectedTab].email);
            if (result) {
                email_data = {
                    "email": $scope.form_data[selectedTab].email
                }
                dataFactory.validateEmail(email_data)
                    .then(function (data) {
                            if (data.result == "fail") {
                                $scope.documentsToValidate[selectedTab].email = "Email Id Doesn't Exist";
                                form_invalid = true;
                            }
                            else {
                                $scope.form_data[selectedTab].invalid.email = false;
                                $scope.documentsToValidate[selectedTab].email = "";
                            }
                        }
                    );
            }
        }
    };

    $scope.validateEmail = function (selectedTab) {
        if ($scope.form_data[selectedTab].email) {
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,4})$/;
            var result = pattern.test($scope.form_data[selectedTab].email);
            if (result) {
                $scope.form_data[selectedTab].invalid.email = false;
            }
            else {
                $scope.form_data[selectedTab].invalid.email = true;
                form_invalid = true;
            }
        }
        else {
            $scope.form_data[selectedTab].invalid.email = false;
        }
    };

    $scope.validateContactNumber = function (selectedTab) {
        if ($scope.form_data[selectedTab].contact_number) {
            if ($scope.form_data[selectedTab].contact_number.length < 10) {
                $scope.form_data[selectedTab].invalid.contact_number = true;
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].invalid.contact_number = false;
            }
        }
        else {
            $scope.form_data[selectedTab].invalid.contact_number = false;
        }
    };

    $scope.validatePan = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].pan) {
                $scope.documentsToValidate[selectedTab].pan = ""
            }
        }
        $scope.form_data[selectedTab].pan.invalid=false;
        if ($scope.form_data[selectedTab].pan.check) {
            if (!$scope.form_data[selectedTab].pan.unique_no) {
                $scope.form_data[selectedTab].errorRequired.pan.unique_no = true;
                $scope.form_data[selectedTab].pan.invalid=true
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.pan.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.pan) {
                                $scope.documentsToValidate[selectedTab].pan = data.invalid.pan;
                                form_invalid = true;
                                $scope.form_data[selectedTab].pan.invalid=true
                            }
                            if (data.invalid.pan) {
                                $scope.documentsToValidate[selectedTab].pan = data.invalid.pan;
                                form_invalid = true;
                                $scope.form_data[selectedTab].pan.invalid=true
                            }
                        }
                    );
            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.pan.unique_no = false;
        }
    };

    $scope.validateLlpin = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].llpin) {
                $scope.documentsToValidate[selectedTab].llpin = ""
            }
        }
        $scope.form_data[selectedTab].llpin.invalid = false;
        if ($scope.form_data[selectedTab].llpin.check) {
            if (!$scope.form_data[selectedTab].llpin.unique_no) {
                $scope.form_data[selectedTab].errorRequired.llpin.unique_no = true;
                $scope.form_data[selectedTab].llpin.invalid = true;
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.llpin.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.llpin) {
                                $scope.documentsToValidate[selectedTab].llpin = data.invalid.llpin;
                                form_invalid = true;
                                $scope.form_data[selectedTab].llpin.invalid = true;
                            }
                            if (data.invalid.llpin) {
                                $scope.documentsToValidate[selectedTab].llpin = data.invalid.llpin;
                                form_invalid = true;
                                $scope.form_data[selectedTab].llpin.invalid = true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.llpin.unique_no = false;

        }
    };
    
    $scope.validateVat = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].vat) {
                $scope.documentsToValidate[selectedTab].vat = ""
            }
        }
        $scope.form_data[selectedTab].vat.invalid=false;
        if ($scope.form_data[selectedTab].vat.check) {
            if (!$scope.form_data[selectedTab].vat.unique_no) {
                $scope.form_data[selectedTab].errorRequired.vat.unique_no = true;
                $scope.form_data[selectedTab].vat.invalid=true;
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.vat.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.vat) {
                                $scope.documentsToValidate[selectedTab].vat = data.invalid.vat;
                                $scope.form_data[selectedTab].vat.invalid=true;
                                form_invalid = true;
                            }
                            if (data.invalid.vat) {
                                $scope.documentsToValidate[selectedTab].vat = data.invalid.vat;
                                $scope.form_data[selectedTab].vat.invalid=true;
                                form_invalid = true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.vat.unique_no = false;

        }
    };

    $scope.validateCst = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].cst) {
                $scope.documentsToValidate[selectedTab].cst = ""
            }
        }
        $scope.form_data[selectedTab].cst.invalid=false;
        if ($scope.form_data[selectedTab].cst.check) {
            if (!$scope.form_data[selectedTab].cst.unique_no) {
                $scope.form_data[selectedTab].errorRequired.cst.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].cst.invalid=true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.cst.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.cst) {
                                $scope.documentsToValidate[selectedTab].cst = data.invalid.cst;
                                form_invalid = true;
                                $scope.form_data[selectedTab].cst.invalid=true;
                            }
                            if (data.invalid.cst) {
                                $scope.documentsToValidate[selectedTab].cst = data.invalid.cst;
                                form_invalid = true;
                                $scope.form_data[selectedTab].cst.invalid=true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.cst.unique_no = false;
        }
    }

    $scope.validatePt = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].pt) {
                $scope.documentsToValidate[selectedTab].pt = ""
            }
        }
        $scope.form_data[selectedTab].pt.invalid=false;
        if ($scope.form_data[selectedTab].pt.check) {
            if (!$scope.form_data[selectedTab].pt.unique_no) {
                $scope.form_data[selectedTab].errorRequired.pt.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].pt.invalid=true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.pt.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.pt) {
                                $scope.documentsToValidate[selectedTab].pt = data.invalid.pt;
                                form_invalid = true;
                                $scope.form_data[selectedTab].pt.invalid=true;
                            }
                            if (data.invalid.pt) {
                                $scope.documentsToValidate[selectedTab].pt = data.invalid.pt;
                                form_invalid = true;
                                $scope.form_data[selectedTab].pt.invalid=true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.pt.unique_no = false;

        }
    }

    $scope.validateIec = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].iec) {
                $scope.documentsToValidate[selectedTab].iec = ""
            }
        }
        $scope.form_data[selectedTab].iec.invalid=false;
        if ($scope.form_data[selectedTab].iec.check) {
            if (!$scope.form_data[selectedTab].iec.unique_no) {
                $scope.form_data[selectedTab].errorRequired.iec.unique_no = true;
                $scope.form_data[selectedTab].iec.invalid=true;
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.iec.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.iec) {
                                $scope.documentsToValidate[selectedTab].iec = data.invalid.iec;
                                form_invalid = true;
                                $scope.form_data[selectedTab].iec.invalid=true;
                                
                            }
                            if (data.invalid.iec) {
                                $scope.documentsToValidate[selectedTab].iec = data.invalid.iec;
                                form_invalid = true;
                                $scope.form_data[selectedTab].iec.invalid=true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.iec.unique_no = false;

        }
    };

    $scope.validateExcise = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].excise) {
                $scope.documentsToValidate[selectedTab].excise = ""
            }
        }
        $scope.form_data[selectedTab].excise.invalid=false;
        if ($scope.form_data[selectedTab].excise.check) {
            if (!$scope.form_data[selectedTab].excise.unique_no) {
                $scope.form_data[selectedTab].errorRequired.excise.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].excise.invalid=true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.excise.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.excise) {
                                $scope.documentsToValidate[selectedTab].excise = data.invalid.excise;
                                form_invalid = true;
                                $scope.form_data[selectedTab].excise.invalid=true;
                            }
                            if (data.invalid.excise) {
                                $scope.documentsToValidate[selectedTab].excise = data.invalid.excise;
                                form_invalid = true;
                                $scope.form_data[selectedTab].excise.invalid=true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.excise.unique_no = false;

        }
    };

    $scope.validateServiceTax = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].service_tax) {
                $scope.documentsToValidate[selectedTab].service_tax = ""
            }
        }
        $scope.form_data[selectedTab].service_tax.invalid= false;
        if ($scope.form_data[selectedTab].service_tax.check) {
            if (!$scope.form_data[selectedTab].service_tax.unique_no) {
                $scope.form_data[selectedTab].errorRequired.service_tax.unique_no = true;
                $scope.form_data[selectedTab].service_tax.invalid= true;
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.service_tax.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.service_tax) {
                                $scope.documentsToValidate[selectedTab].service_tax = data.invalid.service_tax;
                                form_invalid = true;
                                $scope.form_data[selectedTab].service_tax.invalid= true;
                            }
                            if (data.invalid.service_tax) {
                                $scope.documentsToValidate[selectedTab].service_tax = data.invalid.service_tax;
                                form_invalid = true;
                                $scope.form_data[selectedTab].service_tax.invalid= true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.service_tax.unique_no = false;

        }
    };

    $scope.validateAadhar = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].aadhar) {
                $scope.documentsToValidate[selectedTab].aadhar = ""
            }
        }
        $scope.form_data[selectedTab].aadhar.invalid= false;
        if ($scope.form_data[selectedTab].aadhar.check) {
            if (!$scope.form_data[selectedTab].aadhar.unique_no) {
                $scope.form_data[selectedTab].errorRequired.aadhar.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].aadhar.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.aadhar.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalidCheckSum.aadhar) {
                                $scope.documentsToValidate[selectedTab].aadhar = data.invalidCheckSum.aadhar;
                                form_invalid = true;
                                $scope.form_data[selectedTab].aadhar.invalid= true;
                            }
                            if (data.invalid.aadhar) {
                                $scope.documentsToValidate[selectedTab].aadhar = data.invalid.aadhar;
                                form_invalid = true;
                                $scope.form_data[selectedTab].aadhar.invalid= true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.aadhar.unique_no = false;

        }
    };
    
    $scope.validateUAN = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].uan) {
                $scope.documentsToValidate[selectedTab].uan = ""
            }
        }
        $scope.form_data[selectedTab].uan.invalid= false;
        if ($scope.form_data[selectedTab].uan.check) {
            if (!$scope.form_data[selectedTab].uan.unique_no) {
                $scope.form_data[selectedTab].errorRequired.uan.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].uan.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.uan.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalidCheckSumUAN.uan) {
                                $scope.documentsToValidate[selectedTab].uan = data.invalidCheckSumUAN.uan;
                                form_invalid = true;
                                $scope.form_data[selectedTab].uan.invalid= true;
                            }
                            if (data.invalid.uan) {
                                $scope.documentsToValidate[selectedTab].uan = data.invalid.uan;
                                form_invalid = true;
                                $scope.form_data[selectedTab].uan.invalid= true;
                            }
                        }
                    );
            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.uan.unique_no = false;

        }
    };
    
    $scope.validateDl = function (selectedTab) {
    	var reg1= new RegExp("(^[^a-zA-Z0-9/]+$)|(^[0/]+$)");
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].dl) {
                $scope.documentsToValidate[selectedTab].dl = ""
            }
        }
        $scope.form_data[selectedTab].dl.invalid= false;
        if ($scope.form_data[selectedTab].dl.check) {
            if (!$scope.form_data[selectedTab].dl.unique_no || reg1.test($scope.form_data[selectedTab].dl.unique_no)) {
            	console.debug("invalid DL Number")
                $scope.form_data[selectedTab].errorRequired.dl.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].dl.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.dl.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.dl) {
                                $scope.documentsToValidate[selectedTab].dl = data.invalid.dl;
                                form_invalid = true;
                                $scope.form_data[selectedTab].dl.invalid= true;
                            }
                            if (data.invalid.dl) {
                                $scope.documentsToValidate[selectedTab].dl = data.invalid.dl;
                                form_invalid = true;
                                $scope.form_data[selectedTab].dl.invalid= true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.dl.unique_no = false;

        }
    };
    
    $scope.validateElecConsumer = function(selectedTab){
    	var reg1= new RegExp("(^[^a-zA-Z0-9/]+$)|(^[0/]+$)");
    	if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].electricity) {
                $scope.documentsToValidate[selectedTab].electricity = ""
            }
        }
        $scope.form_data[selectedTab].electricity.invalid= false;
        if ($scope.form_data[selectedTab].electricity.check) {
            if (!$scope.form_data[selectedTab].electricity.unique_no ||  reg1.test($scope.form_data[selectedTab].electricity.unique_no)) {
            	console.debug("I am invalid for the Electricity Consumer Number");
                $scope.form_data[selectedTab].errorRequired.electricity.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].electricity.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.electricity.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                    	console.debug(data);
                            if (data.invalid.electricity) {
                                $scope.documentsToValidate[selectedTab].electricity = data.invalid.electricity;
                                form_invalid = true;
                                $scope.form_data[selectedTab].electricity.invalid= true;
                            }
                            if (data.invalid.electricity) {
                                $scope.documentsToValidate[selectedTab].electricity = data.invalid.electricity;
                                form_invalid = true;
                                $scope.form_data[selectedTab].electricity.invalid= true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.electricity.unique_no = false;

        }
    };
    
    $scope.validatePassport = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].passport) {
                $scope.documentsToValidate[selectedTab].passport = ""
            }
        }
        $scope.form_data[selectedTab].passport.invalid= false;
        if ($scope.form_data[selectedTab].passport.check) {
            if (!$scope.form_data[selectedTab].passport.unique_no) {
                $scope.form_data[selectedTab].errorRequired.passport.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].passport.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.passport.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.passport) {
                                $scope.documentsToValidate[selectedTab].passport = data.invalid.passport;
                                form_invalid = true;
                                $scope.form_data[selectedTab].passport.invalid= true;
                            }
                            if (data.invalid.passport) {
                                $scope.documentsToValidate[selectedTab].passport = data.invalid.passport;
                                form_invalid = true;
                                $scope.form_data[selectedTab].passport.invalid= true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.passport.unique_no = false;

        }
    };

    $scope.validateVoting = function (selectedTab) {
    	var reg = new RegExp("^[a-z-A-Z-0-9/]*$");
    	var reg1= new RegExp("(^[^a-zA-Z0-9/]+$)|(^[0/]+$)");
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].voting) {
                $scope.documentsToValidate[selectedTab].voting = ""
            }
        }
        $scope.form_data[selectedTab].voting.invalid = false;
        if ($scope.form_data[selectedTab].voting.check) {
            if (!$scope.form_data[selectedTab].voting.unique_no) {
                $scope.form_data[selectedTab].errorRequired.voting.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].voting.invalid = true;                	
                
            }
            else if(!reg.test($scope.form_data[selectedTab].voting.unique_no) || 
            		reg1.test($scope.form_data[selectedTab].voting.unique_no)){
            	console.debug("RegEx for Voting Didn't matched")
            	form_invalid= true;
            	 $scope.form_data[selectedTab].voting.invalid = true;
            	
            }
            else {
                $scope.form_data[selectedTab].errorRequired.voting.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.voting) {
                                $scope.documentsToValidate[selectedTab].voting = data.invalid.voting;
                                form_invalid = true;
                                $scope.form_data[selectedTab].voting.invalid = true;
                            }
                            if (data.invalid.voting) {
                                $scope.documentsToValidate[selectedTab].voting = data.invalid.voting;
                                form_invalid = true;
                                $scope.form_data[selectedTab].voting.invalid = true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.voting.unique_no = false;

        }
    };

    $scope.validateNrega = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].nrega) {
                $scope.documentsToValidate[selectedTab].nrega = ""
            }
        }
        if ($scope.form_data[selectedTab].nrega.check) {
            if (!$scope.form_data[selectedTab].nrega.unique_no) {
                $scope.form_data[selectedTab].errorRequired.nrega.unique_no = true;
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.nrega.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.nrega) {
                                $scope.documentsToValidate[selectedTab].nrega = data.invalid.nrega;
                                form_invalid = true;
                            }
                            if (data.invalid.nrega) {
                                $scope.documentsToValidate[selectedTab].nrega = data.invalid.nrega;
                                form_invalid = true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.nrega.unique_no = false;

        }
    };

    
    $scope.validateElectricity = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].electricity) {
                $scope.documentsToValidate[selectedTab].electricity = ""
            }
        }
        if ($scope.form_data[selectedTab].electricity.check) {
            if (!$scope.form_data[selectedTab].electricity.unique_no) {
                $scope.form_data[selectedTab].errorRequired.electricity.unique_no = true;
                $scope.form_data[selectedTab].errorRequired.electricity.board="";
                form_invalid = true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.electricity.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.electricity) {
                                $scope.documentsToValidate[selectedTab].electricity = data.invalid.electricity;
                                form_invalid = true;
                            }
                            if (data.invalid.electricity) {
                                $scope.documentsToValidate[selectedTab].electricity = data.invalid.electricity;
                                form_invalid = true;
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.electricity.unique_no = false;
            $scope.form_data[selectedTab].errorRequired.electricity.board = "";

        }
    };
    

    $scope.validateTelephone = function (selectedTab) {
    	console.debug("Going to Validate Tel")
    	var reg = new RegExp("^[0-9-]*$");
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].telephone) {
                $scope.documentsToValidate[selectedTab].telephone = "" //Find out Why ?
            }
        }
        $scope.form_data[selectedTab].telephone.invalid=false
        if ($scope.form_data[selectedTab].telephone.check) {
            if (!$scope.form_data[selectedTab].telephone.unique_no) {
                $scope.form_data[selectedTab].errorRequired.telephone.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].telephone.invalid=true
            }
            else if(!reg.test($scope.form_data[selectedTab].telephone.unique_no)){
            	console.debug("Regex for Tel Didn't match")
            	form_invalid = true;
                $scope.form_data[selectedTab].telephone.invalid=true
            }
            else {
                $scope.form_data[selectedTab].errorRequired.telephone.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.telephone) {
                                $scope.documentsToValidate[selectedTab].telephone = data.invalid.telephone;
                                form_invalid = true;
                                $scope.form_data[selectedTab].telephone.invalid=true

                            }
                            if (data.invalid.telephone) {
                                $scope.documentsToValidate[selectedTab].telephone = data.invalid.telephone;
                                form_invalid = true;
                                $scope.form_data[selectedTab].telephone.invalid=true
                            }
                        }
                    );

            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.telephone.unique_no = false;

        }
    };


    $scope.validateLPG = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].lpg) {
                $scope.documentsToValidate[selectedTab].lpg = ""
            }
        }
        $scope.form_data[selectedTab].lpg.invalid= false;
        if ($scope.form_data[selectedTab].lpg.check) {
            if (!$scope.form_data[selectedTab].lpg.unique_no) {
                $scope.form_data[selectedTab].errorRequired.lpg.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].lpg.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.lpg.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.lpg) {
                                $scope.documentsToValidate[selectedTab].lpg = data.invalid.lpg;
                                form_invalid = true;
                                $scope.form_data[selectedTab].lpg.invalid= true;
                            }
                            if (data.invalid.lpg) {
                                $scope.documentsToValidate[selectedTab].lpg = data.invalid.lpg;
                                form_invalid = true;
                                $scope.form_data[selectedTab].lpg.invalid= true;
                            }
                        }
                    );
            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.lpg.unique_no = false;

        }
    };
    
    
    $scope.validateESIC = function (selectedTab) {
        if ($scope.documentsToValidate[selectedTab]) {
            if ($scope.documentsToValidate[selectedTab].esic) {
                $scope.documentsToValidate[selectedTab].esic = ""
            }
        }
        $scope.form_data[selectedTab].esic.invalid= false;
        if ($scope.form_data[selectedTab].esic.check) {
            if (!$scope.form_data[selectedTab].esic.unique_no) {
                $scope.form_data[selectedTab].errorRequired.esic.unique_no = true;
                form_invalid = true;
                $scope.form_data[selectedTab].esic.invalid= true;
            }
            else {
                $scope.form_data[selectedTab].errorRequired.esic.unique_no = false;
                dataFactory.validateDocuments($scope.form_data[selectedTab])
                    .then(function (data) {
                            if (data.invalid.esic) {
                                $scope.documentsToValidate[selectedTab].esic = data.invalid.esic;
                                form_invalid = true;
                                $scope.form_data[selectedTab].esic.invalid= true;
                            }
                            if (data.invalid.esic) {
                                $scope.documentsToValidate[selectedTab].esic = data.invalid.esic;
                                form_invalid = true;
                                $scope.form_data[selectedTab].esic.invalid= true;
                            }
                        }
                    );
            }
        }
        else {
            $scope.form_data[selectedTab].errorRequired.esic.unique_no = false;

        }
    };
    
    
    
    
    
    $scope.validateAcknowledgementNumber = function (itr_row, selectedTab) {
        if (itr_row) {
            if (itr_row.acknowledgement_number) {
                itr_row.errorRequired.acknowledgement_number = ""
            }
        }
        console.debug("came here to validate the ACK of ITR")
        form_invalid = false;
        $scope.form_data[selectedTab].itr.invalidITR = false;
        if (!itr_row.acknowledgement_number) {
            itr_row.errorRequired.acknowledgement_number.unique_no = true;
            form_invalid = true;
            console.debug("Form Invalidated for date of filling for ITR-0");
        }
        else {
            itr_row.errorRequired.acknowledgement_number.unique_no = false;
        }
        if (!itr_row.date_of_filling) {
            itr_row.errorRequired.date_of_filling.unique_no = true;
            form_invalid = true;
            console.debug("Form Invalidated for date of filling for ITR-1");
        }
        else {
            itr_row.errorRequired.date_of_filling.unique_no = false;
            $scope.validateDateOfFiling(itr_row, selectedTab)
            console.debug("Form validated for date of filling for ITR-1");
        }
        if (itr_row.acknowledgement_number) {
            itr_row.errorRequired.acknowledgement_number.unique_no = false;
            dataFactory.validateItrDocuments(itr_row)
                .then(function (data) {
                	console.debug("data after server response from the Server for the ITR ACK",data);
                    if (data.invalid) {
                    	
                    	console.debug("data After server response from the Server for the ITR ACK came in try block",data);
                        data.invalid="Invalid Ack No";
                    	itr_row.invalid = data.invalid;
                        
                        //form_invalid = false;
                        console.debug("Form Invalidated for date of filling for ITR-2");
                    }
                    else {
                        itr_row.invalid = "";
                       // form_invalid = true;
                        console.debug("Form Invalidated for date of filling for ITR-3");
                    }
                });
        }
    };

    $scope.validateDateOfFiling = function (itr_row, selectedTab) {
        itr_row.errorRequired.invalid_date = false;
        itr_row.errorRequired.date_of_filling.unique_no = false;
        if (itr_row.date_of_filling) {
            var date = CommonServices.getDateFromconvertDDMMYYYYString(itr_row.date_of_filling)
            if (date && date != "Invalid Date") {
                var converted_date = $filter('date')(date, "dd/MM/yyyy");
                if (!converted_date) {
                    itr_row.errorRequired.invalid_date = true;
                    form_invalid = true;
                }
                else {
                    itr_row.errorRequired.invalid_date = false;
                }
            }
            else {
                itr_row.errorRequired.invalid_date = true;
                form_invalid = true;
            }
        }
        else {
            itr_row.errorRequired.date_of_filling.unique_no = true;
        }
    };

    function validateSave(selectedTab) {
        if ($scope.form_data[selectedTab].entity_type && $scope.form_data[selectedTab].relation && $scope.form_data[selectedTab].branch) {
            if ($scope.form_type[selectedTab] == "non-individuals" || $scope.form_type[selectedTab] == "others") {
                $scope.validateEntity(selectedTab);
            }

            if ($scope.form_type[selectedTab] == "individuals") {
                $scope.validateName(selectedTab);
            }
        }
        else {
            form_invalid = true;
        }
    };
    	/*Validate the Form before submitting whether the required fields are filled for the documents or not*/
    function validateForm(selectedTab) {
        if ($scope.form_data[selectedTab].entity_type && $scope.form_data[selectedTab].relation && $scope.form_data[selectedTab].branch) {
            if ($scope.form_type[selectedTab] == "non-individuals") {
                $scope.validateLlpin(selectedTab);
            }

            if ($scope.form_type[selectedTab] == "non-individuals" || $scope.form_type[selectedTab] == "others") {
                $scope.validateEntity(selectedTab);
                $scope.validateVat(selectedTab);
                $scope.validateCst(selectedTab);
                $scope.validatePt(selectedTab);
                $scope.validateIec(selectedTab);
                $scope.validateExcise(selectedTab);
                $scope.validateServiceTax(selectedTab);
                $scope.validateElectricity(selectedTab);
                $scope.validateTelephone(selectedTab);
            }

            if ($scope.form_type[selectedTab] == "individuals") {
                $scope.validateName(selectedTab);
                $scope.validateFatherName(selectedTab);
                $scope.validateGender(selectedTab);
            }
            if ($scope.form_type[selectedTab] != "non-individuals" || $scope.form_type[selectedTab] != "others") {
                $scope.validateAadhar(selectedTab);
                $scope.validateDl(selectedTab);
                $scope.validatePassport(selectedTab);
                $scope.validatePassportValidity(selectedTab);
                $scope.validateVoting(selectedTab);
                $scope.validateNrega(selectedTab);
                $scope.validatePan(selectedTab);
                $scope.validateTelephone(selectedTab);
                $scope.validateElectricity(selectedTab);
                $scope.validateLPG(selectedTab);
                $scope.validateESIC(selectedTab);
                $scope.validateUAN(selectedTab);
            }

            $scope.validateAddress1(selectedTab);
            $scope.validateState(selectedTab);
            $scope.validateCountry(selectedTab);
            $scope.validatePincode(selectedTab);
            $scope.validateDob(selectedTab);
            $scope.validateEmail(selectedTab);
            $scope.validateContactNumber(selectedTab);

        }
    };

    //validation code end
    //Takes a sequence of UnformattedData and return an array of formatted Data 
    function reformatData(unformatted_data) {
    	console.debug("Unformatted Data is ",unformatted_data)
        var formatted_data = []
        for (var i = 0; i < unformatted_data.length; i++) {
            var data = unformatted_data[i];
            /*var passport_data = {
                "check": false,
                "unique_no": "",
                "expiry_date": ""
            };
            if (checkData(data, "passport")) {
                if (checkData(data, "passport").unique_no) {
                    passport_data = checkData(data, "passport")
                }
            }*/
            var passport_data=checkData(data,"passport")
            var pan_data = checkData(data, "pan");
            var llpin_data = checkData(data, "llpin");
            var vat_data = checkData(data, "vat");
            var cst_data = checkData(data, "cst");
            var pt_data = checkData(data, "pt");
            var iec_data = checkData(data, "iec");
            var excise_data = checkData(data, "excise");
            var service_tax_data = checkData(data, "service_tax");
            var aadhar_data = checkData(data, "aadhar");
            var dl_data = checkData(data, "dl");
            var voting_data = checkData(data, "voting");
            var nrega_data = checkData(data, "nrega");
            var electricity_data = checkData(data, "electricity");
            var telephone_data = checkData(data, "telephone");
            var uan_data = checkData(data, "uan");
            var lpg_data = checkData(data, "lpg");
            var esic_data = checkData(data, "esic");
            var converted_dob
            if (data.dob) {
                converted_dob = $filter('date')(data.dob.split(" ")[0], "dd/MM/yyyy");
            }
            var itr = checkData(data, "itr");
            $scope.initialNewRequestJSONData = {
                id: data.id,
                form_id: i,
                name_label: getNameDOBLabel(entity_type).name_label,
                dob_label: getNameDOBLabel(entity_type).dob_label,
                reg_certificate_label: getNameDOBLabel(entity_type).reg_certificate_label,
                "request_id": data.request_id,
                "date": data.created_date,
                "entity_type": data.entity_type,
                "relation": data.relation,
                "branch": data.branch,
                "entity": data.entity_name,
                "full_name": {
                    "first_name": data.first_name,
                    "middle_name": data.middle_name,
                    "last_name": data.last_name
                },
                "father_husband_name": {
                    "first_name": data.father_husband_first_name,
                    "middle_name": data.father_husband_middle_name,
                    "last_name": data.father_husband_last_name
                },
                "address": {
                    "address_line1": data.address,
                    "address_line2": data.address1,
                    "state": data.state,
                    "city": data.city,
                    "pincode": data.pinCode,
                    "country": data.country
                },
                "dob": converted_dob,
                "gender": data.gender,
                "email": data.email,
                "contact_number": data.contactNumber,
                "pan": pan_data,
                "llpin": llpin_data,
                "vat": vat_data,
                "cst": cst_data,
                "pt": pt_data,
                "iec": iec_data,
                "excise": excise_data,
                "service_tax": service_tax_data,
                "aadhar": aadhar_data,
                "dl": dl_data,
                "passport": passport_data,
                "voting": voting_data,
                "nrega": nrega_data,
                "electricity": electricity_data,
                "telephone": telephone_data,
                "uan"  : uan_data,
                "lpg"  : lpg_data,
                "esic"  : esic_data,
                itr: itr,
                "invalid": {
                    "full_name": false,
                    "address": {
                        "pincode": {
                            select_state: false,
                            invalid_pin: false
                        }
                    },
                    "dob": false,
                    "email": false,
                    "contact_number": false,
                    "passport": false
                },
                "errorRequired": {
                    "entity": false,
                    "full_name": {
                        "first_name": false,
                        "middle_name": false,
                        "last_name": false
                    },
                    "address": {
                        "address_line1": false,
                        "address_line2": false,
                        "state": false,
                        "pincode": false
                    },
                    "dob": {
                        "day": false,
                        "month": false,
                        "year": false
                    },
                    "gender": false,
                    "email": false,
                    "contact_number": false,
                    "pan": {
                        "unique_no": false
                    },
                    "llpin": {
                        "unique_no": false
                    },
                    "vat": {
                        "unique_no": false
                    },
                    "cst": {
                        "unique_no": false
                    },
                    "pt": {
                        "unique_no": false
                    },
                    "iec": {
                        "unique_no": false
                    },
                    "excise": {
                        "unique_no": false
                    },
                    "service_tax": {
                        "unique_no": false
                    },
                    "aadhar": {
                        "unique_no": false
                    },
                    "dl": {
                        "unique_no": false
                    },
                    "passport": {
                        "unique_no": false,
                        "expiry_date": {
                            "day": false,
                            "month": false,
                            "year": false
                        }
                    },
                    "voting": {
                        "unique_no": false
                    },
                    "nrega": {
                        "unique_no": false
                    },
                    "electricity": {
                        "unique_no":false,
                        "board":""
                    },
                    "telephone": {
                        "unique_no": false
                    },
                    "uan": {
                        "unique_no": false
                    },
                    "lpg": {
                        "unique_no": false
                    },
                    "esic": {
                        "unique_no": false
                    }
                }
            };
            console.debug("After Reformatting : InitialNewRequestJSONData is ",$scope.initialNewRequestJSONData)
            if (data.entity_status == "parent") {
                if (formatted_data.length == 0) {
                    formatted_data.push($scope.initialNewRequestJSONData);
                }
                else {
                    var temp = formatted_data[0];
                    formatted_data[0] = $scope.initialNewRequestJSONData;
                    formatted_data.push(temp);
                }
            }
            else {
                formatted_data.push($scope.initialNewRequestJSONData);
            }
        }
        return formatted_data;
    }

    function getNameDOBLabel(entity_type) {
        var name_label;
        var dob_label;
        var reg_certificate_label;
        var data = {
            name_label: "",
            dob_label: "",
            reg_certificate_label: ""
        }
        if (entity_type == "Proprietory Concern") {
            name_label = "Proprietor";
            dob_label = "Date of Formation";
        }
        else if (entity_type == "Hindu Undivided Family (HUF)") {
            name_label = "Karta";
            dob_label = "Date of Formation";
        }
        else if (entity_type == "Company" || entity_type == 'Partnership' || entity_type == 'Limited Liability Partnership (LLP)' || entity_type == 'Association of Persons (AOP)' || entity_type == 'Society' || entity_type == 'Body of Individuals (BOI)' || entity_type == 'Trust') {
            dob_label = "Date of Formation";
        }
        else {
            name_label = "Name";
            dob_label = "Date of Birth";
        }
        if (entity_type == "Company") {
            reg_certificate_label = "CIN/FCRN"
        }
        else if (entity_type == "Limited Liability Partnership (LLP)") {
            reg_certificate_label = "LLPIN/FLLPIN"
        }
        else {
            reg_certificate_label = "Registration Certificate"
        }
        data.name_label = name_label;
        data.dob_label = dob_label;
        data.reg_certificate_label = reg_certificate_label;
        return data;
    }

    function checkData(data, to_be_checked) {
        var document_data = {"check": false, unique_no: ""}
        if (to_be_checked == "passport") {
            document_data = {
                "check": false,
                "unique_no": "",
                "expiry_date": ""
            };
            if (data[to_be_checked]) {
                if (data[to_be_checked].check) {
                    document_data.check = true;
                    document_data.unique_no = data[to_be_checked].unique_no;
                    document_data.expiry_date = $filter('date')(data[to_be_checked].expiry_date.split(" ")[0], "dd/MM/yyyy");
                }
            }
        }
      
        else if (to_be_checked == "itr") {
            document_data = {
                "check": false,
                "itr_fields": []
            }
            if (data[to_be_checked]) {
                for (var i = 0; i < data[to_be_checked].length; i++) {
                    if (i == 0 && data[to_be_checked][0]) {
                        document_data.check = data[to_be_checked][0].check
                    }
                    var itr_field = {
                        "assesment_year": data[to_be_checked][i].assessment_year,
                        "acknowledgement_number": data[to_be_checked][i].number,
                        "date_of_filling": $filter('date')(data[to_be_checked][i].date_of_filling.split(" ")[0], "dd/MM/yyyy"),
                        "file": "",
                        date_of_filling_datepicker: {
                            opened: false
                        },
                        "assesment_year_touched": true,
                        "date_of_filling_touched": true,
                        "acknowledgement_number_touched": true,
                        "errorRequired": {
                            "acknowledgement_number": {
                                "unique_no": false
                            },
                            "date_of_filling": {
                                "unique_no": false
                            },
                            "assesment_year": {
                                "unique_no": false
                            },
                            "invalid_date": false
                        },
                        invalid: ""
                    }
                    document_data.itr_fields.push(itr_field);
                }
            }
            else {
                var itr_field = {
                    "assesment_year": "",
                    "acknowledgement_number": "",
                    "date_of_filling": "",
                    "file": "",
                    date_of_filling_datepicker: {
                        opened: false
                    },
                    "assesment_year_touched": false,
                    "date_of_filling_touched": false,
                    "acknowledgement_number_touched": false,
                    "errorRequired": {
                        "acknowledgement_number": {
                            "unique_no": false
                        },
                        "date_of_filling": {
                            "unique_no": false
                        },
                        "assesment_year": {
                            "unique_no": false
                        },
                        "invalid_date": false
                    },
                    invalid: ""
                }
                document_data.itr_fields.push(itr_field);
            }
        }
        else if (data[to_be_checked]) {
            if (data[to_be_checked].check) {
                document_data.check = true;
                document_data.unique_no = data[to_be_checked].unique_no;
            }
        }
        return document_data;
    };

    $scope.showMessagePopup = function (message_heading, message, redirect_url) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    if (redirect_url) {
                        parent.location.href = redirect_url;
                    }
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };
    $scope.showRestrictedMessagePopup = function (message_heading, message, redirect_url) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'restrictedMessage.html',
            backdrop: 'static',
            keyboard: false,
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    if (redirect_url) {
                        parent.location.href = redirect_url;
                    }
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };
    $scope.states = [
        'Andhra Pradesh',
        'Arunachal Pradesh',
        'Assam',
        'Bihar',
        'Chhattisgarh',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal Pradesh',
        'Jammu & Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
        'Madhya Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Mizoram',
        'Nagaland',
        'Odisha',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil Nadu',
        'Tripura',
        'Uttarakhand',
        'Uttar Pradesh',
        'West Bengal',
        'Andaman & Nicobar',
        'Chandigarh',
        'Dadra and Nagar Haveli',
        'Daman & Diu',
        'Delhi',
        'Lakshadweep',
        'Puducherry'
    ];
    $scope.countries = [
        {
            "name": "Afghanistan",
            "code": "AF"
        },
        {
            "name": "Åland Islands",
            "code": "AX"
        },
        {
            "name": "Albania",
            "code": "AL"
        },
        {
            "name": "Algeria",
            "code": "DZ"
        },
        {
            "name": "American Samoa",
            "code": "AS"
        },
        {
            "name": "AndorrA",
            "code": "AD"
        },
        {
            "name": "Angola",
            "code": "AO"
        },
        {
            "name": "Anguilla",
            "code": "AI"
        },
        {
            "name": "Antarctica",
            "code": "AQ"
        },
        {
            "name": "Antigua and Barbuda",
            "code": "AG"
        },
        {
            "name": "Argentina",
            "code": "AR"
        },
        {
            "name": "Armenia",
            "code": "AM"
        },
        {
            "name": "Aruba",
            "code": "AW"
        },
        {
            "name": "Australia",
            "code": "AU"
        },
        {
            "name": "Austria",
            "code": "AT"
        },
        {
            "name": "Azerbaijan",
            "code": "AZ"
        },
        {
            "name": "Bahamas",
            "code": "BS"
        },
        {
            "name": "Bahrain",
            "code": "BH"
        },
        {
            "name": "Bangladesh",
            "code": "BD"
        },
        {
            "name": "Barbados",
            "code": "BB"
        },
        {
            "name": "Belarus",
            "code": "BY"
        },
        {
            "name": "Belgium",
            "code": "BE"
        },
        {
            "name": "Belize",
            "code": "BZ"
        },
        {
            "name": "Benin",
            "code": "BJ"
        },
        {
            "name": "Bermuda",
            "code": "BM"
        },
        {
            "name": "Bhutan",
            "code": "BT"
        },
        {
            "name": "Bolivia",
            "code": "BO"
        },
        {
            "name": "Bosnia and Herzegovina",
            "code": "BA"
        },
        {
            "name": "Botswana",
            "code": "BW"
        },
        {
            "name": "Bouvet Island",
            "code": "BV"
        },
        {
            "name": "Brazil",
            "code": "BR"
        },
        {
            "name": "British Indian Ocean Territory",
            "code": "IO"
        },
        {
            "name": "Brunei Darussalam",
            "code": "BN"
        },
        {
            "name": "Bulgaria",
            "code": "BG"
        },
        {
            "name": "Burkina Faso",
            "code": "BF"
        },
        {
            "name": "Burundi",
            "code": "BI"
        },
        {
            "name": "Cambodia",
            "code": "KH"
        },
        {
            "name": "Cameroon",
            "code": "CM"
        },
        {
            "name": "Canada",
            "code": "CA"
        },
        {
            "name": "Cape Verde",
            "code": "CV"
        },
        {
            "name": "Cayman Islands",
            "code": "KY"
        },
        {
            "name": "Central African Republic",
            "code": "CF"
        },
        {
            "name": "Chad",
            "code": "TD"
        },
        {
            "name": "Chile",
            "code": "CL"
        },
        {
            "name": "China",
            "code": "CN"
        },
        {
            "name": "Christmas Island",
            "code": "CX"
        },
        {
            "name": "Cocos (Keeling) Islands",
            "code": "CC"
        },
        {
            "name": "Colombia",
            "code": "CO"
        },
        {
            "name": "Comoros",
            "code": "KM"
        },
        {
            "name": "Congo",
            "code": "CG"
        },
        {
            "name": "Congo, The Democratic Republic of the",
            "code": "CD"
        },
        {
            "name": "Cook Islands",
            "code": "CK"
        },
        {
            "name": "Costa Rica",
            "code": "CR"
        },
        {
            "name": "Cote D\"Ivoire",
            "code": "CI"
        },
        {
            "name": "Croatia",
            "code": "HR"
        },
        {
            "name": "Cuba",
            "code": "CU"
        },
        {
            "name": "Cyprus",
            "code": "CY"
        },
        {
            "name": "Czech Republic",
            "code": "CZ"
        },
        {
            "name": "Denmark",
            "code": "DK"
        },
        {
            "name": "Djibouti",
            "code": "DJ"
        },
        {
            "name": "Dominica",
            "code": "DM"
        },
        {
            "name": "Dominican Republic",
            "code": "DO"
        },
        {
            "name": "Ecuador",
            "code": "EC"
        },
        {
            "name": "Egypt",
            "code": "EG"
        },
        {
            "name": "El Salvador",
            "code": "SV"
        },
        {
            "name": "Equatorial Guinea",
            "code": "GQ"
        },
        {
            "name": "Eritrea",
            "code": "ER"
        },
        {
            "name": "Estonia",
            "code": "EE"
        },
        {
            "name": "Ethiopia",
            "code": "ET"
        },
        {
            "name": "Falkland Islands (Malvinas)",
            "code": "FK"
        },
        {
            "name": "Faroe Islands",
            "code": "FO"
        },
        {
            "name": "Fiji",
            "code": "FJ"
        },
        {
            "name": "Finland",
            "code": "FI"
        },
        {
            "name": "France",
            "code": "FR"
        },
        {
            "name": "French Guiana",
            "code": "GF"
        },
        {
            "name": "French Polynesia",
            "code": "PF"
        },
        {
            "name": "French Southern Territories",
            "code": "TF"
        },
        {
            "name": "Gabon",
            "code": "GA"
        },
        {
            "name": "Gambia",
            "code": "GM"
        },
        {
            "name": "Georgia",
            "code": "GE"
        },
        {
            "name": "Germany",
            "code": "DE"
        },
        {
            "name": "Ghana",
            "code": "GH"
        },
        {
            "name": "Gibraltar",
            "code": "GI"
        },
        {
            "name": "Greece",
            "code": "GR"
        },
        {
            "name": "Greenland",
            "code": "GL"
        },
        {
            "name": "Grenada",
            "code": "GD"
        },
        {
            "name": "Guadeloupe",
            "code": "GP"
        },
        {
            "name": "Guam",
            "code": "GU"
        },
        {
            "name": "Guatemala",
            "code": "GT"
        },
        {
            "name": "Guernsey",
            "code": "GG"
        },
        {
            "name": "Guinea",
            "code": "GN"
        },
        {
            "name": "Guinea-Bissau",
            "code": "GW"
        },
        {
            "name": "Guyana",
            "code": "GY"
        },
        {
            "name": "Haiti",
            "code": "HT"
        },
        {
            "name": "Heard Island and Mcdonald Islands",
            "code": "HM"
        },
        {
            "name": "Holy See (Vatican City State)",
            "code": "VA"
        },
        {
            "name": "Honduras",
            "code": "HN"
        },
        {
            "name": "Hong Kong",
            "code": "HK"
        },
        {
            "name": "Hungary",
            "code": "HU"
        },
        {
            "name": "Iceland",
            "code": "IS"
        },
        {
            "name": "India",
            "code": "IN"
        },
        {
            "name": "Indonesia",
            "code": "ID"
        },
        {
            "name": "Iran, Islamic Republic Of",
            "code": "IR"
        },
        {
            "name": "Iraq",
            "code": "IQ"
        },
        {
            "name": "Ireland",
            "code": "IE"
        },
        {
            "name": "Isle of Man",
            "code": "IM"
        },
        {
            "name": "Israel",
            "code": "IL"
        },
        {
            "name": "Italy",
            "code": "IT"
        },
        {
            "name": "Jamaica",
            "code": "JM"
        },
        {
            "name": "Japan",
            "code": "JP"
        },
        {
            "name": "Jersey",
            "code": "JE"
        },
        {
            "name": "Jordan",
            "code": "JO"
        },
        {
            "name": "Kazakhstan",
            "code": "KZ"
        },
        {
            "name": "Kenya",
            "code": "KE"
        },
        {
            "name": "Kiribati",
            "code": "KI"
        },
        {
            "name": "Korea, Democratic People\"S Republic of",
            "code": "KP"
        },
        {
            "name": "Korea, Republic of",
            "code": "KR"
        },
        {
            "name": "Kuwait",
            "code": "KW"
        },
        {
            "name": "Kyrgyzstan",
            "code": "KG"
        },
        {
            "name": "Lao People\"S Democratic Republic",
            "code": "LA"
        },
        {
            "name": "Latvia",
            "code": "LV"
        },
        {
            "name": "Lebanon",
            "code": "LB"
        },
        {
            "name": "Lesotho",
            "code": "LS"
        },
        {
            "name": "Liberia",
            "code": "LR"
        },
        {
            "name": "Libyan Arab Jamahiriya",
            "code": "LY"
        },
        {
            "name": "Liechtenstein",
            "code": "LI"
        },
        {
            "name": "Lithuania",
            "code": "LT"
        },
        {
            "name": "Luxembourg",
            "code": "LU"
        },
        {
            "name": "Macao",
            "code": "MO"
        },
        {
            "name": "Macedonia, The Former Yugoslav Republic of",
            "code": "MK"
        },
        {
            "name": "Madagascar",
            "code": "MG"
        },
        {
            "name": "Malawi",
            "code": "MW"
        },
        {
            "name": "Malaysia",
            "code": "MY"
        },
        {
            "name": "Maldives",
            "code": "MV"
        },
        {
            "name": "Mali",
            "code": "ML"
        },
        {
            "name": "Malta",
            "code": "MT"
        },
        {
            "name": "Marshall Islands",
            "code": "MH"
        },
        {
            "name": "Martinique",
            "code": "MQ"
        },
        {
            "name": "Mauritania",
            "code": "MR"
        },
        {
            "name": "Mauritius",
            "code": "MU"
        },
        {
            "name": "Mayotte",
            "code": "YT"
        },
        {
            "name": "Mexico",
            "code": "MX"
        },
        {
            "name": "Micronesia, Federated States of",
            "code": "FM"
        },
        {
            "name": "Moldova, Republic of",
            "code": "MD"
        },
        {
            "name": "Monaco",
            "code": "MC"
        },
        {
            "name": "Mongolia",
            "code": "MN"
        },
        {
            "name": "Montserrat",
            "code": "MS"
        },
        {
            "name": "Morocco",
            "code": "MA"
        },
        {
            "name": "Mozambique",
            "code": "MZ"
        },
        {
            "name": "Myanmar",
            "code": "MM"
        },
        {
            "name": "Namibia",
            "code": "NA"
        },
        {
            "name": "Nauru",
            "code": "NR"
        },
        {
            "name": "Nepal",
            "code": "NP"
        },
        {
            "name": "Netherlands",
            "code": "NL"
        },
        {
            "name": "Netherlands Antilles",
            "code": "AN"
        },
        {
            "name": "New Caledonia",
            "code": "NC"
        },
        {
            "name": "New Zealand",
            "code": "NZ"
        },
        {
            "name": "Nicaragua",
            "code": "NI"
        },
        {
            "name": "Niger",
            "code": "NE"
        },
        {
            "name": "Nigeria",
            "code": "NG"
        },
        {
            "name": "Niue",
            "code": "NU"
        },
        {
            "name": "Norfolk Island",
            "code": "NF"
        },
        {
            "name": "Northern Mariana Islands",
            "code": "MP"
        },
        {
            "name": "Norway",
            "code": "NO"
        },
        {
            "name": "Oman",
            "code": "OM"
        },
        {
            "name": "Pakistan",
            "code": "PK"
        },
        {
            "name": "Palau",
            "code": "PW"
        },
        {
            "name": "Palestinian Territory, Occupied",
            "code": "PS"
        },
        {
            "name": "Panama",
            "code": "PA"
        },
        {
            "name": "Papua New Guinea",
            "code": "PG"
        },
        {
            "name": "Paraguay",
            "code": "PY"
        },
        {
            "name": "Peru",
            "code": "PE"
        },
        {
            "name": "Philippines",
            "code": "PH"
        },
        {
            "name": "Pitcairn",
            "code": "PN"
        },
        {
            "name": "Poland",
            "code": "PL"
        },
        {
            "name": "Portugal",
            "code": "PT"
        },
        {
            "name": "Puerto Rico",
            "code": "PR"
        },
        {
            "name": "Qatar",
            "code": "QA"
        },
        {
            "name": "Reunion",
            "code": "RE"
        },
        {
            "name": "Romania",
            "code": "RO"
        },
        {
            "name": "Russian Federation",
            "code": "RU"
        },
        {
            "name": "RWANDA",
            "code": "RW"
        },
        {
            "name": "Saint Helena",
            "code": "SH"
        },
        {
            "name": "Saint Kitts and Nevis",
            "code": "KN"
        },
        {
            "name": "Saint Lucia",
            "code": "LC"
        },
        {
            "name": "Saint Pierre and Miquelon",
            "code": "PM"
        },
        {
            "name": "Saint Vincent and the Grenadines",
            "code": "VC"
        },
        {
            "name": "Samoa",
            "code": "WS"
        },
        {
            "name": "San Marino",
            "code": "SM"
        },
        {
            "name": "Sao Tome and Principe",
            "code": "ST"
        },
        {
            "name": "Saudi Arabia",
            "code": "SA"
        },
        {
            "name": "Senegal",
            "code": "SN"
        },
        {
            "name": "Serbia and Montenegro",
            "code": "CS"
        },
        {
            "name": "Seychelles",
            "code": "SC"
        },
        {
            "name": "Sierra Leone",
            "code": "SL"
        },
        {
            "name": "Singapore",
            "code": "SG"
        },
        {
            "name": "Slovakia",
            "code": "SK"
        },
        {
            "name": "Slovenia",
            "code": "SI"
        },
        {
            "name": "Solomon Islands",
            "code": "SB"
        },
        {
            "name": "Somalia",
            "code": "SO"
        },
        {
            "name": "South Africa",
            "code": "ZA"
        },
        {
            "name": "South Georgia and the South Sandwich Islands",
            "code": "GS"
        },
        {
            "name": "Spain",
            "code": "ES"
        },
        {
            "name": "Sri Lanka",
            "code": "LK"
        },
        {
            "name": "Sudan",
            "code": "SD"
        },
        {
            "name": "Suriname",
            "code": "SR"
        },
        {
            "name": "Svalbard and Jan Mayen",
            "code": "SJ"
        },
        {
            "name": "Swaziland",
            "code": "SZ"
        },
        {
            "name": "Sweden",
            "code": "SE"
        },
        {
            "name": "Switzerland",
            "code": "CH"
        },
        {
            "name": "Syrian Arab Republic",
            "code": "SY"
        },
        {
            "name": "Taiwan, Province of China",
            "code": "TW"
        },
        {
            "name": "Tajikistan",
            "code": "TJ"
        },
        {
            "name": "Tanzania, United Republic of",
            "code": "TZ"
        },
        {
            "name": "Thailand",
            "code": "TH"
        },
        {
            "name": "Timor-Leste",
            "code": "TL"
        },
        {
            "name": "Togo",
            "code": "TG"
        },
        {
            "name": "Tokelau",
            "code": "TK"
        },
        {
            "name": "Tonga",
            "code": "TO"
        },
        {
            "name": "Trinidad and Tobago",
            "code": "TT"
        },
        {
            "name": "Tunisia",
            "code": "TN"
        },
        {
            "name": "Turkey",
            "code": "TR"
        },
        {
            "name": "Turkmenistan",
            "code": "TM"
        },
        {
            "name": "Turks and Caicos Islands",
            "code": "TC"
        },
        {
            "name": "Tuvalu",
            "code": "TV"
        },
        {
            "name": "Uganda",
            "code": "UG"
        },
        {
            "name": "Ukraine",
            "code": "UA"
        },
        {
            "name": "United Arab Emirates",
            "code": "AE"
        },
        {
            "name": "United Kingdom",
            "code": "GB"
        },
        {
            "name": "United States",
            "code": "US"
        },
        {
            "name": "United States Minor Outlying Islands",
            "code": "UM"
        },
        {
            "name": "Uruguay",
            "code": "UY"
        },
        {
            "name": "Uzbekistan",
            "code": "UZ"
        },
        {
            "name": "Vanuatu",
            "code": "VU"
        },
        {
            "name": "Venezuela",
            "code": "VE"
        },
        {
            "name": "Viet Nam",
            "code": "VN"
        },
        {
            "name": "Virgin Islands, British",
            "code": "VG"
        },
        {
            "name": "Virgin Islands, U.S.",
            "code": "VI"
        },
        {
            "name": "Wallis and Futuna",
            "code": "WF"
        },
        {
            "name": "Western Sahara",
            "code": "EH"
        },
        {
            "name": "Yemen",
            "code": "YE"
        },
        {
            "name": "Zambia",
            "code": "ZM"
        },
        {
            "name": "Zimbabwe",
            "code": "ZW"
        }
    ];

 
    $scope.checkMaxRequestReached();
    $scope.getBranches();
    $scope.getBoards();
});