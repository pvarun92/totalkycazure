instaKYC.controller('userController', function ($scope, $uibModal, CommonServices, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();
    $scope.getUsers = function () {
        dataFactory.getUsersData()
            .then(function (data) {
                //console.log(data.result)
                $scope.user_data = data;
                getActiveUsers()
            });
    };
    function getActiveUsers() {
        $scope.activeUsers = [];
        for (var i = 0; i < $scope.user_data.length; i++) {
            if ($scope.user_data[i].active) {
                $scope.activeUsers.push($scope.user_data[i])
            }
        }
    }

    $scope.getLocations = function () {
        dataFactory.getLocationsData()
            .then(function (data) {
                //console.log(data.result)
                $scope.locations = data;
            });
    };

    $scope.userRoles = ["Edit", "View"];

    $scope.locations = [];

    var current_date = new Date();
    $scope.invalid_filter = {
        filter_from: false,
        filter_to: false,
        filter_to_less_than_from: false
    };

    //pagination start
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    //pagination end

    //datepicker start
    // Disable weekend selection
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showFromCalendar = function () {
        $scope.from_datepicker.opened = true;
    };

    $scope.showToCalendar = function () {
        $scope.to_datepicker.opened = true;
    };

    $scope.from_date = {
        "day": "",
        "month": "",
        "year": ""
    };

    $scope.selectFromDate = function () {
        var converted_date = CommonServices.convertDateInArrayDDMMYYY($scope.filter.filter_from);
        if (converted_date) {
            $scope.from_date.day = converted_date.day;
            $scope.from_date.month = converted_date.month;
            $scope.from_date.year = converted_date.year
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_from, new Date())) {
            $scope.invalid_filter.filter_from = true;
        }
        else {
            $scope.invalid_filter.filter_from = false;
        }
    };

    $scope.to_date = {
        "day": "",
        "month": "",
        "year": ""
    };

    $scope.selectToDate = function () {
        var converted_date = CommonServices.convertDateInArrayDDMMYYY($scope.filter.filter_to);
        if (converted_date) {
            $scope.to_date.day = converted_date.day;
            $scope.to_date.month = converted_date.month;
            $scope.to_date.year = converted_date.year
        }
        if (CommonServices.isGreaterWithDate($scope.filter.filter_to, new Date())) {
            $scope.invalid_filter.filter_to = true;
        }
        else {
            $scope.invalid_filter.filter_to = false;
        }

        if (CommonServices.isGreaterWithDate($scope.filter.filter_to, $scope.filter.filter_from)) {
            $scope.invalid_filter.filter_to_less_than_from = false;
        }
        else {
            $scope.invalid_filter.filter_to_less_than_from = true;
        }
    };

    $scope.from_datepicker = {
        opened: false
    };

    $scope.to_datepicker = {
        opened: false
    };
    //datepicker end


    $scope.filter = {
        invalid: {}
    };
    $scope.validateFromDate = function () {
        $scope.filter.invalid.filter_from = CommonServices.isInvalidDate($scope.from_date);
    };
    $scope.validateToDate = function () {
        $scope.filter.invalid.filter_to = CommonServices.isInvalidDate($scope.to_date);
    };

    $scope.crudUserPopup = function (popup_type, selected) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'crudUserPopup.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($scope) {
                $scope.selected = selected;
                if (popup_type == 'new') {
                    $scope.heading = "Create new User";
                    $scope.new = true;
                    $scope.selected = {}
                }
                else if (popup_type == 'edit') {
                    $scope.heading = "Edit User";
                    $scope.view_name = true;
                    $scope.edit = true;
                    if (selected.location) {
                        selected.location = selected.location + "";
                    }
                }
                else if (popup_type == 'view') {
                    $scope.heading = "View User";
                    $scope.view = true;
                    $scope.view_name = true;
                    if (selected.location) {
                        selected.location = selected.location + "";
                    }
                }
                $scope.close = function () {
                    $scope.getUsers();
                    showMessageModal.dismiss('cancel');
                };
                $scope.save = function (selected) {
                    dataFactory.createUser($scope.selected)
                        .then(function (data) {
                                $scope.getUsers();
                                if (data.result == "User Saved") {
                                    showMessageModal.dismiss('cancel');
                                    $scope.showMessagePopup("Message", "Record saved successfully");
                                }
                                else if (data.result.endsWith("already exist")) {
                                    $scope.showMessagePopup("Error", data.result)
                                }
                                else if (data.result == "Email verification fail") {
                                    $scope.showMessagePopup("Message", "Email ID not exist")
                                }
                                else {
                                    $scope.showMessagePopup("Message", "Record not saved");
                                }
                            }
                        );
                };
                $scope.update = function (selected) {
                    dataFactory.updateUser(selected)
                        .then(function (data) {
                                $scope.getUsers();
                                if (data.result == "User Updated") {
                                    showMessageModal.dismiss('cancel');
                                    $scope.showMessagePopup("Message", "Record updated successfully");
                                }
                                else if (data.result.endsWith("already exist")) {
                                    $scope.showMessagePopup("Error", data.result)
                                }
                                else if (data.result == "Email verification fail") {
                                    $scope.showMessagePopup("Message", "Email ID not exist")
                                }
                                else {
                                    $scope.showMessagePopup("Message", "Record not updated");
                                }
                            }
                        );
                };
                $scope.validateEmailApi = function (email) {
                    if (email) {
                        var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z0-9]{2,255})$/;
                        var result = pattern.test(email);
                        if (result) {
                            email_data = {
                                "email": email
                            };
                            dataFactory.validateEmail(email_data)
                                .then(function (data) {
                                        if (data.result == "fail") {
                                            $scope.emailNotExist = "Email Id Doesn't Exist";
                                        }
                                        else {
                                            $scope.emailNotExist = "";
                                        }
                                    }
                                );
                        }
                    }
                }
            },
            size: 'lg'
        });
    };

    $scope.toggleActiveUser = function (user) {
        if (user.active) {
            user.active = true;
        }
        else {
            user.active = false;
        }
        $scope.toggleUser(user);
    };

    $scope.toggleUser = function (user) {
        dataFactory.toggleUser(user)
            .then(function (data) {
                    if (data.result == "User Updated" && data.activeUser) {
                        getActiveUsers();
                        $scope.showMessagePopup("Message", "User Activated");
                    }
                    else if (data.result == "User Updated" && !data.activeUser) {
                        getActiveUsers();
                        $scope.showMessagePopup("Message", "User suspended");
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not updated");
                    }
                }
            );
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.getUsers();
    $scope.getLocations();
});