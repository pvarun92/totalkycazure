/**
 * Created by Fallon Software on 2/23/2016.
 */
instaKYC.controller('sysadminMenuController', function ($scope, $rootScope, $filter, $uibModal, $interval, $window, dataFactory, CommonServices) {

    if (!localStorage["current_user"]) {
        dataFactory.getCurrentUser()
            .then(function (data) {
                $rootScope.current_user = data;
                localStorage['current_user'] = JSON.stringify($rootScope.current_user);
                $scope.userRole = CommonServices.getCurrentUserRole();
            });
    }

    $scope.today_date = $filter('date')(new Date(), "dd-MM-yyyy");
    $scope.navigate_to = CommonServices.getParameterByName("navigate_to");
    if (!$scope.navigate_to) {
        $scope.navigate_to = "sysAdminDashboardNew";
    }
    $scope.changeFocus = function (li_name) {
        $rootScope.active_li = $scope.navigate_to;
    };

    $rootScope.active_li = $scope.navigate_to.split("?")[0];

    $rootScope.filter_date = "dec_26_2015";

    $rootScope.user = {
        "profile_pic": "profile.jpg",
        "username": "John Doe"
    };

    $scope.searchRequest = function (searchQuery) {
        if (searchQuery) {
            var modalInstance = $uibModal.open({
                templateUrl: 'searchRequestPopup.html',
                scope: $scope,
                controller: function ($scope) {
                    var filter = {"entity_name": searchQuery};
                    dataFactory.getFilteredData(filter)
                        .then(function (data) {
                            $scope.searchedRequest = data.data;
                        });
                    $scope.close = function () {
                        modalInstance.dismiss('cancel');
                    },
                        $scope.getConvertedDate = function (date) {
                            if (date) {
                                date = date.split(" ")[0];
                                date = $filter('date')(date, "dd-MM-yyyy");
                            }
                            return date
                        }
                },
                size: 'lg'
            });
        }
    };

    $scope.collapsedMenu = false;
    $scope.setCollapse = function () {
        $scope.collapsedMenu = !$scope.collapsedMenu
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    }
});