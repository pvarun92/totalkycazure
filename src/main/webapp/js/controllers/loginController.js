/**
 * Created by Fallon Software on 3/1/2016.
 */
instaKYC.controller('loginController', function ($scope, CommonServices) {
    if (localStorage["current_user"]) {
        localStorage["current_user"] = ""
    }
    $scope.error = CommonServices.getParameterByName("error");
    $scope.suspended = CommonServices.getParameterByName("suspended");
    $scope.maxLoggedinSession = CommonServices.getParameterByName("maxLoggedinSession");
    if ($scope.error == "true") {
        $scope.error = true;
    }
    if ($scope.suspended == "true") {
        $scope.suspended = true;
    }
    if ($scope.maxLoggedinSession == "true") {
        $scope.invalidSession = true;
    }
});