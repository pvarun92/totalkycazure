/**
 * Created by Fallon Software on 2/23/2016.
 */
instaKYC.controller('menuController', function ($scope, $rootScope, $filter, $uibModal, $interval, $window, dataFactory, CommonServices) {

    if (!localStorage["current_user"]) {
        dataFactory.getCurrentUser()
            .then(function (data) {
                $rootScope.current_user = data;
                localStorage['current_user'] = JSON.stringify($rootScope.current_user);
                $scope.userRole = CommonServices.getCurrentUserRole();
            });
    }

    dataFactory.getCurrentWorkLog()
        .then(function (data) {
            if (data) {
                $scope.workLog = data.workLog;
                localStorage['workLog'] = data.workLog;
            }
        });

    $scope.today_date = $filter('date')(new Date(), "dd-MM-yyyy");
    $scope.navigate_to = CommonServices.getParameterByName("navigate_to");
    $scope.reques_label = "New Request";
    if (!$scope.navigate_to) {
        $scope.navigate_to = "landing";
    }
    if ($scope.navigate_to == "new_request" || $scope.navigate_to.split("?")[0] == "new_request") {
        var id = CommonServices.getParameterByName("id");
        var view = CommonServices.getParameterByName("show");
        if (id) {
            $scope.reques_label = "Edit Request"
        }
        else {
            $scope.reques_label = "New Request"
        }
        if (view) {
            $scope.reques_label = "View Request"
        }
    }
    $scope.changeFocus = function (li_name) {
        $rootScope.active_li = $scope.navigate_to;
    };

    $rootScope.active_li = $scope.navigate_to.split("?")[0];

    $rootScope.filter_date = "dec_26_2015";

    $rootScope.user = {
        "profile_pic": "profile.jpg",
        "username": "John Doe"
    };

    $scope.resetPasswordPopUp = function () {
        var showMessageModal = $uibModal.open({
            templateUrl: 'changePassword.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            controller: function ($scope) {
                $scope.heading = "Change Password";
                var currentUser = CommonServices.getCurrentUser();
                $scope.selected = {user_name: currentUser.email};
                $scope.confirmPasswordNotMatched = false;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                };
                $scope.isconfirmPasswordMatch = function (selected) {
                    if (selected.new_password != selected.confirm_password) {
                        $scope.confirmPasswordNotMatched = true;
                    }
                };
                $scope.resetPassword = function (selected) {
                    if (!$scope.confirmPasswordNotMatched) {
                        dataFactory.changePassword(selected)
                            .then(function (data) {
                                    if (data.result == "password changed") {
                                        showMessageModal.dismiss('cancel');
                                        $scope.showMessagePopup("Message", "password changed successfully");
                                    }
                                    else if (data.result == "User name and password does not match") {
                                        $scope.showMessagePopup("Error", "User name and password does not match");
                                    }
                                }
                            );
                    }
                    else {
                        $scope.showMessagePopup("Error", "New password and confirm password doesn't match");
                    }
                }
            },
            size: 'lg'
        });
    };

    $scope.getDataCount = function () {
        dataFactory.getDataCount()
            .then(function (data) {
                $rootScope.data_count = data;
            });
    };

    $scope.getDataCountAuto = function () {
        dataFactory.getDataCountAuto()
            .then(function (data) {
                $rootScope.data_count = data;
            });
    };

    $interval(function () {
        $scope.getDataCountAuto();
    }, 15000);

    $scope.searchRequest = function (searchQuery) {
        if (searchQuery) {
            var modalInstance = $uibModal.open({
                templateUrl: 'searchRequestPopup.html',
                scope: $scope,
                controller: function ($scope) {
                    var filter = {"entity_name": searchQuery};
                    dataFactory.getFilteredData(filter)
                        .then(function (data) {
                            $scope.searchedRequest = data.data;
                        });
                    $scope.close = function () {
                        modalInstance.dismiss('cancel');
                    },
                        $scope.getConvertedDate = function (date) {
                            if (date) {
                                date = date.split(" ")[0];
                                date = $filter('date')(date, "dd-MM-yyyy");
                            }
                            return date
                        }
                },
                size: 'lg'
            });
        }
    };

    $scope.collapsedMenu = false;
    $scope.setCollapse = function () {
        $scope.collapsedMenu = !$scope.collapsedMenu
    };

    $scope.getDataCount();

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.selectWorkLog = function (workLog) {
        dataFactory.setCurrentWorkLog(workLog)
            .then(function (data) {
                $scope.workLog = data.workLog;
                localStorage['workLog'] = data.workLog;
                $scope.getDataCount();
                $window.location.reload();
            });
    }

});