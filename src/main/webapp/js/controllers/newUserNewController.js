instaKYC.controller('newUserNewController', function ($scope, $uibModal, CommonServices, dataFactory) {
    $scope.userRole = CommonServices.getCurrentUserRole();
    $scope.getUsers = function () {
        dataFactory.getUsersData()
            .then(function (data) {
                //console.log(data.result)
                $scope.user_data = data;
                getActiveUsers()
            });
    };

    $scope.userRoles = ["Edit", "View"];

    $scope.locations = [];

    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.getLocations = function () {
        dataFactory.getLocationsData()
            .then(function (data) {
                //console.log(data.result)
                $scope.locations = data;
            });
    };

    $scope.getLocations();

    $scope.newUser = {
        locationAccess: []
    };

    $scope.multipleDropDownSetting = {
        scrollableHeight: '200px',
        scrollable: true,
        //enableSearch: true,
        buttonClasses: "input-field multiselect-dropdown-style",
        displayProp: 'locationOrZone',
        idProp: 'id'
    };
});