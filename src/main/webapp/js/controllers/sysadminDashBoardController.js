instaKYC.controller('sysadminDashBoardController', function ($scope, $uibModal, CommonServices, dataFactory) {
    $scope.page = {"customers": [], "users": []};

    $scope.customerTypes = [
        "Scheduled Commercial Bank (Large)",
        "Urban Co-operative Bank (Small)",
        "Urban Co-operative Bank (Medium)",
        "Retail"
    ];

    if (!localStorage["current_user"]) {
        dataFactory.getCurrentUser()
            .then(function (data) {
                var current_user = data;
                localStorage['current_user'] = JSON.stringify(current_user);
                $scope.userRole = CommonServices.getCurrentUserRole();
            });
    }

    $scope.getAdminDashboardData = function () {
        dataFactory.showCustomer()
            .then(function (data) {
                    $scope.page.customers = data;
                    if (data[0]) {
                        $scope.selectCustomer(data[0])
                    }
                }
            );
    };

    $scope.resetCustomer = function (account) {
        $scope.selectedCustomer = {};
        $scope.newAccountForm.$setUntouched();
    };

    $scope.selectUser = function (user) {
        $scope.selectedUser = angular.copy(user);
        $scope.updateUserForm.$setUntouched();
    };

    $scope.resetUser = function (user) {
        $scope.selectedUser = {};
        $scope.newUserForm.$setUntouched();
    };

    $scope.createUser = function (user) {
        user.customerMasterId = $scope.selectedCustomer.id;
        dataFactory.createCustomerUser(user)
            .then(function (data) {
                    $scope.getUsersByCustomer($scope.selectedCustomer);
                    if (data.result == "Customer User Saved") {
                        $scope.showMessagePopup("Message", "Record submitted successfully")
                    }
                    else if (data.result.endsWith("already exist")) {
                        $scope.showMessagePopup("Error", data.result)
                    }
                    else if (data.result == "Email verification fail") {
                        $scope.showMessagePopup("Message", "Email ID not exist")
                    }
                }
            );
    };

    $scope.updateUser = function (user) {
        dataFactory.updateCustomerUser(user)
            .then(function (data) {
                    $scope.getUsersByCustomer($scope.selectedCustomer);
                    if (data.result == "Customer User Updated") {
                        $scope.showMessagePopup("Message", "Record updated successfully")
                    }
                    else if (data.result.endsWith("already exist")) {
                        $scope.showMessagePopup("Error", data.result)
                    }
                    else if (data.result == "Email verification fail") {
                        $scope.showMessagePopup("Message", "Email ID not exist")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not Updated")
                    }
                }
            );
    };

    $scope.deleteUser = function (user) {
        dataFactory.deleteCustomerUser(user)
            .then(function (data) {
                    $scope.getUsersByCustomer($scope.selectedCustomer);
                    if (data.result == "User Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };

    $scope.selectCustomer = function (customer) {
        $scope.selectedCustomer = customer;
        $scope.getUsersByCustomer($scope.selectedCustomer);
//        $scope.updateAccountForm.$setUntouched();
    };

    $scope.getUsersByCustomer = function (customer) {
        dataFactory.getCustomerUsers(customer.id)
            .then(function (data) {
                $scope.page.users = data;
            });
    };

    $scope.createCustomer = function (customer) {
        dataFactory.createCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Saved") {
                        $scope.showMessagePopup("Message", "Record submitted successfully")
                    }
                    else if (data.result == "Email verification fail") {
                        $scope.showMessagePopup("Message", "Email ID not exist")
                    }
                }
            );
    };

    $scope.updateCustomer = function (customer) {
        dataFactory.updateCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Updated") {
                        $scope.showMessagePopup("Message", "Record updated successfully")
                    }
                    else if (data.result == "Email verification fail") {
                        $scope.showMessagePopup("Message", "Email ID not exist")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not Updated")
                    }
                }
            );
    };

    $scope.deleteCustomer = function (customer) {
        dataFactory.deleteCustomer(customer)
            .then(function (data) {
                    $scope.getAdminDashboardData();
                    if (data.result == "Customer Deleted") {
                        $scope.showMessagePopup("Message", "Record deleted successfully")
                    }
                    else {
                        $scope.showMessagePopup("Message", "Record not deleted")
                    }
                }
            );
    };

    $scope.validateEmailApi = function (email) {
        $scope.emailNotExist = "";
        if (email) {
            var pattern = /^[_a-zA-z0-9]+(\.[_a-zA-z0-9]+)*@[a-zA-z0-9-]+(\.[a-zA-z0-9-]+)*(\.[a-zA-z]{1,255})$/;
            var result = pattern.test(email);
            if (result) {
                email_data = {
                    "email": email
                };
                dataFactory.validateEmail(email_data)
                    .then(function (data) {
                            if (data.result == "fail") {
                                $scope.emailNotExist = "Email Id Doesn't Exist";
                            }
                            else {
                                $scope.emailNotExist = "";
                            }
                        }
                    );
            }
        }
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                }
            },
            size: 'sm'
        });
    };

    $scope.domainChangePopup = function (customer) {
        var currentDomain = customer.email.split("@")[1];
        $scope.selectedCustomerDomain = {
            "id": customer.id,
            "currentDomain": currentDomain,
            "newDomain": ""
        };
        $scope.getUsersByCustomer($scope.selectedCustomer);
        $scope.updateAccountForm.$setUntouched();
    };

    $scope.changeDomain = function (selectedCustomerDomain) {
        dataFactory.changeDomain(selectedCustomerDomain)
            .then(function (data) {
                    if (data.data) {
                        $scope.getAdminDashboardData();
                        $scope.showMessagePopup("Message", "Domain Changed");
                    }
                    else {
                        $scope.showMessagePopup("Message", "Domain change failed");
                    }
                }
            );
    };

    $scope.getAdminDashboardData();
});