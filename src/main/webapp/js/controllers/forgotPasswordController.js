/**
 * Created by Fallon Software on 3/1/2016.
 */
instaKYC.controller('forgotPasswordController', function ($scope, $uibModal, $window, CommonServices, dataFactory) {

    $scope.error = CommonServices.getParameterByName("error");
    $scope.OtpPage = false;
    $scope.showGenerateOTP = true;
    if ($scope.error == "true") {
        $scope.error = true;
    }

    $scope.generateOTP = function (email) {
        dataFactory.generateOTP(email)
            .then(function (data) {
                    if (data.result == "OTP has been sent to your registered Mobile number") {
                        $scope.showMessagePopup("Message", data.result);
                    }
                    else {
                        $scope.showMessagePopup("Error", "Please Enter Valid User ID");
                    }
                }
            );
    };

    $scope.generateOtpPassword = function (email) {
        dataFactory.generateOtpPassword(email)
            .then(function (data) {
                    if (data.result == "Revised password has been mailed to the registered Email Id") {
                        $scope.showMessagePopup("Message", data.result);
                    }
                    else if (data.result == "Your account has been deactivated. Please contact to admin" || data.result == "Email doesn't exist" ||
                    		data.result == "Email session expired. please regenerate Password") {
                      //  $scope.invalidOtpMessage = data.result;
                    	$scope.showMessagePopup("Error", data.result);
                    }
                    else {
                        $scope.showMessagePopup("Error", "Please Enter Valid Email ID");
                    }
                }
            );
    };

    var displayOtpPage = function () {
        $scope.showGenerateOTP = false;
        $scope.OtpPage = true;
    };

    $scope.showMessagePopup = function (message_heading, message) {
        var showMessageModal = $uibModal.open({
            templateUrl: 'message.html',
            controller: function ($scope) {
                $scope.message_heading = message_heading;
                $scope.message = message;
                $scope.close = function () {
                    showMessageModal.dismiss('cancel');
                };
                $scope.displayOtpPage = function () {
                    showMessageModal.dismiss('cancel');
                  /*  if (message == "OTP has been sent to your registered Mobile number") {
                        displayOtpPage();
                    }*/
                    if (message == "Revised password has been mailed to the registered Email Id" || message == "Your account has been deactivated. Please contact to admin") {
                        $window.location.href = "/login"
                    }
                }
            },
            size: 'sm'
        });
    }

});